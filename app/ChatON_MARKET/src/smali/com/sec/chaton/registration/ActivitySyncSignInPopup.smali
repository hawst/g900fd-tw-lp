.class public Lcom/sec/chaton/registration/ActivitySyncSignInPopup;
.super Lcom/sec/chaton/base/BaseActivity;
.source "ActivitySyncSignInPopup.java"

# interfaces
.implements Lcom/coolots/sso/a/c;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:I

.field private E:I

.field private F:Z

.field private G:Landroid/view/View;

.field private H:Lcom/sec/chaton/d/ap;

.field private I:Lcom/coolots/sso/a/a;

.field private J:Lcom/sec/common/a/d;

.field private K:Landroid/os/Handler;

.field private L:Landroid/os/Handler;

.field private M:Landroid/os/Handler;

.field private N:Landroid/content/BroadcastReceiver;

.field private O:Lcom/sec/chaton/samsungaccount/bi;

.field private P:Landroid/os/Handler;

.field a:Landroid/os/Handler;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private h:Landroid/content/Context;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/app/ProgressDialog;

.field private o:Lcom/sec/common/a/d;

.field private p:Lcom/sec/chaton/samsungaccount/ba;

.field private q:Lcom/sec/chaton/d/at;

.field private r:Lcom/sec/chaton/d/h;

.field private s:Lcom/sec/chaton/chat/fh;

.field private t:Z

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 65
    const-class v0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    .line 69
    iput v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->c:I

    .line 70
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->d:I

    .line 71
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->e:I

    .line 72
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->f:I

    .line 73
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g:I

    .line 82
    iput-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    .line 84
    iput-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->p:Lcom/sec/chaton/samsungaccount/ba;

    .line 105
    iput v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->E:I

    .line 107
    iput-boolean v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->F:Z

    .line 108
    iput-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->G:Landroid/view/View;

    .line 111
    iput-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->I:Lcom/coolots/sso/a/a;

    .line 344
    new-instance v0, Lcom/sec/chaton/registration/r;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/r;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->K:Landroid/os/Handler;

    .line 563
    new-instance v0, Lcom/sec/chaton/registration/t;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/t;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->L:Landroid/os/Handler;

    .line 625
    new-instance v0, Lcom/sec/chaton/registration/u;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/u;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->M:Landroid/os/Handler;

    .line 925
    new-instance v0, Lcom/sec/chaton/registration/v;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/v;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->N:Landroid/content/BroadcastReceiver;

    .line 1103
    new-instance v0, Lcom/sec/chaton/registration/w;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/w;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a:Landroid/os/Handler;

    .line 1170
    new-instance v0, Lcom/sec/chaton/registration/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/z;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->O:Lcom/sec/chaton/samsungaccount/bi;

    .line 1183
    new-instance v0, Lcom/sec/chaton/registration/aa;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/aa;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->P:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;I)I
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->E:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->G:Landroid/view/View;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 304
    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->u:Z

    if-eqz v0, :cond_2

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SSO was installed, SSO ver : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->D:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->D:I

    const v1, 0x24ab8

    if-lt v0, v1, :cond_0

    .line 308
    invoke-direct {p0, v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Z)V

    .line 342
    :goto_0
    return-void

    .line 309
    :cond_0
    iget v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->D:I

    const/16 v1, 0x32c9

    if-lt v0, v1, :cond_1

    .line 310
    invoke-direct {p0, v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Z)V

    goto :goto_0

    .line 312
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b()V

    goto :goto_0

    .line 316
    :cond_2
    const-string v0, "SSO was NOT installed"

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 329
    :cond_3
    new-instance v0, Lcom/sec/chaton/samsungaccount/ba;

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->O:Lcom/sec/chaton/samsungaccount/bi;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/samsungaccount/ba;-><init>(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/bi;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->p:Lcom/sec/chaton/samsungaccount/ba;

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->p:Lcom/sec/chaton/samsungaccount/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/samsungaccount/ba;->show()V

    goto :goto_0

    .line 335
    :cond_4
    invoke-direct {p0, v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->c(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Lcom/sec/chaton/registration/ab;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ab;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Z)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->c(Z)V

    return-void
.end method

.method private a(Lcom/sec/chaton/registration/ab;Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0b016c

    .line 1207
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g()V

    .line 1209
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "network error, type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/registration/ab;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " errorCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1213
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1278
    :cond_1
    :goto_0
    return-void

    .line 1217
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0024

    new-instance v2, Lcom/sec/chaton/registration/o;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/registration/o;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Lcom/sec/chaton/registration/ab;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/registration/n;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/registration/n;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Lcom/sec/chaton/registration/ab;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->J:Lcom/sec/common/a/d;

    .line 1275
    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->F:Z

    if-nez v0, :cond_1

    .line 1276
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->J:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1030
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->G:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1032
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->G:Landroid/view/View;

    const v1, 0x7f070369

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1033
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1036
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1006
    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->F:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1026
    :cond_0
    :goto_0
    return-void

    .line 1009
    :cond_1
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->G:Landroid/view/View;

    .line 1011
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->G:Landroid/view/View;

    const v1, 0x7f07036a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1012
    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->G:Landroid/view/View;

    const v2, 0x7f070369

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1014
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1015
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1017
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->G:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    .line 1023
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    invoke-interface {v0, v3}, Lcom/sec/common/a/d;->setCancelable(Z)V

    .line 1025
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    .line 282
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 283
    const-string v0, "startActivitySSO"

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 286
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "login_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "login_id_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mcc"

    aput-object v2, v0, v1

    const-string v1, "api_server_url"

    aput-object v1, v0, v5

    const/4 v1, 0x5

    const-string v2, "birthday"

    aput-object v2, v0, v1

    .line 287
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.REQUEST_ACCESSTOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 288
    const-string v2, "client_id"

    const-string v3, "fs24s8z0hh"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    const-string v2, "client_secret"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    const-string v2, "progress_theme"

    const-string v3, "invisible"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    const-string v2, "additional"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 293
    if-eqz p1, :cond_1

    .line 294
    const-string v0, "expired_access_token"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "samsung_account_token"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    :cond_1
    invoke-virtual {p0, v1, v5}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->startActivityForResult(Landroid/content/Intent;I)V

    .line 301
    :goto_0
    return-void

    .line 299
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->c()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->t:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->B:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.request.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 674
    const-string v1, "client_id"

    const-string v2, "fs24s8z0hh"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 675
    const-string v1, "client_secret"

    const-string v2, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 676
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 677
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 679
    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->sendBroadcast(Landroid/content/Intent;)V

    .line 703
    :goto_0
    return-void

    .line 681
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 684
    :cond_1
    const-string v0, "ActivitySignIn will be run"

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 686
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 687
    const-string v1, "client_id"

    const-string v2, "fs24s8z0hh"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 688
    const-string v1, "client_secret"

    const-string v2, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 689
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 693
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 695
    :catch_0
    move-exception v0

    .line 697
    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/samsungaccount/ay;->b:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    .line 698
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Z)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 5

    .prologue
    .line 707
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 708
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "email_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mcc"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "api_server_url"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "birthday"

    aput-object v2, v0, v1

    .line 709
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 710
    const-string v2, "client_id"

    const-string v3, "fs24s8z0hh"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 711
    const-string v2, "client_secret"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 712
    const-string v2, "mypackage"

    iget-object v3, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->x:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 713
    const-string v2, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 715
    const-string v2, "MODE"

    const-string v3, "HIDE_NOTIFICATION_WITH_RESULT"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 716
    const-string v2, "additional"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 718
    if-eqz p1, :cond_0

    .line 720
    const-string v0, "expired_access_token"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "samsung_account_token"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 723
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->sendBroadcast(Landroid/content/Intent;)V

    .line 729
    :goto_0
    return-void

    .line 726
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->c()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->C:Ljava/lang/String;

    return-object p1
.end method

.method private c()V
    .locals 3

    .prologue
    .line 732
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 735
    :cond_0
    const-string v0, "ActivitySignIn will be run"

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 737
    const-string v1, "client_id"

    const-string v2, "fs24s8z0hh"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 738
    const-string v1, "client_secret"

    const-string v2, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 739
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 740
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 741
    const-string v1, "MODE"

    const-string v2, "ADD_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 745
    const/4 v1, 0x2

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 754
    :goto_0
    return-void

    .line 747
    :catch_0
    move-exception v0

    .line 749
    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/samsungaccount/ay;->b:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    .line 750
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->e()V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Z)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Z)V

    return-void
.end method

.method private c(Z)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1061
    const-string v0, "mum_enable_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1062
    if-nez p1, :cond_0

    .line 1063
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->f()V

    .line 1084
    :goto_0
    return-void

    .line 1068
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 1069
    invoke-static {v3}, Lcom/sec/chaton/e/a/f;->b(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v4

    .line 1070
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1071
    invoke-static {v3}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v6

    move v1, v2

    .line 1073
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1074
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v0}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;

    move-result-object v7

    .line 1075
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v8, "Favorites"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1076
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1073
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1081
    :cond_2
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->P:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1082
    const-string v1, "group"

    const/16 v3, 0x148

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZILjava/util/ArrayList;Ljava/util/HashMap;)I

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->A:Ljava/lang/String;

    return-object p1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 915
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 916
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 919
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 920
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->setResult(ILandroid/content/Intent;)V

    .line 921
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->finish()V

    .line 922
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a()V

    return-void
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1041
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1057
    :cond_0
    :goto_0
    return-void

    .line 1044
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->F:Z

    if-nez v0, :cond_0

    .line 1048
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1050
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->u:Z

    if-ne v1, v4, :cond_2

    .line 1051
    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b028b

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v6, v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    .line 1056
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 1053
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b028c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v6, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    goto :goto_1
.end method

.method static synthetic e(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->F:Z

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 1088
    .line 1091
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->e()V

    .line 1093
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->H:Lcom/sec/chaton/d/ap;

    .line 1095
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1096
    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->v:Ljava/lang/String;

    .line 1097
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Samsung email : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1099
    iget-object v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->H:Lcom/sec/chaton/d/ap;

    invoke-virtual {v2, v1, v0}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1282
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dismissProgressBar, progressBar : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mMultiDeviceDialog : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1287
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1288
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1291
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1292
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 1293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->G:Landroid/view/View;

    .line 1295
    :cond_2
    return-void

    .line 1283
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic h(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->M:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->I:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->r:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->D:I

    return v0
.end method

.method static synthetic n(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)I
    .locals 2

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->E:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->E:I

    return v0
.end method

.method static synthetic o(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->w:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->E:I

    return v0
.end method

.method static synthetic q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->C:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/at;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q:Lcom/sec/chaton/d/at;

    return-object v0
.end method

.method static synthetic u(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->d()V

    return-void
.end method

.method static synthetic v(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->A:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic w(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic x(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->j:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic y(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/chat/fh;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->s:Lcom/sec/chaton/chat/fh;

    return-object v0
.end method


# virtual methods
.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 1304
    const v0, 0x7f0c0101

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 1299
    const v0, 0x7f0c0101

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 11

    .prologue
    const v10, 0x7f0b0290

    const/4 v9, 0x0

    const/4 v0, -0x1

    .line 760
    packed-switch p1, :pswitch_data_0

    .line 912
    :cond_0
    :goto_0
    return-void

    .line 762
    :pswitch_0
    if-ne p2, v0, :cond_0

    .line 764
    const-string v0, "country_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 766
    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 767
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->y:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->z:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    invoke-direct {p0, v9}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->c(Z)V

    goto :goto_0

    .line 773
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->e()V

    .line 775
    if-ne p2, v0, :cond_3

    .line 776
    const-string v0, "authcode"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->A:Ljava/lang/String;

    .line 777
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->A:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 778
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q:Lcom/sec/chaton/d/at;

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->A:Ljava/lang/String;

    const-string v2, "fs24s8z0hh"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "authcode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 780
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 781
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 785
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto :goto_1

    .line 790
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 791
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 796
    :cond_4
    const-string v0, "SIGN_IN : result is ERROR"

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 801
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->e()V

    .line 803
    if-ne p2, v0, :cond_8

    .line 804
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->w:Ljava/lang/String;

    .line 807
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 808
    iget v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->D:I

    const v1, 0x24ab8

    if-lt v0, v1, :cond_5

    .line 809
    invoke-direct {p0, v9}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Z)V

    goto/16 :goto_0

    .line 810
    :cond_5
    iget v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->D:I

    const/16 v1, 0x32c9

    if-lt v0, v1, :cond_0

    .line 811
    invoke-direct {p0, v9}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Z)V

    goto/16 :goto_0

    .line 814
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 815
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 819
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0

    .line 824
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 825
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 827
    :cond_9
    const-string v0, ""

    .line 829
    if-eqz p3, :cond_a

    .line 831
    const-string v0, "error_message"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 837
    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NEW_SIGN_IN : result is ERROR, errorMessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 843
    :pswitch_3
    if-ne p2, v0, :cond_0

    .line 844
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->finish()V

    goto/16 :goto_0

    .line 850
    :pswitch_4
    if-ne p2, v0, :cond_e

    .line 851
    const-string v0, "access_token"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 852
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 853
    const-string v1, "login_id"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 854
    const-string v2, "login_id_type"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 855
    const-string v3, "user_id"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 856
    const-string v4, "mcc"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 857
    const-string v5, "api_server_url"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 858
    const-string v6, "birthday"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 860
    sget-boolean v7, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v7, :cond_b

    .line 861
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[ActivityResult] authToken : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " loginID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " userId : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " loginType : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " mcc : "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " birthday : "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " server : "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v2, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    :cond_b
    const-string v2, "samsung_account_token"

    invoke-static {v2, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    const-string v0, "samsung_account_email"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    const-string v0, "samsung_account_user_id"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    const-string v0, "samsung_account_api_server"

    invoke-static {v0, v5}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    const-string v0, "samsung_account_birthday"

    invoke-static {v0, v6}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->y:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->z:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 876
    invoke-static {}, Lcom/sec/chaton/samsungaccount/MainActivity;->c()V

    .line 877
    invoke-direct {p0, v9}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->c(Z)V

    goto/16 :goto_0

    .line 880
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 881
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 884
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0

    .line 886
    :cond_e
    if-nez p2, :cond_f

    .line 887
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->finish()V

    goto/16 :goto_0

    .line 889
    :cond_f
    if-eqz p3, :cond_12

    .line 890
    const-string v0, "error_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 891
    const-string v1, "error_message"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 893
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_10

    .line 894
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[ActivityResult] errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " errorMessage : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    :cond_10
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 903
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 906
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0

    .line 898
    :cond_12
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_10

    .line 899
    const-string v0, "data is null"

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 760
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 128
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 130
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 131
    const-string v0, "onCreate"

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_0
    const v0, 0x7f0300d7

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->setContentView(I)V

    .line 139
    iput-object p0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->x:Ljava/lang/String;

    .line 143
    invoke-static {p0}, Lcom/sec/chaton/util/cb;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->D:I

    .line 145
    iget v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->D:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_3

    .line 146
    iput-boolean v6, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->u:Z

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->w:Ljava/lang/String;

    .line 153
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->v:Ljava/lang/String;

    .line 157
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0280

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 158
    aget-object v1, v0, v5

    iput-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->y:Ljava/lang/String;

    .line 159
    aget-object v0, v0, v6

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->z:Ljava/lang/String;

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->K:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/at;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q:Lcom/sec/chaton/d/at;

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->L:Landroid/os/Handler;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "multidevice_chatlist_sync_last_time"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/chat/fh;->a(Landroid/os/Handler;J)Lcom/sec/chaton/chat/fh;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->s:Lcom/sec/chaton/chat/fh;

    .line 164
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->L:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->r:Lcom/sec/chaton/d/h;

    .line 167
    const v0, 0x7f0703c3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->i:Landroid/widget/Button;

    .line 168
    const v0, 0x7f0703c4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->j:Landroid/widget/Button;

    .line 169
    const v0, 0x7f07025c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->k:Landroid/widget/ImageView;

    .line 170
    const v0, 0x7f07014c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l:Landroid/widget/TextView;

    .line 171
    const v0, 0x7f07014d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->m:Landroid/widget/TextView;

    .line 173
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "isSyncContacts"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->t:Z

    .line 175
    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->t:Z

    if-ne v0, v6, :cond_1

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->i:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->k:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->m:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 184
    const-string v1, "action_sso_receive_code"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 185
    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->N:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 189
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->h:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->I:Lcom/coolots/sso/a/a;

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->I:Lcom/coolots/sso/a/a;

    invoke-virtual {v0, p0, p0}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 195
    :cond_2
    return-void

    .line 148
    :cond_3
    iput-boolean v5, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->u:Z

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 250
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onDestroy()V

    .line 251
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 252
    const-string v0, "onDestroy"

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->I:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->I:Lcom/coolots/sso/a/a;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->N:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->F:Z

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->o:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 274
    iput-object v2, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->G:Landroid/view/View;

    .line 277
    :cond_3
    return-void

    .line 260
    :catch_0
    move-exception v0

    .line 261
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1311
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveCreateAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->u:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->u:Z

    if-ne v0, v3, :cond_1

    .line 1316
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->r:Lcom/sec/chaton/d/h;

    const-string v1, "voip"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;IZ)V

    .line 1325
    :goto_0
    return-void

    .line 1318
    :cond_1
    new-instance v0, Lcom/sec/chaton/registration/an;

    invoke-direct {v0}, Lcom/sec/chaton/registration/an;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/registration/an;->a()V

    .line 1319
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Ljava/lang/String;)V

    .line 1320
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 1321
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->r:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 1322
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->r:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto :goto_0
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 0

    .prologue
    .line 1331
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 239
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onResume()V

    .line 241
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 242
    const-string v0, "onResume"

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onStart()V

    .line 202
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 203
    const-string v0, "onStart"

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->i:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/registration/m;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/m;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->j:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/registration/q;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/q;-><init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    return-void
.end method
