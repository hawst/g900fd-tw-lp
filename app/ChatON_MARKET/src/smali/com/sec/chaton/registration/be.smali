.class Lcom/sec/chaton/registration/be;
.super Landroid/os/Handler;
.source "FragmentGlobalDisclaimer.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 217
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 219
    :sswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->b(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    goto :goto_0

    .line 229
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xabe2

    if-eq v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xabe1

    if-ne v1, v2, :cond_4

    .line 232
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V

    .line 234
    const-string v0, "server sent error cause by WRONG_PARAMETER_VALUE"

    invoke-static {}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/registration/bf;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/bf;-><init>(Lcom/sec/chaton/registration/be;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 246
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    sget-object v2, Lcom/sec/chaton/registration/bl;->a:Lcom/sec/chaton/registration/bl;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;Lcom/sec/chaton/registration/bl;Ljava/lang/String;)V

    .line 249
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dev]GLD failed. httpStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", FaultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V

    goto/16 :goto_0

    .line 259
    :sswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_9

    .line 261
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 262
    if-eqz v1, :cond_8

    .line 265
    iget-object v0, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "3.3.51"

    iget-object v2, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 266
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new version : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/cb;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->b(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->c()V

    goto/16 :goto_0

    .line 283
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v2, "NO"

    invoke-static {v1, v0, v3, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    goto/16 :goto_0

    .line 289
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->b(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    const-string v1, "FIRST"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/bj;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 293
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    sget-object v2, Lcom/sec/chaton/registration/bl;->c:Lcom/sec/chaton/registration/bl;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;Lcom/sec/chaton/registration/bl;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 296
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    sget-object v2, Lcom/sec/chaton/registration/bl;->c:Lcom/sec/chaton/registration/bl;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;Lcom/sec/chaton/registration/bl;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 302
    :sswitch_2
    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->d(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->d(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 303
    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->d(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 306
    :cond_a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_c

    :cond_b
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xa411

    if-ne v1, v2, :cond_e

    .line 307
    :cond_c
    const-string v0, "first_disclaimer_status"

    const-string v1, "DONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->b:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_disclaimer_status"

    const-string v2, "DONE"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 319
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "accept_disclaimer_iso"

    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_d
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 326
    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 327
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 310
    :catch_0
    move-exception v0

    .line 311
    const-string v0, "the task is already running or finished"

    invoke-static {}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 336
    :cond_e
    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 338
    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    sget-object v2, Lcom/sec/chaton/registration/bl;->d:Lcom/sec/chaton/registration/bl;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;Lcom/sec/chaton/registration/bl;Ljava/lang/String;)V

    .line 339
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dev]Accept Disclaimer failed. httpStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", FaultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 344
    :sswitch_3
    const-string v1, "2"

    .line 345
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/AvaliableApps;

    .line 347
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_f

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    if-eqz v0, :cond_f

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->e(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "YES"

    invoke-static {v0, v1, v3, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 350
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 351
    const-string v0, "Samsung apps is ready to upgrade chaton"

    invoke-static {}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 354
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->e(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/be;->a:Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "NO"

    invoke-static {v0, v1, v3, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 355
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 356
    const-string v0, "Samsung apps is NOT ready to upgrade chaton"

    invoke-static {}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 217
    nop

    :sswitch_data_0
    .sparse-switch
        0x68 -> :sswitch_0
        0x450 -> :sswitch_1
        0x451 -> :sswitch_2
        0x452 -> :sswitch_3
    .end sparse-switch
.end method
