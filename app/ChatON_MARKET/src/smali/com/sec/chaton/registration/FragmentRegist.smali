.class public Lcom/sec/chaton/registration/FragmentRegist;
.super Landroid/support/v4/app/Fragment;
.source "FragmentRegist.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;


# instance fields
.field private A:Lcom/sec/chaton/widget/ClearableEditText;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/Button;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/TextView;

.field private F:Landroid/widget/TextView;

.field private G:Landroid/view/View;

.field private H:Ljava/lang/String;

.field private I:Landroid/app/ProgressDialog;

.field private J:Landroid/app/ProgressDialog;

.field private K:Landroid/content/Context;

.field private L:Landroid/view/View;

.field private M:Z

.field private N:Lcom/sec/chaton/d/h;

.field private O:Lcom/sec/chaton/d/ap;

.field private P:Lcom/sec/common/a/d;

.field private Q:Ljava/lang/String;

.field private final R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/String;

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Landroid/os/Bundle;

.field private Z:Z

.field private aa:Landroid/text/TextWatcher;

.field private ab:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field d:Landroid/view/LayoutInflater;

.field e:Landroid/view/View;

.field f:Landroid/view/View$OnClickListener;

.field g:Landroid/view/View$OnClickListener;

.field h:Landroid/os/Handler;

.field i:Landroid/os/Handler;

.field public j:Landroid/os/Handler;

.field k:Landroid/os/Handler;

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private t:[Ljava/lang/CharSequence;

.field private u:[Ljava/lang/CharSequence;

.field private v:[Ljava/lang/CharSequence;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Landroid/widget/LinearLayout;

.field private z:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-string v0, "regi_mode"

    sput-object v0, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    .line 97
    const-string v0, "before_regi"

    sput-object v0, Lcom/sec/chaton/registration/FragmentRegist;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 84
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 88
    const/16 v0, 0x20

    iput v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->l:I

    .line 89
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->m:I

    .line 91
    const/16 v0, 0x30

    iput v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->n:I

    .line 92
    const/16 v0, 0x40

    iput v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->o:I

    .line 99
    const-string v0, "SMS"

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->p:Ljava/lang/String;

    .line 100
    const-string v0, "ACS"

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->q:Ljava/lang/String;

    .line 132
    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    .line 133
    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->J:Landroid/app/ProgressDialog;

    .line 138
    iput-boolean v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->M:Z

    .line 141
    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    .line 152
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "country_letter"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->R:Ljava/lang/String;

    .line 161
    iput-boolean v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->U:Z

    .line 170
    iput-boolean v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->X:Z

    .line 391
    new-instance v0, Lcom/sec/chaton/registration/cn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/cn;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->f:Landroid/view/View$OnClickListener;

    .line 736
    new-instance v0, Lcom/sec/chaton/registration/cy;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/cy;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->aa:Landroid/text/TextWatcher;

    .line 1134
    new-instance v0, Lcom/sec/chaton/registration/cz;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/cz;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->g:Landroid/view/View$OnClickListener;

    .line 1279
    new-instance v0, Lcom/sec/chaton/registration/db;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/db;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->h:Landroid/os/Handler;

    .line 1663
    new-instance v0, Lcom/sec/chaton/registration/di;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/registration/di;-><init>(Lcom/sec/chaton/registration/FragmentRegist;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->i:Landroid/os/Handler;

    .line 1755
    new-instance v0, Lcom/sec/chaton/registration/co;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/co;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->j:Landroid/os/Handler;

    .line 1971
    new-instance v0, Lcom/sec/chaton/registration/ct;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ct;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->k:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentRegist;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1913
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/ActivityNonSelfSMS;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1914
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1915
    if-eqz v1, :cond_0

    .line 1916
    const-string v2, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1917
    if-eqz v1, :cond_0

    const-string v2, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1918
    const-string v1, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    const-string v2, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1935
    :cond_0
    const-string v1, "SMS"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1936
    const-string v1, "isSMS"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1942
    :cond_1
    :goto_0
    const-string v1, "normal_ACS"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1943
    const-string v1, "isSMS"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1944
    const-string v1, "normal_acs"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1952
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "skipRegi"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1954
    const-string v1, "from_regist"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1957
    :cond_3
    iget-boolean v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->V:Z

    if-eqz v1, :cond_4

    .line 1958
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1962
    :cond_4
    iget-boolean v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->W:Z

    if-eqz v1, :cond_5

    .line 1963
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1967
    :cond_5
    const/16 v1, 0x20

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/FragmentRegist;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1969
    return-void

    .line 1937
    :cond_6
    const-string v1, "ACS"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1938
    const-string v1, "isSMS"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentRegist;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v4, 0x40

    const/4 v3, 0x0

    const/16 v2, 0xa

    const/4 v5, 0x1

    .line 1167
    iget-boolean v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->V:Z

    if-eqz v0, :cond_4

    .line 1168
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1169
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "ZW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1170
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->K:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/AdminMenu;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1171
    const-string v1, "mapping_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1172
    const-string v1, "general_tab"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1173
    invoke-virtual {p0, v0, v4}, Lcom/sec/chaton/registration/FragmentRegist;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1175
    :cond_0
    invoke-static {v5}, Lcom/sec/chaton/util/y;->a(I)V

    .line 1176
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "Log On"

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1277
    :cond_1
    :goto_0
    return-void

    .line 1179
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1180
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "ZW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1181
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->K:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/AdminMenu;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1182
    const-string v1, "mapping_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1183
    const-string v1, "general_tab"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1184
    invoke-virtual {p0, v0, v4}, Lcom/sec/chaton/registration/FragmentRegist;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1186
    :cond_3
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/chaton/util/y;->a(I)V

    .line 1187
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "Log On With Save"

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1193
    :cond_4
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentRegist;->T:Ljava/lang/String;

    .line 1194
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->T:Ljava/lang/String;

    const-string v1, "SMS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1197
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "AR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v0, v1

    if-ne v0, v2, :cond_7

    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "MX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v0, v1

    if-lt v0, v2, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v0, v1

    if-ge v0, v2, :cond_8

    .line 1201
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b021e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/registration/da;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/da;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    .line 1207
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1208
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 1221
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gtz v0, :cond_9

    .line 1222
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00ad

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1231
    :cond_9
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1232
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1236
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "AR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "MX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1238
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mCountryCallingCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " userAreaCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " userPhoneNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1245
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->h:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/l;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/l;

    move-result-object v0

    .line 1246
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->h:Landroid/os/Handler;

    invoke-static {v1}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->O:Lcom/sec/chaton/d/ap;

    .line 1247
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->K:Landroid/content/Context;

    if-eqz v1, :cond_c

    .line 1248
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->K:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00b6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    .line 1251
    :cond_c
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v2, "AR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v2, "MX"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v2, "BR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1253
    :cond_d
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v3}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/l;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1273
    :goto_2
    const-string v0, "country_name"

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1275
    const-string v0, "country_code"

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1276
    const-string v0, "temp_country_ISO"

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1241
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mCountryCallingCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " userPhoneNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1256
    :cond_f
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v2}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/l;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentRegist;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Z:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentRegist;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentRegist;->J:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentRegist;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/FragmentRegist;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0b016c

    .line 1569
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1570
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "network error, errorCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1572
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1573
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1576
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/registration/dh;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/dh;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    .line 1587
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1588
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1590
    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentRegist;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/sec/chaton/registration/FragmentRegist;->M:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/widget/ClearableEditText;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 727
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 729
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    .line 730
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    .line 733
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 944
    .line 945
    new-instance v2, Lcom/sec/chaton/d/h;

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->k:Landroid/os/Handler;

    invoke-direct {v2, v0}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 946
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 948
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "AR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "MX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 950
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 959
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addBuddyInternal orgnum="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", country code="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;Z)Lcom/sec/chaton/d/a/h;

    .line 961
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, "+"

    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->ab:Ljava/lang/String;

    .line 962
    return-void

    .line 953
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 957
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 960
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 961
    :cond_4
    const-string v1, ""

    goto :goto_2
.end method

.method static synthetic e(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->C:Landroid/widget/Button;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1593
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1594
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1596
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->T:Ljava/lang/String;

    const-string v1, "SMS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1597
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "SMS"

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1609
    :cond_1
    :goto_0
    return-void

    .line 1598
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->T:Ljava/lang/String;

    const-string v1, "ACS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1599
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/registration/ActivitySelectLanguage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1601
    iget-boolean v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->V:Z

    if-ne v1, v3, :cond_3

    .line 1602
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1605
    :cond_3
    const/16 v1, 0x30

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/registration/FragmentRegist;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    return-object v0
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1612
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "temp_country_ISO"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1613
    const-string v1, "msisdn"

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1614
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "phonenumber"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1615
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn_temp"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1617
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1619
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1620
    const-string v2, "com.sec.chaton"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v2, v2

    if-gtz v2, :cond_3

    .line 1621
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 1622
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1623
    new-instance v3, Landroid/accounts/Account;

    const-string v4, "ChatON"

    const-string v5, "com.sec.chaton"

    invoke-direct {v3, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1625
    invoke-virtual {v1, v3, v2, v7}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 1629
    const-string v1, "com.android.contacts"

    invoke-static {v3, v1, v6}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 1648
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "account_country_code"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v6, :cond_4

    invoke-static {v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1651
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1652
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v7, v2, v6}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    .line 1654
    :cond_1
    invoke-static {}, Lcom/sec/chaton/event/f;->a()V

    .line 1655
    const-string v1, "country_ISO"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1656
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->i:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    .line 1661
    :cond_2
    :goto_1
    return-void

    .line 1631
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "samsung_account_email"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1634
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1640
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "ChatON"

    const-string v3, "com.sec.chaton"

    invoke-direct {v1, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1641
    const-string v2, "com.android.contacts"

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_0

    .line 1642
    const-string v2, "com.android.contacts"

    invoke-static {v1, v2, v6}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 1643
    const-string v2, "com.android.contacts"

    invoke-static {v1, v2, v6}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1658
    :cond_4
    const-string v1, "country_ISO"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1659
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegist;->g()V

    goto :goto_1
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1711
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1712
    const v1, 0x7f030107

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->L:Landroid/view/View;

    .line 1714
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->L:Landroid/view/View;

    const v1, 0x7f07013b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1715
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->L:Landroid/view/View;

    const v2, 0x7f07013c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 1718
    const-string v2, "for_wifi_only_device"

    invoke-static {v2}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1719
    new-instance v0, Lcom/sec/chaton/registration/dl;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/dl;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1734
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0277

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->L:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/registration/dm;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/dm;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    .line 1750
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1751
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1753
    :cond_0
    return-void

    .line 1731
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/registration/FragmentRegist;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegist;->h()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->T:Ljava/lang/String;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 2073
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2077
    if-eqz v0, :cond_0

    .line 2078
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2083
    :cond_0
    :goto_0
    return-void

    .line 2081
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/d/ap;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->O:Lcom/sec/chaton/d/ap;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/registration/FragmentRegist;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegist;->f()V

    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/registration/FragmentRegist;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Z:Z

    return v0
.end method

.method static synthetic m(Lcom/sec/chaton/registration/FragmentRegist;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegist;->e()V

    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->K:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/registration/FragmentRegist;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegist;->g()V

    return-void
.end method

.method static synthetic p(Lcom/sec/chaton/registration/FragmentRegist;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->M:Z

    return v0
.end method

.method static synthetic q(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->N:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->J:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->ab:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/registration/FragmentRegist;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegist;->d()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 9

    .prologue
    const v8, 0x7f0b03a1

    const v7, 0x7f0b03a0

    const v6, 0x7f030036

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1103
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1105
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->d:Landroid/view/LayoutInflater;

    .line 1106
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v6, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->e:Landroid/view/View;

    .line 1107
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->e:Landroid/view/View;

    const v1, 0x7f070144

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1108
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->e:Landroid/view/View;

    const v2, 0x7f070145

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1109
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->e:Landroid/view/View;

    const v3, 0x7f070146

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 1110
    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1111
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1112
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1114
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1115
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    .line 1130
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1132
    :cond_0
    return-void

    .line 1122
    :cond_1
    iput-object v4, p0, Lcom/sec/chaton/registration/FragmentRegist;->e:Landroid/view/View;

    .line 1123
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v6, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->e:Landroid/view/View;

    .line 1124
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method b()V
    .locals 7

    .prologue
    const v6, 0x7f0b03a2

    const/16 v5, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2088
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[CheckACSMode ] mISOcountryCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2099
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2242
    :goto_0
    return-void

    .line 2110
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "JP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "IN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2125
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 2126
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2129
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->D:Landroid/widget/TextView;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v6, v1}, Lcom/sec/chaton/registration/FragmentRegist;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2130
    const-string v0, "sim is america or japan, china, india"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2131
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "AR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "MX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2144
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    if-eqz v0, :cond_5

    .line 2145
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2146
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 2148
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/registration/cv;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/cv;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2161
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->D:Landroid/widget/TextView;

    const v1, 0x7f0b029e

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/registration/FragmentRegist;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2162
    const-string v0, "sim is argentina"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2164
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v1, Lcom/sec/chaton/registration/cw;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/cw;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    goto/16 :goto_0

    .line 2189
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    if-eqz v0, :cond_7

    .line 2190
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2193
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->D:Landroid/widget/TextView;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v6, v1}, Lcom/sec/chaton/registration/FragmentRegist;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sim is NOT america or japan, china, india : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v1, Lcom/sec/chaton/registration/cx;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/cx;-><init>(Lcom/sec/chaton/registration/FragmentRegist;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    goto/16 :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/16 v9, 0x1c

    const/16 v8, 0x15

    const/high16 v7, 0x41a80000    # 21.0f

    const/high16 v6, 0x41900000    # 18.0f

    const/high16 v5, 0x41600000    # 14.0f

    .line 429
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 435
    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v0

    .line 438
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 440
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 485
    :cond_0
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 486
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "country_code"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 487
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "country_ISO"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 491
    if-nez v2, :cond_6

    .line 492
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->s:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 493
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->r:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    .line 496
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 498
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    .line 502
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v8, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, v9, :cond_4

    .line 503
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextSize(F)V

    .line 513
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 514
    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[First]CallingCode : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ISOLetter : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " CountryName : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->b()V

    .line 563
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "AR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "MX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 565
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const/16 v5, 0xa

    invoke-direct {v3, v4, v5}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 656
    :goto_2
    invoke-static {}, Lcom/sec/chaton/util/am;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    .line 659
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->S:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 660
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->S:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Ljava/lang/CharSequence;)V

    .line 706
    :cond_3
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Auto Read #phone > phoneNum : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "CountryCallingCode :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->aa:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 713
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->aa:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 722
    return-void

    .line 505
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x1d

    if-le v2, v3, :cond_5

    .line 506
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextSize(F)V

    goto/16 :goto_0

    .line 509
    :cond_5
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setTextSize(F)V

    goto/16 :goto_0

    .line 517
    :cond_6
    iput-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    .line 518
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->s:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 519
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v8, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, v9, :cond_7

    .line 520
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextSize(F)V

    .line 530
    :goto_4
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 531
    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    .line 532
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SavedISO]CallingCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ISOLetter : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CountryName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 522
    :cond_7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x1d

    if-le v2, v3, :cond_8

    .line 523
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_4

    .line 526
    :cond_8
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_4

    .line 534
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->R:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 536
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->r:Ljava/util/Map;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->R:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 537
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->s:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->R:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 539
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[MID]countryName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " countrycode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ISOletter : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->R:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v8, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, v9, :cond_a

    .line 541
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextSize(F)V

    .line 551
    :goto_5
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " (+"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 552
    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    .line 553
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->R:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    goto/16 :goto_1

    .line 543
    :cond_a
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x1d

    if-le v2, v3, :cond_b

    .line 544
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_5

    .line 547
    :cond_b
    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_5

    .line 558
    :cond_c
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentRegist;->c()V

    goto/16 :goto_1

    .line 567
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const/16 v5, 0x14

    invoke-direct {v3, v4, v5}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    goto/16 :goto_2

    .line 661
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "AR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "MX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 663
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_3

    .line 665
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 668
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    const-string v1, "82"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 669
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 671
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 673
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 675
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    const-string v1, "82"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 676
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 677
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    const-string v1, "91"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 679
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_12

    .line 680
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 682
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 686
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 696
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    const-string v1, "0000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-le v0, v1, :cond_3

    .line 698
    const-string v0, "msisdn did NOT start with CC"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    .line 816
    sparse-switch p1, :sswitch_data_0

    .line 938
    :cond_0
    :goto_0
    return-void

    .line 819
    :sswitch_0
    if-ne p2, v2, :cond_0

    .line 820
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 821
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 822
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 828
    :sswitch_1
    if-ne p2, v4, :cond_0

    .line 829
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "normal_ACS"

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 834
    :sswitch_2
    if-ne p2, v4, :cond_0

    .line 835
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 837
    const-string v1, "PARAMS_COUNTRY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 838
    if-eqz v1, :cond_5

    .line 839
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->x:Ljava/lang/String;

    .line 840
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->r:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    .line 841
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->s:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x15

    if-le v0, v2, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->s:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x1c

    if-gt v0, v2, :cond_3

    .line 842
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 852
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->s:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 856
    const-string v0, "country_name"

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SelectCountry] NewCallingCode : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " PreviusCallingCode : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->x:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->x:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->x:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 866
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 868
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 872
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    .line 883
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->b()V

    .line 886
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "AR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "MX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 888
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    new-array v1, v6, [Landroid/text/InputFilter;

    new-instance v2, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/16 v4, 0xa

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    goto/16 :goto_0

    .line 844
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->s:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x1d

    if-le v0, v2, :cond_4

    .line 845
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    const/high16 v2, 0x41600000    # 14.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    goto/16 :goto_1

    .line 848
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    const/high16 v2, 0x41a80000    # 21.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    goto/16 :goto_1

    .line 876
    :cond_5
    const-string v1, "PARAMS_COUNTRY_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 877
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " (+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 878
    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->w:Ljava/lang/String;

    goto/16 :goto_2

    .line 890
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    new-array v1, v6, [Landroid/text/InputFilter;

    new-instance v2, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/16 v4, 0x14

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    goto/16 :goto_0

    .line 896
    :sswitch_3
    if-ne p2, v4, :cond_8

    .line 902
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 908
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "skipRegi"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 909
    const-string v1, "skipRegi"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 912
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 913
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 918
    :cond_8
    if-nez p2, :cond_9

    .line 921
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 924
    :cond_9
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 925
    const-string v0, "skipRegi"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 926
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "acstoken"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 927
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "was_get_version"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 928
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "country_ISO"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 929
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 931
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "over_sms_request"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 932
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "over_acs_request"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 934
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "authnum"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 816
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x30 -> :sswitch_1
        0x40 -> :sswitch_0
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1035
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1099
    :goto_0
    return-void

    .line 1039
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1042
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "JP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "IN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "EG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "TR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "UA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "CZ"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "ZA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1046
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->a()V

    goto :goto_0

    .line 1047
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "MM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1049
    const-string v0, "ACS"

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1052
    :cond_3
    const-string v0, "SMS"

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1039
    nop

    :pswitch_data_0
    .packed-switch 0x7f070403
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 181
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 183
    const-string v1, "onCreate..."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    if-eqz p1, :cond_0

    .line 186
    iput-boolean v4, p0, Lcom/sec/chaton/registration/FragmentRegist;->U:Z

    .line 190
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Y:Landroid/os/Bundle;

    .line 192
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Y:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    .line 194
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Y:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->V:Z

    .line 195
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->Y:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/registration/FragmentRegist;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->W:Z

    .line 199
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "skipRegi"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->V:Z

    if-ne v1, v4, :cond_2

    .line 203
    iget-boolean v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->U:Z

    if-nez v1, :cond_2

    .line 204
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "isSMS"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 205
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "SMS"

    invoke-direct {p0, v1, v2}, Lcom/sec/chaton/registration/FragmentRegist;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 218
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->K:Landroid/content/Context;

    .line 219
    invoke-static {p0, v4}, Lcom/sec/chaton/base/BaseActivity;->b(Landroid/support/v4/app/Fragment;Z)V

    .line 221
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 222
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/m;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    .line 224
    :cond_3
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->r:Ljava/util/Map;

    .line 225
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->s:Ljava/util/Map;

    .line 226
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0d0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->t:[Ljava/lang/CharSequence;

    .line 227
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->u:[Ljava/lang/CharSequence;

    .line 228
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->v:[Ljava/lang/CharSequence;

    .line 230
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->t:[Ljava/lang/CharSequence;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 231
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->r:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->v:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->u:[Ljava/lang/CharSequence;

    aget-object v3, v3, v0

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->s:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->v:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentRegist;->t:[Ljava/lang/CharSequence;

    aget-object v3, v3, v0

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 207
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "ACS"

    invoke-direct {p0, v1, v2}, Lcom/sec/chaton/registration/FragmentRegist;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 236
    :cond_5
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f070403

    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 241
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 243
    if-eqz v0, :cond_0

    .line 245
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->X:Z

    .line 248
    :cond_0
    const-string v0, "onCreateView..."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const v0, 0x7f0300e4

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 252
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->j:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->N:Lcom/sec/chaton/d/h;

    .line 254
    const v0, 0x7f0700f0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->y:Landroid/widget/LinearLayout;

    .line 257
    const v0, 0x7f07014c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    .line 258
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    const v2, 0x7f02029a

    invoke-virtual {v0, v5, v5, v2, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->B:Landroid/widget/TextView;

    const v2, 0x7f0b02f5

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setHint(I)V

    .line 263
    const v0, 0x7f070400

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 265
    const v0, 0x7f070401

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0, v6}, Lcom/sec/chaton/widget/ClearableEditText;->setInputType(I)V

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    const v2, 0x10000005

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 280
    const v0, 0x7f0703fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->D:Landroid/widget/TextView;

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->D:Landroid/widget/TextView;

    const v2, 0x7f0b03a2

    new-array v3, v8, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/chaton/registration/FragmentRegist;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    const v0, 0x7f0703fd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->E:Landroid/widget/TextView;

    .line 287
    const v0, 0x7f070402

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->F:Landroid/widget/TextView;

    .line 288
    const v0, 0x7f0703fb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->G:Landroid/view/View;

    .line 289
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->E:Landroid/widget/TextView;

    const v2, 0x7f0b03a5

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {}, Lcom/sec/chaton/util/am;->q()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/common/util/j;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/chaton/registration/FragmentRegist;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->F:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->X:Z

    if-nez v0, :cond_3

    .line 293
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->G:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->F:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 298
    :cond_3
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 299
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->C:Landroid/widget/Button;

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->C:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 344
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->y:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentRegist;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 346
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 349
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 408
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 410
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 411
    const-string v0, "onDestroyView"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->I:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 417
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->J:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->J:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->J:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->P:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 425
    :cond_3
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 986
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 987
    const-string v0, "onResume"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "AR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "MX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    const-string v1, "BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->z:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 993
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->C:Landroid/widget/Button;

    if-eqz v0, :cond_2

    .line 994
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->C:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1031
    :cond_2
    :goto_0
    return-void

    .line 992
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->H:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1011
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->C:Landroid/widget/Button;

    if-eqz v0, :cond_2

    .line 1012
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentRegist;->C:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 970
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 971
    const-string v0, "onStop..."

    const-string v1, "ActivityRegist"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 973
    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentRegist;->A:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 974
    return-void
.end method
