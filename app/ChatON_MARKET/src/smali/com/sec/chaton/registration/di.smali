.class Lcom/sec/chaton/registration/di;
.super Landroid/os/Handler;
.source "FragmentRegist.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentRegist;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentRegist;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1663
    iput-object p1, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 1666
    iget-object v0, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1707
    :cond_0
    :goto_0
    return-void

    .line 1669
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1670
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x450

    if-ne v1, v2, :cond_0

    .line 1671
    iget-object v1, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1672
    iget-object v1, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->j(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1674
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1675
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 1676
    const-string v0, "GET_VERSION_NOTICE success"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->o(Lcom/sec/chaton/registration/FragmentRegist;)V

    goto :goto_0

    .line 1680
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1681
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    .line 1682
    iget-object v1, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v2, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b016c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b0024

    new-instance v3, Lcom/sec/chaton/registration/dk;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/dk;-><init>(Lcom/sec/chaton/registration/di;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/registration/dj;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/dj;-><init>(Lcom/sec/chaton/registration/di;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1700
    iget-object v0, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1701
    iget-object v0, p0, Lcom/sec/chaton/registration/di;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1704
    :cond_5
    const-string v0, "GET_VERSION_NOTICE failed becuase netwrok error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
