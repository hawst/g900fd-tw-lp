.class public Lcom/sec/chaton/registration/FragmentDisclaimer;
.super Landroid/support/v4/app/Fragment;
.source "FragmentDisclaimer.java"


# instance fields
.field a:Landroid/os/Handler;

.field private b:Landroid/app/ProgressDialog;

.field private c:Ljava/lang/String;

.field private d:Landroid/content/Context;

.field private e:Landroid/support/v4/app/FragmentActivity;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->b:Landroid/app/ProgressDialog;

    .line 204
    new-instance v0, Lcom/sec/chaton/registration/ba;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/registration/ba;-><init>(Lcom/sec/chaton/registration/FragmentDisclaimer;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentDisclaimer;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->b:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentDisclaimer;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->e:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 306
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->e:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 310
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 312
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 314
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    invoke-virtual {p0, v1}, Lcom/sec/chaton/registration/FragmentDisclaimer;->startActivity(Landroid/content/Intent;)V

    .line 322
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentDisclaimer;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/registration/FragmentDisclaimer;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/registration/FragmentDisclaimer;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->b:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 49
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->e:Landroid/support/v4/app/FragmentActivity;

    .line 50
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 74
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 55
    const v0, 0x7f0300ef

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->f:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->e:Landroid/support/v4/app/FragmentActivity;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->d:Landroid/content/Context;

    .line 60
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->e:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "disclaimerUID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->c:Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->f:Landroid/view/View;

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->e:Landroid/support/v4/app/FragmentActivity;

    .line 69
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 298
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 300
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentDisclaimer;->a()V

    .line 302
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 83
    const-string v0, "onStart : showDisclaimer"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->f:Landroid/view/View;

    const v1, 0x7f070428

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 96
    new-instance v1, Lcom/sec/chaton/registration/ay;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/ay;-><init>(Lcom/sec/chaton/registration/FragmentDisclaimer;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentDisclaimer;->f:Landroid/view/View;

    const v1, 0x7f070427

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 185
    new-instance v1, Lcom/sec/chaton/registration/az;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/az;-><init>(Lcom/sec/chaton/registration/FragmentDisclaimer;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    return-void
.end method
