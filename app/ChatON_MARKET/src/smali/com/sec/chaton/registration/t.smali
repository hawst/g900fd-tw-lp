.class Lcom/sec/chaton/registration/t;
.super Landroid/os/Handler;
.source "ActivitySyncSignInPopup.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 566
    iget-object v0, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->e(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 622
    :goto_0
    return-void

    .line 569
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 570
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 589
    :sswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2

    .line 592
    iget-object v0, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->u(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    goto :goto_0

    .line 573
    :sswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_1

    .line 574
    iget-object v0, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)V

    .line 575
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 576
    iget-object v0, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 577
    iget-object v0, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto :goto_0

    .line 580
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    sget-object v2, Lcom/sec/chaton/registration/ab;->g:Lcom/sec/chaton/registration/ab;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Lcom/sec/chaton/registration/ab;Ljava/lang/String;)V

    goto :goto_0

    .line 598
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    sget-object v2, Lcom/sec/chaton/registration/ab;->a:Lcom/sec/chaton/registration/ab;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Lcom/sec/chaton/registration/ab;Ljava/lang/String;)V

    goto :goto_0

    .line 604
    :sswitch_2
    iget-object v1, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 605
    iget-object v1, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->q(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->dismiss()V

    .line 606
    iget-object v1, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1, v3}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Landroid/view/View;)Landroid/view/View;

    .line 609
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_4

    .line 612
    iget-object v0, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->u(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V

    .line 614
    invoke-static {v3}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;)V

    goto/16 :goto_0

    .line 617
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/registration/t;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    sget-object v2, Lcom/sec/chaton/registration/ab;->b:Lcom/sec/chaton/registration/ab;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Lcom/sec/chaton/registration/ab;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 570
    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_0
        0x131 -> :sswitch_1
        0x8fd -> :sswitch_2
    .end sparse-switch
.end method
