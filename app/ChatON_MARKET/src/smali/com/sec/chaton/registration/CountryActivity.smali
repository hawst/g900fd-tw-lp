.class public Lcom/sec/chaton/registration/CountryActivity;
.super Lcom/sec/common/actionbar/ActionBarListActivity;
.source "CountryActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/chaton/util/cn;


# instance fields
.field a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/sec/chaton/widget/ClearableEditText;

.field private d:Ljava/lang/String;

.field private e:[Ljava/lang/CharSequence;

.field private f:[Ljava/lang/CharSequence;

.field private g:Landroid/content/Context;

.field private h:[Ljava/lang/CharSequence;

.field private i:Lcom/sec/chaton/registration/ao;

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:I

.field private l:Landroid/view/View;

.field private m:I

.field private n:Lcom/sec/widget/FastScrollableListView;

.field private o:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lcom/sec/common/actionbar/ActionBarListActivity;-><init>()V

    .line 50
    iput-object v1, p0, Lcom/sec/chaton/registration/CountryActivity;->d:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->a:Ljava/util/Map;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->b:Ljava/util/Map;

    .line 59
    iput-object v1, p0, Lcom/sec/chaton/registration/CountryActivity;->i:Lcom/sec/chaton/registration/ao;

    .line 60
    iput-object v1, p0, Lcom/sec/chaton/registration/CountryActivity;->j:Ljava/util/ArrayList;

    .line 61
    iput v2, p0, Lcom/sec/chaton/registration/CountryActivity;->k:I

    .line 78
    iput v2, p0, Lcom/sec/chaton/registration/CountryActivity;->m:I

    .line 85
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->o:Ljava/lang/Boolean;

    .line 268
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/CountryActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/CountryActivity;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/chaton/registration/CountryActivity;->m:I

    return v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 553
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 556
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 557
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 558
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 560
    invoke-virtual {p0, v1}, Lcom/sec/chaton/registration/CountryActivity;->startActivity(Landroid/content/Intent;)V

    .line 562
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/registration/CountryActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/registration/CountryActivity;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/registration/CountryActivity;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->h:[Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->c:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 427
    iget-object v1, p0, Lcom/sec/chaton/registration/CountryActivity;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 429
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/registration/CountryActivity;->j:Ljava/util/ArrayList;

    .line 432
    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_0

    .line 434
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    move v1, v2

    .line 438
    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 439
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    .line 440
    iget-object v4, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    aget-object v4, v4, v1

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-gt v3, v4, :cond_2

    .line 441
    iget-object v3, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/sec/chaton/util/as;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/chaton/registration/CountryActivity;->h:[Ljava/lang/CharSequence;

    aget-object v3, v3, v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    const-string v3, "+"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 444
    :cond_1
    iget-object v3, p0, Lcom/sec/chaton/registration/CountryActivity;->a:Ljava/util/Map;

    iget-object v4, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    aget-object v4, v4, v1

    iget-object v5, p0, Lcom/sec/chaton/registration/CountryActivity;->h:[Ljava/lang/CharSequence;

    aget-object v5, v5, v1

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    iget-object v3, p0, Lcom/sec/chaton/registration/CountryActivity;->j:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 438
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 449
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/registration/CountryActivity;->i:Lcom/sec/chaton/registration/ao;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/registration/ao;->a(Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->i:Lcom/sec/chaton/registration/ao;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/ao;->notifyDataSetChanged()V

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->c:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 454
    const-string v0, "YES"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 456
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 462
    :goto_1
    return-void

    .line 458
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->l:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 466
    return-void
.end method

.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 591
    const v0, 0x7f0c00f7

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 583
    const v0, 0x7f0c00f5

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 251
    invoke-super {p0, p1, p2, p3}, Lcom/sec/common/actionbar/ActionBarListActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 252
    if-eq p2, v3, :cond_0

    .line 266
    :goto_0
    return-void

    .line 255
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 257
    :pswitch_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 258
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 259
    const-string v1, "PARAMS_COUNTRY_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 261
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "PARAMS_COUNTRY_CODE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 262
    invoke-virtual {p0, v3, v0}, Lcom/sec/chaton/registration/CountryActivity;->setResult(ILandroid/content/Intent;)V

    .line 263
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->finish()V

    goto :goto_0

    .line 255
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 368
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 404
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 221
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 222
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ADD_BUDDY_TYPE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 223
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 224
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 226
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 89
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 90
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/CountryActivity;->setContentView(I)V

    .line 92
    iput-object p0, p0, Lcom/sec/chaton/registration/CountryActivity;->g:Landroid/content/Context;

    .line 95
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "needBackKey"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->o:Ljava/lang/Boolean;

    .line 98
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 99
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->a()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Z)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->a()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->d(Z)V

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->o:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->a()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/common/actionbar/a;->a(Z)V

    .line 103
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->a()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/common/actionbar/a;->d(Z)V

    .line 129
    :cond_0
    :goto_0
    const v0, 0x7f07014c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/CountryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->c:Lcom/sec/chaton/widget/ClearableEditText;

    .line 136
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->c:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->c:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b02f5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->c:Lcom/sec/chaton/widget/ClearableEditText;

    const v2, 0x10000006

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setImeOptions(I)V

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->c:Lcom/sec/chaton/widget/ClearableEditText;

    const/16 v2, 0x4000

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setInputType(I)V

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->c:Lcom/sec/chaton/widget/ClearableEditText;

    const/16 v2, 0x1e

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setMaxLength(I)V

    .line 144
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f0d0000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    .line 145
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0002

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->f:[Ljava/lang/CharSequence;

    .line 147
    const v0, 0x7f07002b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/CountryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->l:Landroid/view/View;

    .line 158
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->h:[Ljava/lang/CharSequence;

    .line 161
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "extra_hide_country_calling_code"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 163
    if-eqz v2, :cond_4

    .line 164
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 165
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 166
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 168
    :goto_1
    iget-object v6, p0, Lcom/sec/chaton/registration/CountryActivity;->h:[Ljava/lang/CharSequence;

    array-length v6, v6

    if-ge v0, v6, :cond_3

    .line 169
    iget-object v6, p0, Lcom/sec/chaton/registration/CountryActivity;->h:[Ljava/lang/CharSequence;

    aget-object v6, v6, v0

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 170
    iget-object v6, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    aget-object v6, v6, v0

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v6, p0, Lcom/sec/chaton/registration/CountryActivity;->h:[Ljava/lang/CharSequence;

    aget-object v6, v6, v0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v6, p0, Lcom/sec/chaton/registration/CountryActivity;->f:[Ljava/lang/CharSequence;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 106
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->a()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/common/actionbar/a;->a(Z)V

    .line 107
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->a()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/common/actionbar/a;->d(Z)V

    goto/16 :goto_0

    .line 175
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    .line 176
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->h:[Ljava/lang/CharSequence;

    .line 177
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->f:[Ljava/lang/CharSequence;

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->h:[Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->f:[Ljava/lang/CharSequence;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    :cond_4
    move v0, v1

    .line 184
    :goto_2
    iget-object v2, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 185
    iget-object v2, p0, Lcom/sec/chaton/registration/CountryActivity;->a:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/sec/chaton/registration/CountryActivity;->h:[Ljava/lang/CharSequence;

    aget-object v4, v4, v0

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v2, p0, Lcom/sec/chaton/registration/CountryActivity;->b:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/chaton/registration/CountryActivity;->e:[Ljava/lang/CharSequence;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/sec/chaton/registration/CountryActivity;->f:[Ljava/lang/CharSequence;

    aget-object v4, v4, v0

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 191
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/FastScrollableListView;

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->n:Lcom/sec/widget/FastScrollableListView;

    .line 192
    new-instance v0, Lcom/sec/chaton/registration/ao;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ao;-><init>(Lcom/sec/chaton/registration/CountryActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->i:Lcom/sec/chaton/registration/ao;

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->i:Lcom/sec/chaton/registration/ao;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/CountryActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->n:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableListView;->setItemsCanFocus(Z)V

    .line 196
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->n:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v0, p0}, Lcom/sec/widget/FastScrollableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->n:Lcom/sec/widget/FastScrollableListView;

    invoke-virtual {v0, v7}, Lcom/sec/widget/FastScrollableListView;->setFastScrollEnabled(Z)V

    .line 210
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 212
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "mode"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/registration/CountryActivity;->m:I

    .line 215
    :cond_6
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 408
    const v0, 0x7f07014c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/CountryActivity;->d:Ljava/lang/String;

    .line 412
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "selectedCountry ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/CountryActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PARAMS_COUNTRY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/registration/CountryActivity;->b:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/chaton/registration/CountryActivity;->d:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 414
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/registration/CountryActivity;->setResult(ILandroid/content/Intent;)V

    .line 415
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->finish()V

    .line 417
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/registration/ao;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/ao;->notifyDataSetChanged()V

    .line 418
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 567
    invoke-super {p0}, Lcom/sec/common/actionbar/ActionBarListActivity;->onResume()V

    .line 570
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ADD_BUDDY_TYPE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 571
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 572
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 575
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/registration/CountryActivity;->b()V

    .line 576
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 232
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 233
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/CountryActivity;->setResult(I)V

    .line 234
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->finish()V

    .line 235
    const/4 v0, 0x1

    .line 237
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarListActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 481
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    .line 482
    const v0, 0x7f0b0031

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 493
    :cond_0
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 498
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 502
    invoke-virtual {p0}, Lcom/sec/chaton/registration/CountryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 538
    :goto_0
    return-void

    .line 504
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
