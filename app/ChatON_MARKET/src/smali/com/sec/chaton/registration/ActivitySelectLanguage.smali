.class public Lcom/sec/chaton/registration/ActivitySelectLanguage;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivitySelectLanguage.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySelectLanguage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    sget-object v2, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 22
    :cond_0
    if-eqz v0, :cond_1

    .line 23
    const v0, 0x7f0b0005

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivitySelectLanguage;->setTitle(I)V

    .line 26
    :cond_1
    new-instance v0, Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-direct {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;-><init>()V

    return-object v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 31
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivitySelectLanguage;->finish()V

    .line 33
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
