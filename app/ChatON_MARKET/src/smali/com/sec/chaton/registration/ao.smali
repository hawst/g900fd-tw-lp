.class Lcom/sec/chaton/registration/ao;
.super Landroid/widget/BaseAdapter;
.source "CountryActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/CountryActivity;

.field private b:Landroid/view/LayoutInflater;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/registration/CountryActivity;)V
    .locals 1

    .prologue
    .line 274
    iput-object p1, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 272
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/ao;->e:Ljava/lang/String;

    .line 275
    invoke-static {p1}, Lcom/sec/chaton/registration/CountryActivity;->a(Lcom/sec/chaton/registration/CountryActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ao;->b:Landroid/view/LayoutInflater;

    .line 276
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/sec/chaton/registration/ao;->e:Ljava/lang/String;

    .line 296
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    iget-object v0, v0, Lcom/sec/chaton/registration/CountryActivity;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    iget-object v0, v0, Lcom/sec/chaton/registration/CountryActivity;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 290
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 302
    if-nez p2, :cond_0

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030121

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 308
    :cond_0
    const v0, 0x7f07014c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/ao;->c:Landroid/widget/TextView;

    .line 309
    const v0, 0x7f07014d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/ao;->d:Landroid/widget/TextView;

    .line 312
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/CountryActivity;->b(Lcom/sec/chaton/registration/CountryActivity;)I

    move-result v0

    if-nez v0, :cond_1

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->c:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/CountryActivity;->c(Lcom/sec/chaton/registration/CountryActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/CountryActivity;->c(Lcom/sec/chaton/registration/CountryActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 322
    iget-object v1, p0, Lcom/sec/chaton/registration/ao;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/CountryActivity;->c(Lcom/sec/chaton/registration/CountryActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " (+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    iget-object v2, v2, Lcom/sec/chaton/registration/CountryActivity;->a:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    invoke-static {v3}, Lcom/sec/chaton/registration/CountryActivity;->c(Lcom/sec/chaton/registration/CountryActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ao;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 343
    if-ltz v0, :cond_3

    .line 344
    iget-object v1, p0, Lcom/sec/chaton/registration/ao;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    .line 346
    new-instance v2, Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lcom/sec/chaton/registration/ao;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 347
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08001a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    :cond_3
    return-object p2

    .line 327
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/CountryActivity;->d(Lcom/sec/chaton/registration/CountryActivity;)[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_5

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    invoke-static {v1}, Lcom/sec/chaton/registration/CountryActivity;->d(Lcom/sec/chaton/registration/CountryActivity;)[Ljava/lang/CharSequence;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/CountryActivity;->e(Lcom/sec/chaton/registration/CountryActivity;)[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_2

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/registration/ao;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " (+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/ao;->a:Lcom/sec/chaton/registration/CountryActivity;

    invoke-static {v2}, Lcom/sec/chaton/registration/CountryActivity;->e(Lcom/sec/chaton/registration/CountryActivity;)[Ljava/lang/CharSequence;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
