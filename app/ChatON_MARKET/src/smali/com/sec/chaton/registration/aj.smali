.class Lcom/sec/chaton/registration/aj;
.super Landroid/os/Handler;
.source "AuthenticatorActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/AuthenticatorActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V
    .locals 0

    .prologue
    .line 1390
    iput-object p1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1393
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1394
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 1491
    :cond_0
    :goto_0
    return-void

    .line 1399
    :sswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_1

    .line 1407
    iget-object v0, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->h(Lcom/sec/chaton/registration/AuthenticatorActivity;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    goto :goto_0

    .line 1410
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    sget-object v2, Lcom/sec/chaton/registration/am;->a:Lcom/sec/chaton/registration/am;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Lcom/sec/chaton/registration/AuthenticatorActivity;Lcom/sec/chaton/registration/am;Ljava/lang/String;)V

    goto :goto_0

    .line 1425
    :sswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_6

    .line 1427
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 1428
    if-eqz v1, :cond_5

    .line 1431
    iget-object v0, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    iget-object v0, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "3.3.51"

    iget-object v2, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1434
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 1435
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new version : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v2}, Lcom/sec/chaton/registration/AuthenticatorActivity;->i(Lcom/sec/chaton/registration/AuthenticatorActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1437
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->j(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    .line 1439
    iget-object v0, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->k(Lcom/sec/chaton/registration/AuthenticatorActivity;)Z

    move-result v0

    if-ne v0, v3, :cond_3

    iget-object v0, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->critical:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    iget-object v0, v1, Lcom/sec/chaton/io/entry/GetVersionNotice;->critical:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1440
    iget-object v0, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->l(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    goto/16 :goto_0

    .line 1442
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0, v3}, Lcom/sec/chaton/registration/AuthenticatorActivity;->b(Lcom/sec/chaton/registration/AuthenticatorActivity;Z)Z

    .line 1443
    iget-object v0, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->b(Lcom/sec/chaton/registration/AuthenticatorActivity;)Landroid/content/Context;

    move-result-object v0

    const-string v2, "YES"

    invoke-static {v1, v0, v4, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    goto/16 :goto_0

    .line 1446
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->l(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    goto/16 :goto_0

    .line 1449
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    sget-object v2, Lcom/sec/chaton/registration/am;->c:Lcom/sec/chaton/registration/am;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Lcom/sec/chaton/registration/AuthenticatorActivity;Lcom/sec/chaton/registration/am;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1452
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    sget-object v2, Lcom/sec/chaton/registration/am;->c:Lcom/sec/chaton/registration/am;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Lcom/sec/chaton/registration/AuthenticatorActivity;Lcom/sec/chaton/registration/am;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1460
    :sswitch_2
    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->m(Lcom/sec/chaton/registration/AuthenticatorActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->m(Lcom/sec/chaton/registration/AuthenticatorActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1461
    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->m(Lcom/sec/chaton/registration/AuthenticatorActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1464
    :cond_7
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_9

    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xa411

    if-ne v1, v2, :cond_c

    .line 1465
    :cond_9
    const-string v0, "first_disclaimer_status"

    const-string v1, "DONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1467
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    iget-object v0, v0, Lcom/sec/chaton/registration/AuthenticatorActivity;->d:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_a

    .line 1468
    iget-object v0, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    iget-object v0, v0, Lcom/sec/chaton/registration/AuthenticatorActivity;->d:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1473
    :cond_a
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_disclaimer_status"

    const-string v2, "DONE"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1479
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "accept_disclaimer_iso"

    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1481
    :cond_b
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    const-class v2, Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1482
    const-string v1, "is_before_regi"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1484
    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1470
    :catch_0
    move-exception v0

    .line 1471
    const-string v0, "the task is already running or finished"

    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->i(Lcom/sec/chaton/registration/AuthenticatorActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1486
    :cond_c
    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->b(Lcom/sec/chaton/registration/AuthenticatorActivity;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1487
    iget-object v1, p0, Lcom/sec/chaton/registration/aj;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    sget-object v2, Lcom/sec/chaton/registration/am;->d:Lcom/sec/chaton/registration/am;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Lcom/sec/chaton/registration/AuthenticatorActivity;Lcom/sec/chaton/registration/am;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1394
    nop

    :sswitch_data_0
    .sparse-switch
        0x68 -> :sswitch_0
        0x450 -> :sswitch_1
        0x451 -> :sswitch_2
    .end sparse-switch
.end method
