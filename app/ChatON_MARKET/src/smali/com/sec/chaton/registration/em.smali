.class Lcom/sec/chaton/registration/em;
.super Landroid/os/Handler;
.source "FragmentSelectLanguage.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentSelectLanguage;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentSelectLanguage;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const v7, 0x7f0b021e

    const v6, 0x7f0b002a

    const/16 v5, 0x7919

    const v4, 0x7f0b016c

    const v3, 0x7f0b0042

    .line 280
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 281
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x517

    if-ne v1, v2, :cond_3

    .line 282
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_1

    .line 283
    iget-object v2, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    iput-object v1, v2, Lcom/sec/chaton/registration/FragmentSelectLanguage;->b:Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    .line 284
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentSelectLanguage;->b:Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    if-eqz v1, :cond_2

    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentSelectLanguage;->b:Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/GetSMSAuthToken;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentSelectLanguage;->b:Lcom/sec/chaton/io/entry/GetSMSAuthToken;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/GetSMSAuthToken;->token:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->a(Lcom/sec/chaton/registration/FragmentSelectLanguage;Ljava/lang/String;)Ljava/lang/String;

    .line 287
    const-string v0, "acstoken"

    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->a(Lcom/sec/chaton/registration/FragmentSelectLanguage;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "ACSSaveCountInterval"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->b(Lcom/sec/chaton/registration/FragmentSelectLanguage;)V

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    if-ne v1, v5, :cond_4

    .line 295
    const-string v1, "server sent error cause by GET_ACS_AUTHTOKEN_INVALID"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/en;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/en;-><init>(Lcom/sec/chaton/registration/em;)V

    invoke-virtual {v1, v3, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 305
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 307
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->c(Lcom/sec/chaton/registration/FragmentSelectLanguage;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 356
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 358
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->c(Lcom/sec/chaton/registration/FragmentSelectLanguage;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 363
    :cond_3
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x518

    if-ne v1, v2, :cond_0

    .line 364
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "entry.getFaultCode()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_7

    .line 366
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->c(Lcom/sec/chaton/registration/FragmentSelectLanguage;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 370
    const-string v0, "over_acs_request"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 371
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 403
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->c(Lcom/sec/chaton/registration/FragmentSelectLanguage;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 312
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x791b

    if-ne v1, v2, :cond_5

    .line 313
    const-string v1, "server sent error cause by GET_SMS_AUTHTOKEN_GENERATED_RIGHT_BEFORE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Landroid/app/Activity;)V

    .line 317
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 319
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->c(Lcom/sec/chaton/registration/FragmentSelectLanguage;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_1

    .line 323
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    if-ne v1, v5, :cond_6

    .line 324
    const-string v1, "server sent error cause by GET_ACS_AUTHTOKEN_INVALID"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 331
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/eo;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/eo;-><init>(Lcom/sec/chaton/registration/em;)V

    invoke-virtual {v1, v3, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 338
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 340
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->c(Lcom/sec/chaton/registration/FragmentSelectLanguage;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_1

    .line 344
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 347
    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/ep;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ep;-><init>(Lcom/sec/chaton/registration/em;)V

    invoke-virtual {v1, v3, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 354
    const-string v1, "Token for ACS failed becuase netwrok error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 375
    :cond_7
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xa02c

    if-ne v1, v2, :cond_8

    .line 376
    const-string v0, "server sent error cause by REQUEST_ACS_REQUEST_LIMIT"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 385
    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0xa02a

    if-ne v0, v1, :cond_9

    .line 386
    const-string v0, "You have reached your daily ACS limit"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->d(Lcom/sec/chaton/registration/FragmentSelectLanguage;)V

    .line 388
    const-string v0, "over_acs_request"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_2

    .line 391
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/em;->a:Lcom/sec/chaton/registration/FragmentSelectLanguage;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentSelectLanguage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/registration/eq;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/eq;-><init>(Lcom/sec/chaton/registration/em;)V

    invoke-virtual {v0, v3, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 401
    const-string v0, "Requesting ACS failed becuase netwrok error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
