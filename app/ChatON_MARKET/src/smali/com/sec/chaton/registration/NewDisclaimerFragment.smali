.class public Lcom/sec/chaton/registration/NewDisclaimerFragment;
.super Landroid/support/v4/app/Fragment;
.source "NewDisclaimerFragment.java"


# instance fields
.field public a:Landroid/os/Handler;

.field b:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/CheckBox;

.field private e:Landroid/widget/CheckBox;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/LinearLayout;

.field private h:Landroid/widget/ImageButton;

.field private i:Landroid/widget/ImageButton;

.field private j:Landroid/widget/ImageButton;

.field private k:Landroid/app/ProgressDialog;

.field private l:Lcom/sec/chaton/d/bj;

.field private m:Lcom/sec/chaton/d/l;

.field private n:Landroid/support/v4/app/FragmentActivity;

.field private o:Lcom/sec/chaton/io/entry/GetVersionNotice;

.field private p:Ljava/lang/String;

.field private q:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->k:Landroid/app/ProgressDialog;

    .line 78
    const-class v0, Lcom/sec/chaton/registration/NewDisclaimerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->p:Ljava/lang/String;

    .line 275
    new-instance v0, Lcom/sec/chaton/registration/ge;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ge;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a:Landroid/os/Handler;

    .line 511
    new-instance v0, Lcom/sec/chaton/registration/fw;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/fw;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->q:Landroid/content/BroadcastReceiver;

    .line 529
    new-instance v0, Lcom/sec/chaton/registration/fx;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/fx;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->b:Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->n:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/NewDisclaimerFragment;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->o:Lcom/sec/chaton/io/entry/GetVersionNotice;

    return-object p1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->c:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 248
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->c:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/NewDisclaimerFragment;Lcom/sec/chaton/registration/gi;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a(Lcom/sec/chaton/registration/gi;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/registration/gi;Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0b016c

    .line 436
    invoke-direct {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->c()V

    .line 438
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 439
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "network error, type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/registration/gi;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " errorCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->p:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 478
    :goto_0
    return-void

    .line 446
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0024

    new-instance v2, Lcom/sec/chaton/registration/gh;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/registration/gh;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;Lcom/sec/chaton/registration/gi;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/registration/gg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/gg;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->d:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 482
    invoke-virtual {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->k:Landroid/app/ProgressDialog;

    .line 484
    :cond_0
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 491
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->e:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->b()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Lcom/sec/chaton/d/l;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->m:Lcom/sec/chaton/d/l;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Lcom/sec/chaton/d/bj;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->l:Lcom/sec/chaton/d/bj;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->c()V

    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->k:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/registration/NewDisclaimerFragment;)Lcom/sec/chaton/io/entry/GetVersionNotice;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->o:Lcom/sec/chaton/io/entry/GetVersionNotice;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 85
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    iput-object p1, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->n:Landroid/support/v4/app/FragmentActivity;

    .line 86
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 238
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 240
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 91
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 92
    const v0, 0x7f030137

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 96
    const v0, 0x7f0704f6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->c:Landroid/widget/Button;

    .line 97
    const v0, 0x7f0704f1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->d:Landroid/widget/CheckBox;

    .line 98
    const v0, 0x7f0704f0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->f:Landroid/widget/LinearLayout;

    .line 99
    const v0, 0x7f0704f4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->e:Landroid/widget/CheckBox;

    .line 100
    const v0, 0x7f0704f3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->g:Landroid/widget/LinearLayout;

    .line 101
    const v0, 0x7f0704ef

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->h:Landroid/widget/ImageButton;

    .line 102
    const v0, 0x7f0704f2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->i:Landroid/widget/ImageButton;

    .line 103
    const v0, 0x7f0704f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->j:Landroid/widget/ImageButton;

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->l:Lcom/sec/chaton/d/bj;

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/l;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->m:Lcom/sec/chaton/d/l;

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->h:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/chaton/registration/fv;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fv;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->i:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/chaton/registration/fz;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fz;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->j:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/chaton/registration/ga;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ga;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->f:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/chaton/registration/gb;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/gb;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->g:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/chaton/registration/gc;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/gc;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 206
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->c:Landroid/widget/Button;

    new-instance v2, Lcom/sec/chaton/registration/gd;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/gd;-><init>(Lcom/sec/chaton/registration/NewDisclaimerFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 223
    const-string v2, "upgrade_cancel"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 226
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 264
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 265
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 266
    const-string v0, "onDestroy "

    iget-object v1, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->p:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->c()V

    .line 269
    invoke-virtual {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 270
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 232
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 233
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->n:Landroid/support/v4/app/FragmentActivity;

    .line 234
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 252
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 253
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 254
    const-string v0, "onResume "

    iget-object v1, p0, Lcom/sec/chaton/registration/NewDisclaimerFragment;->p:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/registration/NewDisclaimerFragment;->a()V

    .line 258
    return-void
.end method
