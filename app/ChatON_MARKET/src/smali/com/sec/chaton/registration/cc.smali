.class Lcom/sec/chaton/registration/cc;
.super Ljava/lang/Object;
.source "FragmentNonSelfSMS.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 1531
    iput-object p1, p0, Lcom/sec/chaton/registration/cc;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1537
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1543
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1558
    iget-object v0, p0, Lcom/sec/chaton/registration/cc;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1561
    iget-object v0, p0, Lcom/sec/chaton/registration/cc;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->k(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1570
    :goto_0
    return-void

    .line 1565
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/cc;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->k(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
