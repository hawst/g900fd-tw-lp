.class Lcom/sec/chaton/registration/ci;
.super Ljava/lang/Object;
.source "FragmentNonSelfSMS.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/sec/chaton/registration/ci;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 426
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn_temp"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 427
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "phonenumber"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 430
    const-string v0, "for_wifi_only_device"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 431
    const-string v0, "contact_sim_sync"

    iget-object v1, p0, Lcom/sec/chaton/registration/ci;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->c(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 434
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/ci;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->d(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/registration/ci;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v1, p0, Lcom/sec/chaton/registration/ci;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/registration/ci;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b022a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/registration/ci;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->e(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Lcom/sec/chaton/d/h;

    move-result-object v0

    const-string v1, "append"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;)V

    .line 447
    :cond_1
    :goto_0
    return-void

    .line 439
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/ci;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 441
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 442
    iget-object v1, p0, Lcom/sec/chaton/registration/ci;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 443
    iget-object v0, p0, Lcom/sec/chaton/registration/ci;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method
