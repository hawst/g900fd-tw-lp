.class public Lcom/sec/chaton/registration/FragmentConnectAccounts;
.super Landroid/support/v4/app/Fragment;
.source "FragmentConnectAccounts.java"


# instance fields
.field a:Landroid/view/View$OnClickListener;

.field b:Lcom/sec/chaton/d/bb;

.field private c:Ljava/lang/String;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/Button;

.field private h:Landroid/support/v4/app/FragmentActivity;

.field private i:Landroid/widget/Button;

.field private j:Lcom/sec/chaton/d/au;

.field private k:Landroid/app/ProgressDialog;

.field private l:Lcom/sec/common/a/d;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 31
    const-class v0, Lcom/sec/chaton/registration/ActivityConnectAccounts;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->c:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->k:Landroid/app/ProgressDialog;

    .line 44
    iput-object v1, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->l:Lcom/sec/common/a/d;

    .line 121
    new-instance v0, Lcom/sec/chaton/registration/ar;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/ar;-><init>(Lcom/sec/chaton/registration/FragmentConnectAccounts;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a:Landroid/view/View$OnClickListener;

    .line 159
    new-instance v0, Lcom/sec/chaton/registration/as;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/as;-><init>(Lcom/sec/chaton/registration/FragmentConnectAccounts;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->b:Lcom/sec/chaton/d/bb;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->h:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentConnectAccounts;Lcom/sec/chaton/d/au;)Lcom/sec/chaton/d/au;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->j:Lcom/sec/chaton/d/au;

    return-object p1
.end method

.method private a()V
    .locals 4

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->b()V

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->h:Landroid/support/v4/app/FragmentActivity;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->k:Landroid/app/ProgressDialog;

    .line 216
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 222
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentConnectAccounts;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a()V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/registration/FragmentConnectAccounts;)Lcom/sec/chaton/d/au;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->j:Lcom/sec/chaton/d/au;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->d()V

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->h:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0291

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0024

    new-instance v2, Lcom/sec/chaton/registration/ax;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ax;-><init>(Lcom/sec/chaton/registration/FragmentConnectAccounts;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/registration/aw;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/aw;-><init>(Lcom/sec/chaton/registration/FragmentConnectAccounts;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->l:Lcom/sec/common/a/d;

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->l:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 240
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->l:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->l:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->l:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 247
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/registration/FragmentConnectAccounts;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->b()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/registration/FragmentConnectAccounts;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->c()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 205
    packed-switch p1, :pswitch_data_0

    .line 210
    :goto_0
    return-void

    .line 207
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->j:Lcom/sec/chaton/d/au;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/d/au;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0x7f99
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 50
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->h:Landroid/support/v4/app/FragmentActivity;

    .line 51
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 119
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 56
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 57
    const v0, 0x7f030096

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 59
    const-string v0, "[LIFE] onCreate"

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const v0, 0x7f0702eb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->d:Landroid/widget/Button;

    .line 62
    const v0, 0x7f0702ec

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->e:Landroid/widget/Button;

    .line 63
    const v0, 0x7f0702ed

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->f:Landroid/widget/Button;

    .line 64
    const v0, 0x7f0702ee

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->g:Landroid/widget/Button;

    .line 66
    const v0, 0x7f0702ef

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->i:Landroid/widget/Button;

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->d:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->e:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->f:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->g:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->i:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    invoke-static {}, Lcom/sec/chaton/c/a;->d()V

    .line 82
    invoke-static {}, Lcom/sec/chaton/c/a;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->d:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->e:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 87
    :cond_0
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 104
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 106
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 107
    const-string v0, "[LIFE] onDestroy"

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->d()V

    .line 110
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentConnectAccounts;->b()V

    .line 111
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 95
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 96
    const-string v0, "[LIFE] onDetach"

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentConnectAccounts;->h:Landroid/support/v4/app/FragmentActivity;

    .line 99
    return-void
.end method
