.class Lcom/sec/chaton/registration/cd;
.super Ljava/lang/Object;
.source "FragmentNonSelfSMS.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V
    .locals 0

    .prologue
    .line 1573
    iput-object p1, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1578
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1677
    :goto_0
    :pswitch_0
    return-void

    .line 1581
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1582
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Ljava/lang/String;)Ljava/lang/String;

    .line 1583
    const-string v0, "authnum"

    invoke-static {v0, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1584
    const-string v0, "isSMS"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1585
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/registration/ActivitySelectLanguage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1587
    iget-object v1, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->p(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Z

    move-result v1

    if-ne v1, v5, :cond_0

    .line 1588
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1591
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1, v0, v3}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1604
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->q(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1605
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->r(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1606
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->s(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1608
    const-string v0, "isSMS"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1611
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1612
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1614
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    iget-object v1, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v4, v2, v5}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->a(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 1627
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0, v4}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->b(Lcom/sec/chaton/registration/FragmentNonSelfSMS;Ljava/lang/String;)Ljava/lang/String;

    .line 1628
    const-string v0, "authnum"

    invoke-static {v0, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1633
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->t(Lcom/sec/chaton/registration/FragmentNonSelfSMS;)V

    goto/16 :goto_0

    .line 1652
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/TabActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1654
    iget-object v1, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->startActivity(Landroid/content/Intent;)V

    .line 1655
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 1660
    :pswitch_4
    const-string v0, "authnum"

    invoke-static {v0, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1662
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1663
    iget-object v1, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1667
    iget-object v0, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 1671
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/ActivityWebView;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1672
    const-string v1, "PARAM_MENU"

    const-string v2, "VOC"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1673
    const-string v1, "REGI_VOC"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1674
    iget-object v1, p0, Lcom/sec/chaton/registration/cd;->a:Lcom/sec/chaton/registration/FragmentNonSelfSMS;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/registration/FragmentNonSelfSMS;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1578
    nop

    :pswitch_data_0
    .packed-switch 0x7f07039f
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method
