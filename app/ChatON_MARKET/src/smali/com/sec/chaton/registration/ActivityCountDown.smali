.class public Lcom/sec/chaton/registration/ActivityCountDown;
.super Landroid/app/Activity;
.source "ActivityCountDown.java"


# instance fields
.field a:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 108
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityCountDown;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 111
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 113
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    invoke-virtual {p0, v1}, Lcom/sec/chaton/registration/ActivityCountDown;->startActivity(Landroid/content/Intent;)V

    .line 117
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 24
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 25
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v0, 0x7f030098

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivityCountDown;->setContentView(I)V

    .line 27
    const v0, 0x7f0702f0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivityCountDown;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivityCountDown;->a:Landroid/widget/TextView;

    .line 28
    new-instance v0, Lcom/sec/chaton/registration/g;

    const-wide/16 v2, 0x7530

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/registration/g;-><init>(Lcom/sec/chaton/registration/ActivityCountDown;JJ)V

    invoke-virtual {v0}, Lcom/sec/chaton/registration/g;->start()Landroid/os/CountDownTimer;

    .line 40
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 102
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 104
    invoke-direct {p0}, Lcom/sec/chaton/registration/ActivityCountDown;->a()V

    .line 105
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 45
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityCountDown;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 85
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
