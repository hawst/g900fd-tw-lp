.class Lcom/sec/chaton/registration/co;
.super Landroid/os/Handler;
.source "FragmentRegist.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentRegist;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentRegist;)V
    .locals 0

    .prologue
    .line 1755
    iput-object p1, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10

    .prologue
    const v9, 0x7f0b01bc

    const v8, 0x7f0b016c

    const v7, 0x7f0b0039

    const v5, 0x7f0b002a

    const v6, 0x7f0b0024

    .line 1758
    iget-object v0, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1839
    :cond_0
    :goto_0
    return-void

    .line 1761
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1762
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1764
    :pswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_3

    .line 1766
    iget-object v0, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1768
    iget-object v0, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->r(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1771
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1772
    iget-object v0, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 1773
    iget-object v0, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 1777
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1778
    iget-object v1, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->r(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1780
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1781
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v1

    .line 1782
    iget-object v2, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v3, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v5}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/cq;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/cq;-><init>(Lcom/sec/chaton/registration/co;)V

    invoke-virtual {v1, v6, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/cp;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/cp;-><init>(Lcom/sec/chaton/registration/co;)V

    invoke-virtual {v1, v7, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1799
    iget-object v1, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 1801
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dev]Get All buddy failed. httpStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", FaultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1805
    :pswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_6

    .line 1806
    iget-object v0, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->q(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto/16 :goto_0

    .line 1807
    :cond_6
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->d:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_7

    .line 1808
    iget-object v0, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->q(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto/16 :goto_0

    .line 1811
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1812
    iget-object v1, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->r(Lcom/sec/chaton/registration/FragmentRegist;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1814
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1815
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v1

    .line 1816
    iget-object v2, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    iget-object v3, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v3}, Lcom/sec/chaton/registration/FragmentRegist;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v4}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v5}, Lcom/sec/chaton/registration/FragmentRegist;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/cs;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/cs;-><init>(Lcom/sec/chaton/registration/co;)V

    invoke-virtual {v1, v6, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/registration/cr;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/cr;-><init>(Lcom/sec/chaton/registration/co;)V

    invoke-virtual {v1, v7, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1833
    iget-object v1, p0, Lcom/sec/chaton/registration/co;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v1}, Lcom/sec/chaton/registration/FragmentRegist;->f(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 1834
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dev]Upload Address failed. httpStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", FaultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1762
    nop

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
