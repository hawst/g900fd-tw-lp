.class Lcom/sec/chaton/registration/u;
.super Landroid/os/Handler;
.source "ActivitySyncSignInPopup.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V
    .locals 0

    .prologue
    .line 625
    iput-object p1, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const v3, 0x7f0b02a9

    .line 628
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->e(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 668
    :cond_0
    :goto_0
    return-void

    .line 631
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 632
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x450

    if-ne v1, v2, :cond_0

    .line 634
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_5

    .line 635
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 636
    const-string v0, "GET_VERSION_NOTICE success"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 641
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0384

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)V

    .line 642
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->k(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/coolots/sso/a/a;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 643
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->k(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/coolots/sso/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)V

    .line 645
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 646
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 647
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_0

    .line 650
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)V

    .line 651
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 652
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 653
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_0

    .line 656
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    iget-object v1, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Ljava/lang/String;)V

    .line 657
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 658
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 659
    iget-object v0, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->l(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_0

    .line 665
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/registration/u;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    sget-object v2, Lcom/sec/chaton/registration/ab;->c:Lcom/sec/chaton/registration/ab;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Lcom/sec/chaton/registration/ab;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
