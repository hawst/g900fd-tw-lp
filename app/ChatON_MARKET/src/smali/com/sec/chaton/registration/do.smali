.class Lcom/sec/chaton/registration/do;
.super Ljava/lang/Object;
.source "FragmentRegistPushName.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentRegistPushName;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentRegistPushName;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/sec/chaton/registration/do;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 362
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 368
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 373
    iget-object v0, p0, Lcom/sec/chaton/registration/do;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    .line 381
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/registration/do;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v2, v2, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 382
    iget-object v2, p0, Lcom/sec/chaton/registration/do;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v2, v2, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-eq v2, v3, :cond_2

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/registration/do;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/chaton/registration/do;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/do;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-ge v0, v4, :cond_1

    .line 409
    iget-object v0, p0, Lcom/sec/chaton/registration/do;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/registration/do;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 414
    :cond_1
    return-void

    .line 392
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/registration/do;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 381
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
