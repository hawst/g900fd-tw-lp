.class public Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;
.super Landroid/support/v4/app/Fragment;
.source "FragmentGlobalDisclaimer.java"


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field public a:Landroid/os/Handler;

.field b:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/app/ProgressDialog;

.field private d:Lcom/sec/chaton/d/l;

.field private e:Lcom/sec/chaton/d/bj;

.field private f:Lcom/sec/chaton/io/entry/GetVersionNotice;

.field private h:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c:Landroid/app/ProgressDialog;

    .line 210
    new-instance v0, Lcom/sec/chaton/registration/be;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/be;-><init>(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a:Landroid/os/Handler;

    .line 411
    new-instance v0, Lcom/sec/chaton/registration/bi;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/bi;-><init>(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->h:Landroid/content/BroadcastReceiver;

    .line 430
    new-instance v0, Lcom/sec/chaton/registration/bj;

    invoke-direct {v0, p0}, Lcom/sec/chaton/registration/bj;-><init>(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->b:Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Lcom/sec/chaton/d/l;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->d:Lcom/sec/chaton/d/l;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->f:Lcom/sec/chaton/io/entry/GetVersionNotice;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;Lcom/sec/chaton/registration/bl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a(Lcom/sec/chaton/registration/bl;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/registration/bl;Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0b016c

    .line 367
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c()V

    .line 369
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "network error, type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/registration/bl;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " errorCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 409
    :goto_0
    return-void

    .line 377
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0024

    new-instance v2, Lcom/sec/chaton/registration/bh;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/registration/bh;-><init>(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;Lcom/sec/chaton/registration/bl;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/registration/bg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/bg;-><init>(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Lcom/sec/chaton/d/bj;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->e:Lcom/sec/chaton/d/bj;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c:Landroid/app/ProgressDialog;

    .line 201
    :cond_0
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 207
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)Lcom/sec/chaton/io/entry/GetVersionNotice;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->f:Lcom/sec/chaton/io/entry/GetVersionNotice;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->b()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 73
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 167
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 169
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 170
    const-string v0, "onCreate"

    sget-object v1, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 80
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 81
    const v0, 0x7f0300ee

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/l;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->d:Lcom/sec/chaton/d/l;

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->a:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->e:Lcom/sec/chaton/d/bj;

    .line 120
    const v0, 0x7f070425

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 122
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    .line 123
    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 125
    new-instance v2, Lcom/sec/chaton/registration/bb;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/bb;-><init>(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    const v0, 0x7f070426

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 136
    new-instance v2, Lcom/sec/chaton/registration/bc;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/bc;-><init>(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    const v0, 0x7f070427

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 146
    new-instance v2, Lcom/sec/chaton/registration/bd;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/bd;-><init>(Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 158
    const-string v2, "upgrade_cancel"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 162
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 188
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 189
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 190
    const-string v0, "onDestroy "

    sget-object v1, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->c()V

    .line 193
    invoke-virtual {p0}, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 194
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 179
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "onResume"

    sget-object v1, Lcom/sec/chaton/registration/FragmentGlobalDisclaimer;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_0
    return-void
.end method
