.class Lcom/sec/chaton/registration/w;
.super Landroid/os/Handler;
.source "ActivitySyncSignInPopup.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)V
    .locals 0

    .prologue
    .line 1103
    iput-object p1, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1107
    iget-object v0, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->e(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Z

    move-result v0

    if-ne v0, v7, :cond_1

    .line 1166
    :cond_0
    :goto_0
    return-void

    .line 1110
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1112
    iget-object v1, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1113
    iget-object v1, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->g(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1116
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0xce

    if-ne v1, v2, :cond_0

    .line 1117
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_5

    .line 1118
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;

    .line 1120
    if-eqz v0, :cond_4

    .line 1122
    iget-object v1, v0, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->exist_imei:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->exist_imei:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v7, :cond_3

    .line 1124
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1128
    iget-object v0, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 1129
    iget-object v1, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-virtual {v2}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03f9

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v6}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->w(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v5, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v5}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->w(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/registration/y;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/y;-><init>(Lcom/sec/chaton/registration/w;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/registration/x;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/x;-><init>(Lcom/sec/chaton/registration/w;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    .line 1147
    iget-object v1, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v1}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->e(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1148
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1152
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0, v7}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->a(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;Z)V

    goto/16 :goto_0

    .line 1156
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    .line 1157
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1158
    const-string v0, "checkin result is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1163
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/registration/w;->a:Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-static {v0}, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;->b(Lcom/sec/chaton/registration/ActivitySyncSignInPopup;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0
.end method
