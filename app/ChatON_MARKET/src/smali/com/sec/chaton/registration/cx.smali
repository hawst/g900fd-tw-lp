.class Lcom/sec/chaton/registration/cx;
.super Ljava/lang/Object;
.source "FragmentRegist.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentRegist;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentRegist;)V
    .locals 0

    .prologue
    .line 2197
    iput-object p1, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 2202
    const/4 v0, 0x6

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->c(Lcom/sec/chaton/registration/FragmentRegist;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->d(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2204
    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "JP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "IN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "TR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CZ"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ZA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2208
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a()V

    .line 2216
    :goto_0
    const/4 v0, 0x1

    .line 2218
    :goto_1
    return v0

    .line 2209
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2211
    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    const-string v1, "ACS"

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Ljava/lang/String;)V

    goto :goto_0

    .line 2214
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/registration/cx;->a:Lcom/sec/chaton/registration/FragmentRegist;

    const-string v1, "SMS"

    invoke-static {v0, v1}, Lcom/sec/chaton/registration/FragmentRegist;->a(Lcom/sec/chaton/registration/FragmentRegist;Ljava/lang/String;)V

    goto :goto_0

    .line 2218
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
