.class public Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;
.super Landroid/support/v4/app/Fragment;
.source "ActivityProvisioning.java"


# instance fields
.field private a:Landroid/os/Bundle;

.field private b:Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method a(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "join"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->b:Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->b:Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;

    invoke-direct {v0}, Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->b:Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->b:Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->b:Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->b:Lcom/sec/chaton/registration/ActivityProvisioning$JoinWorkerFragment;

    const-string v2, "join"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 84
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 90
    const-string v0, "onConfigurationChanged"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->a:Landroid/os/Bundle;

    .line 56
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 60
    const v0, 0x7f0300f0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->c:Landroid/view/View;

    .line 66
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->a(Landroid/content/res/Configuration;)V

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/registration/ActivityProvisioning$SplashFragment;->c:Landroid/view/View;

    return-object v0
.end method
