.class Lcom/sec/chaton/registration/ah;
.super Ljava/lang/Object;
.source "AuthenticatorActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/AuthenticatorActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/AuthenticatorActivity;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 481
    iget-object v0, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Lcom/sec/chaton/registration/AuthenticatorActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 491
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 492
    const-string v1, "com.sec.chaton"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-gtz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 495
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "samsung_account_email"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 496
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 498
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 499
    invoke-static {v0, v2, v5}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 507
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 511
    const-string v0, "auto_regi_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 513
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "back_regi_status"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 514
    iget-object v0, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    iget-object v1, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->b(Lcom/sec/chaton/registration/AuthenticatorActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivity(Landroid/content/Intent;)V

    .line 520
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->finish()V

    .line 565
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0, v6}, Lcom/sec/chaton/registration/AuthenticatorActivity;->a(Lcom/sec/chaton/registration/AuthenticatorActivity;Z)Z

    .line 567
    :cond_3
    return-void

    .line 500
    :cond_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 501
    invoke-static {v1, v2, v6}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 516
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    const v1, 0x7f0b000a

    invoke-static {v0, v1, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 522
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->c(Lcom/sec/chaton/registration/AuthenticatorActivity;)Z

    move-result v0

    if-ne v0, v5, :cond_8

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_disclaimer_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 523
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    const-class v2, Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 525
    const-string v1, "for_wifi_only_device"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {}, Lcom/sec/chaton/util/am;->w()Z

    move-result v1

    if-nez v1, :cond_7

    .line 527
    const-string v1, "is_ready_to_sa"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 529
    :cond_7
    const-string v1, "is_before_regi"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 530
    iget-object v1, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 531
    iget-object v0, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-virtual {v0, v6, v6}, Lcom/sec/chaton/registration/AuthenticatorActivity;->overridePendingTransition(II)V

    goto :goto_2

    .line 534
    :cond_8
    const-string v0, "provisioning_account_login"

    const-string v1, "PHONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    const-string v0, "[PHONE SMS]"

    const-string v1, "ChatON"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_disclaimer_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 538
    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "selected_country"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 539
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    const-class v2, Lcom/sec/chaton/SelectCountry;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 541
    iget-object v1, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_2

    .line 545
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->d(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    goto/16 :goto_2

    .line 554
    :cond_a
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_selfsms_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 555
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    const-class v2, Lcom/sec/chaton/registration/ActivityRegist;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 556
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 557
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 558
    iget-object v1, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-virtual {v1, v0, v5}, Lcom/sec/chaton/registration/AuthenticatorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_2

    .line 559
    :cond_b
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_pushname_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 560
    iget-object v0, p0, Lcom/sec/chaton/registration/ah;->a:Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-static {v0}, Lcom/sec/chaton/registration/AuthenticatorActivity;->e(Lcom/sec/chaton/registration/AuthenticatorActivity;)V

    goto/16 :goto_2
.end method
