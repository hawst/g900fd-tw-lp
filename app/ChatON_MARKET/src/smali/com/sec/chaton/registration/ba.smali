.class Lcom/sec/chaton/registration/ba;
.super Landroid/os/Handler;
.source "FragmentDisclaimer.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/FragmentDisclaimer;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/FragmentDisclaimer;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/sec/chaton/registration/ba;->a:Lcom/sec/chaton/registration/FragmentDisclaimer;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/registration/ba;->a:Lcom/sec/chaton/registration/FragmentDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentDisclaimer;->a(Lcom/sec/chaton/registration/FragmentDisclaimer;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 287
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 217
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_1

    .line 219
    const-string v0, "Success in accpet disclaimer"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v0, "update_disclaimer_status"

    const-string v1, "DONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v0, "agree_disclaimer"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/registration/ba;->a:Lcom/sec/chaton/registration/FragmentDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentDisclaimer;->a(Lcom/sec/chaton/registration/FragmentDisclaimer;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 285
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/registration/ba;->a:Lcom/sec/chaton/registration/FragmentDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentDisclaimer;->d(Lcom/sec/chaton/registration/FragmentDisclaimer;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 230
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xa411

    if-ne v1, v2, :cond_2

    .line 232
    const-string v0, "Update Accept Disclaimer was failed by already accpeted disclaimer"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v0, "update_disclaimer_status"

    const-string v1, "DONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v0, "agree_disclaimer"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/registration/ba;->a:Lcom/sec/chaton/registration/FragmentDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentDisclaimer;->a(Lcom/sec/chaton/registration/FragmentDisclaimer;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_1

    .line 243
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0xa412

    if-ne v0, v1, :cond_3

    .line 245
    const-string v0, "Update Accept Disclaimer was failed by iso was not matching"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v0, "update_disclaimer_status"

    const-string v1, "DONE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v0, "agree_disclaimer"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/registration/ba;->a:Lcom/sec/chaton/registration/FragmentDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentDisclaimer;->a(Lcom/sec/chaton/registration/FragmentDisclaimer;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_1

    .line 258
    :cond_3
    const-string v0, "failed to accpet disclaimer"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/registration/ba;->a:Lcom/sec/chaton/registration/FragmentDisclaimer;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentDisclaimer;->b(Lcom/sec/chaton/registration/FragmentDisclaimer;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1
.end method
