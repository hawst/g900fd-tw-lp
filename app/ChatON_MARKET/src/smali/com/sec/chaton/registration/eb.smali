.class Lcom/sec/chaton/registration/eb;
.super Ljava/lang/Object;
.source "FragmentRegistPushName.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/registration/du;


# direct methods
.method constructor <init>(Lcom/sec/chaton/registration/du;)V
    .locals 0

    .prologue
    .line 1212
    iput-object p1, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1216
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_pushname_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    iget-object v0, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->i(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "skip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1219
    iget-object v0, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->n(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    iget-object v1, v1, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;)V

    .line 1230
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1231
    iget-object v0, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v0, v0, Lcom/sec/chaton/registration/FragmentRegistPushName;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 1233
    iget-object v0, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    iget-object v1, v1, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-virtual {v1}, Lcom/sec/chaton/registration/FragmentRegistPushName;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0290

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    iget-object v2, v2, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v2}, Lcom/sec/chaton/registration/FragmentRegistPushName;->c(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    iget-object v3, v3, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->d(Lcom/sec/chaton/registration/FragmentRegistPushName;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/registration/FragmentRegistPushName;->a(Lcom/sec/chaton/registration/FragmentRegistPushName;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1236
    :cond_0
    return-void

    .line 1223
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    iget-object v0, v0, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-static {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;->u(Lcom/sec/chaton/registration/FragmentRegistPushName;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/eb;->a:Lcom/sec/chaton/registration/du;

    iget-object v1, v1, Lcom/sec/chaton/registration/du;->a:Lcom/sec/chaton/registration/FragmentRegistPushName;

    iget-object v1, v1, Lcom/sec/chaton/registration/FragmentRegistPushName;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
