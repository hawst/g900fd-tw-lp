.class public Lcom/sec/chaton/registration/ActivityRegist;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityRegist.java"


# instance fields
.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 30
    iput-boolean v0, p0, Lcom/sec/chaton/registration/ActivityRegist;->a:Z

    .line 31
    iput-boolean v0, p0, Lcom/sec/chaton/registration/ActivityRegist;->b:Z

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityRegist;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 43
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/chaton/registration/ActivityRegist;->a:Z

    .line 44
    sget-object v1, Lcom/sec/chaton/registration/FragmentRegist;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/registration/ActivityRegist;->b:Z

    .line 47
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivityRegist;->a:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivityRegist;->b:Z

    if-eqz v0, :cond_2

    .line 48
    :cond_1
    const v0, 0x7f0b0005

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivityRegist;->setTitle(I)V

    .line 51
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/chaton/registration/ActivityRegist;->b:Z

    if-eqz v0, :cond_3

    .line 52
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityRegist;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 53
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityRegist;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->f()V

    .line 62
    :cond_3
    new-instance v0, Lcom/sec/chaton/registration/FragmentRegist;

    invoke-direct {v0}, Lcom/sec/chaton/registration/FragmentRegist;-><init>()V

    return-object v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 69
    const-string v0, "first_time_launch"

    const-string v1, "BACK_PRESSED"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onBackPressed()V

    .line 72
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 113
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityRegist;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 116
    const-string v1, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    const-string v1, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-static {p0}, Lcom/sec/chaton/registration/ActivityRegist;->a(Landroid/app/Activity;)V

    .line 124
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 96
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityRegist;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_0

    .line 99
    const-string v1, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_0

    const-string v1, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-static {p0}, Lcom/sec/chaton/registration/ActivityRegist;->a(Landroid/app/Activity;)V

    .line 107
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 77
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 82
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivityRegist;->setResult(I)V

    .line 83
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_disclaimer_status"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityRegist;->finish()V

    .line 86
    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
