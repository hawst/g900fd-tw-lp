.class public Lcom/sec/chaton/registration/es;
.super Ljava/lang/Object;
.source "FristLauncherDialog.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/os/Bundle;

.field private c:Lcom/sec/chaton/registration/fu;

.field private d:I

.field private e:Lcom/sec/common/a/d;

.field private f:Z

.field private g:Landroid/view/View;

.field private h:Lcom/sec/common/a/d;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/EditText;

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    .line 58
    const-class v0, Lcom/sec/chaton/registration/es;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->k:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Lcom/sec/chaton/registration/fu;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    .line 58
    const-class v0, Lcom/sec/chaton/registration/es;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->k:Ljava/lang/String;

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/registration/es;->d:I

    .line 66
    iput-object p1, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    .line 67
    iput-object p2, p0, Lcom/sec/chaton/registration/es;->b:Landroid/os/Bundle;

    .line 68
    iput-object p3, p0, Lcom/sec/chaton/registration/es;->c:Lcom/sec/chaton/registration/fu;

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/es;)I
    .locals 2

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/chaton/registration/es;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/chaton/registration/es;->d:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/registration/es;I)I
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/chaton/registration/es;->d:I

    return p1
.end method

.method private a(I)V
    .locals 9

    .prologue
    const v8, 0x7f0b0156

    const v3, 0x7f070464

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 249
    const v1, 0x7f030107

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 254
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V

    .line 255
    new-instance v1, Lcom/sec/chaton/registration/et;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/et;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 271
    iget-object v1, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    const v2, 0x7f07013c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 272
    iget-object v2, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    const v3, 0x7f07013b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 276
    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    .line 277
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 280
    const v1, 0x7f0b039a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 357
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/es;->b(I)V

    .line 358
    invoke-direct {p0}, Lcom/sec/chaton/registration/es;->b()V

    .line 359
    return-void

    .line 281
    :cond_1
    const/4 v3, 0x5

    if-ne p1, v3, :cond_2

    .line 282
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 285
    const v1, 0x7f0b0394

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 286
    :cond_2
    const/4 v3, 0x4

    if-ne p1, v3, :cond_3

    .line 287
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 290
    const v1, 0x7f0b0395

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 291
    :cond_3
    if-ne p1, v5, :cond_4

    .line 292
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 293
    const v1, 0x7f0b0398

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 295
    :cond_4
    const/4 v2, 0x3

    if-ne p1, v2, :cond_5

    .line 296
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 299
    const v1, 0x7f0b0399

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 301
    :cond_5
    const/4 v2, 0x7

    if-ne p1, v2, :cond_6

    .line 302
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 304
    const v1, 0x7f0b0393

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 305
    :cond_6
    if-ne p1, v7, :cond_8

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "PacketDataDialg"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_8

    .line 306
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "PacketDataDialg"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 307
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 311
    :cond_7
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 312
    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setText(I)V

    .line 313
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 314
    new-instance v2, Lcom/sec/chaton/registration/fe;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fe;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    new-instance v2, Lcom/sec/chaton/registration/fn;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fn;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 332
    const v1, 0x7f0b0396

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 334
    :cond_8
    const/16 v2, 0xa

    if-ne p1, v2, :cond_0

    .line 335
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 336
    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setText(I)V

    .line 337
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 338
    new-instance v2, Lcom/sec/chaton/registration/fo;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fo;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 345
    new-instance v2, Lcom/sec/chaton/registration/fp;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fp;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 354
    const v1, 0x7f0b039e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v7, -0x2

    const/4 v6, -0x3

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 101
    const-string v2, "android.hardware.telephony"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v2, v1

    .line 102
    :goto_0
    if-nez v2, :cond_1a

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 107
    if-eqz v0, :cond_1a

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    if-ne v0, v1, :cond_1a

    move v0, v1

    .line 112
    :goto_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v2

    .line 114
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v4, :cond_0

    .line 115
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "netState : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/registration/es;->k:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    if-eqz v0, :cond_3

    .line 118
    if-eq v6, v2, :cond_2

    if-eq v7, v2, :cond_2

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->c:Lcom/sec/chaton/registration/fu;

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->b:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, v1}, Lcom/sec/chaton/registration/fu;->a(ZLandroid/os/Bundle;Z)V

    .line 245
    :goto_2
    return-void

    :cond_1
    move v2, v3

    .line 101
    goto :goto_0

    .line 122
    :cond_2
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/es;->a(I)V

    goto :goto_2

    .line 130
    :cond_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_19

    move v0, v1

    .line 134
    :goto_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/j/v;->c(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 137
    if-nez v2, :cond_17

    if-eq v6, v2, :cond_17

    if-eq v7, v2, :cond_17

    move v2, v1

    move v4, v1

    .line 142
    :goto_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/chaton/j/v;->e(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/chaton/j/v;->f(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_16

    :cond_4
    move v9, v1

    .line 147
    :goto_5
    if-eqz v0, :cond_15

    if-eqz v4, :cond_15

    move v4, v1

    .line 151
    :goto_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/chaton/j/v;->d(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_14

    move v5, v1

    .line 155
    :goto_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/chaton/j/v;->g(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_13

    move v6, v1

    .line 158
    :goto_8
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/chaton/j/v;->h(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_12

    move v7, v1

    .line 161
    :goto_9
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/chaton/j/v;->i(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_11

    move v8, v1

    .line 166
    :goto_a
    if-eqz v4, :cond_6

    .line 168
    if-eqz v2, :cond_5

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->c:Lcom/sec/chaton/registration/fu;

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->b:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, v1}, Lcom/sec/chaton/registration/fu;->a(ZLandroid/os/Bundle;Z)V

    goto :goto_2

    .line 171
    :cond_5
    invoke-direct {p0, v10}, Lcom/sec/chaton/registration/es;->a(I)V

    goto :goto_2

    .line 176
    :cond_6
    if-nez v2, :cond_d

    if-nez v8, :cond_d

    .line 178
    if-eqz v0, :cond_7

    .line 180
    invoke-direct {p0, v10}, Lcom/sec/chaton/registration/es;->a(I)V

    goto/16 :goto_2

    .line 185
    :cond_7
    if-nez v9, :cond_8

    .line 187
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/es;->a(I)V

    goto/16 :goto_2

    .line 192
    :cond_8
    if-eqz v6, :cond_a

    .line 194
    if-nez v5, :cond_9

    .line 196
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/es;->a(I)V

    goto/16 :goto_2

    .line 202
    :cond_9
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/es;->a(I)V

    goto/16 :goto_2

    .line 208
    :cond_a
    if-nez v7, :cond_b

    .line 210
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/es;->a(I)V

    goto/16 :goto_2

    .line 214
    :cond_b
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "PacketDataDialg"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_c

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->c:Lcom/sec/chaton/registration/fu;

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->b:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, v1}, Lcom/sec/chaton/registration/fu;->a(ZLandroid/os/Bundle;Z)V

    goto/16 :goto_2

    .line 218
    :cond_c
    invoke-direct {p0, v1}, Lcom/sec/chaton/registration/es;->a(I)V

    goto/16 :goto_2

    .line 228
    :cond_d
    if-eqz v2, :cond_10

    const-string v0, "460"

    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 229
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_e

    .line 230
    const-string v0, "Sim is china, so show connecte via WLAN"

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->k:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_e
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "DoNotShowWlanDialog"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->c:Lcom/sec/chaton/registration/fu;

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->b:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, v1}, Lcom/sec/chaton/registration/fu;->a(ZLandroid/os/Bundle;Z)V

    goto/16 :goto_2

    .line 235
    :cond_f
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/es;->a(I)V

    goto/16 :goto_2

    .line 238
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->c:Lcom/sec/chaton/registration/fu;

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->b:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, v1}, Lcom/sec/chaton/registration/fu;->a(ZLandroid/os/Bundle;Z)V

    goto/16 :goto_2

    :cond_11
    move v8, v3

    goto/16 :goto_a

    :cond_12
    move v7, v3

    goto/16 :goto_9

    :cond_13
    move v6, v3

    goto/16 :goto_8

    :cond_14
    move v5, v3

    goto/16 :goto_7

    :cond_15
    move v4, v3

    goto/16 :goto_6

    :cond_16
    move v9, v3

    goto/16 :goto_5

    :cond_17
    move v2, v3

    move v4, v1

    goto/16 :goto_4

    :cond_18
    move v2, v3

    move v4, v3

    goto/16 :goto_4

    :cond_19
    move v0, v3

    goto/16 :goto_3

    :cond_1a
    move v0, v2

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/registration/es;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/chaton/registration/es;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/registration/es;Z)Z
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/chaton/registration/es;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/registration/es;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/chaton/registration/es;->d:I

    return v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 572
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 573
    const v1, 0x7f0300a3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->i:Landroid/view/View;

    .line 575
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->i:Landroid/view/View;

    const v1, 0x7f07030b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->j:Landroid/widget/EditText;

    .line 577
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/registration/es;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b003b

    new-instance v2, Lcom/sec/chaton/registration/fm;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fm;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/registration/fl;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fl;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->e:Lcom/sec/common/a/d;

    .line 621
    return-void
.end method

.method private b(I)V
    .locals 7

    .prologue
    const v6, 0x7f0b039b

    const v5, 0x7f0b003a

    const v4, 0x7f0b0205

    const v3, 0x7f0b0037

    const/4 v2, 0x1

    .line 363
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 365
    const/16 v1, 0x8

    if-ne p1, v1, :cond_1

    .line 366
    const v1, 0x7f0b0397

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/fq;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fq;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v3, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 377
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    .line 378
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/registration/fr;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/fr;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 567
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 568
    return-void

    .line 385
    :cond_1
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 386
    invoke-virtual {v0, v4}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/fs;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fs;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v3, v2}, Lcom/sec/common/a/a;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 397
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    .line 398
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/registration/ft;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/ft;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 405
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    .line 406
    const v1, 0x7f0b039c

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/ev;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ev;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v6, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/eu;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/eu;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v5, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 424
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/registration/ew;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/ew;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 433
    :cond_3
    const/4 v1, 0x3

    if-ne p1, v1, :cond_4

    .line 434
    invoke-virtual {v0, v4}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/ex;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ex;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v3, v2}, Lcom/sec/common/a/a;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 444
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    .line 445
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/registration/ey;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/ey;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_0

    .line 453
    :cond_4
    const/4 v1, 0x5

    if-ne p1, v1, :cond_5

    .line 454
    invoke-virtual {v0, v4}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/ez;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ez;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v3, v2}, Lcom/sec/common/a/a;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 466
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    .line 467
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/registration/fa;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/fa;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_0

    .line 475
    :cond_5
    const/4 v1, 0x7

    if-ne p1, v1, :cond_6

    .line 476
    invoke-virtual {v0, v4}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0024

    new-instance v3, Lcom/sec/chaton/registration/fc;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/fc;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b03a6

    new-instance v3, Lcom/sec/chaton/registration/fb;

    invoke-direct {v3, p0}, Lcom/sec/chaton/registration/fb;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 497
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    .line 498
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/registration/fd;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/fd;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_0

    .line 506
    :cond_6
    if-ne p1, v2, :cond_7

    .line 507
    const v1, 0x7f0b0392

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/fg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fg;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v6, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/ff;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/ff;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v5, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 527
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    .line 528
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/registration/fh;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/fh;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_0

    .line 537
    :cond_7
    const/16 v1, 0xa

    if-ne p1, v1, :cond_0

    .line 538
    const v1, 0x7f0b039d

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/registration/es;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/fj;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fj;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v6, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/registration/fi;

    invoke-direct {v2, p0}, Lcom/sec/chaton/registration/fi;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-virtual {v1, v5, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 557
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    .line 558
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->h:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/registration/fk;

    invoke-direct {v1, p0}, Lcom/sec/chaton/registration/fk;-><init>(Lcom/sec/chaton/registration/es;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/registration/es;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/registration/es;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->e:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/registration/es;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->b:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/registration/es;)Lcom/sec/chaton/registration/fu;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->c:Lcom/sec/chaton/registration/fu;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/registration/es;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/chaton/registration/es;->f:Z

    return v0
.end method

.method static synthetic h(Lcom/sec/chaton/registration/es;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->j:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/registration/es;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/registration/es;->b:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/sec/chaton/registration/es;->a(Landroid/os/Bundle;)V

    .line 78
    return-void
.end method
