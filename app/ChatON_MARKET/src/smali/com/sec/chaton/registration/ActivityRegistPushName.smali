.class public Lcom/sec/chaton/registration/ActivityRegistPushName;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ActivityRegistPushName.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 23
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "provisioning_sns_login_state"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityRegistPushName;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/common/actionbar/a;->a(Z)V

    .line 25
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityRegistPushName;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/common/actionbar/a;->d(Z)V

    .line 28
    :cond_0
    new-instance v0, Lcom/sec/chaton/registration/FragmentRegistPushName;

    invoke-direct {v0}, Lcom/sec/chaton/registration/FragmentRegistPushName;-><init>()V

    return-object v0
.end method

.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 49
    const v0, 0x7f0c00f8

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 44
    const v0, 0x7f0c00f6

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 33
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 34
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/registration/ActivityRegistPushName;->setResult(I)V

    .line 35
    invoke-virtual {p0}, Lcom/sec/chaton/registration/ActivityRegistPushName;->finish()V

    .line 36
    const/4 v0, 0x1

    .line 38
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
