.class public Lcom/sec/chaton/h/b;
.super Ljava/lang/Object;
.source "InterlockedAppLink.java"

# interfaces
.implements Lcom/sec/chaton/h/a;


# instance fields
.field private a:Landroid/content/Intent;


# direct methods
.method private constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/chaton/h/b;->a:Landroid/content/Intent;

    .line 20
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/sec/chaton/h/b;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/sec/chaton/h/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/h/b;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/v;
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/chaton/h/b;->a:Landroid/content/Intent;

    const-string v1, "callForward"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 25
    iget-object v0, p0, Lcom/sec/chaton/h/b;->a:Landroid/content/Intent;

    const-string v1, "content_type"

    sget-object v2, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 27
    iget-object v0, p0, Lcom/sec/chaton/h/b;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/sec/chaton/h/b;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/sec/chaton/h/b;->a:Landroid/content/Intent;

    const-string v2, "download_uri"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    :cond_0
    sget-object v0, Lcom/sec/chaton/v;->b:Lcom/sec/chaton/v;

    return-object v0
.end method
