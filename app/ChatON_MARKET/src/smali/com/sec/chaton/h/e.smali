.class public Lcom/sec/chaton/h/e;
.super Ljava/lang/Object;
.source "InterlockedContact.java"

# interfaces
.implements Lcom/sec/chaton/h/a;


# instance fields
.field private a:Landroid/content/Intent;


# direct methods
.method private constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    .line 32
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/sec/chaton/h/e;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/sec/chaton/h/e;

    invoke-direct {v0, p0}, Lcom/sec/chaton/h/e;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/v;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 36
    iget-object v0, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    const-string v1, "content_type"

    sget-object v2, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 37
    iget-object v0, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 38
    const-string v6, ""

    .line 42
    if-nez v1, :cond_0

    .line 43
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    .line 113
    :goto_0
    return-object v0

    .line 46
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chooseDest uri: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_1
    invoke-static {v1}, Lcom/sec/chaton/multimedia/vcard/ReadVCardActivity;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto :goto_0

    .line 56
    :cond_2
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "content"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 58
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 65
    if-eqz v2, :cond_8

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 67
    const-string v0, "_display_name"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 72
    :goto_1
    if-eqz v2, :cond_3

    .line 73
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 76
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 77
    iget-object v2, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    const-string v3, "callForward"

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 79
    :try_start_2
    iget-object v2, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    const-string v3, "sub_content"

    const/4 v4, 0x0

    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    .line 84
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const-string v2, "vcf"

    invoke-static {v1, v0, v2}, Lcom/sec/chaton/h/h;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_7

    .line 88
    iget-object v1, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    const-string v2, "download_uri"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    sget-object v0, Lcom/sec/chaton/v;->b:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 72
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_4

    .line 73
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 72
    :cond_4
    throw v0

    .line 80
    :catch_1
    move-exception v0

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    const-string v2, "sub_content"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 93
    :cond_5
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "file"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    const-string v2, "callForward"

    invoke-virtual {v0, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    const-string v2, "download_uri"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    const-string v2, "sub_content"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 110
    :goto_4
    sget-object v0, Lcom/sec/chaton/v;->b:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 101
    :cond_6
    :try_start_4
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 102
    const/4 v1, 0x0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    goto :goto_3

    .line 106
    :catch_2
    move-exception v0

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/h/e;->a:Landroid/content/Intent;

    const-string v1, "sub_content"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_4

    .line 113
    :cond_7
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    :cond_8
    move-object v0, v6

    goto/16 :goto_1
.end method
