.class public Lcom/sec/chaton/mobileweb/a/a;
.super Landroid/os/AsyncTask;
.source "MobileWebHandshakeTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyHandshakeEntry;

.field private c:Landroid/os/Handler;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/chaton/mobileweb/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/mobileweb/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/chaton/mobileweb/a/a;->c:Landroid/os/Handler;

    .line 24
    iput-object p2, p0, Lcom/sec/chaton/mobileweb/a/a;->d:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public static a(Landroid/os/Handler;Ljava/lang/String;)Lcom/sec/chaton/mobileweb/a/a;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/sec/chaton/mobileweb/a/a;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/mobileweb/a/a;-><init>(Landroid/os/Handler;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 38
    const-string v0, "doInBackground()"

    sget-object v1, Lcom/sec/chaton/mobileweb/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/sec/chaton/util/v;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-direct {v0, v1}, Lcom/sec/chaton/util/v;-><init>(Ljava/lang/String;)V

    .line 43
    :try_start_0
    const-class v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyHandshakeEntry;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/v;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyHandshakeEntry;

    iput-object v0, p0, Lcom/sec/chaton/mobileweb/a/a;->b:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyHandshakeEntry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[IN] ENTRY : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/a/a;->b:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyHandshakeEntry;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/mobileweb/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    .line 44
    :catch_0
    move-exception v0

    .line 45
    sget-object v1, Lcom/sec/chaton/mobileweb/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 46
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 55
    const-string v0, "onPostExecute()"

    sget-object v1, Lcom/sec/chaton/mobileweb/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/a/a;->b:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyHandshakeEntry;

    if-nez v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/a/a;->b:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyHandshakeEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyHandshakeEntry;->success:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/a/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/mobileweb/s;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/a/a;->c:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 62
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/a/a;->c:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 63
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 64
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 65
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/chaton/mobileweb/a/a;->c:Landroid/os/Handler;

    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[OUT] RESPONSE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/mobileweb/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :cond_1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/mobileweb/a/a;->a([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/mobileweb/a/a;->a(Ljava/lang/Integer;)V

    return-void
.end method
