.class public abstract Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;
.super Landroid/support/v4/app/Fragment;
.source "AbstractMobileWebFragment.java"

# interfaces
.implements Lcom/sec/chaton/base/d;


# instance fields
.field protected final a:Ljava/lang/String;

.field protected b:Lcom/sec/chaton/mobileweb/r;

.field protected c:Landroid/webkit/WebView;

.field protected final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/mobileweb/o;",
            ">;"
        }
    .end annotation
.end field

.field protected final e:Lcom/sec/chaton/mobileweb/p;

.field protected f:Z

.field g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/chaton/mobileweb/h;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/chaton/mobileweb/h;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private j:Landroid/app/Activity;

.field private k:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/sec/chaton/mobileweb/p;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 49
    iput-boolean v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k:Z

    .line 51
    iput-boolean v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->f:Z

    .line 52
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->g:Ljava/util/Set;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->h:Ljava/util/Set;

    .line 61
    iput-object p1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->e:Lcom/sec/chaton/mobileweb/p;

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->e:Lcom/sec/chaton/mobileweb/p;

    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/p;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->d:Ljava/util/List;

    .line 64
    iput p3, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->i:I

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->j:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Landroid/view/View;)Landroid/webkit/WebView;
.end method

.method protected a(Lcom/sec/chaton/mobileweb/h;)V
    .locals 4

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 102
    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c()V

    .line 104
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "spbd_progress, showOpenProgress(), add (%s), become (%s)  "

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->h:Ljava/util/Set;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 271
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadUrl() : is null webview - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/ url - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 278
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 279
    :catch_0
    move-exception v0

    .line 280
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 270
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 85
    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    :goto_0
    return v0

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 90
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->goBack()V

    goto :goto_0

    .line 93
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b()Landroid/os/Handler;
.end method

.method protected b(Lcom/sec/chaton/mobileweb/h;)V
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    if-nez p1, :cond_2

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 121
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->d()V

    goto :goto_0

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 292
    return-void
.end method

.method protected abstract c()V
.end method

.method protected c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 295
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    const-string v0, "startWebContents(), EMPTY URL !!"

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :goto_0
    return-void

    .line 299
    :cond_0
    sget-object v0, Lcom/sec/chaton/mobileweb/h;->a:Lcom/sec/chaton/mobileweb/h;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a(Lcom/sec/chaton/mobileweb/h;)V

    .line 300
    invoke-virtual {p0, p1}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a(Ljava/lang/String;)V

    .line 301
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k:Z

    goto :goto_0
.end method

.method protected abstract d()V
.end method

.method protected abstract e()V
.end method

.method public f()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->j:Landroid/app/Activity;

    return-object v0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/mobileweb/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->d:Ljava/util/List;

    return-object v0
.end method

.method public h()Lcom/sec/chaton/mobileweb/p;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->e:Lcom/sec/chaton/mobileweb/p;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k:Z

    return v0
.end method

.method protected j()V
    .locals 0

    .prologue
    .line 288
    return-void
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->j:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 454
    :cond_0
    const/4 v0, 0x0

    .line 457
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 163
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 164
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 165
    const-string v0, "onActivityCreated()"

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->j:Landroid/app/Activity;

    .line 134
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 135
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 141
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 142
    const-string v0, "onCreateView()"

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 147
    iget v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->i:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 148
    invoke-virtual {p0, v0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a(Landroid/view/View;)Landroid/webkit/WebView;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    .line 150
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    invoke-static {v1}, Lcom/sec/chaton/mobileweb/s;->a(Landroid/webkit/WebView;)V

    .line 151
    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->b()Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->d:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/sec/chaton/mobileweb/r;->buildMobileWebBridgeClass(Landroid/os/Handler;Ljava/util/List;)Lcom/sec/chaton/mobileweb/r;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->b:Lcom/sec/chaton/mobileweb/r;

    .line 152
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->b:Lcom/sec/chaton/mobileweb/r;

    const-string v3, "ChatONBridge"

    invoke-virtual {v1, v2, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    new-instance v2, Lcom/sec/chaton/mobileweb/i;

    iget-object v3, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-direct {v2, p0, v3}, Lcom/sec/chaton/mobileweb/i;-><init>(Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 154
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    new-instance v2, Lcom/sec/chaton/mobileweb/a;

    iget-object v3, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-direct {v2, p0, v3}, Lcom/sec/chaton/mobileweb/a;-><init>(Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 158
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 205
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 206
    const-string v0, "onDestroy()"

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 209
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 213
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 214
    const-string v0, "onDestroyView()"

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    if-eqz v0, :cond_2

    .line 219
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->c:Landroid/webkit/WebView;

    .line 229
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->b:Lcom/sec/chaton/mobileweb/r;

    if-eqz v0, :cond_3

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->b:Lcom/sec/chaton/mobileweb/r;

    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/r;->release()V

    .line 233
    :cond_3
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 234
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 235
    return-void

    .line 221
    :catch_0
    move-exception v0

    .line 222
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 223
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 239
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 240
    const-string v0, "onDetach()"

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->j:Landroid/app/Activity;

    .line 244
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 245
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 188
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 189
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 190
    const-string v0, "onPause()"

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 180
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 181
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 182
    const-string v0, "onResume()"

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 171
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 172
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 173
    const-string v0, "onStart()"

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->f:Z

    .line 176
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 196
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 197
    const-string v0, "onStop()"

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->f:Z

    .line 200
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 201
    return-void
.end method
