.class Lcom/sec/chaton/mobileweb/a;
.super Lcom/sec/chaton/mobileweb/m;
.source "AbstractMobileWebFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lcom/sec/chaton/mobileweb/a;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    .line 372
    invoke-direct {p0, p2}, Lcom/sec/chaton/mobileweb/m;-><init>(Ljava/lang/String;)V

    .line 373
    return-void
.end method


# virtual methods
.method public onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 410
    iget-object v2, p0, Lcom/sec/chaton/mobileweb/a;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k()Z

    move-result v2

    if-nez v2, :cond_0

    .line 447
    :goto_0
    return v0

    .line 414
    :cond_0
    const-string v2, "hslee_web, chrome, onJsAlert(), url(%s), message(%s), result(%s)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v1

    aput-object p3, v3, v0

    const/4 v4, 0x2

    aput-object p4, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 415
    iget-object v3, p0, Lcom/sec/chaton/mobileweb/a;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iget-object v2, p0, Lcom/sec/chaton/mobileweb/a;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 418
    iget-object v2, p0, Lcom/sec/chaton/mobileweb/a;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-static {v2}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a(Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 419
    const v3, 0x7f0b0007

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 420
    invoke-virtual {v2, p3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 421
    const v3, 0x104000a

    new-instance v4, Lcom/sec/chaton/mobileweb/e;

    invoke-direct {v4, p0, p4}, Lcom/sec/chaton/mobileweb/e;-><init>(Lcom/sec/chaton/mobileweb/a;Landroid/webkit/JsResult;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 428
    invoke-virtual {v2}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v2

    .line 429
    new-instance v3, Lcom/sec/chaton/mobileweb/f;

    invoke-direct {v3, p0, p4}, Lcom/sec/chaton/mobileweb/f;-><init>(Lcom/sec/chaton/mobileweb/a;Landroid/webkit/JsResult;)V

    invoke-interface {v2, v3}, Lcom/sec/common/a/d;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 436
    invoke-interface {v2}, Lcom/sec/common/a/d;->show()V

    .line 437
    new-instance v3, Lcom/sec/chaton/mobileweb/g;

    invoke-direct {v3, p0, p4}, Lcom/sec/chaton/mobileweb/g;-><init>(Lcom/sec/chaton/mobileweb/a;Landroid/webkit/JsResult;)V

    invoke-interface {v2, v3}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 443
    invoke-interface {v2, v1}, Lcom/sec/common/a/d;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 447
    goto :goto_0
.end method

.method public onJsConfirm(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/a;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    :goto_0
    return v3

    .line 381
    :cond_0
    const-string v0, "hslee_web, chrome, onJsConfirm(), url(%s) / message(%s) / result(%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v4

    aput-object p3, v1, v3

    const/4 v2, 0x2

    aput-object p4, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 382
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/a;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/a;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 385
    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/mobileweb/c;

    invoke-direct {v2, p0, p4}, Lcom/sec/chaton/mobileweb/c;-><init>(Lcom/sec/chaton/mobileweb/a;Landroid/webkit/JsResult;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/sec/chaton/mobileweb/b;

    invoke-direct {v2, p0, p4}, Lcom/sec/chaton/mobileweb/b;-><init>(Lcom/sec/chaton/mobileweb/a;Landroid/webkit/JsResult;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 397
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 398
    new-instance v1, Lcom/sec/chaton/mobileweb/d;

    invoke-direct {v1, p0, p4}, Lcom/sec/chaton/mobileweb/d;-><init>(Lcom/sec/chaton/mobileweb/a;Landroid/webkit/JsResult;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 404
    invoke-interface {v0, v4}, Lcom/sec/common/a/d;->setCanceledOnTouchOutside(Z)V

    goto :goto_0
.end method
