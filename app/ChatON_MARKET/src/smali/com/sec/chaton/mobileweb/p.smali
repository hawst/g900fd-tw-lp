.class public final enum Lcom/sec/chaton/mobileweb/p;
.super Ljava/lang/Enum;
.source "MobileWebApiUsageType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/mobileweb/p;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/mobileweb/p;

.field public static final enum b:Lcom/sec/chaton/mobileweb/p;

.field public static final enum c:Lcom/sec/chaton/mobileweb/p;

.field private static final synthetic g:[Lcom/sec/chaton/mobileweb/p;


# instance fields
.field d:I

.field e:Ljava/lang/String;

.field f:Lcom/sec/chaton/mobileweb/u;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v9, 0x2

    const/4 v3, 0x1

    .line 9
    new-instance v0, Lcom/sec/chaton/mobileweb/p;

    const-string v1, "LIVE_CONTENTS"

    sget-object v4, Lcom/sec/chaton/mobileweb/u;->a:Lcom/sec/chaton/mobileweb/u;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/mobileweb/p;-><init>(Ljava/lang/String;IILcom/sec/chaton/mobileweb/u;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/mobileweb/p;->a:Lcom/sec/chaton/mobileweb/p;

    .line 10
    new-instance v6, Lcom/sec/chaton/mobileweb/p;

    const-string v7, "LIVE_MAIN"

    sget-object v10, Lcom/sec/chaton/mobileweb/u;->a:Lcom/sec/chaton/mobileweb/u;

    const-string v11, "/live/main.html"

    move v8, v3

    invoke-direct/range {v6 .. v11}, Lcom/sec/chaton/mobileweb/p;-><init>(Ljava/lang/String;IILcom/sec/chaton/mobileweb/u;Ljava/lang/String;)V

    sput-object v6, Lcom/sec/chaton/mobileweb/p;->b:Lcom/sec/chaton/mobileweb/p;

    .line 12
    new-instance v7, Lcom/sec/chaton/mobileweb/p;

    const-string v8, "LIVE_EVENT"

    const/4 v10, 0x4

    sget-object v11, Lcom/sec/chaton/mobileweb/u;->a:Lcom/sec/chaton/mobileweb/u;

    move-object v12, v5

    invoke-direct/range {v7 .. v12}, Lcom/sec/chaton/mobileweb/p;-><init>(Ljava/lang/String;IILcom/sec/chaton/mobileweb/u;Ljava/lang/String;)V

    sput-object v7, Lcom/sec/chaton/mobileweb/p;->c:Lcom/sec/chaton/mobileweb/p;

    .line 8
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/mobileweb/p;

    sget-object v1, Lcom/sec/chaton/mobileweb/p;->a:Lcom/sec/chaton/mobileweb/p;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/mobileweb/p;->b:Lcom/sec/chaton/mobileweb/p;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/mobileweb/p;->c:Lcom/sec/chaton/mobileweb/p;

    aput-object v1, v0, v9

    sput-object v0, Lcom/sec/chaton/mobileweb/p;->g:[Lcom/sec/chaton/mobileweb/p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/sec/chaton/mobileweb/u;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/chaton/mobileweb/u;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput p3, p0, Lcom/sec/chaton/mobileweb/p;->d:I

    .line 22
    iput-object p4, p0, Lcom/sec/chaton/mobileweb/p;->f:Lcom/sec/chaton/mobileweb/u;

    .line 23
    iput-object p5, p0, Lcom/sec/chaton/mobileweb/p;->e:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/mobileweb/p;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/sec/chaton/mobileweb/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/mobileweb/p;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/mobileweb/p;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/chaton/mobileweb/p;->g:[Lcom/sec/chaton/mobileweb/p;

    invoke-virtual {v0}, [Lcom/sec/chaton/mobileweb/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/mobileweb/p;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/p;->f:Lcom/sec/chaton/mobileweb/u;

    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/u;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/p;->e:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/mobileweb/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 36
    sget-object v1, Lcom/sec/chaton/mobileweb/q;->a:[I

    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/p;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 77
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 38
    :pswitch_0
    sget-object v1, Lcom/sec/chaton/mobileweb/o;->b:Lcom/sec/chaton/mobileweb/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v1, Lcom/sec/chaton/mobileweb/o;->f:Lcom/sec/chaton/mobileweb/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    const-string v1, "live_chat_feature"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    sget-object v1, Lcom/sec/chaton/mobileweb/o;->e:Lcom/sec/chaton/mobileweb/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 44
    :cond_0
    sget-object v1, Lcom/sec/chaton/mobileweb/o;->d:Lcom/sec/chaton/mobileweb/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 50
    :pswitch_1
    sget-object v1, Lcom/sec/chaton/mobileweb/o;->b:Lcom/sec/chaton/mobileweb/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    const-string v1, "live_chat_feature"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    sget-object v1, Lcom/sec/chaton/mobileweb/o;->e:Lcom/sec/chaton/mobileweb/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    :goto_1
    sget-object v1, Lcom/sec/chaton/mobileweb/o;->g:Lcom/sec/chaton/mobileweb/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 54
    :cond_1
    sget-object v1, Lcom/sec/chaton/mobileweb/o;->d:Lcom/sec/chaton/mobileweb/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 71
    :pswitch_2
    sget-object v1, Lcom/sec/chaton/mobileweb/o;->b:Lcom/sec/chaton/mobileweb/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 36
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
