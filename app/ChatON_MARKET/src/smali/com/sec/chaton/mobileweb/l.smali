.class public final enum Lcom/sec/chaton/mobileweb/l;
.super Ljava/lang/Enum;
.source "ChatONAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/mobileweb/l;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/mobileweb/l;

.field public static final enum b:Lcom/sec/chaton/mobileweb/l;

.field public static final enum c:Lcom/sec/chaton/mobileweb/l;

.field private static final synthetic e:[Lcom/sec/chaton/mobileweb/l;


# instance fields
.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 207
    new-instance v0, Lcom/sec/chaton/mobileweb/l;

    const-string v1, "LIVE_CONTENTS"

    const-string v2, "/chat/livecontents"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/mobileweb/l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/mobileweb/l;->a:Lcom/sec/chaton/mobileweb/l;

    .line 208
    new-instance v0, Lcom/sec/chaton/mobileweb/l;

    const-string v1, "BUDDY_POPUP"

    const-string v2, "/buddypopup"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/mobileweb/l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/mobileweb/l;->b:Lcom/sec/chaton/mobileweb/l;

    .line 209
    new-instance v0, Lcom/sec/chaton/mobileweb/l;

    const-string v1, "LIVE_CHAT"

    const-string v2, "/chat/livechat"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/chaton/mobileweb/l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/mobileweb/l;->c:Lcom/sec/chaton/mobileweb/l;

    .line 206
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/mobileweb/l;

    sget-object v1, Lcom/sec/chaton/mobileweb/l;->a:Lcom/sec/chaton/mobileweb/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/mobileweb/l;->b:Lcom/sec/chaton/mobileweb/l;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/mobileweb/l;->c:Lcom/sec/chaton/mobileweb/l;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/mobileweb/l;->e:[Lcom/sec/chaton/mobileweb/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 222
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 224
    iput-object p3, p0, Lcom/sec/chaton/mobileweb/l;->d:Ljava/lang/String;

    .line 225
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/mobileweb/l;
    .locals 5

    .prologue
    .line 213
    invoke-static {}, Lcom/sec/chaton/mobileweb/l;->values()[Lcom/sec/chaton/mobileweb/l;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 214
    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/l;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 218
    :goto_1
    return-object v0

    .line 213
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 218
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/mobileweb/l;
    .locals 1

    .prologue
    .line 206
    const-class v0, Lcom/sec/chaton/mobileweb/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/mobileweb/l;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/mobileweb/l;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/sec/chaton/mobileweb/l;->e:[Lcom/sec/chaton/mobileweb/l;

    invoke-virtual {v0}, [Lcom/sec/chaton/mobileweb/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/mobileweb/l;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/l;->d:Ljava/lang/String;

    return-object v0
.end method
