.class Lcom/sec/chaton/mobileweb/i;
.super Lcom/sec/chaton/mobileweb/n;
.source "AbstractMobileWebFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/sec/chaton/mobileweb/i;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    .line 306
    invoke-direct {p0, p2}, Lcom/sec/chaton/mobileweb/n;-><init>(Ljava/lang/String;)V

    .line 307
    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/i;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    :goto_0
    return-void

    .line 327
    :cond_0
    const-string v0, "hslee_web, view, onPageFinished(), url(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 328
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/i;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/i;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    sget-object v1, Lcom/sec/chaton/mobileweb/h;->a:Lcom/sec/chaton/mobileweb/h;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->b(Lcom/sec/chaton/mobileweb/h;)V

    .line 330
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/mobileweb/n;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/i;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 319
    :goto_0
    return-void

    .line 315
    :cond_0
    const-string v0, "hslee_web, view, onPageStarted(), url(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 316
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/i;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/i;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    sget-object v1, Lcom/sec/chaton/mobileweb/h;->a:Lcom/sec/chaton/mobileweb/h;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->a(Lcom/sec/chaton/mobileweb/h;)V

    .line 318
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/mobileweb/n;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 335
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/i;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    :goto_0
    return-void

    .line 339
    :cond_0
    const-string v0, "hslee_web, view, onReceivedError(), errorCode(%d), description(%s), failingUrl(%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 340
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/i;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/i;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    sget-object v1, Lcom/sec/chaton/mobileweb/h;->a:Lcom/sec/chaton/mobileweb/h;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->b(Lcom/sec/chaton/mobileweb/h;)V

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/i;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->e()V

    .line 343
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/chaton/mobileweb/n;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 348
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/i;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 366
    :goto_0
    return v0

    .line 352
    :cond_0
    const-string v1, "hslee_web, view, shouldOverrideUrlLoading(), url(%s)"

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 353
    iget-object v2, p0, Lcom/sec/chaton/mobileweb/i;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v1, "[_blank]"

    .line 356
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "[_blank]"

    invoke-virtual {p2, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 357
    const-string v1, "[_blank]"

    const-string v2, ""

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 358
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_1

    .line 359
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hslee_web, view, shouldOverrideUrlLoading, changed url for External url : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/mobileweb/i;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 362
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/i;->a:Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 366
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/mobileweb/n;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
