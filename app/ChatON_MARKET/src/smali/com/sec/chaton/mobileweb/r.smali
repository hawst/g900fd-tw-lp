.class public Lcom/sec/chaton/mobileweb/r;
.super Ljava/lang/Object;
.source "MobileWebBridgeClass.java"


# instance fields
.field public final TAG:Ljava/lang/String;

.field final lockObject:Ljava/lang/Object;

.field mHandler:Landroid/os/Handler;

.field mHandshakeIndexMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/mobileweb/o;",
            ">;"
        }
    .end annotation
.end field

.field mHandshakeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/mobileweb/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/os/Handler;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/mobileweb/o;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-class v0, Lcom/sec/chaton/mobileweb/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/mobileweb/r;->TAG:Ljava/lang/String;

    .line 17
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/mobileweb/r;->lockObject:Ljava/lang/Object;

    .line 30
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 31
    const-string v0, "MobileWebBridgeClass has been created !! "

    iget-object v1, p0, Lcom/sec/chaton/mobileweb/r;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/mobileweb/r;->mHandler:Landroid/os/Handler;

    .line 34
    iput-object p2, p0, Lcom/sec/chaton/mobileweb/r;->mHandshakeList:Ljava/util/List;

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/mobileweb/r;->mHandshakeIndexMap:Ljava/util/Map;

    .line 36
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/mobileweb/o;

    .line 37
    iget-object v2, p0, Lcom/sec/chaton/mobileweb/r;->mHandshakeIndexMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/o;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 39
    :cond_1
    return-void
.end method

.method public static buildMobileWebBridgeClass(Landroid/os/Handler;Ljava/util/List;)Lcom/sec/chaton/mobileweb/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/mobileweb/o;",
            ">;)",
            "Lcom/sec/chaton/mobileweb/r;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lcom/sec/chaton/mobileweb/r;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/mobileweb/r;-><init>(Landroid/os/Handler;Ljava/util/List;)V

    return-object v0
.end method

.method private sendMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 48
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/r;->lockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 49
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "%s() is called with arg(%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/mobileweb/r;->TAG:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/r;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/mobileweb/r;->mHandshakeIndexMap:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 54
    :cond_1
    monitor-exit v1

    .line 69
    :goto_0
    return-void

    .line 57
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/r;->mHandshakeIndexMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/mobileweb/o;

    .line 58
    if-nez v0, :cond_3

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "I didn\'t request to use this api : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/mobileweb/r;->TAG:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    monitor-exit v1

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 63
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/sec/chaton/mobileweb/r;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 64
    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/o;->a()I

    move-result v0

    iput v0, v2, Landroid/os/Message;->what:I

    .line 65
    iput-object p2, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 66
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 68
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public badgeclear(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 95
    const-string v0, "badgeclear"

    invoke-direct {p0, v0, p1}, Lcom/sec/chaton/mobileweb/r;->sendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method public gopage(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 83
    const-string v0, "gopage"

    invoke-direct {p0, v0, p1}, Lcom/sec/chaton/mobileweb/r;->sendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public handshake(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 73
    const-string v0, "handshake"

    invoke-direct {p0, v0, p1}, Lcom/sec/chaton/mobileweb/r;->sendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public proxy(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 78
    const-string v0, "proxy"

    invoke-direct {p0, v0, p1}, Lcom/sec/chaton/mobileweb/r;->sendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    .line 42
    iget-object v1, p0, Lcom/sec/chaton/mobileweb/r;->lockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 43
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/sec/chaton/mobileweb/r;->mHandler:Landroid/os/Handler;

    .line 44
    monitor-exit v1

    .line 45
    return-void

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public share(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 89
    const-string v0, "share"

    invoke-direct {p0, v0, p1}, Lcom/sec/chaton/mobileweb/r;->sendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method
