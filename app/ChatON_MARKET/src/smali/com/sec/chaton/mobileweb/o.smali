.class public final enum Lcom/sec/chaton/mobileweb/o;
.super Ljava/lang/Enum;
.source "MobileWebApi.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/mobileweb/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/mobileweb/o;

.field public static final enum b:Lcom/sec/chaton/mobileweb/o;

.field public static final enum c:Lcom/sec/chaton/mobileweb/o;

.field public static final enum d:Lcom/sec/chaton/mobileweb/o;

.field public static final enum e:Lcom/sec/chaton/mobileweb/o;

.field public static final enum f:Lcom/sec/chaton/mobileweb/o;

.field public static final enum g:Lcom/sec/chaton/mobileweb/o;

.field private static final synthetic k:[Lcom/sec/chaton/mobileweb/o;


# instance fields
.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/sec/chaton/mobileweb/o;

    const-string v1, "HANDSHAKE_V1"

    const/16 v3, 0x65

    const-string v4, "handshake"

    const-string v5, "v1"

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/mobileweb/o;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/mobileweb/o;->a:Lcom/sec/chaton/mobileweb/o;

    .line 5
    new-instance v3, Lcom/sec/chaton/mobileweb/o;

    const-string v4, "HANDSHAKE_V2"

    const/16 v6, 0x66

    const-string v7, "handshake"

    const-string v8, "v2"

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/mobileweb/o;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sec/chaton/mobileweb/o;->b:Lcom/sec/chaton/mobileweb/o;

    .line 7
    new-instance v3, Lcom/sec/chaton/mobileweb/o;

    const-string v4, "PROXY_V2"

    const/16 v6, 0xca

    const-string v7, "proxy"

    const-string v8, "v2"

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/mobileweb/o;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sec/chaton/mobileweb/o;->c:Lcom/sec/chaton/mobileweb/o;

    .line 8
    new-instance v3, Lcom/sec/chaton/mobileweb/o;

    const-string v4, "GOPAGE_V1"

    const/16 v6, 0x12d

    const-string v7, "gopage"

    const-string v8, "v1"

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/mobileweb/o;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sec/chaton/mobileweb/o;->d:Lcom/sec/chaton/mobileweb/o;

    .line 9
    new-instance v3, Lcom/sec/chaton/mobileweb/o;

    const-string v4, "GOPAGE_V2"

    const/16 v6, 0x12e

    const-string v7, "gopage"

    const-string v8, "v2"

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/mobileweb/o;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sec/chaton/mobileweb/o;->e:Lcom/sec/chaton/mobileweb/o;

    .line 11
    new-instance v3, Lcom/sec/chaton/mobileweb/o;

    const-string v4, "SHARE_V2"

    const/4 v5, 0x5

    const/16 v6, 0x192

    const-string v7, "share"

    const-string v8, "v2"

    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/mobileweb/o;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sec/chaton/mobileweb/o;->f:Lcom/sec/chaton/mobileweb/o;

    .line 12
    new-instance v3, Lcom/sec/chaton/mobileweb/o;

    const-string v4, "BADGECLEAR_V1"

    const/4 v5, 0x6

    const/16 v6, 0x1f5

    const-string v7, "badgeclear"

    const-string v8, "v1"

    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/mobileweb/o;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sec/chaton/mobileweb/o;->g:Lcom/sec/chaton/mobileweb/o;

    .line 3
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/chaton/mobileweb/o;

    sget-object v1, Lcom/sec/chaton/mobileweb/o;->a:Lcom/sec/chaton/mobileweb/o;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/mobileweb/o;->b:Lcom/sec/chaton/mobileweb/o;

    aput-object v1, v0, v9

    sget-object v1, Lcom/sec/chaton/mobileweb/o;->c:Lcom/sec/chaton/mobileweb/o;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sec/chaton/mobileweb/o;->d:Lcom/sec/chaton/mobileweb/o;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sec/chaton/mobileweb/o;->e:Lcom/sec/chaton/mobileweb/o;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/mobileweb/o;->f:Lcom/sec/chaton/mobileweb/o;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/mobileweb/o;->g:Lcom/sec/chaton/mobileweb/o;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/mobileweb/o;->k:[Lcom/sec/chaton/mobileweb/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput p3, p0, Lcom/sec/chaton/mobileweb/o;->h:I

    .line 35
    iput-object p4, p0, Lcom/sec/chaton/mobileweb/o;->i:Ljava/lang/String;

    .line 36
    iput-object p5, p0, Lcom/sec/chaton/mobileweb/o;->j:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/mobileweb/o;
    .locals 1

    .prologue
    .line 16
    sparse-switch p0, :sswitch_data_0

    .line 26
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 17
    :sswitch_0
    sget-object v0, Lcom/sec/chaton/mobileweb/o;->a:Lcom/sec/chaton/mobileweb/o;

    goto :goto_0

    .line 18
    :sswitch_1
    sget-object v0, Lcom/sec/chaton/mobileweb/o;->b:Lcom/sec/chaton/mobileweb/o;

    goto :goto_0

    .line 19
    :sswitch_2
    sget-object v0, Lcom/sec/chaton/mobileweb/o;->c:Lcom/sec/chaton/mobileweb/o;

    goto :goto_0

    .line 20
    :sswitch_3
    sget-object v0, Lcom/sec/chaton/mobileweb/o;->d:Lcom/sec/chaton/mobileweb/o;

    goto :goto_0

    .line 21
    :sswitch_4
    sget-object v0, Lcom/sec/chaton/mobileweb/o;->e:Lcom/sec/chaton/mobileweb/o;

    goto :goto_0

    .line 22
    :sswitch_5
    sget-object v0, Lcom/sec/chaton/mobileweb/o;->f:Lcom/sec/chaton/mobileweb/o;

    goto :goto_0

    .line 23
    :sswitch_6
    sget-object v0, Lcom/sec/chaton/mobileweb/o;->g:Lcom/sec/chaton/mobileweb/o;

    goto :goto_0

    .line 16
    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x66 -> :sswitch_1
        0xca -> :sswitch_2
        0x12d -> :sswitch_3
        0x12e -> :sswitch_4
        0x192 -> :sswitch_5
        0x1f5 -> :sswitch_6
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/mobileweb/o;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/sec/chaton/mobileweb/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/mobileweb/o;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/mobileweb/o;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/chaton/mobileweb/o;->k:[Lcom/sec/chaton/mobileweb/o;

    invoke-virtual {v0}, [Lcom/sec/chaton/mobileweb/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/mobileweb/o;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/chaton/mobileweb/o;->h:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/o;->i:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/mobileweb/o;->j:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/o;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/o;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
