.class public final enum Lcom/sec/chaton/mobileweb/u;
.super Ljava/lang/Enum;
.source "MobileWebServerType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/mobileweb/u;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/mobileweb/u;

.field private static final synthetic c:[Lcom/sec/chaton/mobileweb/u;


# instance fields
.field b:Lcom/sec/chaton/util/cg;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 7
    new-instance v0, Lcom/sec/chaton/mobileweb/u;

    const-string v1, "LIVE_SERVER"

    sget-object v2, Lcom/sec/chaton/util/cg;->f:Lcom/sec/chaton/util/cg;

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/mobileweb/u;-><init>(Ljava/lang/String;ILcom/sec/chaton/util/cg;)V

    sput-object v0, Lcom/sec/chaton/mobileweb/u;->a:Lcom/sec/chaton/mobileweb/u;

    .line 6
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/chaton/mobileweb/u;

    sget-object v1, Lcom/sec/chaton/mobileweb/u;->a:Lcom/sec/chaton/mobileweb/u;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/mobileweb/u;->c:[Lcom/sec/chaton/mobileweb/u;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/sec/chaton/util/cg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/util/cg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput-object p3, p0, Lcom/sec/chaton/mobileweb/u;->b:Lcom/sec/chaton/util/cg;

    .line 15
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/mobileweb/u;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lcom/sec/chaton/mobileweb/u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/mobileweb/u;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/mobileweb/u;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/sec/chaton/mobileweb/u;->c:[Lcom/sec/chaton/mobileweb/u;

    invoke-virtual {v0}, [Lcom/sec/chaton/mobileweb/u;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/mobileweb/u;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lcom/sec/chaton/mobileweb/v;->a:[I

    invoke-virtual {p0}, Lcom/sec/chaton/mobileweb/u;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 25
    const-string v0, ""

    :goto_0
    return-object v0

    .line 20
    :pswitch_0
    invoke-static {}, Lcom/sec/chaton/mobileweb/s;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 18
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
