.class public Lcom/sec/chaton/samsungaccount/j;
.super Landroid/webkit/WebViewClient;
.source "FragmentSamsungAccountLogin.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)V
    .locals 1

    .prologue
    .line 579
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 594
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/samsungaccount/j;->b:Z

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 849
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 850
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 851
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 853
    :cond_0
    const-string v0, "onPageFinished pd dismissed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    :cond_1
    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 746
    const-string v0, "onPageFinished..."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/j;->b:Z

    if-nez v0, :cond_c

    .line 752
    invoke-static {p2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 754
    const-string v1, "utf-8"

    invoke-static {v0, v1}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 758
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[HoneyComb] params : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " url : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 761
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 762
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 777
    :cond_0
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Name :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 763
    :cond_1
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code_expires_in"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 764
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 765
    :cond_2
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 766
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 767
    :cond_3
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error_code"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 768
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 769
    :cond_4
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code_expires_in"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 770
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 771
    :cond_5
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error_description"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 772
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 773
    :cond_6
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "close"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 774
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Z)Z

    goto/16 :goto_1

    .line 781
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_9

    .line 782
    invoke-virtual {p1, v5}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 783
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->g(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Lcom/sec/chaton/d/at;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "fs24s8z0hh"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    :cond_8
    :goto_2
    return-void

    .line 784
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_a

    .line 785
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/j;->a()V

    .line 786
    invoke-virtual {p1, v5}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 787
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/samsungaccount/l;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/l;-><init>(Lcom/sec/chaton/samsungaccount/j;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_2

    .line 793
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->i(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 794
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0, v4}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Z)Z

    .line 796
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/j;->a()V

    .line 797
    invoke-virtual {p1, v5}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 811
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->j(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)V

    goto :goto_2

    .line 813
    :cond_b
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/j;->a()V

    .line 814
    const-string v0, "url has no regular data"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 818
    :cond_c
    iput-boolean v4, p0, Lcom/sec/chaton/samsungaccount/j;->b:Z

    .line 820
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 821
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_d

    .line 822
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 824
    :cond_d
    const-string v0, "onPageFinished pd dismissed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 840
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 841
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPageStarted... url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v3}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 846
    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 860
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 861
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 863
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " failingUrl = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " desctiption = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/samsungaccount/m;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/m;-><init>(Lcom/sec/chaton/samsungaccount/j;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 875
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 599
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "shouldOverrideUrlLoading, url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    iput-boolean v5, p0, Lcom/sec/chaton/samsungaccount/j;->b:Z

    .line 603
    invoke-static {p2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 605
    const-string v1, "utf-8"

    invoke-static {v0, v1}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 609
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "params :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 612
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 613
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 628
    :cond_0
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Name :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 614
    :cond_1
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code_expires_in"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 615
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 616
    :cond_2
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 617
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 618
    :cond_3
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error_code"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 619
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 620
    :cond_4
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code_expires_in"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 621
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 622
    :cond_5
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error_description"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 623
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 624
    :cond_6
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "close"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 625
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v2, v5}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Z)Z

    goto/16 :goto_1

    .line 632
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_8

    .line 633
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->g(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Lcom/sec/chaton/d/at;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "fs24s8z0hh"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    :goto_2
    return v5

    .line 634
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_9

    .line 635
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/samsungaccount/k;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/k;-><init>(Lcom/sec/chaton/samsungaccount/j;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_2

    .line 643
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->i(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 644
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0, v4}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Z)Z

    .line 645
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/j;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->j(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)V

    goto :goto_2

    .line 647
    :cond_a
    iput-boolean v4, p0, Lcom/sec/chaton/samsungaccount/j;->b:Z

    .line 648
    const-string v0, "url has no regular data"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_2
.end method
