.class public Lcom/sec/chaton/samsungaccount/g;
.super Landroid/webkit/WebChromeClient;
.source "FragmentSamsungAccountLogin.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/g;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 5

    .prologue
    .line 203
    const-string v0, "onJsAlert"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 208
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const/high16 v2, -0x1000000

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 211
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/g;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 212
    const v2, 0x7f0b0007

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 213
    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 214
    const v0, 0x104000a

    new-instance v2, Lcom/sec/chaton/samsungaccount/h;

    invoke-direct {v2, p0, p4}, Lcom/sec/chaton/samsungaccount/h;-><init>(Lcom/sec/chaton/samsungaccount/g;Landroid/webkit/JsResult;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 221
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 222
    new-instance v1, Lcom/sec/chaton/samsungaccount/i;

    invoke-direct {v1, p0, p4}, Lcom/sec/chaton/samsungaccount/i;-><init>(Lcom/sec/chaton/samsungaccount/g;Landroid/webkit/JsResult;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 229
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 232
    const/4 v0, 0x1

    return v0
.end method

.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/g;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    mul-int/lit8 v1, p2, 0x64

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setProgress(I)V

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    return-void
.end method
