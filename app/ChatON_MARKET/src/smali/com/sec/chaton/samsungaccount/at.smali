.class Lcom/sec/chaton/samsungaccount/at;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 505
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 534
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    .prologue
    .line 530
    return-void
.end method

.method public onPageSelected(I)V
    .locals 4

    .prologue
    const v3, 0x7f0201c2

    const v2, 0x7f0201c1

    .line 510
    if-nez p1, :cond_1

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->h(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 512
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->i(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 513
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->j(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 523
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Flick count "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->e(Lcom/sec/chaton/samsungaccount/MainActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->l(Lcom/sec/chaton/samsungaccount/MainActivity;)I

    .line 525
    return-void

    .line 514
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 515
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->h(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 516
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->i(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 517
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->j(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 518
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->h(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 520
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->i(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 521
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->j(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/at;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method
