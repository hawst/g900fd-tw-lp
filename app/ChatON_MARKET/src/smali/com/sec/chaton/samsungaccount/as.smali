.class Lcom/sec/chaton/samsungaccount/as;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 461
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->d(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->e(Lcom/sec/chaton/samsungaccount/MainActivity;)I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 464
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/AdminMenu;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 465
    const-string v1, "mapping_mode"

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->g(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 466
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1, v0, v5}, Lcom/sec/chaton/samsungaccount/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 467
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0, v4}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;I)I

    .line 470
    :cond_0
    invoke-static {v3}, Lcom/sec/chaton/util/y;->a(I)V

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const-string v1, "Log On"

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 472
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 491
    :cond_1
    :goto_0
    return-void

    .line 476
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->d(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 478
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->e(Lcom/sec/chaton/samsungaccount/MainActivity;)I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 479
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/AdminMenu;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 480
    const-string v1, "mapping_mode"

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->g(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 481
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1, v0, v5}, Lcom/sec/chaton/samsungaccount/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0, v4}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;I)I

    .line 485
    :cond_3
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/chaton/util/y;->a(I)V

    .line 486
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const-string v1, "Log On With Save"

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 488
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/as;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    goto :goto_0
.end method
