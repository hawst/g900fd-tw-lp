.class Lcom/sec/chaton/samsungaccount/bd;
.super Landroid/os/Handler;
.source "SamsungAccountLoginDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/ba;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/ba;)V
    .locals 0

    .prologue
    .line 407
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const v6, 0x7f0b016c

    const v5, 0x7f0b0039

    const v4, 0x7f0b002a

    const v3, 0x7f0b0024

    .line 410
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 411
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d3

    if-ne v1, v2, :cond_5

    .line 412
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 413
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 415
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_4

    .line 417
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;

    .line 418
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "userInfo : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->loginID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->countryCode:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 422
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    .line 502
    :cond_2
    :goto_0
    return-void

    .line 424
    :cond_3
    const-string v1, "samsung_account_email"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->loginID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    const-string v1, "samsung_account_faimly_name"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->familyName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    const-string v1, "samsung_account_given_name"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->givenName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->b(Lcom/sec/chaton/samsungaccount/ba;)Landroid/webkit/WebView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 429
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/ba;->dismiss()V

    .line 430
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->h(Lcom/sec/chaton/samsungaccount/ba;)Lcom/sec/chaton/samsungaccount/bi;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->countryCode:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/sec/chaton/samsungaccount/bi;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 433
    :cond_4
    const-string v0, "fail to get to user information"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/samsungaccount/bf;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/bf;-><init>(Lcom/sec/chaton/samsungaccount/bd;)V

    invoke-virtual {v0, v3, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/samsungaccount/be;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/be;-><init>(Lcom/sec/chaton/samsungaccount/bd;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 454
    :cond_5
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d2

    if-ne v1, v2, :cond_2

    .line 456
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_9

    .line 457
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;

    .line 458
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "accessToken : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->access_token:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->userId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 462
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 465
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0

    .line 468
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->access_token:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/samsungaccount/ba;->a(Lcom/sec/chaton/samsungaccount/ba;Ljava/lang/String;)Ljava/lang/String;

    .line 469
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->userId:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/samsungaccount/ba;->b(Lcom/sec/chaton/samsungaccount/ba;Ljava/lang/String;)Ljava/lang/String;

    .line 470
    const-string v0, "samsung_account_token"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->i(Lcom/sec/chaton/samsungaccount/ba;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->a(Lcom/sec/chaton/samsungaccount/ba;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->i(Lcom/sec/chaton/samsungaccount/ba;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fs24s8z0hh"

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v3}, Lcom/sec/chaton/samsungaccount/ba;->j(Lcom/sec/chaton/samsungaccount/ba;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/at;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 475
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 478
    :cond_a
    const-string v0, "fail to get to access token"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bd;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/samsungaccount/bh;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/bh;-><init>(Lcom/sec/chaton/samsungaccount/bd;)V

    invoke-virtual {v0, v3, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/samsungaccount/bg;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/bg;-><init>(Lcom/sec/chaton/samsungaccount/bd;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0
.end method
