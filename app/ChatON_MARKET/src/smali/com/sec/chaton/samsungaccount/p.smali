.class Lcom/sec/chaton/samsungaccount/p;
.super Landroid/os/Handler;
.source "MainActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 1311
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/16 v7, 0x7d6

    const/4 v5, 0x5

    const v3, 0x7f0b02a9

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 1314
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-ne v0, v4, :cond_1

    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v7, :cond_1

    .line 1715
    :cond_0
    :goto_0
    return-void

    .line 1317
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1320
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x68

    if-ne v1, v2, :cond_5

    .line 1321
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_4

    .line 1328
    const-string v0, "mum_enable_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->t(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1329
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0, v6}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;Z)Z

    .line 1330
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0, v4}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(Lcom/sec/chaton/samsungaccount/MainActivity;Z)V

    goto :goto_0

    .line 1332
    :cond_2
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1333
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->u(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/g;->a(Lcom/sec/chaton/d/a/b;)Lcom/sec/chaton/d/a/z;

    goto :goto_0

    .line 1335
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    goto :goto_0

    .line 1339
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->a:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto :goto_0

    .line 1343
    :cond_5
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x44d

    if-ne v1, v2, :cond_7

    .line 1344
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_6

    .line 1346
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const v1, 0x7f0b03d0

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(Lcom/sec/chaton/samsungaccount/MainActivity;I)V

    .line 1347
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    goto/16 :goto_0

    .line 1350
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->k:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1355
    :cond_7
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x450

    if-ne v1, v2, :cond_e

    .line 1356
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_d

    .line 1358
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 1359
    if-eqz v0, :cond_0

    .line 1362
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "3.3.51"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1383
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_8

    .line 1384
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new version : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1387
    :cond_8
    invoke-static {}, Lcom/sec/chaton/util/cb;->b()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1388
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 1389
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->c()V

    goto/16 :goto_0

    .line 1391
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1392
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1393
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "NO"

    invoke-static {v0, v1, v6, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    goto/16 :goto_0

    .line 1400
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->w(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1401
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->x(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 1403
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->y(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1404
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 1408
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const v1, 0x7f0b03d2

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(Lcom/sec/chaton/samsungaccount/MainActivity;I)V

    .line 1409
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->z(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 1415
    :cond_d
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->e:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1420
    :cond_e
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x452

    if-ne v1, v2, :cond_12

    .line 1421
    const-string v1, "2"

    .line 1422
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/AvaliableApps;

    .line 1424
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_10

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1426
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateAppsReady"

    const-string v2, "YES"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1427
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1428
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->A(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "YES"

    invoke-static {v0, v1, v6, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 1430
    :cond_f
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1431
    const-string v0, "Samsung apps is ready to upgrade chaton"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1434
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1435
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->A(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "NO"

    invoke-static {v0, v1, v6, v2}, Lcom/sec/chaton/util/am;->a(Lcom/sec/chaton/io/entry/GetVersionNotice;Landroid/content/Context;ZLjava/lang/String;)V

    .line 1437
    :cond_11
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1438
    const-string v0, "Samsung apps is NOT ready to upgrade chaton"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1444
    :cond_12
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d3

    if-ne v1, v2, :cond_17

    .line 1446
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_16

    .line 1448
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;

    .line 1450
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "userInfo : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1453
    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->loginID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->countryCode:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 1454
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1456
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->B(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;Z)V

    goto/16 :goto_0

    .line 1460
    :cond_14
    const-string v1, "samsung_account_email"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->loginID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1461
    const-string v1, "samsung_account_faimly_name"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->familyName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1462
    const-string v1, "samsung_account_given_name"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->givenName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1463
    const-string v1, "samsung_account_birthday"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->birthDate:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1474
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->g(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v1

    if-ne v1, v4, :cond_15

    .line 1475
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1477
    iget-object v0, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->countryCode:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 1479
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0290

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->C(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->D(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1480
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0, v6}, Lcom/sec/chaton/samsungaccount/MainActivity;->d(Lcom/sec/chaton/samsungaccount/MainActivity;Z)V

    goto/16 :goto_0

    .line 1484
    :cond_15
    iget-object v0, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->countryCode:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 1486
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->F(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->E(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/util/ar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->a(Landroid/os/Handler;)V

    .line 1487
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->E(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/util/ar;

    move-result-object v0

    const/16 v1, 0x3e9

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ar;->a(II)V

    goto/16 :goto_0

    .line 1493
    :cond_16
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->f:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1496
    :cond_17
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d5

    if-ne v1, v2, :cond_25

    .line 1498
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_19

    :cond_18
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc353

    if-ne v1, v2, :cond_1f

    .line 1500
    :cond_19
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "temp_account_country_code"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1502
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1a

    .line 1503
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Final account ISO : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1505
    :cond_1a
    const-string v1, "did_samsung_account_mapping"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1506
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "account_mapping_fail"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1510
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->g(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v1

    if-ne v1, v4, :cond_0

    .line 1512
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1, v6}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;I)I

    .line 1513
    invoke-static {}, Lcom/sec/chaton/samsungaccount/MainActivity;->b()V

    .line 1514
    invoke-static {}, Lcom/sec/chaton/j/af;->c()Z

    .line 1516
    invoke-static {v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1c

    .line 1517
    invoke-static {}, Lcom/sec/chaton/event/f;->a()V

    .line 1518
    const-string v1, "account_country_code"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1519
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->G(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    .line 1552
    :cond_1b
    :goto_1
    invoke-static {}, Lcom/sec/chaton/j/af;->c()Z

    .line 1554
    invoke-static {}, Lcom/sec/chaton/e/a/n;->a()V

    goto/16 :goto_0

    .line 1521
    :cond_1c
    const-string v1, "account_country_code"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1524
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1527
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0384

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V

    .line 1528
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->s(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/coolots/sso/a/a;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 1529
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->s(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/coolots/sso/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    if-eqz v0, :cond_1b

    .line 1530
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V

    .line 1531
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 1532
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 1533
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto :goto_1

    .line 1536
    :cond_1d
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V

    .line 1537
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 1539
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 1540
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_1

    .line 1543
    :cond_1e
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V

    .line 1544
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 1546
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 1547
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_1

    .line 1561
    :cond_1f
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc356

    if-ne v1, v2, :cond_22

    .line 1563
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_20

    .line 1564
    const-string v0, "SSO token validaion was finished"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1567
    :cond_20
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->H(Lcom/sec/chaton/samsungaccount/MainActivity;)I

    move-result v0

    const/16 v1, 0x32c9

    if-lt v0, v1, :cond_21

    .line 1568
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->I(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 1570
    :cond_21
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1572
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1573
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->z(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 1576
    :cond_22
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc352

    if-ne v1, v2, :cond_23

    .line 1577
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1579
    const-string v0, "aucode was expired"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1580
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1581
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->z(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 1582
    :cond_23
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc354

    if-ne v1, v2, :cond_24

    .line 1583
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1585
    const-string v0, "device over max of limitation"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1586
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03d6

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/samsungaccount/q;

    invoke-direct {v3, p0}, Lcom/sec/chaton/samsungaccount/q;-><init>(Lcom/sec/chaton/samsungaccount/p;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1595
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1596
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->J(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 1599
    :cond_24
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->g:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1602
    :cond_25
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d2

    if-ne v1, v2, :cond_29

    .line 1604
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_28

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_28

    .line 1606
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;

    .line 1608
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "accessToken : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1611
    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->access_token:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_26

    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->userId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 1612
    :cond_26
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1614
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->B(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;Z)V

    goto/16 :goto_0

    .line 1617
    :cond_27
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->access_token:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1618
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->userId:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1620
    const-string v0, "samsung_account_user_id"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->K(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1621
    const-string v0, "samsung_account_token"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->L(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->M(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->L(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fs24s8z0hh"

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->K(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/at;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1628
    :cond_28
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->h:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1631
    :cond_29
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v7, :cond_0

    .line 1632
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2c

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2c

    .line 1633
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2a

    .line 1634
    const-string v0, "Fianlly register was success with SA automatically"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1637
    :cond_2a
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0, v6}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;I)I

    .line 1639
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "temp_account_country_code"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1641
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2b

    .line 1642
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Final account ISO : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1645
    :cond_2b
    const-string v1, "did_samsung_account_mapping"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1646
    invoke-static {}, Lcom/sec/chaton/samsungaccount/MainActivity;->b()V

    .line 1648
    const-string v1, "account_country_code"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1652
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1655
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->N(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 1667
    :cond_2c
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc356

    if-ne v1, v2, :cond_2e

    .line 1668
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2d

    .line 1669
    const-string v0, "SSO token validaion was finished"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1672
    :cond_2d
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1676
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->I(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 1677
    :cond_2e
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xc354

    if-ne v1, v2, :cond_2f

    .line 1679
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1683
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1684
    const-string v0, "device over max of limitation"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03d6

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/samsungaccount/r;

    invoke-direct {v3, p0}, Lcom/sec/chaton/samsungaccount/r;-><init>(Lcom/sec/chaton/samsungaccount/p;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1702
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->J(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/sec/common/a/d;->setCancelable(Z)V

    .line 1703
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1704
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->J(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 1708
    :cond_2f
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1712
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/p;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->i:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
