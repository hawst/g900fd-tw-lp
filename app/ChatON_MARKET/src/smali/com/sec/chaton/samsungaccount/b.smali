.class Lcom/sec/chaton/samsungaccount/b;
.super Landroid/os/Handler;
.source "FragmentSamsungAccountLogin.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const v6, 0x7f0b016c

    const v5, 0x7f0b0039

    const v4, 0x7f0b002a

    const v3, 0x7f0b0024

    .line 392
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 443
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d3

    if-ne v1, v2, :cond_5

    .line 444
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 445
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 447
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_4

    .line 449
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;

    .line 451
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "userInfo : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->loginID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->countryCode:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    .line 576
    :cond_2
    :goto_0
    return-void

    .line 458
    :cond_3
    const-string v1, "samsung_account_email"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->loginID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v1, "samsung_account_faimly_name"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->familyName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v1, "samsung_account_given_name"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->givenName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 472
    const-string v2, "country_code"

    iget-object v0, v0, Lcom/sec/chaton/io/entry/SSOUserInformationEntry;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->d(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 480
    :cond_4
    const-string v0, "fail to get to user information"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 484
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/samsungaccount/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/d;-><init>(Lcom/sec/chaton/samsungaccount/b;)V

    invoke-virtual {v0, v3, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/samsungaccount/c;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/c;-><init>(Lcom/sec/chaton/samsungaccount/b;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 497
    :cond_5
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d2

    if-ne v1, v2, :cond_2

    .line 499
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_9

    .line 501
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;

    .line 503
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "accessToken : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->access_token:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->userId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 507
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 508
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 511
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0

    .line 514
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v2, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->access_token:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Ljava/lang/String;)Ljava/lang/String;

    .line 515
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/SSOAccessTokenEntry;->userId:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Ljava/lang/String;)Ljava/lang/String;

    .line 517
    const-string v0, "samsung_account_token"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->e(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->g(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->e(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fs24s8z0hh"

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v3}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->f(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/at;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 524
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 525
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 527
    :cond_a
    const-string v0, "fail to get to access token"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 531
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/b;->a:Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/samsungaccount/f;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/f;-><init>(Lcom/sec/chaton/samsungaccount/b;)V

    invoke-virtual {v0, v3, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/samsungaccount/e;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/e;-><init>(Lcom/sec/chaton/samsungaccount/b;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0
.end method
