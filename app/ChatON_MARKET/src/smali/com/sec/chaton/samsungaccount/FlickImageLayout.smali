.class public Lcom/sec/chaton/samsungaccount/FlickImageLayout;
.super Landroid/support/v4/app/Fragment;
.source "FlickImageLayout.java"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/samsungaccount/FlickImageLayout;->a:I

    .line 29
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/samsungaccount/FlickImageLayout;->a:I

    .line 32
    iput p1, p0, Lcom/sec/chaton/samsungaccount/FlickImageLayout;->a:I

    .line 33
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 38
    if-eqz p1, :cond_0

    const-string v0, "position_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    const-string v0, "position_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/samsungaccount/FlickImageLayout;->a:I

    .line 41
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f0b02e5

    const v8, 0x7f0203a9

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/FlickImageLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v7, :cond_1

    .line 47
    const v0, 0x7f0300c0

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    .line 54
    :goto_0
    const v0, 0x7f0702e1

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 55
    const v1, 0x7f07035b

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 56
    const v2, 0x7f07035a

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 58
    iget v4, p0, Lcom/sec/chaton/samsungaccount/FlickImageLayout;->a:I

    if-nez v4, :cond_2

    .line 60
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 61
    const v0, 0x7f0b028d

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 62
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(I)V

    .line 63
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    :cond_0
    :goto_1
    return-object v3

    .line 50
    :cond_1
    const v0, 0x7f0300bf

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    .line 65
    :cond_2
    iget v4, p0, Lcom/sec/chaton/samsungaccount/FlickImageLayout;->a:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 67
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 68
    const v0, 0x7f0b028e

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 69
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(I)V

    .line 70
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 72
    :cond_3
    iget v4, p0, Lcom/sec/chaton/samsungaccount/FlickImageLayout;->a:I

    if-ne v4, v7, :cond_0

    .line 75
    const v4, 0x7f0203ab

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 76
    const v0, 0x7f0b02e7

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 77
    const v0, 0x7f0b02e6

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 78
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 87
    const-string v0, "position_data"

    iget v1, p0, Lcom/sec/chaton/samsungaccount/FlickImageLayout;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 88
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 89
    return-void
.end method
