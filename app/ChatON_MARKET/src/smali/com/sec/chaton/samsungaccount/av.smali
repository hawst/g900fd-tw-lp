.class Lcom/sec/chaton/samsungaccount/av;
.super Landroid/os/Handler;
.source "MainActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 1139
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1204
    :goto_0
    return-void

    .line 1145
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1146
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 1169
    :sswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_3

    .line 1172
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->p(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto :goto_0

    .line 1149
    :sswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2

    .line 1150
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V

    .line 1151
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->g(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1152
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 1153
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 1154
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto :goto_0

    .line 1156
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto :goto_0

    .line 1160
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->j:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto :goto_0

    .line 1183
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->b:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto :goto_0

    .line 1189
    :sswitch_2
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1191
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_4

    .line 1194
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->p(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1196
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;)V

    goto/16 :goto_0

    .line 1199
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/av;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->c:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1146
    nop

    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_0
        0x131 -> :sswitch_1
        0x8fd -> :sswitch_2
    .end sparse-switch
.end method
