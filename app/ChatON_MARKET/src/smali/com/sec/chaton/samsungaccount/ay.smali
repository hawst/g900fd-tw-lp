.class public final enum Lcom/sec/chaton/samsungaccount/ay;
.super Ljava/lang/Enum;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/samsungaccount/ay;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/samsungaccount/ay;

.field public static final enum b:Lcom/sec/chaton/samsungaccount/ay;

.field private static final synthetic c:[Lcom/sec/chaton/samsungaccount/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 147
    new-instance v0, Lcom/sec/chaton/samsungaccount/ay;

    const-string v1, "normal"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/samsungaccount/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    .line 148
    new-instance v0, Lcom/sec/chaton/samsungaccount/ay;

    const-string v1, "disable"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/samsungaccount/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/ay;->b:Lcom/sec/chaton/samsungaccount/ay;

    .line 146
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/samsungaccount/ay;

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->b:Lcom/sec/chaton/samsungaccount/ay;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/samsungaccount/ay;->c:[Lcom/sec/chaton/samsungaccount/ay;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/samsungaccount/ay;
    .locals 1

    .prologue
    .line 146
    const-class v0, Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/samsungaccount/ay;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/samsungaccount/ay;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/sec/chaton/samsungaccount/ay;->c:[Lcom/sec/chaton/samsungaccount/ay;

    invoke-virtual {v0}, [Lcom/sec/chaton/samsungaccount/ay;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/samsungaccount/ay;

    return-object v0
.end method
