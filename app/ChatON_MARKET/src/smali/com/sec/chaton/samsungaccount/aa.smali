.class Lcom/sec/chaton/samsungaccount/aa;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 2227
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2232
    const-string v0, "auto_regi_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2233
    sget-boolean v0, Lcom/sec/chaton/global/GlobalApplication;->g:Z

    if-eqz v0, :cond_0

    .line 2326
    :goto_0
    return-void

    .line 2240
    :cond_0
    const-string v0, "version"

    const/4 v1, 0x2

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2242
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const-string v2, "authcode"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->d(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2243
    const-string v1, "auth_token"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2245
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_1

    .line 2246
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " authcode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->X(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " authToken : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2250
    :cond_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2252
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->X(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2253
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->M(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->X(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fs24s8z0hh"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2256
    :cond_2
    const-string v0, "self_update"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2257
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 2259
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->Z(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 2267
    :pswitch_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2268
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const-string v2, "email_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->e(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2269
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const-string v2, "user_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2270
    const-string v0, "mcc"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2271
    const-string v2, "api_server_url"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2272
    const-string v3, "birthday"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2274
    const-string v4, "samsung_account_token"

    invoke-static {v4, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2275
    const-string v1, "samsung_account_email"

    iget-object v4, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v4}, Lcom/sec/chaton/samsungaccount/MainActivity;->aa(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2276
    const-string v1, "samsung_account_user_id"

    iget-object v4, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v4}, Lcom/sec/chaton/samsungaccount/MainActivity;->K(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2277
    const-string v1, "samsung_account_api_server"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2278
    const-string v1, "samsung_account_birthday"

    invoke-static {v1, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2281
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0290

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->C(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v4}, Lcom/sec/chaton/samsungaccount/MainActivity;->D(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2283
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->g(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 2288
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2289
    invoke-static {}, Lcom/sec/chaton/samsungaccount/MainActivity;->c()V

    .line 2290
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0, v5}, Lcom/sec/chaton/samsungaccount/MainActivity;->d(Lcom/sec/chaton/samsungaccount/MainActivity;Z)V

    goto/16 :goto_0

    .line 2293
    :cond_4
    const-string v1, "mum_enable_feature"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->ab(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2294
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->ac(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 2295
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 2298
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2299
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const v1, 0x7f0b03d1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(Lcom/sec/chaton/samsungaccount/MainActivity;I)V

    .line 2300
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->F(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->E(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/util/ar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->a(Landroid/os/Handler;)V

    .line 2301
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->E(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/util/ar;

    move-result-object v0

    const/16 v1, 0x3e9

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ar;->a(II)V

    goto/16 :goto_0

    .line 2307
    :cond_6
    const-string v0, "check_list"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2309
    if-lez v0, :cond_9

    .line 2310
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ad(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ad(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2311
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ad(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2313
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ae(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ae(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2314
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ae(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 2315
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Landroid/view/View;)Landroid/view/View;

    .line 2317
    :cond_8
    invoke-static {p1, p2}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2319
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/aa;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->Z(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 2250
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
