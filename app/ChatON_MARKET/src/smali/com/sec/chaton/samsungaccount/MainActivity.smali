.class public Lcom/sec/chaton/samsungaccount/MainActivity;
.super Lcom/sec/chaton/base/BaseActivity;
.source "MainActivity.java"

# interfaces
.implements Lcom/coolots/sso/a/c;


# static fields
.field public static a:Z


# instance fields
.field private A:Lcom/sec/common/a/d;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Ljava/lang/String;

.field private H:Lcom/sec/chaton/widget/ImageTextViewGroup;

.field private I:Lcom/sec/chaton/widget/ImageTextViewGroup;

.field private J:Landroid/widget/LinearLayout;

.field private K:Landroid/widget/Button;

.field private L:Landroid/widget/TextView;

.field private M:Z

.field private N:Lcom/sec/chaton/d/ap;

.field private O:Ljava/lang/String;

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:Landroid/widget/ImageView;

.field private T:Landroid/widget/ImageView;

.field private U:Landroid/widget/ImageView;

.field private V:I

.field private W:Ljava/lang/String;

.field private X:Landroid/support/v4/view/ViewPager;

.field private Y:Lcom/sec/chaton/samsungaccount/ax;

.field private Z:Landroid/widget/LinearLayout;

.field private aA:Landroid/content/BroadcastReceiver;

.field private aB:Landroid/os/Handler;

.field private aC:Lcom/sec/chaton/samsungaccount/bi;

.field private aD:Lcom/sec/chaton/util/ar;

.field private aa:Landroid/widget/LinearLayout;

.field private ab:Landroid/widget/LinearLayout;

.field private ac:Landroid/widget/LinearLayout;

.field private ad:I

.field private ae:I

.field private af:Landroid/widget/EditText;

.field private ag:I

.field private ah:I

.field private ai:Landroid/view/View;

.field private aj:Lcom/sec/common/a/d;

.field private ak:Z

.field private al:Lcom/sec/common/a/d;

.field private am:Lcom/coolots/sso/a/a;

.field private an:Ljava/lang/String;

.field private ao:Ljava/lang/String;

.field private ap:Z

.field private aq:Landroid/widget/TextView;

.field private ar:Z

.field private as:Landroid/view/View;

.field private at:Lcom/sec/chaton/io/entry/GetVersionNotice;

.field private au:Landroid/view/View$OnClickListener;

.field private av:Landroid/os/Handler;

.field private aw:Landroid/os/Handler;

.field private ax:Landroid/os/Handler;

.field private ay:Landroid/content/BroadcastReceiver;

.field private az:Landroid/content/BroadcastReceiver;

.field b:Landroid/os/Handler;

.field c:Landroid/os/Handler;

.field private d:Ljava/lang/String;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private j:Lcom/sec/chaton/samsungaccount/ba;

.field private k:Landroid/app/ProgressDialog;

.field private l:Landroid/content/Context;

.field private m:Ljava/lang/String;

.field private n:Lcom/sec/chaton/d/at;

.field private o:Lcom/sec/chaton/d/l;

.field private p:Lcom/sec/chaton/chat/fh;

.field private q:Lcom/sec/chaton/d/h;

.field private r:Lcom/sec/chaton/d/bj;

.field private s:Lcom/sec/chaton/d/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/chaton/d/a",
            "<*>;"
        }
    .end annotation
.end field

.field private t:Lcom/sec/chaton/d/g;

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Landroid/os/Bundle;

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/samsungaccount/MainActivity;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 133
    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->e:I

    .line 134
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->f:I

    .line 135
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->g:I

    .line 137
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->h:I

    .line 138
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->i:I

    .line 142
    iput-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->j:Lcom/sec/chaton/samsungaccount/ba;

    .line 165
    iput-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    .line 231
    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ad:I

    .line 234
    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ag:I

    .line 235
    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ah:I

    .line 242
    iput-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->am:Lcom/coolots/sso/a/a;

    .line 252
    iput-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    .line 256
    iput-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ar:Z

    .line 257
    iput-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    .line 1048
    new-instance v0, Lcom/sec/chaton/samsungaccount/au;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/au;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->au:Landroid/view/View$OnClickListener;

    .line 1139
    new-instance v0, Lcom/sec/chaton/samsungaccount/av;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/av;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->av:Landroid/os/Handler;

    .line 1244
    new-instance v0, Lcom/sec/chaton/samsungaccount/aw;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/samsungaccount/aw;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->b:Landroid/os/Handler;

    .line 1265
    new-instance v0, Lcom/sec/chaton/samsungaccount/o;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/o;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aw:Landroid/os/Handler;

    .line 1311
    new-instance v0, Lcom/sec/chaton/samsungaccount/p;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/p;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ax:Landroid/os/Handler;

    .line 1720
    new-instance v0, Lcom/sec/chaton/samsungaccount/s;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/s;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->c:Landroid/os/Handler;

    .line 2227
    new-instance v0, Lcom/sec/chaton/samsungaccount/aa;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/aa;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ay:Landroid/content/BroadcastReceiver;

    .line 2507
    new-instance v0, Lcom/sec/chaton/samsungaccount/ab;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/ab;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->az:Landroid/content/BroadcastReceiver;

    .line 2540
    new-instance v0, Lcom/sec/chaton/samsungaccount/ac;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/ac;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aA:Landroid/content/BroadcastReceiver;

    .line 2954
    new-instance v0, Lcom/sec/chaton/samsungaccount/ag;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/ag;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aB:Landroid/os/Handler;

    .line 3058
    new-instance v0, Lcom/sec/chaton/samsungaccount/aj;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/aj;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aC:Lcom/sec/chaton/samsungaccount/bi;

    .line 3080
    new-instance v0, Lcom/sec/chaton/samsungaccount/ak;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/ak;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aD:Lcom/sec/chaton/util/ar;

    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->at:Lcom/sec/chaton/io/entry/GetVersionNotice;

    return-object v0
.end method

.method static synthetic B(Lcom/sec/chaton/samsungaccount/MainActivity;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    return v0
.end method

.method static synthetic C(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic D(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->C:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic E(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/util/ar;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aD:Lcom/sec/chaton/util/ar;

    return-object v0
.end method

.method static synthetic F(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/a;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->s:Lcom/sec/chaton/d/a;

    return-object v0
.end method

.method static synthetic G(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aw:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic H(Lcom/sec/chaton/samsungaccount/MainActivity;)I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    return v0
.end method

.method static synthetic I(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->l()V

    return-void
.end method

.method static synthetic J(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic K(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ao:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic L(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->w:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic M(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/at;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->n:Lcom/sec/chaton/d/at;

    return-object v0
.end method

.method static synthetic N(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->w()V

    return-void
.end method

.method static synthetic O(Lcom/sec/chaton/samsungaccount/MainActivity;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->u:Z

    return v0
.end method

.method static synthetic P(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic Q(Lcom/sec/chaton/samsungaccount/MainActivity;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->Q:Z

    return v0
.end method

.method static synthetic R(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->G:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic S(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->h()V

    return-void
.end method

.method static synthetic T(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ab:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic U(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ac:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic V(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->u()V

    return-void
.end method

.method static synthetic W(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/l;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->o:Lcom/sec/chaton/d/l;

    return-object v0
.end method

.method static synthetic X(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic Y(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/chat/fh;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->p:Lcom/sec/chaton/chat/fh;

    return-object v0
.end method

.method static synthetic Z(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->s()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/MainActivity;I)I
    .locals 0

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ae:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/MainActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->at:Lcom/sec/chaton/io/entry/GetVersionNotice;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V
    .locals 6

    .prologue
    const v5, 0x7f0b0092

    .line 2834
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2835
    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    if-ne p1, v1, :cond_1

    .line 2836
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2841
    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b016c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/samsungaccount/af;

    invoke-direct {v2}, Lcom/sec/chaton/samsungaccount/af;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 2847
    return-void

    .line 2837
    :cond_1
    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->b:Lcom/sec/chaton/samsungaccount/ay;

    if-ne p1, v1, :cond_0

    .line 2838
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b029d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;Z)V
    .locals 6

    .prologue
    .line 2804
    const-string v0, ""

    .line 2806
    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    if-ne p2, v1, :cond_2

    .line 2807
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2813
    :cond_0
    :goto_0
    if-eqz p3, :cond_3

    .line 2814
    invoke-static {p1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b016c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/samsungaccount/ad;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/ad;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    .line 2824
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ar:Z

    if-nez v0, :cond_1

    .line 2825
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 2830
    :cond_1
    :goto_1
    return-void

    .line 2808
    :cond_2
    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->b:Lcom/sec/chaton/samsungaccount/ay;

    if-ne p2, v1, :cond_0

    .line 2809
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b029d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0092

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2828
    :cond_3
    invoke-static {p1, p2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto :goto_1
.end method

.method private a(Landroid/content/res/Configuration;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 770
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    if-eqz v0, :cond_5

    .line 771
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 772
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 774
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 775
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "densityDpi : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Config : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    :cond_0
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 778
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 780
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v6, :cond_3

    .line 781
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    if-ne v5, v1, :cond_2

    .line 782
    invoke-virtual {p0, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->setRequestedOrientation(I)V

    .line 822
    :cond_1
    :goto_0
    return-void

    .line 783
    :cond_2
    const/4 v1, 0x3

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    if-ne v1, v0, :cond_1

    .line 784
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 787
    :cond_3
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    if-nez v1, :cond_4

    .line 788
    invoke-virtual {p0, v5}, Lcom/sec/chaton/samsungaccount/MainActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 789
    :cond_4
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    if-ne v6, v0, :cond_1

    .line 790
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 794
    :cond_5
    const v0, 0x7f070244

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aa:Landroid/widget/LinearLayout;

    .line 795
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v6, :cond_7

    .line 796
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_6

    .line 797
    const-string v0, "It is Landscape Mode"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    :cond_6
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 801
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ab:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 802
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ac:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 803
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aa:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 805
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->Z:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 809
    :cond_7
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_8

    .line 810
    const-string v0, "It is Portrait Mode"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    :cond_8
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 814
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ab:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 815
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ac:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 816
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aa:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 818
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->Z:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->r()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/MainActivity;Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;Z)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0b016c

    .line 1911
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    .line 1913
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1914
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "network error, type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/samsungaccount/az;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " errorCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1917
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1996
    :cond_1
    :goto_0
    return-void

    .line 1921
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0024

    new-instance v2, Lcom/sec/chaton/samsungaccount/y;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/samsungaccount/y;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/samsungaccount/x;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/samsungaccount/x;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    .line 1993
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ar:Z

    if-nez v0, :cond_1

    .line 1994
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1120
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    const v1, 0x7f070369

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1134
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(Ljava/lang/String;)V

    .line 1135
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1083
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ar:Z

    if-eqz v0, :cond_1

    .line 1112
    :cond_0
    :goto_0
    return-void

    .line 1087
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    if-nez v0, :cond_0

    .line 1093
    iput-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    .line 1094
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    .line 1096
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    const v1, 0x7f07036a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1097
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    const v2, 0x7f070369

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1099
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1100
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1102
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    .line 1108
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    invoke-interface {v0, v3}, Lcom/sec/common/a/d;->setCancelable(Z)V

    .line 1110
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 976
    .line 979
    if-eqz p1, :cond_1

    .line 980
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 981
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 982
    iput-boolean v4, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->Q:Z

    .line 994
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 995
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f()V

    .line 1016
    :goto_1
    return-void

    .line 985
    :cond_0
    iput-boolean v3, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->Q:Z

    goto :goto_0

    .line 989
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 990
    iput-boolean v3, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->Q:Z

    goto :goto_0

    .line 999
    :cond_2
    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->G:Ljava/lang/String;

    .line 1002
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Samsung email : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1004
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->u()V

    .line 1006
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "primary_contact_addrss"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1007
    iput-boolean v4, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->P:Z

    .line 1008
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->o:Lcom/sec/chaton/d/l;

    invoke-virtual {v0}, Lcom/sec/chaton/d/l;->a()V

    goto :goto_1

    .line 1011
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->c:Landroid/os/Handler;

    invoke-static {v1}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->N:Lcom/sec/chaton/d/ap;

    .line 1013
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1014
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->N:Lcom/sec/chaton/d/ap;

    invoke-virtual {v2, v1, v0}, Lcom/sec/chaton/d/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 704
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 740
    :cond_0
    :goto_0
    return v0

    .line 707
    :pswitch_0
    iget v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ag:I

    if-ge v2, v3, :cond_1

    .line 708
    iget v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ag:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ag:I

    .line 713
    :goto_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 714
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "volume up : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " volume down : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ah:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 710
    :cond_1
    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ag:I

    goto :goto_1

    .line 719
    :pswitch_1
    iget v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ag:I

    if-ne v2, v3, :cond_2

    .line 720
    iget v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ah:I

    if-ge v2, v3, :cond_3

    .line 721
    iget v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ah:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ah:I

    .line 735
    :cond_2
    :goto_2
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 736
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "volume up : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " volume down : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ah:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 722
    :cond_3
    iget v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ah:I

    if-ne v2, v3, :cond_5

    .line 723
    iget-boolean v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ar:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aj:Lcom/sec/common/a/d;

    if-eqz v2, :cond_4

    .line 724
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aj:Lcom/sec/common/a/d;

    invoke-interface {v2, v1}, Lcom/sec/common/a/d;->setCancelable(Z)V

    .line 725
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aj:Lcom/sec/common/a/d;

    invoke-interface {v2}, Lcom/sec/common/a/d;->show()V

    .line 727
    :cond_4
    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ah:I

    .line 728
    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ag:I

    goto :goto_2

    .line 731
    :cond_5
    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ah:I

    goto :goto_2

    .line 704
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1871
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    .line 1874
    :try_start_0
    const-string v0, "content://com.sec.spp.provider/version_info/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1876
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1878
    if-eqz v1, :cond_2

    .line 1879
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1880
    const-string v0, "current_version"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1881
    const-string v0, "latest_version"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1882
    const-string v0, "need_update"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1884
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v4, :cond_0

    .line 1885
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SPP update status] currentVer : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " latestVer : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " needUpdate : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1888
    :cond_0
    const-string v2, "spp_update_is"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1889
    const-string v2, "spp_latest_ver"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1890
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1905
    :goto_0
    return v0

    .line 1893
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v6

    .line 1894
    goto :goto_0

    .line 1898
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 1899
    const-string v0, "cursor is null"

    invoke-static {v0, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    move v0, v6

    .line 1901
    goto :goto_0

    .line 1903
    :catch_0
    move-exception v0

    .line 1904
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 1905
    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/MainActivity;Z)Z
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->z:Z

    return p1
.end method

.method static synthetic aa(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->an:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ab(Lcom/sec/chaton/samsungaccount/MainActivity;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->R:Z

    return v0
.end method

.method static synthetic ac(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->g()V

    return-void
.end method

.method static synthetic ad(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic ae(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->w:Ljava/lang/String;

    return-object p1
.end method

.method public static b()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2853
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 2854
    const-string v1, "com.sec.chaton"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-gtz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2855
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2856
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "samsung_account_email"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2857
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2860
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-ne v3, v5, :cond_1

    .line 2861
    const/4 v0, 0x0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2870
    :cond_0
    :goto_0
    return-void

    .line 2865
    :cond_1
    invoke-static {v0, v2, v5}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 3205
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    if-nez v0, :cond_1

    .line 3211
    :cond_0
    :goto_0
    return-void

    .line 3208
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aq:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 3209
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/samsungaccount/MainActivity;I)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(I)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/samsungaccount/MainActivity;Z)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Z)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3214
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    if-nez v0, :cond_1

    .line 3220
    :cond_0
    :goto_0
    return-void

    .line 3217
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aq:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 3218
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 5

    .prologue
    .line 2070
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2071
    const-string v0, "runNewSSO"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2073
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2074
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "email_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mcc"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "api_server_url"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "birthday"

    aput-object v2, v0, v1

    .line 2075
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2076
    const-string v2, "client_id"

    const-string v3, "fs24s8z0hh"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2077
    const-string v2, "client_secret"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2078
    const-string v2, "mypackage"

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2079
    const-string v2, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2081
    const-string v2, "MODE"

    const-string v3, "HIDE_NOTIFICATION_WITH_RESULT"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2082
    const-string v2, "additional"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2084
    if-eqz p1, :cond_1

    .line 2086
    const-string v0, "expired_access_token"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "samsung_account_token"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2089
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2094
    :goto_0
    return-void

    .line 2091
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/samsungaccount/MainActivity;I)I
    .locals 0

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ad:I

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aj:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ao:Ljava/lang/String;

    return-object p1
.end method

.method public static c()V
    .locals 5

    .prologue
    .line 2873
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/f;->b(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2874
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2875
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Z)V

    .line 2874
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2877
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    .line 2100
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2101
    const-string v0, "startActivitySSO"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2103
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2104
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "login_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "login_id_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mcc"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "api_server_url"

    aput-object v2, v0, v1

    const-string v1, "birthday"

    aput-object v1, v0, v5

    .line 2105
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.REQUEST_ACCESSTOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2106
    const-string v2, "client_id"

    const-string v3, "fs24s8z0hh"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2107
    const-string v2, "client_secret"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2108
    const-string v2, "progress_theme"

    const-string v3, "invisible"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2109
    const-string v2, "additional"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2111
    if-eqz p1, :cond_1

    .line 2112
    const-string v0, "expired_access_token"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "samsung_account_token"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2115
    :cond_1
    invoke-virtual {p0, v1, v5}, Lcom/sec/chaton/samsungaccount/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2119
    :goto_0
    return-void

    .line 2117
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/samsungaccount/MainActivity;Z)Z
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->P:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->af:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->v:Ljava/lang/String;

    return-object p1
.end method

.method private d()V
    .locals 3

    .prologue
    .line 874
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0290

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->C:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 876
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/samsungaccount/MainActivity;Z)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/sec/chaton/samsungaccount/MainActivity;->d(Z)V

    return-void
.end method

.method private d(Z)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2881
    const-string v0, "mum_enable_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2882
    if-nez p1, :cond_0

    .line 2883
    invoke-direct {p0, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Z)V

    .line 2915
    :goto_0
    return-void

    .line 2888
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 2889
    invoke-static {v3}, Lcom/sec/chaton/e/a/f;->b(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2890
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2891
    invoke-static {v3}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v6

    move v1, v2

    .line 2893
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2894
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v0}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;

    move-result-object v7

    .line 2895
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v8, "Favorites"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2896
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2893
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2901
    :cond_2
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aB:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 2902
    const-string v1, "group"

    const/16 v3, 0x148

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZILjava/util/ArrayList;Ljava/util/HashMap;)I

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/samsungaccount/MainActivity;)I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ae:I

    return v0
.end method

.method static synthetic e(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->an:Ljava/lang/String;

    return-object p1
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 881
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->u:Z

    if-eqz v0, :cond_3

    .line 882
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SSO was installed, SSO ver : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    const v1, 0x24ab8

    if-lt v0, v1, :cond_1

    .line 884
    invoke-direct {p0, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Z)V

    .line 933
    :cond_0
    :goto_0
    return-void

    .line 885
    :cond_1
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    const/16 v1, 0x32c9

    if-lt v0, v1, :cond_2

    .line 886
    invoke-direct {p0, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(Z)V

    goto :goto_0

    .line 888
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->m()V

    goto :goto_0

    .line 892
    :cond_3
    const-string v0, "SSO was NOT installed"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->G:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v4, :cond_5

    .line 895
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 896
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 906
    :cond_4
    new-instance v0, Lcom/sec/chaton/samsungaccount/ba;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aC:Lcom/sec/chaton/samsungaccount/bi;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/samsungaccount/ba;-><init>(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/bi;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->j:Lcom/sec/chaton/samsungaccount/ba;

    .line 907
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->j:Lcom/sec/chaton/samsungaccount/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/samsungaccount/ba;->show()V

    goto :goto_0

    .line 911
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0290

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->B:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->C:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    if-ne v0, v4, :cond_6

    .line 914
    invoke-direct {p0, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->d(Z)V

    goto :goto_0

    .line 918
    :cond_6
    const-string v0, "auto_regi_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 919
    invoke-static {}, Lcom/sec/chaton/util/cb;->a()Lcom/sec/chaton/util/cc;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/cc;->d:Lcom/sec/chaton/util/cc;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/cc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 921
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->n:Lcom/sec/chaton/d/at;

    invoke-virtual {v0}, Lcom/sec/chaton/d/at;->b()V

    goto :goto_0

    .line 923
    :cond_7
    invoke-static {}, Lcom/sec/chaton/util/cb;->a()Lcom/sec/chaton/util/cc;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/cc;->b:Lcom/sec/chaton/util/cc;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/cc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 924
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;)V

    .line 925
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto/16 :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/samsungaccount/MainActivity;Z)Z
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->R:Z

    return p1
.end method

.method static synthetic f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->O:Ljava/lang/String;

    return-object p1
.end method

.method private f()V
    .locals 3

    .prologue
    .line 937
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->u()V

    .line 940
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    if-nez v0, :cond_3

    .line 941
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "primary_contact_addrss"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 944
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->o:Lcom/sec/chaton/d/l;

    invoke-virtual {v0}, Lcom/sec/chaton/d/l;->a()V

    .line 973
    :cond_0
    :goto_0
    return-void

    .line 946
    :cond_1
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 947
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->t:Lcom/sec/chaton/d/g;

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/g;->a(Lcom/sec/chaton/d/a/b;)Lcom/sec/chaton/d/a/z;

    .line 951
    :goto_1
    const-string v0, "[GLD] Already get server address"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 949
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->r:Lcom/sec/chaton/d/bj;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    goto :goto_1

    .line 955
    :cond_3
    const-string v0, "auto_regi_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 956
    invoke-static {}, Lcom/sec/chaton/util/cb;->a()Lcom/sec/chaton/util/cc;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/cc;->d:Lcom/sec/chaton/util/cc;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/cc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 958
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->d()V

    .line 959
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 960
    const-string v0, "Mapping & Auto Request"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 964
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->e()V

    .line 965
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 966
    const-string v0, "Mapping Request"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 1019
    .line 1022
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1025
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1038
    :goto_0
    return-void

    .line 1029
    :cond_0
    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->G:Ljava/lang/String;

    .line 1032
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Samsung email : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->c:Landroid/os/Handler;

    invoke-static {v1}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->N:Lcom/sec/chaton/d/ap;

    .line 1036
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1037
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->N:Lcom/sec/chaton/d/ap;

    invoke-virtual {v2, v1, v0}, Lcom/sec/chaton/d/ap;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/samsungaccount/MainActivity;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    return v0
.end method

.method static synthetic h(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->S:Landroid/widget/ImageView;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->O:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1042
    const v0, 0x7f0b03d1

    invoke-direct {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(I)V

    .line 1043
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->s:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aD:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->a(Landroid/os/Handler;)V

    .line 1044
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aD:Lcom/sec/chaton/util/ar;

    const/16 v1, 0x3e9

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ar;->a(II)V

    .line 1045
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->T:Landroid/widget/ImageView;

    return-object v0
.end method

.method private i()Z
    .locals 3

    .prologue
    .line 1210
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateIsCritical"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkCriticalUpdate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1212
    return v0
.end method

.method static synthetic j(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->U:Landroid/widget/ImageView;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1216
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.chaton.ACTION_DISMISS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1217
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1218
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/samsungaccount/MainActivity;)I
    .locals 2

    .prologue
    .line 113
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ae:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ae:I

    return v0
.end method

.method private l()V
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 2000
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ad:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ad:I

    .line 2001
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ad:I

    if-lt v0, v1, :cond_1

    .line 2002
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    .line 2004
    :cond_1
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ad:I

    if-ge v0, v1, :cond_4

    .line 2006
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    const v1, 0x24ab8

    if-lt v0, v1, :cond_3

    .line 2007
    invoke-direct {p0, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Z)V

    .line 2032
    :cond_2
    :goto_0
    return-void

    .line 2009
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(Z)V

    goto :goto_0

    .line 2013
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ad:I

    .line 2015
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/samsungaccount/z;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/z;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    .line 2027
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ar:Z

    if-nez v0, :cond_2

    .line 2028
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 2035
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2036
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.request.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2037
    const-string v1, "client_id"

    const-string v2, "fs24s8z0hh"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2038
    const-string v1, "client_secret"

    const-string v2, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2039
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2040
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2042
    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2066
    :goto_0
    return-void

    .line 2044
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    .line 2046
    const-string v0, "ActivitySignIn will be run"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2047
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2048
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2049
    const-string v1, "client_id"

    const-string v2, "fs24s8z0hh"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2050
    const-string v1, "client_secret"

    const-string v2, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2051
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2055
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2056
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/samsungaccount/MainActivity;->a:Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2058
    :catch_0
    move-exception v0

    .line 2060
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/samsungaccount/ay;->b:Lcom/sec/chaton/samsungaccount/ay;

    iget-boolean v3, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;Z)V

    .line 2061
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic m(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->p()V

    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    .line 2123
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    .line 2125
    const-string v0, "ActivitySignIn will be run"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2126
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2127
    const-string v1, "client_id"

    const-string v2, "fs24s8z0hh"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2128
    const-string v1, "client_secret"

    const-string v2, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2129
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2130
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2131
    const-string v1, "MODE"

    const-string v2, "ADD_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2135
    const/4 v1, 0x2

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2136
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/samsungaccount/MainActivity;->a:Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2144
    :goto_0
    return-void

    .line 2138
    :catch_0
    move-exception v0

    .line 2140
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/samsungaccount/ay;->b:Lcom/sec/chaton/samsungaccount/ay;

    iget-boolean v3, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;Z)V

    .line 2141
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ar:Z

    return v0
.end method

.method static synthetic o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 2149
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2150
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2153
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2155
    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 2156
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->setResult(ILandroid/content/Intent;)V

    .line 2171
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->finish()V

    .line 2172
    return-void

    .line 2158
    :cond_1
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private p()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2177
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    if-ne v0, v1, :cond_0

    .line 2178
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->finish()V

    .line 2203
    :goto_0
    return-void

    .line 2182
    :cond_0
    iput-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->D:Z

    .line 2187
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->u()V

    .line 2190
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "primary_contact_addrss"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2193
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->o:Lcom/sec/chaton/d/l;

    invoke-virtual {v0}, Lcom/sec/chaton/d/l;->a()V

    goto :goto_0

    .line 2196
    :cond_1
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2197
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->t:Lcom/sec/chaton/d/g;

    sget-object v1, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/g;->a(Lcom/sec/chaton/d/a/b;)Lcom/sec/chaton/d/a/z;

    .line 2201
    :goto_1
    const-string v0, "[GLD] Already get server address"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2199
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->r:Lcom/sec/chaton/d/bj;

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    goto :goto_1
.end method

.method static synthetic p(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o()V

    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 2207
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2208
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2210
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2211
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->setResult(ILandroid/content/Intent;)V

    .line 2212
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->finish()V

    .line 2213
    return-void
.end method

.method static synthetic q(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    return-void
.end method

.method private r()V
    .locals 2

    .prologue
    .line 2217
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    .line 2219
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2220
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->setResult(ILandroid/content/Intent;)V

    .line 2221
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->finish()V

    .line 2223
    return-void
.end method

.method static synthetic r(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->j()V

    return-void
.end method

.method static synthetic s(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->am:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method private s()V
    .locals 3

    .prologue
    .line 2331
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    .line 2333
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    iget-boolean v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;Z)V

    .line 2334
    return-void
.end method

.method private t()V
    .locals 3

    .prologue
    .line 2367
    const-string v0, "showPasswordLockActivity"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2368
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 2370
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2372
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2373
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2375
    invoke-virtual {p0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 2377
    :cond_0
    return-void
.end method

.method static synthetic t(Lcom/sec/chaton/samsungaccount/MainActivity;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->P:Z

    return v0
.end method

.method static synthetic u(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/g;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->t:Lcom/sec/chaton/d/g;

    return-object v0
.end method

.method private u()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2759
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2786
    :cond_0
    :goto_0
    return-void

    .line 2764
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    if-nez v0, :cond_0

    .line 2769
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2770
    invoke-static {p0}, Lcom/sec/chaton/util/cb;->c(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    .line 2773
    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->D:Z

    if-eq v1, v4, :cond_2

    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->z:Z

    if-ne v1, v4, :cond_3

    .line 2774
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v5, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    .line 2785
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 2777
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    const/4 v2, -0x1

    if-le v1, v2, :cond_4

    .line 2778
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b028b

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v5, v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    goto :goto_1

    .line 2780
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b028c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v5, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    goto :goto_1
.end method

.method static synthetic v(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/bj;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->r:Lcom/sec/chaton/d/bj;

    return-object v0
.end method

.method private v()V
    .locals 2

    .prologue
    .line 2788
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2789
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dismissProgressBar, progressBar : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mMultiDeviceDialog : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2792
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2793
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2796
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2797
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 2798
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    .line 2800
    :cond_2
    return-void

    .line 2789
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1
.end method

.method private w()V
    .locals 3

    .prologue
    const v2, 0x7f0b02a9

    .line 3224
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3225
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->am:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_1

    .line 3226
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->am:Lcom/coolots/sso/a/a;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 3227
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;)V

    .line 3229
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 3249
    :goto_0
    return-void

    .line 3232
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 3236
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 3237
    const-string v0, "mChatonV is null"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3239
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;)V

    .line 3240
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto :goto_0

    .line 3244
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;)V

    .line 3246
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto :goto_0
.end method

.method static synthetic w(Lcom/sec/chaton/samsungaccount/MainActivity;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->D:Z

    return v0
.end method

.method static synthetic x(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->q()V

    return-void
.end method

.method static synthetic y(Lcom/sec/chaton/samsungaccount/MainActivity;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->z:Z

    return v0
.end method

.method static synthetic z(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->e()V

    return-void
.end method


# virtual methods
.method a()V
    .locals 4

    .prologue
    const v3, 0x7f07042d

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 827
    const v0, 0x7f0300f0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->setContentView(I)V

    .line 829
    invoke-virtual {p0, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aq:Landroid/widget/TextView;

    .line 831
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/res/Configuration;)V

    .line 833
    const v0, 0x7f07042f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 834
    const v0, 0x7f070430

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 836
    invoke-virtual {p0, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 837
    const v0, 0x7f07042e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 838
    return-void
.end method

.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 3259
    const v0, 0x7f0c00fd

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 3254
    const v0, 0x7f0c00fd

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 10

    .prologue
    const v9, 0x7f0b0290

    const/4 v8, 0x1

    const/4 v1, -0x1

    const/4 v7, 0x0

    .line 2567
    packed-switch p1, :pswitch_data_0

    .line 2755
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2569
    :pswitch_1
    if-ne p2, v1, :cond_0

    .line 2571
    const-string v0, "country_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2573
    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    if-ne v1, v8, :cond_1

    .line 2574
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0, v1, v8}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 2593
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->B:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->C:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2594
    invoke-direct {p0, v7}, Lcom/sec/chaton/samsungaccount/MainActivity;->d(Z)V

    goto :goto_0

    .line 2598
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0, v1, v7}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 2601
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->B:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->C:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2602
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->n:Lcom/sec/chaton/d/at;

    invoke-virtual {v0}, Lcom/sec/chaton/d/at;->b()V

    goto :goto_0

    .line 2611
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->u()V

    .line 2613
    if-ne p2, v1, :cond_3

    .line 2614
    const-string v0, "authcode"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->v:Ljava/lang/String;

    .line 2615
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->v:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2616
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->n:Lcom/sec/chaton/d/at;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->v:Ljava/lang/String;

    const-string v2, "fs24s8z0hh"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2624
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "authcode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2618
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    .line 2621
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    iget-boolean v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;Z)V

    goto :goto_1

    .line 2626
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    .line 2629
    const-string v0, "SIGN_IN : result is ERROR"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2634
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->u()V

    .line 2637
    sget-boolean v0, Lcom/sec/chaton/global/GlobalApplication;->g:Z

    if-nez v0, :cond_0

    .line 2643
    if-ne p2, v1, :cond_6

    .line 2644
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    .line 2647
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2648
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    const v1, 0x24ab8

    if-lt v0, v1, :cond_4

    .line 2649
    invoke-direct {p0, v7}, Lcom/sec/chaton/samsungaccount/MainActivity;->c(Z)V

    goto/16 :goto_0

    .line 2650
    :cond_4
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    const/16 v1, 0x32c9

    if-lt v0, v1, :cond_0

    .line 2651
    invoke-direct {p0, v7}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(Z)V

    goto/16 :goto_0

    .line 2654
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    .line 2657
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    iget-boolean v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;Z)V

    goto/16 :goto_0

    .line 2662
    :cond_6
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->v()V

    .line 2664
    const-string v0, ""

    .line 2666
    if-eqz p3, :cond_7

    .line 2668
    const-string v0, "error_message"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2674
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NEW_SIGN_IN : result is ERROR, errorMessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2680
    :pswitch_4
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 2681
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->p()V

    goto/16 :goto_0

    .line 2686
    :pswitch_5
    if-ne p2, v1, :cond_c

    .line 2687
    const-string v0, "access_token"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2688
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 2689
    const-string v1, "login_id"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->an:Ljava/lang/String;

    .line 2690
    const-string v1, "login_id_type"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2691
    const-string v2, "user_id"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ao:Ljava/lang/String;

    .line 2692
    const-string v2, "mcc"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2693
    const-string v3, "api_server_url"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2694
    const-string v4, "birthday"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2696
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_8

    .line 2697
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[ActivityResult] authToken : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " loginID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->an:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " userId : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ao:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " loginType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " mcc : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " birthday : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " server : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2702
    :cond_8
    const-string v1, "samsung_account_token"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2703
    const-string v0, "samsung_account_email"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->an:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2704
    const-string v0, "samsung_account_user_id"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ao:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2705
    const-string v0, "samsung_account_api_server"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2706
    const-string v0, "samsung_account_birthday"

    invoke-static {v0, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2709
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->B:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->C:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2711
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    if-ne v0, v8, :cond_9

    .line 2712
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2713
    invoke-static {}, Lcom/sec/chaton/samsungaccount/MainActivity;->c()V

    .line 2714
    invoke-direct {p0, v7}, Lcom/sec/chaton/samsungaccount/MainActivity;->d(Z)V

    goto/16 :goto_0

    .line 2717
    :cond_9
    const-string v0, "mum_enable_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->R:Z

    if-nez v0, :cond_a

    .line 2718
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->g()V

    .line 2719
    iput-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->O:Ljava/lang/String;

    goto/16 :goto_0

    .line 2722
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2723
    const v0, 0x7f0b03d1

    invoke-direct {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(I)V

    .line 2724
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->s:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aD:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->a(Landroid/os/Handler;)V

    .line 2725
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aD:Lcom/sec/chaton/util/ar;

    const/16 v1, 0x3e9

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ar;->a(II)V

    goto/16 :goto_0

    .line 2731
    :cond_b
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->s()V

    goto/16 :goto_0

    .line 2733
    :cond_c
    if-nez p2, :cond_d

    .line 2734
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->finish()V

    goto/16 :goto_0

    .line 2736
    :cond_d
    if-eqz p3, :cond_f

    .line 2737
    const-string v0, "error_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2738
    const-string v1, "error_message"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2740
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_e

    .line 2741
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[ActivityResult] errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " errorMessage : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2749
    :cond_e
    :goto_2
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->s()V

    goto/16 :goto_0

    .line 2745
    :cond_f
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_e

    .line 2746
    const-string v0, "data is null"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2567
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2491
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->F:Z

    if-ne v0, v1, :cond_1

    .line 2493
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->E:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->J:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2495
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onBackPressed()V

    .line 2504
    :goto_0
    return-void

    .line 2497
    :cond_0
    invoke-static {p0}, Lcom/sec/chaton/global/GlobalApplication;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 2501
    :cond_1
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 749
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 750
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 751
    const-string v0, "onConfigurationChanged"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->X:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    .line 756
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->X:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 758
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 759
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged, item : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->X:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->Y:Lcom/sec/chaton/samsungaccount/ax;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 762
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->X:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 764
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/res/Configuration;)V

    .line 766
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v7, 0x7f070363

    const v4, 0x7f0702da

    const/16 v3, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 263
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 264
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 265
    const-string v0, "onCreate"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :cond_0
    iput-object p0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    .line 271
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    .line 276
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->am:Lcom/coolots/sso/a/a;

    .line 278
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->am:Lcom/coolots/sso/a/a;

    invoke-virtual {v0, p0, p0}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->d:Ljava/lang/String;

    .line 284
    iput-boolean v5, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->P:Z

    .line 286
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "packageName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->x:Landroid/os/Bundle;

    .line 295
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->x:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    .line 297
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->x:Landroid/os/Bundle;

    const-string v1, "is_mapping_mode"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->x:Landroid/os/Bundle;

    const-string v1, "is_multi_device_mode"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->E:Z

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->x:Landroid/os/Bundle;

    const-string v1, "disable_back_button"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->F:Z

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->x:Landroid/os/Bundle;

    const-string v1, "auto_regi"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ak:Z

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->x:Landroid/os/Bundle;

    const-string v1, "is_ready_to_sa"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    .line 311
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Start Mode] mappingMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " multiDeviceMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->E:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " disableBack : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->F:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isAutoRegi : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ak:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isReadytoSA : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    :cond_4
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    if-eqz v0, :cond_f

    .line 318
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a()V

    .line 327
    :goto_0
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-ne v0, v6, :cond_5

    .line 329
    iput-boolean v6, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->M:Z

    .line 335
    :cond_5
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    if-nez v0, :cond_8

    .line 336
    const v0, 0x7f070362

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ImageTextViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    .line 337
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v1, 0x7f0203fb

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setImageResource(I)V

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v1, 0x7f0b0277

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setText(I)V

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    new-instance v1, Lcom/sec/chaton/samsungaccount/n;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/n;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    const v0, 0x7f070364

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ImageTextViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->I:Lcom/sec/chaton/widget/ImageTextViewGroup;

    .line 352
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->I:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v1, 0x7f0203fa

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setImageResource(I)V

    .line 353
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->I:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v1, 0x7f0b0092

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setText(I)V

    .line 354
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->I:Lcom/sec/chaton/widget/ImageTextViewGroup;

    new-instance v1, Lcom/sec/chaton/samsungaccount/ae;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/ae;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 369
    const v0, 0x7f070365

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->J:Landroid/widget/LinearLayout;

    .line 371
    const v0, 0x7f070368

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0702d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->K:Landroid/widget/Button;

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->K:Landroid/widget/Button;

    const v1, 0x7f0b02eb

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 373
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->K:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/samsungaccount/aq;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/aq;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 386
    const v0, 0x7f070367

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->L:Landroid/widget/TextView;

    .line 391
    const v0, 0x7f07035c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->X:Landroid/support/v4/view/ViewPager;

    .line 394
    const v0, 0x7f07035e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->S:Landroid/widget/ImageView;

    .line 395
    const v0, 0x7f07035f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->T:Landroid/widget/ImageView;

    .line 396
    const v0, 0x7f070360

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->U:Landroid/widget/ImageView;

    .line 400
    const v0, 0x7f07035d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->Z:Landroid/widget/LinearLayout;

    .line 402
    const v0, 0x7f070361

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ab:Landroid/widget/LinearLayout;

    .line 403
    const v0, 0x7f070366

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ac:Landroid/widget/LinearLayout;

    .line 408
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/res/Configuration;)V

    .line 412
    const-string v0, "for_wifi_only_device"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/sec/chaton/util/am;->w()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 414
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setVisibility(I)V

    .line 415
    invoke-virtual {p0, v7}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 426
    :goto_1
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->M:Z

    if-eqz v0, :cond_7

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->U:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 431
    :cond_7
    new-instance v0, Lcom/sec/chaton/samsungaccount/ax;

    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->M:Z

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/chaton/samsungaccount/ax;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;Landroid/support/v4/app/FragmentManager;Z)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->Y:Lcom/sec/chaton/samsungaccount/ax;

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->X:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->Y:Lcom/sec/chaton/samsungaccount/ax;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 435
    invoke-static {}, Lcom/sec/chaton/util/y;->a()V

    .line 443
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->J:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->au:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 449
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 450
    const v1, 0x7f0300a3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ai:Landroid/view/View;

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ai:Landroid/view/View;

    const v1, 0x7f07030b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->af:Landroid/widget/EditText;

    .line 455
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ai:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b003b

    new-instance v2, Lcom/sec/chaton/samsungaccount/as;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/as;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/samsungaccount/ar;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/ar;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aj:Lcom/sec/common/a/d;

    .line 505
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->X:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/sec/chaton/samsungaccount/at;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/at;-><init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 574
    :cond_8
    sget-boolean v0, Lcom/sec/chaton/c/a;->d:Z

    if-eqz v0, :cond_a

    .line 575
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_9

    .line 576
    const-string v0, "Welcome, First Time after update ChatON."

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    :cond_9
    invoke-static {}, Lcom/sec/chaton/util/am;->s()V

    .line 580
    const-string v0, "chaton_version"

    sget-object v1, Lcom/sec/chaton/c/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    sput-boolean v5, Lcom/sec/chaton/c/a;->d:Z

    .line 615
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "email : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " reuslt : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->u:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ax:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/at;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->n:Lcom/sec/chaton/d/at;

    .line 618
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ax:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/l;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->o:Lcom/sec/chaton/d/l;

    .line 619
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ax:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->r:Lcom/sec/chaton/d/bj;

    .line 624
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->av:Landroid/os/Handler;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "multidevice_chatlist_sync_last_time"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/chat/fh;->a(Landroid/os/Handler;J)Lcom/sec/chaton/chat/fh;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->p:Lcom/sec/chaton/chat/fh;

    .line 625
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->av:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    .line 626
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->s:Lcom/sec/chaton/d/a;

    .line 627
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ax:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/g;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->t:Lcom/sec/chaton/d/g;

    .line 629
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->i()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 630
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateAppsReady"

    const-string v2, "NO"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 631
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/UpgradeDialog;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 632
    const-string v2, "isCritical"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 633
    const-string v2, "isFromHome"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 634
    const-string v2, "isReadyApps"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 635
    invoke-virtual {p0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 636
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->b:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    .line 643
    :cond_b
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0280

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 644
    aget-object v1, v0, v5

    iput-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->B:Ljava/lang/String;

    .line 645
    aget-object v0, v0, v6

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->C:Ljava/lang/String;

    .line 649
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 650
    const-string v1, "action_sso_receive_code"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 651
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ay:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 654
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 655
    const-string v1, "upgrade_cancel"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 656
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aA:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 660
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->G:Ljava/lang/String;

    .line 663
    const-string v0, "auto_regi_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 664
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 665
    const-string v1, "back_auto_regi"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 666
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->az:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 668
    sget-boolean v0, Lcom/sec/chaton/global/GlobalApplication;->g:Z

    if-ne v0, v6, :cond_d

    .line 669
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_c

    .line 670
    const-string v0, "Now, background register is still going"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    :cond_c
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->u()V

    .line 678
    :cond_d
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    if-eqz v0, :cond_e

    .line 679
    const-string v0, "mum_enable_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 680
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f()V

    .line 689
    :cond_e
    :goto_2
    return-void

    .line 320
    :cond_f
    const v0, 0x7f0300c1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 417
    :cond_10
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    if-eqz v0, :cond_11

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->H:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setVisibility(I)V

    .line 422
    :goto_3
    invoke-virtual {p0, v7}, Lcom/sec/chaton/samsungaccount/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 420
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->I:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setVisibility(I)V

    goto :goto_3

    .line 683
    :cond_12
    invoke-direct {p0, v6}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Z)V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2454
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onDestroy()V

    .line 2455
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2456
    const-string v0, "onDestroy"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ar:Z

    .line 2461
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2462
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2464
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2465
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->A:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 2466
    iput-object v2, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->as:Landroid/view/View;

    .line 2468
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2469
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->al:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 2472
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ay:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2473
    const-string v0, "auto_regi_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2474
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->az:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2476
    :cond_4
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/samsungaccount/MainActivity;->a:Z

    .line 2480
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->am:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_5

    .line 2481
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->am:Lcom/coolots/sso/a/a;

    invoke-virtual {v0, p0, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 2485
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->aA:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2486
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 694
    invoke-direct {p0, p1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(I)Z

    .line 695
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/base/BaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 2341
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onPause()V

    .line 2342
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2344
    const-string v0, "onPause"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2348
    :cond_0
    return-void
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 3182
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 3183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveCreateAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3186
    :cond_0
    if-ne p1, v3, :cond_1

    .line 3187
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    const-string v1, "voip"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;IZ)V

    .line 3199
    :goto_0
    return-void

    .line 3189
    :cond_1
    new-instance v0, Lcom/sec/chaton/registration/an;

    invoke-direct {v0}, Lcom/sec/chaton/registration/an;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/registration/an;->a()V

    .line 3190
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Ljava/lang/String;)V

    .line 3191
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->y:Z

    if-eqz v0, :cond_2

    .line 3192
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 3193
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 3194
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto :goto_0

    .line 3196
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->q:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto :goto_0
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 0

    .prologue
    .line 3176
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 2353
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onResume()V

    .line 2354
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2355
    const-string v0, "onResume"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2358
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/MainActivity;->t()V

    .line 2363
    return-void
.end method

.method protected onStart()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2385
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onStart()V

    .line 2386
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2387
    const-string v0, "onStart"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2390
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    .line 2392
    iget v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_3

    .line 2393
    iput-boolean v4, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->u:Z

    .line 2397
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    .line 2400
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "email : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " verSSO : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->V:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " supportOldSSO : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->u:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2402
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    if-nez v0, :cond_1

    .line 2404
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->L:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/common/util/j;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2407
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ap:Z

    if-nez v0, :cond_2

    .line 2408
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->u:Z

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2414
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ab:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2415
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ac:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2438
    :cond_2
    :goto_1
    return-void

    .line 2395
    :cond_3
    iput-boolean v3, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->u:Z

    goto :goto_0

    .line 2417
    :cond_4
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "account_mapping_fail"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_5

    .line 2423
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ab:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2424
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ac:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 2432
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ab:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2433
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->ac:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 2445
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onStop()V

    .line 2446
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2447
    const-string v0, "onStop"

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/MainActivity;->W:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2449
    :cond_0
    return-void
.end method
