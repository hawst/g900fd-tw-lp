.class Lcom/sec/chaton/samsungaccount/ag;
.super Landroid/os/Handler;
.source "MainActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 2954
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2957
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 3005
    :cond_0
    :goto_0
    return-void

    .line 2960
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2962
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 2963
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2964
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Is Group Uploaded"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2965
    const-string v0, "local group info upload to server"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2967
    const-string v0, "auto_regi_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2968
    invoke-static {}, Lcom/sec/chaton/util/cb;->a()Lcom/sec/chaton/util/cc;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/cc;->d:Lcom/sec/chaton/util/cc;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/cc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/chaton/util/cb;->a()Lcom/sec/chaton/util/cc;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/cc;->a:Lcom/sec/chaton/util/cc;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/cc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2970
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->M(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/at;->a()V

    goto :goto_0

    .line 2971
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/cb;->a()Lcom/sec/chaton/util/cc;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/cc;->b:Lcom/sec/chaton/util/cc;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/cc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2972
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V

    .line 2973
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    goto :goto_0

    .line 2979
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ae(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ae(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2980
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ae(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 2981
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Landroid/view/View;)Landroid/view/View;

    .line 2985
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0122

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0291

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0024

    new-instance v3, Lcom/sec/chaton/samsungaccount/ai;

    invoke-direct {v3, p0}, Lcom/sec/chaton/samsungaccount/ai;-><init>(Lcom/sec/chaton/samsungaccount/ag;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b00dc

    new-instance v3, Lcom/sec/chaton/samsungaccount/ah;

    invoke-direct {v3, p0}, Lcom/sec/chaton/samsungaccount/ah;-><init>(Lcom/sec/chaton/samsungaccount/ag;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 2999
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3000
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ag;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->J(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 2960
    nop

    :pswitch_data_0
    .packed-switch 0x12e
        :pswitch_0
    .end packed-switch
.end method
