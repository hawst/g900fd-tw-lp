.class public Lcom/sec/chaton/samsungaccount/ba;
.super Landroid/app/Dialog;
.source "SamsungAccountLoginDialog.java"


# instance fields
.field protected a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/sec/chaton/samsungaccount/bi;

.field private k:Lcom/sec/chaton/d/at;

.field private l:Landroid/widget/FrameLayout;

.field private m:Landroid/content/Context;

.field private n:Landroid/webkit/WebView;

.field private o:Landroid/app/ProgressDialog;

.field private p:Landroid/widget/ImageView;

.field private q:Z

.field private r:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/bi;)V
    .locals 1

    .prologue
    .line 88
    const v0, 0x1030010

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 55
    const-string v0, "/mobile/account/check.do"

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->c:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->d:Ljava/lang/String;

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->o:Landroid/app/ProgressDialog;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/samsungaccount/ba;->q:Z

    .line 407
    new-instance v0, Lcom/sec/chaton/samsungaccount/bd;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/bd;-><init>(Lcom/sec/chaton/samsungaccount/ba;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->r:Landroid/os/Handler;

    .line 89
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/ba;->m:Landroid/content/Context;

    .line 90
    iput-object p2, p0, Lcom/sec/chaton/samsungaccount/ba;->j:Lcom/sec/chaton/samsungaccount/bi;

    .line 91
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/ba;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/ba;->o:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/ba;)Lcom/sec/chaton/d/at;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->k:Lcom/sec/chaton/d/at;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/ba;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/ba;->h:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 161
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/ba;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->p:Landroid/widget/ImageView;

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->p:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/samsungaccount/bc;

    invoke-direct {v1, p0}, Lcom/sec/chaton/samsungaccount/bc;-><init>(Lcom/sec/chaton/samsungaccount/ba;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/ba;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ba;->p:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->p:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 172
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/ba;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/chaton/samsungaccount/ba;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/samsungaccount/ba;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->n:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/samsungaccount/ba;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/ba;->i:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://account.samsung.com/mobile/account/check.do?serviceID=fs24s8z0hh&actionID=StartOAuth2&countryCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ba;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&languageCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ba;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->e:Ljava/lang/String;

    .line 401
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->n:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ba;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 402
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Load url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ba;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    return-void

    .line 397
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://account.samsung.com/mobile/account/check.do?serviceID=fs24s8z0hh&actionID=StartOAuth2&countryCode=&languageCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ba;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->m:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/samsungaccount/ba;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/ba;->q:Z

    return v0
.end method

.method static synthetic e(Lcom/sec/chaton/samsungaccount/ba;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/ba;->b()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/samsungaccount/ba;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->p:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->o:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/samsungaccount/ba;)Lcom/sec/chaton/samsungaccount/bi;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->j:Lcom/sec/chaton/samsungaccount/bi;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/samsungaccount/ba;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/samsungaccount/ba;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->o:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->m:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->m:Landroid/content/Context;

    .line 103
    :cond_1
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 104
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x2

    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 108
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0, v5}, Lcom/sec/chaton/samsungaccount/ba;->requestWindowFeature(I)Z

    .line 110
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/ba;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 111
    const v1, 0x3f333333    # 0.7f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 112
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/ba;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 114
    new-instance v0, Lcom/sec/chaton/samsungaccount/bb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/bb;-><init>(Lcom/sec/chaton/samsungaccount/ba;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/samsungaccount/ba;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->r:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/at;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->k:Lcom/sec/chaton/d/at;

    .line 123
    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->f:Ljava/lang/String;

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->m:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->g:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "en"

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->g:Ljava/lang/String;

    .line 132
    :cond_0
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ba;->m:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->l:Landroid/widget/FrameLayout;

    .line 133
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/ba;->a()V

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->p:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 136
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ba;->m:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 137
    new-instance v2, Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/ba;->m:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/chaton/samsungaccount/ba;->n:Landroid/webkit/WebView;

    .line 138
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ba;->n:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 141
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ba;->n:Landroid/webkit/WebView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 144
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ba;->n:Landroid/webkit/WebView;

    invoke-virtual {v2, v5}, Landroid/webkit/WebView;->setVerticalScrollbarOverlay(Z)V

    .line 146
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ba;->n:Landroid/webkit/WebView;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ba;->n:Landroid/webkit/WebView;

    new-instance v3, Lcom/sec/chaton/samsungaccount/bm;

    invoke-direct {v3, p0, v7}, Lcom/sec/chaton/samsungaccount/bm;-><init>(Lcom/sec/chaton/samsungaccount/ba;Lcom/sec/chaton/samsungaccount/bb;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 148
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ba;->n:Landroid/webkit/WebView;

    new-instance v3, Lcom/sec/chaton/samsungaccount/bj;

    invoke-direct {v3, p0, v7}, Lcom/sec/chaton/samsungaccount/bj;-><init>(Lcom/sec/chaton/samsungaccount/ba;Lcom/sec/chaton/samsungaccount/bb;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 150
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->n:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->l:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->l:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ba;->p:Landroid/widget/ImageView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ba;->l:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/samsungaccount/ba;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/ba;->b()V

    .line 158
    return-void
.end method
