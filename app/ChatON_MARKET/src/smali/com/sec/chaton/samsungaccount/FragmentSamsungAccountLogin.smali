.class public Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;
.super Landroid/support/v4/app/Fragment;
.source "FragmentSamsungAccountLogin.java"


# instance fields
.field protected a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/webkit/WebView;

.field private d:Landroid/content/Context;

.field private e:Landroid/app/ProgressDialog;

.field private f:Ljava/lang/String;

.field private g:Lcom/sec/chaton/d/at;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Landroid/support/v4/app/FragmentActivity;

.field private o:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->e:Landroid/app/ProgressDialog;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b:Ljava/util/ArrayList;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->l:Z

    .line 389
    new-instance v0, Lcom/sec/chaton/samsungaccount/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/samsungaccount/b;-><init>(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)V

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->o:Landroid/os/Handler;

    .line 579
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->e:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->n:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->h:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://account.samsung.com/mobile/account/check.do?serviceID=fs24s8z0hh&actionID=StartOAuth2&countryCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&languageCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->f:Ljava/lang/String;

    .line 299
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 300
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Load url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    return-void

    .line 294
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://account.samsung.com/mobile/account/check.do?serviceID=fs24s8z0hh&actionID=StartOAuth2&countryCode=&languageCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->l:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->i:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 322
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->n:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 325
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 328
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    invoke-virtual {p0, v1}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->startActivity(Landroid/content/Intent;)V

    .line 332
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->e:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Lcom/sec/chaton/d/at;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->g:Lcom/sec/chaton/d/at;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->l:Z

    return v0
.end method

.method static synthetic j(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 81
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->n:Landroid/support/v4/app/FragmentActivity;

    .line 82
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 189
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 190
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 87
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 89
    const v0, 0x7f0300cf

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->n:Landroid/support/v4/app/FragmentActivity;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->d:Landroid/content/Context;

    .line 98
    const v0, 0x7f0703a9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c:Landroid/webkit/WebView;

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c:Landroid/webkit/WebView;

    new-instance v2, Lcom/sec/chaton/samsungaccount/g;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/g;-><init>(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c:Landroid/webkit/WebView;

    invoke-virtual {v0, v5}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setVerticalScrollbarOverlay(Z)V

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c:Landroid/webkit/WebView;

    new-instance v2, Lcom/sec/chaton/samsungaccount/j;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/j;-><init>(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 126
    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->j:Ljava/lang/String;

    .line 127
    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->k:Ljava/lang/String;

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    const-string v0, "en"

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->k:Ljava/lang/String;

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->o:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/at;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->g:Lcom/sec/chaton/d/at;

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->d:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->e:Landroid/app/ProgressDialog;

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->e:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/sec/chaton/samsungaccount/a;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/a;-><init>(Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 173
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->a()V

    .line 174
    const-string v0, "GLD skip by request that is mapping"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 346
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 347
    const-string v0, "onDestroy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 354
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->n:Landroid/support/v4/app/FragmentActivity;

    .line 184
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 307
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 308
    const-string v0, "onPause"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 314
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 315
    const-string v0, "onResume"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/FragmentSamsungAccountLogin;->b()V

    .line 318
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 339
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 340
    const-string v0, "onStop"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    return-void
.end method
