.class Lcom/sec/chaton/samsungaccount/s;
.super Landroid/os/Handler;
.source "MainActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 1720
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10

    .prologue
    const v9, 0x7f0b0039

    const v8, 0x7f0b0037

    const/4 v4, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1724
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-ne v0, v6, :cond_1

    .line 1837
    :cond_0
    :goto_0
    return-void

    .line 1727
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1729
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0xce

    if-ne v1, v2, :cond_6

    .line 1731
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->q(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    .line 1733
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_5

    .line 1734
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;

    .line 1736
    if-eqz v0, :cond_4

    .line 1737
    iget-object v1, v0, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->exist_imei:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->exist_imei:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v6, :cond_2

    .line 1739
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1743
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03f9

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v5}, Lcom/sec/chaton/samsungaccount/MainActivity;->R(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v5}, Lcom/sec/chaton/samsungaccount/MainActivity;->R(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/samsungaccount/u;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/u;-><init>(Lcom/sec/chaton/samsungaccount/s;)V

    invoke-virtual {v1, v8, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/samsungaccount/t;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/t;-><init>(Lcom/sec/chaton/samsungaccount/s;)V

    invoke-virtual {v1, v9, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1766
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1767
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->J(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 1771
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->Q(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1772
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->b(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 1775
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0, v6}, Lcom/sec/chaton/samsungaccount/MainActivity;->d(Lcom/sec/chaton/samsungaccount/MainActivity;Z)V

    goto/16 :goto_0

    .line 1780
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    .line 1781
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1782
    const-string v0, "checkin result is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1787
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0

    .line 1790
    :cond_6
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0xcf

    if-ne v1, v2, :cond_0

    .line 1791
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_9

    .line 1792
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;

    .line 1794
    if-eqz v0, :cond_8

    .line 1795
    iget-object v1, v0, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->exist_imei:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    iget-object v0, v0, Lcom/sec/chaton/io/entry/CheckInRegisterParamEntry;->exist_imei:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v6, :cond_7

    .line 1797
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1801
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03f9

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v5}, Lcom/sec/chaton/samsungaccount/MainActivity;->R(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v5}, Lcom/sec/chaton/samsungaccount/MainActivity;->R(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/samsungaccount/w;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/w;-><init>(Lcom/sec/chaton/samsungaccount/s;)V

    invoke-virtual {v1, v8, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/samsungaccount/v;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/v;-><init>(Lcom/sec/chaton/samsungaccount/s;)V

    invoke-virtual {v1, v9, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1818
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1819
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->J(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 1823
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->S(Lcom/sec/chaton/samsungaccount/MainActivity;)V

    goto/16 :goto_0

    .line 1827
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    .line 1828
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1829
    const-string v0, "checkin result is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1834
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/s;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/samsungaccount/ay;->a:Lcom/sec/chaton/samsungaccount/ay;

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Landroid/content/Context;Lcom/sec/chaton/samsungaccount/ay;)V

    goto/16 :goto_0
.end method
