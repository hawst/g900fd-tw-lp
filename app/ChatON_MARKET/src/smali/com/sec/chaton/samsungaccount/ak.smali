.class Lcom/sec/chaton/samsungaccount/ak;
.super Lcom/sec/chaton/util/ar;
.source "MainActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 3080
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {p0}, Lcom/sec/chaton/util/ar;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 3083
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3167
    :cond_0
    :goto_0
    return-void

    .line 3088
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3e9

    if-ne v0, v1, :cond_0

    .line 3089
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_6

    .line 3090
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ae(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ae(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3091
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->ae(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 3092
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Landroid/view/View;)Landroid/view/View;

    .line 3094
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 3095
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 3096
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dev]Push registration failed. Network is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v0, :cond_4

    const-string v0, "unavailable"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Error is : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3100
    :cond_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0xbbc

    if-ne v0, v1, :cond_5

    .line 3101
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0042

    new-instance v3, Lcom/sec/chaton/samsungaccount/am;

    invoke-direct {v3, p0}, Lcom/sec/chaton/samsungaccount/am;-><init>(Lcom/sec/chaton/samsungaccount/ak;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/samsungaccount/al;

    invoke-direct {v3, p0}, Lcom/sec/chaton/samsungaccount/al;-><init>(Lcom/sec/chaton/samsungaccount/ak;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 3119
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3120
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->J(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 3096
    :cond_4
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 3127
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b029a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0223

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0024

    new-instance v3, Lcom/sec/chaton/samsungaccount/ao;

    invoke-direct {v3, p0}, Lcom/sec/chaton/samsungaccount/ao;-><init>(Lcom/sec/chaton/samsungaccount/ak;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0232

    new-instance v3, Lcom/sec/chaton/samsungaccount/an;

    invoke-direct {v3, p0}, Lcom/sec/chaton/samsungaccount/an;-><init>(Lcom/sec/chaton/samsungaccount/ak;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 3157
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3158
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->J(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 3163
    :cond_6
    const-string v0, "Push registration success."

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->k(Lcom/sec/chaton/samsungaccount/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3164
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/ak;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->M(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/at;->b()V

    goto/16 :goto_0
.end method
