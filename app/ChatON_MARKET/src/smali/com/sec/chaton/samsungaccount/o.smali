.class Lcom/sec/chaton/samsungaccount/o;
.super Landroid/os/Handler;
.source "MainActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/samsungaccount/MainActivity;)V
    .locals 0

    .prologue
    .line 1265
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const v3, 0x7f0b02a9

    .line 1268
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->n(Lcom/sec/chaton/samsungaccount/MainActivity;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1308
    :cond_0
    :goto_0
    return-void

    .line 1271
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1272
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x450

    if-ne v1, v2, :cond_0

    .line 1274
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_5

    .line 1275
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 1276
    const-string v0, "GET_VERSION_NOTICE success"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1282
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0384

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V

    .line 1283
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->s(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/coolots/sso/a/a;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1284
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->f(Lcom/sec/chaton/samsungaccount/MainActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->s(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/coolots/sso/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 1285
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V

    .line 1286
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 1287
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 1288
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_0

    .line 1291
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V

    .line 1292
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 1293
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 1294
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_0

    .line 1297
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Ljava/lang/String;)V

    .line 1298
    invoke-static {}, Lcom/sec/chaton/e/a/af;->b()V

    .line 1299
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 1300
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->o(Lcom/sec/chaton/samsungaccount/MainActivity;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->c()V

    goto/16 :goto_0

    .line 1305
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/o;->a:Lcom/sec/chaton/samsungaccount/MainActivity;

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->d:Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/samsungaccount/MainActivity;->a(Lcom/sec/chaton/samsungaccount/MainActivity;Lcom/sec/chaton/samsungaccount/az;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
