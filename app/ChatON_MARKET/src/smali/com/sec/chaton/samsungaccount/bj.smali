.class Lcom/sec/chaton/samsungaccount/bj;
.super Landroid/webkit/WebChromeClient;
.source "SamsungAccountLoginDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/ba;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/samsungaccount/ba;)V
    .locals 0

    .prologue
    .line 505
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/bj;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/samsungaccount/ba;Lcom/sec/chaton/samsungaccount/bb;)V
    .locals 0

    .prologue
    .line 505
    invoke-direct {p0, p1}, Lcom/sec/chaton/samsungaccount/bj;-><init>(Lcom/sec/chaton/samsungaccount/ba;)V

    return-void
.end method


# virtual methods
.method public onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 5

    .prologue
    .line 513
    const-string v0, "onJsAlert"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 515
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const/high16 v2, -0x1000000

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 517
    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bj;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 518
    const v2, 0x7f0b0007

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 519
    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 520
    const v0, 0x104000a

    new-instance v2, Lcom/sec/chaton/samsungaccount/bk;

    invoke-direct {v2, p0, p4}, Lcom/sec/chaton/samsungaccount/bk;-><init>(Lcom/sec/chaton/samsungaccount/bj;Landroid/webkit/JsResult;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 527
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 528
    new-instance v1, Lcom/sec/chaton/samsungaccount/bl;

    invoke-direct {v1, p0, p4}, Lcom/sec/chaton/samsungaccount/bl;-><init>(Lcom/sec/chaton/samsungaccount/bj;Landroid/webkit/JsResult;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 534
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 536
    const/4 v0, 0x1

    return v0
.end method

.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 2

    .prologue
    .line 508
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    return-void
.end method
