.class Lcom/sec/chaton/samsungaccount/bm;
.super Landroid/webkit/WebViewClient;
.source "SamsungAccountLoginDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/samsungaccount/ba;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/sec/chaton/samsungaccount/ba;)V
    .locals 1

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/samsungaccount/bm;->b:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/samsungaccount/ba;Lcom/sec/chaton/samsungaccount/bb;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/samsungaccount/bm;-><init>(Lcom/sec/chaton/samsungaccount/ba;)V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 344
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 347
    :cond_0
    const-string v0, "onPageFinished pd dismissed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_1
    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 244
    const-string v0, "onPageFinished..."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget-boolean v0, p0, Lcom/sec/chaton/samsungaccount/bm;->b:Z

    if-nez v0, :cond_c

    .line 250
    invoke-static {p2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 252
    const-string v1, "utf-8"

    invoke-static {v0, v1}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[HoneyComb] params : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " url : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 259
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 260
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    :cond_0
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Name :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 261
    :cond_1
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code_expires_in"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 262
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 263
    :cond_2
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 264
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 265
    :cond_3
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error_code"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 266
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 267
    :cond_4
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code_expires_in"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 268
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 269
    :cond_5
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error_description"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 270
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 271
    :cond_6
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "close"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 272
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/samsungaccount/ba;->a(Lcom/sec/chaton/samsungaccount/ba;Z)Z

    goto/16 :goto_1

    .line 279
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_9

    .line 280
    invoke-virtual {p1, v5}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->a(Lcom/sec/chaton/samsungaccount/ba;)Lcom/sec/chaton/d/at;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "fs24s8z0hh"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :cond_8
    :goto_2
    return-void

    .line 283
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_a

    .line 284
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/bm;->a()V

    .line 285
    invoke-virtual {p1, v5}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/samsungaccount/bo;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/bo;-><init>(Lcom/sec/chaton/samsungaccount/bm;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_2

    .line 296
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->d(Lcom/sec/chaton/samsungaccount/ba;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 297
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0, v4}, Lcom/sec/chaton/samsungaccount/ba;->a(Lcom/sec/chaton/samsungaccount/ba;Z)Z

    .line 299
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/bm;->a()V

    .line 300
    invoke-virtual {p1, v5}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->e(Lcom/sec/chaton/samsungaccount/ba;)V

    goto :goto_2

    .line 304
    :cond_b
    invoke-direct {p0}, Lcom/sec/chaton/samsungaccount/bm;->a()V

    .line 305
    const-string v0, "url has no regular data"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->f(Lcom/sec/chaton/samsungaccount/ba;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 310
    :cond_c
    iput-boolean v4, p0, Lcom/sec/chaton/samsungaccount/bm;->b:Z

    .line 312
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_d

    .line 314
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 316
    :cond_d
    const-string v0, "onPageFinished pd dismissed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 330
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPageStarted... url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v3}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/samsungaccount/ba;->a(Lcom/sec/chaton/samsungaccount/ba;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->g(Lcom/sec/chaton/samsungaccount/ba;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 355
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 356
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " failingUrl = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " desctiption = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/samsungaccount/bp;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/bp;-><init>(Lcom/sec/chaton/samsungaccount/bm;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 369
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 2

    .prologue
    .line 375
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onReceivedSslError] view="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " handler = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " error = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    :cond_0
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    .line 379
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "shouldOverrideUrlLoading, url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    iput-boolean v5, p0, Lcom/sec/chaton/samsungaccount/bm;->b:Z

    .line 185
    invoke-static {p2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 187
    const-string v1, "utf-8"

    invoke-static {v0, v1}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 191
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "params :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 194
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 195
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    :cond_0
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Name :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :cond_1
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code_expires_in"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 197
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 198
    :cond_2
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 199
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 200
    :cond_3
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error_code"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 201
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 202
    :cond_4
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "code_expires_in"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 203
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 204
    :cond_5
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error_description"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 205
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v2, v2, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 206
    :cond_6
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "close"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 207
    iget-object v2, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v2, v5}, Lcom/sec/chaton/samsungaccount/ba;->a(Lcom/sec/chaton/samsungaccount/ba;Z)Z

    goto/16 :goto_1

    .line 214
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_8

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->a(Lcom/sec/chaton/samsungaccount/ba;)Lcom/sec/chaton/d/at;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "fs24s8z0hh"

    const-string v3, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :goto_2
    return v5

    .line 216
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    iget-object v0, v0, Lcom/sec/chaton/samsungaccount/ba;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_9

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v1}, Lcom/sec/chaton/samsungaccount/ba;->c(Lcom/sec/chaton/samsungaccount/ba;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/samsungaccount/bn;

    invoke-direct {v2, p0}, Lcom/sec/chaton/samsungaccount/bn;-><init>(Lcom/sec/chaton/samsungaccount/bm;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_2

    .line 229
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->d(Lcom/sec/chaton/samsungaccount/ba;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0, v4}, Lcom/sec/chaton/samsungaccount/ba;->a(Lcom/sec/chaton/samsungaccount/ba;Z)Z

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->e(Lcom/sec/chaton/samsungaccount/ba;)V

    goto :goto_2

    .line 234
    :cond_a
    iput-boolean v4, p0, Lcom/sec/chaton/samsungaccount/bm;->b:Z

    .line 235
    const-string v0, "url has no regular data"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/samsungaccount/bm;->a:Lcom/sec/chaton/samsungaccount/ba;

    invoke-static {v0}, Lcom/sec/chaton/samsungaccount/ba;->f(Lcom/sec/chaton/samsungaccount/ba;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 237
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_2
.end method
