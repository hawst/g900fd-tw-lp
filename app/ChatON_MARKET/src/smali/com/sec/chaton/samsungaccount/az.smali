.class final enum Lcom/sec/chaton/samsungaccount/az;
.super Ljava/lang/Enum;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/samsungaccount/az;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/samsungaccount/az;

.field public static final enum b:Lcom/sec/chaton/samsungaccount/az;

.field public static final enum c:Lcom/sec/chaton/samsungaccount/az;

.field public static final enum d:Lcom/sec/chaton/samsungaccount/az;

.field public static final enum e:Lcom/sec/chaton/samsungaccount/az;

.field public static final enum f:Lcom/sec/chaton/samsungaccount/az;

.field public static final enum g:Lcom/sec/chaton/samsungaccount/az;

.field public static final enum h:Lcom/sec/chaton/samsungaccount/az;

.field public static final enum i:Lcom/sec/chaton/samsungaccount/az;

.field public static final enum j:Lcom/sec/chaton/samsungaccount/az;

.field public static final enum k:Lcom/sec/chaton/samsungaccount/az;

.field private static final synthetic l:[Lcom/sec/chaton/samsungaccount/az;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 152
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "get_server_address"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->a:Lcom/sec/chaton/samsungaccount/az;

    .line 153
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "get_buddies"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->b:Lcom/sec/chaton/samsungaccount/az;

    .line 154
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "get_chatlist"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->c:Lcom/sec/chaton/samsungaccount/az;

    .line 155
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "version_for_nation"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->d:Lcom/sec/chaton/samsungaccount/az;

    .line 156
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "version_for_upgrade"

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->e:Lcom/sec/chaton/samsungaccount/az;

    .line 157
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "SA_user_info"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->f:Lcom/sec/chaton/samsungaccount/az;

    .line 158
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "mapping"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->g:Lcom/sec/chaton/samsungaccount/az;

    .line 159
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "SA_access_token"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->h:Lcom/sec/chaton/samsungaccount/az;

    .line 160
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "auto_regi"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->i:Lcom/sec/chaton/samsungaccount/az;

    .line 161
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "extra_info"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->j:Lcom/sec/chaton/samsungaccount/az;

    .line 162
    new-instance v0, Lcom/sec/chaton/samsungaccount/az;

    const-string v1, "chaton_v"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/samsungaccount/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->k:Lcom/sec/chaton/samsungaccount/az;

    .line 151
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/chaton/samsungaccount/az;

    sget-object v1, Lcom/sec/chaton/samsungaccount/az;->a:Lcom/sec/chaton/samsungaccount/az;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/samsungaccount/az;->b:Lcom/sec/chaton/samsungaccount/az;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/samsungaccount/az;->c:Lcom/sec/chaton/samsungaccount/az;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/samsungaccount/az;->d:Lcom/sec/chaton/samsungaccount/az;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/samsungaccount/az;->e:Lcom/sec/chaton/samsungaccount/az;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->f:Lcom/sec/chaton/samsungaccount/az;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->g:Lcom/sec/chaton/samsungaccount/az;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->h:Lcom/sec/chaton/samsungaccount/az;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->i:Lcom/sec/chaton/samsungaccount/az;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->j:Lcom/sec/chaton/samsungaccount/az;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/chaton/samsungaccount/az;->k:Lcom/sec/chaton/samsungaccount/az;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/samsungaccount/az;->l:[Lcom/sec/chaton/samsungaccount/az;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/samsungaccount/az;
    .locals 1

    .prologue
    .line 151
    const-class v0, Lcom/sec/chaton/samsungaccount/az;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/samsungaccount/az;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/samsungaccount/az;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/sec/chaton/samsungaccount/az;->l:[Lcom/sec/chaton/samsungaccount/az;

    invoke-virtual {v0}, [Lcom/sec/chaton/samsungaccount/az;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/samsungaccount/az;

    return-object v0
.end method
