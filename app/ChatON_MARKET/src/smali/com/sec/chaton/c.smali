.class Lcom/sec/chaton/c;
.super Ljava/lang/Object;
.source "AdminMenu.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/AdminMenu;


# direct methods
.method constructor <init>(Lcom/sec/chaton/AdminMenu;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 332
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 571
    :goto_0
    :pswitch_0
    return-void

    .line 335
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->b(Lcom/sec/chaton/AdminMenu;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 336
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->b(Lcom/sec/chaton/AdminMenu;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 339
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1, v0}, Lcom/sec/chaton/AdminMenu;->a(Lcom/sec/chaton/AdminMenu;I)I

    .line 340
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->c(Lcom/sec/chaton/AdminMenu;)V

    .line 341
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->d(Lcom/sec/chaton/AdminMenu;)V

    .line 342
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    const/16 v2, 0x64

    invoke-static {v1, v2}, Lcom/sec/chaton/AdminMenu;->b(Lcom/sec/chaton/AdminMenu;I)I

    .line 343
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v3}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/sec/chaton/AdminMenu;->a(Lcom/sec/chaton/AdminMenu;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 344
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v1

    const-string v2, "Make Dummy ChatRoom ..."

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 345
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 346
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 348
    new-instance v0, Lcom/sec/chaton/l;

    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-direct {v0, v1}, Lcom/sec/chaton/l;-><init>(Lcom/sec/chaton/AdminMenu;)V

    .line 349
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1, v4}, Lcom/sec/chaton/AdminMenu;->a(Lcom/sec/chaton/AdminMenu;Z)Z

    .line 350
    invoke-virtual {v0}, Lcom/sec/chaton/l;->start()V

    goto :goto_0

    .line 356
    :pswitch_2
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->b(Lcom/sec/chaton/AdminMenu;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 357
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->b(Lcom/sec/chaton/AdminMenu;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 360
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1, v0}, Lcom/sec/chaton/AdminMenu;->a(Lcom/sec/chaton/AdminMenu;I)I

    .line 361
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->c(Lcom/sec/chaton/AdminMenu;)V

    .line 362
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->d(Lcom/sec/chaton/AdminMenu;)V

    .line 363
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    const/16 v2, 0xc8

    invoke-static {v1, v2}, Lcom/sec/chaton/AdminMenu;->b(Lcom/sec/chaton/AdminMenu;I)I

    .line 364
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v3}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/sec/chaton/AdminMenu;->a(Lcom/sec/chaton/AdminMenu;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 365
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v1

    const-string v2, "Make Dummy ChatRoom ..."

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 369
    new-instance v0, Lcom/sec/chaton/l;

    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-direct {v0, v1}, Lcom/sec/chaton/l;-><init>(Lcom/sec/chaton/AdminMenu;)V

    .line 370
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1, v4}, Lcom/sec/chaton/AdminMenu;->a(Lcom/sec/chaton/AdminMenu;Z)Z

    .line 371
    invoke-virtual {v0}, Lcom/sec/chaton/l;->start()V

    goto/16 :goto_0

    .line 376
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/c/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 383
    :pswitch_4
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {}, Lcom/sec/chaton/util/y;->b()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/AdminMenu;->c(Lcom/sec/chaton/AdminMenu;I)I

    .line 384
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->g(Lcom/sec/chaton/AdminMenu;)I

    move-result v1

    if-lez v1, :cond_2

    .line 385
    const-string v1, "Chat ON Log was disenabled..."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    invoke-static {v0}, Lcom/sec/chaton/util/y;->b(I)V

    .line 387
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Log Off"

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->h(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v0

    const-string v1, "Log On"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 390
    :cond_2
    invoke-static {v4}, Lcom/sec/chaton/util/y;->b(I)V

    .line 391
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Log On"

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 392
    const-string v0, "Chat ON Log was enabled..."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->h(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v0

    const-string v1, "Log Off"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 397
    :pswitch_5
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->i(Lcom/sec/chaton/AdminMenu;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->j(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v2}, Lcom/sec/chaton/AdminMenu;->k(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v3}, Lcom/sec/chaton/AdminMenu;->l(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v4}, Lcom/sec/chaton/AdminMenu;->m(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/chaton/c/g;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 399
    invoke-static {}, Lcom/sec/chaton/j/af;->c()Z

    goto/16 :goto_0

    .line 403
    :pswitch_6
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->j(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 404
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->k(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->l(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->m(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 407
    invoke-static {}, Lcom/sec/chaton/c/g;->a()V

    goto/16 :goto_0

    .line 410
    :pswitch_7
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/am;->a(Landroid/content/Context;)V

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Random device id is generated."

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 418
    :pswitch_8
    const-string v1, "admin_dev_gld"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 419
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "GLD server change to DEV sever"

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 424
    :pswitch_9
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 425
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/AdminMenu;->setResult(ILandroid/content/Intent;)V

    .line 426
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "hidden_skip_mode"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-virtual {v0}, Lcom/sec/chaton/AdminMenu;->finish()V

    goto/16 :goto_0

    .line 430
    :pswitch_a
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v3}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/sec/chaton/AdminMenu;->a(Lcom/sec/chaton/AdminMenu;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 431
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v1

    const-string v2, "Backup chaton database"

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 432
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 433
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 434
    new-instance v0, Lcom/sec/chaton/k;

    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-direct {v0, v1}, Lcom/sec/chaton/k;-><init>(Lcom/sec/chaton/AdminMenu;)V

    .line 435
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1, v4}, Lcom/sec/chaton/AdminMenu;->a(Lcom/sec/chaton/AdminMenu;Z)Z

    .line 436
    invoke-virtual {v0}, Lcom/sec/chaton/k;->start()V

    goto/16 :goto_0

    .line 441
    :pswitch_b
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->d(Landroid/content/ContentResolver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 449
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 450
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 452
    :try_start_1
    invoke-static {v2}, Lcom/sec/chaton/e/a/d;->d(Landroid/content/ContentResolver;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_3

    .line 460
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 461
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "hanzitopinyin hidden mode interval : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v0, v2, v0

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.chaton"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 442
    :catch_0
    move-exception v0

    .line 444
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 445
    :catch_1
    move-exception v0

    .line 447
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_1

    .line 453
    :catch_2
    move-exception v2

    .line 455
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 456
    :catch_3
    move-exception v2

    .line 458
    invoke-virtual {v2}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_2

    .line 465
    :pswitch_c
    new-instance v1, Lcom/sec/chaton/d/h;

    invoke-direct {v1, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v1, v0}, Lcom/sec/chaton/d/h;->b(Z)V

    goto/16 :goto_0

    .line 469
    :pswitch_d
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "admin_upgrade"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 472
    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 479
    :goto_3
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_3

    .line 480
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Current upgrade : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AdminMenu"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    :cond_3
    packed-switch v0, :pswitch_data_1

    .line 494
    :goto_4
    const-string v1, "admin_upgrade"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 475
    :cond_4
    add-int/lit8 v0, v1, 0x1

    goto :goto_3

    .line 485
    :pswitch_e
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->n(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "upgrade : disable"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 488
    :pswitch_f
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->n(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "upgrade : normal"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 491
    :pswitch_10
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->n(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "upgrade : critical"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 497
    :pswitch_11
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "admin_notice"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v4, :cond_5

    .line 498
    const-string v1, "admin_notice"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 499
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->o(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v0

    const-string v1, "notice : disable"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 501
    :cond_5
    const-string v0, "admin_notice"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 502
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->o(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v0

    const-string v1, "notice : enable"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 506
    :pswitch_12
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "admin_disclaimer"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v4, :cond_6

    .line 507
    const-string v1, "admin_disclaimer"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 508
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->p(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v0

    const-string v1, "disclaimer : disable"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 510
    :cond_6
    const-string v0, "admin_disclaimer"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->p(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v0

    const-string v1, "disclaimer : enable"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 517
    :pswitch_13
    iget-object v0, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->q(Lcom/sec/chaton/AdminMenu;)V

    goto/16 :goto_0

    .line 524
    :pswitch_14
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v1

    .line 525
    if-eqz v1, :cond_7

    .line 526
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->d()Z

    .line 527
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->r(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "A-Resend:DISABLED"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 528
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "disable : AutoResend"

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 530
    :cond_7
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->c()Z

    .line 531
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->r(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "A-Resend:ENABLED"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 532
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "enable : AutoResend"

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 537
    :pswitch_15
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->g()Z

    move-result v1

    .line 538
    if-eqz v1, :cond_8

    .line 539
    invoke-static {v0}, Lcom/sec/chaton/msgsend/q;->a(Z)V

    .line 540
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->s(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "A-Resend:Noti disabled"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 541
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "disable : Noti "

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 543
    :cond_8
    invoke-static {v4}, Lcom/sec/chaton/msgsend/q;->a(Z)V

    .line 544
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->s(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "A-Resend:Noti enabled"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 545
    iget-object v1, p0, Lcom/sec/chaton/c;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "enable : Noti"

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 550
    :pswitch_16
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->b()V

    goto/16 :goto_0

    .line 554
    :pswitch_17
    sget-object v0, Lcom/sec/chaton/msgsend/x;->l:Lcom/sec/chaton/msgsend/x;

    invoke-static {v0}, Lcom/sec/chaton/msgsend/p;->a(Lcom/sec/chaton/msgsend/x;)V

    goto/16 :goto_0

    .line 561
    :pswitch_18
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->a()V

    goto/16 :goto_0

    .line 566
    :pswitch_19
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->c()V

    goto/16 :goto_0

    .line 332
    :pswitch_data_0
    .packed-switch 0x7f070033
        :pswitch_4
        :pswitch_3
        :pswitch_14
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_b
        :pswitch_c
        :pswitch_1
        :pswitch_2
        :pswitch_15
        :pswitch_16
        :pswitch_18
        :pswitch_17
        :pswitch_19
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_d
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch

    .line 483
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method
