.class public Lcom/sec/chaton/u;
.super Ljava/lang/Object;
.source "HandleIntent.java"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Ljava/lang/String;

.field public static i:Ljava/lang/String;

.field public static j:Ljava/lang/String;

.field public static k:Ljava/lang/String;

.field public static l:Ljava/lang/String;

.field private static m:Ljava/lang/String;

.field private static n:Ljava/lang/String;

.field private static o:Ljava/lang/String;

.field private static p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const-string v0, "receiver"

    sput-object v0, Lcom/sec/chaton/u;->a:Ljava/lang/String;

    .line 83
    const-string v0, "intent_data1"

    sput-object v0, Lcom/sec/chaton/u;->b:Ljava/lang/String;

    .line 84
    const-string v0, "file"

    sput-object v0, Lcom/sec/chaton/u;->c:Ljava/lang/String;

    .line 85
    const-string v0, "intent_data2"

    sput-object v0, Lcom/sec/chaton/u;->d:Ljava/lang/String;

    .line 86
    const-string v0, "chaton"

    sput-object v0, Lcom/sec/chaton/u;->m:Ljava/lang/String;

    .line 87
    const-string v0, "android.intent.action.SPELL"

    sput-object v0, Lcom/sec/chaton/u;->n:Ljava/lang/String;

    .line 89
    const-string v0, "intent_from"

    sput-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    .line 90
    const-string v0, "com.sec.chaton"

    sput-object v0, Lcom/sec/chaton/u;->o:Ljava/lang/String;

    .line 93
    const-string v0, "com.sec.chaton.action.ADD_BUDDY"

    sput-object v0, Lcom/sec/chaton/u;->p:Ljava/lang/String;

    .line 94
    const-string v0, "com.sec.chaton.action.VIEW_BUDDY"

    sput-object v0, Lcom/sec/chaton/u;->f:Ljava/lang/String;

    .line 96
    const-string v0, "com.sec.chaton.action.CREATE_ACCOUNT_CHATONV"

    sput-object v0, Lcom/sec/chaton/u;->g:Ljava/lang/String;

    .line 98
    const-string v0, "auto_backup"

    sput-object v0, Lcom/sec/chaton/u;->h:Ljava/lang/String;

    .line 100
    const-string v0, "com.sec.chaton.action.VOICE_CHAT"

    sput-object v0, Lcom/sec/chaton/u;->i:Ljava/lang/String;

    .line 101
    const-string v0, "com.sec.chaton.action.VIDEO_CHAT"

    sput-object v0, Lcom/sec/chaton/u;->j:Ljava/lang/String;

    .line 102
    const-string v0, "com.sec.chaton.action.VOICE_GROUP_CHAT"

    sput-object v0, Lcom/sec/chaton/u;->k:Ljava/lang/String;

    .line 103
    const-string v0, "com.sec.chaton.action.VIDEO_GROUP_CHAT"

    sput-object v0, Lcom/sec/chaton/u;->l:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/v;
    .locals 10

    .prologue
    const v9, 0x7f0b008d

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 554
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 557
    if-nez v4, :cond_1

    .line 558
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_0

    .line 559
    const-string v0, "checkIsBuddyAndIsInBox(), abnormal status !!!!"

    const-class v1, Lcom/sec/chaton/u;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    :cond_0
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    .line 679
    :goto_0
    return-object v0

    .line 565
    :cond_1
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/w;

    .line 566
    if-nez p3, :cond_3

    if-eqz p4, :cond_2

    array-length v1, p4

    if-nez v1, :cond_3

    :cond_2
    if-nez p2, :cond_3

    .line 567
    const-string v0, "ONEtoONE / fail to find"

    const-class v1, Lcom/sec/chaton/u;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto :goto_0

    .line 572
    :cond_3
    if-nez p2, :cond_7

    if-eqz p4, :cond_7

    array-length v1, p4

    if-lez v1, :cond_7

    .line 573
    aget-object p2, p4, v3

    move-object v2, p3

    move-object p3, p2

    .line 579
    :goto_1
    if-eqz p3, :cond_8

    .line 580
    const-string v1, "0999"

    invoke-virtual {p3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    .line 581
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 582
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkIsBuddyAndIsInBox(), spbd_intent, exist special buddy table : "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "/"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v6, Lcom/sec/chaton/u;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :cond_4
    if-eqz v5, :cond_13

    .line 599
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p3}, Lcom/sec/chaton/e/a/af;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    .line 602
    const-string v6, "specialbuddy"

    invoke-virtual {p1, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 606
    :goto_2
    if-nez v5, :cond_5

    .line 607
    invoke-static {p0, p1, p3}, Lcom/sec/chaton/u;->b(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v1

    .line 611
    :cond_5
    const-string v6, "fromPush"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 616
    const-string v0, "buddyNO"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 617
    const-string v0, "buddyNO"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 618
    const-string v1, "receivers"

    new-array v2, v8, [Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 619
    const-string v0, "buddyNO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 622
    :cond_6
    sget-object v0, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 574
    :cond_7
    if-nez p2, :cond_14

    .line 576
    const-string v2, ""

    goto :goto_1

    .line 585
    :cond_8
    const-string v0, "ONEtoONE / fail to find."

    const-class v1, Lcom/sec/chaton/u;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 629
    :cond_9
    if-nez v2, :cond_a

    const-string v2, ""

    .line 630
    :cond_a
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 634
    sget-object v2, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    .line 635
    if-eqz v4, :cond_b

    const-string v7, "chatType"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 636
    const-string v2, "chatType"

    const/4 v7, -0x1

    invoke-virtual {v4, v2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    .line 638
    :cond_b
    sget-object v4, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v2, v4, :cond_12

    .line 639
    invoke-static {p3, v6, p0}, Lcom/sec/chaton/u;->a(Ljava/lang/String;Ljava/lang/StringBuilder;Landroid/content/Context;)Z

    move-result v2

    .line 642
    :goto_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 646
    if-eqz v5, :cond_c

    .line 647
    if-eqz v2, :cond_11

    .line 648
    invoke-static {p0, v6}, Lcom/sec/chaton/u;->a(Landroid/content/Context;Ljava/lang/StringBuilder;)Z

    move-result v0

    .line 650
    :goto_4
    if-nez v1, :cond_d

    if-nez v0, :cond_d

    .line 651
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 652
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 656
    :cond_c
    if-nez v1, :cond_d

    if-nez v2, :cond_d

    sget-object v5, Lcom/sec/chaton/w;->b:Lcom/sec/chaton/w;

    if-eq v0, v5, :cond_d

    .line 657
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 658
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 662
    :cond_d
    if-eqz v1, :cond_e

    .line 664
    const-string v0, "receivers"

    new-array v1, v8, [Ljava/lang/String;

    aput-object p3, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 668
    :cond_e
    if-eqz v2, :cond_10

    .line 669
    const-string v0, "inboxNO"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 679
    :cond_f
    :goto_5
    sget-object v0, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 673
    :cond_10
    const-string v0, "inboxNO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 674
    const-string v0, "inboxNO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_5

    :cond_11
    move v0, v3

    goto :goto_4

    :cond_12
    move v2, v3

    goto :goto_3

    :cond_13
    move v1, v3

    goto/16 :goto_2

    :cond_14
    move-object v2, p3

    move-object p3, p2

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Intent;Landroid/content/Context;)Lcom/sec/chaton/v;
    .locals 10

    .prologue
    const v6, 0x7f0b008c

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 114
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "uid"

    invoke-virtual {v0, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 121
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    .line 123
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/chaton/u;->n:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 124
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/sec/chaton/u;->m:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 127
    :cond_1
    sget-object v0, Lcom/sec/chaton/u;->n:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 128
    invoke-static {p0, p1}, Lcom/sec/chaton/u;->b(Landroid/content/Intent;Landroid/content/Context;)Lcom/sec/chaton/v;

    move-result-object v0

    .line 129
    sget-object v3, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/v;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 507
    :cond_2
    :goto_0
    return-object v0

    .line 133
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ChatON specific data="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v3, Lcom/sec/chaton/u;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    .line 136
    :try_start_0
    invoke-static {p0}, Lcom/sec/chaton/h/j;->a(Landroid/content/Intent;)Lcom/sec/chaton/h/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/chaton/h/a;->a()Lcom/sec/chaton/v;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 142
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Interlocked With "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / Type : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/sec/chaton/u;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/u;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "receivers"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 146
    :cond_5
    const-string v0, "receivers"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 148
    if-nez v0, :cond_3f

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/u;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 149
    sget-object v0, Lcom/sec/chaton/u;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 156
    new-array v0, v1, [Ljava/lang/String;

    aput-object v3, v0, v8

    move-object v3, v0

    .line 162
    :goto_1
    if-eqz v3, :cond_6

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aget-object v4, v3, v8

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 163
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 164
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 169
    :cond_6
    if-eqz v3, :cond_8

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "android.intent.extra.TEXT"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 170
    :cond_7
    const-string v0, "If both receiver and content are given, proper access token is required."

    const-class v4, Lcom/sec/chaton/u;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/chaton/api/access_token/AccessTokenProvider;->a(Landroid/content/Context;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 172
    const-string v0, "Access token is incorrect."

    const-class v1, Lcom/sec/chaton/u;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 178
    :cond_8
    const-string v0, "callChatList"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 179
    if-eqz v3, :cond_15

    array-length v0, v3

    if-le v0, v1, :cond_15

    .line 180
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 181
    sget-object v0, Lcom/sec/chaton/w;->a:Lcom/sec/chaton/w;

    .line 182
    if-eqz v2, :cond_9

    sget-object v4, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 183
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/w;

    .line 185
    :cond_9
    sget-object v2, Lcom/sec/chaton/w;->b:Lcom/sec/chaton/w;

    if-ne v0, v2, :cond_11

    .line 189
    array-length v2, v3

    move v0, v8

    :goto_2
    if-ge v0, v2, :cond_3e

    aget-object v4, v3, v0

    .line 190
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    move v0, v1

    .line 195
    :goto_3
    if-eqz v0, :cond_b

    .line 196
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 201
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 189
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 206
    :cond_b
    array-length v0, v3

    if-le v0, v1, :cond_f

    .line 207
    const-string v0, "chatType"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 208
    const-string v0, "chatType"

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 212
    :cond_c
    sget-object v0, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-static {v0, v3}, Lcom/sec/chaton/e/a/n;->a(Lcom/sec/chaton/e/r;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 214
    const-string v1, "inboxNO"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    :cond_d
    :goto_4
    const-string v0, "receivers"

    invoke-virtual {p0, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 232
    sget-object v0, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    .line 266
    :goto_5
    sget-object v1, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    if-eq v0, v1, :cond_2

    .line 296
    :cond_e
    :goto_6
    sget-object v1, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    if-ne v0, v1, :cond_1a

    .line 297
    const v1, 0x7f0b002f

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 218
    :cond_f
    array-length v0, v3

    if-ne v0, v1, :cond_10

    .line 219
    const-string v0, "chatType"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 220
    const-string v0, "chatType"

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_4

    .line 224
    :cond_10
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 229
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 236
    :cond_11
    invoke-static {p1, v3}, Lcom/sec/chaton/u;->a(Landroid/content/Context;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 237
    array-length v2, v0

    if-le v2, v1, :cond_13

    .line 238
    const-string v1, "chatType"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 239
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 255
    :cond_12
    :goto_7
    const-string v1, "receivers"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    sget-object v0, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    goto :goto_5

    .line 242
    :cond_13
    array-length v2, v0

    if-ne v2, v1, :cond_14

    .line 243
    const-string v1, "chatType"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 244
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_7

    .line 248
    :cond_14
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 253
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 259
    :cond_15
    const-string v0, "chatType"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 260
    const-string v0, "chatType"

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 262
    :cond_16
    invoke-static {p1, p0, v2, v2, v3}, Lcom/sec/chaton/u;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/v;

    move-result-object v0

    goto/16 :goto_5

    .line 273
    :cond_17
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "inboxNO"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 277
    const-string v0, "inboxNO"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 278
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 279
    invoke-static {v2, v3, p1}, Lcom/sec/chaton/u;->a(Ljava/lang/String;Ljava/lang/StringBuilder;Landroid/content/Context;)Z

    move-result v0

    .line 281
    if-eqz v0, :cond_18

    .line 282
    const-string v0, "callChatList"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 283
    sget-object v0, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    goto/16 :goto_6

    .line 286
    :cond_18
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_6

    .line 292
    :cond_19
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "callChatTab"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 293
    sget-object v0, Lcom/sec/chaton/v;->d:Lcom/sec/chaton/v;

    goto/16 :goto_6

    .line 299
    :cond_1a
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 300
    sget-object v1, Lcom/sec/chaton/u;->b:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 307
    :cond_1b
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_23

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_23

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_23

    .line 308
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    .line 313
    const-string v1, "android.intent.category.BROWSABLE"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    sget-object v1, Lcom/sec/chaton/u;->m:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1d

    :cond_1c
    sget-object v1, Lcom/sec/chaton/u;->m:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 319
    :cond_1d
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 320
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 323
    :try_start_1
    invoke-static {p0}, Lcom/sec/chaton/h/j;->a(Landroid/content/Intent;)Lcom/sec/chaton/h/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/chaton/h/a;->a()Lcom/sec/chaton/v;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 328
    sget-object v2, Lcom/sec/chaton/v;->b:Lcom/sec/chaton/v;

    if-ne v0, v2, :cond_1e

    .line 329
    sget-object v0, Lcom/sec/chaton/v;->b:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 330
    :cond_1e
    sget-object v0, Lcom/sec/chaton/u;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 332
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 334
    :cond_1f
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 341
    :cond_20
    const-string v0, "Shocrcut from contact"

    const-class v1, Lcom/sec/chaton/u;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 344
    if-eqz v1, :cond_21

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 345
    const-string v0, "data1"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 346
    const-string v2, "chatType"

    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 347
    const-string v2, "callChatList"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 355
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p1, p0, v0, v2, v3}, Lcom/sec/chaton/u;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/v;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 361
    if-eqz v1, :cond_2

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 358
    :cond_21
    :try_start_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b008c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 359
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 361
    if-eqz v1, :cond_2

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 361
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_22

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_22
    throw v0

    .line 369
    :cond_23
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_27

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/u;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 370
    invoke-static {p1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 371
    new-instance v1, Lcom/coolots/sso/a/a;

    invoke-direct {v1}, Lcom/coolots/sso/a/a;-><init>()V

    .line 372
    invoke-virtual {v1, p1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_25

    .line 373
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_24

    .line 374
    const-string v0, "ChatONV be started to register"

    const-class v1, Lcom/sec/chaton/u;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    :cond_24
    sget-object v0, Lcom/sec/chaton/v;->f:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 381
    :cond_25
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 382
    const-string v1, "ChatONV already registerd"

    const-class v2, Lcom/sec/chaton/u;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 387
    :cond_26
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 388
    const-string v1, "ChatONV was not available"

    const-class v2, Lcom/sec/chaton/u;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 396
    :cond_27
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_28

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/u;->p:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 397
    sget-object v0, Lcom/sec/chaton/v;->g:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 400
    :cond_28
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_29

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/u;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 401
    sget-object v0, Lcom/sec/chaton/v;->h:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 405
    :cond_29
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2a

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/u;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 406
    sget-object v0, Lcom/sec/chaton/v;->j:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 409
    :cond_2a
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2b

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/u;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 410
    sget-object v0, Lcom/sec/chaton/v;->k:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 413
    :cond_2b
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2c

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/u;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 414
    sget-object v0, Lcom/sec/chaton/v;->l:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 417
    :cond_2c
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2d

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/u;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 418
    sget-object v0, Lcom/sec/chaton/v;->m:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 421
    :cond_2d
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2e

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/u;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 422
    sget-object v0, Lcom/sec/chaton/v;->n:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 425
    :cond_2e
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 426
    if-eqz v0, :cond_3c

    .line 428
    const-string v3, "callMyPageTab"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 429
    sget-object v0, Lcom/sec/chaton/v;->e:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 432
    :cond_2f
    const-string v3, "callAlertSetting"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_30

    .line 433
    sget-object v0, Lcom/sec/chaton/v;->i:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 437
    :cond_30
    const-string v3, "callChatTab"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 438
    sget-object v0, Lcom/sec/chaton/v;->d:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 443
    :cond_31
    const-string v3, "callRestart"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_32

    const-string v3, "finish"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_32

    const-string v3, "callUpgradeDialog"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 444
    :cond_32
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 448
    :cond_33
    const-string v3, "inboxNO"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 449
    const-string v3, "receivers"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 450
    const-string v4, "chatType"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 452
    const-string v5, "buddyNO"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 458
    sget-object v6, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v6}, Lcom/sec/chaton/e/r;->a()I

    move-result v6

    if-ne v4, v6, :cond_34

    .line 460
    invoke-static {p1, p0, v5, v9, v3}, Lcom/sec/chaton/u;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/v;

    move-result-object v0

    goto/16 :goto_0

    .line 463
    :cond_34
    const-string v3, "fromPush"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_36

    .line 468
    const-string v2, "buddyNO"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 469
    const-string v2, "buddyNO"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 470
    const-string v2, "receivers"

    new-array v1, v1, [Ljava/lang/String;

    aput-object v0, v1, v8

    invoke-virtual {p0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 471
    const-string v0, "buddyNO"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 475
    :cond_35
    sget-object v0, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 478
    :cond_36
    if-eqz v9, :cond_3b

    .line 480
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-array v5, v1, [Ljava/lang/String;

    const-string v0, "inbox_no"

    aput-object v0, v5, v8

    const-string v6, "inbox_no=?"

    new-array v7, v1, [Ljava/lang/String;

    aput-object v9, v7, v8

    move-object v8, v2

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 482
    if-eqz v1, :cond_37

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_38

    .line 483
    :cond_37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0317

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 484
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 487
    if-eqz v1, :cond_2

    .line 488
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 487
    :cond_38
    if-eqz v1, :cond_39

    .line 488
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 492
    :cond_39
    const-string v0, "inboxNO"

    invoke-virtual {p0, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 497
    const-string v0, "Shocrcut from homescreen"

    const-class v1, Lcom/sec/chaton/u;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    sget-object v0, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 487
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_3a

    .line 488
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3a
    throw v0

    .line 495
    :cond_3b
    sget-object v0, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 503
    :cond_3c
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 507
    :cond_3d
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    goto/16 :goto_0

    .line 324
    :catch_0
    move-exception v1

    goto/16 :goto_0

    .line 137
    :catch_1
    move-exception v1

    goto/16 :goto_0

    :cond_3e
    move v0, v8

    goto/16 :goto_3

    :cond_3f
    move-object v3, v0

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 684
    .line 686
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 687
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "buddy_no"

    aput-object v3, v2, v6

    const-string v3, "buddy_name"

    aput-object v3, v2, v7

    const-string v3, "buddy_name=?"

    new-array v4, v7, [Ljava/lang/String;

    aput-object p2, v4, v6

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 689
    if-eqz v1, :cond_2

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v7, :cond_2

    .line 690
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 691
    const-string v0, "buddy_no"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 692
    const-string v2, "receivers"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v7

    .line 705
    :goto_0
    if-eqz v1, :cond_0

    .line 706
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 711
    :cond_0
    :goto_1
    return v0

    .line 705
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 706
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    :cond_2
    move v0, v6

    goto :goto_0

    :cond_3
    move v0, v6

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/StringBuilder;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 867
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 868
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    const-string v3, "message_inbox_no=?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object v5, v4, v7

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 870
    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 874
    :cond_0
    if-eqz v0, :cond_1

    .line 875
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v7

    .line 878
    :goto_0
    return v0

    .line 874
    :cond_2
    if-eqz v0, :cond_3

    .line 875
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v6

    .line 878
    goto :goto_0

    .line 874
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_4

    .line 875
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/StringBuilder;Landroid/content/Context;)Z
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 776
    .line 777
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 778
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 779
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "inbox_no"

    aput-object v3, v2, v6

    const-string v3, "inbox_no = ?"

    new-array v4, v7, [Ljava/lang/String;

    aput-object v8, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 781
    if-eqz v1, :cond_7

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_7

    move v0, v7

    .line 785
    :goto_0
    if-eqz v1, :cond_0

    .line 786
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 823
    :cond_0
    :goto_1
    return v0

    .line 785
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 786
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 790
    :cond_2
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 791
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "inbox_no"

    aput-object v3, v2, v6

    const-string v3, "inbox_last_chat_type"

    aput-object v3, v2, v7

    const-string v3, "inbox_no= ( SELECT inbox_no FROM inbox_buddy_relation WHERE buddy_no = ? )"

    new-array v4, v7, [Ljava/lang/String;

    aput-object p0, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 794
    if-eqz v1, :cond_5

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 799
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 800
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 801
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 807
    const-string v2, "inbox_last_chat_type"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    .line 808
    const/16 v3, 0xc

    if-ne v2, v3, :cond_3

    move v0, v6

    .line 817
    :goto_2
    if-eqz v1, :cond_0

    .line 818
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 811
    :cond_3
    :try_start_2
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v0, v7

    goto :goto_2

    .line 817
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_4

    .line 818
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :cond_5
    move v0, v6

    goto :goto_2

    :cond_6
    move v0, v6

    goto :goto_1

    :cond_7
    move v0, v6

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 830
    new-array v0, v1, [Ljava/lang/String;

    .line 831
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_1

    .line 861
    :cond_0
    :goto_0
    return-object v0

    .line 835
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 836
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 837
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 839
    array-length v4, p1

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v1, p1, v0

    .line 840
    const-string v5, "\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\'"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 839
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 843
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 845
    const-string v0, "buddy_no"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 849
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "buddy_no"

    aput-object v5, v2, v4

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 850
    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 851
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 852
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 856
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    .line 857
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 856
    :cond_4
    if-eqz v1, :cond_5

    .line 857
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 861
    :cond_5
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto/16 :goto_0

    .line 856
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_3
.end method

.method private static b(Landroid/content/Intent;Landroid/content/Context;)Lcom/sec/chaton/v;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 513
    sget-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    .line 514
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_0

    .line 547
    :goto_0
    return-object v0

    .line 517
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "mQuery"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 518
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "chatonmessage"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 519
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "chatonbuddy"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 521
    if-nez v2, :cond_5

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_5

    .line 525
    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    if-nez v3, :cond_2

    .line 526
    const-string v0, "null"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 527
    sget-object v0, Lcom/sec/chaton/v;->b:Lcom/sec/chaton/v;

    .line 544
    :cond_1
    :goto_2
    const-string v1, "callForward"

    invoke-virtual {p0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 545
    const-string v1, "content_type"

    sget-object v2, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 546
    sget-object v1, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    sget-object v2, Lcom/sec/chaton/w;->b:Lcom/sec/chaton/w;

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    .line 530
    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    if-nez v1, :cond_3

    .line 531
    const-string v0, "download_uri"

    invoke-virtual {p0, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 532
    sget-object v0, Lcom/sec/chaton/v;->b:Lcom/sec/chaton/v;

    goto :goto_2

    .line 534
    :cond_3
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 535
    const-string v0, "download_uri"

    invoke-virtual {p0, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 536
    invoke-static {p1, p0, v1}, Lcom/sec/chaton/u;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v0

    .line 537
    if-eqz v0, :cond_4

    .line 538
    const-string v0, "chatType"

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 539
    const-string v0, "callChatList"

    invoke-virtual {p0, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 541
    :cond_4
    sget-object v0, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto :goto_1
.end method

.method private static b(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 718
    .line 722
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 723
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "buddy_no"

    aput-object v3, v2, v6

    const-string v3, "buddy_show_phone_number"

    aput-object v3, v2, v7

    const-string v3, "buddy_extra_info"

    aput-object v3, v2, v4

    const-string v3, "buddy_msisdns"

    aput-object v3, v2, v8

    const-string v3, "buddy_no=?"

    new-array v4, v7, [Ljava/lang/String;

    aput-object p2, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 733
    if-eqz v2, :cond_5

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 734
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 736
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 737
    const/4 v0, 0x2

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 738
    const/4 v1, 0x3

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    move v1, v6

    move v6, v7

    .line 741
    :goto_0
    if-eqz v2, :cond_0

    .line 742
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 750
    :cond_0
    :goto_1
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_3

    .line 751
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 752
    const-string v3, "addExtrasToIntent"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 753
    if-eqz p2, :cond_1

    .line 754
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 756
    :cond_1
    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "extras="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 757
    if-eqz v0, :cond_2

    .line 758
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 761
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/u;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    :cond_3
    const-string v2, "showPhoneNumber"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 765
    const-string v1, "extraInfo"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 766
    const-string v0, "msisdns"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 767
    const-string v0, "is_buddy"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 769
    return v6

    .line 741
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_4

    .line 742
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :cond_5
    move-object v0, v5

    move v1, v6

    goto :goto_0

    :cond_6
    move-object v0, v5

    move v1, v6

    goto :goto_1
.end method
