.class public final enum Lcom/sec/chaton/msgsend/o;
.super Ljava/lang/Enum;
.source "MsgContract.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/msgsend/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/msgsend/o;

.field public static final enum b:Lcom/sec/chaton/msgsend/o;

.field public static final enum c:Lcom/sec/chaton/msgsend/o;

.field public static final enum d:Lcom/sec/chaton/msgsend/o;

.field public static final enum e:Lcom/sec/chaton/msgsend/o;

.field public static final enum f:Lcom/sec/chaton/msgsend/o;

.field public static final enum g:Lcom/sec/chaton/msgsend/o;

.field public static final enum h:Lcom/sec/chaton/msgsend/o;

.field public static final enum i:Lcom/sec/chaton/msgsend/o;

.field private static final synthetic k:[Lcom/sec/chaton/msgsend/o;


# instance fields
.field j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 30
    new-instance v0, Lcom/sec/chaton/msgsend/o;

    const-string v1, "CMD_UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/msgsend/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/o;->a:Lcom/sec/chaton/msgsend/o;

    .line 31
    new-instance v0, Lcom/sec/chaton/msgsend/o;

    const-string v1, "CMD_ADD_MSG"

    invoke-direct {v0, v1, v8, v4}, Lcom/sec/chaton/msgsend/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/o;->b:Lcom/sec/chaton/msgsend/o;

    .line 33
    new-instance v0, Lcom/sec/chaton/msgsend/o;

    const-string v1, "CMD_TRY_TO_SEND_MSG"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/chaton/msgsend/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/o;->c:Lcom/sec/chaton/msgsend/o;

    .line 34
    new-instance v0, Lcom/sec/chaton/msgsend/o;

    const-string v1, "CMD_LOAD_MSG"

    invoke-direct {v0, v1, v6, v6}, Lcom/sec/chaton/msgsend/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/o;->d:Lcom/sec/chaton/msgsend/o;

    .line 36
    new-instance v0, Lcom/sec/chaton/msgsend/o;

    const-string v1, "CMD_UPDATE_MSG"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v7}, Lcom/sec/chaton/msgsend/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/o;->e:Lcom/sec/chaton/msgsend/o;

    .line 37
    new-instance v0, Lcom/sec/chaton/msgsend/o;

    const-string v1, "CMD_REMOVE_MSG"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/chaton/msgsend/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/o;->f:Lcom/sec/chaton/msgsend/o;

    .line 38
    new-instance v0, Lcom/sec/chaton/msgsend/o;

    const-string v1, "CMD_REMOVE_INBOX"

    const/4 v2, 0x6

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/o;->g:Lcom/sec/chaton/msgsend/o;

    .line 39
    new-instance v0, Lcom/sec/chaton/msgsend/o;

    const-string v1, "CMD_REMOVE_ALL_MSG"

    const/4 v2, 0x7

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/o;->h:Lcom/sec/chaton/msgsend/o;

    .line 40
    new-instance v0, Lcom/sec/chaton/msgsend/o;

    const-string v1, "CMD_PRINT_DEBUG"

    const/16 v2, 0x8

    const/16 v3, 0x270f

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/o;->i:Lcom/sec/chaton/msgsend/o;

    .line 29
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/chaton/msgsend/o;

    sget-object v1, Lcom/sec/chaton/msgsend/o;->a:Lcom/sec/chaton/msgsend/o;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/msgsend/o;->b:Lcom/sec/chaton/msgsend/o;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/chaton/msgsend/o;->c:Lcom/sec/chaton/msgsend/o;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/msgsend/o;->d:Lcom/sec/chaton/msgsend/o;

    aput-object v1, v0, v6

    const/4 v1, 0x4

    sget-object v2, Lcom/sec/chaton/msgsend/o;->e:Lcom/sec/chaton/msgsend/o;

    aput-object v2, v0, v1

    sget-object v1, Lcom/sec/chaton/msgsend/o;->f:Lcom/sec/chaton/msgsend/o;

    aput-object v1, v0, v7

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/msgsend/o;->g:Lcom/sec/chaton/msgsend/o;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/msgsend/o;->h:Lcom/sec/chaton/msgsend/o;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/msgsend/o;->i:Lcom/sec/chaton/msgsend/o;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/msgsend/o;->k:[Lcom/sec/chaton/msgsend/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/sec/chaton/msgsend/o;->j:I

    .line 46
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/msgsend/o;
    .locals 1

    .prologue
    .line 53
    sparse-switch p0, :sswitch_data_0

    .line 64
    sget-object v0, Lcom/sec/chaton/msgsend/o;->a:Lcom/sec/chaton/msgsend/o;

    :goto_0
    return-object v0

    .line 54
    :sswitch_0
    sget-object v0, Lcom/sec/chaton/msgsend/o;->b:Lcom/sec/chaton/msgsend/o;

    goto :goto_0

    .line 56
    :sswitch_1
    sget-object v0, Lcom/sec/chaton/msgsend/o;->c:Lcom/sec/chaton/msgsend/o;

    goto :goto_0

    .line 57
    :sswitch_2
    sget-object v0, Lcom/sec/chaton/msgsend/o;->d:Lcom/sec/chaton/msgsend/o;

    goto :goto_0

    .line 59
    :sswitch_3
    sget-object v0, Lcom/sec/chaton/msgsend/o;->e:Lcom/sec/chaton/msgsend/o;

    goto :goto_0

    .line 60
    :sswitch_4
    sget-object v0, Lcom/sec/chaton/msgsend/o;->f:Lcom/sec/chaton/msgsend/o;

    goto :goto_0

    .line 61
    :sswitch_5
    sget-object v0, Lcom/sec/chaton/msgsend/o;->g:Lcom/sec/chaton/msgsend/o;

    goto :goto_0

    .line 62
    :sswitch_6
    sget-object v0, Lcom/sec/chaton/msgsend/o;->h:Lcom/sec/chaton/msgsend/o;

    goto :goto_0

    .line 63
    :sswitch_7
    sget-object v0, Lcom/sec/chaton/msgsend/o;->i:Lcom/sec/chaton/msgsend/o;

    goto :goto_0

    .line 53
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x5 -> :sswitch_3
        0xa -> :sswitch_4
        0xb -> :sswitch_5
        0xc -> :sswitch_6
        0x270f -> :sswitch_7
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/msgsend/o;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/chaton/msgsend/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/o;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/msgsend/o;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/chaton/msgsend/o;->k:[Lcom/sec/chaton/msgsend/o;

    invoke-virtual {v0}, [Lcom/sec/chaton/msgsend/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/msgsend/o;

    return-object v0
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/chaton/msgsend/o;->j:I

    return v0
.end method
