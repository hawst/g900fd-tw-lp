.class public Lcom/sec/chaton/msgsend/i;
.super Ljava/lang/Object;
.source "ElapsedTime.java"


# instance fields
.field a:J

.field b:J

.field c:J


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method public static a()Lcom/sec/chaton/msgsend/i;
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/sec/chaton/msgsend/i;

    invoke-direct {v0}, Lcom/sec/chaton/msgsend/i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/chaton/msgsend/i;
    .locals 2

    .prologue
    .line 21
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/i;->a:J

    .line 22
    return-object p0
.end method

.method public c()Lcom/sec/chaton/msgsend/i;
    .locals 4

    .prologue
    .line 26
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/i;->b:J

    .line 27
    iget-wide v0, p0, Lcom/sec/chaton/msgsend/i;->b:J

    iget-wide v2, p0, Lcom/sec/chaton/msgsend/i;->a:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/i;->c:J

    .line 28
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 37
    const-string v0, "elapsed in %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/chaton/msgsend/j;

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/i;->c:J

    invoke-direct {v3, v4, v5}, Lcom/sec/chaton/msgsend/j;-><init>(J)V

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
