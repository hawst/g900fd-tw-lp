.class public Lcom/sec/chaton/msgsend/j;
.super Ljava/lang/Object;
.source "ElapsedTime.java"


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public f:J


# direct methods
.method public constructor <init>(J)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x3e8

    const-wide/16 v6, 0x18

    const-wide/16 v4, 0x3c

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-wide p1, p0, Lcom/sec/chaton/msgsend/j;->a:J

    .line 56
    rem-long v0, p1, v2

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/j;->f:J

    .line 57
    div-long v0, p1, v2

    .line 60
    rem-long v2, v0, v4

    iput-wide v2, p0, Lcom/sec/chaton/msgsend/j;->e:J

    .line 61
    div-long/2addr v0, v4

    .line 64
    rem-long v2, v0, v4

    iput-wide v2, p0, Lcom/sec/chaton/msgsend/j;->d:J

    .line 65
    div-long/2addr v0, v4

    .line 68
    rem-long v2, v0, v6

    iput-wide v2, p0, Lcom/sec/chaton/msgsend/j;->c:J

    .line 69
    div-long/2addr v0, v6

    .line 72
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/j;->b:J

    .line 73
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 2

    .prologue
    .line 49
    sub-long v0, p1, p3

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/msgsend/j;-><init>(J)V

    .line 50
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 76
    const-string v0, "(%d msec), %dday/%02d:%02d:%02d.%03d"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/j;->a:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/j;->b:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/j;->c:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/j;->d:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/j;->e:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/j;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
