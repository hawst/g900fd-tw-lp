.class public Lcom/sec/chaton/msgsend/p;
.super Ljava/lang/Object;
.source "MsgSendGateway.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/chaton/msgsend/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 227
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (deleteAllMsgWithExceptionMsgList)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    const/4 v0, -0x1

    .line 233
    :goto_0
    return v0

    .line 232
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 233
    invoke-virtual {v0, p0, p1, p2}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v0

    goto :goto_0
.end method

.method public static a()V
    .locals 3

    .prologue
    .line 98
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (isAutoResendEnabled)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->e()Landroid/content/Intent;

    move-result-object v0

    .line 104
    const-string v1, "cmd_priority"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 105
    sget-object v1, Lcom/sec/chaton/msgsend/o;->d:Lcom/sec/chaton/msgsend/o;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgsend/p;->a(Lcom/sec/chaton/msgsend/o;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static a(IIILcom/sec/chaton/j/c/k;)V
    .locals 3

    .prologue
    .line 131
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (setFileUploadCompleteResult)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 137
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/sec/chaton/msgsend/l;->a(IIILcom/sec/chaton/j/c/k;)Z

    goto :goto_0
.end method

.method public static a(ILjava/lang/String;Lcom/sec/chaton/d/bi;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 119
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (setSErverResult)"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    .line 125
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v1

    .line 126
    invoke-virtual {v1, p0, p1, p2, p3}, Lcom/sec/chaton/msgsend/l;->a(ILjava/lang/String;Lcom/sec/chaton/d/bi;Z)Z

    .line 127
    sget-object v1, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v2, "setServerResult(), %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(Lcom/sec/chaton/msgsend/o;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 23
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    .line 24
    const-string v1, "cmd"

    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/o;->a()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 25
    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 26
    return-void
.end method

.method public static a(Lcom/sec/chaton/msgsend/x;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 76
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "itsTimeToSend(), reason(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled, (ItsTimeToSend)"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->e()Landroid/content/Intent;

    move-result-object v0

    .line 84
    const-string v1, "send_retry_reason"

    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/x;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 85
    sget-object v1, Lcom/sec/chaton/msgsend/o;->c:Lcom/sec/chaton/msgsend/o;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgsend/p;->a(Lcom/sec/chaton/msgsend/o;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 176
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (deleteInbox_direct)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 182
    invoke-virtual {v0, p0}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 159
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (deleteMsg_direct)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 165
    invoke-virtual {v0, p0, p1, p2}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;J)Z

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 327
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled, update_updateSentMessage)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 334
    :goto_0
    return-void

    .line 332
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v6, v5

    .line 333
    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/Integer;)V
    .locals 7

    .prologue
    .line 343
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (update_updateFileUploaded)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    :goto_0
    return-void

    .line 348
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 349
    const/4 v5, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 7

    .prologue
    .line 359
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (updateStatus)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    :goto_0
    return-void

    .line 364
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 365
    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 313
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled, (update_updateAttchMessage)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-wide v1, p1

    move-object v4, p3

    move-object v5, v3

    .line 318
    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/sec/chaton/e/r;JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/aa;[Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 281
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (insertSendMessageWithoutUpload)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    :goto_0
    return-void

    .line 286
    :cond_0
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p8

    move-wide v3, p2

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p4

    move-object/from16 v11, p7

    invoke-static/range {v0 .. v11}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;ILcom/sec/chaton/msgsend/aa;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/sec/chaton/e/r;JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/aa;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 12

    .prologue
    .line 293
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled insertMediaMessageWithUpload)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 299
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p8

    move-wide v3, p2

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p4

    move-object/from16 v8, p11

    move-object/from16 v9, p9

    move/from16 v10, p10

    move-object/from16 v11, p7

    .line 298
    invoke-static/range {v0 .. v11}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;ILcom/sec/chaton/msgsend/aa;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;ILcom/sec/chaton/msgsend/aa;)V
    .locals 10

    .prologue
    .line 47
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Request Send Msg, chatType(%s), msgType(%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p7, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    new-instance v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    move-wide v1, p3

    move-object/from16 v3, p7

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move/from16 v8, p10

    move-object/from16 v9, p11

    invoke-direct/range {v0 .. v9}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;-><init>(JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/sec/chaton/msgsend/aa;)V

    .line 67
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->e()Landroid/content/Intent;

    move-result-object v1

    .line 68
    const-string v2, "data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 69
    const-string v0, "inbox_no"

    invoke-virtual {v1, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const-string v0, "chat_type"

    invoke-virtual {p1}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 71
    const-string v0, "participants"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    sget-object v0, Lcom/sec/chaton/msgsend/o;->b:Lcom/sec/chaton/msgsend/o;

    invoke-static {v0, v1}, Lcom/sec/chaton/msgsend/p;->a(Lcom/sec/chaton/msgsend/o;Landroid/content/Intent;)V

    .line 73
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 186
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (deleteAllMsgWithStatus)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    :goto_0
    return-void

    .line 191
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 192
    invoke-virtual {v0, p0, p1}, Lcom/sec/chaton/msgsend/l;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 196
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (deleteInboxMsgExclude)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    const/4 v0, -0x1

    .line 202
    :goto_0
    return v0

    .line 201
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 202
    invoke-virtual {v0, p0, p1}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 253
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (deleteMsgWithTargetList)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 255
    const/4 v0, -0x1

    .line 259
    :goto_0
    return v0

    .line 258
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 259
    invoke-virtual {v0, p0, p1, p2}, Lcom/sec/chaton/msgsend/l;->b(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v0

    goto :goto_0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 141
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->e()Landroid/content/Intent;

    move-result-object v0

    .line 142
    sget-object v1, Lcom/sec/chaton/msgsend/o;->i:Lcom/sec/chaton/msgsend/o;

    invoke-static {v1, v0}, Lcom/sec/chaton/msgsend/p;->a(Lcom/sec/chaton/msgsend/o;Landroid/content/Intent;)V

    .line 143
    return-void
.end method

.method public static b(Ljava/lang/String;JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 385
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 386
    sget-object v0, Lcom/sec/chaton/msgsend/p;->a:Ljava/lang/String;

    const-string v1, "Auto Resend is disabled (update_messageContents)"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 392
    :goto_0
    return-void

    .line 390
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 391
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLjava/lang/String;)Z

    goto :goto_0
.end method

.method public static c()V
    .locals 1

    .prologue
    .line 215
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/l;->d()V

    .line 217
    return-void
.end method

.method public static d()V
    .locals 0

    .prologue
    .line 220
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->c()V

    .line 221
    return-void
.end method

.method private static e()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 29
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/msgsend/ChatONMessageService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method
