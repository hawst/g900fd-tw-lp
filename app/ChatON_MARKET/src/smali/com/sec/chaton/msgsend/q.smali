.class public Lcom/sec/chaton/msgsend/q;
.super Ljava/lang/Object;
.source "MsgSendHelper.java"


# static fields
.field static a:Lcom/sec/chaton/msgsend/r;

.field static b:Lcom/sec/chaton/msgsend/r;

.field static final c:Ljava/lang/Object;

.field private static final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/chaton/msgsend/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    .line 58
    sget-object v0, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    sput-object v0, Lcom/sec/chaton/msgsend/q;->a:Lcom/sec/chaton/msgsend/r;

    .line 59
    sget-object v0, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    sput-object v0, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/chaton/msgsend/q;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/io/File;
    .locals 4

    .prologue
    .line 230
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->f()Ljava/lang/String;

    move-result-object v0

    .line 231
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->h()Lcom/sec/chaton/push/PushClientApplication;

    move-result-object v1

    .line 232
    const-string v2, "trace"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContextWrapper;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 233
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 63
    sget-object v1, Lcom/sec/chaton/msgsend/q;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 64
    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    sput-object v0, Lcom/sec/chaton/msgsend/q;->a:Lcom/sec/chaton/msgsend/r;

    .line 65
    sget-object v0, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    sput-object v0, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    .line 66
    monitor-exit v1

    .line 67
    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 242
    invoke-static {p0}, Lcom/sec/chaton/msgsend/q;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 243
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm:ss.SSS (Z)"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 244
    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 245
    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 246
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 247
    const/4 v2, 0x0

    .line 249
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v4, 0x1

    invoke-direct {v1, v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 260
    if-eqz v1, :cond_0

    .line 262
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    .line 270
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 272
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9

    .line 280
    :cond_1
    :goto_1
    return-void

    .line 251
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 252
    :goto_2
    :try_start_4
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_2

    .line 253
    sget-object v2, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 260
    :cond_2
    if-eqz v1, :cond_3

    .line 262
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 270
    :cond_3
    :goto_3
    if-eqz v1, :cond_1

    .line 272
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 273
    :catch_1
    move-exception v0

    .line 274
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 275
    sget-object v1, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    :goto_4
    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 255
    :catch_2
    move-exception v0

    move-object v1, v2

    .line 256
    :goto_5
    :try_start_7
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_4

    .line 257
    sget-object v2, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 260
    :cond_4
    if-eqz v1, :cond_5

    .line 262
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 270
    :cond_5
    :goto_6
    if-eqz v1, :cond_1

    .line 272
    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_1

    .line 273
    :catch_3
    move-exception v0

    .line 274
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 275
    sget-object v1, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    goto :goto_4

    .line 260
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_7
    if-eqz v1, :cond_6

    .line 262
    :try_start_a
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 270
    :cond_6
    :goto_8
    if-eqz v1, :cond_7

    .line 272
    :try_start_b
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    .line 260
    :cond_7
    :goto_9
    throw v0

    .line 263
    :catch_4
    move-exception v2

    .line 264
    sget-boolean v3, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v3, :cond_6

    .line 265
    sget-object v3, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_8

    .line 273
    :catch_5
    move-exception v1

    .line 274
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_7

    .line 275
    sget-object v2, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_9

    .line 263
    :catch_6
    move-exception v0

    .line 264
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_3

    .line 265
    sget-object v2, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_3

    .line 263
    :catch_7
    move-exception v0

    .line 264
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_5

    .line 265
    sget-object v2, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_6

    .line 263
    :catch_8
    move-exception v0

    .line 264
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 265
    sget-object v2, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 273
    :catch_9
    move-exception v0

    .line 274
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 275
    sget-object v1, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    goto :goto_4

    .line 260
    :catchall_1
    move-exception v0

    goto :goto_7

    .line 255
    :catch_a
    move-exception v0

    goto :goto_5

    .line 251
    :catch_b
    move-exception v0

    goto/16 :goto_2
.end method

.method public static a(Z)V
    .locals 5

    .prologue
    .line 313
    sget-object v0, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    const-string v1, "setNotiEnable? -> (%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 314
    if-eqz p0, :cond_0

    .line 315
    sget-object v0, Lcom/sec/chaton/msgsend/r;->c:Lcom/sec/chaton/msgsend/r;

    sput-object v0, Lcom/sec/chaton/msgsend/q;->a:Lcom/sec/chaton/msgsend/r;

    .line 321
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "noti_for_autoresend"

    sget-object v2, Lcom/sec/chaton/msgsend/q;->a:Lcom/sec/chaton/msgsend/r;

    invoke-virtual {v2}, Lcom/sec/chaton/msgsend/r;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 322
    return-void

    .line 317
    :cond_0
    sget-object v0, Lcom/sec/chaton/msgsend/r;->b:Lcom/sec/chaton/msgsend/r;

    sput-object v0, Lcom/sec/chaton/msgsend/q;->a:Lcom/sec/chaton/msgsend/r;

    .line 318
    invoke-static {}, Lcom/sec/chaton/msgsend/t;->a()V

    goto :goto_0
.end method

.method public static b()Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 71
    sget-object v3, Lcom/sec/chaton/msgsend/q;->c:Ljava/lang/Object;

    monitor-enter v3

    .line 74
    :try_start_0
    sget-object v2, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    sget-object v4, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    if-ne v2, v4, :cond_1

    .line 75
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v4, "autoresend_enable"

    sget-object v5, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    invoke-virtual {v5}, Lcom/sec/chaton/msgsend/r;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 76
    invoke-static {v2}, Lcom/sec/chaton/msgsend/r;->a(I)Lcom/sec/chaton/msgsend/r;

    move-result-object v2

    .line 77
    sget-object v4, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    const-string v5, "Read AUTO-RESEND preference, %s "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    sget-object v4, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    if-ne v2, v4, :cond_0

    .line 81
    sget-object v2, Lcom/sec/chaton/msgsend/r;->b:Lcom/sec/chaton/msgsend/r;

    .line 84
    :cond_0
    sget-object v4, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    const-string v5, "Set AUTO-RESEND value in-memory, %s -> %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v2, v6, v7

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    sput-object v2, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    .line 90
    :cond_1
    sget-object v2, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    sget-object v4, Lcom/sec/chaton/msgsend/r;->c:Lcom/sec/chaton/msgsend/r;

    if-ne v2, v4, :cond_2

    .line 95
    :goto_0
    monitor-exit v3

    return v0

    :cond_2
    move v0, v1

    .line 93
    goto :goto_0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(Z)Z
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 182
    new-array v0, v1, [Ljava/lang/String;

    const-string v3, "KR"

    aput-object v3, v0, v2

    .line 183
    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "450"

    aput-object v4, v3, v2

    .line 184
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 185
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 187
    const-string v3, "[INFO] NOT USE AUTO RESEND COUNTRY IS : (ISO) %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v4, v5, v2

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 188
    const-string v5, "[INFO] NOT USE AUTO RESEND COUNTRY IS : (MCC) %s"

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v0, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 189
    sget-object v6, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v6, v3, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    sget-object v6, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v6, v5, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "account_country_code"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 194
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v7

    const-string v8, "country_ISO"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 195
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v8

    const-string v9, "country_letter"

    const-string v10, ""

    invoke-virtual {v8, v9, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 198
    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v9

    .line 201
    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v10

    .line 204
    invoke-interface {v4, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    invoke-interface {v4, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    invoke-interface {v4, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v0, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v0, v10}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 212
    :goto_0
    const-string v4, "[RESULT] Check ISO Country [ENABLE ? : %s] : SA(%s), Phone(%s), skip(%s), SimMCC(%s), NetMCC(%s)"

    const/4 v11, 0x6

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v11, v2

    aput-object v6, v11, v1

    const/4 v1, 0x2

    aput-object v7, v11, v1

    const/4 v1, 0x3

    aput-object v8, v11, v1

    const/4 v1, 0x4

    aput-object v9, v11, v1

    const/4 v1, 0x5

    aput-object v10, v11, v1

    invoke-static {v4, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 214
    sget-object v4, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    if-eqz p0, :cond_1

    .line 217
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 218
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    .line 223
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/msgsend/q;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 226
    :cond_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static c()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 118
    sget-object v1, Lcom/sec/chaton/msgsend/q;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 119
    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    .line 120
    sget-object v2, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    const-string v3, "turnOnAutoResend(), previous(%s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    sget-object v2, Lcom/sec/chaton/msgsend/r;->c:Lcom/sec/chaton/msgsend/r;

    sput-object v2, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    .line 123
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "autoresend_enable"

    sget-object v4, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    invoke-virtual {v4}, Lcom/sec/chaton/msgsend/r;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 124
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    sget-object v1, Lcom/sec/chaton/msgsend/r;->b:Lcom/sec/chaton/msgsend/r;

    if-ne v0, v1, :cond_0

    .line 128
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->c()V

    .line 129
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->a()V

    .line 132
    :cond_0
    return v6

    .line 124
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static d()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 138
    sget-object v1, Lcom/sec/chaton/msgsend/q;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    .line 140
    sget-object v2, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    const-string v3, "turnOffAutoResend(), previous(%s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    sget-object v2, Lcom/sec/chaton/msgsend/r;->b:Lcom/sec/chaton/msgsend/r;

    sput-object v2, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    .line 143
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "autoresend_enable"

    sget-object v4, Lcom/sec/chaton/msgsend/q;->b:Lcom/sec/chaton/msgsend/r;

    invoke-virtual {v4}, Lcom/sec/chaton/msgsend/r;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 144
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    sget-object v1, Lcom/sec/chaton/msgsend/r;->c:Lcom/sec/chaton/msgsend/r;

    if-ne v0, v1, :cond_0

    .line 152
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->c()V

    .line 155
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 157
    invoke-static {v0}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;)I

    .line 159
    invoke-static {}, Lcom/sec/chaton/msgsend/p;->b()V

    .line 162
    :cond_0
    return v6

    .line 144
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static e()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 171
    invoke-static {v3}, Lcom/sec/chaton/msgsend/q;->b(Z)Z

    move-result v0

    .line 172
    sget-object v1, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    const-string v2, "checkAndSetUseAutoResend() - set : result (%s)"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    if-eqz v0, :cond_0

    .line 174
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->c()Z

    .line 178
    :goto_0
    return-void

    .line 176
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->d()Z

    goto :goto_0
.end method

.method public static f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    const-string v0, "auto_resend_activation_trace.txt"

    return-object v0
.end method

.method public static g()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 288
    sget-object v0, Lcom/sec/chaton/msgsend/q;->a:Lcom/sec/chaton/msgsend/r;

    sget-object v3, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    if-ne v0, v3, :cond_1

    .line 289
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "noti_for_autoresend"

    sget-object v4, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    invoke-virtual {v4}, Lcom/sec/chaton/msgsend/r;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 290
    invoke-static {v0}, Lcom/sec/chaton/msgsend/r;->a(I)Lcom/sec/chaton/msgsend/r;

    move-result-object v0

    .line 292
    sget-object v3, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    const-string v4, "Read NOTI-AUTO-RESEND preference, %s "

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 294
    sget-object v3, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    if-ne v0, v3, :cond_0

    .line 295
    sget-object v0, Lcom/sec/chaton/msgsend/r;->b:Lcom/sec/chaton/msgsend/r;

    .line 298
    :cond_0
    sget-object v3, Lcom/sec/chaton/msgsend/q;->d:Ljava/lang/String;

    const-string v4, "Set NOTI-AUTO-RESEND value in-memory, %s -> %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, Lcom/sec/chaton/msgsend/q;->a:Lcom/sec/chaton/msgsend/r;

    aput-object v6, v5, v2

    aput-object v0, v5, v1

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 299
    sput-object v0, Lcom/sec/chaton/msgsend/q;->a:Lcom/sec/chaton/msgsend/r;

    .line 304
    :cond_1
    sget-object v0, Lcom/sec/chaton/msgsend/q;->a:Lcom/sec/chaton/msgsend/r;

    sget-object v3, Lcom/sec/chaton/msgsend/r;->c:Lcom/sec/chaton/msgsend/r;

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 309
    :goto_0
    return v0

    :cond_2
    move v0, v2

    .line 307
    goto :goto_0
.end method
