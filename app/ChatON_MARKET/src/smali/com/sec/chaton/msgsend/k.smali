.class public final enum Lcom/sec/chaton/msgsend/k;
.super Ljava/lang/Enum;
.source "IgnoreUseAutoResend.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/msgsend/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/msgsend/k;

.field public static final enum b:Lcom/sec/chaton/msgsend/k;

.field public static final d:Lcom/sec/chaton/msgsend/k;

.field private static final synthetic e:[Lcom/sec/chaton/msgsend/k;


# instance fields
.field c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lcom/sec/chaton/msgsend/k;

    const-string v1, "YES"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/sec/chaton/msgsend/k;->a:Lcom/sec/chaton/msgsend/k;

    .line 9
    new-instance v0, Lcom/sec/chaton/msgsend/k;

    const-string v1, "NO"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/msgsend/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/sec/chaton/msgsend/k;->b:Lcom/sec/chaton/msgsend/k;

    .line 7
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/msgsend/k;

    sget-object v1, Lcom/sec/chaton/msgsend/k;->a:Lcom/sec/chaton/msgsend/k;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/msgsend/k;->b:Lcom/sec/chaton/msgsend/k;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/msgsend/k;->e:[Lcom/sec/chaton/msgsend/k;

    .line 21
    sget-object v0, Lcom/sec/chaton/msgsend/k;->b:Lcom/sec/chaton/msgsend/k;

    sput-object v0, Lcom/sec/chaton/msgsend/k;->d:Lcom/sec/chaton/msgsend/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput-boolean p3, p0, Lcom/sec/chaton/msgsend/k;->c:Z

    .line 15
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/msgsend/k;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/sec/chaton/msgsend/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/k;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/msgsend/k;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/sec/chaton/msgsend/k;->e:[Lcom/sec/chaton/msgsend/k;

    invoke-virtual {v0}, [Lcom/sec/chaton/msgsend/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/msgsend/k;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/sec/chaton/msgsend/k;->c:Z

    return v0
.end method
