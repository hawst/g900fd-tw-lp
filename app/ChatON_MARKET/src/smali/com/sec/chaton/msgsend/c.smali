.class public Lcom/sec/chaton/msgsend/c;
.super Ljava/lang/Object;
.source "ChatONChatRoomEntity.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/sec/chaton/e/r;

.field private d:[Ljava/lang/String;

.field private e:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/chaton/msgsend/ChatONMsgEntity;",
            ">;"
        }
    .end annotation
.end field

.field private f:J

.field private g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/chaton/msgsend/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->f:J

    .line 44
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->g:J

    .line 45
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->h:J

    .line 46
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->i:J

    .line 47
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->j:J

    .line 48
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->k:J

    .line 148
    iput-object p1, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    .line 149
    iput-object p2, p0, Lcom/sec/chaton/msgsend/c;->c:Lcom/sec/chaton/e/r;

    .line 151
    iput-object p3, p0, Lcom/sec/chaton/msgsend/c;->d:[Ljava/lang/String;

    .line 154
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    .line 159
    return-void
.end method

.method private a(Lcom/sec/chaton/d/o;Lcom/sec/chaton/msgsend/w;)Lcom/sec/chaton/msgsend/w;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 561
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "sendAllInboxMsgByUnit()"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 562
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 563
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 565
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 567
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 569
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 570
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 571
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 572
    sget-object v3, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v4, " Check to send (One Unit): (No need to resend msg) %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 576
    :cond_0
    sget-object v3, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v4, " Check to send (One Unit): %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 580
    const-wide/16 v3, 0x32

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 587
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/chaton/msgsend/c;->d:[Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/msgsend/ChatONMsgEntity;[Ljava/lang/String;)Z

    move-result v3

    .line 588
    if-eqz v3, :cond_2

    .line 589
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a()V

    .line 595
    :goto_2
    iget v0, p2, Lcom/sec/chaton/msgsend/w;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/sec/chaton/msgsend/w;->b:I

    goto :goto_0

    .line 581
    :catch_0
    move-exception v3

    .line 582
    sget-boolean v4, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v4, :cond_1

    .line 583
    sget-object v4, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 592
    :cond_2
    sget-object v3, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a(Lcom/sec/chaton/msgsend/aa;)Lcom/sec/chaton/msgsend/aa;

    .line 593
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 597
    :cond_3
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "Request to send (%d/%d)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p2, Lcom/sec/chaton/msgsend/w;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget v4, p2, Lcom/sec/chaton/msgsend/w;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 599
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 600
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 601
    sget-object v2, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "Remove INVALID MESSAGE : %s "

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 604
    :cond_4
    return-object p2
.end method

.method private b(Lcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/ChatONMsgEntity;
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 366
    return-object v0
.end method

.method private b(Lcom/sec/chaton/d/o;Lcom/sec/chaton/msgsend/w;)Lcom/sec/chaton/msgsend/w;
    .locals 13

    .prologue
    .line 622
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "sendAllInboxMsgByUsingBulk()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 623
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 626
    invoke-virtual {p1}, Lcom/sec/chaton/d/o;->d()Ljava/lang/String;

    move-result-object v3

    .line 629
    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v4

    .line 631
    array-length v5, v4

    .line 632
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 633
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 635
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_8

    .line 636
    aget-object v0, v4, v2

    check-cast v0, Ljava/util/Map$Entry;

    .line 637
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 638
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 639
    sget-object v1, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v8, " Check to send (Bulk check): (No need to resend msg) %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-static {v1, v8, v9}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 635
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 643
    :cond_1
    sget-object v1, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v8, " Check to send (Bulk check): %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-static {v1, v8, v9}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 646
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->f()Lcom/sec/chaton/e/w;

    move-result-object v1

    sget-object v8, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v1, v8, :cond_2

    .line 647
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->f()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-gtz v1, :cond_0

    .line 648
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a()V

    .line 649
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 653
    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 654
    if-lez v8, :cond_3

    .line 655
    iget v1, p2, Lcom/sec/chaton/msgsend/w;->b:I

    add-int/2addr v1, v8

    iput v1, p2, Lcom/sec/chaton/msgsend/w;->b:I

    .line 656
    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->c:Lcom/sec/chaton/e/r;

    iget-object v9, p0, Lcom/sec/chaton/msgsend/c;->d:[Ljava/lang/String;

    invoke-virtual {p1, v1, v3, v9, v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/util/ArrayList;)J

    .line 657
    add-int/lit8 v1, v8, -0x1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 658
    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    .line 659
    invoke-virtual {p0, v9, v10}, Lcom/sec/chaton/msgsend/c;->a(J)V

    .line 661
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 663
    mul-int/lit8 v1, v8, 0xa

    .line 664
    const/16 v8, 0xa

    if-ge v1, v8, :cond_4

    .line 665
    const/16 v1, 0xa

    .line 667
    :cond_4
    const/16 v8, 0x3e8

    if-le v1, v8, :cond_5

    .line 668
    const/16 v1, 0x3e8

    .line 671
    :cond_5
    :try_start_0
    sget-object v8, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v9, "sendAllInboxMsgByUsingBulk(), sleep a little time ...  : properDelay(%d ms)"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 672
    int-to-long v8, v1

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 680
    :cond_6
    :goto_2
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a()V

    .line 681
    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->d:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/msgsend/ChatONMsgEntity;[Ljava/lang/String;)Z

    move-result v1

    .line 682
    if-nez v1, :cond_7

    .line 683
    sget-object v1, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a(Lcom/sec/chaton/msgsend/aa;)Lcom/sec/chaton/msgsend/aa;

    .line 684
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 686
    :cond_7
    iget v0, p2, Lcom/sec/chaton/msgsend/w;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/sec/chaton/msgsend/w;->b:I

    goto/16 :goto_1

    .line 673
    :catch_0
    move-exception v1

    .line 674
    sget-boolean v8, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v8, :cond_6

    .line 675
    sget-object v8, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 691
    :cond_8
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 692
    if-lez v0, :cond_9

    .line 693
    iget v1, p2, Lcom/sec/chaton/msgsend/w;->b:I

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p2, Lcom/sec/chaton/msgsend/w;->b:I

    .line 694
    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->c:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/msgsend/c;->d:[Ljava/lang/String;

    invoke-virtual {p1, v1, v3, v2, v6}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;[Ljava/lang/String;Ljava/util/ArrayList;)J

    .line 695
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 696
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 697
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/msgsend/c;->a(J)V

    .line 699
    :cond_9
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 701
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "Request to send (%d/%d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p2, Lcom/sec/chaton/msgsend/w;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p2, Lcom/sec/chaton/msgsend/w;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 703
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 704
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 705
    sget-object v2, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "Remove INVALID MESSAGE : %s "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 708
    :cond_a
    return-object p2
.end method

.method private c(J)Z
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 135
    if-nez v0, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 143
    :goto_0
    return v0

    .line 139
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->l()Lcom/sec/chaton/msgsend/aa;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/msgsend/aa;->d:Lcom/sec/chaton/msgsend/aa;

    if-ne v1, v2, :cond_1

    .line 140
    sget-object v1, Lcom/sec/chaton/msgsend/aa;->c:Lcom/sec/chaton/msgsend/aa;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a(Lcom/sec/chaton/msgsend/aa;)Lcom/sec/chaton/msgsend/aa;

    .line 141
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    .line 143
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private i()Z
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 52
    iget-wide v2, p0, Lcom/sec/chaton/msgsend/c;->f:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 53
    invoke-static {}, Lcom/sec/chaton/c/g;->c()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    int-to-long v2, v2

    .line 56
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 57
    iget-wide v6, p0, Lcom/sec/chaton/msgsend/c;->f:J

    sub-long v6, v4, v6

    .line 59
    cmp-long v8, v6, v2

    if-lez v8, :cond_2

    .line 60
    sget-object v8, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v9, "IsInProgressAllowChat(), force to release, diff(%d) = current(%d) - request(%d), wait for msgId(%d), Threshold(%d)"

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v10, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v10, v1

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v10, v11

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v10, v12

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v10, v13

    invoke-static {v8, v9, v10}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    const-string v1, "force to release ALLOW-CHAT"

    invoke-static {}, Lcom/sec/chaton/msgsend/l;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/msgsend/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_0
    iget-wide v1, p0, Lcom/sec/chaton/msgsend/c;->i:J

    invoke-direct {p0, v1, v2}, Lcom/sec/chaton/msgsend/c;->c(J)Z

    .line 66
    const-wide/16 v1, -0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/msgsend/c;->a(ZJ)V

    .line 76
    :cond_1
    :goto_0
    return v0

    .line 69
    :cond_2
    sget-object v8, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v9, "IsInProgressAllowChat(), wait more ... , diff(%d) = current(%d) - request(%d), wait for msgId(%d), Threshold(%d)"

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v10, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v1

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v11

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v12

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v13

    invoke-static {v8, v9, v10}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 71
    goto :goto_0
.end method

.method private j()Z
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 81
    iget-wide v2, p0, Lcom/sec/chaton/msgsend/c;->g:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 82
    invoke-static {}, Lcom/sec/chaton/c/g;->e()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    int-to-long v2, v2

    .line 84
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 85
    iget-wide v6, p0, Lcom/sec/chaton/msgsend/c;->g:J

    sub-long v6, v4, v6

    .line 86
    cmp-long v8, v6, v2

    if-lez v8, :cond_2

    .line 87
    sget-object v8, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v9, "IsInProgressInitChat(), force to elease, diff(%d) = current(%d) - request(%d), wait for msgId(%d), Threshold(%d)"

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v10, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v10, v1

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v10, v11

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v10, v12

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v10, v13

    invoke-static {v8, v9, v10}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    const-string v1, "force to release INIT-CHAT"

    invoke-static {}, Lcom/sec/chaton/msgsend/l;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/msgsend/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_0
    iget-wide v1, p0, Lcom/sec/chaton/msgsend/c;->j:J

    invoke-direct {p0, v1, v2}, Lcom/sec/chaton/msgsend/c;->c(J)Z

    .line 93
    const-wide/16 v1, -0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/msgsend/c;->b(ZJ)V

    .line 103
    :cond_1
    :goto_0
    return v0

    .line 96
    :cond_2
    sget-object v8, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v9, "IsInProgressInitChat(), wait more ... , diff(%d) = current(%d) - request(%d), wait for msgId(%d), Threshold(%d)"

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v10, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v1

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v11

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v12

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v13

    invoke-static {v8, v9, v10}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 98
    goto :goto_0
.end method

.method private k()Z
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 108
    iget-wide v2, p0, Lcom/sec/chaton/msgsend/c;->k:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 109
    invoke-static {}, Lcom/sec/chaton/c/g;->d()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    int-to-long v2, v2

    .line 111
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 112
    iget-wide v6, p0, Lcom/sec/chaton/msgsend/c;->h:J

    sub-long v6, v4, v6

    .line 113
    cmp-long v8, v6, v2

    if-lez v8, :cond_2

    .line 114
    sget-object v8, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v9, "IsInProgressSerialChat(), force to elease, diff(%d) = current(%d) - request(%d), wait for msgId(%d), Threshold(%d)"

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v10, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v10, v1

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v10, v11

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->k:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v10, v12

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v10, v13

    invoke-static {v8, v9, v10}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    const-string v1, "force to release SERIAL-CHAT"

    invoke-static {}, Lcom/sec/chaton/msgsend/l;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/msgsend/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    iget-wide v1, p0, Lcom/sec/chaton/msgsend/c;->k:J

    invoke-direct {p0, v1, v2}, Lcom/sec/chaton/msgsend/c;->c(J)Z

    .line 120
    const-wide/16 v1, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/msgsend/c;->a(J)V

    .line 130
    :cond_1
    :goto_0
    return v0

    .line 123
    :cond_2
    sget-object v8, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v9, "IsInProgressSerialChat(), wait more ... , diff(%d) = current(%d) - request(%d), wait for msgId(%d), Threshold(%d)"

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v10, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v1

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v11

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->k:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v12

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v10, v13

    invoke-static {v8, v9, v10}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 125
    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1012
    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    if-gtz v1, :cond_2

    .line 1013
    :cond_0
    sget-object v1, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "This inbox(%s) has no msg"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1045
    :cond_1
    :goto_0
    return v0

    .line 1017
    :cond_2
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1021
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v2

    .line 1023
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 1024
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1026
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1027
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1028
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1029
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 1031
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->l()Lcom/sec/chaton/msgsend/aa;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1035
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1039
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 1040
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1041
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 1042
    sget-object v4, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v5, "Remove Request MESSAGE : %s "

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v0, v6, v7

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 1044
    :cond_5
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "Remove Request MESSAGE result : total(%d), %s "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 1045
    goto/16 :goto_0
.end method

.method public a(Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1083
    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    if-gtz v1, :cond_2

    .line 1084
    :cond_0
    sget-object v1, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "This inbox(%s) has no msg"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1118
    :cond_1
    :goto_0
    return v0

    .line 1088
    :cond_2
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1092
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v2

    .line 1094
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 1095
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1097
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1098
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1099
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1100
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 1102
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1106
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->l()Lcom/sec/chaton/msgsend/aa;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/msgsend/aa;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1107
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1112
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 1113
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1114
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 1115
    sget-object v4, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v5, "Remove Request MESSAGE : %s "

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v0, v6, v7

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 1117
    :cond_5
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "Remove Request MESSAGE result : total(%d), %s "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 1118
    goto/16 :goto_0
.end method

.method public a()Lcom/sec/chaton/e/r;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->c:Lcom/sec/chaton/e/r;

    return-object v0
.end method

.method public a(J)V
    .locals 6

    .prologue
    .line 608
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 609
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->h:J

    .line 613
    :goto_0
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "setSerialChatLastMember(), MsgID (%d) -> (%d), requestTime(%d)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->k:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 614
    iput-wide p1, p0, Lcom/sec/chaton/msgsend/c;->k:J

    .line 615
    return-void

    .line 611
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->h:J

    goto :goto_0
.end method

.method public a(ZJ)V
    .locals 8

    .prologue
    const-wide/16 v0, -0x1

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 271
    if-ne p1, v4, :cond_0

    .line 272
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->f:J

    .line 273
    iput-wide p2, p0, Lcom/sec/chaton/msgsend/c;->i:J

    .line 274
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "setInProgressAllowChat(), inProgressAllowChat(%s), mInboxNo(%s), msgId(%d)"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 281
    :goto_0
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "setInProgressAllowChat(), ---- set result (inboxNo:%s) : inProgressAllowChat(%s) msgId(%d), requestTime(%d) ---- "

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/c;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    return-void

    .line 277
    :cond_0
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->f:J

    .line 278
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->i:J

    goto :goto_0
.end method

.method a([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/chaton/msgsend/c;->d:[Ljava/lang/String;

    .line 167
    return-void
.end method

.method public a(JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 906
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 907
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "This inbox is NOT AVAILABLE"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 970
    :goto_0
    return v1

    .line 911
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 912
    if-nez v0, :cond_1

    .line 913
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "not exist!!"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 918
    :cond_1
    sget-object v3, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v4, "UpdateMsg() #1 [Current] : %s"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v0, v5, v1

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 919
    sget-object v3, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v4, "UpdateMsg() #2 [Change New] : Status(%s->%s) / FormattedMsg (%s->%s) / IsFileUploaded (%d->%d)"

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->l()Lcom/sec/chaton/msgsend/aa;

    move-result-object v6

    aput-object v6, v5, v1

    aput-object p3, v5, v2

    const/4 v6, 0x2

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->j()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    aput-object p4, v5, v6

    const/4 v6, 0x4

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->k()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x5

    aput-object p5, v5, v6

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 924
    if-eqz p3, :cond_2

    sget-object v3, Lcom/sec/chaton/msgsend/aa;->a:Lcom/sec/chaton/msgsend/aa;

    if-eq p3, v3, :cond_2

    .line 925
    invoke-virtual {v0, p3}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a(Lcom/sec/chaton/msgsend/aa;)Lcom/sec/chaton/msgsend/aa;

    .line 926
    sget-object v3, Lcom/sec/chaton/msgsend/d;->a:[I

    invoke-virtual {p3}, Lcom/sec/chaton/msgsend/aa;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 936
    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a(Z)V

    .line 943
    :cond_2
    :pswitch_0
    if-eqz p4, :cond_3

    .line 944
    invoke-virtual {v0, p4}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 949
    :cond_3
    if-eqz p5, :cond_4

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ltz v3, :cond_4

    .line 950
    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a(I)I

    .line 954
    :cond_4
    if-eqz p3, :cond_6

    sget-object v3, Lcom/sec/chaton/msgsend/aa;->e:Lcom/sec/chaton/msgsend/aa;

    if-ne p3, v3, :cond_6

    .line 957
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->b()V

    .line 958
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/msgsend/c;->b(J)Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    move-result-object v0

    .line 962
    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    move v1, v0

    .line 967
    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 965
    goto :goto_1

    :cond_6
    move v1, v2

    .line 970
    goto/16 :goto_0

    .line 926
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public a(JLjava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 886
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 887
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "This inbox is NOT AVAILABLE"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 902
    :goto_0
    return v0

    .line 891
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 892
    if-nez v0, :cond_1

    .line 893
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "not exist!!"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 894
    goto :goto_0

    .line 898
    :cond_1
    sget-object v3, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v4, "UpdateMsg() #1 [Current] : %s"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v0, v5, v1

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 899
    sget-object v3, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v4, "UpdateMsg() #2 [Change New] : chatMsg(%s->%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->g()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    aput-object p3, v5, v2

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 900
    invoke-virtual {v0, p3}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a(Ljava/lang/String;)V

    move v0, v2

    .line 902
    goto :goto_0
.end method

.method a(Lcom/sec/chaton/j/ak;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 245
    if-nez p1, :cond_1

    .line 255
    :cond_0
    :goto_0
    return v0

    .line 249
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/j/ak;->d()Ljava/lang/String;

    move-result-object v1

    .line 250
    invoke-virtual {p1}, Lcom/sec/chaton/j/ak;->e()I

    move-result v2

    .line 251
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-gtz v2, :cond_0

    .line 255
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/msgsend/ChatONMsgEntity;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 374
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 375
    sget-object v1, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "This inbox is NOT AVAILABLE"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 397
    :goto_0
    :pswitch_0
    return v0

    .line 379
    :cond_0
    sget-object v2, Lcom/sec/chaton/msgsend/d;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->l()Lcom/sec/chaton/msgsend/aa;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/msgsend/aa;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 389
    invoke-direct {p0, p1}, Lcom/sec/chaton/msgsend/c;->b(Lcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    move-result-object v2

    .line 390
    sget-object v3, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v4, "new msg entity %s "

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p1, v5, v0

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 392
    if-nez v2, :cond_1

    move v0, v1

    .line 393
    goto :goto_0

    .line 396
    :cond_1
    sget-object v3, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v4, "exist msg entity : %s "

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v2, v5, v0

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 397
    goto :goto_0

    .line 379
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;IJ)Z
    .locals 7

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->c:Lcom/sec/chaton/e/r;

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v6

    .line 180
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "setReadyMessageControl(), (requested) in inbox(%s): sessionId(%s), serverIp(%s), serverPort(%d)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    const/4 v3, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    invoke-virtual {v6, p1}, Lcom/sec/chaton/d/o;->b(Ljava/lang/String;)Z

    .line 185
    invoke-virtual {v6, p4, p5}, Lcom/sec/chaton/d/o;->b(J)V

    .line 186
    invoke-virtual {v6}, Lcom/sec/chaton/d/o;->f()Lcom/sec/chaton/j/ak;

    move-result-object v0

    .line 187
    invoke-virtual {p0, v0}, Lcom/sec/chaton/msgsend/c;->a(Lcom/sec/chaton/j/ak;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    sget-object v1, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "setReadyMessageControl(), (Last result) #1 has valid TcpContext already (%s)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    const/4 v0, 0x1

    .line 241
    :goto_0
    return v0

    .line 193
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-lez p3, :cond_1

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    invoke-virtual {v6, v0, p2, p3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 195
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 196
    const-wide/32 v2, 0x75300

    add-long/2addr v0, v2

    invoke-virtual {v6, v0, v1}, Lcom/sec/chaton/d/o;->c(J)V

    .line 197
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "setReadyMessageControl(), (Last result) #2 set by using params, serverIp(%s), serverPot(%d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    const/4 v0, 0x1

    goto :goto_0

    .line 205
    :cond_1
    const-string v3, "inbox_no=?"

    .line 206
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v1, v4, v0

    .line 207
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "inbox_server_ip"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "inbox_server_port"

    aput-object v1, v2, v0

    .line 208
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 209
    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 211
    if-eqz v0, :cond_2

    .line 212
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 213
    const-string v1, "inbox_server_ip"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 214
    const-string v1, "inbox_server_port"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result p3

    .line 215
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 219
    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    if-lez p3, :cond_3

    .line 220
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "setReadyMessageControl(), (ongoing result) #3 set by using DB value, serverIp(%s), serverPot(%d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    invoke-virtual {v6, v0, p2, p3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 235
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 236
    const-wide/32 v2, 0x75300

    add-long/2addr v0, v2

    invoke-virtual {v6, v0, v1}, Lcom/sec/chaton/d/o;->c(J)V

    .line 238
    invoke-virtual {v6}, Lcom/sec/chaton/d/o;->f()Lcom/sec/chaton/j/ak;

    move-result-object v0

    .line 239
    sget-object v1, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "SET READY Message Control, (Last Result) in inbox(%s): new TcpContext (%s) "

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 241
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 223
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object p2

    .line 224
    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bk;->b()I

    move-result p3

    .line 225
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    if-lez p3, :cond_4

    .line 226
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "setReadyMessageControl(), (ongoing result) #4 set by using GLD value, serverIp(%s), serverPot(%d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 229
    :cond_4
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "[ERROR] setReadyMessageControl(), (Last Result) Nothing to use even GLD"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 230
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public b(Ljava/util/ArrayList;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1049
    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    if-gtz v1, :cond_2

    .line 1050
    :cond_0
    sget-object v1, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "This inbox(%s) has no msg"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1079
    :cond_1
    :goto_0
    return v0

    .line 1054
    :cond_2
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1058
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v2

    .line 1060
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 1061
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1063
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1064
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1065
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1066
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 1068
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->l()Lcom/sec/chaton/msgsend/aa;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1069
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1073
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 1074
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1075
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 1076
    sget-object v4, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v5, "Remove Request MESSAGE : %s "

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v0, v6, v7

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 1078
    :cond_5
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "Remove Request MESSAGE result : total(%d), %s "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 1079
    goto/16 :goto_0
.end method

.method public b(Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1122
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 1123
    :cond_0
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "This inbox(%s) has no msg"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1149
    :cond_1
    :goto_0
    return v1

    .line 1127
    :cond_2
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1131
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v3

    .line 1134
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 1135
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 1136
    if-eqz v0, :cond_4

    .line 1137
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->l()Lcom/sec/chaton/msgsend/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/aa;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1139
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 1140
    sget-object v5, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v6, "Remove Request MESSAGE : %s "

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v0, v7, v2

    invoke-static {v5, v6, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1141
    if-eqz v0, :cond_4

    .line 1142
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_2
    move v1, v0

    .line 1146
    goto :goto_1

    .line 1148
    :cond_3
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v4, "Remove Request MESSAGE result : total(%d), %s "

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-static {v0, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public b(J)Lcom/sec/chaton/msgsend/ChatONMsgEntity;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 974
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 975
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "This inbox is NOT AVAILABLE"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 976
    const/4 v0, 0x0

    .line 986
    :goto_0
    return-object v0

    .line 979
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 985
    sget-object v1, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v2, "REMOVE MSG (inboxNo(%s), requestMsgId(%d)) : remove result (%s)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v4, v3, v5

    const/4 v4, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method b()V
    .locals 5

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    .line 174
    :cond_0
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "release(), %s MSG all are released"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    return-void
.end method

.method public b(ZJ)V
    .locals 9

    .prologue
    const-wide/16 v2, -0x1

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->c:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_0

    .line 292
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "setInProgressInitChat(), IGNORED - BROADCAST ROOM,  inProgressInitChat(%s), mInboxNo(%s), msgId(%d)"

    new-array v2, v8, [Ljava/lang/Object;

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/c;->j:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    :goto_0
    return-void

    .line 296
    :cond_0
    if-ne p1, v5, :cond_1

    .line 297
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/c;->g:J

    .line 298
    iput-wide p2, p0, Lcom/sec/chaton/msgsend/c;->j:J

    .line 299
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "setInProgressInitChat(), inProgressInitChat(%s), mInboxNo(%s), msgId(%d)"

    new-array v2, v8, [Ljava/lang/Object;

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/c;->j:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    :goto_1
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "setInProgressInitChat(),  ---- set result (%s), msgId(%d) : reqInProgress(%s), nowInProgress(%d) ---- "

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/c;->g:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 302
    :cond_1
    iput-wide v2, p0, Lcom/sec/chaton/msgsend/c;->g:J

    .line 303
    iput-wide v2, p0, Lcom/sec/chaton/msgsend/c;->j:J

    goto :goto_1
.end method

.method public c()J
    .locals 2

    .prologue
    .line 263
    iget-wide v0, p0, Lcom/sec/chaton/msgsend/c;->j:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 267
    iget-wide v0, p0, Lcom/sec/chaton/msgsend/c;->i:J

    return-wide v0
.end method

.method e()Z
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_0

    .line 323
    const/4 v0, 0x0

    .line 325
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 618
    iget-wide v0, p0, Lcom/sec/chaton/msgsend/c;->k:J

    return-wide v0
.end method

.method public g()Lcom/sec/chaton/msgsend/w;
    .locals 13

    .prologue
    const-wide/16 v11, -0x1

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 713
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "sendAllInboxMsg()"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 714
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 715
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "This inbox(%s) is NOT AVAILABLE"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 716
    new-instance v0, Lcom/sec/chaton/msgsend/w;

    sget-object v1, Lcom/sec/chaton/msgsend/v;->i:Lcom/sec/chaton/msgsend/v;

    invoke-direct {v0, v1}, Lcom/sec/chaton/msgsend/w;-><init>(Lcom/sec/chaton/msgsend/v;)V

    .line 860
    :goto_0
    return-object v0

    .line 719
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 720
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "This inbox(%s) has no msg"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 721
    new-instance v0, Lcom/sec/chaton/msgsend/w;

    sget-object v1, Lcom/sec/chaton/msgsend/v;->b:Lcom/sec/chaton/msgsend/v;

    invoke-direct {v0, v1}, Lcom/sec/chaton/msgsend/w;-><init>(Lcom/sec/chaton/msgsend/v;)V

    goto :goto_0

    .line 724
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/msgsend/c;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "This inbox(%s) is IN PROGRESS INIT-CHAT-REQUEST (msgId : %d). Hold ON !!"

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->c()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 726
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->h()V

    .line 727
    new-instance v0, Lcom/sec/chaton/msgsend/w;

    sget-object v1, Lcom/sec/chaton/msgsend/v;->c:Lcom/sec/chaton/msgsend/v;

    invoke-direct {v0, v1}, Lcom/sec/chaton/msgsend/w;-><init>(Lcom/sec/chaton/msgsend/v;)V

    goto :goto_0

    .line 730
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/msgsend/c;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 731
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "This inbox(%s) is IN PROGRESS ALLOW-CHAT-REQUEST (msgId : %d). Hold ON !!"

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->d()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 732
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->h()V

    .line 733
    new-instance v0, Lcom/sec/chaton/msgsend/w;

    sget-object v1, Lcom/sec/chaton/msgsend/v;->d:Lcom/sec/chaton/msgsend/v;

    invoke-direct {v0, v1}, Lcom/sec/chaton/msgsend/w;-><init>(Lcom/sec/chaton/msgsend/v;)V

    goto :goto_0

    .line 736
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/msgsend/c;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "This inbox(%s) is IN PROGRESS SERIAL-CHAT-REQUEST (msgId : %d). Hold ON !!"

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->f()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 738
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->h()V

    .line 739
    new-instance v0, Lcom/sec/chaton/msgsend/w;

    sget-object v1, Lcom/sec/chaton/msgsend/v;->e:Lcom/sec/chaton/msgsend/v;

    invoke-direct {v0, v1}, Lcom/sec/chaton/msgsend/w;-><init>(Lcom/sec/chaton/msgsend/v;)V

    goto/16 :goto_0

    .line 742
    :cond_4
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v4

    .line 745
    new-instance v3, Lcom/sec/chaton/msgsend/w;

    sget-object v0, Lcom/sec/chaton/msgsend/v;->a:Lcom/sec/chaton/msgsend/v;

    invoke-direct {v3, v0}, Lcom/sec/chaton/msgsend/w;-><init>(Lcom/sec/chaton/msgsend/v;)V

    .line 746
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    iput v0, v3, Lcom/sec/chaton/msgsend/w;->a:I

    .line 747
    iput v2, v3, Lcom/sec/chaton/msgsend/w;->b:I

    .line 750
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/msgsend/c;->c:Lcom/sec/chaton/e/r;

    invoke-static {v0, v5}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v5

    .line 751
    if-nez v5, :cond_5

    .line 752
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "[ERROR !!] it\'s impossible to get MessageControl"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 753
    sget-object v0, Lcom/sec/chaton/msgsend/v;->g:Lcom/sec/chaton/msgsend/v;

    iput-object v0, v3, Lcom/sec/chaton/msgsend/w;->c:Lcom/sec/chaton/msgsend/v;

    move-object v0, v3

    .line 754
    goto/16 :goto_0

    .line 757
    :cond_5
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v6, "(sendAllInboxMsg : %s)"

    new-array v7, v1, [Ljava/lang/Object;

    aput-object p0, v7, v2

    invoke-static {v0, v6, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 758
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v6, "(sendAllInboxMsg with TCP Context : %s)"

    new-array v7, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/sec/chaton/d/o;->f()Lcom/sec/chaton/j/ak;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-static {v0, v6, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 761
    invoke-virtual {v5}, Lcom/sec/chaton/d/o;->q()Z

    move-result v6

    .line 762
    invoke-virtual {v5}, Lcom/sec/chaton/d/o;->f()Lcom/sec/chaton/j/ak;

    move-result-object v0

    .line 766
    if-nez v0, :cond_6

    .line 767
    sget-object v0, Lcom/sec/chaton/msgsend/v;->g:Lcom/sec/chaton/msgsend/v;

    iput-object v0, v3, Lcom/sec/chaton/msgsend/w;->c:Lcom/sec/chaton/msgsend/v;

    .line 768
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sendAllInboxMsg(), tcpContext is null, inbox is : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 769
    goto/16 :goto_0

    .line 773
    :cond_6
    invoke-static {v0}, Lcom/sec/chaton/j/af;->a(Lcom/sec/chaton/j/ak;)Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->c:Lcom/sec/chaton/e/r;

    sget-object v7, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v7, :cond_13

    move v0, v1

    .line 777
    :goto_1
    iget-object v7, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v7}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    .line 778
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 779
    if-nez v6, :cond_7

    if-eqz v0, :cond_f

    .line 780
    :cond_7
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 781
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 782
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 784
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 785
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->c()Z

    move-result v7

    if-nez v7, :cond_8

    .line 786
    sget-object v7, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v9, " Check to send (init/allow): (No need to resend msg) %s"

    new-array v10, v1, [Ljava/lang/Object;

    aput-object v0, v10, v2

    invoke-static {v7, v9, v10}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 790
    :cond_8
    sget-object v7, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v9, " Check to send (init/allow):  %s"

    new-array v10, v1, [Ljava/lang/Object;

    aput-object v0, v10, v2

    invoke-static {v7, v9, v10}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 793
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a()V

    .line 794
    if-eqz v6, :cond_9

    .line 795
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {p0, v1, v9, v10}, Lcom/sec/chaton/msgsend/c;->b(ZJ)V

    .line 800
    :goto_3
    iget-object v7, p0, Lcom/sec/chaton/msgsend/c;->d:[Ljava/lang/String;

    invoke-virtual {v5, v0, v7}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/msgsend/ChatONMsgEntity;[Ljava/lang/String;)Z

    move-result v7

    .line 801
    if-nez v7, :cond_b

    .line 802
    if-eqz v6, :cond_a

    .line 803
    invoke-virtual {p0, v2, v11, v12}, Lcom/sec/chaton/msgsend/c;->b(ZJ)V

    .line 808
    :goto_4
    sget-object v7, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    invoke-virtual {v0, v7}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a(Lcom/sec/chaton/msgsend/aa;)Lcom/sec/chaton/msgsend/aa;

    .line 809
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 797
    :cond_9
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {p0, v1, v9, v10}, Lcom/sec/chaton/msgsend/c;->a(ZJ)V

    goto :goto_3

    .line 805
    :cond_a
    invoke-virtual {p0, v2, v11, v12}, Lcom/sec/chaton/msgsend/c;->a(ZJ)V

    goto :goto_4

    .line 813
    :cond_b
    iget v0, v3, Lcom/sec/chaton/msgsend/w;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/sec/chaton/msgsend/w;->b:I

    .line 814
    if-eqz v6, :cond_d

    .line 815
    sget-object v0, Lcom/sec/chaton/msgsend/v;->c:Lcom/sec/chaton/msgsend/v;

    iput-object v0, v3, Lcom/sec/chaton/msgsend/w;->c:Lcom/sec/chaton/msgsend/v;

    .line 827
    :cond_c
    :goto_5
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 828
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 829
    sget-object v5, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v6, "Remove INVALID MESSAGE : %s "

    new-array v7, v1, [Ljava/lang/Object;

    aput-object v0, v7, v2

    invoke-static {v5, v6, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    .line 817
    :cond_d
    sget-object v0, Lcom/sec/chaton/msgsend/v;->d:Lcom/sec/chaton/msgsend/v;

    iput-object v0, v3, Lcom/sec/chaton/msgsend/w;->c:Lcom/sec/chaton/msgsend/v;

    goto :goto_5

    :cond_e
    move-object v0, v3

    .line 831
    goto/16 :goto_0

    .line 835
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    if-eqz v0, :cond_10

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v6, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 836
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    const-wide/32 v8, 0x75300

    add-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/d/o;->c(J)V

    .line 844
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->c:Lcom/sec/chaton/e/r;

    sget-object v6, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v0, v6, :cond_11

    .line 845
    invoke-direct {p0, v5, v3}, Lcom/sec/chaton/msgsend/c;->a(Lcom/sec/chaton/d/o;Lcom/sec/chaton/msgsend/w;)Lcom/sec/chaton/msgsend/w;

    move-result-object v0

    .line 851
    :goto_7
    iget v3, v0, Lcom/sec/chaton/msgsend/w;->b:I

    if-gtz v3, :cond_12

    .line 852
    sget-object v3, Lcom/sec/chaton/msgsend/v;->a:Lcom/sec/chaton/msgsend/v;

    iput-object v3, v0, Lcom/sec/chaton/msgsend/w;->c:Lcom/sec/chaton/msgsend/v;

    .line 858
    :goto_8
    invoke-virtual {v4}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    .line 859
    sget-object v3, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v5, "elapsed in sending all messages : (%s)"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-static {v3, v5, v1}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 847
    :cond_11
    invoke-direct {p0, v5, v3}, Lcom/sec/chaton/msgsend/c;->b(Lcom/sec/chaton/d/o;Lcom/sec/chaton/msgsend/w;)Lcom/sec/chaton/msgsend/w;

    move-result-object v0

    goto :goto_7

    .line 855
    :cond_12
    sget-object v3, Lcom/sec/chaton/msgsend/v;->f:Lcom/sec/chaton/msgsend/v;

    iput-object v3, v0, Lcom/sec/chaton/msgsend/w;->c:Lcom/sec/chaton/msgsend/v;

    goto :goto_8

    :cond_13
    move v0, v2

    goto/16 :goto_1
.end method

.method public h()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1153
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1154
    sget-object v0, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v1, "__print_all_msg(), This inbox is NOT AVAILABLE"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1167
    :cond_0
    return-void

    .line 1158
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 1159
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1160
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 1161
    sget-object v2, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "----------- print all msg (%d msg)----------"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1162
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1163
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1164
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 1165
    sget-object v2, Lcom/sec/chaton/msgsend/c;->a:Ljava/lang/String;

    const-string v3, "%s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1172
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/c;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1173
    const-string v0, "MsgEntity(null)"

    .line 1180
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->d:[Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1181
    const-string v1, "null"

    .line 1185
    :goto_1
    const-string v2, "[Inbox] inboxNo(%s), chatType(%s), recv(%s), MsgEnitity(%s), inProgressInitChat(%d), inProgressAllowChat(%d, msgId(%d))"

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/msgsend/c;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/chaton/msgsend/c;->c:Lcom/sec/chaton/e/r;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v1, v3, v4

    const/4 v1, 0x3

    aput-object v0, v3, v1

    const/4 v0, 0x4

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x5

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x6

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/c;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1176
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgsend/c;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1183
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/msgsend/c;->d:[Ljava/lang/String;

    array-length v1, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
