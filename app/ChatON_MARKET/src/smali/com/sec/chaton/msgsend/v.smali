.class public final enum Lcom/sec/chaton/msgsend/v;
.super Ljava/lang/Enum;
.source "MsgSendResultCode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/msgsend/v;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/msgsend/v;

.field public static final enum b:Lcom/sec/chaton/msgsend/v;

.field public static final enum c:Lcom/sec/chaton/msgsend/v;

.field public static final enum d:Lcom/sec/chaton/msgsend/v;

.field public static final enum e:Lcom/sec/chaton/msgsend/v;

.field public static final enum f:Lcom/sec/chaton/msgsend/v;

.field public static final enum g:Lcom/sec/chaton/msgsend/v;

.field public static final enum h:Lcom/sec/chaton/msgsend/v;

.field public static final enum i:Lcom/sec/chaton/msgsend/v;

.field private static final synthetic k:[Lcom/sec/chaton/msgsend/v;


# instance fields
.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 33
    new-instance v0, Lcom/sec/chaton/msgsend/v;

    const-string v1, "NOTHING_TODO"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/chaton/msgsend/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/v;->a:Lcom/sec/chaton/msgsend/v;

    .line 34
    new-instance v0, Lcom/sec/chaton/msgsend/v;

    const-string v1, "EMPTY_ROOM"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/chaton/msgsend/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/v;->b:Lcom/sec/chaton/msgsend/v;

    .line 35
    new-instance v0, Lcom/sec/chaton/msgsend/v;

    const-string v1, "REQ_INIT_CHAT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/chaton/msgsend/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/v;->c:Lcom/sec/chaton/msgsend/v;

    .line 36
    new-instance v0, Lcom/sec/chaton/msgsend/v;

    const-string v1, "REQ_ALLOW_CHAT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/chaton/msgsend/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/v;->d:Lcom/sec/chaton/msgsend/v;

    .line 37
    new-instance v0, Lcom/sec/chaton/msgsend/v;

    const-string v1, "REQ_SERIAL_CHAT_WAIT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/chaton/msgsend/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/v;->e:Lcom/sec/chaton/msgsend/v;

    .line 38
    new-instance v0, Lcom/sec/chaton/msgsend/v;

    const-string v1, "REQ_SEND_SUCCESS_ALL"

    const/4 v2, 0x5

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/v;->f:Lcom/sec/chaton/msgsend/v;

    .line 39
    new-instance v0, Lcom/sec/chaton/msgsend/v;

    const-string v1, "UNDETERMINED_STATUS"

    const/4 v2, 0x6

    const/16 v3, -0x63

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/v;->g:Lcom/sec/chaton/msgsend/v;

    .line 40
    new-instance v0, Lcom/sec/chaton/msgsend/v;

    const-string v1, "DOES_NOT_EXIST_ROOM"

    const/4 v2, 0x7

    const/16 v3, -0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/v;->h:Lcom/sec/chaton/msgsend/v;

    .line 41
    new-instance v0, Lcom/sec/chaton/msgsend/v;

    const-string v1, "IS_NOT_VALID_ROOM"

    const/16 v2, 0x8

    const/16 v3, -0x65

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/v;->i:Lcom/sec/chaton/msgsend/v;

    .line 32
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/chaton/msgsend/v;

    sget-object v1, Lcom/sec/chaton/msgsend/v;->a:Lcom/sec/chaton/msgsend/v;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/msgsend/v;->b:Lcom/sec/chaton/msgsend/v;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/msgsend/v;->c:Lcom/sec/chaton/msgsend/v;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/msgsend/v;->d:Lcom/sec/chaton/msgsend/v;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/msgsend/v;->e:Lcom/sec/chaton/msgsend/v;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/msgsend/v;->f:Lcom/sec/chaton/msgsend/v;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/msgsend/v;->g:Lcom/sec/chaton/msgsend/v;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/msgsend/v;->h:Lcom/sec/chaton/msgsend/v;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/msgsend/v;->i:Lcom/sec/chaton/msgsend/v;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/msgsend/v;->k:[Lcom/sec/chaton/msgsend/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/sec/chaton/msgsend/v;->j:I

    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/msgsend/v;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/chaton/msgsend/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/v;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/msgsend/v;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/chaton/msgsend/v;->k:[Lcom/sec/chaton/msgsend/v;

    invoke-virtual {v0}, [Lcom/sec/chaton/msgsend/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/msgsend/v;

    return-object v0
.end method
