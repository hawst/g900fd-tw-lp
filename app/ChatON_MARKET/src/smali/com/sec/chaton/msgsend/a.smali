.class public Lcom/sec/chaton/msgsend/a;
.super Ljava/lang/Object;
.source "BulkChatMsg.java"


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/msgsend/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/msgsend/a;->a:Ljava/util/List;

    .line 65
    return-void
.end method

.method public static a()Lcom/sec/chaton/msgsend/a;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/sec/chaton/msgsend/a;

    invoke-direct {v0}, Lcom/sec/chaton/msgsend/a;-><init>()V

    .line 69
    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(J)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 45
    monitor-enter p0

    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/msgsend/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/b;

    .line 48
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/b;->c()J

    move-result-wide v4

    .line 49
    cmp-long v0, v4, p1

    if-nez v0, :cond_1

    .line 50
    const/4 v0, 0x1

    .line 56
    :goto_1
    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/msgsend/a;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/msgsend/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 53
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 54
    goto :goto_0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public declared-synchronized a(Lcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/a;
    .locals 5

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/sec/chaton/msgsend/b;

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->e()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->f()Lcom/sec/chaton/e/w;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/msgsend/b;-><init>(JLcom/sec/chaton/e/w;Ljava/lang/String;)V

    .line 80
    iget-object v1, p0, Lcom/sec/chaton/msgsend/a;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-object p0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/msgsend/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/msgsend/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/b;

    .line 87
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/b;->d()Lcom/sec/chaton/msgsend/b;

    move-result-object v0

    .line 88
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 90
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized c()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/msgsend/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 95
    new-array v2, v0, [Ljava/lang/String;

    .line 97
    const/4 v0, 0x0

    .line 98
    iget-object v1, p0, Lcom/sec/chaton/msgsend/a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/b;

    .line 99
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/b;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 102
    :cond_0
    monitor-exit p0

    return-object v2

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
