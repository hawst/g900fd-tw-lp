.class public final enum Lcom/sec/chaton/msgsend/r;
.super Ljava/lang/Enum;
.source "MsgSendHelper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/msgsend/r;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/msgsend/r;

.field public static final enum b:Lcom/sec/chaton/msgsend/r;

.field public static final enum c:Lcom/sec/chaton/msgsend/r;

.field private static final synthetic e:[Lcom/sec/chaton/msgsend/r;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    new-instance v0, Lcom/sec/chaton/msgsend/r;

    const-string v1, "NOT_DETERMINED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/msgsend/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    .line 33
    new-instance v0, Lcom/sec/chaton/msgsend/r;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v4, v3}, Lcom/sec/chaton/msgsend/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/r;->b:Lcom/sec/chaton/msgsend/r;

    .line 34
    new-instance v0, Lcom/sec/chaton/msgsend/r;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/msgsend/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/r;->c:Lcom/sec/chaton/msgsend/r;

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/msgsend/r;

    sget-object v1, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/msgsend/r;->b:Lcom/sec/chaton/msgsend/r;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/msgsend/r;->c:Lcom/sec/chaton/msgsend/r;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/msgsend/r;->e:[Lcom/sec/chaton/msgsend/r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/sec/chaton/msgsend/r;->d:I

    .line 39
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/msgsend/r;
    .locals 1

    .prologue
    .line 46
    packed-switch p0, :pswitch_data_0

    .line 48
    sget-object v0, Lcom/sec/chaton/msgsend/r;->a:Lcom/sec/chaton/msgsend/r;

    .line 50
    :goto_0
    return-object v0

    .line 49
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/msgsend/r;->b:Lcom/sec/chaton/msgsend/r;

    goto :goto_0

    .line 50
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/msgsend/r;->c:Lcom/sec/chaton/msgsend/r;

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/msgsend/r;
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/chaton/msgsend/r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/r;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/msgsend/r;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/chaton/msgsend/r;->e:[Lcom/sec/chaton/msgsend/r;

    invoke-virtual {v0}, [Lcom/sec/chaton/msgsend/r;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/msgsend/r;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/chaton/msgsend/r;->d:I

    return v0
.end method
