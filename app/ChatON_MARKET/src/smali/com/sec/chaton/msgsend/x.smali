.class public final enum Lcom/sec/chaton/msgsend/x;
.super Ljava/lang/Enum;
.source "MsgSendRetryReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/msgsend/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/msgsend/x;

.field public static final enum b:Lcom/sec/chaton/msgsend/x;

.field public static final enum c:Lcom/sec/chaton/msgsend/x;

.field public static final enum d:Lcom/sec/chaton/msgsend/x;

.field public static final enum e:Lcom/sec/chaton/msgsend/x;

.field public static final enum f:Lcom/sec/chaton/msgsend/x;

.field public static final enum g:Lcom/sec/chaton/msgsend/x;

.field public static final enum h:Lcom/sec/chaton/msgsend/x;

.field public static final enum i:Lcom/sec/chaton/msgsend/x;

.field public static final enum j:Lcom/sec/chaton/msgsend/x;

.field public static final enum k:Lcom/sec/chaton/msgsend/x;

.field public static final enum l:Lcom/sec/chaton/msgsend/x;

.field private static final synthetic n:[Lcom/sec/chaton/msgsend/x;


# instance fields
.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "UNDETERMINED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->a:Lcom/sec/chaton/msgsend/x;

    .line 5
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_NEW_MSG_REQUESTED"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->b:Lcom/sec/chaton/msgsend/x;

    .line 6
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_LATEST_SENT_MSG_SUCCESS"

    invoke-direct {v0, v1, v6, v5}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->c:Lcom/sec/chaton/msgsend/x;

    .line 7
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_LATEST_SENT_MSG_FAIL"

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->d:Lcom/sec/chaton/msgsend/x;

    .line 8
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_LATEST_ALLOW_CHAT_SUCCESS"

    invoke-direct {v0, v1, v8, v7}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->e:Lcom/sec/chaton/msgsend/x;

    .line 9
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_LATEST_ALLOW_CHAT_FAIL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->f:Lcom/sec/chaton/msgsend/x;

    .line 10
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_LATEST_INIT_CHAT_SUCCESS"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->g:Lcom/sec/chaton/msgsend/x;

    .line 11
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_LATEST_INIT_CHAT_FAIL"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->h:Lcom/sec/chaton/msgsend/x;

    .line 13
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_LOADED_MSG_FROM_DB"

    const/16 v2, 0x8

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->i:Lcom/sec/chaton/msgsend/x;

    .line 14
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_NETWORK_CHANGED_ON"

    const/16 v2, 0x9

    const/16 v3, 0xc8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->j:Lcom/sec/chaton/msgsend/x;

    .line 15
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_CHATON_STARTED"

    const/16 v2, 0xa

    const/16 v3, 0x3e8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->k:Lcom/sec/chaton/msgsend/x;

    .line 16
    new-instance v0, Lcom/sec/chaton/msgsend/x;

    const-string v1, "REASON_MANUAL_REQUESTED"

    const/16 v2, 0xb

    const/16 v3, 0x270f

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/x;->l:Lcom/sec/chaton/msgsend/x;

    .line 3
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/sec/chaton/msgsend/x;

    sget-object v1, Lcom/sec/chaton/msgsend/x;->a:Lcom/sec/chaton/msgsend/x;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/msgsend/x;->b:Lcom/sec/chaton/msgsend/x;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/msgsend/x;->c:Lcom/sec/chaton/msgsend/x;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/msgsend/x;->d:Lcom/sec/chaton/msgsend/x;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/msgsend/x;->e:Lcom/sec/chaton/msgsend/x;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/msgsend/x;->f:Lcom/sec/chaton/msgsend/x;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/msgsend/x;->g:Lcom/sec/chaton/msgsend/x;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/msgsend/x;->h:Lcom/sec/chaton/msgsend/x;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/msgsend/x;->i:Lcom/sec/chaton/msgsend/x;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/chaton/msgsend/x;->j:Lcom/sec/chaton/msgsend/x;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/chaton/msgsend/x;->k:Lcom/sec/chaton/msgsend/x;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/chaton/msgsend/x;->l:Lcom/sec/chaton/msgsend/x;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/msgsend/x;->n:[Lcom/sec/chaton/msgsend/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lcom/sec/chaton/msgsend/x;->m:I

    .line 21
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/msgsend/x;
    .locals 1

    .prologue
    .line 28
    sparse-switch p0, :sswitch_data_0

    .line 42
    sget-object v0, Lcom/sec/chaton/msgsend/x;->a:Lcom/sec/chaton/msgsend/x;

    :goto_0
    return-object v0

    .line 29
    :sswitch_0
    sget-object v0, Lcom/sec/chaton/msgsend/x;->b:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 30
    :sswitch_1
    sget-object v0, Lcom/sec/chaton/msgsend/x;->c:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 31
    :sswitch_2
    sget-object v0, Lcom/sec/chaton/msgsend/x;->d:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 32
    :sswitch_3
    sget-object v0, Lcom/sec/chaton/msgsend/x;->e:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 33
    :sswitch_4
    sget-object v0, Lcom/sec/chaton/msgsend/x;->f:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 34
    :sswitch_5
    sget-object v0, Lcom/sec/chaton/msgsend/x;->g:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 35
    :sswitch_6
    sget-object v0, Lcom/sec/chaton/msgsend/x;->h:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 37
    :sswitch_7
    sget-object v0, Lcom/sec/chaton/msgsend/x;->i:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 38
    :sswitch_8
    sget-object v0, Lcom/sec/chaton/msgsend/x;->j:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 39
    :sswitch_9
    sget-object v0, Lcom/sec/chaton/msgsend/x;->k:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 40
    :sswitch_a
    sget-object v0, Lcom/sec/chaton/msgsend/x;->l:Lcom/sec/chaton/msgsend/x;

    goto :goto_0

    .line 28
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x64 -> :sswitch_7
        0xc8 -> :sswitch_8
        0x3e8 -> :sswitch_9
        0x270f -> :sswitch_a
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/msgsend/x;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/sec/chaton/msgsend/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/x;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/msgsend/x;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/chaton/msgsend/x;->n:[Lcom/sec/chaton/msgsend/x;

    invoke-virtual {v0}, [Lcom/sec/chaton/msgsend/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/msgsend/x;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/chaton/msgsend/x;->m:I

    return v0
.end method

.method public b()J
    .locals 6

    .prologue
    const-wide/16 v2, 0x1388

    const-wide/16 v0, 0x32

    .line 50
    sget-object v4, Lcom/sec/chaton/msgsend/y;->a:[I

    iget v5, p0, Lcom/sec/chaton/msgsend/x;->m:I

    invoke-static {v5}, Lcom/sec/chaton/msgsend/x;->a(I)Lcom/sec/chaton/msgsend/x;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/msgsend/x;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 64
    const-wide/16 v0, 0x2710

    :goto_0
    :pswitch_0
    return-wide v0

    :pswitch_1
    move-wide v0, v2

    .line 53
    goto :goto_0

    :pswitch_2
    move-wide v0, v2

    .line 55
    goto :goto_0

    :pswitch_3
    move-wide v0, v2

    .line 57
    goto :goto_0

    :pswitch_4
    move-wide v0, v2

    .line 59
    goto :goto_0

    .line 60
    :pswitch_5
    const-wide/16 v0, 0x1f4

    goto :goto_0

    .line 61
    :pswitch_6
    const-wide/16 v0, 0xbb8

    goto :goto_0

    .line 62
    :pswitch_7
    const-wide/16 v0, 0x12c

    goto :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
