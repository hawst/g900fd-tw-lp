.class public final enum Lcom/sec/chaton/msgsend/aa;
.super Ljava/lang/Enum;
.source "MsgSendStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/msgsend/aa;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/msgsend/aa;

.field public static final enum b:Lcom/sec/chaton/msgsend/aa;

.field public static final enum c:Lcom/sec/chaton/msgsend/aa;

.field public static final enum d:Lcom/sec/chaton/msgsend/aa;

.field public static final enum e:Lcom/sec/chaton/msgsend/aa;

.field private static final synthetic g:[Lcom/sec/chaton/msgsend/aa;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/sec/chaton/msgsend/aa;

    const-string v1, "UNKNOWN_STATUS"

    const/16 v2, -0x270f

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/msgsend/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/aa;->a:Lcom/sec/chaton/msgsend/aa;

    .line 7
    new-instance v0, Lcom/sec/chaton/msgsend/aa;

    const-string v1, "FAILED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/msgsend/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    .line 8
    new-instance v0, Lcom/sec/chaton/msgsend/aa;

    const-string v1, "PENDING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/chaton/msgsend/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/aa;->c:Lcom/sec/chaton/msgsend/aa;

    .line 9
    new-instance v0, Lcom/sec/chaton/msgsend/aa;

    const-string v1, "SENDING"

    invoke-direct {v0, v1, v6, v3}, Lcom/sec/chaton/msgsend/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/aa;->d:Lcom/sec/chaton/msgsend/aa;

    .line 10
    new-instance v0, Lcom/sec/chaton/msgsend/aa;

    const-string v1, "SENDED"

    invoke-direct {v0, v1, v7, v4}, Lcom/sec/chaton/msgsend/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/msgsend/aa;->e:Lcom/sec/chaton/msgsend/aa;

    .line 5
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/msgsend/aa;

    sget-object v1, Lcom/sec/chaton/msgsend/aa;->a:Lcom/sec/chaton/msgsend/aa;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/msgsend/aa;->c:Lcom/sec/chaton/msgsend/aa;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/msgsend/aa;->d:Lcom/sec/chaton/msgsend/aa;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/msgsend/aa;->e:Lcom/sec/chaton/msgsend/aa;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/chaton/msgsend/aa;->g:[Lcom/sec/chaton/msgsend/aa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lcom/sec/chaton/msgsend/aa;->f:I

    .line 21
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/msgsend/aa;
    .locals 1

    .prologue
    .line 28
    packed-switch p0, :pswitch_data_0

    .line 34
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/msgsend/aa;->a:Lcom/sec/chaton/msgsend/aa;

    :goto_0
    return-object v0

    .line 29
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    goto :goto_0

    .line 30
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/msgsend/aa;->c:Lcom/sec/chaton/msgsend/aa;

    goto :goto_0

    .line 31
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/msgsend/aa;->d:Lcom/sec/chaton/msgsend/aa;

    goto :goto_0

    .line 32
    :pswitch_4
    sget-object v0, Lcom/sec/chaton/msgsend/aa;->e:Lcom/sec/chaton/msgsend/aa;

    goto :goto_0

    .line 28
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/msgsend/aa;
    .locals 1

    .prologue
    .line 5
    const-class v0, Lcom/sec/chaton/msgsend/aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/aa;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/msgsend/aa;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/sec/chaton/msgsend/aa;->g:[Lcom/sec/chaton/msgsend/aa;

    invoke-virtual {v0}, [Lcom/sec/chaton/msgsend/aa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/msgsend/aa;

    return-object v0
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/chaton/msgsend/aa;->f:I

    return v0
.end method
