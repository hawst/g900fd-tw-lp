.class public Lcom/sec/chaton/msgsend/ChatONMsgEntity;
.super Ljava/lang/Object;
.source "ChatONMsgEntity.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/chaton/msgsend/ChatONMsgEntity;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mChatMsg:Ljava/lang/String;

.field private mChatMsgTranslated:Ljava/lang/String;

.field private mCreateTime:J

.field private mFormattedMsg:Ljava/lang/String;

.field private mIsFileUploaded:I

.field private mIsInProgress:Z

.field private mLastSendEndTime:J

.field private mLastSendTryTime:J

.field private mLocalFileUri:Ljava/lang/String;

.field private mMsgId:Ljava/lang/Long;

.field private mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

.field private mMsgType:Lcom/sec/chaton/e/w;

.field private mSendTryCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->TAG:Ljava/lang/String;

    .line 316
    new-instance v0, Lcom/sec/chaton/msgsend/g;

    invoke-direct {v0}, Lcom/sec/chaton/msgsend/g;-><init>()V

    sput-object v0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 10

    .prologue
    .line 225
    invoke-static/range {p9 .. p9}, Lcom/sec/chaton/msgsend/aa;->a(I)Lcom/sec/chaton/msgsend/aa;

    move-result-object v9

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;-><init>(JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/sec/chaton/msgsend/aa;)V

    .line 226
    return-void
.end method

.method public constructor <init>(JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/sec/chaton/msgsend/aa;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean v2, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsInProgress:Z

    .line 19
    iput v2, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mSendTryCount:I

    .line 20
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mCreateTime:J

    .line 22
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendTryTime:J

    .line 23
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendEndTime:J

    .line 240
    iput-object p9, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

    .line 243
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgId:Ljava/lang/Long;

    .line 244
    iput-object p3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgType:Lcom/sec/chaton/e/w;

    .line 245
    iput-object p4, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsg:Ljava/lang/String;

    .line 246
    iput-object p5, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsgTranslated:Ljava/lang/String;

    .line 254
    iput-object p6, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLocalFileUri:Ljava/lang/String;

    .line 255
    iput-object p7, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mFormattedMsg:Ljava/lang/String;

    .line 256
    iput p8, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsFileUploaded:I

    .line 257
    invoke-direct {p0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->m()V

    .line 258
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean v2, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsInProgress:Z

    .line 19
    iput v2, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mSendTryCount:I

    .line 20
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mCreateTime:J

    .line 22
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendTryTime:J

    .line 23
    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendEndTime:J

    .line 262
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/msgsend/aa;->a(I)Lcom/sec/chaton/msgsend/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

    .line 263
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mSendTryCount:I

    .line 264
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mCreateTime:J

    .line 265
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendTryTime:J

    .line 272
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgId:Ljava/lang/Long;

    .line 273
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgType:Lcom/sec/chaton/e/w;

    .line 274
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsg:Ljava/lang/String;

    .line 275
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsgTranslated:Ljava/lang/String;

    .line 283
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLocalFileUri:Ljava/lang/String;

    .line 284
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mFormattedMsg:Ljava/lang/String;

    .line 285
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsFileUploaded:I

    .line 286
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mSendTryCount:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 28
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mCreateTime:J

    .line 30
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsFileUploaded:I

    .line 196
    iput p1, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsFileUploaded:I

    .line 197
    return v0
.end method

.method public a(Lcom/sec/chaton/msgsend/aa;)Lcom/sec/chaton/msgsend/aa;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

    .line 184
    iput-object p1, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

    .line 185
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 33
    sget-object v0, Lcom/sec/chaton/msgsend/aa;->d:Lcom/sec/chaton/msgsend/aa;

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

    .line 34
    iget v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mSendTryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mSendTryCount:I

    .line 35
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendTryTime:J

    .line 36
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsg:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsInProgress:Z

    .line 168
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mFormattedMsg:Ljava/lang/String;

    .line 190
    iput-object p1, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mFormattedMsg:Ljava/lang/String;

    .line 191
    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 39
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendEndTime:J

    .line 40
    return-void
.end method

.method public c()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 43
    sget-object v2, Lcom/sec/chaton/msgsend/h;->a:[I

    iget-object v3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

    invoke-virtual {v3}, Lcom/sec/chaton/msgsend/aa;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 71
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 54
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsInProgress:Z

    if-nez v2, :cond_0

    .line 60
    iget v2, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mSendTryCount:I

    const/16 v3, 0x2d0

    if-le v2, v3, :cond_1

    .line 61
    sget-object v2, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->TAG:Ljava/lang/String;

    const-string v3, "doesNeedToResend(), Too many retry this msg : %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p0, v4, v0

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    sget-object v2, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->TAG:Ljava/lang/String;

    const-string v3, "doesNeedToResend(), Too many retry this msg : %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p0, v4, v0

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    sget-object v2, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->TAG:Ljava/lang/String;

    const-string v3, "doesNeedToResend(), WToo many retry this msg : %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v0

    invoke-static {v2, v3, v1}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    sget-object v1, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    iput-object v1, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

    .line 67
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgId:Ljava/lang/Long;

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/q;->b(Landroid/content/ContentResolver;Ljava/lang/Long;)I

    goto :goto_0

    :cond_1
    move v0, v1

    .line 71
    goto :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method d()Ljava/lang/String;
    .locals 6

    .prologue
    .line 75
    const-string v0, "(not completed to send)"

    .line 76
    iget-wide v1, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendEndTime:J

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mCreateTime:J

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 77
    new-instance v0, Lcom/sec/chaton/msgsend/j;

    iget-wide v1, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendEndTime:J

    iget-wide v3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mCreateTime:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/msgsend/j;-><init>(JJ)V

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/j;->toString()Ljava/lang/String;

    move-result-object v0

    .line 80
    :cond_0
    const-string v1, "TryCount(%d), Create(%d)/LastSendTry(%d)/LastSendEnd(%d) --> %s"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mSendTryCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mCreateTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendTryTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-wide v4, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendEndTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgId:Ljava/lang/Long;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 378
    if-nez p1, :cond_1

    .line 386
    :cond_0
    :goto_0
    return v0

    .line 382
    :cond_1
    check-cast p1, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 383
    iget-object v1, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgId:Ljava/lang/Long;

    iget-object v2, p1, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgId:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Lcom/sec/chaton/e/w;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgType:Lcom/sec/chaton/e/w;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsg:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsgTranslated:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLocalFileUri:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mFormattedMsg:Ljava/lang/String;

    return-object v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsFileUploaded:I

    return v0
.end method

.method public l()Lcom/sec/chaton/msgsend/aa;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 341
    const-string v0, "[MSG] : (%s) MsgSendStatus(%s), msgId(%d), msgType(%s), chatMsg(%s), chatMsgTranslated(%s), DownUri(%s), FormattedMsg(%s), IsFileUploaded(%d)"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgId:Ljava/lang/Long;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgType:Lcom/sec/chaton/e/w;

    aput-object v2, v1, v4

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsg:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsgTranslated:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLocalFileUri:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mFormattedMsg:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget v3, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsFileUploaded:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 368
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgSendStatus:Lcom/sec/chaton/msgsend/aa;

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/aa;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 292
    iget v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mSendTryCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 293
    iget-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mCreateTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 294
    iget-wide v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLastSendTryTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mMsgType:Lcom/sec/chaton/e/w;

    invoke-virtual {v0}, Lcom/sec/chaton/e/w;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mChatMsgTranslated:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mLocalFileUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mFormattedMsg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 312
    iget v0, p0, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->mIsFileUploaded:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 313
    return-void
.end method
