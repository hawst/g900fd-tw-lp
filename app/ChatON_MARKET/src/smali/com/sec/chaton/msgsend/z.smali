.class public Lcom/sec/chaton/msgsend/z;
.super Ljava/lang/Object;
.source "MsgSendScheduleHelper.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/chaton/msgsend/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgsend/z;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 18
    invoke-static {p0}, Lcom/sec/chaton/msgsend/z;->b(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V
    .locals 0

    .prologue
    .line 43
    invoke-static {p0, p1}, Lcom/sec/chaton/msgsend/z;->b(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)Z

    .line 44
    return-void
.end method

.method private static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 84
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/msgsend/MsgSendEventReceiver;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    const-string v1, "com.sec.chaton.msgsend.MsgSendScheduleHelper.ALARM_TIMER_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    const/high16 v1, 0x20000000

    invoke-static {p0, v2, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 88
    if-nez v1, :cond_0

    .line 89
    sget-object v0, Lcom/sec/chaton/msgsend/z;->a:Ljava/lang/String;

    const-string v1, "stopAlarm(), pending intent is NULL"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    :goto_0
    return-void

    .line 93
    :cond_0
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 94
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 96
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    const-string v0, "canceled Alarm"

    invoke-static {}, Lcom/sec/chaton/msgsend/l;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/msgsend/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_1
    sget-object v0, Lcom/sec/chaton/msgsend/z;->a:Ljava/lang/String;

    const-string v1, "stopAlarm(), canceled ALARM"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 47
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    sget-object v0, Lcom/sec/chaton/msgsend/z;->a:Ljava/lang/String;

    const-string v2, "  (Refused SET ALARM !!)  -  AUTO RESEND OFF)"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 80
    :goto_0
    return v0

    .line 52
    :cond_0
    invoke-static {}, Lcom/sec/common/util/i;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    sget-object v0, Lcom/sec/chaton/msgsend/z;->a:Ljava/lang/String;

    const-string v3, "  (Refused SET ALARM !!)  -  requested (%s,  %d msec)"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/x;->b()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 54
    goto :goto_0

    .line 57
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/chaton/msgsend/MsgSendEventReceiver;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    const-string v3, "set_alarm_reason"

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/x;->a()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 59
    const-string v3, "com.sec.chaton.msgsend.MsgSendScheduleHelper.ALARM_TIMER_ACTION"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const/high16 v3, 0x10000000

    invoke-static {p0, v1, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 63
    if-nez v3, :cond_2

    .line 64
    sget-object v0, Lcom/sec/chaton/msgsend/z;->a:Ljava/lang/String;

    const-string v2, "pending intent is NULL"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 65
    goto :goto_0

    .line 68
    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/x;->b()J

    move-result-wide v4

    .line 69
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 70
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 71
    invoke-virtual {v0, v8, v4, v5, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 74
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 75
    const-string v0, "setALARM: %d, %s"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/x;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v1

    aput-object p1, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 76
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/msgsend/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_3
    sget-object v0, Lcom/sec/chaton/msgsend/z;->a:Ljava/lang/String;

    const-string v3, " -------------- set ALARM (%s,  %d msec)"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-virtual {p1}, Lcom/sec/chaton/msgsend/x;->b()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 80
    goto/16 :goto_0
.end method
