.class public Lcom/sec/chaton/msgsend/w;
.super Ljava/lang/Object;
.source "MsgSendResultCode.java"


# instance fields
.field public a:I

.field public b:I

.field c:Lcom/sec/chaton/msgsend/v;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/msgsend/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput v0, p0, Lcom/sec/chaton/msgsend/w;->a:I

    .line 13
    iput v0, p0, Lcom/sec/chaton/msgsend/w;->b:I

    .line 14
    iput-object p1, p0, Lcom/sec/chaton/msgsend/w;->c:Lcom/sec/chaton/msgsend/v;

    .line 15
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 25
    const-string v0, "[Result : %s, Req %d/Total %d]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/msgsend/w;->c:Lcom/sec/chaton/msgsend/v;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/chaton/msgsend/w;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/sec/chaton/msgsend/w;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 26
    return-object v0
.end method
