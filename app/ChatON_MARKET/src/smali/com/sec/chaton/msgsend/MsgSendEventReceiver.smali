.class public Lcom/sec/chaton/msgsend/MsgSendEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MsgSendEventReceiver.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/sec/chaton/msgsend/MsgSendEventReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgsend/MsgSendEventReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 12
    if-nez p2, :cond_0

    .line 48
    :goto_0
    return-void

    .line 16
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    sget-object v0, Lcom/sec/chaton/msgsend/MsgSendEventReceiver;->a:Ljava/lang/String;

    const-string v1, "getAction == null"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 22
    :cond_1
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v1

    .line 41
    const-string v2, "com.sec.chaton.msgsend.MsgSendScheduleHelper.ALARM_TIMER_ACTION"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v6, :cond_2

    .line 42
    const-string v2, "set_alarm_reason"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 43
    invoke-static {v2}, Lcom/sec/chaton/msgsend/x;->a(I)Lcom/sec/chaton/msgsend/x;

    move-result-object v2

    .line 44
    invoke-static {v2}, Lcom/sec/chaton/msgsend/p;->a(Lcom/sec/chaton/msgsend/x;)V

    .line 47
    :cond_2
    sget-object v2, Lcom/sec/chaton/msgsend/MsgSendEventReceiver;->a:Ljava/lang/String;

    const-string v3, "process onReceive (%s) : %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
