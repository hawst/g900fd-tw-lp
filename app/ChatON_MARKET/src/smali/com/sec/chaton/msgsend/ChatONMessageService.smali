.class public Lcom/sec/chaton/msgsend/ChatONMessageService;
.super Landroid/app/Service;
.source "ChatONMessageService.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private volatile c:Landroid/os/Looper;

.field private volatile d:Lcom/sec/chaton/msgsend/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/chaton/msgsend/ChatONMessageService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgsend/ChatONMessageService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 17
    const-string v0, "Messaging Service"

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMessageService;->b:Ljava/lang/String;

    .line 154
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/chaton/msgsend/ChatONMessageService;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a(Landroid/content/Intent;)V
    .locals 13

    .prologue
    const/16 v12, -0x270f

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v3, -0x1

    const/4 v4, 0x0

    .line 22
    const-string v0, "cmd"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 23
    invoke-static {v0}, Lcom/sec/chaton/msgsend/o;->a(I)Lcom/sec/chaton/msgsend/o;

    move-result-object v10

    .line 25
    sget-object v0, Lcom/sec/chaton/msgsend/ChatONMessageService;->a:Ljava/lang/String;

    const-string v1, "Handler Intent [START] : CMD(%s)"

    new-array v2, v8, [Ljava/lang/Object;

    aput-object v10, v2, v9

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v11

    .line 29
    sget-object v0, Lcom/sec/chaton/msgsend/e;->a:[I

    invoke-virtual {v10}, Lcom/sec/chaton/msgsend/o;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move v0, v9

    .line 150
    :goto_0
    invoke-virtual {v11}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    .line 151
    sget-object v1, Lcom/sec/chaton/msgsend/ChatONMessageService;->a:Ljava/lang/String;

    const-string v2, "Handler Intent [END] : CMD(%s), (%s), (processed? %s)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v10, v3, v9

    aput-object v11, v3, v8

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152
    return-void

    .line 31
    :pswitch_0
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    .line 32
    const-string v0, "inbox_no"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    const-string v0, "chat_type"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    .line 34
    const-string v0, "participants"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 35
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 37
    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/c;

    .line 40
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/msgsend/x;->b:Lcom/sec/chaton/msgsend/x;

    invoke-static {v0, v1}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V

    move v0, v8

    .line 42
    goto :goto_0

    .line 46
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 47
    const-string v1, "send_retry_reason"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 48
    invoke-static {v1}, Lcom/sec/chaton/msgsend/x;->a(I)Lcom/sec/chaton/msgsend/x;

    move-result-object v1

    .line 52
    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgsend/l;->a(Lcom/sec/chaton/msgsend/x;)V

    move v0, v8

    .line 53
    goto :goto_0

    .line 67
    :pswitch_2
    const-string v0, "inbox_no"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 68
    const-string v0, "msg_id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 73
    const-string v0, "send_status"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 74
    const-string v0, "send_status"

    sget-object v5, Lcom/sec/chaton/msgsend/aa;->a:Lcom/sec/chaton/msgsend/aa;

    invoke-virtual {v5}, Lcom/sec/chaton/msgsend/aa;->a()I

    move-result v5

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/msgsend/aa;->a(I)Lcom/sec/chaton/msgsend/aa;

    move-result-object v0

    .line 75
    sget-object v5, Lcom/sec/chaton/msgsend/aa;->a:Lcom/sec/chaton/msgsend/aa;

    if-ne v0, v5, :cond_4

    move-object v7, v4

    .line 79
    :goto_1
    const-string v0, "formatted_msg"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 80
    const-string v0, "formatted_msg"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 82
    :goto_2
    const-string v0, "is_file_uploaded"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    const-string v0, "is_file_uploaded"

    invoke-virtual {p1, v0, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 84
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v12, :cond_0

    move-object v6, v4

    .line 88
    :cond_0
    :goto_3
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    move-object v4, v7

    .line 89
    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    move v0, v8

    .line 90
    goto/16 :goto_0

    .line 94
    :pswitch_3
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/l;->e()Z

    move v0, v8

    .line 98
    goto/16 :goto_0

    .line 113
    :pswitch_4
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 114
    const-string v1, "inbox_no"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 115
    const-string v2, "msg_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 116
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;J)Z

    move v0, v8

    .line 117
    goto/16 :goto_0

    .line 121
    :pswitch_5
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 122
    const-string v1, "inbox_no"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 123
    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;)Z

    move v0, v8

    .line 124
    goto/16 :goto_0

    .line 128
    :pswitch_6
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/l;->d()V

    move v0, v8

    .line 130
    goto/16 :goto_0

    .line 134
    :pswitch_7
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->b()Lcom/sec/chaton/msgsend/l;

    move-result-object v0

    .line 136
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    invoke-static {}, Lcom/sec/chaton/msgsend/l;->a()Ljava/lang/String;

    move-result-object v1

    .line 138
    const-string v2, "Debug Print"

    invoke-static {v2, v1, v1}, Lcom/sec/chaton/msgsend/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/l;->c()V

    move v0, v8

    .line 142
    goto/16 :goto_0

    :cond_2
    move-object v6, v4

    goto :goto_3

    :cond_3
    move-object v5, v4

    goto :goto_2

    :cond_4
    move-object v7, v0

    goto/16 :goto_1

    :cond_5
    move-object v7, v4

    goto/16 :goto_1

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 183
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 184
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Messaging Service"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 185
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 187
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMessageService;->c:Landroid/os/Looper;

    .line 188
    new-instance v0, Lcom/sec/chaton/msgsend/f;

    iget-object v1, p0, Lcom/sec/chaton/msgsend/ChatONMessageService;->c:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/msgsend/f;-><init>(Lcom/sec/chaton/msgsend/ChatONMessageService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMessageService;->d:Lcom/sec/chaton/msgsend/f;

    .line 189
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMessageService;->c:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 224
    sget-object v0, Lcom/sec/chaton/msgsend/ChatONMessageService;->a:Ljava/lang/String;

    const-string v1, " ---------------- Service DESTROYED --------------------"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 225
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 226
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8

    .prologue
    const/4 v2, -0x1

    const/4 v7, 0x1

    .line 196
    if-nez p1, :cond_0

    .line 214
    :goto_0
    return v7

    .line 199
    :cond_0
    const-string v0, "cmd_priority"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 200
    const-string v1, "cmd"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 201
    invoke-static {v1}, Lcom/sec/chaton/msgsend/o;->a(I)Lcom/sec/chaton/msgsend/o;

    move-result-object v1

    .line 203
    sget-object v2, Lcom/sec/chaton/msgsend/ChatONMessageService;->a:Ljava/lang/String;

    const-string v3, "requested : startId(%d), priority(%d), Command(%s)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    iget-object v1, p0, Lcom/sec/chaton/msgsend/ChatONMessageService;->d:Lcom/sec/chaton/msgsend/f;

    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/f;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 206
    iput p3, v1, Landroid/os/Message;->arg1:I

    .line 207
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 209
    if-ne v0, v7, :cond_1

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMessageService;->d:Lcom/sec/chaton/msgsend/f;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgsend/f;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    goto :goto_0

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/msgsend/ChatONMessageService;->d:Lcom/sec/chaton/msgsend/f;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgsend/f;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
