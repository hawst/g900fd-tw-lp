.class public Lcom/sec/chaton/msgsend/l;
.super Ljava/lang/Object;
.source "MessageProcessor.java"


# static fields
.field public static final a:Ljava/lang/String;

.field private static c:Lcom/sec/chaton/msgsend/l;


# instance fields
.field final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/msgsend/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/chaton/msgsend/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    .line 47
    invoke-virtual {p0}, Lcom/sec/chaton/msgsend/l;->e()Z

    move-result v0

    .line 48
    if-eqz v0, :cond_0

    .line 49
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    .line 50
    sget-object v1, Lcom/sec/chaton/msgsend/x;->i:Lcom/sec/chaton/msgsend/x;

    invoke-static {v0, v1}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V

    .line 52
    :cond_0
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Boolean;JLcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/c;
    .locals 7

    .prologue
    .line 670
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "addMsg(), inboxNo(%s), chatType(%s), sesseionId(%s), serverIP(%s), serverPort(%d), isInboxValid(%s), lastSessionMergeTime(%d), msgEntity(%s)"

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p4, v3, v4

    const/4 v4, 0x3

    aput-object p5, v3, v4

    const/4 v4, 0x4

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    aput-object p7, v3, v4

    const/4 v4, 0x6

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x7

    aput-object p10, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 672
    iget-object v1, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/msgsend/c;

    .line 676
    if-nez v1, :cond_4

    .line 677
    sget-object v1, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "New manged inbox is created!! addMsg(), inboxNo(%s), chatType(%s), sesseionId(%s), serverIP(%s), serverPort(%d), isInboxValid(%s), lastSessionMergeTime(%d)"

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p4, v3, v4

    const/4 v4, 0x3

    aput-object p5, v3, v4

    const/4 v4, 0x4

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    aput-object p7, v3, v4

    const/4 v4, 0x6

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 680
    if-nez p3, :cond_0

    .line 681
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    .line 682
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sec/chaton/e/a/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p3

    .line 684
    if-nez p3, :cond_0

    .line 685
    sget-object v1, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "[ERROR] no participant, so make it FAILED !!!!!!!!! inboxNo(%s), chatType(%s), Msg(%s)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p10, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 686
    sget-object v1, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;->a(Lcom/sec/chaton/msgsend/aa;)Lcom/sec/chaton/msgsend/aa;

    .line 694
    :cond_0
    new-instance v1, Lcom/sec/chaton/msgsend/c;

    invoke-direct {v1, p1, p2, p3}, Lcom/sec/chaton/msgsend/c;-><init>(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;)V

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move-wide v5, p8

    .line 695
    invoke-virtual/range {v1 .. v6}, Lcom/sec/chaton/msgsend/c;->a(Ljava/lang/String;Ljava/lang/String;IJ)Z

    move-result v2

    .line 696
    if-nez v2, :cond_1

    .line 697
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 699
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 711
    :cond_2
    :goto_0
    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, Lcom/sec/chaton/msgsend/c;->a(Lcom/sec/chaton/msgsend/ChatONMsgEntity;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 712
    if-nez v2, :cond_3

    .line 713
    const/4 v1, 0x0

    .line 716
    :cond_3
    monitor-exit p0

    return-object v1

    .line 702
    :cond_4
    :try_start_1
    sget-object v2, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v3, "addMsg in exist inbox (%s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 704
    if-eqz p3, :cond_2

    .line 705
    invoke-virtual {v1, p3}, Lcom/sec/chaton/msgsend/c;->a([Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 670
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    const-string v0, ""

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Lcom/sec/chaton/j/s;)V
    .locals 4

    .prologue
    .line 743
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "updateAllowChat(), inboxNo(%s), msgResult(%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 744
    if-nez p1, :cond_0

    .line 745
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "request inbox is NULL"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 772
    :goto_0
    monitor-exit p0

    return-void

    .line 749
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "updateAllowChat(), -- do nothing anymore..."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 743
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Lcom/sec/chaton/j/s;J)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 775
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v3, "updateChatRequest(), inboxNo(%s), msgResult(%s), msgId(%d)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const/4 v5, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 776
    if-nez p1, :cond_1

    .line 777
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "request inbox is NULL"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 857
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 784
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    if-eqz v0, :cond_b

    .line 785
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 786
    if-nez v0, :cond_2

    .line 787
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "not exist inbox : %s "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 775
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 791
    :cond_2
    :try_start_2
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->d()J

    move-result-wide v3

    .line 792
    cmp-long v5, v3, p3

    if-nez v5, :cond_4

    .line 793
    sget-object v5, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v6, "IS THE SAME ALLOW CHAT MSG ID (true) : %d / %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v7, v8

    const/4 v3, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    invoke-static {v5, v6, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 794
    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v3, v4, v5}, Lcom/sec/chaton/msgsend/c;->a(ZJ)V

    move v3, v1

    .line 802
    :goto_1
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->a()Lcom/sec/chaton/e/r;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v4, v5, :cond_3

    .line 803
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->c()J

    move-result-wide v4

    .line 804
    cmp-long v6, v4, p3

    if-nez v6, :cond_5

    .line 805
    sget-object v6, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v7, "IS THE SAME INIT CHAT MSG ID (true) : %d / %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v9

    const/4 v4, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v8, v4

    invoke-static {v6, v7, v8}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 806
    const/4 v4, 0x0

    const-wide/16 v5, -0x1

    invoke-virtual {v0, v4, v5, v6}, Lcom/sec/chaton/msgsend/c;->b(ZJ)V

    .line 815
    :cond_3
    :goto_2
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->a()Lcom/sec/chaton/e/r;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v4, v5, :cond_7

    .line 816
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->f()J

    move-result-wide v4

    .line 817
    cmp-long v6, v4, p3

    if-nez v6, :cond_6

    .line 818
    sget-object v2, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v6, "IS THE SAME SerialChat last member MSG ID (true) : %d / %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v8

    const/4 v4, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v7, v4

    invoke-static {v2, v6, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 819
    const-wide/16 v4, -0x1

    invoke-virtual {v0, v4, v5}, Lcom/sec/chaton/msgsend/c;->a(J)V

    move v7, v1

    move v8, v3

    .line 828
    :goto_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v9

    .line 829
    sget-object v0, Lcom/sec/chaton/msgsend/m;->b:[I

    invoke-virtual {p2}, Lcom/sec/chaton/j/s;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 831
    :pswitch_0
    sget-object v4, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    .line 832
    if-eqz v8, :cond_8

    .line 833
    sget-object v0, Lcom/sec/chaton/msgsend/x;->f:Lcom/sec/chaton/msgsend/x;

    invoke-static {v9, v0}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V

    goto/16 :goto_0

    .line 798
    :cond_4
    sget-object v5, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v6, "IS THE SAME ALLOW CHAT MSG ID (false) : %d / %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v7, v8

    const/4 v3, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    invoke-static {v5, v6, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v3, v2

    goto/16 :goto_1

    .line 810
    :cond_5
    sget-object v6, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v7, "IS THE SAME INIT CHAT MSG ID (false) : %d / %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v9

    const/4 v4, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v8, v4

    invoke-static {v6, v7, v8}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 823
    :cond_6
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "IS THE SAME SerialChat last member MSG ID (false) : %d / %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v6, v4

    invoke-static {v0, v1, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_7
    move v7, v2

    move v8, v3

    goto :goto_3

    .line 835
    :cond_8
    sget-object v0, Lcom/sec/chaton/msgsend/x;->d:Lcom/sec/chaton/msgsend/x;

    invoke-static {v9, v0}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V

    goto/16 :goto_0

    .line 840
    :pswitch_1
    sget-object v4, Lcom/sec/chaton/msgsend/aa;->c:Lcom/sec/chaton/msgsend/aa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    .line 841
    if-eqz v8, :cond_9

    .line 842
    sget-object v0, Lcom/sec/chaton/msgsend/x;->f:Lcom/sec/chaton/msgsend/x;

    invoke-static {v9, v0}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V

    goto/16 :goto_0

    .line 844
    :cond_9
    sget-object v0, Lcom/sec/chaton/msgsend/x;->d:Lcom/sec/chaton/msgsend/x;

    invoke-static {v9, v0}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V

    goto/16 :goto_0

    .line 849
    :pswitch_2
    sget-object v4, Lcom/sec/chaton/msgsend/aa;->e:Lcom/sec/chaton/msgsend/aa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    .line 850
    if-eqz v8, :cond_a

    .line 851
    sget-object v0, Lcom/sec/chaton/msgsend/x;->e:Lcom/sec/chaton/msgsend/x;

    invoke-static {v9, v0}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V

    goto/16 :goto_0

    .line 852
    :cond_a
    if-eqz v7, :cond_0

    .line 853
    sget-object v0, Lcom/sec/chaton/msgsend/x;->c:Lcom/sec/chaton/msgsend/x;

    invoke-static {v9, v0}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_b
    move v7, v2

    move v8, v2

    goto/16 :goto_3

    .line 829
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static declared-synchronized b()Lcom/sec/chaton/msgsend/l;
    .locals 2

    .prologue
    .line 94
    const-class v1, Lcom/sec/chaton/msgsend/l;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->c:Lcom/sec/chaton/msgsend/l;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Lcom/sec/chaton/msgsend/l;

    invoke-direct {v0}, Lcom/sec/chaton/msgsend/l;-><init>()V

    sput-object v0, Lcom/sec/chaton/msgsend/l;->c:Lcom/sec/chaton/msgsend/l;

    .line 97
    :cond_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->c:Lcom/sec/chaton/msgsend/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/String;Lcom/sec/chaton/j/s;J)V
    .locals 9

    .prologue
    .line 868
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "updateInitChat(), inboxNo(%s), msgResult(%s), msgId(%d)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 869
    if-nez p1, :cond_1

    .line 870
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "request inbox is NULL"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 917
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 874
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 875
    if-nez v0, :cond_2

    .line 876
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "not exist inbox : %s "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 868
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 881
    :cond_2
    :try_start_2
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->a()Lcom/sec/chaton/e/r;

    move-result-object v7

    .line 882
    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v7, v1, :cond_3

    .line 883
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->c()J

    move-result-wide v1

    .line 884
    cmp-long v3, v1, p3

    if-nez v3, :cond_4

    .line 885
    sget-object v3, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v4, "IS THE SAME INIT CHAT MSG ID (true) : %d / %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 886
    const/4 v1, 0x0

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/msgsend/c;->b(ZJ)V

    .line 894
    :cond_3
    :goto_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v8

    .line 895
    sget-object v0, Lcom/sec/chaton/msgsend/m;->b:[I

    invoke-virtual {p2}, Lcom/sec/chaton/j/s;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 905
    :pswitch_0
    sget-object v4, Lcom/sec/chaton/msgsend/aa;->c:Lcom/sec/chaton/msgsend/aa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    .line 906
    sget-object v0, Lcom/sec/chaton/msgsend/x;->h:Lcom/sec/chaton/msgsend/x;

    invoke-static {v8, v0}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V

    goto :goto_0

    .line 890
    :cond_4
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v3, "IS THE SAME INIT CHAT MSG ID (false) : %d / %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 897
    :pswitch_1
    sget-object v4, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    .line 899
    sget-object v0, Lcom/sec/chaton/msgsend/x;->h:Lcom/sec/chaton/msgsend/x;

    invoke-static {v8, v0}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V

    goto/16 :goto_0

    .line 911
    :pswitch_2
    sget-object v4, Lcom/sec/chaton/msgsend/aa;->e:Lcom/sec/chaton/msgsend/aa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    .line 912
    sget-object v0, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v7, v0, :cond_0

    .line 913
    sget-object v0, Lcom/sec/chaton/msgsend/x;->g:Lcom/sec/chaton/msgsend/x;

    invoke-static {v8, v0}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 895
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private declared-synchronized f()Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 720
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "queryAllPendings()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 721
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v7

    .line 723
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 724
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "My ChatonID is (%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 725
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v6

    .line 739
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 729
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 730
    invoke-static {}, Lcom/sec/chaton/e/v;->f()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v5, v4, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 732
    invoke-virtual {v7}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    .line 733
    sget-object v1, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "elapsed (%s)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v7, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 735
    if-nez v0, :cond_0

    move-object v0, v6

    .line 736
    goto :goto_0

    .line 720
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 202
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "deleteAllMsgWithExceptionStatus(), inboxNo(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 204
    if-nez v0, :cond_0

    .line 205
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "not exist inbox : %s "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    const/4 v0, -0x1

    .line 209
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-virtual {v0, p2}, Lcom/sec/chaton/msgsend/c;->a(Ljava/util/ArrayList;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "deleteAllMsgWithExceptionMsgList(), inboxNo(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 190
    if-nez v0, :cond_0

    .line 191
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "not exist inbox : %s "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    const/4 v0, -0x1

    .line 195
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-virtual {v0, p2, p3}, Lcom/sec/chaton/msgsend/c;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/c;
    .locals 11

    .prologue
    .line 147
    monitor-enter p0

    const/4 v5, 0x0

    const/high16 v6, -0x80000000

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v10, p5

    :try_start_0
    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Boolean;JLcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 148
    monitor-exit p0

    return-object v0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZJLcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/c;
    .locals 11

    .prologue
    .line 142
    monitor-enter p0

    :try_start_0
    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Boolean;JLcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 143
    monitor-exit p0

    return-object v0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/sec/chaton/msgsend/x;)V
    .locals 9

    .prologue
    .line 330
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "sendAllChatONMsg(), reason(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 331
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, " -------------- (ACTION !!) SEND ALL CHATON MSG in actually here. (%s) -------------- "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 332
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    const-string v0, "Try to send all"

    invoke-static {}, Lcom/sec/chaton/msgsend/l;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/msgsend/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_0
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v2

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 339
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 340
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 341
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 342
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 343
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 344
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 345
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->g()Lcom/sec/chaton/msgsend/w;

    move-result-object v0

    .line 346
    sget-object v5, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v6, "inbox(%s) sending messages result : (%s)"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v8, 0x1

    aput-object v0, v7, v8

    invoke-static {v5, v6, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 349
    sget-object v5, Lcom/sec/chaton/msgsend/m;->a:[I

    iget-object v6, v0, Lcom/sec/chaton/msgsend/w;->c:Lcom/sec/chaton/msgsend/v;

    invoke-virtual {v6}, Lcom/sec/chaton/msgsend/v;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 351
    :pswitch_0
    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 357
    :cond_1
    :try_start_1
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 358
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 359
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 360
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 361
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 362
    iget-object v1, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/msgsend/c;

    .line 363
    if-eqz v1, :cond_2

    .line 364
    sget-object v4, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v5, "sendAllChatONMsg() - REMOVE Useless Room"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 365
    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/c;->b()V

    .line 366
    iget-object v1, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 371
    :cond_3
    invoke-virtual {v2}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    .line 372
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "elapsed in sending all messages : (%s) "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373
    monitor-exit p0

    return-void

    .line 349
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized a(IIILcom/sec/chaton/j/c/k;)Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 922
    monitor-enter p0

    if-nez p4, :cond_0

    .line 923
    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "FileUpload result is NULL !! "

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v4

    .line 992
    :goto_0
    monitor-exit p0

    return v0

    .line 927
    :cond_0
    :try_start_1
    invoke-virtual {p4}, Lcom/sec/chaton/j/c/k;->b()J

    move-result-wide v2

    .line 928
    invoke-virtual {p4}, Lcom/sec/chaton/j/c/k;->a()Z

    move-result v0

    .line 929
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/c/g;->a(J)Lcom/sec/chaton/j/c/i;

    move-result-object v1

    .line 931
    if-nez v1, :cond_1

    .line 932
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "FileUploadTask is not in MANAGER !! "

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v4

    .line 933
    goto :goto_0

    .line 936
    :cond_1
    invoke-virtual {v1}, Lcom/sec/chaton/j/c/i;->a()Ljava/lang/String;

    move-result-object v1

    .line 938
    sget-object v5, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v6, "setFileUploadCompleteResult(),  -------- Result -- fileUploadResultType(%d), inboxNo(%s), msgId(%d), resultCode(%d), result(%s)"

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v1, v8, v9

    const/4 v9, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v8}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 941
    if-eqz v0, :cond_2

    .line 942
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    :goto_1
    move v0, v7

    .line 992
    goto :goto_0

    .line 948
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 949
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 950
    if-nez v0, :cond_3

    .line 951
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "not exist inbox : %s "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v3, v5

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v4

    .line 952
    goto :goto_0

    .line 955
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->d()J

    move-result-wide v4

    .line 956
    cmp-long v6, v4, v2

    if-nez v6, :cond_5

    .line 957
    sget-object v6, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v8, "IS THE SAME ALLOW CHAT MSG ID (true) : %d / %d"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v9, v10

    const/4 v4, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v9, v4

    invoke-static {v6, v8, v9}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 958
    const/4 v4, 0x0

    const-wide/16 v5, -0x1

    invoke-virtual {v0, v4, v5, v6}, Lcom/sec/chaton/msgsend/c;->a(ZJ)V

    .line 966
    :goto_2
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->a()Lcom/sec/chaton/e/r;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v4, v5, :cond_4

    .line 967
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->c()J

    move-result-wide v4

    .line 968
    cmp-long v6, v4, v2

    if-nez v6, :cond_6

    .line 969
    sget-object v6, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v8, "IS THE SAME INIT CHAT MSG ID (true) : %d / %d"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v9, v10

    const/4 v4, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v9, v4

    invoke-static {v6, v8, v9}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 970
    const/4 v4, 0x0

    const-wide/16 v5, -0x1

    invoke-virtual {v0, v4, v5, v6}, Lcom/sec/chaton/msgsend/c;->b(ZJ)V

    .line 980
    :cond_4
    :goto_3
    invoke-static {p2, p3}, Lcom/sec/chaton/j/c/i;->a(II)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 981
    sget-object v4, Lcom/sec/chaton/msgsend/aa;->c:Lcom/sec/chaton/msgsend/aa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z

    .line 988
    :goto_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    .line 989
    sget-object v1, Lcom/sec/chaton/msgsend/x;->d:Lcom/sec/chaton/msgsend/x;

    invoke-static {v0, v1}, Lcom/sec/chaton/msgsend/z;->a(Landroid/content/Context;Lcom/sec/chaton/msgsend/x;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 922
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 962
    :cond_5
    :try_start_2
    sget-object v6, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v8, "IS THE SAME ALLOW CHAT MSG ID (false) : %d / %d"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v9, v10

    const/4 v4, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v9, v4

    invoke-static {v6, v8, v9}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 974
    :cond_6
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v6, "IS THE SAME INIT CHAT MSG ID (false) : %d / %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v9

    const/4 v4, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v8, v4

    invoke-static {v0, v6, v8}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 984
    :cond_7
    sget-object v4, Lcom/sec/chaton/msgsend/aa;->b:Lcom/sec/chaton/msgsend/aa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method

.method public declared-synchronized a(ILjava/lang/String;Lcom/sec/chaton/d/bi;Z)Z
    .locals 10

    .prologue
    .line 486
    monitor-enter p0

    :try_start_0
    invoke-virtual {p3}, Lcom/sec/chaton/d/bi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 487
    if-nez v0, :cond_0

    .line 488
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "ERROR in setServerResult_direct(), oResultEntry is NULL ------------------------ "

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 489
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "ERROR in setServerResult_direct(), oResultEntry is NULL ------------------------ "

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 490
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "ERROR in setServerResult_direct(), oResultEntry is NULL ------------------------ "

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 491
    const/4 v0, 0x0

    .line 625
    :goto_0
    monitor-exit p0

    return v0

    .line 494
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v1

    .line 495
    iget-boolean v2, v0, Lcom/sec/chaton/a/a/k;->a:Z

    .line 496
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v3

    .line 499
    sget-object v5, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v6, "setServerResult_direct(),  -- Result -- GpbType(%s, %d), inboxNo(%s), msgId(%d), resultCode(%d), result(%s)"

    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p1}, Lcom/sec/chaton/j/g;->c(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    aput-object p2, v7, v8

    const/4 v8, 0x3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x5

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 502
    if-eqz p4, :cond_10

    .line 503
    sparse-switch p1, :sswitch_data_0

    .line 625
    :cond_1
    :goto_1
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 505
    :sswitch_1
    const/16 v2, 0x7d6

    if-eq v1, v2, :cond_2

    const/16 v2, 0xbc0

    if-ne v1, v2, :cond_3

    .line 506
    :cond_2
    sget-object v0, Lcom/sec/chaton/j/s;->a:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/j/s;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 486
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 508
    :cond_3
    :try_start_2
    invoke-static {v1}, Lcom/sec/chaton/j/q;->a(I)Z

    move-result v2

    if-nez v2, :cond_4

    const/16 v2, 0x1b5a

    if-eq v1, v2, :cond_4

    const/16 v2, 0x1b5b

    if-ne v1, v2, :cond_5

    .line 511
    :cond_4
    sget-object v0, Lcom/sec/chaton/j/s;->b:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/j/s;)V

    goto :goto_1

    .line 514
    :cond_5
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-eqz v0, :cond_6

    .line 515
    sget-object v0, Lcom/sec/chaton/j/s;->c:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/j/s;)V

    goto :goto_1

    .line 518
    :cond_6
    sget-object v0, Lcom/sec/chaton/j/s;->e:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/j/s;)V

    goto :goto_1

    .line 526
    :sswitch_2
    invoke-static {v1}, Lcom/sec/chaton/j/q;->a(I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 527
    sget-object v0, Lcom/sec/chaton/j/s;->b:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0, v3, v4}, Lcom/sec/chaton/msgsend/l;->b(Ljava/lang/String;Lcom/sec/chaton/j/s;J)V

    goto :goto_1

    .line 530
    :cond_7
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-eqz v0, :cond_8

    .line 531
    sget-object v0, Lcom/sec/chaton/j/s;->c:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0, v3, v4}, Lcom/sec/chaton/msgsend/l;->b(Ljava/lang/String;Lcom/sec/chaton/j/s;J)V

    goto :goto_1

    .line 534
    :cond_8
    sget-object v0, Lcom/sec/chaton/j/s;->e:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0, v3, v4}, Lcom/sec/chaton/msgsend/l;->b(Ljava/lang/String;Lcom/sec/chaton/j/s;J)V

    goto :goto_1

    .line 548
    :sswitch_3
    invoke-static {v1}, Lcom/sec/chaton/j/q;->a(I)Z

    move-result v2

    if-nez v2, :cond_9

    const/16 v2, 0x1a

    if-eq v1, v2, :cond_9

    const/16 v2, 0x1b5a

    if-eq v1, v2, :cond_9

    const/16 v2, 0x1b5b

    if-ne v1, v2, :cond_a

    .line 552
    :cond_9
    sget-object v0, Lcom/sec/chaton/j/s;->b:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0, v3, v4}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/j/s;J)V

    goto :goto_1

    .line 554
    :cond_a
    const/16 v2, 0x7d6

    if-eq v1, v2, :cond_b

    const/16 v2, 0xbc0

    if-ne v1, v2, :cond_c

    .line 555
    :cond_b
    sget-object v0, Lcom/sec/chaton/j/s;->a:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0, v3, v4}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/j/s;J)V

    goto :goto_1

    .line 557
    :cond_c
    const/16 v2, 0x384

    if-eq v1, v2, :cond_1

    .line 561
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-eqz v0, :cond_d

    .line 562
    sget-object v0, Lcom/sec/chaton/j/s;->c:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0, v3, v4}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/j/s;J)V

    goto/16 :goto_1

    .line 565
    :cond_d
    sget-object v0, Lcom/sec/chaton/j/s;->e:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0, v3, v4}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/j/s;J)V

    goto/16 :goto_1

    .line 572
    :sswitch_4
    invoke-static {v1}, Lcom/sec/chaton/j/q;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0x1b5a

    if-eq v1, v2, :cond_1

    const/16 v2, 0x1b5b

    if-eq v1, v2, :cond_1

    .line 576
    const/16 v2, 0xbbb

    if-eq v1, v2, :cond_e

    const/16 v2, 0x2710

    if-ne v1, v2, :cond_f

    .line 578
    :cond_e
    sget-object v0, Lcom/sec/chaton/j/s;->c:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/j/s;)V

    goto/16 :goto_1

    .line 580
    :cond_f
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-eqz v0, :cond_1

    .line 581
    invoke-virtual {p0, p2}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;)Z

    goto/16 :goto_1

    .line 597
    :cond_10
    sparse-switch p1, :sswitch_data_1

    goto/16 :goto_1

    .line 599
    :sswitch_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    const/16 v1, 0xbbb

    if-ne v0, v1, :cond_1

    .line 600
    sget-object v0, Lcom/sec/chaton/j/s;->c:Lcom/sec/chaton/j/s;

    invoke-direct {p0, p2, v0}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/j/s;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 503
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x2 -> :sswitch_1
        0x4 -> :sswitch_3
        0x8 -> :sswitch_4
        0xc -> :sswitch_0
        0x12 -> :sswitch_0
        0x1f -> :sswitch_0
        0x26 -> :sswitch_0
        0x66 -> :sswitch_3
        0x6a -> :sswitch_2
        0x6b -> :sswitch_0
    .end sparse-switch

    .line 597
    :sswitch_data_1
    .sparse-switch
        0x2 -> :sswitch_5
        0x6 -> :sswitch_0
        0x22 -> :sswitch_0
        0x24 -> :sswitch_0
        0x67 -> :sswitch_0
        0x68 -> :sswitch_0
        0x6b -> :sswitch_0
    .end sparse-switch
.end method

.method public declared-synchronized a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 172
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v3, "deleteInbox(), inboxNo(%s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 174
    if-nez v0, :cond_0

    .line 175
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "not exist inbox : %s "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 181
    :goto_0
    monitor-exit p0

    return v0

    .line 179
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->b()V

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v2

    .line 181
    goto :goto_0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;J)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 250
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v3, "deleteMsg(), inboxNo(%s), msgId(%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 252
    if-nez v0, :cond_0

    .line 253
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "not exist inbox : %s "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    :goto_0
    monitor-exit p0

    return v2

    :cond_0
    :try_start_1
    invoke-virtual {v0, p2, p3}, Lcom/sec/chaton/msgsend/c;->b(J)Lcom/sec/chaton/msgsend/ChatONMsgEntity;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 653
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "updateMsg(), inboxNo(%s), msgId(%d), sendStatus(%s), formattedMsg(%s), isFileUploaded(%s)"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p4, v3, v4

    const/4 v4, 0x3

    aput-object p5, v3, v4

    const/4 v4, 0x4

    aput-object p6, v3, v4

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 656
    const/4 v0, 0x0

    .line 657
    iget-object v2, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    if-eqz v2, :cond_0

    .line 658
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 660
    :cond_0
    if-nez v0, :cond_1

    move v0, v1

    .line 664
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    move-wide v1, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    :try_start_1
    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/msgsend/c;->a(JLcom/sec/chaton/msgsend/aa;Ljava/lang/String;Ljava/lang/Integer;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 653
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;JLjava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 643
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "updateMsgMessage(), inboxNo(%s), msgId(%d), chatMsg(%s)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p4, v3, v4

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 644
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 645
    if-nez v0, :cond_0

    move v0, v1

    .line 649
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/chaton/msgsend/c;->a(JLjava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 643
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 216
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "deleteAllMsgWithStatus(), inboxNo(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 218
    if-nez v0, :cond_0

    .line 219
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "not exist inbox : %s "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    const/4 v0, -0x1

    .line 223
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-virtual {v0, p2}, Lcom/sec/chaton/msgsend/c;->b(Ljava/util/ArrayList;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 230
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "deleteMsgWithTargetList(), inboxNo(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 232
    if-nez v0, :cond_0

    .line 233
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "not exist inbox : %s "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    const/4 v0, -0x1

    .line 237
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-virtual {v0, p2, p3}, Lcom/sec/chaton/msgsend/c;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 230
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 8

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "__print_all_data()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 136
    :goto_0
    monitor-exit p0

    return-void

    .line 106
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v2

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 123
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 124
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 125
    sget-object v1, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v4, "----------- print all message (%d chatrooms)----------"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v1, v4, v5}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 128
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 129
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 130
    sget-object v4, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v5, "  [CHATROOM]  key(%s) : %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v1, 0x1

    aput-object v0, v6, v1

    invoke-static {v4, v5, v6}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 134
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    .line 135
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized d()V
    .locals 5

    .prologue
    .line 152
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v1, "deleteAll()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v1

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 157
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 158
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 160
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgsend/c;

    .line 161
    invoke-virtual {v0}, Lcom/sec/chaton/msgsend/c;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 165
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 167
    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    .line 168
    sget-object v0, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "elapsed in \'DELETE ALL INBOX & MSG (%s) "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized e()Z
    .locals 22

    .prologue
    .line 260
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "loadMessageFromDB()"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    invoke-static {}, Lcom/sec/chaton/msgsend/i;->a()Lcom/sec/chaton/msgsend/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/msgsend/i;->b()Lcom/sec/chaton/msgsend/i;

    move-result-object v13

    .line 264
    invoke-direct/range {p0 .. p0}, Lcom/sec/chaton/msgsend/l;->f()Landroid/database/Cursor;

    move-result-object v14

    .line 265
    if-nez v14, :cond_0

    .line 266
    sget-object v1, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "query result is (null)"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    const/4 v1, 0x0

    .line 325
    :goto_0
    monitor-exit p0

    return v1

    .line 270
    :cond_0
    :try_start_1
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 271
    sget-object v2, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v3, "query result COUNT is (%d)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/msgsend/l;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 275
    sget-object v1, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "Some MSGs are already exist in MessageList"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 278
    :cond_1
    const/4 v1, -0x1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 279
    :cond_2
    :goto_1
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 281
    const-string v1, "message_inbox_no"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 282
    const-string v1, "inbox_session_id"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 283
    const/4 v15, 0x0

    .line 284
    const-string v1, "inbox_chat_type"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v16

    .line 286
    const-string v1, "_id"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 287
    const-string v1, "message_content_type"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v4

    .line 288
    const-string v1, "message_content"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 289
    const-string v1, "message_content_translated"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 291
    const-string v1, "message_type"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 293
    const-string v1, "message_download_uri"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 294
    const-string v1, "message_formatted"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 295
    const-string v1, "message_is_file_upload"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 298
    const-string v1, "inbox_server_ip"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 299
    const-string v1, "inbox_server_port"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 301
    const-string v1, "Y"

    const-string v19, "inbox_valid"

    move-object/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    .line 303
    const-string v1, "lasst_session_merge_time"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 307
    const/4 v1, 0x1

    move/from16 v0, v19

    if-ne v0, v1, :cond_2

    .line 308
    new-instance v1, Lcom/sec/chaton/msgsend/ChatONMsgEntity;

    invoke-direct/range {v1 .. v10}, Lcom/sec/chaton/msgsend/ChatONMsgEntity;-><init>(JLcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    move-object/from16 v2, p0

    move-object v3, v11

    move-object/from16 v4, v16

    move-object v5, v15

    move-object v6, v12

    move-object/from16 v7, v17

    move/from16 v8, v18

    move/from16 v9, v19

    move-wide/from16 v10, v20

    move-object v12, v1

    .line 314
    invoke-virtual/range {v2 .. v12}, Lcom/sec/chaton/msgsend/l;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZJLcom/sec/chaton/msgsend/ChatONMsgEntity;)Lcom/sec/chaton/msgsend/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 260
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 319
    :cond_3
    :try_start_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 321
    invoke-virtual {v13}, Lcom/sec/chaton/msgsend/i;->c()Lcom/sec/chaton/msgsend/i;

    .line 322
    sget-object v1, Lcom/sec/chaton/msgsend/l;->a:Ljava/lang/String;

    const-string v2, "LoadMessageFromDB - end (%s) "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v13, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 324
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/msgsend/l;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 325
    const/4 v1, 0x1

    goto/16 :goto_0
.end method
