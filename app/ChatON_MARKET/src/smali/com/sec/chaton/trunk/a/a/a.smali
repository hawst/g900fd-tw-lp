.class public Lcom/sec/chaton/trunk/a/a/a;
.super Lcom/sec/chaton/d/a/a;
.source "AddCommentTask.java"


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 51
    iput-object p3, p0, Lcom/sec/chaton/trunk/a/a/a;->b:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lcom/sec/chaton/trunk/a/a/a;->c:Ljava/lang/String;

    .line 53
    iput-object p5, p0, Lcom/sec/chaton/trunk/a/a/a;->e:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_0

    .line 114
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 115
    const-string v0, "01000016"

    const-string v1, "0201"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    .line 119
    :cond_0
    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 60
    const-string v1, "sessionid"

    iget-object v2, p0, Lcom/sec/chaton/trunk/a/a/a;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v1, "itemid"

    iget-object v2, p0, Lcom/sec/chaton/trunk/a/a/a;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    const-string v1, "comment"

    iget-object v2, p0, Lcom/sec/chaton/trunk/a/a/a;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    new-instance v1, Lcom/sec/chaton/util/af;

    invoke-direct {v1}, Lcom/sec/chaton/util/af;-><init>()V

    .line 64
    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/af;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/sec/chaton/trunk/a/a/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-object v0
.end method
