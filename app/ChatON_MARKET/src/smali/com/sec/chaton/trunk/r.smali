.class public Lcom/sec/chaton/trunk/r;
.super Landroid/widget/BaseAdapter;
.source "TrunkAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final b:Ljava/lang/String;

.field private static o:Lcom/sec/chaton/e/a/u;

.field private static q:Lcom/sec/chaton/e/a/v;


# instance fields
.field public a:Z

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lcom/sec/chaton/trunk/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/sec/common/f/c;

.field private f:Landroid/content/Context;

.field private g:Lcom/sec/chaton/trunk/a/b;

.field private h:Ljava/lang/String;

.field private i:Lcom/sec/chaton/trunk/TrunkView;

.field private j:I

.field private k:I

.field private l:Landroid/database/Cursor;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:Landroid/view/LayoutInflater;

.field private p:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    .line 806
    new-instance v0, Lcom/sec/chaton/trunk/t;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/t;-><init>()V

    sput-object v0, Lcom/sec/chaton/trunk/r;->q:Lcom/sec/chaton/e/a/v;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;ILcom/sec/chaton/trunk/a/b;Ljava/lang/String;Lcom/sec/common/f/c;Lcom/sec/chaton/trunk/TrunkView;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            "I",
            "Lcom/sec/chaton/trunk/a/b;",
            "Ljava/lang/String;",
            "Lcom/sec/common/f/c;",
            "Lcom/sec/chaton/trunk/TrunkView;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 72
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/chaton/trunk/r;->k:I

    .line 83
    new-instance v0, Lcom/sec/chaton/trunk/s;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/s;-><init>(Lcom/sec/chaton/trunk/r;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/r;->p:Landroid/os/Handler;

    .line 164
    iput-object p6, p0, Lcom/sec/chaton/trunk/r;->e:Lcom/sec/common/f/c;

    .line 166
    iput-object p1, p0, Lcom/sec/chaton/trunk/r;->f:Landroid/content/Context;

    .line 167
    iput-object p4, p0, Lcom/sec/chaton/trunk/r;->g:Lcom/sec/chaton/trunk/a/b;

    .line 168
    iput-object p5, p0, Lcom/sec/chaton/trunk/r;->h:Ljava/lang/String;

    .line 169
    iput-object p7, p0, Lcom/sec/chaton/trunk/r;->i:Lcom/sec/chaton/trunk/TrunkView;

    .line 170
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/r;->d:Ljava/util/Map;

    .line 171
    iput-object p2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    .line 172
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    .line 174
    iput-object p8, p0, Lcom/sec/chaton/trunk/r;->m:Ljava/util/List;

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->f:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/trunk/r;->n:Landroid/view/LayoutInflater;

    .line 178
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 179
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v0

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/sec/chaton/trunk/r;->j:I

    .line 185
    :goto_0
    return-void

    .line 181
    :cond_0
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v0

    div-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/chaton/trunk/r;->j:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/r;)Lcom/sec/chaton/trunk/TrunkView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->i:Lcom/sec/chaton/trunk/TrunkView;

    return-object v0
.end method

.method private a(Landroid/view/View;)Lcom/sec/chaton/trunk/w;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 581
    new-instance v1, Lcom/sec/chaton/trunk/w;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/w;-><init>(Lcom/sec/chaton/trunk/r;)V

    .line 583
    const v0, 0x7f07048a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 584
    invoke-direct {p0, v3, v0, v1}, Lcom/sec/chaton/trunk/r;->a(ILandroid/view/View;Lcom/sec/chaton/trunk/w;)V

    .line 585
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 586
    iget v2, p0, Lcom/sec/chaton/trunk/r;->j:I

    mul-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 587
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v2

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 589
    const v0, 0x7f07014b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 590
    invoke-direct {p0, v4, v0, v1}, Lcom/sec/chaton/trunk/r;->a(ILandroid/view/View;Lcom/sec/chaton/trunk/w;)V

    .line 591
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 592
    iget v2, p0, Lcom/sec/chaton/trunk/r;->j:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 593
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v2

    aget-object v2, v2, v4

    iget-object v2, v2, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 595
    const v0, 0x7f0702e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 596
    invoke-direct {p0, v5, v0, v1}, Lcom/sec/chaton/trunk/r;->a(ILandroid/view/View;Lcom/sec/chaton/trunk/w;)V

    .line 597
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 598
    iget v2, p0, Lcom/sec/chaton/trunk/r;->j:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 599
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v2

    aget-object v2, v2, v5

    iget-object v2, v2, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 601
    const v0, 0x7f07048b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 602
    invoke-direct {p0, v6, v0, v1}, Lcom/sec/chaton/trunk/r;->a(ILandroid/view/View;Lcom/sec/chaton/trunk/w;)V

    .line 603
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    aget-object v0, v0, v6

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 604
    iget v2, p0, Lcom/sec/chaton/trunk/r;->j:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 605
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v2

    aget-object v2, v2, v6

    iget-object v2, v2, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 607
    const v0, 0x7f07048c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 608
    invoke-direct {p0, v7, v0, v1}, Lcom/sec/chaton/trunk/r;->a(ILandroid/view/View;Lcom/sec/chaton/trunk/w;)V

    .line 609
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    aget-object v0, v0, v7

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 610
    iget v2, p0, Lcom/sec/chaton/trunk/r;->j:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 611
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v2

    aget-object v2, v2, v7

    iget-object v2, v2, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 613
    const v0, 0x7f07048d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 614
    const/4 v2, 0x5

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/chaton/trunk/r;->a(ILandroid/view/View;Lcom/sec/chaton/trunk/w;)V

    .line 615
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    const/4 v2, 0x5

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 616
    iget v2, p0, Lcom/sec/chaton/trunk/r;->j:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 617
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v2

    const/4 v3, 0x5

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 619
    const v0, 0x7f07048e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 620
    const/4 v2, 0x6

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/chaton/trunk/r;->a(ILandroid/view/View;Lcom/sec/chaton/trunk/w;)V

    .line 621
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    const/4 v2, 0x6

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 622
    iget v2, p0, Lcom/sec/chaton/trunk/r;->j:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 623
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v2

    const/4 v3, 0x6

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 625
    const v0, 0x7f07048f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 626
    const/4 v2, 0x7

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/chaton/trunk/r;->a(ILandroid/view/View;Lcom/sec/chaton/trunk/w;)V

    .line 627
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    const/4 v2, 0x7

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 628
    iget v2, p0, Lcom/sec/chaton/trunk/r;->j:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 629
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v2

    const/4 v3, 0x7

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 631
    const v0, 0x7f070490

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 632
    const/16 v2, 0x8

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/chaton/trunk/r;->a(ILandroid/view/View;Lcom/sec/chaton/trunk/w;)V

    .line 633
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    const/16 v2, 0x8

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 634
    iget v2, p0, Lcom/sec/chaton/trunk/r;->j:I

    mul-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 635
    invoke-static {v1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v2

    const/16 v3, 0x8

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 637
    return-object v1
.end method

.method private a(ILandroid/view/View;Lcom/sec/chaton/trunk/w;)V
    .locals 3

    .prologue
    .line 574
    invoke-static {p3}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/trunk/u;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p2, v2}, Lcom/sec/chaton/trunk/u;-><init>(Lcom/sec/chaton/trunk/r;Landroid/view/View;Lcom/sec/chaton/trunk/s;)V

    aput-object v1, v0, p1

    .line 575
    invoke-static {p3}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 577
    return-void
.end method

.method private a(Landroid/widget/ImageView;Lcom/sec/chaton/trunk/c/g;)V
    .locals 1

    .prologue
    .line 763
    if-eqz p1, :cond_1

    .line 764
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne p2, v0, :cond_2

    .line 765
    const v0, 0x7f020138

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 770
    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 772
    :cond_1
    return-void

    .line 767
    :cond_2
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne p2, v0, :cond_0

    .line 768
    const v0, 0x7f02012b

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private a(Landroid/widget/ImageView;Lcom/sec/chaton/trunk/c/g;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 529
    if-eqz p1, :cond_1

    if-eqz p3, :cond_1

    .line 531
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-ne p2, v0, :cond_0

    .line 532
    const v0, 0x7f020440

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 533
    const/16 v0, 0x8

    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 536
    :cond_0
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne p2, v0, :cond_1

    .line 537
    const v0, 0x7f020447

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 541
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/r;Landroid/widget/ImageView;Lcom/sec/chaton/trunk/c/g;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/trunk/r;->a(Landroid/widget/ImageView;Lcom/sec/chaton/trunk/c/g;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/r;Landroid/widget/ImageView;Lcom/sec/chaton/trunk/c/g;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/trunk/r;->a(Landroid/widget/ImageView;Lcom/sec/chaton/trunk/c/g;Landroid/widget/ImageView;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/trunk/u;Lcom/sec/chaton/trunk/v;)V
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 663
    const-string v0, ""

    .line 664
    const-string v0, ""

    .line 665
    iget-object v7, p1, Lcom/sec/chaton/trunk/u;->b:Landroid/widget/ImageView;

    .line 666
    iget-object v8, p1, Lcom/sec/chaton/trunk/u;->h:Landroid/widget/ImageView;

    .line 668
    iget-object v0, p2, Lcom/sec/chaton/trunk/v;->g:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_4

    .line 669
    invoke-direct {p0}, Lcom/sec/chaton/trunk/r;->f()Ljava/lang/String;

    move-result-object v3

    .line 674
    :goto_0
    const-string v0, ""

    .line 677
    :try_start_0
    iget-object v0, p2, Lcom/sec/chaton/trunk/v;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 685
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 688
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->d:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/b/b;

    .line 690
    if-eqz v0, :cond_0

    .line 691
    invoke-virtual {v0, v9}, Lcom/sec/chaton/trunk/b/b;->a(Ljava/lang/Object;)V

    .line 692
    invoke-virtual {v0, v6}, Lcom/sec/chaton/trunk/b/b;->cancel(Z)Z

    .line 696
    :cond_0
    invoke-static {}, Lcom/sec/chaton/trunk/b/a;->a()Lcom/sec/chaton/trunk/b/a;

    move-result-object v0

    .line 703
    invoke-static {v1}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 705
    invoke-static {v1}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 706
    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    iput-object v1, p2, Lcom/sec/chaton/trunk/v;->g:Lcom/sec/chaton/trunk/c/g;

    .line 709
    :cond_1
    if-nez v2, :cond_6

    .line 711
    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 713
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->e:Lcom/sec/common/f/c;

    invoke-virtual {v1, v7}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 715
    new-instance v5, Lcom/sec/chaton/trunk/x;

    invoke-direct {v5, p0, v9}, Lcom/sec/chaton/trunk/x;-><init>(Lcom/sec/chaton/trunk/r;Lcom/sec/chaton/trunk/s;)V

    .line 716
    invoke-virtual {v5, v8}, Lcom/sec/chaton/trunk/x;->b(Landroid/widget/ImageView;)V

    .line 718
    iget-object v1, p2, Lcom/sec/chaton/trunk/v;->g:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-eq v1, v2, :cond_2

    iget-object v1, p2, Lcom/sec/chaton/trunk/v;->g:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v2, :cond_5

    .line 719
    :cond_2
    const-string v1, "Can\'t find cache data. Execting download."

    sget-object v2, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    iget-object v1, p2, Lcom/sec/chaton/trunk/v;->g:Lcom/sec/chaton/trunk/c/g;

    invoke-virtual {v5, v1}, Lcom/sec/chaton/trunk/x;->a(Lcom/sec/chaton/trunk/c/g;)V

    .line 724
    invoke-virtual {v5, v7}, Lcom/sec/chaton/trunk/x;->a(Landroid/widget/ImageView;)V

    .line 725
    iget-object v1, p1, Lcom/sec/chaton/trunk/u;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v5, v1}, Lcom/sec/chaton/trunk/x;->a(Landroid/widget/ProgressBar;)V

    .line 727
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->p:Landroid/os/Handler;

    iget-object v2, p2, Lcom/sec/chaton/trunk/v;->d:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/trunk/b/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/sec/chaton/trunk/b/b;

    move-result-object v0

    .line 729
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->d:Ljava/util/Map;

    invoke-interface {v1, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 732
    iget-object v0, p1, Lcom/sec/chaton/trunk/u;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 737
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 759
    :cond_3
    :goto_1
    return-void

    .line 671
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/trunk/r;->g()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 678
    :catch_0
    move-exception v0

    .line 679
    const-string v1, "TrunkAdapter.ExtractFileNameFromUrl"

    sget-object v2, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    sget-object v1, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 740
    :cond_5
    iget-object v0, p2, Lcom/sec/chaton/trunk/v;->g:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_3

    .line 741
    const-string v0, "Can\'t find cache data. Show default image for video."

    sget-object v1, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    invoke-direct {p0, v7, v0, v8}, Lcom/sec/chaton/trunk/r;->a(Landroid/widget/ImageView;Lcom/sec/chaton/trunk/c/g;Landroid/widget/ImageView;)V

    .line 745
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    invoke-direct {p0, v8, v0}, Lcom/sec/chaton/trunk/r;->a(Landroid/widget/ImageView;Lcom/sec/chaton/trunk/c/g;)V

    goto :goto_1

    .line 750
    :cond_6
    const-string v0, "Found cached data."

    sget-object v1, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fileUri: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "item.contentType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/trunk/v;->g:Lcom/sec/chaton/trunk/c/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    new-instance v0, Lcom/sec/chaton/trunk/al;

    iget-object v1, p2, Lcom/sec/chaton/trunk/v;->g:Lcom/sec/chaton/trunk/c/g;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3, v8}, Lcom/sec/chaton/trunk/al;-><init>(Landroid/net/Uri;Lcom/sec/chaton/trunk/c/g;Ljava/lang/Boolean;Landroid/widget/ImageView;)V

    .line 756
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->e:Lcom/sec/common/f/c;

    invoke-virtual {v1, v7, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_1
.end method

.method private a(Lcom/sec/chaton/trunk/w;I)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 641
    invoke-static {p1}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v0

    aget-object v0, v0, p2

    iget-object v0, v0, Lcom/sec/chaton/trunk/v;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 659
    :goto_0
    return-void

    .line 645
    :cond_0
    const-string v0, "ME"

    invoke-static {p1}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v1

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/chaton/trunk/v;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 647
    invoke-static {p1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    aget-object v0, v0, p2

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 649
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    invoke-static {p1}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v1

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/chaton/trunk/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 650
    invoke-static {p1}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v0

    aget-object v0, v0, p2

    iget-object v1, v0, Lcom/sec/chaton/trunk/v;->j:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    invoke-static {p1}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v2

    aget-object v2, v2, p2

    iget-object v2, v2, Lcom/sec/chaton/trunk/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 652
    :cond_1
    invoke-static {p1}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v0

    aget-object v0, v0, p2

    iget-object v0, v0, Lcom/sec/chaton/trunk/v;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 656
    :cond_2
    invoke-static {p1}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v0

    aget-object v0, v0, p2

    iget-object v0, v0, Lcom/sec/chaton/trunk/v;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 657
    invoke-static {p1}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v0

    aget-object v0, v0, p2

    iget-object v0, v0, Lcom/sec/chaton/trunk/u;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 878
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/trunk/r;->q:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    sput-object v0, Lcom/sec/chaton/trunk/r;->o:Lcom/sec/chaton/e/a/u;

    .line 879
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v2, v0

    aput-object p2, v2, v7

    const/4 v0, 0x2

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 881
    sget-object v0, Lcom/sec/chaton/trunk/r;->o:Lcom/sec/chaton/e/a/u;

    const/16 v1, 0x63

    sget-object v3, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "message_inbox_no=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "message_content_type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-virtual {v6}, Lcom/sec/chaton/e/w;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " OR "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "message_content_type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    invoke-virtual {v6}, Lcom/sec/chaton/e/w;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' OR "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "message_content_type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-virtual {v6}, Lcom/sec/chaton/e/w;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' )"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "message_content"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is not null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "message_type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "!="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "message_type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "!="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/r;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/r;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->e:Lcom/sec/common/f/c;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/r;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/chaton/trunk/r;->k:I

    return v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e()Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/chaton/trunk/r;->o:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method private f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 780
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 782
    invoke-static {}, Lcom/sec/chaton/trunk/c/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 783
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 784
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 786
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 795
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 797
    invoke-static {}, Lcom/sec/chaton/trunk/c/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 798
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 799
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 800
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 801
    const-string v1, "thumbnail"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 803
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 562
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 567
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    .line 570
    return-void

    .line 567
    :catchall_0
    move-exception v0

    iput-object p1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    throw v0
.end method

.method public a(Lcom/sec/chaton/trunk/a/b;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/sec/chaton/trunk/r;->g:Lcom/sec/chaton/trunk/a/b;

    .line 545
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/v;Z)V
    .locals 3

    .prologue
    .line 548
    if-eqz p2, :cond_1

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/trunk/v;->a:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/trunk/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/trunk/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 556
    iput-object p1, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    .line 557
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 375
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 376
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 378
    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 381
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v3, "sender_uid"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 382
    const-string v2, "ME"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 383
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v3, "item_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 384
    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 399
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 401
    :cond_2
    return-void

    .line 389
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v3, "sender_uid"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 390
    const-string v2, "ME"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 391
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v3, "item_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 392
    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 393
    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 364
    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 408
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/b/b;

    .line 410
    if-eqz v0, :cond_0

    .line 411
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/b/b;->a(Ljava/lang/Object;)V

    .line 412
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/b/b;->cancel(Z)Z

    goto :goto_0

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 417
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/chaton/trunk/r;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 200
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15

    .prologue
    .line 205
    const-string v1, "yeseul / getView Start "

    sget-object v2, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTag position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " get: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/trunk/r;->k:I

    rem-int v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    if-nez p2, :cond_1

    .line 212
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->n:Landroid/view/LayoutInflater;

    const v2, 0x7f030114

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 213
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/r;->a(Landroid/view/View;)Lcom/sec/chaton/trunk/w;

    move-result-object v1

    .line 214
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v12, v1

    .line 224
    :goto_0
    iget v1, p0, Lcom/sec/chaton/trunk/r;->k:I

    mul-int v1, v1, p1

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gt v1, v2, :cond_2

    .line 225
    iget v1, p0, Lcom/sec/chaton/trunk/r;->k:I

    mul-int v1, v1, p1

    .line 229
    :goto_1
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 231
    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    const/4 v1, 0x0

    move v13, v1

    .line 235
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v3, "item_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 236
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v4, "item_type"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/trunk/database/d;->a(I)Lcom/sec/chaton/trunk/database/d;

    move-result-object v4

    .line 237
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v5, "total_comment_count"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 238
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v6, "thumbnail"

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 239
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v7, "unread_comment_count"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 240
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v8, "isams"

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 241
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v9, "content_type"

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "true"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;Z)Lcom/sec/chaton/trunk/c/g;

    move-result-object v9

    .line 242
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v10, "down_url"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 243
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    const-string v11, "sender_uid"

    invoke-interface {v2, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 246
    new-instance v1, Lcom/sec/chaton/trunk/v;

    move-object v2, p0

    invoke-direct/range {v1 .. v11}, Lcom/sec/chaton/trunk/v;-><init>(Lcom/sec/chaton/trunk/r;Ljava/lang/String;Lcom/sec/chaton/trunk/database/d;ILjava/lang/String;ILjava/lang/String;Lcom/sec/chaton/trunk/c/g;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 248
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v1, v13, 0x1

    iget v2, p0, Lcom/sec/chaton/trunk/r;->k:I

    if-lt v1, v2, :cond_9

    .line 252
    :cond_0
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lcom/sec/chaton/trunk/r;->k:I

    if-ge v1, v2, :cond_3

    .line 253
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_3
    iget v2, p0, Lcom/sec/chaton/trunk/r;->k:I

    if-ge v1, v2, :cond_3

    .line 254
    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->e:Lcom/sec/common/f/c;

    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v3

    aget-object v3, v3, v1

    iget-object v3, v3, Lcom/sec/chaton/trunk/u;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 253
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 217
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/trunk/w;

    .line 219
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTag sconvertView "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v12, v1

    goto/16 :goto_0

    .line 227
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/trunk/r;->l:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    goto/16 :goto_1

    .line 258
    :cond_3
    const/4 v1, 0x0

    move v2, v1

    :goto_4
    iget v1, p0, Lcom/sec/chaton/trunk/r;->k:I

    if-ge v2, v1, :cond_8

    .line 259
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v2, :cond_7

    .line 260
    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/trunk/v;

    .line 261
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v3

    aget-object v3, v3, v2

    iget-object v3, v3, Lcom/sec/chaton/trunk/u;->d:Landroid/widget/ProgressBar;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 262
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v3

    aget-object v3, v3, v2

    invoke-direct {p0, v3, v1}, Lcom/sec/chaton/trunk/r;->a(Lcom/sec/chaton/trunk/u;Lcom/sec/chaton/trunk/v;)V

    .line 264
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v3

    aget-object v3, v3, v2

    iget-object v3, v3, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    invoke-virtual {v3, v12}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 265
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v3

    aget-object v3, v3, v2

    iget-object v3, v3, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 266
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v3

    aput-object v1, v3, v2

    .line 267
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v3

    aget-object v3, v3, v2

    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v4

    aget-object v4, v4, v2

    iget-object v4, v4, Lcom/sec/chaton/trunk/u;->c:Landroid/widget/CheckBox;

    iput-object v4, v3, Lcom/sec/chaton/trunk/v;->j:Landroid/widget/CheckBox;

    .line 270
    iget v3, v1, Lcom/sec/chaton/trunk/v;->e:I

    if-lez v3, :cond_4

    .line 271
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v3

    aget-object v3, v3, v2

    iget-object v3, v3, Lcom/sec/chaton/trunk/u;->f:Landroid/widget/ImageView;

    const v4, 0x7f02043e

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 277
    :goto_5
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v3

    aget-object v3, v3, v2

    iget-object v3, v3, Lcom/sec/chaton/trunk/u;->e:Landroid/widget/TextView;

    iget v4, v1, Lcom/sec/chaton/trunk/v;->c:I

    invoke-static {v3, v4}, Lcom/sec/chaton/trunk/c/f;->a(Landroid/widget/TextView;I)V

    .line 279
    iget v1, v1, Lcom/sec/chaton/trunk/v;->c:I

    if-gtz v1, :cond_5

    .line 280
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/u;->e:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 281
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/u;->f:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 287
    :goto_6
    iget-boolean v1, p0, Lcom/sec/chaton/trunk/r;->a:Z

    if-eqz v1, :cond_6

    .line 288
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/v;->j:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 289
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/u;->g:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 290
    invoke-direct {p0, v12, v2}, Lcom/sec/chaton/trunk/r;->a(Lcom/sec/chaton/trunk/w;I)V

    .line 258
    :goto_7
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_4

    .line 274
    :cond_4
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v3

    aget-object v3, v3, v2

    iget-object v3, v3, Lcom/sec/chaton/trunk/u;->f:Landroid/widget/ImageView;

    const v4, 0x7f02043d

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_5

    .line 283
    :cond_5
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/u;->e:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/u;->f:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_6

    .line 292
    :cond_6
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/v;->j:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 293
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/u;->g:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_7

    .line 297
    :cond_7
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/u;->b:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 298
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 299
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->a(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/u;

    move-result-object v1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/sec/chaton/trunk/u;->a:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 300
    invoke-static {v12}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v1

    const/4 v3, 0x0

    aput-object v3, v1, v2

    goto :goto_7

    .line 304
    :cond_8
    const-string v1, "yeseul / getView End "

    sget-object v2, Lcom/sec/chaton/trunk/r;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    return-object p2

    :cond_9
    move v13, v1

    goto/16 :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 311
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/w;

    .line 315
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 350
    :goto_0
    :sswitch_0
    iget-object v2, p0, Lcom/sec/chaton/trunk/r;->i:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/w;->b(Lcom/sec/chaton/trunk/w;)[Lcom/sec/chaton/trunk/v;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v2, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/v;)V

    .line 352
    return-void

    .line 321
    :sswitch_1
    const/4 v1, 0x1

    .line 322
    goto :goto_0

    .line 325
    :sswitch_2
    const/4 v1, 0x2

    .line 326
    goto :goto_0

    .line 328
    :sswitch_3
    const/4 v1, 0x3

    .line 329
    goto :goto_0

    .line 331
    :sswitch_4
    const/4 v1, 0x4

    .line 332
    goto :goto_0

    .line 334
    :sswitch_5
    const/4 v1, 0x5

    .line 335
    goto :goto_0

    .line 337
    :sswitch_6
    const/4 v1, 0x6

    .line 338
    goto :goto_0

    .line 340
    :sswitch_7
    const/4 v1, 0x7

    .line 341
    goto :goto_0

    .line 343
    :sswitch_8
    const/16 v1, 0x8

    .line 344
    goto :goto_0

    .line 315
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f07014b -> :sswitch_1
        0x7f0702e1 -> :sswitch_2
        0x7f07048a -> :sswitch_0
        0x7f07048b -> :sswitch_3
        0x7f07048c -> :sswitch_4
        0x7f07048d -> :sswitch_5
        0x7f07048e -> :sswitch_6
        0x7f07048f -> :sswitch_7
        0x7f070490 -> :sswitch_8
    .end sparse-switch
.end method
