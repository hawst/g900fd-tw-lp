.class public Lcom/sec/chaton/trunk/ba;
.super Lcom/sec/chaton/trunk/a;
.source "TrunkItemPresenter.java"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Landroid/os/Handler;

.field private d:Lcom/sec/chaton/trunk/i;

.field private e:Landroid/app/Activity;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Landroid/graphics/Bitmap;

.field private k:Ljava/lang/String;

.field private l:J

.field private m:Ljava/lang/String;

.field private n:Lcom/sec/chaton/trunk/c/g;

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Lcom/sec/chaton/trunk/a/a;

.field private t:Landroid/os/Handler;

.field private u:Lcom/sec/chaton/trunk/a/a/e;

.field private v:Lcom/sec/chaton/trunk/a/a/c;

.field private w:Lcom/sec/chaton/trunk/b/b;

.field private x:Lcom/sec/chaton/trunk/a/a/b;

.field private y:Lcom/sec/chaton/trunk/a/a/i;

.field private z:Lcom/sec/chaton/trunk/a/a/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/sec/chaton/trunk/ba;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/trunk/i;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/sec/chaton/trunk/a;-><init>()V

    .line 102
    new-instance v0, Lcom/sec/chaton/trunk/bb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/bb;-><init>(Lcom/sec/chaton/trunk/ba;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->C:Landroid/os/Handler;

    .line 215
    iput-object p3, p0, Lcom/sec/chaton/trunk/ba;->f:Ljava/lang/String;

    .line 216
    iput-object p4, p0, Lcom/sec/chaton/trunk/ba;->g:Ljava/lang/String;

    .line 217
    iput-object p5, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    .line 218
    iput-object p1, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    .line 219
    iput-object p2, p0, Lcom/sec/chaton/trunk/ba;->t:Landroid/os/Handler;

    .line 220
    iput-object p6, p0, Lcom/sec/chaton/trunk/ba;->A:Ljava/lang/String;

    .line 221
    iput-object p7, p0, Lcom/sec/chaton/trunk/ba;->B:Ljava/lang/String;

    .line 222
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->t:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/ba;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    return-object p1
.end method

.method private a(II)V
    .locals 6

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ba;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 380
    :goto_0
    return-void

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->u:Lcom/sec/chaton/trunk/a/a/e;

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->u:Lcom/sec/chaton/trunk/a/a/e;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/e;->c()V

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 358
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/trunk/database/a/a;->f(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 371
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget v1, p0, Lcom/sec/chaton/trunk/ba;->o:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/i;->a(ILandroid/database/Cursor;)V

    .line 374
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 375
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->s:Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/sec/chaton/trunk/a/a;->a(Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/chaton/trunk/a/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->u:Lcom/sec/chaton/trunk/a/a/e;

    goto :goto_0

    .line 359
    :catch_0
    move-exception v0

    .line 360
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 361
    sget-object v1, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 363
    :catch_1
    move-exception v0

    .line 364
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 365
    sget-object v1, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 377
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->s:Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->A:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/trunk/ba;->B:Ljava/lang/String;

    const/16 v4, 0x64

    const/16 v5, 0xef

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/trunk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/chaton/trunk/a/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->u:Lcom/sec/chaton/trunk/a/a/e;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/ba;II)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/trunk/ba;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/ba;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/trunk/ba;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 455
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ba;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 464
    :goto_0
    return-void

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->y:Lcom/sec/chaton/trunk/a/a/i;

    if-eqz v0, :cond_1

    .line 460
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->y:Lcom/sec/chaton/trunk/a/a/i;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/i;->c()V

    .line 463
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->s:Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/chaton/trunk/a/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/trunk/a/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->y:Lcom/sec/chaton/trunk/a/a/i;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ba;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 446
    :goto_0
    return-void

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->v:Lcom/sec/chaton/trunk/a/a/c;

    if-eqz v0, :cond_1

    .line 442
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->v:Lcom/sec/chaton/trunk/a/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/c;->c()V

    .line 445
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->s:Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/sec/chaton/trunk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/chaton/trunk/a/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->v:Lcom/sec/chaton/trunk/a/a/c;

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/ba;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 663
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/chaton/trunk/c/a;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    const/4 v1, 0x3

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "thumbnail"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/ba;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/ba;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/ba;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 672
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/chaton/trunk/c/a;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/ba;)Lcom/sec/chaton/trunk/a/a/e;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->u:Lcom/sec/chaton/trunk/a/a/e;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/ba;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ba;->p()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/ba;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ba;->k()V

    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/trunk/ba;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ba;->m()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/trunk/ba;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ba;->l()V

    return-void
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    return-object v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 390
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ba;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 399
    :goto_0
    return-void

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->x:Lcom/sec/chaton/trunk/a/a/b;

    if-eqz v0, :cond_1

    .line 395
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->x:Lcom/sec/chaton/trunk/a/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/b;->c()V

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->s:Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/trunk/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/trunk/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->x:Lcom/sec/chaton/trunk/a/a/b;

    goto :goto_0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ba;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->z:Lcom/sec/chaton/trunk/a/a/g;

    if-eqz v0, :cond_2

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->z:Lcom/sec/chaton/trunk/a/a/g;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/g;->c()V

    .line 416
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/trunk/TrunkDetailActivity;

    .line 418
    if-eqz v0, :cond_3

    .line 419
    iget-object v0, v0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    .line 423
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->s:Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/trunk/a/a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/trunk/a/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->z:Lcom/sec/chaton/trunk/a/a/g;

    goto :goto_0
.end method

.method private m()V
    .locals 7

    .prologue
    .line 470
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ba;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 504
    :goto_0
    return-void

    .line 475
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/ba;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->m:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 477
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v3, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v4, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 479
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->w:Lcom/sec/chaton/trunk/b/b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->w:Lcom/sec/chaton/trunk/b/b;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_2

    .line 483
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 484
    const-string v0, "Cancel previous download task."

    sget-object v1, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->w:Lcom/sec/chaton/trunk/b/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/b/b;->cancel(Z)Z

    .line 491
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->n:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_4

    .line 492
    invoke-static {}, Lcom/sec/chaton/trunk/b/a;->a()Lcom/sec/chaton/trunk/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->C:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->m:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/trunk/b/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/sec/chaton/trunk/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->w:Lcom/sec/chaton/trunk/b/b;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 497
    :catch_0
    move-exception v0

    .line 498
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 499
    sget-object v1, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 502
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->w:Lcom/sec/chaton/trunk/b/b;

    goto :goto_0

    .line 494
    :cond_4
    :try_start_1
    invoke-static {}, Lcom/sec/chaton/trunk/b/a;->a()Lcom/sec/chaton/trunk/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->C:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->m:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/trunk/b/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/sec/chaton/trunk/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->w:Lcom/sec/chaton/trunk/b/b;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private n()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 512
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 514
    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "item_id"

    aput-object v2, v1, v5

    const-string v2, "=?"

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 515
    new-array v4, v6, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    aput-object v1, v4, v5

    .line 516
    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "registration_time"

    aput-object v2, v1, v5

    const-string v2, " ASC"

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 518
    sget-object v1, Lcom/sec/chaton/trunk/database/b;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private o()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 529
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "item_id"

    aput-object v3, v1, v5

    const-string v3, " = ?"

    aput-object v3, v1, v4

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 530
    new-array v4, v4, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    aput-object v1, v4, v5

    .line 533
    sget-object v1, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private p()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 540
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ba;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 622
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ba;->o()Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 549
    if-nez v1, :cond_2

    .line 602
    if-eqz v1, :cond_0

    .line 603
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 553
    :cond_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 555
    const-string v0, "sender_uid"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->i:Ljava/lang/String;

    .line 557
    const-string v0, "ME"

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 558
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0094

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->k:Ljava/lang/String;

    .line 562
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 563
    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/chaton/util/bt;->i(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->j:Landroid/graphics/Bitmap;

    .line 576
    :cond_3
    :goto_1
    const-string v0, "registration_time"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/chaton/trunk/ba;->l:J

    .line 577
    const-string v0, "down_url"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->m:Ljava/lang/String;

    .line 580
    const-string v0, "item_description"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->r:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 585
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/ba;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 586
    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->f:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/chaton/trunk/ba;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 587
    iget-object v3, p0, Lcom/sec/chaton/trunk/ba;->m:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 588
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v5, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->p:Ljava/lang/String;

    .line 589
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v0, v4

    const/4 v2, 0x1

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v4, v0, v2

    const/4 v2, 0x2

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->q:Ljava/lang/String;
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 598
    :goto_2
    :try_start_3
    const-string v0, "content_type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;Z)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->n:Lcom/sec/chaton/trunk/c/g;

    .line 599
    const-string v0, "total_comment_count"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/trunk/ba;->o:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 602
    :cond_4
    if-eqz v1, :cond_5

    .line 603
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 608
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->r:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/i;->b(Ljava/lang/String;)V

    .line 611
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->i:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/i;->a(Ljava/lang/String;)V

    .line 615
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->j:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/i;->a(Landroid/graphics/Bitmap;)V

    .line 617
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->k:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/i;->c(Ljava/lang/String;)V

    .line 618
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget-wide v1, p0, Lcom/sec/chaton/trunk/ba;->l:J

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/i;->a(J)V

    .line 619
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget v1, p0, Lcom/sec/chaton/trunk/ba;->o:I

    invoke-direct {p0}, Lcom/sec/chaton/trunk/ba;->n()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/i;->a(ILandroid/database/Cursor;)V

    .line 621
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ba;->q()V

    goto/16 :goto_0

    .line 566
    :cond_6
    :try_start_4
    const-string v0, "sender_name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->k:Ljava/lang/String;

    .line 570
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->i:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 571
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/util/bt;->i(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->j:Landroid/graphics/Bitmap;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 602
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_7

    .line 603
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 602
    :cond_7
    throw v0

    .line 590
    :catch_0
    move-exception v0

    .line 591
    :try_start_5
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_8

    .line 592
    sget-object v2, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 595
    :cond_8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->q:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2
.end method

.method private q()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 628
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ba;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 655
    :cond_0
    :goto_0
    return-void

    .line 632
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 633
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "Redraw content image: "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->q:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->n:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_5

    .line 637
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->q:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 638
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->n:Lcom/sec/chaton/trunk/c/g;

    invoke-interface {v0, v1, v3, v3}, Lcom/sec/chaton/trunk/i;->a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 643
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->n:Lcom/sec/chaton/trunk/c/g;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/trunk/ba;->p:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/chaton/trunk/ba;->q:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/trunk/i;->a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 646
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->n:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_0

    .line 647
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->q:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 648
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->n:Lcom/sec/chaton/trunk/c/g;

    invoke-interface {v0, v1, v3, v3}, Lcom/sec/chaton/trunk/i;->a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 653
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->n:Lcom/sec/chaton/trunk/c/g;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->q:Ljava/lang/String;

    invoke-interface {v0, v1, v3, v2}, Lcom/sec/chaton/trunk/i;->a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected f()V
    .locals 3

    .prologue
    .line 226
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->f()V

    .line 228
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 229
    const-string v0, "TrunkItemPresenter.onCreate()"

    sget-object v1, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/i;->a()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    .line 234
    new-instance v0, Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ba;->C:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/trunk/a/a;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->s:Lcom/sec/chaton/trunk/a/a;

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    new-instance v1, Lcom/sec/chaton/trunk/bc;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/bc;-><init>(Lcom/sec/chaton/trunk/ba;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/i;->a(Lcom/sec/chaton/trunk/l;)V

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    new-instance v1, Lcom/sec/chaton/trunk/bd;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/bd;-><init>(Lcom/sec/chaton/trunk/ba;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/i;->a(Lcom/sec/chaton/trunk/k;)V

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    new-instance v1, Lcom/sec/chaton/trunk/be;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/be;-><init>(Lcom/sec/chaton/trunk/ba;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/i;->c(Lcom/sec/chaton/trunk/k;)V

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    new-instance v1, Lcom/sec/chaton/trunk/bf;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/bf;-><init>(Lcom/sec/chaton/trunk/ba;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/i;->b(Lcom/sec/chaton/trunk/k;)V

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    new-instance v1, Lcom/sec/chaton/trunk/bg;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/bg;-><init>(Lcom/sec/chaton/trunk/ba;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/i;->a(Lcom/sec/chaton/trunk/j;)V

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    new-instance v1, Lcom/sec/chaton/trunk/bh;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/bh;-><init>(Lcom/sec/chaton/trunk/ba;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/i;->a(Lcom/sec/chaton/trunk/m;)V

    .line 278
    return-void
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 282
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->g()V

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 286
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 287
    const-string v0, "TrunkItemPresenter.onResume()"

    sget-object v1, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 292
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ba;->p()V

    .line 297
    :goto_0
    return-void

    .line 294
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->d:Lcom/sec/chaton/trunk/i;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/i;->b()V

    goto :goto_0
.end method

.method protected h()V
    .locals 2

    .prologue
    .line 301
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->h()V

    .line 303
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 304
    const-string v0, "TrunkItemPresenter.onPause()"

    sget-object v1, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 2

    .prologue
    .line 310
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->i()V

    .line 312
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 313
    const-string v0, "TrunkItemPresenter.onDestroy()"

    sget-object v1, Lcom/sec/chaton/trunk/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->u:Lcom/sec/chaton/trunk/a/a/e;

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->u:Lcom/sec/chaton/trunk/a/a/e;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/e;->c()V

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->x:Lcom/sec/chaton/trunk/a/a/b;

    if-eqz v0, :cond_2

    .line 321
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->x:Lcom/sec/chaton/trunk/a/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/b;->c()V

    .line 324
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->v:Lcom/sec/chaton/trunk/a/a/c;

    if-eqz v0, :cond_3

    .line 325
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->v:Lcom/sec/chaton/trunk/a/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/c;->c()V

    .line 328
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->y:Lcom/sec/chaton/trunk/a/a/i;

    if-eqz v0, :cond_4

    .line 329
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->y:Lcom/sec/chaton/trunk/a/a/i;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/i;->c()V

    .line 332
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->w:Lcom/sec/chaton/trunk/b/b;

    if-eqz v0, :cond_5

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/trunk/ba;->w:Lcom/sec/chaton/trunk/b/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/b/b;->cancel(Z)Z

    .line 336
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ba;->w:Lcom/sec/chaton/trunk/b/b;

    .line 337
    return-void
.end method
