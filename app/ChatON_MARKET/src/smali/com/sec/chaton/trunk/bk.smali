.class Lcom/sec/chaton/trunk/bk;
.super Ljava/lang/Object;
.source "TrunkItemView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkItemView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkItemView;)V
    .locals 0

    .prologue
    .line 583
    iput-object p1, p0, Lcom/sec/chaton/trunk/bk;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 586
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 588
    if-nez v0, :cond_1

    .line 589
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_0

    .line 590
    const-string v0, "ContentFragment.onItemLongClick cursor is null."

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkItemView;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v0, v1

    .line 613
    :goto_0
    return v0

    .line 596
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/trunk/bk;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkItemView;->y(Lcom/sec/chaton/trunk/TrunkItemView;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 597
    goto :goto_0

    .line 600
    :cond_2
    const-string v2, "writer_uid"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 601
    const-string v3, "comment_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 603
    const-string v3, "ME"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 604
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 605
    const-string v0, "[TRUNK] MyUid == Sender. Ignore it."

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkItemView;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move v0, v1

    .line 608
    goto :goto_0

    .line 612
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/trunk/bk;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1, v0}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Lcom/sec/chaton/trunk/TrunkItemView;Ljava/lang/String;)V

    .line 613
    const/4 v0, 0x1

    goto :goto_0
.end method
