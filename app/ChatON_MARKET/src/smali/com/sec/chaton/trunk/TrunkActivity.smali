.class public Lcom/sec/chaton/trunk/TrunkActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "TrunkActivity.java"

# interfaces
.implements Lcom/sec/chaton/trunk/co;


# instance fields
.field private a:Lcom/sec/chaton/trunk/TrunkView;

.field private b:Lcom/sec/chaton/trunk/q;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 35
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/sec/chaton/trunk/TrunkView;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/TrunkView;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkActivity;->a:Lcom/sec/chaton/trunk/TrunkView;

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkActivity;->a:Lcom/sec/chaton/trunk/TrunkView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkActivity;->b:Lcom/sec/chaton/trunk/q;

    .line 58
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkActivity;->a:Lcom/sec/chaton/trunk/TrunkView;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/trunk/c/g;IZ)V
    .locals 2

    .prologue
    .line 107
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 108
    const-class v1, Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 109
    const-string v1, "sessionId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    const-string v1, "inboxNo"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const-string v1, "itemId"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v1, "isvalid"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 113
    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkActivity;->startActivity(Landroid/content/Intent;)V

    .line 116
    return-void
.end method

.method public c()Lcom/sec/common/actionbar/a;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkActivity;->b:Lcom/sec/chaton/trunk/q;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkActivity;->b:Lcom/sec/chaton/trunk/q;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/q;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onBackPressed()V

    .line 77
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    if-eqz p1, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkActivity;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/q;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkActivity;->b:Lcom/sec/chaton/trunk/q;

    .line 51
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 65
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 67
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 83
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 90
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 86
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkActivity;->onBackPressed()V

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
