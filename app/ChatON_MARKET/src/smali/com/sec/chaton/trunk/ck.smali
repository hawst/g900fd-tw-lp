.class Lcom/sec/chaton/trunk/ck;
.super Ljava/lang/Object;
.source "TrunkView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkView;)V
    .locals 0

    .prologue
    .line 757
    iput-object p1, p0, Lcom/sec/chaton/trunk/ck;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 760
    sget-object v0, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/b;->ordinal()I

    move-result v0

    .line 761
    sget-object v1, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/a/b;->ordinal()I

    move-result v1

    .line 763
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v2

    .line 765
    const/4 v3, -0x3

    if-eq v3, v2, :cond_0

    const/4 v3, -0x2

    if-ne v3, v2, :cond_2

    .line 766
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ck;->a:Lcom/sec/chaton/trunk/TrunkView;

    sget-object v1, Lcom/sec/chaton/trunk/cp;->a:Lcom/sec/chaton/trunk/cp;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Lcom/sec/chaton/trunk/cp;)V

    .line 800
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 801
    :goto_1
    return-void

    .line 769
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/trunk/ck;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkView;->i(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/trunk/a/b;->ordinal()I

    move-result v2

    if-ne p2, v2, :cond_4

    .line 770
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 771
    const-string v0, "Current item == Selected item"

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkView;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    :cond_3
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_1

    .line 779
    :cond_4
    if-ne p2, v0, :cond_6

    .line 780
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_5

    .line 781
    const-string v1, "Which: %s, New: %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkView;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    const-string v0, "Current: %s, New: %s"

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ck;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkView;->i(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/a/b;

    move-result-object v2

    aput-object v2, v1, v5

    sget-object v2, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkView;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/ck;->a:Lcom/sec/chaton/trunk/TrunkView;

    sget-object v1, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    invoke-static {v0, v4, v4, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;ZZLcom/sec/chaton/trunk/a/b;)V

    goto :goto_0

    .line 786
    :cond_6
    if-ne p2, v1, :cond_8

    .line 787
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_7

    .line 788
    const-string v0, "Which: %s, New: %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkView;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    const-string v0, "Current: %s, New: %s"

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/trunk/ck;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkView;->i(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/a/b;

    move-result-object v2

    aput-object v2, v1, v5

    sget-object v2, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkView;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/trunk/ck;->a:Lcom/sec/chaton/trunk/TrunkView;

    sget-object v1, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    invoke-static {v0, v4, v4, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;ZZLcom/sec/chaton/trunk/a/b;)V

    goto/16 :goto_0

    .line 794
    :cond_8
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_1

    .line 795
    const-string v0, "Unknown ordering type. %s"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkView;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
