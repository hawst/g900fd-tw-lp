.class public Lcom/sec/chaton/trunk/TrunkCommentView;
.super Lcom/sec/chaton/trunk/ITrunkCommentView;
.source "TrunkCommentView.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Lcom/sec/chaton/trunk/aj;

.field private d:Landroid/view/inputmethod/InputMethodManager;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/ImageButton;

.field private g:Landroid/app/Dialog;

.field private h:Landroid/app/Dialog;

.field private i:Landroid/app/Dialog;

.field private j:Z

.field private k:Z

.field private l:Landroid/widget/Toast;

.field private m:Lcom/sec/chaton/trunk/y;

.field private n:Lcom/sec/chaton/trunk/e;

.field private o:Landroid/view/View;

.field private p:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/TrunkCommentView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ITrunkCommentView;-><init>()V

    .line 124
    new-instance v0, Lcom/sec/chaton/trunk/ab;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/ab;-><init>(Lcom/sec/chaton/trunk/TrunkCommentView;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->p:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->g:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkCommentView;Landroid/widget/ImageButton;)Landroid/widget/ImageButton;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->f:Landroid/widget/ImageButton;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/chaton/trunk/TrunkCommentView;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/sec/chaton/a/a/f;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 378
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkCommentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0139

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 380
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 381
    const/4 v2, -0x3

    if-eq v2, v1, :cond_0

    const/4 v2, -0x2

    if-ne v2, v1, :cond_2

    .line 382
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->l:Landroid/widget/Toast;

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 383
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->l:Landroid/widget/Toast;

    invoke-virtual {v0, v3}, Landroid/widget/Toast;->setDuration(I)V

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->l:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 445
    :cond_1
    :goto_0
    return-void

    .line 389
    :cond_2
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0x15d3d

    if-ne v1, v2, :cond_4

    .line 410
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->j:Z

    if-nez v0, :cond_1

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->b:Landroid/app/Activity;

    const v1, 0x7f0b0169

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->c:Lcom/sec/chaton/trunk/aj;

    if-eqz v0, :cond_3

    .line 415
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->c:Lcom/sec/chaton/trunk/aj;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/aj;->a()V

    .line 417
    :cond_3
    iput-boolean v3, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->j:Z

    goto :goto_0

    .line 421
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->b:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkCommentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/trunk/ah;

    invoke-direct {v2, p0}, Lcom/sec/chaton/trunk/ah;-><init>(Lcom/sec/chaton/trunk/TrunkCommentView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkCommentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/trunk/ag;

    invoke-direct {v2, p0}, Lcom/sec/chaton/trunk/ag;-><init>(Lcom/sec/chaton/trunk/TrunkCommentView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->h:Landroid/app/Dialog;

    .line 436
    iput-boolean v4, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->j:Z

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->h:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 441
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->h:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkCommentView;Lcom/sec/chaton/a/a/f;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkCommentView;->a(Lcom/sec/chaton/a/a/f;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkCommentView;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->j:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->g:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->g:Landroid/app/Dialog;

    .line 368
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->g:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->g:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 374
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkCommentView;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->k:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->b:Landroid/app/Activity;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 451
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->i:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01df

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0160

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/trunk/ai;

    invoke-direct {v2, p0}, Lcom/sec/chaton/trunk/ai;-><init>(Lcom/sec/chaton/trunk/TrunkCommentView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->i:Landroid/app/Dialog;

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->i:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->i:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->i:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 471
    :cond_1
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/TrunkCommentView;)Lcom/sec/chaton/trunk/aj;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->c:Lcom/sec/chaton/trunk/aj;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/TrunkCommentView;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkCommentView;->c()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->o:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->f:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/trunk/TrunkCommentView;)Lcom/sec/chaton/trunk/e;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->n:Lcom/sec/chaton/trunk/e;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->d:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/trunk/TrunkCommentView;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkCommentView;->b()V

    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/trunk/e;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->n:Lcom/sec/chaton/trunk/e;

    .line 185
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 189
    invoke-super {p0, p1}, Lcom/sec/chaton/trunk/ITrunkCommentView;->onAttach(Landroid/app/Activity;)V

    .line 191
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->b:Landroid/app/Activity;

    .line 194
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->b:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/trunk/aj;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->c:Lcom/sec/chaton/trunk/aj;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    return-void

    .line 195
    :catch_0
    move-exception v0

    .line 196
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "%s must implement ITrunkCommentViewListener."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 210
    invoke-super {p0, p1}, Lcom/sec/chaton/trunk/ITrunkCommentView;->onCreate(Landroid/os/Bundle;)V

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->b:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->d:Landroid/view/inputmethod/InputMethodManager;

    .line 214
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->l:Landroid/widget/Toast;

    .line 215
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const v3, 0x7f07046b

    const/4 v2, 0x0

    .line 219
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/trunk/ITrunkCommentView;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 226
    const v0, 0x7f03010c

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->o:Landroid/view/View;

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->o:Landroid/view/View;

    const v1, 0x7f07046a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->e:Landroid/widget/EditText;

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/trunk/ac;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/ac;-><init>(Lcom/sec/chaton/trunk/TrunkCommentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/trunk/ad;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/ad;-><init>(Lcom/sec/chaton/trunk/TrunkCommentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/chaton/trunk/ae;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/ae;-><init>(Lcom/sec/chaton/trunk/TrunkCommentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->f:Landroid/widget/ImageButton;

    .line 292
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->f:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/trunk/af;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/af;-><init>(Lcom/sec/chaton/trunk/TrunkCommentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 342
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkCommentView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 343
    const-string v1, "sessionId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 344
    const-string v2, "itemId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 345
    const-string v3, "isvalid"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->k:Z

    .line 347
    new-instance v0, Lcom/sec/chaton/trunk/y;

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->p:Landroid/os/Handler;

    invoke-direct {v0, p0, v3, v1, v2}, Lcom/sec/chaton/trunk/y;-><init>(Lcom/sec/chaton/trunk/ITrunkCommentView;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->m:Lcom/sec/chaton/trunk/y;

    .line 348
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->m:Lcom/sec/chaton/trunk/y;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/y;->a()V

    .line 350
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->k:Z

    if-nez v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->o:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->o:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 359
    invoke-super {p0}, Lcom/sec/chaton/trunk/ITrunkCommentView;->onDestroyView()V

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->m:Lcom/sec/chaton/trunk/y;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/y;->d()V

    .line 362
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 202
    invoke-super {p0}, Lcom/sec/chaton/trunk/ITrunkCommentView;->onDetach()V

    .line 204
    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->b:Landroid/app/Activity;

    .line 205
    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkCommentView;->c:Lcom/sec/chaton/trunk/aj;

    .line 206
    return-void
.end method
