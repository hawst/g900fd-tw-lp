.class public Lcom/sec/chaton/trunk/TrunkItemView;
.super Landroid/support/v4/app/Fragment;
.source "TrunkItemView.java"

# interfaces
.implements Lcom/sec/chaton/trunk/ak;
.implements Lcom/sec/chaton/trunk/c/e;
.implements Lcom/sec/chaton/trunk/i;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private A:Landroid/app/Dialog;

.field private B:Landroid/app/Dialog;

.field private C:Landroid/app/Dialog;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Lcom/sec/chaton/trunk/c/g;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:I

.field private K:I

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Ljava/lang/String;

.field private P:Z

.field private Q:Z

.field private R:Lcom/sec/chaton/trunk/c/b;

.field private S:Lcom/sec/chaton/trunk/ba;

.field private T:Z

.field private U:Lcom/sec/chaton/trunk/l;

.field private V:Lcom/sec/chaton/trunk/k;

.field private W:Lcom/sec/chaton/trunk/k;

.field private X:Lcom/sec/chaton/trunk/k;

.field private Y:Lcom/sec/chaton/trunk/j;

.field private Z:Lcom/sec/chaton/trunk/m;

.field private aa:Lcom/sec/common/f/c;

.field private ab:Landroid/net/Uri;

.field private ac:Landroid/widget/TextView;

.field private ad:Ljava/lang/String;

.field private ae:Ljava/lang/String;

.field private af:Ljava/lang/String;

.field private ag:Z

.field private ah:Ljava/lang/String;

.field private ai:Z

.field private aj:Landroid/widget/LinearLayout;

.field private ak:Z

.field private al:Landroid/os/Handler;

.field private am:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private b:Landroid/view/inputmethod/InputMethodManager;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/view/Menu;

.field private e:Landroid/app/Activity;

.field private f:Lcom/sec/chaton/trunk/br;

.field private g:Lcom/sec/chaton/trunk/b;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/ProgressBar;

.field private l:Landroid/widget/ProgressBar;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/FrameLayout;

.field private p:Landroid/widget/LinearLayout;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/Toast;

.field private t:Z

.field private u:Z

.field private v:Landroid/view/View;

.field private w:Landroid/view/View;

.field private x:Landroid/view/View;

.field private y:Landroid/view/View;

.field private z:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    const-class v0, Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/TrunkItemView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 303
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ak:Z

    .line 308
    new-instance v0, Lcom/sec/chaton/trunk/bi;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/bi;-><init>(Lcom/sec/chaton/trunk/TrunkItemView;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->al:Landroid/os/Handler;

    .line 583
    new-instance v0, Lcom/sec/chaton/trunk/bk;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/bk;-><init>(Lcom/sec/chaton/trunk/TrunkItemView;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->am:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->b:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic B(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic C(Lcom/sec/chaton/trunk/TrunkItemView;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->j()V

    return-void
.end method

.method static synthetic D(Lcom/sec/chaton/trunk/TrunkItemView;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->i()V

    return-void
.end method

.method static synthetic E(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ad:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic F(Lcom/sec/chaton/trunk/TrunkItemView;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->n()V

    return-void
.end method

.method static synthetic G(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/m;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->Z:Lcom/sec/chaton/trunk/m;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkItemView;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->c:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->F:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkItemView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->F:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/bs;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkItemView;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/trunk/bs;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1485
    sget-object v0, Lcom/sec/chaton/trunk/bq;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/trunk/bs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1499
    :goto_0
    return-void

    .line 1487
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->s:Landroid/widget/Toast;

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 1488
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->s:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 1489
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->s:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1493
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->s:Landroid/widget/Toast;

    const v1, 0x7f0b002a

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 1494
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->s:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 1495
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->s:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1485
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1454
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->R:Lcom/sec/chaton/trunk/c/b;

    const-string v1, "comment"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/c/b;->a(Ljava/lang/String;)Z

    .line 1455
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1456
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1457
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1458
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1467
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->T:Z

    .line 1469
    return-void

    .line 1461
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->w:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1463
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1464
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->L:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkItemView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Ljava/lang/String;)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 1236
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->N:Z

    if-eqz v0, :cond_1

    .line 1245
    :cond_0
    :goto_0
    return-void

    .line 1240
    :cond_1
    if-eqz p1, :cond_0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->Q:Z

    return p1
.end method

.method private c(Z)V
    .locals 1

    .prologue
    .line 1248
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->t:Z

    .line 1249
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1257
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/TrunkItemView;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->Q:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ak:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/j;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->Y:Lcom/sec/chaton/trunk/j;

    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1521
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0094

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0d0003

    new-instance v2, Lcom/sec/chaton/trunk/bo;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/trunk/bo;-><init>(Lcom/sec/chaton/trunk/TrunkItemView;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->B:Landroid/app/Dialog;

    .line 1536
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->B:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1537
    return-void
.end method

.method private d(Z)V
    .locals 1

    .prologue
    .line 1260
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->u:Z

    .line 1261
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1271
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->P:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/TrunkItemView;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->T:Z

    return v0
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ag:Z

    return p1
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/TrunkItemView;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->P:Z

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->N:Z

    return p1
.end method

.method static synthetic g(Lcom/sec/chaton/trunk/TrunkItemView;)I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->J:I

    return v0
.end method

.method static synthetic g(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->M:Z

    return p1
.end method

.method static synthetic h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/chaton/trunk/TrunkItemView;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->O:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/trunk/TrunkItemView;Z)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Z)V

    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 1387
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->n()V

    .line 1388
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->b:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    .line 1389
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    const v1, 0x7f07046a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->c:Landroid/widget/EditText;

    .line 1390
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->c:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 1391
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1394
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->V:Lcom/sec/chaton/trunk/k;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/k;->a()V

    .line 1395
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/k;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->W:Lcom/sec/chaton/trunk/k;

    return-object v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 1398
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->N:Z

    .line 1401
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->k()V

    .line 1403
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->X:Lcom/sec/chaton/trunk/k;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/k;->a()V

    .line 1404
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1433
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->R:Lcom/sec/chaton/trunk/c/b;

    const-string v1, "content"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/c/b;->a(Ljava/lang/String;)Z

    .line 1435
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->p:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1436
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1438
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/trunk/TrunkItemView;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->o()V

    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1441
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->R:Lcom/sec/chaton/trunk/c/b;

    const-string v1, "content"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/c/b;->b(Ljava/lang/String;)Z

    .line 1443
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->R:Lcom/sec/chaton/trunk/c/b;

    const-string v1, "content"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/c/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1444
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->p:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1445
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_0

    .line 1446
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    const v1, 0x7f020138

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1447
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1451
    :cond_0
    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/trunk/TrunkItemView;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->m()V

    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/trunk/TrunkItemView;)I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->K:I

    return v0
.end method

.method private m()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1472
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->R:Lcom/sec/chaton/trunk/c/b;

    const-string v1, "comment"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/c/b;->b(Ljava/lang/String;)Z

    .line 1474
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->R:Lcom/sec/chaton/trunk/c/b;

    const-string v1, "comment"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/c/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1475
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->w:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1477
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1478
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1481
    :cond_0
    iput-boolean v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->T:Z

    .line 1482
    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->A:Landroid/app/Dialog;

    return-object v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->A:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 1507
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->A:Landroid/app/Dialog;

    .line 1509
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->A:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1512
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->A:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 1513
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->A:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1515
    :cond_1
    return-void
.end method

.method static synthetic o(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/br;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->f:Lcom/sec/chaton/trunk/br;

    return-object v0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 1543
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->C:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 1544
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0160

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/trunk/bp;

    invoke-direct {v2, p0}, Lcom/sec/chaton/trunk/bp;-><init>(Lcom/sec/chaton/trunk/TrunkItemView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->C:Landroid/app/Dialog;

    .line 1560
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->C:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1564
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->C:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 1565
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->C:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1567
    :cond_1
    return-void
.end method

.method private p()V
    .locals 4

    .prologue
    .line 1580
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1581
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 1583
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 1584
    if-nez v0, :cond_1

    .line 1610
    :cond_0
    :goto_0
    return-void

    .line 1589
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1591
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1593
    if-eqz v2, :cond_0

    .line 1603
    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1604
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1605
    const/high16 v1, 0x435c0000    # 220.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1607
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1608
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0
.end method

.method static synthetic p(Lcom/sec/chaton/trunk/TrunkItemView;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ak:Z

    return v0
.end method

.method static synthetic q(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->I:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->d:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/c/g;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    return-object v0
.end method

.method static synthetic v(Lcom/sec/chaton/trunk/TrunkItemView;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->l()V

    return-void
.end method

.method static synthetic w(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic x(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic y(Lcom/sec/chaton/trunk/TrunkItemView;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ag:Z

    return v0
.end method

.method static synthetic z(Lcom/sec/chaton/trunk/TrunkItemView;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->N:Z

    return v0
.end method


# virtual methods
.method public a()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    return-object v0
.end method

.method public a(ILandroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 1275
    iput p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->J:I

    .line 1279
    if-eqz p2, :cond_8

    .line 1280
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 1283
    :goto_0
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 1284
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "setCommentList(). mTotalCommentCount: "

    aput-object v3, v2, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, ", mViewCommentCount: "

    aput-object v3, v2, v6

    const/4 v3, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/TrunkItemView;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    :cond_0
    iget v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->J:I

    if-eq v2, v5, :cond_1

    iget v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->J:I

    if-nez v2, :cond_5

    .line 1290
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->r:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/chaton/trunk/TrunkItemView;->J:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1291
    iput-boolean v5, p0, Lcom/sec/chaton/trunk/TrunkItemView;->P:Z

    .line 1306
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->L:Z

    if-eqz v0, :cond_7

    .line 1308
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->O:Ljava/lang/String;

    .line 1326
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->g:Lcom/sec/chaton/trunk/b;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/trunk/b;->changeCursor(Landroid/database/Cursor;)V

    .line 1327
    return-void

    .line 1293
    :cond_5
    iget v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->J:I

    if-le v2, v5, :cond_2

    .line 1294
    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->r:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/chaton/trunk/TrunkItemView;->J:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1296
    iget v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->J:I

    if-lt v0, v2, :cond_6

    .line 1297
    iput-boolean v5, p0, Lcom/sec/chaton/trunk/TrunkItemView;->P:Z

    goto :goto_1

    .line 1299
    :cond_6
    iput-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->P:Z

    goto :goto_1

    .line 1314
    :cond_7
    if-eqz p2, :cond_4

    invoke-interface {p2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1315
    const-string v0, "comment_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->O:Ljava/lang/String;

    .line 1319
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 1320
    new-array v0, v6, [Ljava/lang/Object;

    const-string v2, "Last Comment Id: "

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->O:Ljava/lang/String;

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/TrunkItemView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(J)V
    .locals 7

    .prologue
    .line 1122
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1123
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1124
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1125
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyy-MM-dd"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1126
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy"

    invoke-direct {v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1128
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1129
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->j:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1136
    :goto_0
    return-void

    .line 1130
    :cond_0
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1131
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->j:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1133
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->j:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1052
    if-eqz p1, :cond_0

    .line 1053
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1057
    :goto_0
    return-void

    .line 1055
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->h:Landroid/widget/ImageView;

    const v1, 0x7f0201bb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1141
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->M:Z

    if-eqz v0, :cond_1

    .line 1233
    :cond_0
    :goto_0
    return-void

    .line 1146
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->N:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1150
    :cond_2
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    .line 1151
    iput-object p3, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    .line 1153
    invoke-direct {p0, v7}, Lcom/sec/chaton/trunk/TrunkItemView;->c(Z)V

    .line 1156
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1157
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    goto :goto_0

    .line 1159
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_e

    .line 1160
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    .line 1162
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1163
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 1166
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    if-nez v0, :cond_a

    .line 1167
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1168
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_8

    .line 1169
    new-array v0, v8, [Ljava/lang/Object;

    const-string v1, "For thumbnail image exists, show it. "

    aput-object v1, v0, v7

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/TrunkItemView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 1173
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/ad;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1179
    :cond_9
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->p()V

    .line 1181
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->j()V

    goto/16 :goto_0

    .line 1183
    :cond_a
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkItemView;->c(Z)V

    .line 1184
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    .line 1186
    if-eqz v0, :cond_b

    .line 1187
    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    .line 1190
    :cond_b
    new-instance v1, Lcom/sec/chaton/trunk/al;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/trunk/al;-><init>(Landroid/net/Uri;Lcom/sec/chaton/trunk/c/g;Ljava/lang/Boolean;Landroid/widget/ImageView;)V

    .line 1192
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_c

    .line 1193
    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "Is AMS image: "

    aput-object v3, v2, v7

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/TrunkItemView;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    :cond_c
    if-eqz v0, :cond_d

    .line 1197
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Z)V

    .line 1198
    invoke-direct {p0, v7}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Z)V

    .line 1205
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 1202
    :cond_d
    invoke-direct {p0, v7}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Z)V

    .line 1203
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Z)V

    goto :goto_1

    .line 1209
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_0

    .line 1211
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    .line 1215
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    if-nez v0, :cond_f

    .line 1217
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    const v1, 0x7f020449

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1218
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    const v1, 0x7f020138

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1219
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1221
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1223
    :cond_f
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkItemView;->c(Z)V

    .line 1224
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Z)V

    .line 1226
    new-instance v0, Lcom/sec/chaton/trunk/al;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/trunk/al;-><init>(Landroid/net/Uri;Lcom/sec/chaton/trunk/c/g;Ljava/lang/Boolean;Landroid/widget/ImageView;)V

    .line 1228
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0
.end method

.method public a(Lcom/sec/chaton/trunk/j;)V
    .locals 0

    .prologue
    .line 1364
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->Y:Lcom/sec/chaton/trunk/j;

    .line 1365
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/k;)V
    .locals 0

    .prologue
    .line 1349
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->V:Lcom/sec/chaton/trunk/k;

    .line 1350
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/l;)V
    .locals 0

    .prologue
    .line 1344
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->U:Lcom/sec/chaton/trunk/l;

    .line 1345
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/m;)V
    .locals 0

    .prologue
    .line 1369
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->Z:Lcom/sec/chaton/trunk/m;

    .line 1370
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1039
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->I:Ljava/lang/String;

    .line 1041
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1048
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 1374
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_0

    .line 1375
    const-string v0, "Storage state is changed. finish activity."

    sget-object v1, Lcom/sec/chaton/trunk/TrunkItemView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1378
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->f:Lcom/sec/chaton/trunk/br;

    if-eqz v0, :cond_1

    .line 1379
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->f:Lcom/sec/chaton/trunk/br;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ak:Z

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/br;->a(ZZ)V

    .line 1381
    :cond_1
    return-void
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1648
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->M:Z

    if-eqz v0, :cond_1

    .line 1734
    :cond_0
    :goto_0
    return-void

    .line 1653
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->N:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1657
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ah:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    .line 1659
    invoke-direct {p0, v7}, Lcom/sec/chaton/trunk/TrunkItemView;->c(Z)V

    .line 1662
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1663
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    goto :goto_0

    .line 1665
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1666
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    .line 1668
    :cond_5
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ai:Z

    if-nez v0, :cond_a

    .line 1669
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    .line 1670
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    .line 1675
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    if-nez v0, :cond_6

    .line 1677
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    const v1, 0x7f020448

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1678
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    goto :goto_0

    .line 1681
    :cond_6
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkItemView;->c(Z)V

    .line 1682
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    .line 1684
    if-nez v0, :cond_8

    .line 1685
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 1691
    :goto_1
    new-instance v1, Lcom/sec/chaton/trunk/al;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/trunk/al;-><init>(Landroid/net/Uri;Lcom/sec/chaton/trunk/c/g;Ljava/lang/Boolean;Landroid/widget/ImageView;)V

    .line 1693
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_7

    .line 1694
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "Is AMS image: "

    aput-object v3, v2, v7

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/TrunkItemView;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1697
    :cond_7
    if-eqz v0, :cond_9

    .line 1698
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Z)V

    .line 1699
    invoke-direct {p0, v7}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Z)V

    .line 1705
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 1687
    :cond_8
    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    goto :goto_1

    .line 1702
    :cond_9
    invoke-direct {p0, v7}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Z)V

    .line 1703
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Z)V

    goto :goto_2

    .line 1711
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    .line 1712
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    .line 1717
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    if-nez v0, :cond_b

    .line 1718
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    const v1, 0x7f020449

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1719
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    const v1, 0x7f020138

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1720
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1722
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1724
    :cond_b
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkItemView;->c(Z)V

    .line 1725
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Z)V

    .line 1727
    new-instance v0, Lcom/sec/chaton/trunk/al;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ab:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/trunk/al;-><init>(Landroid/net/Uri;Lcom/sec/chaton/trunk/c/g;Ljava/lang/Boolean;Landroid/widget/ImageView;)V

    .line 1729
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0
.end method

.method public b(Lcom/sec/chaton/trunk/k;)V
    .locals 0

    .prologue
    .line 1354
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->W:Lcom/sec/chaton/trunk/k;

    .line 1355
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1629
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ad:Ljava/lang/String;

    .line 1631
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ad:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1632
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    float-to-int v1, v1

    invoke-static {v0, p1, v1}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1633
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ac:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1634
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ac:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1637
    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 1739
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->f:Lcom/sec/chaton/trunk/br;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ak:Z

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/br;->a(ZZ)V

    .line 1740
    return-void
.end method

.method public c(Lcom/sec/chaton/trunk/k;)V
    .locals 0

    .prologue
    .line 1359
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->X:Lcom/sec/chaton/trunk/k;

    .line 1360
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1107
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1108
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTranscriptMode(I)V

    .line 623
    :cond_0
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 956
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->f:Lcom/sec/chaton/trunk/br;

    if-eqz v0, :cond_0

    .line 957
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->f:Lcom/sec/chaton/trunk/br;

    iget-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ak:Z

    invoke-interface {v0, v2, v1}, Lcom/sec/chaton/trunk/br;->a(ZZ)V

    .line 959
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->b:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_1

    .line 960
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    const v1, 0x7f07046a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->c:Landroid/widget/EditText;

    .line 961
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->c:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 962
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 965
    :cond_1
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 1062
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    new-instance v1, Lcom/sec/chaton/trunk/bn;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/bn;-><init>(Lcom/sec/chaton/trunk/TrunkItemView;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 1070
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 1103
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1613
    iput-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->P:Z

    .line 1616
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->U:Lcom/sec/chaton/trunk/l;

    if-eqz v0, :cond_0

    .line 1617
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Ljava/lang/Boolean;)V

    .line 1619
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->U:Lcom/sec/chaton/trunk/l;

    const/16 v1, 0x64

    const/16 v2, 0xef

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/l;->a(II)V

    .line 1621
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 629
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 631
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    .line 634
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/trunk/br;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->f:Lcom/sec/chaton/trunk/br;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 638
    return-void

    .line 635
    :catch_0
    move-exception v0

    .line 636
    new-instance v0, Ljava/lang/ClassCastException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, " must implement ITrunkItemViewListener."

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 1571
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1573
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->p()V

    .line 1574
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 650
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 653
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 654
    const-string v1, "sessionId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->D:Ljava/lang/String;

    .line 655
    const-string v1, "inboxNo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->E:Ljava/lang/String;

    .line 656
    const-string v1, "itemId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->F:Ljava/lang/String;

    .line 659
    const-string v1, "ownerUid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ae:Ljava/lang/String;

    .line 660
    const-string v1, "fileName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->af:Ljava/lang/String;

    .line 663
    const-string v1, "totalcomment"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->J:I

    .line 664
    const-string v1, "isvalid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ag:Z

    .line 667
    const-string v1, "mediaUri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ah:Ljava/lang/String;

    .line 668
    const-string v1, "isVideo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ai:Z

    .line 671
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->s:Landroid/widget/Toast;

    .line 673
    new-instance v0, Lcom/sec/chaton/trunk/c/b;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/c/b;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->R:Lcom/sec/chaton/trunk/c/b;

    .line 675
    new-instance v0, Lcom/sec/chaton/trunk/b;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    invoke-direct {v0, v1, v3, v2, p0}, Lcom/sec/chaton/trunk/b;-><init>(Landroid/content/Context;Landroid/database/Cursor;ILcom/sec/chaton/trunk/TrunkItemView;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->g:Lcom/sec/chaton/trunk/b;

    .line 677
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    .line 678
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 921
    const v0, 0x7f0f0037

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 923
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->d:Landroid/view/Menu;

    .line 925
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 926
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/16 v7, 0x8

    const/high16 v6, 0x41880000    # 17.0f

    const/4 v5, 0x0

    const/4 v4, -0x2

    const/4 v9, 0x0

    .line 682
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 684
    const v0, 0x7f030115

    invoke-virtual {p1, v0, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 686
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->b:Landroid/view/inputmethod/InputMethodManager;

    .line 693
    const v0, 0x7f070491

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    .line 695
    const v0, 0x7f070492

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aj:Landroid/widget/LinearLayout;

    .line 700
    const v0, 0x7f03007e

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    invoke-virtual {p1, v0, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->v:Landroid/view/View;

    .line 701
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->v:Landroid/view/View;

    const v1, 0x7f0702e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->m:Landroid/widget/ImageView;

    .line 702
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->v:Landroid/view/View;

    const v1, 0x7f0702e2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->o:Landroid/widget/FrameLayout;

    .line 704
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    .line 705
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 706
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->p:Landroid/widget/LinearLayout;

    .line 707
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->p:Landroid/widget/LinearLayout;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 709
    new-instance v0, Landroid/widget/ProgressBar;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    const v3, 0x7f0c0036

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->k:Landroid/widget/ProgressBar;

    .line 710
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->k:Landroid/widget/ProgressBar;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 712
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->q:Landroid/widget/LinearLayout;

    .line 713
    new-instance v0, Landroid/widget/ProgressBar;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    const v3, 0x7f0c0037

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->l:Landroid/widget/ProgressBar;

    .line 714
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->l:Landroid/widget/ProgressBar;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-static {v6}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v6}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 716
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->q:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->l:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 718
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 719
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 720
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->q:Landroid/widget/LinearLayout;

    const/high16 v1, 0x40e00000    # 7.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v9, v1, v9, v9}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 723
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->p:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 725
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->v:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 728
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 729
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 730
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 731
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->o:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 732
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->o:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 733
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 734
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 742
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->v:Landroid/view/View;

    const v1, 0x7f0702e3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ac:Landroid/widget/TextView;

    .line 743
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->v:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->h:Landroid/widget/ImageView;

    .line 744
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->v:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->i:Landroid/widget/TextView;

    .line 745
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->v:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->j:Landroid/widget/TextView;

    .line 747
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ac:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 749
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->o:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/sec/chaton/trunk/bl;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/bl;-><init>(Lcom/sec/chaton/trunk/TrunkItemView;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 810
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->v:Landroid/view/View;

    invoke-virtual {v0, v1, v5, v9}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 813
    const v0, 0x7f030090

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    invoke-virtual {p1, v0, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->w:Landroid/view/View;

    .line 814
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->w:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->r:Landroid/widget/TextView;

    .line 817
    const v0, 0x7f03010f

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    invoke-virtual {p1, v0, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->x:Landroid/view/View;

    .line 818
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->x:Landroid/view/View;

    invoke-virtual {v0, v1, v5, v9}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 841
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->w:Landroid/view/View;

    invoke-virtual {v0, v1, v5, v9}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 844
    const v0, 0x7f030111

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    invoke-virtual {p1, v0, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->y:Landroid/view/View;

    .line 845
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->y:Landroid/view/View;

    invoke-virtual {v0, v1, v5, v9}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 847
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->am:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 848
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->z:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->g:Lcom/sec/chaton/trunk/b;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 853
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 855
    invoke-direct {p0, v9}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Z)V

    .line 861
    new-instance v0, Lcom/sec/chaton/trunk/ba;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->al:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkItemView;->E:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkItemView;->D:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/trunk/TrunkItemView;->F:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ae:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/trunk/TrunkItemView;->af:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/trunk/ba;-><init>(Lcom/sec/chaton/trunk/i;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->S:Lcom/sec/chaton/trunk/ba;

    .line 862
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->S:Lcom/sec/chaton/trunk/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ba;->a()V

    .line 865
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Ljava/lang/Boolean;)V

    .line 867
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->U:Lcom/sec/chaton/trunk/l;

    const/16 v1, 0x64

    const/16 v2, 0xef

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/l;->a(II)V

    .line 869
    return-object v8
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 913
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 915
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->R:Lcom/sec/chaton/trunk/c/b;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/c/b;->a()V

    .line 916
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 902
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 904
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->S:Lcom/sec/chaton/trunk/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ba;->d()V

    .line 906
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    if-eqz v0, :cond_0

    .line 907
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->aa:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 909
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 642
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 644
    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->e:Landroid/app/Activity;

    .line 645
    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->f:Lcom/sec/chaton/trunk/br;

    .line 646
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, -0x2

    const/4 v3, -0x3

    const/4 v1, 0x0

    .line 969
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 971
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    .line 1024
    :cond_1
    :goto_1
    return v0

    .line 974
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0037

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/trunk/bm;

    invoke-direct {v3, p0}, Lcom/sec/chaton/trunk/bm;-><init>(Lcom/sec/chaton/trunk/TrunkItemView;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_1

    .line 991
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 995
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->f:Lcom/sec/chaton/trunk/br;

    if-eqz v0, :cond_0

    .line 996
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 997
    if-eq v3, v0, :cond_2

    if-ne v4, v0, :cond_3

    .line 998
    :cond_2
    sget-object v0, Lcom/sec/chaton/trunk/bs;->a:Lcom/sec/chaton/trunk/bs;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/bs;)V

    goto :goto_0

    .line 1000
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "TrunkShareCheckPopup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->f()V

    goto/16 :goto_0

    .line 1003
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->f:Lcom/sec/chaton/trunk/br;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ad:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v4}, Lcom/sec/chaton/trunk/br;->a(Lcom/sec/chaton/trunk/c/g;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1009
    :pswitch_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1010
    if-eq v3, v0, :cond_5

    if-ne v4, v0, :cond_6

    .line 1011
    :cond_5
    sget-object v0, Lcom/sec/chaton/trunk/bs;->a:Lcom/sec/chaton/trunk/bs;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/bs;)V

    goto/16 :goto_0

    .line 1013
    :cond_6
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Ljava/lang/Boolean;)V

    .line 1014
    iput-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->P:Z

    .line 1015
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->U:Lcom/sec/chaton/trunk/l;

    const/16 v2, 0x64

    const/16 v3, 0xef

    invoke-interface {v0, v2, v3}, Lcom/sec/chaton/trunk/l;->a(II)V

    goto/16 :goto_0

    .line 1020
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkItemView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/TrunkDetailActivity;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkItemView;->H:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 971
    :pswitch_data_0
    .packed-switch 0x7f0705bb
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 888
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 890
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->S:Lcom/sec/chaton/trunk/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ba;->c()V

    .line 891
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6

    .prologue
    const v5, 0x7f0705bc

    const/4 v4, 0x1

    const/4 v3, 0x0

    const v2, 0x7f0705bb

    .line 931
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 933
    const-string v0, "ME"

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkItemView;->I:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->ag:Z

    if-nez v0, :cond_1

    .line 934
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->d:Landroid/view/Menu;

    const v1, 0x7f0705bd

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 937
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->t:Z

    if-eqz v0, :cond_2

    .line 938
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->d:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 943
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->G:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-eq v0, v1, :cond_4

    .line 944
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->u:Z

    if-eqz v0, :cond_3

    .line 945
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->d:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 953
    :goto_1
    return-void

    .line 940
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->d:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 947
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->d:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 950
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->d:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 881
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 883
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkItemView;->S:Lcom/sec/chaton/trunk/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ba;->b()V

    .line 884
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 874
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 876
    invoke-static {p0}, Lcom/sec/chaton/trunk/c/c;->a(Lcom/sec/chaton/trunk/c/e;)V

    .line 877
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 895
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 897
    invoke-static {p0}, Lcom/sec/chaton/trunk/c/c;->b(Lcom/sec/chaton/trunk/c/e;)V

    .line 898
    return-void
.end method
