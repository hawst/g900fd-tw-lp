.class Lcom/sec/chaton/trunk/z;
.super Landroid/os/Handler;
.source "TrunkCommentPresenter.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/y;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/y;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/chaton/trunk/z;->a:Lcom/sec/chaton/trunk/y;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 47
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 49
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 51
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/trunk/z;->a:Lcom/sec/chaton/trunk/y;

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/y;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 56
    const-string v1, "[TRUNK] Received METHOD_ADD_COMMENT"

    invoke-static {}, Lcom/sec/chaton/trunk/y;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/trunk/z;->a:Lcom/sec/chaton/trunk/y;

    invoke-static {v1}, Lcom/sec/chaton/trunk/y;->a(Lcom/sec/chaton/trunk/y;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 60
    iget-object v1, p0, Lcom/sec/chaton/trunk/z;->a:Lcom/sec/chaton/trunk/y;

    invoke-static {v1}, Lcom/sec/chaton/trunk/y;->a(Lcom/sec/chaton/trunk/y;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 63
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_0

    .line 64
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x15d43

    if-ne v0, v1, :cond_0

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/z;->a:Lcom/sec/chaton/trunk/y;

    invoke-static {v0}, Lcom/sec/chaton/trunk/y;->b(Lcom/sec/chaton/trunk/y;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/z;->a:Lcom/sec/chaton/trunk/y;

    invoke-static {v1}, Lcom/sec/chaton/trunk/y;->c(Lcom/sec/chaton/trunk/y;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/trunk/database/a/a;->b(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 69
    invoke-static {}, Lcom/sec/chaton/trunk/y;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x388
        :pswitch_0
    .end packed-switch
.end method
