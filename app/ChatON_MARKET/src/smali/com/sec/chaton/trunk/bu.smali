.class Lcom/sec/chaton/trunk/bu;
.super Ljava/lang/Object;
.source "TrunkPageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkPageActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkPageActivity;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/chaton/trunk/bu;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 287
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/bu;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    iget-object v0, v0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/bu;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Lcom/sec/chaton/trunk/TrunkPageActivity;)I

    move-result v1

    if-le v0, v1, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/trunk/bu;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    iget-object v0, v0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/trunk/bu;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Lcom/sec/chaton/trunk/TrunkPageActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/bz;

    .line 297
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/trunk/bu;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->b(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/trunk/TrunkDetailActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 298
    const-string v2, "sessionId"

    iget-object v3, p0, Lcom/sec/chaton/trunk/bu;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->c(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const-string v2, "inboxNo"

    iget-object v3, p0, Lcom/sec/chaton/trunk/bu;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->d(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    const-string v2, "itemId"

    iget-object v3, v0, Lcom/sec/chaton/trunk/bz;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 301
    const-string v2, "downloadUrl"

    iget-object v3, v0, Lcom/sec/chaton/trunk/bz;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 302
    const-string v2, "contentType"

    iget-object v3, v0, Lcom/sec/chaton/trunk/bz;->i:Lcom/sec/chaton/trunk/c/g;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 303
    const-string v2, "totalcomment"

    iget v0, v0, Lcom/sec/chaton/trunk/bz;->j:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 304
    const-string v0, "isvalid"

    iget-object v2, p0, Lcom/sec/chaton/trunk/bu;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    iget-boolean v2, v2, Lcom/sec/chaton/trunk/TrunkPageActivity;->d:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/trunk/bu;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
