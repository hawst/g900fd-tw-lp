.class Lcom/sec/chaton/trunk/bv;
.super Ljava/lang/Object;
.source "TrunkPageActivity.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkPageActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkPageActivity;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 425
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 414
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 19

    .prologue
    .line 352
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_4

    .line 353
    if-eqz p3, :cond_3

    .line 354
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->b(Lcom/sec/chaton/trunk/TrunkPageActivity;I)I

    .line 355
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Lcom/sec/chaton/trunk/TrunkPageActivity;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 357
    const/4 v2, 0x0

    .line 358
    const/4 v1, 0x0

    .line 359
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    iget-object v3, v3, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    move/from16 v18, v2

    .line 361
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 362
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "sender_uid"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 363
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "sender_name"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 364
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "registration_time"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 365
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "item_id"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 366
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "down_url"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 368
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "isams"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 369
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "content_type"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;Z)Lcom/sec/chaton/trunk/c/g;

    move-result-object v12

    .line 371
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "total_comment_count"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 373
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "item_description"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "unread_comment_count"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 377
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->f(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    move/from16 v17, v18

    .line 381
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->c(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 382
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "message_session_id"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Lcom/sec/chaton/trunk/TrunkPageActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 385
    :cond_0
    new-instance v1, Lcom/sec/chaton/trunk/bz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->c(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v4}, Lcom/sec/chaton/trunk/TrunkPageActivity;->d(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    iget-boolean v14, v14, Lcom/sec/chaton/trunk/TrunkPageActivity;->d:Z

    invoke-direct/range {v1 .. v16}, Lcom/sec/chaton/trunk/bz;-><init>(Lcom/sec/chaton/trunk/TrunkPageActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/trunk/c/g;IZLjava/lang/String;I)V

    .line 387
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_1

    .line 388
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add item : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkPageActivity;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    iget-object v2, v2, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    add-int/lit8 v1, v18, 0x1

    move/from16 v18, v1

    move/from16 v1, v17

    .line 395
    goto/16 :goto_0

    .line 396
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->g(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 397
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v2, v1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Lcom/sec/chaton/trunk/TrunkPageActivity;I)V

    .line 410
    :cond_3
    :goto_2
    return-void

    .line 400
    :cond_4
    const/4 v1, 0x2

    move/from16 v0, p1

    if-ne v0, v1, :cond_3

    .line 401
    if-eqz p3, :cond_3

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 402
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    .line 403
    const-string v1, "total_comment_count"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 405
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    iget-object v1, v1, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Lcom/sec/chaton/trunk/TrunkPageActivity;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/trunk/bz;

    iput v2, v1, Lcom/sec/chaton/trunk/bz;->j:I

    .line 407
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/trunk/bv;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v1, v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->c(Lcom/sec/chaton/trunk/TrunkPageActivity;I)V

    goto :goto_2

    :cond_5
    move/from16 v17, v1

    goto/16 :goto_1
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 1

    .prologue
    .line 418
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 421
    :cond_0
    return-void
.end method
