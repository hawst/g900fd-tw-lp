.class Lcom/sec/chaton/trunk/cj;
.super Ljava/lang/Object;
.source "TrunkView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkView;)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f070592

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 553
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->m(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->m(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 555
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->j(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/r;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/r;->a(Z)V

    .line 561
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->j(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 562
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->n(Lcom/sec/chaton/trunk/TrunkView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 563
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->n(Lcom/sec/chaton/trunk/TrunkView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 566
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkView;->e(Lcom/sec/chaton/trunk/TrunkView;Z)V

    .line 575
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->f(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 576
    return-void

    .line 557
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->m(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 558
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->j(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/r;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/r;->a(Z)V

    goto :goto_0

    .line 568
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->n(Lcom/sec/chaton/trunk/TrunkView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 569
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->n(Lcom/sec/chaton/trunk/TrunkView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 572
    iget-object v0, p0, Lcom/sec/chaton/trunk/cj;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0, v2}, Lcom/sec/chaton/trunk/TrunkView;->e(Lcom/sec/chaton/trunk/TrunkView;Z)V

    goto :goto_1
.end method
