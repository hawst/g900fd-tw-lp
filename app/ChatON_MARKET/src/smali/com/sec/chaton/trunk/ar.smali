.class Lcom/sec/chaton/trunk/ar;
.super Landroid/os/Handler;
.source "TrunkFullView.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkFullView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkFullView;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, -0x2

    const/4 v6, -0x3

    const v5, 0x7f0b002a

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 220
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 222
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_0

    .line 224
    const-string v0, "Fragment had been detached."

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkFullView;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 295
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/trunk/b/e;

    .line 296
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 298
    iget-object v2, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkFullView;->f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkFullView;->f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 299
    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1, v4}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Lcom/sec/chaton/trunk/TrunkFullView;Z)Z

    .line 304
    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkFullView;->g(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, v4}, Lcom/sec/chaton/trunk/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkFullView;->h(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkFullView;->f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/trunk/TrunkFullView;->a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V

    .line 310
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->i(Lcom/sec/chaton/trunk/TrunkFullView;)V

    goto :goto_0

    .line 233
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 235
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_3

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/TrunkFullView;->a(Lcom/sec/chaton/trunk/TrunkFullView;Z)Z

    goto :goto_0

    .line 238
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1, v3}, Lcom/sec/chaton/trunk/TrunkFullView;->a(Lcom/sec/chaton/trunk/TrunkFullView;Z)Z

    .line 240
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x1518b

    if-ne v0, v1, :cond_4

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->a(Lcom/sec/chaton/trunk/TrunkFullView;)V

    goto :goto_0

    .line 247
    :cond_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 248
    if-eq v6, v0, :cond_5

    if-ne v7, v0, :cond_6

    .line 249
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Lcom/sec/chaton/trunk/TrunkFullView;)V

    goto/16 :goto_0

    .line 251
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->c(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    invoke-static {v0, v5, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 261
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 263
    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkFullView;->d(Lcom/sec/chaton/trunk/TrunkFullView;)Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 264
    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkFullView;->d(Lcom/sec/chaton/trunk/TrunkFullView;)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 267
    :cond_7
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_8

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->c(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    const v1, 0x7f0b0167

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->e(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/ay;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->e(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/ay;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/trunk/ay;->a()V

    goto/16 :goto_0

    .line 274
    :cond_8
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 275
    if-eq v6, v1, :cond_9

    if-ne v7, v1, :cond_a

    .line 276
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Lcom/sec/chaton/trunk/TrunkFullView;)V

    goto/16 :goto_0

    .line 277
    :cond_a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x13952

    if-ne v0, v1, :cond_b

    .line 278
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->c(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    const v1, 0x7f0b016b

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 287
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->c(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    invoke-static {v0, v5, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 316
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/trunk/b/d;

    .line 317
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 319
    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkFullView;->f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkFullView;->f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Lcom/sec/chaton/trunk/TrunkFullView;Z)Z

    .line 323
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->i(Lcom/sec/chaton/trunk/TrunkFullView;)V

    goto/16 :goto_0

    .line 329
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/trunk/b/f;

    .line 330
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/f;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 332
    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkFullView;->f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkFullView;->f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkFullView;->c(Lcom/sec/chaton/trunk/TrunkFullView;Z)Z

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Lcom/sec/chaton/trunk/TrunkFullView;Z)Z

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    iget-object v0, v0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->h(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_10

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->j(Lcom/sec/chaton/trunk/TrunkFullView;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->k(Lcom/sec/chaton/trunk/TrunkFullView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkFullView;->d(Lcom/sec/chaton/trunk/TrunkFullView;Z)V

    .line 355
    :cond_d
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->c(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkFullView;->c(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 358
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->i(Lcom/sec/chaton/trunk/TrunkFullView;)V

    goto/16 :goto_0

    .line 344
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/TrunkFullView;->d(Lcom/sec/chaton/trunk/TrunkFullView;Z)V

    goto :goto_1

    .line 348
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->h(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_d

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->k(Lcom/sec/chaton/trunk/TrunkFullView;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->j(Lcom/sec/chaton/trunk/TrunkFullView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/sec/chaton/trunk/ar;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkFullView;->d(Lcom/sec/chaton/trunk/TrunkFullView;Z)V

    goto :goto_1

    .line 230
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x385 -> :sswitch_1
        0x389 -> :sswitch_2
    .end sparse-switch
.end method
