.class Lcom/sec/chaton/trunk/cg;
.super Landroid/os/Handler;
.source "TrunkView.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkView;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, -0x2

    const/4 v6, -0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 249
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    iget-boolean v0, v0, Lcom/sec/chaton/trunk/TrunkView;->a:Z

    if-ne v0, v4, :cond_1

    .line 254
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_0

    .line 255
    const-string v0, "TrunkView is destoried."

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkView;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 263
    iget-object v1, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v1, v5}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 267
    :sswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 268
    const-string v1, "Response: TrunkMessageControl.METHOD_GET_ITEM_LIST"

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkView;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_8

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->b(Lcom/sec/chaton/trunk/TrunkView;)V

    .line 280
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->c(Lcom/sec/chaton/trunk/TrunkView;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Z)Z

    .line 283
    invoke-static {}, Lcom/sec/chaton/trunk/database/a/b;->a()Lcom/sec/chaton/trunk/database/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/database/a/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 284
    new-instance v1, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v1}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk_item"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v1

    const-string v2, "registration_time DESC"

    invoke-virtual {v1, v0, v5, v5, v2}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 290
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/16 v2, 0x1e

    if-lt v0, v2, :cond_6

    .line 292
    const/16 v0, 0x1d

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    const-string v2, "item_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    :goto_1
    if-eqz v1, :cond_4

    .line 300
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 304
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->f(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/trunk/ch;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/ch;-><init>(Lcom/sec/chaton/trunk/cg;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 358
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkView;->b(Lcom/sec/chaton/trunk/TrunkView;Z)Z

    goto/16 :goto_0

    .line 296
    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 299
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_7

    .line 300
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 299
    :cond_7
    throw v0

    .line 336
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->g(Lcom/sec/chaton/trunk/TrunkView;)V

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->h(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/Spinner;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->h(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/Spinner;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkView;->i(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/a/b;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 342
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->j(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->getCount()I

    move-result v0

    if-nez v0, :cond_a

    .line 343
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    sget-object v1, Lcom/sec/chaton/trunk/cq;->c:Lcom/sec/chaton/trunk/cq;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Lcom/sec/chaton/trunk/cq;)V

    .line 346
    :cond_a
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 348
    if-eq v6, v0, :cond_b

    if-ne v7, v0, :cond_c

    .line 349
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    sget-object v1, Lcom/sec/chaton/trunk/cp;->a:Lcom/sec/chaton/trunk/cp;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Lcom/sec/chaton/trunk/cp;)V

    goto :goto_2

    .line 351
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    sget-object v1, Lcom/sec/chaton/trunk/cp;->b:Lcom/sec/chaton/trunk/cp;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Lcom/sec/chaton/trunk/cp;)V

    goto :goto_2

    .line 365
    :sswitch_1
    iget-object v1, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkView;->k(Lcom/sec/chaton/trunk/TrunkView;)Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 366
    iget-object v1, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkView;->k(Lcom/sec/chaton/trunk/TrunkView;)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 369
    :cond_d
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_e

    .line 370
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->l(Lcom/sec/chaton/trunk/TrunkView;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0167

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 371
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkView;->c(Lcom/sec/chaton/trunk/TrunkView;Z)Z

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->j(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->b()V

    .line 373
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->m(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 374
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    sget-object v1, Lcom/sec/chaton/trunk/cq;->e:Lcom/sec/chaton/trunk/cq;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Lcom/sec/chaton/trunk/cq;)V

    .line 402
    :goto_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->o(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 376
    :cond_e
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 377
    if-eq v6, v1, :cond_f

    if-ne v7, v1, :cond_10

    .line 378
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    sget-object v1, Lcom/sec/chaton/trunk/cp;->a:Lcom/sec/chaton/trunk/cp;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Lcom/sec/chaton/trunk/cp;)V

    goto :goto_3

    .line 379
    :cond_10
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x13952

    if-ne v0, v1, :cond_12

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->l(Lcom/sec/chaton/trunk/TrunkView;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b016b

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 382
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkView;->d(Lcom/sec/chaton/trunk/TrunkView;Z)Z

    .line 383
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->j(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->b()V

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->m(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 385
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    sget-object v1, Lcom/sec/chaton/trunk/cq;->e:Lcom/sec/chaton/trunk/cq;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Lcom/sec/chaton/trunk/cq;)V

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->n(Lcom/sec/chaton/trunk/TrunkView;)Landroid/view/Menu;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->n(Lcom/sec/chaton/trunk/TrunkView;)Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f07058f

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkView;->n(Lcom/sec/chaton/trunk/TrunkView;)Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f07058f

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 393
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkView;->e(Lcom/sec/chaton/trunk/TrunkView;Z)V

    goto/16 :goto_3

    .line 398
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/trunk/cg;->a:Lcom/sec/chaton/trunk/TrunkView;

    sget-object v1, Lcom/sec/chaton/trunk/cp;->b:Lcom/sec/chaton/trunk/cp;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/TrunkView;Lcom/sec/chaton/trunk/cp;)V

    goto/16 :goto_3

    .line 265
    nop

    :sswitch_data_0
    .sparse-switch
        0x386 -> :sswitch_0
        0x38c -> :sswitch_1
    .end sparse-switch
.end method
