.class Lcom/sec/chaton/trunk/bi;
.super Landroid/os/Handler;
.source "TrunkItemView.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkItemView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkItemView;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const v7, 0x13952

    const/4 v6, -0x2

    const/4 v5, -0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 311
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 314
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_0

    .line 315
    const-string v0, "Fragment had been detached."

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkItemView;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 504
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/trunk/b/e;

    .line 505
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 507
    iget-object v2, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 508
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1, v3}, Lcom/sec/chaton/trunk/TrunkItemView;->f(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 513
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->t(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, v3}, Lcom/sec/chaton/trunk/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 516
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->u(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v3}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V

    .line 519
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->v(Lcom/sec/chaton/trunk/TrunkItemView;)V

    goto :goto_0

    .line 324
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 326
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_5

    .line 327
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1, v3}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 330
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 331
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/entry/GetItemEntry;

    .line 332
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    iget-object v0, v0, Lcom/sec/chaton/trunk/entry/GetItemEntry;->item:Lcom/sec/chaton/trunk/entry/inner/TrunkItem;

    iget-object v0, v0, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->itemid:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Ljava/lang/String;)Ljava/lang/String;

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/TrunkDetailActivity;

    .line 334
    if-eqz v0, :cond_3

    .line 335
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->a:Ljava/lang/String;

    .line 340
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->c(Lcom/sec/chaton/trunk/TrunkItemView;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->i(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/trunk/bj;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/bj;-><init>(Lcom/sec/chaton/trunk/bi;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 372
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/TrunkItemView;->c(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 374
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->j(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/trunk/k;->a()V

    .line 395
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->l(Lcom/sec/chaton/trunk/TrunkItemView;)V

    goto/16 :goto_0

    .line 377
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1, v4}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 379
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x1518b

    if-ne v0, v1, :cond_6

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->k(Lcom/sec/chaton/trunk/TrunkItemView;)V

    goto :goto_1

    .line 386
    :cond_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 387
    if-eq v5, v0, :cond_7

    if-ne v6, v0, :cond_8

    .line 388
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    sget-object v1, Lcom/sec/chaton/trunk/bs;->a:Lcom/sec/chaton/trunk/bs;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V

    goto :goto_1

    .line 390
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    sget-object v1, Lcom/sec/chaton/trunk/bs;->b:Lcom/sec/chaton/trunk/bs;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V

    goto :goto_1

    .line 407
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 409
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_b

    .line 410
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/entry/GetCommentListEntry;

    .line 412
    iget-object v0, v0, Lcom/sec/chaton/trunk/entry/GetCommentListEntry;->comments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_9

    .line 415
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 434
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->l(Lcom/sec/chaton/trunk/TrunkItemView;)V

    goto/16 :goto_0

    .line 417
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->m(Lcom/sec/chaton/trunk/TrunkItemView;)I

    move-result v0

    add-int/lit8 v0, v0, 0x64

    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->g(Lcom/sec/chaton/trunk/TrunkItemView;)I

    move-result v1

    if-lt v0, v1, :cond_a

    .line 420
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    goto :goto_2

    .line 422
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    goto :goto_2

    .line 426
    :cond_b
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 427
    if-eq v5, v0, :cond_c

    if-ne v6, v0, :cond_d

    .line 428
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    sget-object v1, Lcom/sec/chaton/trunk/bs;->a:Lcom/sec/chaton/trunk/bs;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V

    goto :goto_2

    .line 430
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    sget-object v1, Lcom/sec/chaton/trunk/bs;->b:Lcom/sec/chaton/trunk/bs;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V

    goto :goto_2

    .line 440
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 442
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->n(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 443
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->n(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 446
    :cond_e
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_f

    .line 447
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0167

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 449
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->o(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/br;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->o(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/br;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->p(Lcom/sec/chaton/trunk/TrunkItemView;)Z

    move-result v1

    invoke-interface {v0, v4, v1}, Lcom/sec/chaton/trunk/br;->a(ZZ)V

    goto/16 :goto_0

    .line 453
    :cond_f
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 454
    if-eq v5, v1, :cond_10

    if-ne v6, v1, :cond_11

    .line 455
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    sget-object v1, Lcom/sec/chaton/trunk/bs;->a:Lcom/sec/chaton/trunk/bs;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V

    goto/16 :goto_0

    .line 456
    :cond_11
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    if-ne v0, v7, :cond_12

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b016b

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkItemView;->e(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 460
    const-string v0, "ME"

    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->q(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->r(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f0705bd

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 466
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    sget-object v1, Lcom/sec/chaton/trunk/bs;->b:Lcom/sec/chaton/trunk/bs;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V

    goto/16 :goto_0

    .line 474
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 476
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->n(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_13

    .line 477
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->n(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 480
    :cond_13
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_0

    .line 483
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 484
    if-eq v5, v1, :cond_14

    if-ne v6, v1, :cond_15

    .line 485
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    sget-object v1, Lcom/sec/chaton/trunk/bs;->a:Lcom/sec/chaton/trunk/bs;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V

    goto/16 :goto_0

    .line 486
    :cond_15
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    if-ne v0, v7, :cond_16

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b016b

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkItemView;->e(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 490
    const-string v0, "ME"

    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->q(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->r(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f0705bd

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 496
    :cond_16
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    sget-object v1, Lcom/sec/chaton/trunk/bs;->b:Lcom/sec/chaton/trunk/bs;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V

    goto/16 :goto_0

    .line 525
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/trunk/b/d;

    .line 526
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 528
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_17

    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 529
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkItemView;->f(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 532
    :cond_17
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->v(Lcom/sec/chaton/trunk/TrunkItemView;)V

    goto/16 :goto_0

    .line 538
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/trunk/b/f;

    .line 539
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/f;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 540
    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1b

    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 541
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/TrunkItemView;->g(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 542
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkItemView;->f(Lcom/sec/chaton/trunk/TrunkItemView;Z)Z

    .line 544
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->w(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 546
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkItemView;->h(Lcom/sec/chaton/trunk/TrunkItemView;Z)V

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->u(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-eq v0, v1, :cond_18

    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->u(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_1c

    .line 552
    :cond_18
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->w(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f020448

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 553
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->u(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-eq v0, v1, :cond_19

    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 554
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->x(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02012b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 555
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->x(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 570
    :cond_1a
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 573
    :cond_1b
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->v(Lcom/sec/chaton/trunk/TrunkItemView;)V

    goto/16 :goto_0

    .line 560
    :cond_1c
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->u(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_1a

    .line 564
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->w(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f020449

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 565
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->x(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f020138

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 566
    iget-object v0, p0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->x(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 321
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_5
        0x4 -> :sswitch_6
        0x385 -> :sswitch_1
        0x387 -> :sswitch_2
        0x389 -> :sswitch_3
        0x38a -> :sswitch_4
    .end sparse-switch
.end method
