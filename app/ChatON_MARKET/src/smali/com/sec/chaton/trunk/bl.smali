.class Lcom/sec/chaton/trunk/bl;
.super Ljava/lang/Object;
.source "TrunkItemView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkItemView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkItemView;)V
    .locals 0

    .prologue
    .line 749
    iput-object p1, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 754
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 804
    :cond_0
    :goto_0
    return-void

    .line 760
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->z(Lcom/sec/chaton/trunk/TrunkItemView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 766
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->A(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 767
    iget-object v1, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f07046a

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-static {v1, v0}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Landroid/widget/EditText;)Landroid/widget/EditText;

    .line 768
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->B(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 769
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->A(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->B(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 774
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->u(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->u(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_4

    .line 776
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->o(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/br;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 778
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->o(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/br;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->u(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/br;->a(Lcom/sec/chaton/trunk/c/g;Ljava/lang/String;)V

    goto :goto_0

    .line 783
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->u(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_0

    .line 784
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 785
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->b(Lcom/sec/chaton/trunk/TrunkItemView;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    const v2, 0x7f0b002b

    invoke-virtual {v1, v2}, Lcom/sec/chaton/trunk/TrunkItemView;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 790
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_8

    .line 792
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 793
    const/4 v1, -0x3

    if-eq v1, v0, :cond_6

    const/4 v1, -0x2

    if-ne v1, v0, :cond_7

    .line 794
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    sget-object v1, Lcom/sec/chaton/trunk/bs;->a:Lcom/sec/chaton/trunk/bs;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V

    goto/16 :goto_0

    .line 796
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->C(Lcom/sec/chaton/trunk/TrunkItemView;)V

    goto/16 :goto_0

    .line 799
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->o(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/br;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->o(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/br;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/bl;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->s(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/br;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
