.class final enum Lcom/sec/chaton/trunk/cq;
.super Ljava/lang/Enum;
.source "TrunkView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/trunk/cq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/trunk/cq;

.field public static final enum b:Lcom/sec/chaton/trunk/cq;

.field public static final enum c:Lcom/sec/chaton/trunk/cq;

.field public static final enum d:Lcom/sec/chaton/trunk/cq;

.field public static final enum e:Lcom/sec/chaton/trunk/cq;

.field private static final synthetic f:[Lcom/sec/chaton/trunk/cq;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 106
    new-instance v0, Lcom/sec/chaton/trunk/cq;

    const-string v1, "TrunkItem"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/trunk/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/trunk/cq;->a:Lcom/sec/chaton/trunk/cq;

    .line 107
    new-instance v0, Lcom/sec/chaton/trunk/cq;

    const-string v1, "Nothing"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/trunk/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/trunk/cq;->b:Lcom/sec/chaton/trunk/cq;

    .line 108
    new-instance v0, Lcom/sec/chaton/trunk/cq;

    const-string v1, "NetworkError"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/trunk/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/trunk/cq;->c:Lcom/sec/chaton/trunk/cq;

    .line 109
    new-instance v0, Lcom/sec/chaton/trunk/cq;

    const-string v1, "DeleteMode"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/trunk/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/trunk/cq;->d:Lcom/sec/chaton/trunk/cq;

    .line 110
    new-instance v0, Lcom/sec/chaton/trunk/cq;

    const-string v1, "TrunkMode"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/trunk/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/trunk/cq;->e:Lcom/sec/chaton/trunk/cq;

    .line 105
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/trunk/cq;

    sget-object v1, Lcom/sec/chaton/trunk/cq;->a:Lcom/sec/chaton/trunk/cq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/trunk/cq;->b:Lcom/sec/chaton/trunk/cq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/trunk/cq;->c:Lcom/sec/chaton/trunk/cq;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/trunk/cq;->d:Lcom/sec/chaton/trunk/cq;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/trunk/cq;->e:Lcom/sec/chaton/trunk/cq;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/trunk/cq;->f:[Lcom/sec/chaton/trunk/cq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/trunk/cq;
    .locals 1

    .prologue
    .line 105
    const-class v0, Lcom/sec/chaton/trunk/cq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/cq;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/trunk/cq;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/sec/chaton/trunk/cq;->f:[Lcom/sec/chaton/trunk/cq;

    invoke-virtual {v0}, [Lcom/sec/chaton/trunk/cq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/trunk/cq;

    return-object v0
.end method
