.class Lcom/sec/chaton/trunk/bb;
.super Landroid/os/Handler;
.source "TrunkItemPresenter.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/ba;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/ba;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ba;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 186
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 114
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 116
    iget-object v1, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v1}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 117
    iget-object v1, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v1}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 120
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_3

    .line 121
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x1518b

    if-ne v0, v1, :cond_3

    .line 123
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->b(Lcom/sec/chaton/trunk/ba;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v1}, Lcom/sec/chaton/trunk/ba;->c(Lcom/sec/chaton/trunk/ba;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/trunk/database/a/a;->b(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 126
    invoke-static {}, Lcom/sec/chaton/trunk/ba;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->c(Lcom/sec/chaton/trunk/ba;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 136
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    iget-object v1, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v1}, Lcom/sec/chaton/trunk/ba;->d(Lcom/sec/chaton/trunk/ba;)Lcom/sec/chaton/trunk/a/a/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/a/a/e;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;Ljava/lang/String;)Ljava/lang/String;

    .line 139
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->c(Lcom/sec/chaton/trunk/ba;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->e(Lcom/sec/chaton/trunk/ba;)V

    goto/16 :goto_0

    .line 148
    :sswitch_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 152
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->e(Lcom/sec/chaton/trunk/ba;)V

    goto/16 :goto_0

    .line 158
    :sswitch_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 167
    :sswitch_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 171
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->e(Lcom/sec/chaton/trunk/ba;)V

    goto/16 :goto_0

    .line 177
    :sswitch_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 195
    :sswitch_6
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 204
    :sswitch_7
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/trunk/bb;->a:Lcom/sec/chaton/trunk/ba;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ba;->a(Lcom/sec/chaton/trunk/ba;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 111
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_6
        0x4 -> :sswitch_7
        0x385 -> :sswitch_1
        0x387 -> :sswitch_2
        0x389 -> :sswitch_3
        0x38a -> :sswitch_4
        0x38b -> :sswitch_5
    .end sparse-switch
.end method
