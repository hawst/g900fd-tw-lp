.class public Lcom/sec/chaton/trunk/al;
.super Lcom/sec/common/f/a;
.source "TrunkDispatcherTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/sec/chaton/trunk/c/g;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Landroid/graphics/drawable/BitmapDrawable;

.field private e:Z

.field private i:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/sec/chaton/trunk/c/g;Ljava/lang/Boolean;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 49
    iput-object p2, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    .line 51
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/al;->b:Landroid/content/Context;

    .line 55
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/al;->e:Z

    .line 60
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    .line 62
    iput-object p4, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    .line 64
    return-void
.end method

.method private a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 350
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 351
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 353
    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 355
    if-eqz p2, :cond_0

    .line 359
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 366
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 367
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 368
    const/high16 v1, 0x435c0000    # 220.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 369
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 374
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Z)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    .line 237
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 238
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/trunk/al;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 242
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 247
    :cond_1
    if-eqz p1, :cond_6

    .line 248
    :try_start_0
    instance-of v1, p1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 250
    move-object v0, p1

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 251
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 252
    const-string v1, " This bitmap is already recycled. "

    const-class v2, Lcom/sec/chaton/trunk/al;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :cond_2
    :goto_0
    return-void

    .line 258
    :cond_3
    iget-boolean v1, p0, Lcom/sec/chaton/trunk/al;->e:Z

    if-eqz v1, :cond_4

    .line 260
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->h()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-direct {p0, v1, p1}, Lcom/sec/chaton/trunk/al;->a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 270
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v2, :cond_5

    .line 271
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 272
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    const v2, 0x7f020138

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 273
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 335
    :catch_0
    move-exception v1

    .line 336
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0149

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 337
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 340
    :catchall_0
    move-exception v1

    throw v1

    .line 267
    :cond_4
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->h()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 338
    :catch_1
    move-exception v1

    .line 339
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 275
    :cond_5
    :try_start_4
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v2, :cond_2

    .line 276
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 277
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    const v2, 0x7f02012b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 278
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 302
    :cond_6
    iget-boolean v1, p0, Lcom/sec/chaton/trunk/al;->e:Z

    if-eqz v1, :cond_a

    .line 303
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-eq v1, v2, :cond_7

    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v2, :cond_9

    .line 304
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->h()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020448

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 315
    :cond_8
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v2, :cond_d

    .line 316
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 317
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    const v2, 0x7f02012b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 318
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 305
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v2, :cond_8

    .line 306
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->h()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020449

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 309
    :cond_a
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-eq v1, v2, :cond_b

    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v2, :cond_c

    .line 310
    :cond_b
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->h()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020440

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 311
    :cond_c
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v2, :cond_8

    .line 312
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->h()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020447

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 320
    :cond_d
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v2, :cond_e

    .line 322
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 323
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    const v2, 0x7f020138

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 324
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 329
    :cond_e
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 330
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 68
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 69
    const-string v0, "onPreDispatch()"

    const-class v1, Lcom/sec/chaton/trunk/al;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/al;->e:Z

    if-nez v0, :cond_1

    .line 76
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 83
    :cond_2
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 89
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 90
    const-string v0, "onDispatch()"

    const-class v1, Lcom/sec/chaton/trunk/al;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 94
    sget-object v0, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v2

    .line 230
    :goto_0
    return-object v0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    .line 102
    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    const-string v3, "thumbnail"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 104
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 109
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v4, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v4, :cond_3

    .line 111
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 112
    const-string v4, "jpg"

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 115
    :cond_3
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v3, v5

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "thumbnail"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 120
    :cond_4
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 122
    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v3, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-eq v1, v3, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v3, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v3, :cond_6

    .line 124
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v0}, Lcom/sec/chaton/util/ad;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    .line 131
    :cond_6
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 132
    const/4 v3, 0x0

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 133
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 134
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 135
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    goto/16 :goto_0

    .line 158
    :catch_0
    move-exception v0

    .line 160
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0b0149

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 161
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v0, v2

    .line 162
    goto/16 :goto_0

    .line 164
    :catch_1
    move-exception v0

    .line 165
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 166
    goto/16 :goto_0

    .line 167
    :catchall_0
    move-exception v0

    throw v0

    .line 172
    :cond_7
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-eq v0, v1, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_9

    .line 173
    :cond_8
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v0}, Lcom/sec/chaton/util/ad;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    .line 177
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->a:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_b

    .line 178
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/al;->e:Z

    if-nez v0, :cond_a

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->b:Landroid/content/Context;

    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v0, v1, v3, v4, v5}, Lcom/sec/chaton/util/r;->a(Landroid/content/Context;Ljava/io/File;ZZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    .line 191
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->c:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    goto/16 :goto_0

    .line 197
    :catch_2
    move-exception v0

    .line 199
    :try_start_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0b0149

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 200
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v0, v2

    .line 201
    goto/16 :goto_0

    .line 203
    :catch_3
    move-exception v0

    .line 204
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v0, v2

    .line 205
    goto/16 :goto_0

    .line 206
    :catchall_1
    move-exception v0

    throw v0

    :cond_b
    move-object v0, v2

    .line 230
    goto/16 :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/al;->g()Ljava/lang/Object;

    move-result-object v1

    .line 395
    if-eqz v1, :cond_0

    .line 396
    instance-of v0, v1, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 397
    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 398
    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 400
    :cond_1
    instance-of v0, v1, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->d:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/trunk/al;->d:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 404
    if-eqz v0, :cond_0

    .line 405
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 406
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method
