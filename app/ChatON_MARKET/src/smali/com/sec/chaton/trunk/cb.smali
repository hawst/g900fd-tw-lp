.class Lcom/sec/chaton/trunk/cb;
.super Landroid/os/Handler;
.source "TrunkPresenter.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/ca;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/ca;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ca;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 79
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 81
    :sswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 82
    const-string v1, "Response: TrunkMessageControl.METHOD_GET_ITEM_LIST"

    invoke-static {}, Lcom/sec/chaton/trunk/ca;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v1}, Lcom/sec/chaton/trunk/ca;->a(Lcom/sec/chaton/trunk/ca;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 86
    iget-object v1, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v1}, Lcom/sec/chaton/trunk/ca;->a(Lcom/sec/chaton/trunk/ca;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 89
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_4

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ca;->b(Lcom/sec/chaton/trunk/ca;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ca;->c(Lcom/sec/chaton/trunk/ca;)V

    .line 95
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/ca;->a(Lcom/sec/chaton/trunk/ca;Z)Z

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ca;->d(Lcom/sec/chaton/trunk/ca;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ca;->e(Lcom/sec/chaton/trunk/ca;)V

    goto :goto_0

    .line 106
    :sswitch_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ca;->a(Lcom/sec/chaton/trunk/ca;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/trunk/cb;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ca;->a(Lcom/sec/chaton/trunk/ca;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 79
    nop

    :sswitch_data_0
    .sparse-switch
        0x386 -> :sswitch_0
        0x38c -> :sswitch_1
    .end sparse-switch
.end method
