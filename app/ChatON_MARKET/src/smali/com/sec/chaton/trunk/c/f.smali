.class public Lcom/sec/chaton/trunk/c/f;
.super Ljava/lang/Object;
.source "TrunkUtil.java"


# direct methods
.method public static a(Ljava/lang/String;Z)Lcom/sec/chaton/trunk/c/g;
    .locals 2

    .prologue
    .line 33
    if-eqz p1, :cond_0

    .line 34
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    .line 50
    :goto_0
    return-object v0

    .line 37
    :cond_0
    const-string v0, "image"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    goto :goto_0

    .line 39
    :cond_1
    const-string v0, "video"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 40
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    goto :goto_0

    .line 41
    :cond_2
    const-string v0, "mixed"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 42
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 43
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    goto :goto_0

    .line 46
    :cond_3
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    goto :goto_0

    .line 50
    :cond_4
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->d:Lcom/sec/chaton/trunk/c/g;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 56
    if-nez p0, :cond_1

    move-object v0, v1

    .line 79
    :cond_0
    :goto_0
    return-object v0

    .line 60
    :cond_1
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object v2

    .line 62
    const-string v0, "/"

    invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 63
    const-string v0, "?"

    invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 65
    if-ne v3, v4, :cond_2

    move-object v0, v1

    .line 66
    goto :goto_0

    .line 69
    :cond_2
    if-ne v0, v4, :cond_3

    .line 70
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 73
    :cond_3
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    :cond_4
    new-instance v0, Ljava/net/MalformedURLException;

    const-string v1, "File name is null."

    invoke-direct {v0, v1}, Ljava/net/MalformedURLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/widget/TextView;I)V
    .locals 3

    .prologue
    .line 114
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 117
    const v0, 0x7f0b005a

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(I)V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 85
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v0

    .line 89
    :cond_1
    const-string v2, "file://"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 90
    const/4 v2, 0x6

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 94
    :cond_2
    new-instance v2, Lcom/sec/amsoma/AMSLibs;

    invoke-direct {v2}, Lcom/sec/amsoma/AMSLibs;-><init>()V

    .line 95
    invoke-virtual {v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_Init()V

    .line 96
    new-instance v3, Lcom/sec/amsoma/structure/AMS_UI_DATA;

    invoke-direct {v3}, Lcom/sec/amsoma/structure/AMS_UI_DATA;-><init>()V

    .line 98
    new-array v4, v1, [Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;

    .line 99
    new-instance v5, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;

    invoke-direct {v5}, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;-><init>()V

    aput-object v5, v4, v0

    .line 100
    aget-object v5, v4, v0

    invoke-virtual {v5, v0}, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->setM_eExportType(I)V

    .line 101
    aget-object v5, v4, v0

    invoke-virtual {v5, p0}, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->setM_strFileName(Ljava/lang/String;)V

    .line 103
    invoke-virtual {v2, v3, v4}, Lcom/sec/amsoma/AMSLibs;->VipAMS_IsAMSJPEGFile(Lcom/sec/amsoma/structure/AMS_UI_DATA;[Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;)I

    move-result v2

    .line 105
    if-nez v2, :cond_0

    move v0, v1

    .line 106
    goto :goto_0
.end method
