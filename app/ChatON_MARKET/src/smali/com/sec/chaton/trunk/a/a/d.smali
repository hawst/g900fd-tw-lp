.class public Lcom/sec/chaton/trunk/a/a/d;
.super Lcom/sec/chaton/d/a/a;
.source "GetItemListTask.java"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Z

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/sec/chaton/trunk/a/a/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Z)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/trunk/a/a/d;->e:Ljava/lang/String;

    .line 61
    iput-boolean p3, p0, Lcom/sec/chaton/trunk/a/a/d;->c:Z

    .line 62
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 13

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_14

    .line 73
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;

    .line 75
    if-nez v0, :cond_1

    .line 76
    const-string v0, "Http result object is null"

    sget-object v1, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    const-string v5, ""

    .line 82
    const-string v4, ""

    .line 83
    const/4 v3, 0x0

    .line 84
    const/4 v2, 0x0

    .line 85
    const/4 v7, 0x0

    .line 86
    const/4 v6, 0x0

    .line 90
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 92
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->d()Lcom/sec/chaton/j/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/j/h;->f()Ljava/util/List;

    move-result-object v1

    .line 94
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/NameValuePair;

    .line 95
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "sessionid"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 96
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    move v12, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v1

    move v1, v12

    :goto_2
    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    move v2, v1

    .line 102
    goto :goto_1

    .line 97
    :cond_2
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 98
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/trunk/a/b;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/a/b;

    move-result-object v1

    move-object v3, v4

    move-object v4, v5

    move v12, v2

    move-object v2, v1

    move v1, v12

    goto :goto_2

    .line 99
    :cond_3
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 100
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    goto :goto_2

    .line 101
    :cond_4
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "startitemid"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_17

    .line 102
    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    move-object v4, v5

    move-object v12, v3

    move-object v3, v1

    move v1, v2

    move-object v2, v12

    goto :goto_2

    .line 109
    :cond_5
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "0"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 111
    const/4 v1, 0x1

    move v4, v1

    .line 115
    :goto_3
    if-nez v3, :cond_6

    .line 116
    const-string v0, "The ordering type is null."

    sget-object v1, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 121
    :cond_6
    iget-boolean v1, p0, Lcom/sec/chaton/trunk/a/a/d;->c:Z

    if-eqz v1, :cond_7

    .line 122
    const-string v1, "Truncate trunk item cache."

    sget-object v3, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-static {v5}, Lcom/sec/chaton/trunk/database/a/a;->c(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    :cond_7
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_15

    .line 139
    const-string v1, "Save trunk item to database."

    sget-object v3, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v1, v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 144
    if-eqz v4, :cond_8

    .line 145
    add-int/lit8 v2, v2, -0x1

    .line 146
    add-int/lit8 v1, v1, -0x1

    .line 149
    :cond_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Request count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Response count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    sub-int v1, v2, v1

    if-gtz v1, :cond_9

    const/4 v4, 0x1

    .line 157
    :goto_4
    if-eqz v4, :cond_b

    .line 158
    invoke-static {}, Lcom/sec/chaton/trunk/database/a/b;->a()Lcom/sec/chaton/trunk/database/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/database/a/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 160
    new-instance v2, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v2}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v3, "trunk_item"

    invoke-virtual {v2, v3}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v6, 0x0

    const-string v7, "registration_time DESC"

    invoke-virtual {v2, v1, v3, v6, v7}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_a

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_a

    .line 163
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 164
    const-string v2, "registration_time"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/trunk/a/a/d;->e:Ljava/lang/String;

    .line 174
    :goto_5
    const/4 v1, 0x0

    move v2, v1

    :goto_6
    iget-object v1, v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_f

    .line 175
    iget-object v1, v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;->items:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;

    .line 176
    iput-object v5, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->sessionid:Ljava/lang/String;

    .line 178
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Item id: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->itemid:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", HasMore: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", IsLastItem: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v3, v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_c

    const/4 v3, 0x1

    :goto_7
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v3, v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_e

    .line 181
    if-eqz v4, :cond_d

    .line 182
    const/4 v3, 0x1

    invoke-static {v1, v3}, Lcom/sec/chaton/trunk/database/a/a;->a(Lcom/sec/chaton/trunk/entry/inner/TrunkItem;Z)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Item id: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->itemid:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :goto_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_6

    .line 151
    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 166
    :cond_a
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/chaton/trunk/a/a/d;->e:Ljava/lang/String;

    goto/16 :goto_5

    .line 169
    :cond_b
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/chaton/trunk/a/a/d;->e:Ljava/lang/String;

    goto/16 :goto_5

    .line 178
    :cond_c
    const/4 v3, 0x0

    goto :goto_7

    .line 186
    :cond_d
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/sec/chaton/trunk/database/a/a;->a(Lcom/sec/chaton/trunk/entry/inner/TrunkItem;Z)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Item id: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->itemid:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 192
    :cond_e
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/sec/chaton/trunk/database/a/a;->a(Lcom/sec/chaton/trunk/entry/inner/TrunkItem;Z)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Item id: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->itemid:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :cond_f
    move v2, v4

    .line 199
    :goto_9
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 203
    iget-object v1, p0, Lcom/sec/chaton/trunk/a/a/d;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 205
    invoke-static {}, Lcom/sec/chaton/trunk/database/a/b;->a()Lcom/sec/chaton/trunk/database/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/database/a/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 206
    new-instance v3, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v3}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v4, "trunk_item"

    invoke-virtual {v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    const-string v7, "registration_time DESC"

    invoke-virtual {v3, v1, v4, v6, v7}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 208
    const/16 v3, 0x1d

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 210
    const-string v3, "item_type"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 211
    iget-object v1, p0, Lcom/sec/chaton/trunk/a/a/d;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    iget-object v1, v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;->items:Ljava/util/List;

    const/16 v6, 0x1d

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;

    iget-object v1, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->regdttm:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v3, v6

    if-gez v1, :cond_0

    .line 212
    invoke-static {v5}, Lcom/sec/chaton/trunk/database/a/a;->c(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    const/4 v1, 0x0

    move v3, v1

    :goto_a
    iget-object v1, v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_13

    .line 215
    iget-object v1, v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;->items:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;

    .line 216
    iput-object v5, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->sessionid:Ljava/lang/String;

    .line 218
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Item id: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->itemid:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", HasMore: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", IsLastItem: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;->items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_10

    const/4 v4, 0x1

    :goto_b
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v4, v0, Lcom/sec/chaton/trunk/entry/GetItemListEntry;->items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_12

    .line 221
    if-eqz v2, :cond_11

    .line 222
    const/4 v4, 0x1

    invoke-static {v1, v4}, Lcom/sec/chaton/trunk/database/a/a;->a(Lcom/sec/chaton/trunk/entry/inner/TrunkItem;Z)Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Item id: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->itemid:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :goto_c
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_a

    .line 218
    :cond_10
    const/4 v4, 0x0

    goto :goto_b

    .line 226
    :cond_11
    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/sec/chaton/trunk/database/a/a;->a(Lcom/sec/chaton/trunk/entry/inner/TrunkItem;Z)Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Item id: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->itemid:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 232
    :cond_12
    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/sec/chaton/trunk/database/a/a;->a(Lcom/sec/chaton/trunk/entry/inner/TrunkItem;Z)Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Item id: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, v1, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->itemid:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/trunk/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 237
    :cond_13
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v8}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    goto/16 :goto_0

    .line 244
    :cond_14
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 245
    const-string v0, "01000016"

    const-string v1, "0102"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto/16 :goto_0

    :cond_15
    move v2, v6

    goto/16 :goto_9

    :cond_16
    move v4, v7

    goto/16 :goto_3

    :cond_17
    move v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    goto/16 :goto_2
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method
