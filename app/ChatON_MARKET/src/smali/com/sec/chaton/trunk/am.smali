.class public Lcom/sec/chaton/trunk/am;
.super Lcom/sec/chaton/trunk/a;
.source "TrunkFullPresenter.java"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private d:Lcom/sec/chaton/trunk/f;

.field private e:Landroid/app/Activity;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lcom/sec/chaton/trunk/c/g;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Lcom/sec/chaton/trunk/a/a;

.field private p:Landroid/os/Handler;

.field private q:Lcom/sec/chaton/trunk/a/a/e;

.field private r:Lcom/sec/chaton/trunk/b/b;

.field private s:Lcom/sec/chaton/trunk/a/a/b;

.field private t:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/chaton/trunk/am;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/trunk/f;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/sec/chaton/trunk/a;-><init>()V

    .line 80
    new-instance v0, Lcom/sec/chaton/trunk/an;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/an;-><init>(Lcom/sec/chaton/trunk/am;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->t:Landroid/os/Handler;

    .line 156
    iput-object p3, p0, Lcom/sec/chaton/trunk/am;->f:Ljava/lang/String;

    .line 157
    iput-object p4, p0, Lcom/sec/chaton/trunk/am;->g:Ljava/lang/String;

    .line 158
    iput-object p5, p0, Lcom/sec/chaton/trunk/am;->h:Ljava/lang/String;

    .line 159
    iput-object p1, p0, Lcom/sec/chaton/trunk/am;->d:Lcom/sec/chaton/trunk/f;

    .line 160
    iput-object p2, p0, Lcom/sec/chaton/trunk/am;->p:Landroid/os/Handler;

    .line 161
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->p:Landroid/os/Handler;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 479
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/chaton/trunk/c/a;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    const/4 v1, 0x3

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "thumbnail"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/am;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->q:Lcom/sec/chaton/trunk/a/a/e;

    if-eqz v0, :cond_1

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->q:Lcom/sec/chaton/trunk/a/a/e;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/e;->c()V

    .line 260
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->e:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/trunk/database/a/a;->f(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 274
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->o:Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/trunk/am;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/sec/chaton/trunk/a/a;->a(Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/chaton/trunk/a/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->q:Lcom/sec/chaton/trunk/a/a/e;

    goto :goto_0

    .line 261
    :catch_0
    move-exception v0

    .line 262
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 263
    sget-object v1, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 265
    :catch_1
    move-exception v0

    .line 266
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 267
    sget-object v1, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/am;II)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/trunk/am;->a(II)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/am;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->e:Landroid/app/Activity;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 488
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/chaton/trunk/c/a;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/am;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/am;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/chaton/trunk/am;->n()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/am;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/chaton/trunk/am;->k()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/am;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/chaton/trunk/am;->l()V

    return-void
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    return-object v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/am;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    :goto_0
    return-void

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->s:Lcom/sec/chaton/trunk/a/a/b;

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->s:Lcom/sec/chaton/trunk/a/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/b;->c()V

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->o:Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/trunk/am;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/trunk/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/trunk/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->s:Lcom/sec/chaton/trunk/a/a/b;

    goto :goto_0
.end method

.method private l()V
    .locals 7

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/am;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    :goto_0
    return-void

    .line 306
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/am;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 308
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v3, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v4, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->r:Lcom/sec/chaton/trunk/b/b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->r:Lcom/sec/chaton/trunk/b/b;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_2

    .line 314
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 315
    const-string v0, "Cancel previous download task."

    sget-object v1, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->r:Lcom/sec/chaton/trunk/b/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/b/b;->cancel(Z)Z

    .line 322
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->k:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_4

    .line 323
    invoke-static {}, Lcom/sec/chaton/trunk/b/a;->a()Lcom/sec/chaton/trunk/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->t:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/trunk/am;->j:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/trunk/b/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/sec/chaton/trunk/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->r:Lcom/sec/chaton/trunk/b/b;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 328
    :catch_0
    move-exception v0

    .line 329
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 330
    sget-object v1, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 333
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->r:Lcom/sec/chaton/trunk/b/b;

    goto :goto_0

    .line 325
    :cond_4
    :try_start_1
    invoke-static {}, Lcom/sec/chaton/trunk/b/a;->a()Lcom/sec/chaton/trunk/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->t:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/trunk/am;->j:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/trunk/b/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/sec/chaton/trunk/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->r:Lcom/sec/chaton/trunk/b/b;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private m()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 343
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 345
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "item_id"

    aput-object v3, v1, v5

    const-string v3, " = ?"

    aput-object v3, v1, v4

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 346
    new-array v4, v4, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->h:Ljava/lang/String;

    aput-object v1, v4, v5

    .line 349
    sget-object v1, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 356
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/am;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 363
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/trunk/am;->m()Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 365
    if-nez v1, :cond_2

    .line 421
    if-eqz v1, :cond_0

    .line 422
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 369
    :cond_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 371
    const-string v0, "sender_uid"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->i:Ljava/lang/String;

    .line 395
    const-string v0, "down_url"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->j:Ljava/lang/String;

    .line 398
    const-string v0, "item_description"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->n:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/am;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 404
    iget-object v2, p0, Lcom/sec/chaton/trunk/am;->f:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/chaton/trunk/am;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 405
    iget-object v3, p0, Lcom/sec/chaton/trunk/am;->j:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 406
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v5, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->l:Ljava/lang/String;

    .line 407
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v0, v4

    const/4 v2, 0x1

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v4, v0, v2

    const/4 v2, 0x2

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->m:Ljava/lang/String;
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 416
    :goto_1
    :try_start_3
    const-string v0, "content_type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;Z)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->k:Lcom/sec/chaton/trunk/c/g;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 421
    :cond_3
    if-eqz v1, :cond_4

    .line 422
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 426
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->d:Lcom/sec/chaton/trunk/f;

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->i:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/f;->a(Ljava/lang/String;)V

    .line 437
    invoke-direct {p0}, Lcom/sec/chaton/trunk/am;->o()V

    goto/16 :goto_0

    .line 408
    :catch_0
    move-exception v0

    .line 409
    :try_start_4
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_5

    .line 410
    sget-object v2, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 413
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->m:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 421
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_6

    .line 422
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 421
    :cond_6
    throw v0
.end method

.method private o()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 444
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/am;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 471
    :cond_0
    :goto_0
    return-void

    .line 448
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 449
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "Redraw content image: "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/chaton/trunk/am;->m:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->k:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_5

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->m:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 454
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->d:Lcom/sec/chaton/trunk/f;

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->k:Lcom/sec/chaton/trunk/c/g;

    invoke-interface {v0, v1, v3, v3}, Lcom/sec/chaton/trunk/f;->a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->d:Lcom/sec/chaton/trunk/f;

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->k:Lcom/sec/chaton/trunk/c/g;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/trunk/am;->l:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/chaton/trunk/am;->m:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/trunk/f;->a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 462
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->k:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_0

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->m:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 464
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->d:Lcom/sec/chaton/trunk/f;

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->k:Lcom/sec/chaton/trunk/c/g;

    invoke-interface {v0, v1, v3, v3}, Lcom/sec/chaton/trunk/f;->a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 469
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->d:Lcom/sec/chaton/trunk/f;

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->k:Lcom/sec/chaton/trunk/c/g;

    iget-object v2, p0, Lcom/sec/chaton/trunk/am;->m:Ljava/lang/String;

    invoke-interface {v0, v1, v3, v2}, Lcom/sec/chaton/trunk/f;->a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected f()V
    .locals 3

    .prologue
    .line 165
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->f()V

    .line 167
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 168
    const-string v0, "TrunkFullPresenter.onCreate()"

    sget-object v1, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->d:Lcom/sec/chaton/trunk/f;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/f;->a()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->e:Landroid/app/Activity;

    .line 173
    new-instance v0, Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/am;->e:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/trunk/am;->t:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/trunk/a/a;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->o:Lcom/sec/chaton/trunk/a/a;

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->d:Lcom/sec/chaton/trunk/f;

    new-instance v1, Lcom/sec/chaton/trunk/ao;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/ao;-><init>(Lcom/sec/chaton/trunk/am;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/f;->a(Lcom/sec/chaton/trunk/h;)V

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->d:Lcom/sec/chaton/trunk/f;

    new-instance v1, Lcom/sec/chaton/trunk/ap;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/ap;-><init>(Lcom/sec/chaton/trunk/am;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/f;->a(Lcom/sec/chaton/trunk/g;)V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->d:Lcom/sec/chaton/trunk/f;

    new-instance v1, Lcom/sec/chaton/trunk/aq;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/aq;-><init>(Lcom/sec/chaton/trunk/am;)V

    invoke-interface {v0, v1}, Lcom/sec/chaton/trunk/f;->b(Lcom/sec/chaton/trunk/g;)V

    .line 197
    return-void
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 201
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->g()V

    .line 203
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 204
    const-string v0, "TrunkFullPresenter.onResume()"

    sget-object v1, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/trunk/am;->n()V

    .line 208
    return-void
.end method

.method protected h()V
    .locals 2

    .prologue
    .line 212
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->h()V

    .line 214
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "TrunkFullPresenter.onPause()"

    sget-object v1, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 2

    .prologue
    .line 221
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->i()V

    .line 223
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 224
    const-string v0, "TrunkFullPresenter.onDestroy()"

    sget-object v1, Lcom/sec/chaton/trunk/am;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->q:Lcom/sec/chaton/trunk/a/a/e;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->q:Lcom/sec/chaton/trunk/a/a/e;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/e;->c()V

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->s:Lcom/sec/chaton/trunk/a/a/b;

    if-eqz v0, :cond_2

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->s:Lcom/sec/chaton/trunk/a/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/b;->c()V

    .line 235
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->r:Lcom/sec/chaton/trunk/b/b;

    if-eqz v0, :cond_3

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/trunk/am;->r:Lcom/sec/chaton/trunk/b/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/b/b;->cancel(Z)Z

    .line 239
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/trunk/am;->r:Lcom/sec/chaton/trunk/b/b;

    .line 240
    return-void
.end method
