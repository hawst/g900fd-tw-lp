.class public Lcom/sec/chaton/trunk/TrunkPageActivity;
.super Lcom/sec/chaton/base/BaseActivity;
.source "TrunkPageActivity.java"

# interfaces
.implements Lcom/sec/chaton/trunk/ay;


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field private static final f:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/ImageView;

.field private B:Landroid/content/BroadcastReceiver;

.field private C:Landroid/widget/LinearLayout;

.field private D:Landroid/widget/TextView;

.field private E:Z

.field private F:Ljava/lang/String;

.field private G:Landroid/widget/Toast;

.field private H:Z

.field private I:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private J:Landroid/view/View$OnClickListener;

.field c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/trunk/bz;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field e:Lcom/sec/chaton/e/a/v;

.field private g:Landroid/support/v4/view/ViewPager;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lcom/sec/chaton/trunk/by;

.field private l:Ljava/lang/String;

.field private m:Lcom/sec/chaton/trunk/c/g;

.field private n:Lcom/sec/chaton/trunk/a/b;

.field private o:Lcom/sec/chaton/e/a/u;

.field private p:I

.field private q:Landroid/database/Cursor;

.field private r:I

.field private s:Landroid/widget/TextView;

.field private t:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/chaton/trunk/TrunkFullView;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcom/sec/common/f/c;

.field private v:Landroid/content/Context;

.field private w:Landroid/view/View;

.field private x:Landroid/view/MenuItem;

.field private y:Landroid/view/MenuItem;

.field private z:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/TrunkPageActivity;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    .line 263
    new-instance v0, Lcom/sec/chaton/trunk/bt;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/bt;-><init>(Lcom/sec/chaton/trunk/TrunkPageActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->I:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 282
    new-instance v0, Lcom/sec/chaton/trunk/bu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/bu;-><init>(Lcom/sec/chaton/trunk/TrunkPageActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->J:Landroid/view/View$OnClickListener;

    .line 348
    new-instance v0, Lcom/sec/chaton/trunk/bv;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/bv;-><init>(Lcom/sec/chaton/trunk/TrunkPageActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->e:Lcom/sec/chaton/e/a/v;

    .line 629
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkPageActivity;)I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkPageActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->q:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkPageActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->j:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lcom/sec/chaton/trunk/bz;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1011
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->i:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1012
    iget-object v1, p1, Lcom/sec/chaton/trunk/bz;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1013
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x2

    aput-object v1, v2, v0

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1019
    :goto_0
    return-object v0

    .line 1014
    :catch_0
    move-exception v0

    .line 1015
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 1016
    sget-object v1, Lcom/sec/chaton/trunk/TrunkPageActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1019
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 494
    iput p1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    .line 498
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_4

    .line 500
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/bz;

    .line 501
    if-eqz v0, :cond_2

    .line 504
    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->H:Z

    if-eqz v1, :cond_5

    .line 505
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->z:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 510
    :goto_0
    iget v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->p:I

    add-int/lit8 v1, v1, -0x1

    if-ge p1, v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->H:Z

    if-eqz v1, :cond_6

    .line 511
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->A:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 534
    :goto_1
    iget v1, v0, Lcom/sec/chaton/trunk/bz;->j:I

    invoke-direct {p0, v1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->b(I)V

    .line 537
    iget v1, v0, Lcom/sec/chaton/trunk/bz;->m:I

    if-lez v1, :cond_7

    .line 538
    invoke-direct {p0, v4}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Z)V

    .line 544
    :goto_2
    iget-object v1, v0, Lcom/sec/chaton/trunk/bz;->l:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 545
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 546
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/trunk/bz;->l:Ljava/lang/String;

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v1, v0, v2}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 547
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->D:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 548
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->F:Ljava/lang/String;

    .line 549
    iput-boolean v4, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->E:Z

    .line 555
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 556
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    if-eqz v0, :cond_3

    .line 557
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->c()Z

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 558
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 560
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    if-eqz v0, :cond_4

    .line 561
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->d()Z

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 562
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 567
    :cond_4
    return-void

    .line 507
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->z:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 513
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->A:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 541
    :cond_7
    invoke-direct {p0, v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Z)V

    goto/16 :goto_2

    .line 551
    :cond_8
    iput-boolean v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->E:Z

    .line 552
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkPageActivity;I)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(I)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1162
    if-eqz p1, :cond_1

    .line 1163
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->v:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02043e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1169
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->s:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 1170
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1172
    :cond_0
    return-void

    .line 1166
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->v:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02043d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkPageActivity;I)I
    .locals 0

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->p:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->v:Landroid/content/Context;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 435
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/chaton/trunk/c/a;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->s:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 573
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/TrunkPageActivity;I)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->b(I)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->q:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/trunk/TrunkPageActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->g:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/chaton/trunk/TrunkPageActivity;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 474
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 475
    const-string v0, "showPasswordLockActivity"

    sget-object v1, Lcom/sec/chaton/trunk/TrunkPageActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 479
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 480
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 481
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 482
    invoke-virtual {p0, v1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->startActivity(Landroid/content/Intent;)V

    .line 484
    :cond_1
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 936
    new-instance v0, Lcom/sec/chaton/trunk/bw;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/bw;-><init>(Lcom/sec/chaton/trunk/TrunkPageActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->B:Landroid/content/BroadcastReceiver;

    .line 945
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 946
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 947
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 948
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 949
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->B:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 950
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->j()V

    .line 951
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/trunk/TrunkPageActivity;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->j()V

    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 954
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 955
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 956
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->finish()V

    .line 959
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/trunk/TrunkPageActivity;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->m()V

    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 962
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->B:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 963
    return-void
.end method

.method private m()V
    .locals 6

    .prologue
    const v2, 0x7f0b0166

    const/4 v4, 0x0

    .line 1092
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1093
    const v0, 0x7f0b002b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1111
    :cond_0
    :goto_0
    return-void

    .line 1100
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->m:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->m:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_4

    .line 1101
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->F:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    .line 1108
    :cond_3
    :goto_1
    if-eqz v4, :cond_0

    .line 1109
    const/4 v0, 0x2

    invoke-virtual {p0, v4, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1104
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->m:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_3

    .line 1105
    invoke-virtual {p0, v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->F:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2, v4}, Lcom/sec/chaton/util/ch;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 1074
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->finish()V

    .line 1076
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/c/g;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 967
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 969
    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-ne p1, v1, :cond_2

    .line 970
    invoke-static {p2}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 971
    const-class v1, Lcom/sec/vip/amschaton/AMSPlayerActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 972
    const-string v1, "AMS_FILE_PATH"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 973
    const-string v1, "VIEWER_MODE"

    const/16 v2, 0x3ea

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 998
    :cond_0
    :goto_0
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1005
    :cond_1
    :goto_1
    return-void

    .line 983
    :cond_2
    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne p1, v1, :cond_0

    .line 989
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 990
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 991
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "video/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 992
    const-string v1, "android.intent.extra.finishOnCompletion"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 999
    :catch_0
    move-exception v0

    .line 1000
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1001
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 1002
    sget-object v1, Lcom/sec/chaton/trunk/TrunkPageActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1025
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1066
    :goto_0
    return-void

    .line 1029
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1030
    const v0, 0x7f0b003d

    invoke-static {p0, v0, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1035
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1037
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01e2

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 1063
    :cond_2
    new-instance v0, Lcom/sec/chaton/multimedia/a/a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ChatON"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1, v3}, Lcom/sec/chaton/multimedia/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1064
    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 1177
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1178
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 1179
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->c()Z

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1180
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 1182
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 1183
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->d()Z

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1184
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 1187
    :cond_1
    return-void
.end method

.method public c()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/trunk/bz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 681
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public d()Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->g:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1080
    new-instance v0, Lcom/sec/chaton/trunk/bx;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/bx;-><init>(Lcom/sec/chaton/trunk/TrunkPageActivity;)V

    invoke-static {p0, v0}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 1088
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 1089
    return-void
.end method

.method protected f()V
    .locals 4

    .prologue
    const/4 v1, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1115
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->H:Z

    if-nez v0, :cond_2

    .line 1116
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 1117
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1118
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1123
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->w:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1125
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1126
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->A:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1128
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->E:Z

    if-eqz v0, :cond_1

    .line 1129
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->C:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1132
    :cond_1
    iput-boolean v3, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->H:Z

    .line 1157
    :goto_0
    return-void

    .line 1134
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    if-eqz v0, :cond_3

    .line 1135
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1136
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1140
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->w:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1142
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    if-lez v0, :cond_4

    .line 1143
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1146
    :cond_4
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    iget v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->p:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    .line 1147
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->A:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1151
    :cond_5
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->E:Z

    if-eqz v0, :cond_6

    .line 1152
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1155
    :cond_6
    iput-boolean v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->H:Z

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 319
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/base/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 321
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 325
    packed-switch p1, :pswitch_data_0

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 327
    :pswitch_0
    if-eqz p3, :cond_2

    .line 328
    const-string v0, "noUnread"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 330
    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/bz;

    iput v2, v0, Lcom/sec/chaton/trunk/bz;->m:I

    .line 334
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Z)V

    goto :goto_0

    .line 338
    :cond_2
    const-string v0, "Inform the item is deleted"

    sget-object v1, Lcom/sec/chaton/trunk/TrunkPageActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->finish()V

    goto :goto_0

    .line 325
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 232
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 235
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->D:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1}, Landroid/widget/TextView;->scrollTo(II)V

    .line 239
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 152
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 154
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/AMS/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/TrunkPageActivity;->a:Ljava/lang/String;

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/chaton/trunk/TrunkPageActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "amsuserfiles/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/TrunkPageActivity;->b:Ljava/lang/String;

    .line 157
    const v0, 0x7f0300b2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->setContentView(I)V

    .line 158
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->u:Lcom/sec/common/f/c;

    .line 159
    iput-object p0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->v:Landroid/content/Context;

    .line 160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    .line 162
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 171
    const v0, 0x7f070330

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->w:Landroid/view/View;

    .line 172
    const v0, 0x7f07032d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->C:Landroid/widget/LinearLayout;

    .line 173
    const v0, 0x7f07032e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->D:Landroid/widget/TextView;

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->w:Landroid/view/View;

    const v4, 0x7f07014c

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->s:Landroid/widget/TextView;

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->w:Landroid/view/View;

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->J:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    new-instance v0, Lcom/sec/chaton/trunk/by;

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-direct {v0, p0, v4}, Lcom/sec/chaton/trunk/by;-><init>(Lcom/sec/chaton/trunk/TrunkPageActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->k:Lcom/sec/chaton/trunk/by;

    .line 181
    const v0, 0x7f07032a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->g:Landroid/support/v4/view/ViewPager;

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->g:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->k:Lcom/sec/chaton/trunk/by;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->g:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->I:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 185
    const v0, 0x7f07032b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->z:Landroid/widget/ImageView;

    .line 186
    const v0, 0x7f07032c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->A:Landroid/widget/ImageView;

    .line 188
    if-eqz p1, :cond_4

    .line 190
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 191
    const-string v0, "[restore from onSaveInstanceState]"

    sget-object v3, Lcom/sec/chaton/trunk/TrunkPageActivity;->f:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :cond_0
    const-string v0, "itemId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->h:Ljava/lang/String;

    .line 194
    const-string v0, "inboxNo"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->i:Ljava/lang/String;

    .line 195
    const-string v0, "sessionId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->j:Ljava/lang/String;

    .line 196
    const-string v0, "isvalid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->d:Z

    .line 205
    :cond_1
    :goto_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ItemId: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", InboxNo: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "SessionId: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "isValid :"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->d:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/trunk/TrunkPageActivity;->f:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_2
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->e:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v3, v4}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->o:Lcom/sec/chaton/e/a/u;

    .line 212
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "trunkOrderingType"

    sget-object v4, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v4}, Lcom/sec/chaton/trunk/a/b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/trunk/a/b;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->n:Lcom/sec/chaton/trunk/a/b;

    .line 214
    sget-object v0, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->n:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/trunk/a/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->o:Lcom/sec/chaton/e/a/u;

    sget-object v3, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    const-string v5, "session_id=?"

    new-array v6, v1, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->j:Ljava/lang/String;

    aput-object v4, v6, v8

    const-string v7, "registration_time DESC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :goto_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->G:Landroid/widget/Toast;

    .line 224
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/common/actionbar/a;->b(Z)V

    .line 225
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/common/actionbar/a;->a(Z)V

    .line 226
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/common/actionbar/a;->c(Z)V

    .line 227
    return-void

    .line 154
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 198
    :cond_4
    if-eqz v3, :cond_1

    .line 199
    const-string v0, "itemId"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->h:Ljava/lang/String;

    .line 200
    const-string v0, "inboxNo"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->i:Ljava/lang/String;

    .line 201
    const-string v0, "sessionId"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->j:Ljava/lang/String;

    .line 202
    const-string v0, "isvalid"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->d:Z

    goto/16 :goto_1

    .line 219
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->o:Lcom/sec/chaton/e/a/u;

    sget-object v3, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    const-string v5, "session_id=?"

    new-array v6, v1, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->j:Ljava/lang/String;

    aput-object v4, v6, v8

    const-string v7, "last_comment_time DESC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->u:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 490
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onDestroy()V

    .line 491
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 469
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->l()V

    .line 470
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onPause()V

    .line 471
    return-void
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 440
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->i()V

    .line 441
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->h()V

    .line 442
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->o:Lcom/sec/chaton/e/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->o:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x2

    sget-object v3, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    const-string v5, "item_id=?"

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    iget v8, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-virtual {v4}, Lcom/sec/chaton/trunk/TrunkFullView;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onResume()V

    .line 449
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 454
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 455
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 456
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->c()Z

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 459
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    if-eqz v0, :cond_2

    .line 460
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->t:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->d()Z

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 461
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 465
    :cond_2
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 245
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/bz;

    .line 248
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/sec/chaton/trunk/bz;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 250
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 251
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onSaveInstanceState] ItemId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/trunk/bz;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", inboxNo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/trunk/TrunkPageActivity;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :cond_0
    const-string v1, "itemId"

    iget-object v0, v0, Lcom/sec/chaton/trunk/bz;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v0, "inboxNo"

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v0, "sessionId"

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string v0, "isvalid"

    iget-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 260
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 261
    return-void
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 722
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0031

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 724
    const v0, 0x7f07058c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->x:Landroid/view/MenuItem;

    .line 725
    const v0, 0x7f0705b2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->y:Landroid/view/MenuItem;

    .line 727
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(I)V

    .line 728
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 866
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 867
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->r:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/bz;

    .line 868
    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Lcom/sec/chaton/trunk/bz;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->l:Ljava/lang/String;

    .line 869
    iget-object v1, v0, Lcom/sec/chaton/trunk/bz;->i:Lcom/sec/chaton/trunk/c/g;

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->m:Lcom/sec/chaton/trunk/c/g;

    .line 871
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 914
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 873
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    .line 875
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->l:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 876
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 884
    :sswitch_1
    const-string v1, "true"

    iget-object v0, v0, Lcom/sec/chaton/trunk/bz;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkPageActivity;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 885
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "TrunkShareCheckPopup"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 886
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->e()V

    goto :goto_0

    .line 888
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->m()V

    goto :goto_0

    .line 891
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->m()V

    goto :goto_0

    .line 871
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f07058c -> :sswitch_0
        0x7f0705b2 -> :sswitch_1
    .end sparse-switch
.end method
