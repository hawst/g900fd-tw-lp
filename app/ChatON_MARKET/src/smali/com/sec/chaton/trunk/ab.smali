.class Lcom/sec/chaton/trunk/ab;
.super Landroid/os/Handler;
.source "TrunkCommentView.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkCommentView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkCommentView;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 127
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkCommentView;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_0

    .line 131
    const-string v0, "Fragment had been detached."

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkCommentView;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 139
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 141
    :pswitch_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_2

    .line 142
    const-string v1, "[TRUNK] Received METHOD_ADD_COMMENT"

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkCommentView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkCommentView;->a(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 146
    iget-object v1, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkCommentView;->a(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 149
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_5

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkCommentView;->b(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkCommentView;->c(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0170

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkCommentView;->d(Lcom/sec/chaton/trunk/TrunkCommentView;)Lcom/sec/chaton/trunk/aj;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkCommentView;->d(Lcom/sec/chaton/trunk/TrunkCommentView;)Lcom/sec/chaton/trunk/aj;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/chaton/trunk/aj;->a()V

    .line 159
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkCommentView;->a(Lcom/sec/chaton/trunk/TrunkCommentView;Z)Z

    goto :goto_0

    .line 160
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_0

    .line 161
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0x15d43

    if-ne v1, v2, :cond_6

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkCommentView;->e(Lcom/sec/chaton/trunk/TrunkCommentView;)V

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkCommentView;->a(Lcom/sec/chaton/trunk/TrunkCommentView;Z)Z

    goto :goto_0

    .line 167
    :cond_6
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0x13952

    if-ne v1, v2, :cond_7

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkCommentView;->c(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b016b

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkCommentView;->a(Lcom/sec/chaton/trunk/TrunkCommentView;Z)Z

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0, v3}, Lcom/sec/chaton/trunk/TrunkCommentView;->b(Lcom/sec/chaton/trunk/TrunkCommentView;Z)Z

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkCommentView;->f(Lcom/sec/chaton/trunk/TrunkCommentView;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 175
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/trunk/ab;->a:Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-static {v1, v0}, Lcom/sec/chaton/trunk/TrunkCommentView;->a(Lcom/sec/chaton/trunk/TrunkCommentView;Lcom/sec/chaton/a/a/f;)V

    goto/16 :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x388
        :pswitch_0
    .end packed-switch
.end method
