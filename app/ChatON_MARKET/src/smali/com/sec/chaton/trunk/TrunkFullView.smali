.class public Lcom/sec/chaton/trunk/TrunkFullView;
.super Landroid/support/v4/app/Fragment;
.source "TrunkFullView.java"

# interfaces
.implements Lcom/sec/chaton/multimedia/image/at;
.implements Lcom/sec/chaton/trunk/c/e;
.implements Lcom/sec/chaton/trunk/f;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private A:Lcom/sec/chaton/trunk/c/b;

.field private B:Lcom/sec/chaton/trunk/am;

.field private C:Lcom/sec/chaton/trunk/h;

.field private D:Lcom/sec/chaton/trunk/g;

.field private E:Lcom/sec/chaton/trunk/g;

.field private F:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private G:Lcom/sec/chaton/trunk/az;

.field private H:Landroid/view/ViewTreeObserver;

.field private I:Lcom/sec/common/f/c;

.field private J:Landroid/net/Uri;

.field private K:Z

.field private L:I

.field private M:Lcom/sec/chaton/multimedia/image/b;

.field private N:Lcom/sec/chaton/trunk/al;

.field private O:Landroid/os/Handler;

.field private P:Landroid/view/View$OnClickListener;

.field private Q:Landroid/os/Handler;

.field public a:Landroid/widget/ImageView;

.field private c:Landroid/app/Activity;

.field private d:Lcom/sec/chaton/trunk/ay;

.field private e:Landroid/widget/Toast;

.field private f:Landroid/widget/ProgressBar;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/FrameLayout;

.field private k:Landroid/app/Dialog;

.field private l:Landroid/app/Dialog;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Lcom/sec/chaton/trunk/c/g;

.field private q:Ljava/lang/String;

.field private r:I

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/TrunkFullView;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 217
    new-instance v0, Lcom/sec/chaton/trunk/ar;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/ar;-><init>(Lcom/sec/chaton/trunk/TrunkFullView;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->O:Landroid/os/Handler;

    .line 820
    new-instance v0, Lcom/sec/chaton/trunk/av;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/av;-><init>(Lcom/sec/chaton/trunk/TrunkFullView;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->P:Landroid/view/View$OnClickListener;

    .line 999
    new-instance v0, Lcom/sec/chaton/trunk/ax;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/ax;-><init>(Lcom/sec/chaton/trunk/TrunkFullView;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->Q:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkFullView;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->m()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkFullView;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->u:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkFullView;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->l()V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 842
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->w:Z

    if-eqz v0, :cond_0

    .line 851
    :goto_0
    return-void

    .line 846
    :cond_0
    if-eqz p1, :cond_1

    .line 847
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->i:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 849
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkFullView;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->w:Z

    return p1
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 831
    invoke-static {}, Lcom/sec/common/util/i;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 832
    if-eqz p1, :cond_0

    const-string v0, "thumbnail"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 833
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 834
    const/4 v0, 0x1

    .line 838
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/TrunkPageActivity;
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->h()Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/TrunkFullView;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->v:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/TrunkFullView;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->k:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/TrunkFullView;Z)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Z)V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/ay;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->d:Lcom/sec/chaton/trunk/ay;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/sec/chaton/trunk/TrunkFullView;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->n:Ljava/lang/String;

    return-object v0
.end method

.method private h()Lcom/sec/chaton/trunk/TrunkPageActivity;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->c:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/trunk/TrunkPageActivity;

    .line 367
    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/c/g;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 889
    invoke-static {}, Lcom/sec/chaton/util/ck;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 891
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->w:Z

    .line 894
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->j()V

    .line 896
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->E:Lcom/sec/chaton/trunk/g;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/g;->a()V

    .line 900
    :goto_0
    return-void

    .line 898
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01e2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/chaton/trunk/TrunkFullView;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->k()V

    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/trunk/TrunkFullView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 903
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->A:Lcom/sec/chaton/trunk/c/b;

    const-string v1, "content"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/c/b;->a(Ljava/lang/String;)Z

    .line 905
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->f:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 906
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 907
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/trunk/TrunkFullView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->g:Landroid/widget/ImageView;

    return-object v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->A:Lcom/sec/chaton/trunk/c/b;

    const-string v1, "content"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/c/b;->b(Ljava/lang/String;)Z

    .line 912
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->A:Lcom/sec/chaton/trunk/c/b;

    const-string v1, "content"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/c/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 913
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->f:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 916
    :cond_0
    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/az;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->G:Lcom/sec/chaton/trunk/az;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 919
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->e:Landroid/widget/Toast;

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 920
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->e:Landroid/widget/Toast;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V

    .line 921
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->e:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 922
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 944
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->l:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 945
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->h()Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0160

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/trunk/aw;

    invoke-direct {v2, p0}, Lcom/sec/chaton/trunk/aw;-><init>(Lcom/sec/chaton/trunk/TrunkFullView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->l:Landroid/app/Dialog;

    .line 961
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->l:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 965
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->l:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 966
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->l:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 968
    :cond_1
    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/trunk/TrunkFullView;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->w:Z

    return v0
.end method

.method static synthetic n(Lcom/sec/chaton/trunk/TrunkFullView;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->i()V

    return-void
.end method

.method static synthetic o(Lcom/sec/chaton/trunk/TrunkFullView;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/al;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->N:Lcom/sec/chaton/trunk/al;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->I:Lcom/sec/common/f/c;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->c:Landroid/app/Activity;

    return-object v0
.end method

.method public a(Lcom/sec/chaton/trunk/c/g;Ljava/io/File;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 631
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->v:Z

    if-eqz v0, :cond_1

    .line 818
    :cond_0
    :goto_0
    return-void

    .line 636
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->w:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->t:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->t:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 642
    :cond_2
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    .line 643
    iput-object p3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->t:Ljava/lang/String;

    .line 645
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    if-eqz v0, :cond_3

    .line 646
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->t:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 651
    :cond_3
    iput-boolean v5, p0, Lcom/sec/chaton/trunk/TrunkFullView;->x:Z

    .line 654
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->t:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->t:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 655
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->I:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 817
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->d:Lcom/sec/chaton/trunk/ay;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/ay;->b()V

    goto :goto_0

    .line 657
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_f

    .line 658
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->t:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    .line 660
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    if-nez v0, :cond_9

    .line 661
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 662
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_7

    .line 663
    new-array v0, v2, [Ljava/lang/Object;

    const-string v1, "For thumbnail image exists, show it. "

    aput-object v1, v0, v5

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/TrunkFullView;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->I:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 666
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/ad;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 673
    :cond_8
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->i()V

    goto :goto_1

    .line 675
    :cond_9
    iput-boolean v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->z:Z

    .line 676
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->t:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    .line 680
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_a

    .line 681
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "Is AMS image: "

    aput-object v2, v1, v5

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/trunk/TrunkFullView;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    :cond_a
    if-eqz v0, :cond_c

    .line 686
    invoke-direct {p0, v3}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Z)V

    .line 687
    iput-boolean v5, p0, Lcom/sec/chaton/trunk/TrunkFullView;->x:Z

    .line 689
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 690
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->F:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 692
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 693
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->P:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 695
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->i:Landroid/widget/ImageView;

    const v1, 0x7f02012b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 697
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 698
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->i:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/trunk/at;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/at;-><init>(Lcom/sec/chaton/trunk/TrunkFullView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 730
    :goto_2
    new-instance v0, Lcom/sec/chaton/trunk/al;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/trunk/al;-><init>(Landroid/net/Uri;Lcom/sec/chaton/trunk/c/g;Ljava/lang/Boolean;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->N:Lcom/sec/chaton/trunk/al;

    .line 732
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->M:Lcom/sec/chaton/multimedia/image/b;

    if-nez v0, :cond_e

    .line 733
    new-instance v0, Lcom/sec/chaton/multimedia/image/b;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->j:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkFullView;->Q:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/multimedia/image/b;-><init>(Ljava/lang/String;Landroid/view/ViewGroup;Landroid/view/View;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->M:Lcom/sec/chaton/multimedia/image/b;

    .line 734
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_d

    .line 735
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->M:Lcom/sec/chaton/multimedia/image/b;

    new-array v1, v5, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    .line 726
    :cond_c
    invoke-direct {p0, v5}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Z)V

    .line 727
    iput-boolean v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->x:Z

    goto :goto_2

    .line 737
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->M:Lcom/sec/chaton/multimedia/image/b;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v5, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/b;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    .line 741
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->I:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->N:Lcom/sec/chaton/trunk/al;

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_1

    .line 746
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    sget-object v1, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne v0, v1, :cond_5

    .line 748
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 749
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->F:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 752
    :cond_10
    invoke-direct {p0, v3}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Z)V

    .line 753
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 754
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->P:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 756
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->t:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    .line 761
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    if-nez v0, :cond_11

    .line 763
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    const v1, 0x7f020449

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 765
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->I:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 775
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->i:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/trunk/au;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/au;-><init>(Lcom/sec/chaton/trunk/TrunkFullView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 767
    :cond_11
    iput-boolean v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->z:Z

    .line 768
    iput-boolean v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->x:Z

    .line 770
    new-instance v0, Lcom/sec/chaton/trunk/al;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->J:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkFullView;->i:Landroid/widget/ImageView;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/trunk/al;-><init>(Landroid/net/Uri;Lcom/sec/chaton/trunk/c/g;Ljava/lang/Boolean;Landroid/widget/ImageView;)V

    .line 772
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->I:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_3
.end method

.method public a(Lcom/sec/chaton/trunk/g;)V
    .locals 0

    .prologue
    .line 860
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->D:Lcom/sec/chaton/trunk/g;

    .line 861
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/h;)V
    .locals 0

    .prologue
    .line 855
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->C:Lcom/sec/chaton/trunk/h;

    .line 856
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 619
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->s:Ljava/lang/String;

    .line 621
    const-string v0, "ME"

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->K:Z

    if-eqz v0, :cond_0

    .line 622
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->y:Z

    .line 627
    :goto_0
    return-void

    .line 624
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->y:Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 870
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_0

    .line 871
    const-string v0, "Storage state is changed. finish activity."

    sget-object v1, Lcom/sec/chaton/trunk/TrunkFullView;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->d:Lcom/sec/chaton/trunk/ay;

    if-eqz v0, :cond_1

    .line 875
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->d:Lcom/sec/chaton/trunk/ay;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/ay;->a()V

    .line 877
    :cond_1
    return-void
.end method

.method public b()Lcom/sec/chaton/trunk/az;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->G:Lcom/sec/chaton/trunk/az;

    return-object v0
.end method

.method public b(Lcom/sec/chaton/trunk/g;)V
    .locals 0

    .prologue
    .line 865
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->E:Lcom/sec/chaton/trunk/g;

    .line 866
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 971
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->x:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 975
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->z:Z

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 987
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->o:Ljava/lang/String;

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 992
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->h()Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 993
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->b()Lcom/sec/chaton/trunk/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 994
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->h()Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->f()V

    .line 997
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 372
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 374
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->c:Landroid/app/Activity;

    .line 377
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->c:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/trunk/ay;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->d:Lcom/sec/chaton/trunk/ay;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    return-void

    .line 378
    :catch_0
    move-exception v0

    .line 379
    new-instance v0, Ljava/lang/ClassCastException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, " must implement ITrunkFullViewListener."

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 394
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "position"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->L:I

    .line 396
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 402
    if-eqz p1, :cond_2

    .line 404
    const-string v0, "sessionId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->m:Ljava/lang/String;

    .line 405
    const-string v0, "inboxNo"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->n:Ljava/lang/String;

    .line 406
    const-string v0, "itemId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->o:Ljava/lang/String;

    .line 407
    const-string v0, "downloadUrl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->q:Ljava/lang/String;

    .line 408
    const-string v0, "contentType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/c/f;->a(Ljava/lang/String;Z)Lcom/sec/chaton/trunk/c/g;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    .line 409
    const-string v0, "totalcomment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->r:I

    .line 410
    const-string v0, "isvalid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->K:Z

    .line 412
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 414
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 415
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sessionId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", inboxNo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", itemId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", contentType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", downloadUri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", totalCommentCount"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->r:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isvalid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->K:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[restore from onSaveInstanceState]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/trunk/TrunkFullView;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    :cond_0
    :goto_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->e:Landroid/widget/Toast;

    .line 451
    new-instance v0, Lcom/sec/chaton/trunk/c/b;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/c/b;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->A:Lcom/sec/chaton/trunk/c/b;

    .line 453
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->I:Lcom/sec/common/f/c;

    .line 455
    new-instance v0, Lcom/sec/chaton/trunk/az;

    invoke-direct {v0, p0, p0}, Lcom/sec/chaton/trunk/az;-><init>(Lcom/sec/chaton/trunk/TrunkFullView;Lcom/sec/chaton/multimedia/image/at;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->G:Lcom/sec/chaton/trunk/az;

    .line 456
    return-void

    :cond_1
    move v0, v1

    .line 394
    goto/16 :goto_0

    .line 429
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->h()Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->h()Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->L:I

    if-le v0, v2, :cond_0

    .line 430
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkFullView;->h()Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->c()Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->L:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/bz;

    .line 432
    if-eqz v0, :cond_0

    .line 434
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_3

    .line 435
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Item]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/bz;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/TrunkFullView;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :cond_3
    iget-object v2, v0, Lcom/sec/chaton/trunk/bz;->a:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->m:Ljava/lang/String;

    .line 439
    iget-object v2, v0, Lcom/sec/chaton/trunk/bz;->b:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->n:Ljava/lang/String;

    .line 440
    iget-object v2, v0, Lcom/sec/chaton/trunk/bz;->f:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->o:Ljava/lang/String;

    .line 441
    iget-object v2, v0, Lcom/sec/chaton/trunk/bz;->h:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->q:Ljava/lang/String;

    .line 442
    iget-object v2, v0, Lcom/sec/chaton/trunk/bz;->i:Lcom/sec/chaton/trunk/c/g;

    iput-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    .line 443
    iget v2, v0, Lcom/sec/chaton/trunk/bz;->j:I

    iput v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->r:I

    .line 444
    iget-boolean v0, v0, Lcom/sec/chaton/trunk/bz;->k:Z

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->K:Z

    goto/16 :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 514
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 516
    const v0, 0x7f030113

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 518
    const v0, 0x7f070484

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->j:Landroid/widget/FrameLayout;

    .line 519
    const v0, 0x7f070485

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    .line 520
    const v0, 0x7f070486

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->g:Landroid/widget/ImageView;

    .line 521
    const v0, 0x7f070487

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->h:Landroid/widget/ImageView;

    .line 523
    const v0, 0x7f070488

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->i:Landroid/widget/ImageView;

    .line 524
    const v0, 0x7f070489

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->f:Landroid/widget/ProgressBar;

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->G:Lcom/sec/chaton/trunk/az;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 532
    invoke-direct {p0, v2}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Z)V

    .line 534
    new-instance v0, Lcom/sec/chaton/trunk/am;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->O:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkFullView;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkFullView;->m:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/trunk/TrunkFullView;->o:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/trunk/am;-><init>(Lcom/sec/chaton/trunk/f;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->B:Lcom/sec/chaton/trunk/am;

    .line 535
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->B:Lcom/sec/chaton/trunk/am;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/am;->a()V

    .line 537
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->C:Lcom/sec/chaton/trunk/h;

    const/16 v1, 0x64

    const/16 v2, 0xef

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/h;->a(II)V

    .line 541
    return-object v6
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 607
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 609
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->A:Lcom/sec/chaton/trunk/c/b;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/c/b;->a()V

    .line 610
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 596
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 598
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->B:Lcom/sec/chaton/trunk/am;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/am;->d()V

    .line 600
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->I:Lcom/sec/common/f/c;

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->I:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 603
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 385
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 387
    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->c:Landroid/app/Activity;

    .line 388
    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->d:Lcom/sec/chaton/trunk/ay;

    .line 389
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 578
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 580
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->F:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 584
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->B:Lcom/sec/chaton/trunk/am;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/am;->c()V

    .line 585
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 558
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 561
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->a:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    .line 563
    new-instance v0, Lcom/sec/chaton/trunk/as;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/as;-><init>(Lcom/sec/chaton/trunk/TrunkFullView;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->F:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 569
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->H:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->F:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkFullView;->B:Lcom/sec/chaton/trunk/am;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/am;->b()V

    .line 574
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 484
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 486
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 487
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sessionId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", inboxNo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", itemId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 490
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", contentType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", downloadUri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", totalCommentCount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->r:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 495
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isvalid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/chaton/trunk/TrunkFullView;->K:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 498
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onSaveInstanceState]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/TrunkFullView;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    :cond_0
    const-string v0, "sessionId"

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    const-string v0, "inboxNo"

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const-string v0, "itemId"

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    const-string v0, "downloadUrl"

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    const-string v0, "contentType"

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->p:Lcom/sec/chaton/trunk/c/g;

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/c/g;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    const-string v0, "totalcomment"

    iget v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->r:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 507
    const-string v0, "isvalid"

    iget-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkFullView;->K:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 509
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 510
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 551
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 553
    invoke-static {p0}, Lcom/sec/chaton/trunk/c/c;->a(Lcom/sec/chaton/trunk/c/e;)V

    .line 554
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 589
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 591
    invoke-static {p0}, Lcom/sec/chaton/trunk/c/c;->b(Lcom/sec/chaton/trunk/c/e;)V

    .line 592
    return-void
.end method
