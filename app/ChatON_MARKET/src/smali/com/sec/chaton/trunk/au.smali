.class Lcom/sec/chaton/trunk/au;
.super Ljava/lang/Object;
.source "TrunkFullView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkFullView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/TrunkFullView;)V
    .locals 0

    .prologue
    .line 775
    iput-object p1, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 780
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 812
    :cond_0
    :goto_0
    return-void

    .line 786
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->m(Lcom/sec/chaton/trunk/TrunkFullView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 793
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 794
    iget-object v0, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->c(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/TrunkPageActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    const v2, 0x7f0b002b

    invoke-virtual {v1, v2}, Lcom/sec/chaton/trunk/TrunkFullView;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 799
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_5

    .line 801
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 802
    const/4 v1, -0x3

    if-eq v1, v0, :cond_3

    const/4 v1, -0x2

    if-ne v1, v0, :cond_4

    .line 803
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->b(Lcom/sec/chaton/trunk/TrunkFullView;)V

    goto :goto_0

    .line 805
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->n(Lcom/sec/chaton/trunk/TrunkFullView;)V

    goto :goto_0

    .line 808
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->e(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/ay;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkFullView;->e(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/ay;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkFullView;->h(Lcom/sec/chaton/trunk/TrunkFullView;)Lcom/sec/chaton/trunk/c/g;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/trunk/au;->a:Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-static {v2}, Lcom/sec/chaton/trunk/TrunkFullView;->f(Lcom/sec/chaton/trunk/TrunkFullView;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/ay;->a(Lcom/sec/chaton/trunk/c/g;Ljava/lang/String;)V

    goto :goto_0
.end method
