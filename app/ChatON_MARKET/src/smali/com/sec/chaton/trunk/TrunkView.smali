.class public Lcom/sec/chaton/trunk/TrunkView;
.super Lcom/sec/chaton/trunk/ITrunkView;
.source "TrunkView.java"

# interfaces
.implements Lcom/sec/chaton/trunk/q;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/LinearLayout;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Landroid/widget/Toast;

.field private E:Lcom/sec/common/f/c;

.field private F:Landroid/widget/CheckedTextView;

.field private G:I

.field private H:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private I:Landroid/app/Dialog;

.field private J:Landroid/view/Menu;

.field private K:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private L:Z

.field private M:Landroid/widget/ImageView;

.field private N:Landroid/widget/TextView;

.field private O:Landroid/widget/TextView;

.field private P:Landroid/widget/LinearLayout;

.field private Q:Landroid/widget/ImageView;

.field private R:Z

.field private S:Z

.field private T:Lcom/sec/common/a/d;

.field private U:Landroid/os/Handler;

.field a:Z

.field public b:Z

.field private d:Lcom/sec/chaton/trunk/p;

.field private e:Lcom/sec/chaton/trunk/n;

.field private f:Lcom/sec/chaton/trunk/o;

.field private g:Landroid/app/Activity;

.field private h:Lcom/sec/chaton/trunk/co;

.field private i:Landroid/content/Intent;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Landroid/widget/ListView;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/Spinner;

.field private s:Lcom/sec/chaton/trunk/r;

.field private t:Lcom/sec/chaton/trunk/a/b;

.field private u:Z

.field private v:Lcom/sec/chaton/trunk/cq;

.field private w:Lcom/sec/chaton/trunk/ca;

.field private x:Ljava/lang/Object;

.field private y:Ljava/lang/Object;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const-class v0, Lcom/sec/chaton/trunk/TrunkView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ITrunkView;-><init>()V

    .line 203
    iput-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkView;->b:Z

    .line 206
    iput-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkView;->C:Z

    .line 223
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->K:Ljava/util/ArrayList;

    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->R:Z

    .line 239
    iput-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkView;->S:Z

    .line 246
    new-instance v0, Lcom/sec/chaton/trunk/cg;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/cg;-><init>(Lcom/sec/chaton/trunk/TrunkView;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->U:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkView;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkView;->x:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkView;->B:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkView;Lcom/sec/chaton/trunk/cp;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cp;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkView;Lcom/sec/chaton/trunk/cq;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cq;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkView;ZZLcom/sec/chaton/trunk/a/b;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/trunk/TrunkView;->a(ZZLcom/sec/chaton/trunk/a/b;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/trunk/cp;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 996
    sget-object v0, Lcom/sec/chaton/trunk/cn;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/trunk/cp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1010
    :goto_0
    return-void

    .line 998
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->D:Landroid/widget/Toast;

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 999
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->D:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 1000
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->D:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1004
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->D:Landroid/widget/Toast;

    const v1, 0x7f0b002a

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 1005
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->D:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 1006
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->D:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 996
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/sec/chaton/trunk/cq;)V
    .locals 8

    .prologue
    const v7, 0x7f0705ba

    const v6, 0x7f0705b7

    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1242
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkView;->v:Lcom/sec/chaton/trunk/cq;

    .line 1244
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->x:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1354
    :goto_0
    return-void

    .line 1248
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 1249
    const-string v0, "Switch view to %s"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->v:Lcom/sec/chaton/trunk/cq;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    :cond_1
    sget-object v0, Lcom/sec/chaton/trunk/cn;->b:[I

    invoke-virtual {p1}, Lcom/sec/chaton/trunk/cq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1255
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->q:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 1256
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1258
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1259
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->o:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1260
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->n:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1261
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1266
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->q:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    .line 1267
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1269
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1270
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1271
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1272
    const/4 v1, -0x3

    if-eq v1, v0, :cond_4

    const/4 v1, -0x2

    if-ne v1, v0, :cond_5

    .line 1273
    :cond_4
    sget-object v0, Lcom/sec/chaton/trunk/cp;->a:Lcom/sec/chaton/trunk/cp;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cp;)V

    .line 1277
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->n:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1278
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1275
    :cond_5
    sget-object v0, Lcom/sec/chaton/trunk/cp;->b:Lcom/sec/chaton/trunk/cp;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cp;)V

    goto :goto_1

    .line 1283
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->q:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_6

    .line 1284
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1286
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1287
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->o:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1288
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->n:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1289
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1294
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_7

    .line 1295
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v6, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1296
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v7, v5}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1300
    :cond_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1301
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/TrunkActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 1305
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 1307
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkView;->G:I

    if-ge v0, v5, :cond_b

    .line 1308
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 1309
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 1320
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    iput-boolean v5, v0, Lcom/sec/chaton/trunk/r;->a:Z

    .line 1321
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->L:Z

    if-eqz v0, :cond_9

    .line 1322
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1325
    :cond_9
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->S:Z

    if-eqz v0, :cond_a

    .line 1327
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->c()V

    .line 1329
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    goto/16 :goto_0

    .line 1311
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/chaton/trunk/TrunkView;->G:I

    if-ne v0, v1, :cond_c

    .line 1312
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 1313
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_2

    .line 1316
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 1317
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_2

    .line 1334
    :pswitch_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_d

    .line 1335
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v6, v5}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1336
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v7, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1339
    :cond_d
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_f

    .line 1340
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/TrunkActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b015b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 1348
    :cond_e
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 1349
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    iput-boolean v3, v0, Lcom/sec/chaton/trunk/r;->a:Z

    .line 1350
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    goto/16 :goto_0

    .line 1342
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_e

    .line 1343
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    const v1, 0x7f07058f

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3

    .line 1252
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1199
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1209
    :goto_0
    return-void

    .line 1203
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->A:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1205
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->e:Lcom/sec/chaton/trunk/n;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    const/16 v2, 0x1f

    const/16 v3, 0xef

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/sec/chaton/trunk/n;->a(Lcom/sec/chaton/trunk/a/b;IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->x:Ljava/lang/Object;

    .line 1207
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->h()V

    goto :goto_0
.end method

.method private a(ZZLcom/sec/chaton/trunk/a/b;)V
    .locals 7

    .prologue
    const/16 v1, 0x64

    const/16 v0, 0x1e

    const v6, 0x7f07058f

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1130
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->i()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1190
    :goto_0
    return-void

    .line 1134
    :cond_0
    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    if-eq p3, v3, :cond_1

    .line 1135
    iput-boolean v5, p0, Lcom/sec/chaton/trunk/TrunkView;->u:Z

    .line 1140
    :cond_1
    iput-boolean p2, p0, Lcom/sec/chaton/trunk/TrunkView;->R:Z

    .line 1142
    sget-object v3, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    if-ne p3, v3, :cond_5

    .line 1143
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 1144
    const-string v1, "Request type is recent. Load item count: %d"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->d:Lcom/sec/chaton/trunk/p;

    const/16 v3, 0xef

    invoke-interface {v1, p1, p3, v0, v3}, Lcom/sec/chaton/trunk/p;->a(ZLcom/sec/chaton/trunk/a/b;II)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->x:Ljava/lang/Object;

    .line 1158
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 1159
    iput-boolean v5, p0, Lcom/sec/chaton/trunk/TrunkView;->R:Z

    .line 1162
    :cond_3
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->R:Z

    if-eqz v0, :cond_7

    .line 1163
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->f()V

    .line 1170
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_4

    .line 1171
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1172
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 1189
    :cond_4
    :goto_2
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->h()V

    goto :goto_0

    .line 1148
    :cond_5
    sget-object v0, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    if-ne p3, v0, :cond_9

    .line 1149
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_6

    .line 1150
    const-string v0, "Request type is unread comment. Load item count: %d"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move v0, v1

    .line 1153
    goto :goto_1

    .line 1177
    :cond_7
    invoke-virtual {p0, v5}, Lcom/sec/chaton/trunk/TrunkView;->a(Z)V

    .line 1178
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_8

    .line 1179
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1180
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 1185
    :cond_8
    invoke-direct {p0, v2}, Lcom/sec/chaton/trunk/TrunkView;->b(Z)V

    goto :goto_2

    :cond_9
    move v0, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkView;)Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->u:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/TrunkView;Z)Z
    .locals 0

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkView;->C:Z

    return p1
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkView;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->e()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkView;->a(Ljava/lang/String;)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 1487
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1489
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->Q:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1490
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->Q:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1493
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/TrunkView;Z)Z
    .locals 0

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkView;->u:Z

    return p1
.end method

.method private c()V
    .locals 3

    .prologue
    .line 836
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->T:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->T:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 871
    :goto_0
    return-void

    .line 840
    :cond_0
    const v0, 0x7f0b000f

    .line 842
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 843
    const v0, 0x7f0b0161

    .line 846
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/trunk/cl;

    invoke-direct {v2, p0}, Lcom/sec/chaton/trunk/cl;-><init>(Lcom/sec/chaton/trunk/TrunkView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->T:Lcom/sec/common/a/d;

    .line 862
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->T:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/trunk/cm;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/cm;-><init>(Lcom/sec/chaton/trunk/TrunkView;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 870
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->T:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/TrunkView;)Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->C:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/TrunkView;Z)Z
    .locals 0

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkView;->L:Z

    return p1
.end method

.method private d()I
    .locals 2

    .prologue
    .line 874
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 875
    const v0, 0x7f0b023d

    .line 878
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0b03f6

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/TrunkView;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->x:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/TrunkView;Z)Z
    .locals 0

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkView;->l:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/TrunkView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->B:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    sget-object v1, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    if-ne v0, v1, :cond_2

    .line 1111
    sget-object v0, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    .line 1116
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 1117
    const-string v0, "Toggle ordering type to %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "trunkOrderingType"

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/trunk/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    return-void

    .line 1112
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    sget-object v1, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    if-ne v0, v1, :cond_0

    .line 1113
    sget-object v0, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/TrunkView;Z)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/chaton/trunk/TrunkView;->b(Z)V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1212
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1213
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1216
    invoke-virtual {p0, v2}, Lcom/sec/chaton/trunk/TrunkView;->a(Z)V

    .line 1218
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->q:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 1219
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1221
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1222
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1223
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1224
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/TrunkView;Z)Z
    .locals 0

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/TrunkView;->S:Z

    return p1
.end method

.method private g()V
    .locals 4

    .prologue
    .line 1227
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1228
    const-string v0, "Hide trunk item list loading view. change view to %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkView;->v:Lcom/sec/chaton/trunk/cq;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1231
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1233
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->v:Lcom/sec/chaton/trunk/cq;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cq;)V

    .line 1234
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/trunk/TrunkView;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->h()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->r:Landroid/widget/Spinner;

    return-object v0
.end method

.method private h()V
    .locals 5

    .prologue
    const v4, 0x7f0705b9

    const/4 v3, 0x1

    const v2, 0x7f0705b8

    const/4 v1, 0x0

    .line 1361
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->x:Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 1370
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_1

    .line 1371
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1374
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_2

    .line 1376
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1394
    :cond_2
    :goto_0
    return-void

    .line 1381
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_4

    .line 1383
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->getCount()I

    move-result v0

    if-eqz v0, :cond_5

    .line 1384
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1390
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_2

    .line 1391
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 1386
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method static synthetic i(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/a/b;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    return-object v0
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 1404
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1405
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 1406
    const-string v0, "SessionId is invalid."

    sget-object v1, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1409
    :cond_1
    const/4 v0, 0x0

    .line 1412
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic j(Lcom/sec/chaton/trunk/TrunkView;)Lcom/sec/chaton/trunk/r;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1437
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->I:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 1438
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->g:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->I:Landroid/app/Dialog;

    .line 1440
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->I:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1443
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->I:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 1444
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->I:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1446
    :cond_1
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/trunk/TrunkView;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->I:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/trunk/TrunkView;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/trunk/TrunkView;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/trunk/TrunkView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->P:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/trunk/TrunkView;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;Lcom/sec/chaton/trunk/a/b;)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4022000000000000L    # 9.0

    const v7, 0x7f07058f

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 885
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 886
    const-string v0, "SetTrunkItemList( OrderingType: %s )"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p2, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->x:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 893
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->g()V

    .line 895
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->A:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 898
    invoke-virtual {p0, v1}, Lcom/sec/chaton/trunk/TrunkView;->a(Z)V

    .line 901
    :cond_1
    iput-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkView;->b:Z

    .line 903
    iput v1, p0, Lcom/sec/chaton/trunk/TrunkView;->G:I

    .line 905
    if-eqz p1, :cond_8

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_8

    .line 906
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->K:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 909
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->l:Z

    if-eqz v0, :cond_3

    .line 910
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_2

    .line 911
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 912
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 917
    :cond_2
    invoke-direct {p0, v6}, Lcom/sec/chaton/trunk/TrunkView;->b(Z)V

    .line 920
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToLast()Z

    .line 921
    const-string v0, "item_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->B:Ljava/lang/String;

    .line 923
    const-string v0, "item_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/trunk/database/d;->a(I)Lcom/sec/chaton/trunk/database/d;

    move-result-object v0

    .line 924
    sget-object v2, Lcom/sec/chaton/trunk/database/d;->b:Lcom/sec/chaton/trunk/database/d;

    if-ne v0, v2, :cond_6

    .line 925
    iput-boolean v6, p0, Lcom/sec/chaton/trunk/TrunkView;->b:Z

    .line 930
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 933
    :cond_4
    const-string v0, "sender_uid"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 934
    const-string v2, "ME"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 935
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkView;->G:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/trunk/TrunkView;->G:I

    .line 937
    :cond_5
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 939
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move v0, v1

    .line 942
    :goto_1
    int-to-double v2, v0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-gez v2, :cond_7

    .line 943
    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->K:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 944
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "trunkDataList index add :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    div-int/lit8 v3, v3, 0x9

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 927
    :cond_6
    iput-boolean v1, p0, Lcom/sec/chaton/trunk/TrunkView;->b:Z

    goto :goto_0

    .line 946
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trunkDataList index add :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trunkDataList index cursor size :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trunkDataList index size :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->K:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    :cond_8
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->L:Z

    if-eqz v0, :cond_9

    .line 954
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/r;->a(Ljava/util/HashMap;)V

    .line 957
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/trunk/r;->a(Lcom/sec/chaton/trunk/a/b;)V

    .line 958
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/trunk/r;->a(Landroid/database/Cursor;)V

    .line 959
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->notifyDataSetChanged()V

    .line 961
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->h()V

    .line 963
    if-eqz p1, :cond_a

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_e

    .line 964
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->v:Lcom/sec/chaton/trunk/cq;

    sget-object v2, Lcom/sec/chaton/trunk/cq;->c:Lcom/sec/chaton/trunk/cq;

    if-ne v0, v2, :cond_d

    .line 965
    sget-object v0, Lcom/sec/chaton/trunk/cq;->c:Lcom/sec/chaton/trunk/cq;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cq;)V

    .line 969
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_b

    .line 970
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 971
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 976
    :cond_b
    invoke-direct {p0, v1}, Lcom/sec/chaton/trunk/TrunkView;->b(Z)V

    .line 987
    :goto_3
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->z:Z

    if-nez v0, :cond_c

    .line 988
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    invoke-direct {p0, v1, v1, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(ZZLcom/sec/chaton/trunk/a/b;)V

    .line 990
    iput-boolean v6, p0, Lcom/sec/chaton/trunk/TrunkView;->z:Z

    .line 993
    :cond_c
    return-void

    .line 967
    :cond_d
    sget-object v0, Lcom/sec/chaton/trunk/cq;->b:Lcom/sec/chaton/trunk/cq;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cq;)V

    goto :goto_2

    .line 980
    :cond_e
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->L:Z

    if-eqz v0, :cond_f

    .line 981
    sget-object v0, Lcom/sec/chaton/trunk/cq;->d:Lcom/sec/chaton/trunk/cq;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cq;)V

    goto :goto_3

    .line 983
    :cond_f
    sget-object v0, Lcom/sec/chaton/trunk/cq;->a:Lcom/sec/chaton/trunk/cq;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cq;)V

    goto :goto_3
.end method

.method public a(Lcom/sec/chaton/trunk/n;)V
    .locals 0

    .prologue
    .line 1020
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkView;->e:Lcom/sec/chaton/trunk/n;

    .line 1021
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/o;)V
    .locals 0

    .prologue
    .line 1025
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkView;->f:Lcom/sec/chaton/trunk/o;

    .line 1026
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/p;)V
    .locals 0

    .prologue
    .line 1015
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkView;->d:Lcom/sec/chaton/trunk/p;

    .line 1016
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/v;)V
    .locals 8

    .prologue
    const v4, 0x7f070592

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1065
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->v:Lcom/sec/chaton/trunk/cq;

    sget-object v1, Lcom/sec/chaton/trunk/cq;->d:Lcom/sec/chaton/trunk/cq;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    iget-boolean v0, v0, Lcom/sec/chaton/trunk/r;->a:Z

    if-eqz v0, :cond_4

    .line 1067
    :cond_0
    const-string v0, "ME"

    iget-object v1, p1, Lcom/sec/chaton/trunk/v;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1068
    iget-object v0, p1, Lcom/sec/chaton/trunk/v;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1069
    iget-object v0, p1, Lcom/sec/chaton/trunk/v;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1070
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 1075
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    iget-object v1, p1, Lcom/sec/chaton/trunk/v;->j:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/chaton/trunk/r;->a(Lcom/sec/chaton/trunk/v;Z)V

    .line 1077
    iget v0, p0, Lcom/sec/chaton/trunk/TrunkView;->G:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/chaton/trunk/TrunkView;->G:I

    if-ne v0, v1, :cond_1

    .line 1078
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 1082
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1083
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1084
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 1088
    invoke-direct {p0, v2}, Lcom/sec/chaton/trunk/TrunkView;->b(Z)V

    .line 1104
    :goto_1
    return-void

    .line 1072
    :cond_2
    iget-object v0, p1, Lcom/sec/chaton/trunk/v;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 1091
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1092
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 1096
    invoke-direct {p0, v3}, Lcom/sec/chaton/trunk/TrunkView;->b(Z)V

    goto :goto_1

    .line 1100
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->h:Lcom/sec/chaton/trunk/co;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/chaton/trunk/v;->a:Ljava/lang/String;

    iget-object v4, p1, Lcom/sec/chaton/trunk/v;->h:Ljava/lang/String;

    iget-object v5, p1, Lcom/sec/chaton/trunk/v;->g:Lcom/sec/chaton/trunk/c/g;

    iget v6, p1, Lcom/sec/chaton/trunk/v;->c:I

    iget-boolean v7, p0, Lcom/sec/chaton/trunk/TrunkView;->l:Z

    invoke-interface/range {v0 .. v7}, Lcom/sec/chaton/trunk/co;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/trunk/c/g;IZ)V

    goto :goto_1
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1418
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->j()V

    .line 1420
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1422
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1423
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1424
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1425
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1426
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1430
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->f:Lcom/sec/chaton/trunk/o;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->j:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lcom/sec/chaton/trunk/o;->a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->y:Ljava/lang/Object;

    .line 1431
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const v2, 0x7f07058f

    .line 668
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 670
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 672
    if-eqz p1, :cond_1

    .line 673
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0300e3

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setActionView(I)Landroid/view/MenuItem;

    .line 680
    :cond_0
    :goto_0
    return-void

    .line 675
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1451
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->v:Lcom/sec/chaton/trunk/cq;

    sget-object v2, Lcom/sec/chaton/trunk/cq;->d:Lcom/sec/chaton/trunk/cq;

    if-ne v1, v2, :cond_0

    .line 1452
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/r;->b()V

    .line 1453
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v1, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 1454
    sget-object v0, Lcom/sec/chaton/trunk/cq;->e:Lcom/sec/chaton/trunk/cq;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cq;)V

    .line 1455
    const/4 v0, 0x1

    .line 1458
    :cond_0
    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 414
    invoke-super {p0, p1}, Lcom/sec/chaton/trunk/ITrunkView;->onAttach(Landroid/app/Activity;)V

    .line 416
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkView;->g:Landroid/app/Activity;

    .line 419
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->g:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/trunk/co;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->h:Lcom/sec/chaton/trunk/co;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 423
    return-void

    .line 420
    :catch_0
    move-exception v0

    .line 421
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "%s must implement ITrunkViewListener."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 435
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 436
    const-string v0, "TrunkView.onCreate()"

    sget-object v1, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/trunk/ITrunkView;->onCreate(Landroid/os/Bundle;)V

    .line 441
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->D:Landroid/widget/Toast;

    .line 443
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6

    .prologue
    const v1, 0x7f0705b7

    const v5, 0x7f070592

    const v4, 0x7f07058f

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 685
    const v0, 0x7f0f0036

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 687
    iput-object p1, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    .line 688
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->L:Z

    if-eqz v0, :cond_3

    .line 689
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v1, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 690
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    const v1, 0x7f0705ba

    invoke-interface {v0, v1, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 692
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 694
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 711
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 716
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->R:Z

    if-nez v0, :cond_2

    .line 717
    invoke-virtual {p0, v3}, Lcom/sec/chaton/trunk/TrunkView;->a(Z)V

    .line 720
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/trunk/ITrunkView;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 721
    return-void

    .line 700
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v1, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 701
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    const v1, 0x7f0705ba

    invoke-interface {v0, v1, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 703
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 704
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 707
    invoke-direct {p0, v2}, Lcom/sec/chaton/trunk/TrunkView;->b(Z)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    .line 447
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 448
    const-string v0, "TrunkView.onCreateView()"

    sget-object v1, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->a:Z

    .line 452
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->z:Z

    .line 453
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->C:Z

    .line 454
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->L:Z

    .line 456
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->E:Lcom/sec/common/f/c;

    .line 459
    if-eqz p3, :cond_1

    .line 460
    const-string v0, "checkedItem"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    .line 461
    const-string v0, "isRefreshInDelete"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->L:Z

    .line 462
    const-string v0, "onPopup"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->S:Z

    .line 466
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->setHasOptionsMenu(Z)V

    .line 468
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->i:Landroid/content/Intent;

    .line 471
    const v0, 0x7f03010b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 474
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 475
    const v0, 0x7f070118

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->P:Landroid/widget/LinearLayout;

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->P:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 477
    const v0, 0x7f07011e

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->N:Landroid/widget/TextView;

    .line 478
    const v0, 0x7f070120

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->O:Landroid/widget/TextView;

    .line 479
    const v0, 0x7f07011a

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->M:Landroid/widget/ImageView;

    .line 481
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->N:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "mTitle"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->O:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "mSubtitle"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 483
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->M:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "imageProfile"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 485
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "sessionId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->j:Ljava/lang/String;

    .line 486
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "inboxNO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->k:Ljava/lang/String;

    .line 487
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "isValid"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->l:Z

    .line 489
    const v0, 0x7f070121

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->Q:Landroid/widget/ImageView;

    .line 490
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->Q:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/trunk/ci;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/ci;-><init>(Lcom/sec/chaton/trunk/TrunkView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 518
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "trunkOrderingType"

    sget-object v2, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/trunk/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/trunk/a/b;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    .line 520
    new-instance v0, Lcom/sec/chaton/trunk/ca;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->U:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkView;->j:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/chaton/trunk/ca;-><init>(Lcom/sec/chaton/trunk/ITrunkView;Landroid/os/Handler;Lcom/sec/chaton/trunk/a/b;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->w:Lcom/sec/chaton/trunk/ca;

    .line 522
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->K:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 523
    new-instance v0, Lcom/sec/chaton/trunk/r;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->g:Landroid/app/Activity;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    iget-object v5, p0, Lcom/sec/chaton/trunk/TrunkView;->k:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/trunk/TrunkView;->E:Lcom/sec/common/f/c;

    iget-object v8, p0, Lcom/sec/chaton/trunk/TrunkView;->K:Ljava/util/ArrayList;

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/trunk/r;-><init>(Landroid/content/Context;Landroid/database/Cursor;ILcom/sec/chaton/trunk/a/b;Ljava/lang/String;Lcom/sec/common/f/c;Lcom/sec/chaton/trunk/TrunkView;Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    .line 525
    const v0, 0x7f070467

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->A:Landroid/widget/LinearLayout;

    .line 528
    const v0, 0x7f070466

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    .line 530
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 531
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 532
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 534
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_2

    .line 535
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    const v1, 0x7f07058f

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 536
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    const v1, 0x7f07058f

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 537
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 538
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    const v1, 0x7f070592

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 539
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    const v1, 0x7f070592

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 545
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->b(Z)V

    .line 547
    const v0, 0x7f070294

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    new-instance v1, Lcom/sec/chaton/trunk/cj;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/cj;-><init>(Lcom/sec/chaton/trunk/TrunkView;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 580
    const v0, 0x7f070468

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->n:Landroid/view/View;

    .line 581
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->n:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 582
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->n:Landroid/view/View;

    const v2, 0x7f07014c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 583
    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->n:Landroid/view/View;

    const v3, 0x7f07014d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 585
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 587
    const v1, 0x7f02034f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 588
    const v0, 0x7f0b03ea

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 591
    const v0, 0x7f0703f3

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->o:Landroid/view/View;

    .line 592
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->o:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 593
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->o:Landroid/view/View;

    const v2, 0x7f07014c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 594
    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->o:Landroid/view/View;

    const v3, 0x7f07014d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 596
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 598
    const v1, 0x7f02034e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 599
    const v0, 0x7f0b002a

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 601
    const v0, 0x7f0703c1

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->p:Landroid/view/View;

    .line 603
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->L:Z

    if-eqz v0, :cond_4

    .line 604
    sget-object v0, Lcom/sec/chaton/trunk/cq;->d:Lcom/sec/chaton/trunk/cq;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->v:Lcom/sec/chaton/trunk/cq;

    .line 609
    :goto_1
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->f()V

    .line 611
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->w:Lcom/sec/chaton/trunk/ca;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ca;->a()V

    .line 613
    return-object v9

    .line 511
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->i:Landroid/content/Intent;

    const-string v1, "sessionId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->j:Ljava/lang/String;

    .line 512
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->i:Landroid/content/Intent;

    const-string v1, "inboxNO"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->k:Ljava/lang/String;

    .line 513
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->i:Landroid/content/Intent;

    const-string v1, "isValid"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->l:Z

    goto/16 :goto_0

    .line 606
    :cond_4
    sget-object v0, Lcom/sec/chaton/trunk/cq;->a:Lcom/sec/chaton/trunk/cq;

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->v:Lcom/sec/chaton/trunk/cq;

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 659
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 660
    const-string v0, "TrunkView.onDestroy()"

    sget-object v1, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/trunk/ITrunkView;->onDestroy()V

    .line 664
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 635
    invoke-super {p0}, Lcom/sec/chaton/trunk/ITrunkView;->onDestroyView()V

    .line 637
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/TrunkView;->a:Z

    .line 638
    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkView;->x:Ljava/lang/Object;

    .line 641
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->c()V

    .line 645
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 647
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 650
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->E:Lcom/sec/common/f/c;

    if-eqz v0, :cond_1

    .line 651
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->E:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 654
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->w:Lcom/sec/chaton/trunk/ca;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ca;->d()V

    .line 655
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 427
    invoke-super {p0}, Lcom/sec/chaton/trunk/ITrunkView;->onDetach()V

    .line 429
    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->g:Landroid/app/Activity;

    .line 430
    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->h:Lcom/sec/chaton/trunk/co;

    .line 431
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const v1, 0x7f070592

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 734
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 831
    :cond_0
    :goto_0
    return v4

    .line 737
    :sswitch_0
    sget-object v0, Lcom/sec/chaton/trunk/cq;->d:Lcom/sec/chaton/trunk/cq;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cq;)V

    .line 738
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 740
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->J:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_0

    .line 746
    :sswitch_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    invoke-direct {p0, v3, v3, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(ZZLcom/sec/chaton/trunk/a/b;)V

    goto :goto_0

    .line 751
    :sswitch_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 752
    const-string v0, "Current order type: %s, ordinal: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/trunk/a/b;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/TrunkView;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->g:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0150

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0d0015

    iget-object v2, p0, Lcom/sec/chaton/trunk/TrunkView;->t:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/trunk/a/b;->ordinal()I

    move-result v2

    new-instance v3, Lcom/sec/chaton/trunk/ck;

    invoke-direct {v3, p0}, Lcom/sec/chaton/trunk/ck;-><init>(Lcom/sec/chaton/trunk/TrunkView;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/common/a/a;->a(IILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 805
    :sswitch_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 806
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 808
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->b()V

    .line 809
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->F:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 810
    sget-object v0, Lcom/sec/chaton/trunk/cq;->e:Lcom/sec/chaton/trunk/cq;

    invoke-direct {p0, v0}, Lcom/sec/chaton/trunk/TrunkView;->a(Lcom/sec/chaton/trunk/cq;)V

    .line 812
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813
    invoke-direct {p0, v3}, Lcom/sec/chaton/trunk/TrunkView;->b(Z)V

    goto/16 :goto_0

    .line 821
    :sswitch_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    .line 823
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->c()V

    .line 825
    iput-boolean v3, p0, Lcom/sec/chaton/trunk/TrunkView;->S:Z

    goto/16 :goto_0

    .line 734
    :sswitch_data_0
    .sparse-switch
        0x7f07058f -> :sswitch_0
        0x7f070591 -> :sswitch_3
        0x7f070592 -> :sswitch_4
        0x7f0705b8 -> :sswitch_2
        0x7f0705b9 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 628
    invoke-super {p0}, Lcom/sec/chaton/trunk/ITrunkView;->onPause()V

    .line 630
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->w:Lcom/sec/chaton/trunk/ca;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ca;->c()V

    .line 631
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 725
    invoke-super {p0, p1}, Lcom/sec/chaton/trunk/ITrunkView;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 727
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkView;->h()V

    .line 728
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 618
    invoke-super {p0}, Lcom/sec/chaton/trunk/ITrunkView;->onResume()V

    .line 623
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->w:Lcom/sec/chaton/trunk/ca;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ca;->b()V

    .line 624
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1466
    invoke-super {p0, p1}, Lcom/sec/chaton/trunk/ITrunkView;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1469
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->v:Lcom/sec/chaton/trunk/cq;

    sget-object v1, Lcom/sec/chaton/trunk/cq;->d:Lcom/sec/chaton/trunk/cq;

    if-ne v0, v1, :cond_1

    .line 1470
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->s:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/r;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    .line 1471
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->H:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 1472
    const-string v1, "checkedItem"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1473
    const-string v0, "isRefreshInDelete"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1474
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->T:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkView;->T:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1475
    const-string v0, "onPopup"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1482
    :goto_0
    return-void

    .line 1477
    :cond_0
    const-string v0, "onPopup"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1480
    :cond_1
    const-string v0, "isRefreshInDelete"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method
