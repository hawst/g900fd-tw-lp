.class public Lcom/sec/chaton/trunk/a/a/c;
.super Lcom/sec/chaton/d/a/a;
.source "GetCommentListTask.java"


# static fields
.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/sec/chaton/trunk/a/a/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/a/a/c;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/d/a/a;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;)V

    .line 47
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/f;)V
    .locals 10

    .prologue
    const v4, 0x7fffffff

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 56
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_6

    .line 58
    const-string v2, ""

    .line 61
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->d()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/h;->f()Ljava/util/List;

    move-result-object v0

    .line 63
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v4

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 64
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v8, "itemid"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 65
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 68
    :goto_1
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v8, "startcommentid"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 69
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_2
    move v3, v0

    move-object v2, v1

    goto :goto_0

    .line 73
    :cond_0
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/entry/GetCommentListEntry;

    .line 75
    if-nez v0, :cond_2

    .line 76
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_1

    .line 77
    const-string v0, "Http result object is null"

    sget-object v1, Lcom/sec/chaton/trunk/a/a/c;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_1
    :goto_3
    return-void

    .line 83
    :cond_2
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 84
    const-string v1, "Save comment to database"

    sget-object v7, Lcom/sec/chaton/trunk/a/a/c;->b:Ljava/lang/String;

    invoke-static {v1, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_3
    if-ne v3, v4, :cond_7

    move v1, v5

    .line 93
    :goto_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v3, v1

    .line 95
    :goto_5
    iget-object v1, v0, Lcom/sec/chaton/trunk/entry/GetCommentListEntry;->comments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_5

    .line 96
    iget-object v1, v0, Lcom/sec/chaton/trunk/entry/GetCommentListEntry;->comments:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/trunk/entry/inner/Comment;

    .line 97
    iput-object v2, v1, Lcom/sec/chaton/trunk/entry/inner/Comment;->itemid:Ljava/lang/String;

    .line 99
    sget-boolean v7, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v7, :cond_4

    .line 100
    const-string v7, " Comment Id: %s"

    new-array v8, v6, [Ljava/lang/Object;

    iget-object v9, v1, Lcom/sec/chaton/trunk/entry/inner/Comment;->cid:Ljava/lang/String;

    aput-object v9, v8, v5

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/chaton/trunk/a/a/c;->b:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_4
    invoke-static {v1}, Lcom/sec/chaton/trunk/database/a/a;->a(Lcom/sec/chaton/trunk/entry/inner/Comment;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5

    .line 106
    :cond_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    goto :goto_3

    .line 109
    :cond_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v6, :cond_1

    .line 110
    const-string v0, "01000016"

    const-string v1, "0203"

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/a/a/f;)V

    goto :goto_3

    :cond_7
    move v1, v6

    goto :goto_4

    :cond_8
    move-object v1, v2

    goto/16 :goto_1

    :cond_9
    move v0, v3

    goto/16 :goto_2
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return-object v0
.end method
