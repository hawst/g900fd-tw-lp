.class public Lcom/sec/chaton/trunk/TrunkDetailActivity;
.super Lcom/sec/chaton/base/BaseMultiPaneActivity;
.source "TrunkDetailActivity.java"

# interfaces
.implements Lcom/sec/chaton/trunk/aj;
.implements Lcom/sec/chaton/trunk/br;


# instance fields
.field protected a:Ljava/lang/String;

.field private b:Lcom/sec/chaton/trunk/TrunkItemView;

.field private c:Lcom/sec/chaton/trunk/ak;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseMultiPaneActivity;-><init>()V

    .line 75
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 102
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 103
    const-string v1, "sessionId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string v1, "inboxNo"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    const-string v1, "ownerUid"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    const-string v1, "fileName"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    const-string v1, "totalcomment"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 108
    const-string v1, "isvalid"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 109
    const-string v1, "mediaUri"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    const-string v1, "isVideo"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 111
    const-class v1, Lcom/sec/chaton/trunk/TrunkDetailActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 113
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 114
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 278
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 281
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 282
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 283
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    invoke-virtual {p0, v1}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 286
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->b:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->g()V

    .line 250
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/c/g;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 190
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 192
    invoke-static {p2}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    const-class v1, Lcom/sec/vip/amschaton/AMSPlayerActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 194
    const-string v1, "AMS_FILE_PATH"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    const-string v1, "VIEWER_MODE"

    const/16 v2, 0x3ea

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 205
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 206
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/chaton/trunk/c/g;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const v2, 0x7f0b0166

    const/4 v4, 0x0

    .line 169
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 170
    const v0, 0x7f0b002b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->a:Lcom/sec/chaton/trunk/c/g;

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne p1, v0, :cond_4

    .line 178
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    .line 183
    :cond_3
    :goto_1
    if-eqz v4, :cond_0

    .line 184
    const/4 v0, 0x2

    invoke-virtual {p0, v4, v0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 179
    :cond_4
    sget-object v0, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-ne p1, v0, :cond_3

    .line 180
    invoke-virtual {p0, v2}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2, p3, v4}, Lcom/sec/chaton/util/ch;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_1
.end method

.method public a(ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 344
    if-eqz p1, :cond_0

    .line 345
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->setResult(ILandroid/content/Intent;)V

    .line 352
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->finish()V

    .line 353
    return-void

    .line 348
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 349
    const-string v1, "noUnread"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 350
    invoke-virtual {p0, v2, v0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 260
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->a(ZZ)V

    .line 261
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 211
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 213
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 215
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "video/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    const-string v1, "android.intent.extra.finishOnCompletion"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 222
    :cond_0
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :cond_1
    :goto_0
    return-void

    .line 223
    :catch_0
    move-exception v0

    .line 224
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 225
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 226
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->b:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->d()V

    .line 245
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 289
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    :goto_0
    return-void

    .line 293
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 294
    const v0, 0x7f0b003d

    invoke-static {p0, v0, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 299
    :cond_1
    new-instance v0, Lcom/sec/chaton/multimedia/a/a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ChatON"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1, v3}, Lcom/sec/chaton/multimedia/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 300
    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->c:Lcom/sec/chaton/trunk/ak;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->c:Lcom/sec/chaton/trunk/ak;

    invoke-interface {v0}, Lcom/sec/chaton/trunk/ak;->c()V

    .line 360
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseMultiPaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 123
    const v0, 0x7f03010d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->setContentView(I)V

    .line 128
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->b(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 131
    new-instance v1, Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-direct {v1}, Lcom/sec/chaton/trunk/TrunkItemView;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->b:Lcom/sec/chaton/trunk/TrunkItemView;

    .line 132
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->b:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/trunk/TrunkItemView;->setArguments(Landroid/os/Bundle;)V

    .line 134
    iget-object v1, p0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->b:Lcom/sec/chaton/trunk/TrunkItemView;

    iput-object v1, p0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->c:Lcom/sec/chaton/trunk/ak;

    .line 138
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 139
    const-string v2, "sessionId"

    const-string v3, "sessionId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v2, "itemId"

    const-string v3, "itemId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v2, "isvalid"

    const-string v3, "isvalid"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 143
    new-instance v0, Lcom/sec/chaton/trunk/TrunkCommentView;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/TrunkCommentView;-><init>()V

    .line 144
    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/TrunkCommentView;->setArguments(Landroid/os/Bundle;)V

    .line 156
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 157
    const v2, 0x7f07046d

    iget-object v3, p0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->b:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 158
    const v2, 0x7f07046c

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 159
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 160
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 267
    invoke-super {p0}, Lcom/sec/chaton/base/BaseMultiPaneActivity;->onResume()V

    .line 269
    invoke-direct {p0}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->d()V

    .line 272
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 275
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 325
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 332
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseMultiPaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 328
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/TrunkDetailActivity;->b:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->e()V

    goto :goto_0

    .line 325
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
