.class Lcom/sec/chaton/trunk/bj;
.super Ljava/lang/Object;
.source "TrunkItemView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/bi;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/bi;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/sec/chaton/trunk/bj;->a:Lcom/sec/chaton/trunk/bi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    .prologue
    const/16 v2, 0x64

    .line 353
    iget-object v0, p0, Lcom/sec/chaton/trunk/bj;->a:Lcom/sec/chaton/trunk/bi;

    iget-object v0, v0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/j;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/trunk/bj;->a:Lcom/sec/chaton/trunk/bi;

    iget-object v0, v0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->e(Lcom/sec/chaton/trunk/TrunkItemView;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/trunk/bj;->a:Lcom/sec/chaton/trunk/bi;

    iget-object v0, v0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->f(Lcom/sec/chaton/trunk/TrunkItemView;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/trunk/bj;->a:Lcom/sec/chaton/trunk/bi;

    iget-object v0, v0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->g(Lcom/sec/chaton/trunk/TrunkItemView;)I

    move-result v0

    if-le v0, v2, :cond_1

    .line 354
    add-int v0, p2, p3

    if-lt v0, p4, :cond_1

    .line 356
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 357
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_2

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/bj;->a:Lcom/sec/chaton/trunk/bi;

    iget-object v0, v0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    sget-object v1, Lcom/sec/chaton/trunk/bs;->a:Lcom/sec/chaton/trunk/bs;

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Lcom/sec/chaton/trunk/bs;)V

    .line 363
    :goto_0
    const-string v0, "Load more comments"

    invoke-static {}, Lcom/sec/chaton/trunk/TrunkItemView;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_1
    return-void

    .line 360
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/bj;->a:Lcom/sec/chaton/trunk/bi;

    iget-object v0, v0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/TrunkItemView;->a(Lcom/sec/chaton/trunk/TrunkItemView;Ljava/lang/Boolean;)V

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/trunk/bj;->a:Lcom/sec/chaton/trunk/bi;

    iget-object v0, v0, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkItemView;->d(Lcom/sec/chaton/trunk/TrunkItemView;)Lcom/sec/chaton/trunk/j;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/bj;->a:Lcom/sec/chaton/trunk/bi;

    iget-object v1, v1, Lcom/sec/chaton/trunk/bi;->a:Lcom/sec/chaton/trunk/TrunkItemView;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkItemView;->h(Lcom/sec/chaton/trunk/TrunkItemView;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/trunk/j;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 348
    return-void
.end method
