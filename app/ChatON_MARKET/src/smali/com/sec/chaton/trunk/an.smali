.class Lcom/sec/chaton/trunk/an;
.super Landroid/os/Handler;
.source "TrunkFullPresenter.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/am;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/am;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 85
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/am;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 127
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v0}, Lcom/sec/chaton/trunk/am;->a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v0}, Lcom/sec/chaton/trunk/am;->a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 92
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 94
    iget-object v1, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v1}, Lcom/sec/chaton/trunk/am;->a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 95
    iget-object v1, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v1}, Lcom/sec/chaton/trunk/am;->a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 98
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_3

    .line 99
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x1518b

    if-ne v0, v1, :cond_3

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v0}, Lcom/sec/chaton/trunk/am;->b(Lcom/sec/chaton/trunk/am;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v1}, Lcom/sec/chaton/trunk/am;->c(Lcom/sec/chaton/trunk/am;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/trunk/database/a/a;->b(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/database/a/a;->a(Landroid/content/Context;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 102
    :catch_0
    move-exception v0

    .line 103
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 104
    invoke-static {}, Lcom/sec/chaton/trunk/am;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v0}, Lcom/sec/chaton/trunk/am;->d(Lcom/sec/chaton/trunk/am;)V

    goto :goto_0

    .line 118
    :sswitch_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v0}, Lcom/sec/chaton/trunk/am;->a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v0}, Lcom/sec/chaton/trunk/am;->a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 136
    :sswitch_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v0}, Lcom/sec/chaton/trunk/am;->a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v0}, Lcom/sec/chaton/trunk/am;->a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 145
    :sswitch_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v0}, Lcom/sec/chaton/trunk/am;->a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/trunk/an;->a:Lcom/sec/chaton/trunk/am;

    invoke-static {v0}, Lcom/sec/chaton/trunk/am;->a(Lcom/sec/chaton/trunk/am;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 89
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x385 -> :sswitch_1
        0x389 -> :sswitch_2
    .end sparse-switch
.end method
