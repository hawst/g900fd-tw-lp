.class public abstract Lcom/sec/chaton/trunk/a;
.super Ljava/lang/Object;
.source "AbstractPresenter.java"


# instance fields
.field protected a:Z

.field protected b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/a;->a:Z

    if-eqz v0, :cond_0

    .line 28
    :goto_0
    return-void

    .line 21
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/a;->a:Z

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/a;->b:Z

    .line 27
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/a;->f()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/a;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/a;->g()V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/a;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/a;->h()V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/a;->b:Z

    if-eqz v0, :cond_0

    .line 58
    :goto_0
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/a;->a:Z

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/a;->b:Z

    .line 57
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/a;->i()V

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 61
    iget-boolean v1, p0, Lcom/sec/chaton/trunk/a;->a:Z

    if-nez v1, :cond_1

    .line 69
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    iget-boolean v1, p0, Lcom/sec/chaton/trunk/a;->b:Z

    if-nez v1, :cond_0

    .line 69
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected f()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method protected g()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method protected h()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method protected i()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method
