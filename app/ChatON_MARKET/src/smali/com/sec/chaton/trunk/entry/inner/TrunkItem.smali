.class public Lcom/sec/chaton/trunk/entry/inner/TrunkItem;
.super Lcom/sec/chaton/io/entry/Entry;
.source "TrunkItem.java"


# instance fields
.field public commentlist:Lcom/sec/chaton/trunk/entry/inner/CommentList;
    .annotation runtime Lcom/sec/chaton/io/entry/EntryParserInfo;
        name = "commentlist"
    .end annotation
.end field

.field public description:Ljava/lang/String;

.field public file:Lcom/sec/chaton/trunk/entry/inner/File;
    .annotation runtime Lcom/sec/chaton/io/entry/EntryParserInfo;
        name = "file"
    .end annotation
.end field

.field public itemid:Ljava/lang/String;

.field public lastcmtdttm:Ljava/lang/Long;

.field public regdttm:Ljava/lang/Long;

.field public sessionid:Ljava/lang/String;

.field public totcmtcnt:Ljava/lang/Integer;

.field public unreadcmtcnt:Ljava/lang/Integer;

.field public user:Lcom/sec/chaton/trunk/entry/inner/User;
    .annotation runtime Lcom/sec/chaton/io/entry/EntryParserInfo;
        name = "sender"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/chaton/io/entry/Entry;-><init>()V

    .line 16
    new-instance v0, Lcom/sec/chaton/trunk/entry/inner/User;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/entry/inner/User;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->user:Lcom/sec/chaton/trunk/entry/inner/User;

    .line 19
    new-instance v0, Lcom/sec/chaton/trunk/entry/inner/File;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/entry/inner/File;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->file:Lcom/sec/chaton/trunk/entry/inner/File;

    .line 22
    new-instance v0, Lcom/sec/chaton/trunk/entry/inner/CommentList;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/entry/inner/CommentList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/trunk/entry/inner/TrunkItem;->commentlist:Lcom/sec/chaton/trunk/entry/inner/CommentList;

    return-void
.end method
