.class public final enum Lcom/sec/chaton/trunk/a/b;
.super Ljava/lang/Enum;
.source "TrunkMessageControl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/trunk/a/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/trunk/a/b;

.field public static final enum b:Lcom/sec/chaton/trunk/a/b;

.field private static final synthetic d:[Lcom/sec/chaton/trunk/a/b;


# instance fields
.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    new-instance v0, Lcom/sec/chaton/trunk/a/b;

    const-string v1, "UnreadComment"

    const-string v2, "unreadcommented"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/trunk/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    .line 63
    new-instance v0, Lcom/sec/chaton/trunk/a/b;

    const-string v1, "Recent"

    const-string v2, "recentadded"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/trunk/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    .line 61
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/trunk/a/b;

    sget-object v1, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/chaton/trunk/a/b;->d:[Lcom/sec/chaton/trunk/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    iput-object p3, p0, Lcom/sec/chaton/trunk/a/b;->c:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/trunk/a/b;
    .locals 1

    .prologue
    .line 78
    const-string v0, "recentadded"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    sget-object v0, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    .line 84
    :goto_0
    return-object v0

    .line 80
    :cond_0
    const-string v0, "unreadcommented"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    sget-object v0, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    goto :goto_0

    .line 84
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/trunk/a/b;
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/sec/chaton/trunk/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/a/b;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/trunk/a/b;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/chaton/trunk/a/b;->d:[Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v0}, [Lcom/sec/chaton/trunk/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/trunk/a/b;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/trunk/a/b;->c:Ljava/lang/String;

    return-object v0
.end method
