.class Lcom/sec/chaton/trunk/cf;
.super Landroid/content/AsyncQueryHandler;
.source "TrunkPresenter.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/ca;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/trunk/ca;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 408
    iput-object p1, p0, Lcom/sec/chaton/trunk/cf;->a:Lcom/sec/chaton/trunk/ca;

    .line 409
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 410
    return-void
.end method


# virtual methods
.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 414
    invoke-super {p0, p1, p2, p3}, Landroid/content/AsyncQueryHandler;->onDeleteComplete(ILjava/lang/Object;I)V

    .line 416
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 417
    const-string v0, "Clear trunk item is completed."

    invoke-static {}, Lcom/sec/chaton/trunk/ca;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/cf;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v0, v4}, Lcom/sec/chaton/trunk/ca;->a(Lcom/sec/chaton/trunk/ca;Lcom/sec/chaton/trunk/cf;)Lcom/sec/chaton/trunk/cf;

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/trunk/cf;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ca;->i(Lcom/sec/chaton/trunk/ca;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/trunk/cf;->a:Lcom/sec/chaton/trunk/ca;

    iget-object v1, p0, Lcom/sec/chaton/trunk/cf;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v1}, Lcom/sec/chaton/trunk/ca;->l(Lcom/sec/chaton/trunk/ca;)Lcom/sec/chaton/trunk/ITrunkView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/ITrunkView;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/trunk/cf;->a:Lcom/sec/chaton/trunk/ca;

    invoke-virtual {v1, v2, v4, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/ca;->a(Lcom/sec/chaton/trunk/ca;Landroid/support/v4/content/Loader;)Landroid/support/v4/content/Loader;

    .line 427
    :goto_0
    return-void

    .line 425
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/cf;->a:Lcom/sec/chaton/trunk/ca;

    invoke-static {v0}, Lcom/sec/chaton/trunk/ca;->e(Lcom/sec/chaton/trunk/ca;)V

    goto :goto_0
.end method
