.class public Lcom/sec/chaton/trunk/y;
.super Lcom/sec/chaton/trunk/a;
.source "TrunkCommentPresenter.java"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private d:Lcom/sec/chaton/trunk/ITrunkCommentView;

.field private e:Landroid/app/Activity;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lcom/sec/chaton/trunk/a/a/a;

.field private i:Lcom/sec/chaton/trunk/a/a;

.field private j:Landroid/os/Handler;

.field private k:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/chaton/trunk/y;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/y;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/trunk/ITrunkCommentView;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/chaton/trunk/a;-><init>()V

    .line 42
    new-instance v0, Lcom/sec/chaton/trunk/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/z;-><init>(Lcom/sec/chaton/trunk/y;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/y;->k:Landroid/os/Handler;

    .line 80
    iput-object p1, p0, Lcom/sec/chaton/trunk/y;->d:Lcom/sec/chaton/trunk/ITrunkCommentView;

    .line 81
    iput-object p3, p0, Lcom/sec/chaton/trunk/y;->f:Ljava/lang/String;

    .line 82
    iput-object p4, p0, Lcom/sec/chaton/trunk/y;->g:Ljava/lang/String;

    .line 83
    iput-object p2, p0, Lcom/sec/chaton/trunk/y;->j:Landroid/os/Handler;

    .line 84
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/y;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/trunk/y;->j:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/y;Lcom/sec/chaton/trunk/a/a/a;)Lcom/sec/chaton/trunk/a/a/a;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/chaton/trunk/y;->h:Lcom/sec/chaton/trunk/a/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/y;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/chaton/trunk/y;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/y;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/trunk/y;->e:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/y;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/trunk/y;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/y;)Lcom/sec/chaton/trunk/a/a/a;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/trunk/y;->h:Lcom/sec/chaton/trunk/a/a/a;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/y;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/trunk/y;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/y;)Lcom/sec/chaton/trunk/a/a;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/trunk/y;->i:Lcom/sec/chaton/trunk/a/a;

    return-object v0
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/chaton/trunk/y;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected f()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/trunk/y;->d:Lcom/sec/chaton/trunk/ITrunkCommentView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ITrunkCommentView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/y;->e:Landroid/app/Activity;

    .line 90
    new-instance v0, Lcom/sec/chaton/trunk/a/a;

    iget-object v1, p0, Lcom/sec/chaton/trunk/y;->e:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/trunk/y;->k:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/trunk/a/a;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/y;->i:Lcom/sec/chaton/trunk/a/a;

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/trunk/y;->d:Lcom/sec/chaton/trunk/ITrunkCommentView;

    new-instance v1, Lcom/sec/chaton/trunk/aa;

    invoke-direct {v1, p0}, Lcom/sec/chaton/trunk/aa;-><init>(Lcom/sec/chaton/trunk/y;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/ITrunkCommentView;->a(Lcom/sec/chaton/trunk/e;)V

    .line 112
    return-void
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/trunk/y;->j:Landroid/os/Handler;

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/trunk/y;->h:Lcom/sec/chaton/trunk/a/a/a;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/chaton/trunk/y;->h:Lcom/sec/chaton/trunk/a/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/a;->c()V

    .line 121
    :cond_0
    return-void
.end method
