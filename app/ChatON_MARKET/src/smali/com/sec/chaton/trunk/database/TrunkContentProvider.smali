.class public Lcom/sec/chaton/trunk/database/TrunkContentProvider;
.super Landroid/content/ContentProvider;
.source "TrunkContentProvider.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)I
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 518
    const-string v0, "TrunkContentProvider.onTrunkItemMarkAsRead"

    sget-object v2, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 524
    const/4 v2, 0x0

    .line 527
    :try_start_0
    const-string v3, "SELECT %s FROM %s WHERE %s = ?"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "session_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "trunk_item"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "item_id"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 529
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 531
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 532
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 539
    if-eqz v2, :cond_0

    .line 540
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 544
    :cond_0
    const-string v2, "unread_comment_count > ?"

    .line 545
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v1

    .line 547
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 548
    const-string v6, "unread_comment_count"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 551
    sget-object v6, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, v5, v2, v4}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 553
    if-lez v0, :cond_1

    .line 554
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 556
    const-string v4, "UPDATE %s "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 557
    const-string v4, "\tSET "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    const-string v4, "\t\t%s = %s - 1 "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    const-string v4, "\tWHERE "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    const-string v4, "\t\t%s = ?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "trunk"

    aput-object v5, v4, v1

    const-string v5, "unread_item_count"

    aput-object v5, v4, v8

    const-string v5, "unread_item_count"

    aput-object v5, v4, v9

    const-string v5, "session_id"

    aput-object v5, v4, v10

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 564
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SQL: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    new-array v4, v8, [Ljava/lang/Object;

    aput-object v3, v4, v1

    invoke-virtual {p1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 569
    :cond_1
    :goto_0
    return v0

    .line 534
    :cond_2
    :try_start_2
    const-string v0, "Can\'t find trunk item"

    sget-object v3, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 539
    if-eqz v2, :cond_3

    .line 540
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v1

    .line 536
    goto :goto_0

    .line 539
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_4

    .line 540
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 539
    :cond_4
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 14

    .prologue
    .line 576
    const-string v2, "TrunkContentProvider.onTrunkItemDeleted"

    sget-object v3, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    const/4 v9, 0x0

    .line 580
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 582
    const-string v2, "effect"

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v11

    .line 583
    const-string v2, "rearrangeLoadMoreItem"

    const/4 v3, 0x1

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v12

    .line 584
    const/4 v2, 0x0

    .line 586
    if-eqz p2, :cond_0

    .line 587
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 588
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 593
    :cond_0
    new-instance v3, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v3}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v4, "trunk_item"

    invoke-virtual {v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v3

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v3, v0, v1}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v3

    .line 595
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 601
    :cond_1
    :goto_0
    const/4 v8, 0x0

    .line 605
    const/4 v4, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    move-object/from16 v3, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    :try_start_0
    invoke-virtual/range {v2 .. v7}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 608
    if-eqz v3, :cond_c

    move v2, v9

    .line 609
    :goto_1
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 610
    add-int/lit8 v9, v2, 0x1

    .line 612
    const-string v2, "item_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 613
    const-string v2, "session_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 614
    const-string v2, "item_type"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/trunk/database/d;->a(I)Lcom/sec/chaton/trunk/database/d;

    move-result-object v6

    .line 615
    const-string v2, "unread_comment_count"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 618
    if-eqz v11, :cond_4

    .line 619
    invoke-interface {v10, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Integer;

    .line 621
    if-nez v2, :cond_2

    .line 622
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v8, 0x0

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v2, v8

    const/4 v8, 0x1

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v2, v8

    .line 625
    :cond_2
    const/4 v8, 0x0

    const/4 v13, 0x0

    aget-object v13, v2, v13

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v2, v8

    .line 627
    if-lez v7, :cond_3

    .line 629
    const/4 v7, 0x1

    const/4 v8, 0x1

    aget-object v8, v2, v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v7

    .line 632
    :cond_3
    invoke-interface {v10, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    :cond_4
    const-string v2, "DELETE FROM %s WHERE %s=? AND %s=?"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "trunk_item"

    aput-object v8, v5, v7

    const/4 v7, 0x1

    const-string v8, "item_id"

    aput-object v8, v5, v7

    const/4 v7, 0x2

    const-string v8, "item_type"

    aput-object v8, v5, v7

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 638
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v5, v7

    const/4 v7, 0x1

    invoke-virtual {v6}, Lcom/sec/chaton/trunk/database/d;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-virtual {p1, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 640
    if-eqz v12, :cond_5

    .line 641
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Rearrange load more item: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    sget-object v2, Lcom/sec/chaton/trunk/database/d;->a:Lcom/sec/chaton/trunk/database/d;

    if-ne v6, v2, :cond_5

    .line 645
    invoke-direct {p0, v4}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 646
    invoke-direct {p0, v4}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    :goto_2
    move v2, v9

    .line 652
    goto/16 :goto_1

    .line 598
    :cond_6
    const-string v4, "item_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    goto/16 :goto_0

    .line 648
    :cond_7
    :try_start_2
    const-string v2, "This item hasn\'t more item. skip rearrange logic."

    sget-object v4, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 655
    :catchall_0
    move-exception v2

    :goto_3
    if-eqz v3, :cond_8

    .line 656
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 655
    :cond_8
    throw v2

    :cond_9
    move v4, v2

    :goto_4
    if-eqz v3, :cond_a

    .line 656
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 660
    :cond_a
    if-eqz v11, :cond_b

    .line 667
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 670
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Integer;

    .line 671
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Effect other table. Total item count: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, v3, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Unread item count: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v7, v3, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    const-string v6, "UPDATE %s \tSET \t\t%s = %s - ?, \t\t%s = %s - ? \tWHERE %s = ?"

    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "trunk"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "total_item_count"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "total_item_count"

    aput-object v9, v7, v8

    const/4 v8, 0x3

    const-string v9, "unread_item_count"

    aput-object v9, v7, v8

    const/4 v8, 0x4

    const-string v9, "unread_item_count"

    aput-object v9, v7, v8

    const/4 v8, 0x5

    const-string v9, "session_id"

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 676
    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v3, v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x1

    aget-object v3, v3, v9

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v8

    const/4 v3, 0x2

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v7, v3

    invoke-virtual {p1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 680
    :cond_b
    return v4

    .line 655
    :catchall_1
    move-exception v2

    move-object v3, v8

    goto/16 :goto_3

    :cond_c
    move v4, v9

    goto/16 :goto_4
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 414
    .line 420
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/database/e;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 421
    sget-object v2, Lcom/sec/chaton/trunk/database/e;->a:Landroid/net/Uri;

    .line 422
    const-string v1, "trunk"

    .line 423
    const-string v0, "session_id"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 442
    :goto_0
    const-string v4, "replace"

    invoke-direct {p0, p2, v4, v6}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v4

    .line 444
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 446
    if-nez v4, :cond_4

    .line 447
    new-instance v2, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v2}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    invoke-virtual {v2, v1}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v1

    invoke-virtual {v1, p1, p3}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    .line 449
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 508
    :cond_0
    :goto_1
    return-object v0

    .line 426
    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 427
    sget-object v2, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    .line 428
    const-string v1, "trunk_item"

    .line 429
    const-string v0, "item_id"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 434
    :cond_2
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/database/b;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 435
    sget-object v2, Lcom/sec/chaton/trunk/database/b;->a:Landroid/net/Uri;

    .line 436
    const-string v1, "trunk_comment"

    .line 437
    const-string v0, "comment_id"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v3

    .line 439
    goto :goto_1

    .line 453
    :cond_4
    invoke-virtual {p0, v0, p3, v3, v3}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 455
    if-nez v2, :cond_5

    .line 456
    new-instance v2, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v2}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    invoke-virtual {v2, v1}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    invoke-virtual {v2, p1, p3}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    .line 458
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 460
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 461
    const-string v2, "withLoadMoreItem"

    invoke-direct {p0, p2, v2, v6}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v2

    .line 463
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Is with load more item: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    if-eqz v2, :cond_0

    .line 466
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 467
    const-string v3, "item_type"

    sget-object v4, Lcom/sec/chaton/trunk/database/d;->b:Lcom/sec/chaton/trunk/database/d;

    invoke-virtual {v4}, Lcom/sec/chaton/trunk/database/d;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 469
    new-instance v3, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v3}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    invoke-virtual {v3, v1}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v1

    const-string v3, "item_id=?"

    new-array v4, v5, [Ljava/lang/String;

    const-string v5, "item_id"

    invoke-virtual {p3, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v1

    invoke-virtual {v1, p1, v2}, Lcom/sec/chaton/trunk/database/f;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    goto/16 :goto_1

    .line 492
    :cond_5
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 493
    const-string v2, "withLoadMoreItem"

    invoke-direct {p0, p2, v2, v6}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v2

    .line 495
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Is with load more item: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    if-eqz v2, :cond_0

    .line 498
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 499
    const-string v3, "item_type"

    sget-object v4, Lcom/sec/chaton/trunk/database/d;->b:Lcom/sec/chaton/trunk/database/d;

    invoke-virtual {v4}, Lcom/sec/chaton/trunk/database/d;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 501
    new-instance v3, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v3}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    invoke-virtual {v3, v1}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v1

    const-string v3, "item_id=?"

    new-array v4, v5, [Ljava/lang/String;

    const-string v5, "item_id"

    invoke-virtual {p3, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v1

    invoke-virtual {v1, p1, v2}, Lcom/sec/chaton/trunk/database/f;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    goto/16 :goto_1
.end method

.method private a()V
    .locals 4

    .prologue
    .line 63
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    .line 66
    iget-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.develop.sec.chaton.provider"

    const-string v2, "trunk"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.develop.sec.chaton.provider"

    const-string v2, "trunk/*"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.develop.sec.chaton.provider"

    const-string v2, "trunk_item/trunk_comment"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.develop.sec.chaton.provider"

    const-string v2, "trunk_item/trunk_comment/item/*"

    const/16 v3, 0x16

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.develop.sec.chaton.provider"

    const-string v2, "trunk_item/trunk_comment/*"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.develop.sec.chaton.provider"

    const-string v2, "trunk_item"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.develop.sec.chaton.provider"

    const-string v2, "trunk_item/markAsRead/*"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.develop.sec.chaton.provider"

    const-string v2, "trunk_item/*"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 78
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 858
    if-nez p1, :cond_1

    .line 868
    :cond_0
    :goto_0
    return p3

    .line 862
    :cond_1
    invoke-virtual {p1, p2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 864
    if-eqz v0, :cond_0

    .line 868
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p3

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 776
    sget-object v0, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 777
    const-string v3, "item_type=?"

    .line 778
    new-array v4, v7, [Ljava/lang/String;

    sget-object v0, Lcom/sec/chaton/trunk/database/d;->b:Lcom/sec/chaton/trunk/database/d;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/database/d;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    move-object v0, p0

    move-object v5, v2

    .line 780
    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 783
    if-nez v0, :cond_0

    move v0, v6

    .line 792
    :goto_0
    return v0

    .line 788
    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 796
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move v0, v6

    .line 789
    goto :goto_0

    .line 796
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move v0, v7

    .line 792
    goto :goto_0

    .line 796
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10

    .prologue
    .line 687
    const-string v0, "TrunkContentProvider.onTrunkCommentDeleted"

    sget-object v1, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 691
    const-string v0, "effect"

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v4

    .line 692
    const/4 v0, 0x0

    .line 694
    if-eqz p2, :cond_0

    .line 695
    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 696
    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 701
    :cond_0
    new-instance v1, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v1}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    invoke-virtual {v1, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    .line 703
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v5, ""

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 709
    :cond_1
    :goto_0
    if-eqz v4, :cond_2

    .line 711
    const/4 v1, 0x0

    .line 714
    :try_start_0
    const-string v0, "SELECT %s, COUNT(*) FROM %s WHERE %s GROUP BY %s "

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "item_id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "trunk_comment"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-virtual {v2}, Lcom/sec/chaton/trunk/database/f;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "item_id"

    aput-object v7, v5, v6

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 716
    invoke-virtual {v2}, Lcom/sec/chaton/trunk/database/f;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 718
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 719
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x1

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 726
    if-eqz v1, :cond_2

    .line 727
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 733
    :cond_2
    const-string v0, "trunk_comment"

    invoke-virtual {v2, v0}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    .line 735
    invoke-virtual {v2, p1}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v2

    .line 737
    if-eqz v4, :cond_7

    .line 738
    const-string v0, "Effect other table"

    sget-object v1, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 744
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 745
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 746
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 748
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TrunkItemId: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ", DeletingRowCount: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v1, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 752
    const-string v1, "UPDATE %s "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 753
    const-string v1, "\tSET "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 754
    const-string v1, "\t\t%s=%s-%d "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 755
    const-string v1, "\tWHERE "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 756
    const-string v1, "\t\t%s=?"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 758
    sget-object v1, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "trunk_item"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "total_comment_count"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "total_comment_count"

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v8

    const/4 v5, 0x4

    const-string v8, "item_id"

    aput-object v8, v7, v5

    invoke-static {v1, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 760
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SQL: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {p1, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 706
    :cond_3
    const-string v1, "comment_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v2, v1, v5}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    goto/16 :goto_0

    .line 721
    :cond_4
    :try_start_1
    const-string v0, "Can\'t find trunk comment"

    sget-object v2, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 723
    const/4 v0, 0x0

    .line 726
    if-eqz v1, :cond_5

    .line 727
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 766
    :cond_5
    :goto_2
    return v0

    .line 726
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_6

    .line 727
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 726
    :cond_6
    throw v0

    :cond_7
    move v0, v2

    .line 766
    goto :goto_2
.end method

.method private b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 807
    const-string v3, "item_id > ?"

    .line 808
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 812
    :try_start_0
    sget-object v1, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v5, "item_id ASC"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 815
    if-eqz v1, :cond_0

    .line 816
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 817
    const-string v0, "item_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 818
    const-string v2, "item_type"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/trunk/database/d;->a(I)Lcom/sec/chaton/trunk/database/d;

    move-result-object v2

    .line 820
    sget-object v3, Lcom/sec/chaton/trunk/database/d;->a:Lcom/sec/chaton/trunk/database/d;

    if-ne v2, v3, :cond_0

    .line 821
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Move load more trunk item to previous item. Item id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    sget-object v2, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 825
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 826
    const-string v4, "item_id"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v0, v4}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 843
    :cond_0
    if-eqz v1, :cond_1

    .line 844
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 847
    :cond_1
    return-void

    .line 843
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_2

    .line 844
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 843
    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .prologue
    .line 386
    invoke-static {}, Lcom/sec/chaton/trunk/database/a/b;->a()Lcom/sec/chaton/trunk/database/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/database/a/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 388
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 390
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 392
    :try_start_0
    new-array v4, v3, [Landroid/content/ContentProviderResult;

    .line 394
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 395
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    invoke-virtual {v0, p0, v4, v1}, Landroid/content/ContentProviderOperation;->apply(Landroid/content/ContentProvider;[Landroid/content/ContentProviderResult;I)Landroid/content/ContentProviderResult;

    move-result-object v0

    aput-object v0, v4, v1

    .line 394
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 398
    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 400
    return-object v4

    .line 402
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 279
    invoke-static {}, Lcom/sec/chaton/trunk/database/a/b;->a()Lcom/sec/chaton/trunk/database/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/database/a/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 282
    iget-object v2, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 284
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 287
    sparse-switch v2, :sswitch_data_0

    .line 311
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 316
    return v0

    .line 289
    :sswitch_0
    :try_start_1
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    .line 290
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 313
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 294
    :sswitch_1
    :try_start_2
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    const-string v3, "session_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    .line 295
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    goto :goto_0

    .line 300
    :sswitch_2
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 301
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    goto :goto_0

    .line 306
    :sswitch_3
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 307
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 287
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0xa -> :sswitch_2
        0xc -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_3
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 98
    sparse-switch v0, :sswitch_data_0

    .line 119
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 100
    :sswitch_0
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.trunk"

    goto :goto_0

    .line 103
    :sswitch_1
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.trunk"

    goto :goto_0

    .line 106
    :sswitch_2
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.trunk.item"

    goto :goto_0

    .line 109
    :sswitch_3
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.trunk.item"

    goto :goto_0

    .line 113
    :sswitch_4
    const-string v0, "vnd.chaton.cursor.dir/vnd.chaton.trunk.comment"

    goto :goto_0

    .line 116
    :sswitch_5
    const-string v0, "vnd.chaton.cursor.item/vnd.chaton.trunk.comment"

    goto :goto_0

    .line 98
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0xa -> :sswitch_2
        0xc -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_5
        0x16 -> :sswitch_4
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 230
    invoke-static {}, Lcom/sec/chaton/trunk/database/a/b;->a()Lcom/sec/chaton/trunk/database/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/database/a/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 231
    iget-object v2, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 233
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 236
    sparse-switch v2, :sswitch_data_0

    .line 268
    :goto_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 271
    return-object v0

    .line 239
    :sswitch_0
    :try_start_0
    invoke-direct {p0, v1, p1, p2}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 240
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 242
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 268
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 249
    :sswitch_1
    :try_start_1
    invoke-direct {p0, v1, p1, p2}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 250
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 252
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 259
    :sswitch_2
    invoke-direct {p0, v1, p1, p2}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 260
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 262
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 236
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xa -> :sswitch_1
        0x14 -> :sswitch_2
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a()V

    .line 88
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 127
    invoke-static {}, Lcom/sec/chaton/trunk/database/a/b;->a()Lcom/sec/chaton/trunk/database/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/database/a/b;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 132
    sparse-switch v0, :sswitch_data_0

    .line 218
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "URI isn\'t valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :sswitch_0
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1, v6, v6, p5}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/trunk/database/e;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 222
    :cond_0
    :goto_0
    return-object v0

    .line 146
    :sswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 148
    new-instance v2, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v2}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v3, "trunk"

    invoke-virtual {v2, v3}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    const-string v3, "session_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    invoke-virtual {v2, v1, v6, v6, p5}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 150
    if-eqz v1, :cond_1

    .line 151
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/database/e;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object v0, v1

    goto :goto_0

    .line 159
    :sswitch_2
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk_item"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1, v6, v6, p5}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 161
    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto :goto_0

    .line 170
    :sswitch_3
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 172
    new-instance v2, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v2}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v3, "trunk_item"

    invoke-virtual {v2, v3}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    const-string v3, "item_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    invoke-virtual {v2, v1, v6, v6, p5}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 174
    if-eqz v1, :cond_1

    .line 175
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 183
    :sswitch_4
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk_comment"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1, v6, v6, p5}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/trunk/database/b;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 194
    :sswitch_5
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk_comment"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    const-string v3, "item_id=?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x3

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1, v6, v6, p5}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/trunk/database/b;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 205
    :sswitch_6
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 207
    new-instance v2, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v2}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v3, "trunk_comment"

    invoke-virtual {v2, v3}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    const-string v3, "comment_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    invoke-virtual {v2, v1, v6, v6, p5}, Lcom/sec/chaton/trunk/database/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 209
    if-eqz v1, :cond_1

    .line 210
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/trunk/database/b;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_1
    move-object v0, v1

    goto/16 :goto_0

    .line 132
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0xa -> :sswitch_2
        0xc -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_6
        0x16 -> :sswitch_5
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 324
    invoke-static {}, Lcom/sec/chaton/trunk/database/a/b;->a()Lcom/sec/chaton/trunk/database/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/database/a/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 328
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 331
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 369
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 374
    return v0

    .line 333
    :sswitch_0
    :try_start_1
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1, p2}, Lcom/sec/chaton/trunk/database/f;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    .line 334
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 371
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 338
    :sswitch_1
    :try_start_2
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    const-string v3, "session_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1, p2}, Lcom/sec/chaton/trunk/database/f;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    .line 339
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    goto :goto_0

    .line 343
    :sswitch_2
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk_item"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1, p2}, Lcom/sec/chaton/trunk/database/f;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    .line 344
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    goto :goto_0

    .line 349
    :sswitch_3
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk_item"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    const-string v3, "item_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1, p2}, Lcom/sec/chaton/trunk/database/f;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    .line 350
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    goto/16 :goto_0

    .line 354
    :sswitch_4
    invoke-direct {p0, v1, p1}, Lcom/sec/chaton/trunk/database/TrunkContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)I

    move-result v0

    .line 355
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    goto/16 :goto_0

    .line 359
    :sswitch_5
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk_comment"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1, p2}, Lcom/sec/chaton/trunk/database/f;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    .line 360
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    goto/16 :goto_0

    .line 364
    :sswitch_6
    new-instance v0, Lcom/sec/chaton/trunk/database/f;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/database/f;-><init>()V

    const-string v2, "trunk_comment"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v2

    const-string v3, "comment_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v6, 0x2

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/trunk/database/f;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/chaton/trunk/database/f;

    move-result-object v0

    invoke-virtual {v0, v1, p2}, Lcom/sec/chaton/trunk/database/f;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    .line 365
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 331
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0xa -> :sswitch_2
        0xc -> :sswitch_3
        0xd -> :sswitch_4
        0x14 -> :sswitch_5
        0x15 -> :sswitch_6
    .end sparse-switch
.end method
