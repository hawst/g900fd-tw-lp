.class public Lcom/sec/chaton/trunk/by;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "TrunkPageActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/TrunkPageActivity;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/trunk/TrunkPageActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 638
    iput-object p1, p0, Lcom/sec/chaton/trunk/by;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    .line 639
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 640
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 634
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentStatePagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 635
    iget-object v0, p0, Lcom/sec/chaton/trunk/by;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v0}, Lcom/sec/chaton/trunk/TrunkPageActivity;->h(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lcom/sec/chaton/trunk/by;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    iget-object v0, v0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 645
    iget-object v0, p0, Lcom/sec/chaton/trunk/by;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    iget-object v0, v0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 646
    iget-object v0, p0, Lcom/sec/chaton/trunk/by;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    iget-object v0, v0, Lcom/sec/chaton/trunk/TrunkPageActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/bz;

    .line 647
    if-eqz v0, :cond_0

    .line 648
    new-instance v0, Lcom/sec/chaton/trunk/TrunkFullView;

    invoke-direct {v0}, Lcom/sec/chaton/trunk/TrunkFullView;-><init>()V

    .line 650
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 651
    const-string v2, "position"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 652
    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/TrunkFullView;->setArguments(Landroid/os/Bundle;)V

    .line 654
    iget-object v1, p0, Lcom/sec/chaton/trunk/by;->a:Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-static {v1}, Lcom/sec/chaton/trunk/TrunkPageActivity;->h(Lcom/sec/chaton/trunk/TrunkPageActivity;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 665
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 675
    const/4 v0, -0x2

    return v0
.end method
