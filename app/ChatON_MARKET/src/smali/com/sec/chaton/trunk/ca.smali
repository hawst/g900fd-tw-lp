.class public Lcom/sec/chaton/trunk/ca;
.super Lcom/sec/chaton/trunk/a;
.source "TrunkPresenter.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/sec/chaton/trunk/c/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/chaton/trunk/a;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/sec/chaton/trunk/c/e;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;

.field private static p:Z


# instance fields
.field private d:Lcom/sec/chaton/trunk/ITrunkView;

.field private e:Landroid/app/Activity;

.field private f:Lcom/sec/chaton/trunk/a/b;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Lcom/sec/chaton/trunk/a/a;

.field private j:Lcom/sec/chaton/trunk/a/a/d;

.field private k:Lcom/sec/chaton/trunk/a/a/h;

.field private l:Landroid/os/Handler;

.field private m:Lcom/sec/chaton/trunk/a/b;

.field private n:Landroid/support/v4/content/Loader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field private q:Lcom/sec/chaton/trunk/cf;

.field private r:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/chaton/trunk/ca;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    .line 64
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/trunk/ca;->p:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/trunk/ITrunkView;Landroid/os/Handler;Lcom/sec/chaton/trunk/a/b;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/sec/chaton/trunk/a;-><init>()V

    .line 68
    new-instance v0, Lcom/sec/chaton/trunk/cb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/trunk/cb;-><init>(Lcom/sec/chaton/trunk/ca;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/ca;->r:Landroid/os/Handler;

    .line 122
    iput-object p1, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    .line 123
    iput-object p3, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    iput-object v0, p0, Lcom/sec/chaton/trunk/ca;->m:Lcom/sec/chaton/trunk/a/b;

    .line 125
    iput-object p4, p0, Lcom/sec/chaton/trunk/ca;->g:Ljava/lang/String;

    .line 126
    iput-object p2, p0, Lcom/sec/chaton/trunk/ca;->l:Landroid/os/Handler;

    .line 127
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/ca;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->l:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/ca;Landroid/support/v4/content/Loader;)Landroid/support/v4/content/Loader;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/ca;Lcom/sec/chaton/trunk/a/a/d;)Lcom/sec/chaton/trunk/a/a/d;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/chaton/trunk/ca;->j:Lcom/sec/chaton/trunk/a/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/ca;Lcom/sec/chaton/trunk/a/a/h;)Lcom/sec/chaton/trunk/a/a/h;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/chaton/trunk/ca;->k:Lcom/sec/chaton/trunk/a/a/h;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/ca;Lcom/sec/chaton/trunk/cf;)Lcom/sec/chaton/trunk/cf;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/chaton/trunk/ca;->q:Lcom/sec/chaton/trunk/cf;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/trunk/ca;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/chaton/trunk/ca;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/trunk/ca;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/ca;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/trunk/ca;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ca;->l()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/trunk/ca;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/chaton/trunk/ca;->o:Z

    return v0
.end method

.method static synthetic e(Lcom/sec/chaton/trunk/ca;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ca;->k()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/trunk/ca;)Landroid/support/v4/content/Loader;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/trunk/ca;)Lcom/sec/chaton/trunk/a/a/d;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->j:Lcom/sec/chaton/trunk/a/a/d;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/trunk/ca;)Lcom/sec/chaton/trunk/a/b;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/trunk/ca;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/trunk/ca;)Lcom/sec/chaton/trunk/a/a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->i:Lcom/sec/chaton/trunk/a/a;

    return-object v0
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/trunk/ca;)Lcom/sec/chaton/trunk/a/a/h;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->k:Lcom/sec/chaton/trunk/a/a/h;

    return-object v0
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 281
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ca;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->q:Lcom/sec/chaton/trunk/cf;

    if-nez v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/trunk/ITrunkView;->a(Landroid/database/Cursor;Lcom/sec/chaton/trunk/a/b;)V

    goto :goto_0

    .line 293
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    if-nez v0, :cond_4

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ITrunkView;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    goto :goto_0

    .line 299
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ca;->m:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/trunk/a/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    iput-object v0, p0, Lcom/sec/chaton/trunk/ca;->m:Lcom/sec/chaton/trunk/a/b;

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ITrunkView;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    goto :goto_0

    .line 306
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->isStarted()Z

    move-result v0

    if-nez v0, :cond_6

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->startLoading()V

    .line 310
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->forceLoad()V

    goto :goto_0
.end method

.method static synthetic l(Lcom/sec/chaton/trunk/ca;)Lcom/sec/chaton/trunk/ITrunkView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    sget-object v1, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    if-ne v0, v1, :cond_1

    .line 320
    sget-object v0, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    iput-object v0, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    sget-object v1, Lcom/sec/chaton/trunk/a/b;->a:Lcom/sec/chaton/trunk/a/b;

    if-ne v0, v1, :cond_0

    .line 322
    sget-object v0, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    iput-object v0, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 366
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ca;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 377
    :goto_0
    return-void

    .line 370
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 371
    const-string v0, "TrunkPresenter.onLoadFinished()"

    sget-object v1, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const-string v0, "Loader id: %s"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v0, "Cursor count: %s"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v0, p2, v1}, Lcom/sec/chaton/trunk/ITrunkView;->a(Landroid/database/Cursor;Lcom/sec/chaton/trunk/a/b;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 397
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 398
    const-string v0, "TrunkPresenter.onStorageStateChanged. externalStorageAvailable=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ca;->k()V

    .line 402
    return-void
.end method

.method protected f()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 131
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->f()V

    .line 133
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 134
    const-string v0, "TrunkPresenter.onCreate()"

    sget-object v3, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ITrunkView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/trunk/ca;->e:Landroid/app/Activity;

    .line 139
    new-instance v0, Lcom/sec/chaton/trunk/a/a;

    iget-object v3, p0, Lcom/sec/chaton/trunk/ca;->e:Landroid/app/Activity;

    iget-object v4, p0, Lcom/sec/chaton/trunk/ca;->r:Landroid/os/Handler;

    invoke-direct {v0, v3, v4}, Lcom/sec/chaton/trunk/a/a;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/ca;->i:Lcom/sec/chaton/trunk/a/a;

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    new-instance v3, Lcom/sec/chaton/trunk/cc;

    invoke-direct {v3, p0}, Lcom/sec/chaton/trunk/cc;-><init>(Lcom/sec/chaton/trunk/ca;)V

    invoke-virtual {v0, v3}, Lcom/sec/chaton/trunk/ITrunkView;->a(Lcom/sec/chaton/trunk/p;)V

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    new-instance v3, Lcom/sec/chaton/trunk/cd;

    invoke-direct {v3, p0}, Lcom/sec/chaton/trunk/cd;-><init>(Lcom/sec/chaton/trunk/ca;)V

    invoke-virtual {v0, v3}, Lcom/sec/chaton/trunk/ITrunkView;->a(Lcom/sec/chaton/trunk/n;)V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    new-instance v3, Lcom/sec/chaton/trunk/ce;

    invoke-direct {v3, p0}, Lcom/sec/chaton/trunk/ce;-><init>(Lcom/sec/chaton/trunk/ca;)V

    invoke-virtual {v0, v3}, Lcom/sec/chaton/trunk/ITrunkView;->a(Lcom/sec/chaton/trunk/o;)V

    .line 198
    sget-boolean v0, Lcom/sec/chaton/trunk/ca;->p:Z

    if-eqz v0, :cond_2

    .line 199
    sput-boolean v1, Lcom/sec/chaton/trunk/ca;->p:Z

    .line 201
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 202
    const-string v0, "Clear all trunk item."

    sget-object v3, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_1
    sget-object v0, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "effect"

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "rearrangeLoadMoreItem"

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 209
    new-instance v0, Lcom/sec/chaton/trunk/cf;

    iget-object v4, p0, Lcom/sec/chaton/trunk/ca;->e:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-direct {v0, p0, v4}, Lcom/sec/chaton/trunk/cf;-><init>(Lcom/sec/chaton/trunk/ca;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/sec/chaton/trunk/ca;->q:Lcom/sec/chaton/trunk/cf;

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->q:Lcom/sec/chaton/trunk/cf;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/trunk/cf;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 213
    :cond_2
    return-void
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 217
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->g()V

    .line 219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/ca;->o:Z

    .line 221
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 222
    const-string v0, "TrunkPresenter.onResume()"

    sget-object v1, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_0
    invoke-static {p0}, Lcom/sec/chaton/trunk/c/c;->a(Lcom/sec/chaton/trunk/c/e;)V

    .line 228
    invoke-direct {p0}, Lcom/sec/chaton/trunk/ca;->k()V

    .line 229
    return-void
.end method

.method protected h()V
    .locals 2

    .prologue
    .line 233
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->h()V

    .line 235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/trunk/ca;->o:Z

    .line 237
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 238
    const-string v0, "TrunkPresenter.onPause()"

    sget-object v1, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->stopLoading()V

    .line 247
    :cond_1
    invoke-static {p0}, Lcom/sec/chaton/trunk/c/c;->b(Lcom/sec/chaton/trunk/c/e;)V

    .line 248
    return-void
.end method

.method protected i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 252
    invoke-super {p0}, Lcom/sec/chaton/trunk/a;->i()V

    .line 254
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 255
    const-string v0, "TrunkPresenter.onDestroy()"

    sget-object v1, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->q:Lcom/sec/chaton/trunk/cf;

    if-eqz v0, :cond_1

    .line 261
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/trunk/ca;->p:Z

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->q:Lcom/sec/chaton/trunk/cf;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/trunk/cf;->cancelOperation(I)V

    .line 264
    iput-object v2, p0, Lcom/sec/chaton/trunk/ca;->q:Lcom/sec/chaton/trunk/cf;

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    if-eqz v0, :cond_2

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/ITrunkView;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 270
    iput-object v2, p0, Lcom/sec/chaton/trunk/ca;->n:Landroid/support/v4/content/Loader;

    .line 274
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->j:Lcom/sec/chaton/trunk/a/a/d;

    if-eqz v0, :cond_3

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->j:Lcom/sec/chaton/trunk/a/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/a/a/d;->c()V

    .line 276
    iput-object v2, p0, Lcom/sec/chaton/trunk/ca;->j:Lcom/sec/chaton/trunk/a/a/d;

    .line 278
    :cond_3
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 328
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 329
    const-string v0, "TrunkPresenter.onCreateLoader()"

    sget-object v2, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    sget-object v2, Lcom/sec/chaton/trunk/a/b;->b:Lcom/sec/chaton/trunk/a/b;

    if-ne v0, v2, :cond_1

    .line 337
    const-string v4, "session_id = ?"

    .line 338
    new-array v5, v9, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->g:Ljava/lang/String;

    aput-object v0, v5, v1

    .line 339
    const-string v6, "registration_time DESC"

    .line 353
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 354
    const-string v0, "Selection: %s"

    new-array v2, v9, [Ljava/lang/Object;

    aput-object v4, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    array-length v2, v5

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v5, v0

    .line 356
    const-string v7, "SelectionArg: %s"

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v3, v8, v1

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v7, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v3, v7}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 347
    :cond_1
    const-string v4, "session_id = ?"

    .line 348
    new-array v5, v9, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->g:Ljava/lang/String;

    aput-object v0, v5, v1

    .line 350
    const-string v6, "last_comment_time DESC"

    goto :goto_0

    .line 358
    :cond_2
    const-string v0, "SortOrder: "

    new-array v2, v9, [Ljava/lang/Object;

    aput-object v6, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_3
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/chaton/trunk/ca;->e:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/trunk/database/c;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 40
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/trunk/ca;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 381
    invoke-virtual {p0}, Lcom/sec/chaton/trunk/ca;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    :goto_0
    return-void

    .line 385
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 386
    const-string v0, "TrunkPresenter.onLoadReset()"

    sget-object v1, Lcom/sec/chaton/trunk/ca;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/trunk/ca;->d:Lcom/sec/chaton/trunk/ITrunkView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/trunk/ca;->f:Lcom/sec/chaton/trunk/a/b;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/trunk/ITrunkView;->a(Landroid/database/Cursor;Lcom/sec/chaton/trunk/a/b;)V

    goto :goto_0
.end method
