.class Lcom/sec/chaton/trunk/s;
.super Landroid/os/Handler;
.source "TrunkAdapter.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/trunk/r;


# direct methods
.method constructor <init>(Lcom/sec/chaton/trunk/r;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/chaton/trunk/s;->a:Lcom/sec/chaton/trunk/r;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    .line 86
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/trunk/s;->a:Lcom/sec/chaton/trunk/r;

    invoke-static {v0}, Lcom/sec/chaton/trunk/r;->a(Lcom/sec/chaton/trunk/r;)Lcom/sec/chaton/trunk/TrunkView;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/chaton/trunk/TrunkView;->a:Z

    if-eqz v0, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 95
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/trunk/b/e;

    .line 97
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 101
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->d()Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/trunk/x;

    .line 104
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Download complete: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ImageView: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/x;->b()Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/trunk/r;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v3, p0, Lcom/sec/chaton/trunk/s;->a:Lcom/sec/chaton/trunk/r;

    invoke-static {v3}, Lcom/sec/chaton/trunk/r;->b(Lcom/sec/chaton/trunk/r;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/e;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v3, v4, v0, v5}, Lcom/sec/chaton/trunk/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 112
    new-instance v0, Lcom/sec/chaton/trunk/al;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/x;->a()Lcom/sec/chaton/trunk/c/g;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/x;->d()Landroid/widget/ImageView;

    move-result-object v5

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/sec/chaton/trunk/al;-><init>(Landroid/net/Uri;Lcom/sec/chaton/trunk/c/g;Ljava/lang/Boolean;Landroid/widget/ImageView;)V

    .line 113
    iget-object v2, p0, Lcom/sec/chaton/trunk/s;->a:Lcom/sec/chaton/trunk/r;

    invoke-static {v2}, Lcom/sec/chaton/trunk/r;->c(Lcom/sec/chaton/trunk/r;)Lcom/sec/common/f/c;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/chaton/trunk/x;->b()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 117
    invoke-virtual {v1}, Lcom/sec/chaton/trunk/x;->c()Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0

    .line 124
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Download canceled. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/trunk/r;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 131
    :pswitch_2
    const-string v0, "Download failed"

    invoke-static {}, Lcom/sec/chaton/trunk/r;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/trunk/b/f;

    .line 135
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/f;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 139
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/b/f;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/trunk/x;

    .line 141
    iget-object v1, p0, Lcom/sec/chaton/trunk/s;->a:Lcom/sec/chaton/trunk/r;

    invoke-static {v1}, Lcom/sec/chaton/trunk/r;->c(Lcom/sec/chaton/trunk/r;)Lcom/sec/common/f/c;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/x;->b()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 144
    iget-object v1, p0, Lcom/sec/chaton/trunk/s;->a:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/x;->b()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/x;->a()Lcom/sec/chaton/trunk/c/g;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/x;->d()Landroid/widget/ImageView;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/chaton/trunk/r;->a(Lcom/sec/chaton/trunk/r;Landroid/widget/ImageView;Lcom/sec/chaton/trunk/c/g;Landroid/widget/ImageView;)V

    .line 146
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/x;->a()Lcom/sec/chaton/trunk/c/g;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->b:Lcom/sec/chaton/trunk/c/g;

    if-eq v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/x;->a()Lcom/sec/chaton/trunk/c/g;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/trunk/c/g;->c:Lcom/sec/chaton/trunk/c/g;

    if-ne v1, v2, :cond_3

    .line 147
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/trunk/s;->a:Lcom/sec/chaton/trunk/r;

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/x;->d()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/trunk/x;->a()Lcom/sec/chaton/trunk/c/g;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/trunk/r;->a(Lcom/sec/chaton/trunk/r;Landroid/widget/ImageView;Lcom/sec/chaton/trunk/c/g;)V

    .line 150
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/trunk/x;->c()Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
