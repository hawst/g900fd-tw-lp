.class public Lcom/sec/chaton/IntentControllerActivity;
.super Lcom/sec/chaton/base/BaseActivity;
.source "IntentControllerActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/IntentControllerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 27
    const-string v1, "skipSplash"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 29
    return-object v0
.end method


# virtual methods
.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 80
    const v0, 0x7f0c00fd

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 75
    const v0, 0x7f0c00fd

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lcom/sec/chaton/IntentControllerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 45
    const/high16 v0, 0x100000

    invoke-static {v1, v0}, Lcom/sec/common/util/k;->a(Landroid/content/Intent;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    invoke-static {p0}, Lcom/sec/chaton/HomeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 48
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0}, Lcom/sec/chaton/IntentControllerActivity;->isTaskRoot()Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 55
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/chaton/IntentControllerActivity;->startActivity(Landroid/content/Intent;)V

    .line 70
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/IntentControllerActivity;->finish()V

    .line 71
    return-void

    .line 57
    :cond_1
    const-string v0, "skipSplash"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 59
    const-string v2, "packageName"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 61
    if-eqz v2, :cond_3

    .line 62
    const-string v3, "com.coolots.chaton"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "com.coolots.chatonforcanada"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 63
    :cond_2
    const/4 v0, 0x1

    .line 67
    :cond_3
    invoke-static {p0, v1, v0}, Lcom/sec/chaton/HomeActivity;->a(Landroid/content/Context;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/IntentControllerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
