.class public Lcom/sec/chaton/widget/WeightHorizontalLayout;
.super Landroid/view/ViewGroup;
.source "WeightHorizontalLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 133
    invoke-virtual {p0}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->getChildCount()I

    move-result v3

    .line 135
    const/4 v1, 0x2

    if-ge v3, v1, :cond_1

    .line 155
    :cond_0
    return-void

    :cond_1
    move v2, v0

    move v1, v0

    .line 141
    :goto_0
    if-ge v2, v3, :cond_0

    .line 142
    invoke-virtual {p0, v2}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 144
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-eq v0, v5, :cond_2

    .line 145
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 146
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v5, v1

    .line 147
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v5

    .line 148
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 150
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v7

    invoke-virtual {v4, v5, v7, v6, v8}, Landroid/view/View;->layout(IIII)V

    .line 152
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v6

    add-int/2addr v0, v1

    .line 141
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 18

    .prologue
    .line 40
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v15

    .line 41
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v16

    .line 44
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    .line 45
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    .line 47
    const/4 v14, 0x0

    .line 51
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->getChildCount()I

    move-result v1

    .line 53
    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 129
    :goto_0
    return-void

    .line 57
    :cond_0
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 58
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v17

    .line 59
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 60
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 65
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_2

    .line 66
    const/4 v1, 0x0

    invoke-static {v12, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 67
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v3, v9, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v3

    iget v3, v9, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v3

    .line 74
    :goto_1
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_3

    .line 75
    const/4 v3, 0x0

    invoke-static {v12, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, v17

    move/from16 v7, p2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 76
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget v4, v10, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v4, v10, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    .line 82
    :goto_2
    add-int v13, v1, v3

    .line 85
    if-gt v13, v12, :cond_4

    .line 86
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_1

    .line 87
    sub-int v1, v12, v1

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, v17

    move/from16 v7, p2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 88
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v3, v10, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v3

    iget v3, v10, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v3

    .line 99
    :cond_1
    :goto_3
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 101
    sparse-switch v15, :sswitch_data_0

    move v2, v14

    .line 115
    :goto_4
    sparse-switch v16, :sswitch_data_1

    .line 128
    :goto_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->setMeasuredDimension(II)V

    goto/16 :goto_0

    .line 70
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 79
    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 93
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v4, 0x8

    if-eq v1, v4, :cond_1

    .line 94
    sub-int v1, v12, v3

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/sec/chaton/widget/WeightHorizontalLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 95
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v3, v9, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v3

    iget v3, v9, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v3

    goto :goto_3

    .line 103
    :sswitch_0
    invoke-static {v13, v12}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_4

    :sswitch_1
    move v2, v12

    .line 108
    goto :goto_4

    :sswitch_2
    move v2, v13

    .line 111
    goto :goto_4

    .line 117
    :sswitch_3
    invoke-static {v1, v11}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_5

    :sswitch_4
    move v1, v11

    .line 122
    goto :goto_5

    .line 101
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_1
    .end sparse-switch

    .line 115
    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x40000000 -> :sswitch_4
    .end sparse-switch
.end method
