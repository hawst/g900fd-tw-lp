.class public Lcom/sec/chaton/widget/ImageTextViewGroup;
.super Landroid/widget/LinearLayout;
.source "ImageTextViewGroup.java"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/widget/ImageTextViewGroup;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 31
    sget-object v0, Lcom/sec/chaton/l/g;->ImageTextViewGroup:[I

    sget v1, Lcom/sec/chaton/l/b;->imageTextViewGroupStyle:I

    invoke-virtual {p1, p2, v0, v1, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 33
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 34
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    .line 35
    const v3, 0x1010034

    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 36
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 38
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 41
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->a:Landroid/widget/ImageView;

    .line 42
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setDuplicateParentStateEnabled(Z)V

    .line 43
    if-eqz v1, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v5, v5, v2, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->a:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    new-instance v0, Lcom/sec/chaton/widget/AdaptableTextView;

    invoke-direct {v0, p1, v8}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->b:Landroid/widget/TextView;

    .line 52
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setDuplicateParentStateEnabled(Z)V

    .line 53
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->b:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->b:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    return-void
.end method


# virtual methods
.method public setDrawablePadding(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v1, p1, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 71
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    return-void
.end method

.method public setImageResource(I)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 63
    return-void
.end method

.method public setText(I)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 75
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/chaton/widget/ImageTextViewGroup;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 87
    return-void
.end method
