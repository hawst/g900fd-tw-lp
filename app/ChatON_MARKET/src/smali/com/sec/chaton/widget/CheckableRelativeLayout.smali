.class public Lcom/sec/chaton/widget/CheckableRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "CheckableRelativeLayout.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field private c:Z

.field private d:I

.field private e:I

.field private f:Landroid/widget/CompoundButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const-string v0, "http://schemas.android.com/apk/res-auto"

    iput-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->a:Ljava/lang/String;

    .line 23
    const-string v0, "checkable_widget"

    iput-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->b:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->d:I

    .line 34
    const-string v0, "http://schemas.android.com/apk/res-auto"

    const-string v1, "checkable_widget"

    const/4 v2, -0x1

    invoke-interface {p2, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->e:I

    .line 35
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->c:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->e:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    .line 45
    :cond_0
    iget v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->d:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChoiceMode(I)V

    .line 47
    iget-boolean v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->c:Z

    invoke-virtual {p0, v0}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChecked(Z)V

    .line 49
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 50
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    .line 57
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->c:Z

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    if-nez v0, :cond_0

    .line 68
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0
.end method

.method public setChoiceMode(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 92
    iput p1, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->d:I

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    if-nez v0, :cond_0

    .line 124
    :goto_0
    return-void

    .line 98
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 100
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setVisibility(I)V

    goto :goto_0

    .line 105
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 107
    invoke-virtual {p0}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    new-array v1, v1, [I

    const v2, 0x1010219

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 109
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    .line 116
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    new-array v1, v1, [I

    const v2, 0x101021a

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 120
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->c:Z

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    if-nez v0, :cond_1

    .line 84
    :goto_1
    return-void

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/widget/CheckableRelativeLayout;->f:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->toggle()V

    goto :goto_1
.end method
