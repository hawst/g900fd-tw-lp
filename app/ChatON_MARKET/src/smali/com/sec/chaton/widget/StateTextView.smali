.class public Lcom/sec/chaton/widget/StateTextView;
.super Lcom/sec/chaton/widget/AdaptableTextView;
.source "StateTextView.java"


# instance fields
.field private a:Lcom/sec/chaton/widget/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/widget/StateTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/sec/chaton/l/b;->stateTextViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/widget/StateTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    new-instance v0, Lcom/sec/chaton/widget/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, p3, v1}, Lcom/sec/chaton/widget/l;-><init>(Landroid/widget/TextView;Landroid/util/AttributeSet;ILcom/sec/chaton/widget/k;)V

    iput-object v0, p0, Lcom/sec/chaton/widget/StateTextView;->a:Lcom/sec/chaton/widget/l;

    .line 35
    return-void
.end method

.method static synthetic a()[I
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/chaton/widget/StateTextView;->PRESSED_ENABLED_STATE_SET:[I

    return-object v0
.end method

.method static synthetic b()[I
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/chaton/widget/StateTextView;->ENABLED_FOCUSED_STATE_SET:[I

    return-object v0
.end method

.method static synthetic c()[I
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/chaton/widget/StateTextView;->ENABLED_SELECTED_STATE_SET:[I

    return-object v0
.end method

.method static synthetic d()[I
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/chaton/widget/StateTextView;->ENABLED_STATE_SET:[I

    return-object v0
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lcom/sec/chaton/widget/AdaptableTextView;->drawableStateChanged()V

    .line 41
    iget-object v0, p0, Lcom/sec/chaton/widget/StateTextView;->a:Lcom/sec/chaton/widget/l;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/chaton/widget/StateTextView;->a:Lcom/sec/chaton/widget/l;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/l;->a()V

    .line 44
    :cond_0
    return-void
.end method
