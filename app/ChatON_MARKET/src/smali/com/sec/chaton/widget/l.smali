.class public final Lcom/sec/chaton/widget/l;
.super Ljava/lang/Object;
.source "StateTextView.java"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method private constructor <init>(Landroid/widget/TextView;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    .line 60
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/l/g;->StateTextView:[I

    invoke-virtual {v0, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 62
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/widget/l;->b:I

    .line 65
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/widget/l;->c:I

    .line 68
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/widget/l;->d:I

    .line 71
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/widget/l;->e:I

    .line 74
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/widget/l;->f:I

    .line 77
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 79
    invoke-virtual {p0}, Lcom/sec/chaton/widget/l;->a()V

    .line 80
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/TextView;Landroid/util/AttributeSet;ILcom/sec/chaton/widget/k;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/widget/l;-><init>(Landroid/widget/TextView;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static a(Landroid/widget/TextView;Landroid/util/AttributeSet;I)Lcom/sec/chaton/widget/l;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/sec/chaton/widget/l;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/chaton/widget/l;-><init>(Landroid/widget/TextView;Landroid/util/AttributeSet;I)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getDrawableState()[I

    move-result-object v0

    .line 85
    iget v1, p0, Lcom/sec/chaton/widget/l;->d:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/chaton/widget/StateTextView;->a()[I

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/StateSet;->stateSetMatches([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/widget/l;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 107
    :goto_0
    return-void

    .line 88
    :cond_0
    iget v1, p0, Lcom/sec/chaton/widget/l;->e:I

    if-eq v1, v2, :cond_1

    invoke-static {}, Lcom/sec/chaton/widget/StateTextView;->b()[I

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/StateSet;->stateSetMatches([I[I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/widget/l;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 91
    :cond_1
    iget v1, p0, Lcom/sec/chaton/widget/l;->c:I

    if-eq v1, v2, :cond_2

    invoke-static {}, Lcom/sec/chaton/widget/StateTextView;->c()[I

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/StateSet;->stateSetMatches([I[I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/widget/l;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 95
    :cond_2
    invoke-static {}, Lcom/sec/chaton/widget/StateTextView;->d()[I

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/StateSet;->stateSetMatches([I[I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/widget/l;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 99
    :cond_3
    iget v0, p0, Lcom/sec/chaton/widget/l;->f:I

    if-eq v0, v2, :cond_4

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/widget/l;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 103
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/widget/l;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/widget/l;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0
.end method
