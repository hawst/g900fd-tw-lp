.class public Lcom/sec/chaton/widget/SelectableImageView;
.super Landroid/widget/ImageView;
.source "SelectableImageView.java"


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/widget/SelectableImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    sget-object v0, Lcom/sec/chaton/l/g;->SelectableImageView:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 41
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/widget/SelectableImageView;->a:Landroid/graphics/drawable/Drawable;

    .line 43
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    invoke-virtual {p0, v2}, Lcom/sec/chaton/widget/SelectableImageView;->setFocusable(Z)V

    .line 46
    invoke-virtual {p0, v2}, Lcom/sec/chaton/widget/SelectableImageView;->setClickable(Z)V

    .line 47
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/widget/SelectableImageView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/sec/chaton/widget/SelectableImageView;->invalidate()V

    .line 76
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 60
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 62
    iget-object v0, p0, Lcom/sec/chaton/widget/SelectableImageView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/widget/SelectableImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/sec/chaton/widget/SelectableImageView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 64
    iget-object v0, p0, Lcom/sec/chaton/widget/SelectableImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/sec/chaton/widget/SelectableImageView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/chaton/widget/SelectableImageView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/widget/SelectableImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 67
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 53
    invoke-virtual {p0}, Lcom/sec/chaton/widget/SelectableImageView;->invalidate()V

    .line 55
    return v0
.end method
