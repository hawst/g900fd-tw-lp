.class public Lcom/sec/chaton/widget/ProfileImageView;
.super Landroid/widget/ImageView;
.source "ProfileImageView.java"


# instance fields
.field a:I

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Landroid/view/View$OnClickListener;

.field private g:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->e:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->a:I

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->b:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Lcom/sec/chaton/widget/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/widget/g;-><init>(Lcom/sec/chaton/widget/ProfileImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->f:Landroid/view/View$OnClickListener;

    .line 101
    new-instance v0, Lcom/sec/chaton/widget/h;

    invoke-direct {v0, p0}, Lcom/sec/chaton/widget/h;-><init>(Lcom/sec/chaton/widget/ProfileImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->g:Landroid/view/View$OnClickListener;

    .line 33
    iput-object p1, p0, Lcom/sec/chaton/widget/ProfileImageView;->c:Landroid/content/Context;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->e:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->a:I

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->b:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Lcom/sec/chaton/widget/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/widget/g;-><init>(Lcom/sec/chaton/widget/ProfileImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->f:Landroid/view/View$OnClickListener;

    .line 101
    new-instance v0, Lcom/sec/chaton/widget/h;

    invoke-direct {v0, p0}, Lcom/sec/chaton/widget/h;-><init>(Lcom/sec/chaton/widget/ProfileImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->g:Landroid/view/View$OnClickListener;

    .line 43
    iput-object p1, p0, Lcom/sec/chaton/widget/ProfileImageView;->c:Landroid/content/Context;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->e:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->a:I

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->b:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Lcom/sec/chaton/widget/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/widget/g;-><init>(Lcom/sec/chaton/widget/ProfileImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->f:Landroid/view/View$OnClickListener;

    .line 101
    new-instance v0, Lcom/sec/chaton/widget/h;

    invoke-direct {v0, p0}, Lcom/sec/chaton/widget/h;-><init>(Lcom/sec/chaton/widget/ProfileImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->g:Landroid/view/View$OnClickListener;

    .line 38
    iput-object p1, p0, Lcom/sec/chaton/widget/ProfileImageView;->c:Landroid/content/Context;

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/widget/ProfileImageView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/widget/ProfileImageView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/widget/ProfileImageView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;Lcom/sec/chaton/e/r;)V
    .locals 2

    .prologue
    .line 56
    sget-object v0, Lcom/sec/chaton/widget/i;->a:[I

    invoke-virtual {p2}, Lcom/sec/chaton/e/r;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 73
    :goto_0
    return-void

    .line 58
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/widget/ProfileImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    check-cast p1, Landroid/widget/ImageView;

    sget-object v1, Lcom/sec/chaton/util/bw;->a:Lcom/sec/chaton/util/bw;

    invoke-virtual {v0, p1, v1}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/widget/ProfileImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 63
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/widget/ProfileImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    check-cast p1, Landroid/widget/ImageView;

    sget-object v1, Lcom/sec/chaton/util/bw;->c:Lcom/sec/chaton/util/bw;

    invoke-virtual {v0, p1, v1}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    .line 64
    iget-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/widget/ProfileImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 69
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/chaton/widget/ProfileImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    check-cast p1, Landroid/widget/ImageView;

    sget-object v1, Lcom/sec/chaton/util/bw;->b:Lcom/sec/chaton/util/bw;

    invoke-virtual {v0, p1, v1}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/widget/ProfileImageView;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/widget/ProfileImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public setBuddyName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/chaton/widget/ProfileImageView;->e:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setBuddyNo(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/chaton/widget/ProfileImageView;->d:Ljava/lang/String;

    .line 48
    return-void
.end method
