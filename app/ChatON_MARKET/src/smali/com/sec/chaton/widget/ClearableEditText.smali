.class public Lcom/sec/chaton/widget/ClearableEditText;
.super Landroid/widget/FrameLayout;
.source "ClearableEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/chaton/widget/e;


# instance fields
.field private a:Landroid/view/ViewGroup;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/ImageButton;

.field private d:I

.field private e:Lcom/sec/chaton/widget/b;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private f:Landroid/view/View$OnClickListener;

.field private g:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    sget v0, Lcom/sec/chaton/l/b;->clearableEditTextStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/widget/ClearableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 151
    sget-object v0, Lcom/sec/chaton/l/g;->ClearableEditText:[I

    invoke-virtual {p1, p2, v0, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 153
    const/4 v0, 0x5

    sget v2, Lcom/sec/chaton/l/f;->layout_common_clearable_edit_text:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 155
    invoke-virtual {p0}, Lcom/sec/chaton/widget/ClearableEditText;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v0, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->a:Landroid/view/ViewGroup;

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/widget/ClearableEditText;->addView(Landroid/view/View;)V

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->a:Landroid/view/ViewGroup;

    instance-of v0, v0, Lcom/sec/chaton/widget/d;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->a:Landroid/view/ViewGroup;

    check-cast v0, Lcom/sec/chaton/widget/d;

    invoke-interface {v0, p0}, Lcom/sec/chaton/widget/d;->setOnDrawableStateChanged(Lcom/sec/chaton/widget/e;)V

    .line 161
    invoke-virtual {p0}, Lcom/sec/chaton/widget/ClearableEditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_0

    .line 164
    iget-object v2, p0, Lcom/sec/chaton/widget/ClearableEditText;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getDrawableState()[I

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->a:Landroid/view/ViewGroup;

    sget v2, Lcom/sec/chaton/l/d;->clearable_text1:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->setSingleLine()V

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 173
    invoke-virtual {v1, v6, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 174
    iget-object v2, p0, Lcom/sec/chaton/widget/ClearableEditText;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setMinimumHeight(I)V

    .line 176
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 177
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 178
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getInputType()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 179
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getImeOptions()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 181
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 183
    if-lez v0, :cond_1

    .line 184
    invoke-virtual {p0, v0}, Lcom/sec/chaton/widget/ClearableEditText;->setMaxLength(I)V

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->a:Landroid/view/ViewGroup;

    sget v1, Lcom/sec/chaton/l/d;->clearable_button1:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->c:Landroid/widget/ImageButton;

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v6

    iput-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->g:Landroid/graphics/drawable/Drawable;

    .line 197
    invoke-direct {p0}, Lcom/sec/chaton/widget/ClearableEditText;->c()V

    .line 198
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/chaton/widget/ClearableEditText;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/EditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 206
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->c:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 212
    :goto_0
    return-void

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/EditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->c:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 116
    return-void
.end method

.method public a(Landroid/view/View;[I)V
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/sec/chaton/widget/ClearableEditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 250
    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 253
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 80
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public b(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 120
    return-void
.end method

.method public b()[Landroid/text/InputFilter;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getFilters()[Landroid/text/InputFilter;

    move-result-object v0

    return-object v0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->f:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->f:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 244
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 221
    iget v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->d:I

    if-lez v0, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget v1, p0, Lcom/sec/chaton/widget/ClearableEditText;->d:I

    if-ne v0, v1, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->e:Lcom/sec/chaton/widget/b;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->e:Lcom/sec/chaton/widget/b;

    invoke-interface {v0}, Lcom/sec/chaton/widget/b;->a()V

    .line 227
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/widget/ClearableEditText;->c()V

    .line 228
    return-void
.end method

.method public setClearButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/chaton/widget/ClearableEditText;->f:Landroid/view/View$OnClickListener;

    .line 60
    return-void
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 136
    return-void
.end method

.method public setHint(I)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(I)V

    .line 94
    return-void
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 90
    return-void
.end method

.method public setImeOptions(I)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 124
    return-void
.end method

.method public setInputType(I)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setInputType(I)V

    .line 144
    return-void
.end method

.method public setMaxLength(I)V
    .locals 5

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/chaton/widget/ClearableEditText;->d:I

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    iget v4, p0, Lcom/sec/chaton/widget/ClearableEditText;->d:I

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 100
    return-void
.end method

.method public setMinimumHeight(I)V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setMinimumHeight(I)V

    .line 66
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setMinimumHeight(I)V

    .line 67
    return-void
.end method

.method public setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 132
    return-void
.end method

.method public setOnMaxLengthReachListener(Lcom/sec/chaton/widget/b;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/chaton/widget/ClearableEditText;->e:Lcom/sec/chaton/widget/b;

    .line 47
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setSelection(I)V

    .line 108
    return-void
.end method

.method public setSelection(II)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->setSelection(II)V

    .line 112
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/widget/ClearableEditText;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 75
    return-void
.end method
