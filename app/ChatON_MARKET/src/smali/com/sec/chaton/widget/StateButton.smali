.class public Lcom/sec/chaton/widget/StateButton;
.super Landroid/widget/Button;
.source "StateButton.java"


# instance fields
.field private a:Lcom/sec/chaton/widget/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/widget/StateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 26
    sget v0, Lcom/sec/chaton/l/b;->stateButtonStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/widget/StateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    invoke-static {p0, p2, p3}, Lcom/sec/chaton/widget/l;->a(Landroid/widget/TextView;Landroid/util/AttributeSet;I)Lcom/sec/chaton/widget/l;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/widget/StateButton;->a:Lcom/sec/chaton/widget/l;

    .line 33
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Landroid/widget/Button;->drawableStateChanged()V

    .line 39
    iget-object v0, p0, Lcom/sec/chaton/widget/StateButton;->a:Lcom/sec/chaton/widget/l;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/chaton/widget/StateButton;->a:Lcom/sec/chaton/widget/l;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/l;->a()V

    .line 42
    :cond_0
    return-void
.end method
