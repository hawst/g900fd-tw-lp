.class public Lcom/sec/chaton/widget/AdaptableEditText;
.super Landroid/widget/EditText;
.source "AdaptableEditText.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 24
    invoke-static {p0}, Lcom/sec/chaton/widget/a;->a(Landroid/widget/TextView;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-static {p0}, Lcom/sec/chaton/widget/a;->a(Landroid/widget/TextView;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    invoke-static {p0}, Lcom/sec/chaton/widget/a;->a(Landroid/widget/TextView;)V

    .line 37
    return-void
.end method


# virtual methods
.method public setFilters([Landroid/text/InputFilter;)V
    .locals 5

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 43
    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 44
    instance-of v3, v0, Lcom/sec/chaton/util/bn;

    if-eqz v3, :cond_0

    .line 45
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/sec/chaton/widget/AdaptableEditText;->getInputExtras(Z)Landroid/os/Bundle;

    move-result-object v3

    .line 46
    const-string v4, "maxLength"

    check-cast v0, Lcom/sec/chaton/util/bn;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bn;->a()I

    move-result v0

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 43
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 49
    :cond_1
    return-void
.end method
