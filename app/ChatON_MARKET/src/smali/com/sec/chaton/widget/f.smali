.class public Lcom/sec/chaton/widget/f;
.super Landroid/app/AlertDialog;
.source "MultideviceSyncProgressDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 22
    invoke-direct {p0}, Lcom/sec/chaton/widget/f;->a()V

    .line 23
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/chaton/widget/f;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c2

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 40
    const v0, 0x7f07036a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/widget/f;->b:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f070369

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/widget/f;->a:Landroid/widget/TextView;

    .line 42
    invoke-direct {p0, v1}, Lcom/sec/chaton/widget/f;->a(Landroid/view/View;)V

    .line 43
    invoke-virtual {p0, p0}, Lcom/sec/chaton/widget/f;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 44
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lcom/sec/chaton/widget/f;->setView(Landroid/view/View;)V

    .line 48
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public onShow(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method
