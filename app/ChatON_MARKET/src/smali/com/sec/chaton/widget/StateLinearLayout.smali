.class public Lcom/sec/chaton/widget/StateLinearLayout;
.super Landroid/widget/LinearLayout;
.source "StateLinearLayout.java"

# interfaces
.implements Lcom/sec/chaton/widget/d;


# instance fields
.field private a:Lcom/sec/chaton/widget/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 43
    iget-object v0, p0, Lcom/sec/chaton/widget/StateLinearLayout;->a:Lcom/sec/chaton/widget/e;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/chaton/widget/StateLinearLayout;->a:Lcom/sec/chaton/widget/e;

    invoke-virtual {p0}, Lcom/sec/chaton/widget/StateLinearLayout;->getDrawableState()[I

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lcom/sec/chaton/widget/e;->a(Landroid/view/View;[I)V

    .line 46
    :cond_0
    return-void
.end method

.method public setOnDrawableStateChanged(Lcom/sec/chaton/widget/e;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/chaton/widget/StateLinearLayout;->a:Lcom/sec/chaton/widget/e;

    .line 37
    return-void
.end method
