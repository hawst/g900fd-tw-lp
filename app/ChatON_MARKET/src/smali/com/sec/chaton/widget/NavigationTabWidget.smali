.class public Lcom/sec/chaton/widget/NavigationTabWidget;
.super Lcom/sec/chaton/widget/StateTextView;
.source "NavigationTabWidget.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/widget/StateTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 22
    sget v0, Lcom/sec/chaton/l/b;->actionbarNavigationTabWidgetStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/widget/StateTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/widget/StateTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public setTitleText(I)V
    .locals 0

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/chaton/widget/NavigationTabWidget;->setText(I)V

    .line 39
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/sec/chaton/widget/NavigationTabWidget;->setText(Ljava/lang/CharSequence;)V

    .line 35
    return-void
.end method
