.class public Lcom/sec/chaton/widget/m;
.super Landroid/app/ProgressDialog;
.source "StrongProgressDialog.java"


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/widget/m;-><init>(Landroid/content/Context;Z)V

    .line 20
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-virtual {p0, p2}, Lcom/sec/chaton/widget/m;->setCancelable(Z)V

    .line 26
    new-instance v0, Lcom/sec/chaton/widget/n;

    invoke-direct {v0, p0}, Lcom/sec/chaton/widget/n;-><init>(Lcom/sec/chaton/widget/m;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/widget/m;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 36
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;
    .locals 6

    .prologue
    .line 43
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;
    .locals 6

    .prologue
    .line 47
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/sec/chaton/widget/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/widget/m;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {v0, p2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {v0, p3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 55
    invoke-virtual {v0, p4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 56
    invoke-virtual {v0, p5}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 57
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 58
    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/sec/chaton/widget/m;
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/sec/chaton/widget/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/widget/m;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/sec/chaton/widget/m;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/widget/m;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method
