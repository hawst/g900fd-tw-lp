.class public Lcom/sec/chaton/io/entry/inner/Msg;
.super Lcom/sec/chaton/io/entry/Entry;
.source "Msg.java"


# instance fields
.field public address:Ljava/lang/String;

.field public chattype:Ljava/lang/Integer;

.field public id:Ljava/lang/String;

.field public msgFromLang:Ljava/lang/String;

.field public msgToLang:Ljava/lang/String;

.field public msgTranslated:Ljava/lang/String;

.field public port:Ljava/lang/Integer;

.field public receiver:Ljava/lang/String;

.field public sender:Ljava/lang/String;

.field public sessionid:Ljava/lang/String;

.field public tid:Ljava/lang/String;

.field public time:Ljava/lang/Long;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/chaton/io/entry/Entry;-><init>()V

    return-void
.end method


# virtual methods
.method public isValid()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/Msg;->tid:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 42
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/Msg;->type:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 46
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/Msg;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/Msg;->sender:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/Msg;->receiver:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/Msg;->sessionid:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 66
    const/4 v0, 0x1

    goto :goto_0
.end method
