.class public Lcom/sec/chaton/io/entry/NotificationEntry;
.super Lcom/sec/chaton/io/entry/Entry;
.source "NotificationEntry.java"


# instance fields
.field public badge:Lcom/sec/chaton/io/entry/inner/Badge;

.field public newfeaturelist:Lcom/sec/chaton/io/entry/inner/NewFeature;

.field public trunklist:Lcom/sec/chaton/io/entry/inner/TrunkList;

.field public utctimestamp:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/chaton/io/entry/Entry;-><init>()V

    .line 19
    new-instance v0, Lcom/sec/chaton/io/entry/inner/Badge;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/Badge;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/io/entry/NotificationEntry;->badge:Lcom/sec/chaton/io/entry/inner/Badge;

    .line 21
    new-instance v0, Lcom/sec/chaton/io/entry/inner/TrunkList;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/TrunkList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/io/entry/NotificationEntry;->trunklist:Lcom/sec/chaton/io/entry/inner/TrunkList;

    .line 22
    new-instance v0, Lcom/sec/chaton/io/entry/inner/NewFeature;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/NewFeature;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/io/entry/NotificationEntry;->newfeaturelist:Lcom/sec/chaton/io/entry/inner/NewFeature;

    return-void
.end method
