.class public Lcom/sec/chaton/io/entry/TranslationAuthEntry;
.super Lcom/sec/chaton/io/entry/Entry;
.source "TranslationAuthEntry.java"


# instance fields
.field private __gldInfo:Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;

.field private __parsed:Z

.field public error:Ljava/lang/String;

.field public error_code:Ljava/lang/Long;

.field public error_description:Ljava/lang/String;

.field public gld_region:Lorg/json/JSONObject;

.field public rcode:Ljava/lang/Long;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/chaton/io/entry/Entry;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->__parsed:Z

    .line 63
    return-void
.end method

.method private getUrl(Lcom/sec/chaton/io/entry/a;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 78
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->parseMore()Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->__gldInfo:Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 115
    :goto_0
    return-object v0

    .line 79
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 80
    goto :goto_0

    .line 88
    :cond_0
    sget-object v0, Lcom/sec/chaton/io/entry/a;->a:Lcom/sec/chaton/io/entry/a;

    if-ne p1, v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->__gldInfo:Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;->primary:Ljava/util/List;

    .line 94
    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_3

    :cond_1
    move-object v0, v1

    .line 95
    goto :goto_0

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->__gldInfo:Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;->secondary:Ljava/util/List;

    goto :goto_1

    .line 98
    :cond_3
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;

    .line 99
    iget-object v2, v0, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->address:Ljava/lang/String;

    .line 100
    iget-object v3, v0, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->port:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 101
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v0, v1

    .line 102
    goto :goto_0

    .line 105
    :cond_4
    const-string v1, "http://"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "https://"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 106
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 110
    :cond_6
    const/16 v1, 0x1bb

    if-ne v3, v1, :cond_7

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->address:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 113
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->address:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private parseMore()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 119
    iget-boolean v2, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->__parsed:Z

    if-ne v2, v0, :cond_0

    .line 160
    :goto_0
    return v0

    .line 123
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->gld_region:Lorg/json/JSONObject;

    if-nez v2, :cond_1

    move v0, v1

    .line 124
    goto :goto_0

    .line 127
    :cond_1
    new-instance v2, Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;-><init>()V

    iput-object v2, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->__gldInfo:Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;

    .line 129
    iget-object v2, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->__gldInfo:Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;

    iget-object v3, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->gld_region:Lorg/json/JSONObject;

    const-string v4, "expdate"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;->expdate:Ljava/lang/Long;

    .line 131
    iget-object v2, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->gld_region:Lorg/json/JSONObject;

    const-string v3, "primary"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 132
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 133
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    move v2, v1

    .line 135
    :goto_1
    if-ge v2, v4, :cond_2

    .line 136
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 137
    new-instance v6, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;

    invoke-direct {v6}, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;-><init>()V

    .line 138
    const-string v7, "region"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->region:Ljava/lang/String;

    .line 139
    const-string v7, "name"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->name:Ljava/lang/String;

    .line 140
    const-string v7, "port"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->port:Ljava/lang/String;

    .line 141
    const-string v7, "address"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v6, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->address:Ljava/lang/String;

    .line 142
    iget-object v5, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->__gldInfo:Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;

    iget-object v5, v5, Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;->primary:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 145
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->gld_region:Lorg/json/JSONObject;

    const-string v4, "secondary"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 146
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 147
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 149
    :goto_2
    if-ge v1, v2, :cond_3

    .line 150
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 151
    new-instance v5, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;

    invoke-direct {v5}, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;-><init>()V

    .line 152
    const-string v6, "region"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->region:Ljava/lang/String;

    .line 153
    const-string v6, "name"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->name:Ljava/lang/String;

    .line 154
    const-string v6, "port"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->port:Ljava/lang/String;

    .line 155
    const-string v6, "address"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/sec/chaton/io/entry/TranslationAuthEntry$Server;->address:Ljava/lang/String;

    .line 156
    iget-object v4, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->__gldInfo:Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;

    iget-object v4, v4, Lcom/sec/chaton/io/entry/TranslationAuthEntry$ServerInfo;->secondary:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 159
    :cond_3
    iput-boolean v0, p0, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->__parsed:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public getUrlPrimary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/chaton/io/entry/a;->a:Lcom/sec/chaton/io/entry/a;

    invoke-direct {p0, v0}, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->getUrl(Lcom/sec/chaton/io/entry/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUrlSecondary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/sec/chaton/io/entry/a;->b:Lcom/sec/chaton/io/entry/a;

    invoke-direct {p0, v0}, Lcom/sec/chaton/io/entry/TranslationAuthEntry;->getUrl(Lcom/sec/chaton/io/entry/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
