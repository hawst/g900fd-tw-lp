.class public final enum Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;
.super Ljava/lang/Enum;
.source "MessageType4Entry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

.field public static final enum ENTRY_TYPE_PUSH:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

.field public static final enum ENTRY_TYPE_WHOLE:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6
    new-instance v0, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    const-string v1, "ENTRY_TYPE_PUSH"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;->ENTRY_TYPE_PUSH:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    .line 7
    new-instance v0, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    const-string v1, "ENTRY_TYPE_WHOLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;->ENTRY_TYPE_WHOLE:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    .line 5
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    sget-object v1, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;->ENTRY_TYPE_PUSH:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;->ENTRY_TYPE_WHOLE:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;->$VALUES:[Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;
    .locals 1

    .prologue
    .line 5
    const-class v0, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;->$VALUES:[Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    invoke-virtual {v0}, [Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    return-object v0
.end method
