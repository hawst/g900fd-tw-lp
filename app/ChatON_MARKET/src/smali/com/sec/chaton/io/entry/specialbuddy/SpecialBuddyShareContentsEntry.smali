.class public Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;
.super Lcom/sec/chaton/io/entry/Entry;
.source "SpecialBuddyShareContentsEntry.java"


# static fields
.field public static final KEY_BUDDY_HASH:Ljava/lang/String; = "hash"

.field public static final KEY_BUDDY_ID:Ljava/lang/String; = "id"

.field public static final KEY_BUDDY_NAME:Ljava/lang/String; = "name"

.field public static final KEY_TITLE:Ljava/lang/String; = "title"

.field public static final KEY_WEB_URL:Ljava/lang/String; = "url"


# instance fields
.field public hash:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/chaton/io/entry/Entry;-><init>()V

    return-void
.end method
