.class public Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;
.super Lcom/sec/chaton/io/entry/Entry;
.source "SpecialBuddyShareEntry.java"


# instance fields
.field public content:Ljava/lang/String;

.field public parsedContent:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/chaton/io/entry/Entry;-><init>()V

    return-void
.end method


# virtual methods
.method public getJsonContents()Lorg/json/JSONObject;
    .locals 2

    .prologue
    .line 23
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;->content:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    :goto_0
    return-object v0

    .line 24
    :catch_0
    move-exception v0

    .line 25
    const-class v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 26
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getParsedContents()Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;->parsedContent:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/sec/chaton/util/v;

    iget-object v1, p0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;->content:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/sec/chaton/util/v;-><init>(Ljava/lang/String;)V

    .line 35
    :try_start_0
    const-class v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/v;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    iput-object v0, p0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;->parsedContent:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;->parsedContent:Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    return-object v0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    const-class v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getRawContents()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;->content:Ljava/lang/String;

    return-object v0
.end method
