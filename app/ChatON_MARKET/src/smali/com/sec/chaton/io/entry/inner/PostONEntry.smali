.class public Lcom/sec/chaton/io/entry/inner/PostONEntry;
.super Lcom/sec/chaton/io/entry/Entry;
.source "PostONEntry.java"


# instance fields
.field public commentreadcount:Ljava/lang/String;

.field public commentunreadcount:Ljava/lang/String;

.field public imgstatus:Ljava/lang/String;

.field public multimedialist:Ljava/util/ArrayList;
    .annotation runtime Lcom/sec/chaton/io/entry/EntryField;
        type = Lcom/sec/chaton/io/entry/inner/PostONMultimediaList;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/PostONMultimediaList;",
            ">;"
        }
    .end annotation
.end field

.field public postonid:Ljava/lang/String;

.field public postonmessage:Ljava/lang/String;

.field public read_msec:Ljava/lang/String;

.field public regdttm:Ljava/lang/String;

.field public sender:Ljava/lang/String;

.field public sendername:Ljava/lang/String;

.field public totalmoodcount:Ljava/lang/String;

.field public unreadmoodcount:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/chaton/io/entry/Entry;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/io/entry/inner/PostONEntry;->multimedialist:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 26
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/PostONEntry;->multimedialist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONMultimediaList;

    .line 28
    const-string v3, "multimedialist:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/PostONMultimediaList;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 31
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
