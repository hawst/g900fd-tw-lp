.class public Lcom/sec/chaton/io/entry/inner/FontType;
.super Ljava/lang/Object;
.source "FontType.java"


# instance fields
.field category:Ljava/lang/String;

.field fileName:Ljava/lang/String;

.field fontFileSize:F

.field fontTitle:Ljava/lang/String;

.field id:I

.field thumbnailImagePath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fontTitle:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fileName:Ljava/lang/String;

    .line 44
    iput p3, p0, Lcom/sec/chaton/io/entry/inner/FontType;->id:I

    .line 45
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/sec/chaton/io/entry/inner/FontType;->thumbnailImagePath:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fileName:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fontTitle:Ljava/lang/String;

    .line 36
    int-to-float v0, p4

    iput v0, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fontFileSize:F

    .line 37
    iput-object p1, p0, Lcom/sec/chaton/io/entry/inner/FontType;->category:Ljava/lang/String;

    .line 38
    iput p6, p0, Lcom/sec/chaton/io/entry/inner/FontType;->id:I

    .line 39
    return-void
.end method


# virtual methods
.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/FontType;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFontFileSize()F
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fontFileSize:F

    return v0
.end method

.method public getFontTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fontTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getFontType()Landroid/graphics/Typeface;
    .locals 3

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/sec/chaton/io/entry/inner/FontType;->getFontTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/io/entry/inner/FontType;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/settings/downloads/bj;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Font added from "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const/4 v0, 0x0

    .line 105
    :try_start_0
    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    .line 106
    :catch_0
    move-exception v1

    .line 107
    const-string v1, "Cannot make font from file"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :cond_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/chaton/io/entry/inner/FontType;->id:I

    return v0
.end method

.method public getThumbnailImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/FontType;->thumbnailImagePath:Ljava/lang/String;

    return-object v0
.end method

.method public retrieveFromPrefereceValue(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fontTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/chaton/io/entry/inner/FontType;->category:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fileName:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setFontFileSize(F)V
    .locals 0

    .prologue
    .line 92
    iput p1, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fontFileSize:F

    .line 93
    return-void
.end method

.method public setFontTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/chaton/io/entry/inner/FontType;->fontTitle:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setId(I)V
    .locals 0

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/chaton/io/entry/inner/FontType;->id:I

    .line 77
    return-void
.end method

.method public setThumbnailImagePath(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/io/entry/inner/FontType;->thumbnailImagePath:Ljava/lang/String;

    .line 53
    return-void
.end method
