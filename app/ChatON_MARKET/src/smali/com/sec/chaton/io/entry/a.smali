.class final enum Lcom/sec/chaton/io/entry/a;
.super Ljava/lang/Enum;
.source "TranslationAuthEntry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/io/entry/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/io/entry/a;

.field public static final enum b:Lcom/sec/chaton/io/entry/a;

.field private static final synthetic c:[Lcom/sec/chaton/io/entry/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    new-instance v0, Lcom/sec/chaton/io/entry/a;

    const-string v1, "PRIMARY"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/io/entry/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/io/entry/a;->a:Lcom/sec/chaton/io/entry/a;

    .line 65
    new-instance v0, Lcom/sec/chaton/io/entry/a;

    const-string v1, "SECONDARY"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/io/entry/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/io/entry/a;->b:Lcom/sec/chaton/io/entry/a;

    .line 63
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/io/entry/a;

    sget-object v1, Lcom/sec/chaton/io/entry/a;->a:Lcom/sec/chaton/io/entry/a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/io/entry/a;->b:Lcom/sec/chaton/io/entry/a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/io/entry/a;->c:[Lcom/sec/chaton/io/entry/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/io/entry/a;
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/sec/chaton/io/entry/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/a;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/io/entry/a;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/chaton/io/entry/a;->c:[Lcom/sec/chaton/io/entry/a;

    invoke-virtual {v0}, [Lcom/sec/chaton/io/entry/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/io/entry/a;

    return-object v0
.end method
