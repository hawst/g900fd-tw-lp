.class public Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$ChatONVApplicatoinInfoParser;
.super Lcom/sec/common/d/a/a;
.source "ChatONVApplicationInfoEntry.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/common/d/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 51
    new-instance v4, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;

    invoke-direct {v4}, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;-><init>()V

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    const/4 v2, 0x0

    .line 57
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 59
    :try_start_1
    const-string v2, ""

    .line 60
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 61
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 62
    new-instance v0, Ljava/lang/InterruptedException;

    const-string v2, "ChatONVApplicatoinInfoParser is interrupted."

    invoke-direct {v0, v2}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 70
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 68
    :cond_0
    :goto_2
    throw v0

    .line 65
    :cond_1
    :try_start_3
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 68
    :cond_2
    if-eqz v1, :cond_3

    .line 70
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 76
    :cond_3
    :goto_3
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 77
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Raw JSON: "

    aput-object v2, v1, v3

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_4
    :try_start_5
    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 82
    const-string v0, "install"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 83
    const-string v2, "config"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    .line 85
    iget-object v2, v4, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    const-string v5, "enable"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->enable:Ljava/lang/Boolean;

    .line 87
    iget-object v2, v4, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    iget-object v2, v2, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->enable:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 88
    const-string v2, "urlList"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    move v0, v3

    .line 89
    :goto_4
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_7

    .line 90
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, ""

    if-eq v3, v5, :cond_5

    .line 91
    iget-object v3, v4, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    iget-object v3, v3, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 89
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 93
    :cond_5
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_6

    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is null or empty string"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->access$000()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_6
    iget-object v3, v4, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->install:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;

    iget-object v3, v3, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Install;->urlList:Ljava/util/ArrayList;

    const-string v5, ""

    invoke-virtual {v3, v0, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_5

    .line 104
    :catch_0
    move-exception v0

    .line 105
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 101
    :cond_7
    :try_start_6
    iget-object v0, v4, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->config:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Config;

    const-string v2, "videoCallMax"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Config;->videoCallMax:Ljava/lang/Integer;

    .line 102
    iget-object v0, v4, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry;->config:Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Config;

    const-string v2, "voiceCallMax"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/io/entry/ChatONVApplicationInfoEntry$Config;->voiceCallMax:Ljava/lang/Integer;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_0

    .line 108
    return-object v4

    .line 71
    :catch_1
    move-exception v1

    goto/16 :goto_2

    :catch_2
    move-exception v1

    goto/16 :goto_3

    .line 68
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1
.end method
