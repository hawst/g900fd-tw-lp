.class public Lcom/sec/chaton/io/entry/ApplinkMsgEntry;
.super Lcom/sec/chaton/io/entry/MessageType4Entry;
.source "ApplinkMsgEntry.java"


# instance fields
.field public content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

.field public format:Ljava/lang/String;

.field public push_message:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/chaton/io/entry/MessageType4Entry;-><init>()V

    .line 98
    return-void
.end method

.method public static createJson(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 258
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 259
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 261
    const-string v2, "type"

    const-string v3, "link"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 262
    const-string v2, "format"

    const-string v3, "json"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 271
    invoke-static {p0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->makePushMessage(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 276
    const-string v3, "push_message"

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 278
    const-string v2, "type"

    const-string v3, "app"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 279
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 280
    const-string v3, "type"

    const-string v4, "text"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 281
    const-string v3, "text"

    invoke-virtual {v2, v3, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 282
    const-string v3, "message"

    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 283
    const-string v2, "appInfo"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 285
    const-string v2, "content"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 287
    return-object v0
.end method

.method public static getDisplayMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 188
    :try_start_0
    invoke-static {p0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->parse(Ljava/lang/String;)Lcom/sec/chaton/io/entry/MessageType4Entry;

    move-result-object v0

    .line 189
    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/MessageType4Entry;->getDisplayMessage()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 196
    :cond_0
    :goto_0
    return-object p0

    .line 190
    :catch_0
    move-exception v0

    .line 191
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 192
    const-class v1, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isPushMessage(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 174
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 176
    const-string v1, "push_message"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    const/4 v0, 0x0

    .line 183
    :goto_0
    return v0

    .line 181
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static makePushMessage(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5

    .prologue
    const/16 v4, 0x62

    .line 292
    .line 294
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 295
    const-string v1, "type"

    const-string v2, "link"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 296
    const-string v1, "content_type"

    const-string v2, "app"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 297
    const-string v1, "text"

    invoke-virtual {v0, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 298
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 299
    invoke-static {v1}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v1

    .line 300
    if-lt v4, v1, :cond_0

    .line 328
    :goto_0
    return-object v0

    .line 304
    :cond_0
    const-string v0, ".."

    .line 305
    const-string v0, ".."

    invoke-static {v0}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v0

    .line 306
    invoke-static {p0}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v2

    .line 309
    add-int/lit8 v1, v1, -0x62

    .line 310
    sub-int v1, v2, v1

    .line 311
    if-le v1, v0, :cond_1

    .line 312
    const-string v0, ".."

    invoke-static {p0, v1, v0}, Lcom/sec/chaton/util/cl;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 318
    :goto_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 319
    const-string v2, "type"

    const-string v3, "link"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 320
    const-string v2, "content_type"

    const-string v3, "app"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 321
    const-string v2, "text"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 322
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 323
    invoke-static {v0}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v0

    .line 324
    if-ge v4, v0, :cond_2

    .line 325
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "exceeds 100 bytes"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_1
    const-string v1, ".."

    invoke-static {p0, v0, v1}, Lcom/sec/chaton/util/cl;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 328
    goto :goto_0
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/chaton/io/entry/MessageType4Entry;
    .locals 1

    .prologue
    .line 201
    invoke-static {p0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->isPushMessage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    invoke-static {p0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->parseAsPushMessage(Ljava/lang/String;)Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;

    move-result-object v0

    .line 206
    :goto_0
    return-object v0

    .line 204
    :cond_0
    invoke-static {p0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->parseAsFullMessage(Ljava/lang/String;)Lcom/sec/chaton/io/entry/ApplinkMsgEntry;

    move-result-object v0

    goto :goto_0
.end method

.method public static parseAsFullMessage(Ljava/lang/String;)Lcom/sec/chaton/io/entry/ApplinkMsgEntry;
    .locals 4

    .prologue
    .line 233
    new-instance v1, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;

    invoke-direct {v1}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;-><init>()V

    .line 234
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 235
    const-string v0, "type"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->type:Ljava/lang/String;

    .line 236
    const-string v0, "format"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->format:Ljava/lang/String;

    .line 237
    const-string v0, "content"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 239
    new-instance v3, Lcom/sec/chaton/k/a/a;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/sec/chaton/k/a/a;-><init>(Ljava/lang/String;)V

    .line 240
    const-class v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    invoke-virtual {v3, v0}, Lcom/sec/chaton/k/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iput-object v0, v1, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    .line 242
    const-string v0, "push_message"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 243
    instance-of v2, v0, Lorg/json/JSONObject;

    if-eqz v2, :cond_0

    .line 244
    check-cast v0, Lorg/json/JSONObject;

    .line 245
    new-instance v2, Lcom/sec/chaton/k/a/a;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/sec/chaton/k/a/a;-><init>(Ljava/lang/String;)V

    .line 246
    const-class v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;

    invoke-virtual {v2, v0}, Lcom/sec/chaton/k/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;

    iput-object v0, v1, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->push_message:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;

    .line 252
    :goto_0
    sget-object v0, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;->ENTRY_TYPE_WHOLE:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->setEntryType(Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;)V

    .line 253
    return-object v1

    .line 248
    :cond_0
    new-instance v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;-><init>()V

    iput-object v2, v1, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->push_message:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;

    .line 249
    iget-object v2, v1, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->push_message:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;->text:Ljava/lang/String;

    goto :goto_0
.end method

.method public static parseAsPushMessage(Ljava/lang/String;)Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;
    .locals 3

    .prologue
    .line 214
    new-instance v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;-><init>()V

    .line 216
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 217
    const-string v2, "type"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;->type:Ljava/lang/String;

    .line 218
    const-string v2, "content_type"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;->content_type:Ljava/lang/String;

    .line 219
    const-string v2, "text"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;->text:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :goto_0
    sget-object v1, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;->ENTRY_TYPE_PUSH:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;->setEntryType(Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;)V

    .line 225
    return-object v0

    .line 220
    :catch_0
    move-exception v1

    .line 221
    iput-object p0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$PushMessageEntry;->text:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getAnythingExecuteUri()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;->param:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 170
    :goto_0
    return-object v0

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;->param:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;

    .line 166
    iget-object v3, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->executeUri:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 167
    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->executeUri:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 170
    goto :goto_0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    if-nez v0, :cond_1

    .line 129
    :cond_0
    const-string v0, ""

    .line 136
    :goto_0
    return-object v0

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    const-string v0, ""

    goto :goto_0

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    if-nez v0, :cond_1

    .line 141
    :cond_0
    const-string v0, ""

    .line 144
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;->version:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDisplayMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->message:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$MessageEntry;

    if-nez v0, :cond_1

    .line 67
    :cond_0
    const-string v0, ""

    .line 74
    :goto_0
    return-object v0

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->message:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$MessageEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$MessageEntry;->text:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    const-string v0, ""

    goto :goto_0

    .line 74
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->message:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$MessageEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$MessageEntry;->text:Ljava/lang/String;

    goto :goto_0
.end method

.method public getParam(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;->param:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 157
    :goto_0
    return-object v0

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->content:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry;->appInfo:Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry;->param:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;

    .line 153
    iget-object v3, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->OS:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "phone"

    iget-object v4, v0, Lcom/sec/chaton/io/entry/ApplinkMsgEntry$ContentEntry$AppInfoEntry$ParamEntry;->deviceType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 157
    goto :goto_0
.end method
