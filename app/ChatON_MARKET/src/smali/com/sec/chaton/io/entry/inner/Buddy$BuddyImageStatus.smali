.class public final enum Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;
.super Ljava/lang/Enum;
.source "Buddy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

.field public static final enum GROUP_IMAGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

.field public static final enum ME_IMAGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

.field public static final enum NONE_PROFILE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

.field public static final enum NOT_CHANGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

.field public static final enum PROFILE_DELETED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

.field public static final enum PROFILE_UPDATED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 13
    new-instance v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    const-string v1, "PROFILE_UPDATED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_UPDATED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    .line 14
    new-instance v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    const-string v1, "PROFILE_DELETED"

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_DELETED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    .line 15
    new-instance v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    const-string v1, "NONE_PROFILE"

    invoke-direct {v0, v1, v4, v5}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NONE_PROFILE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    .line 16
    new-instance v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    const-string v1, "NOT_CHANGE"

    invoke-direct {v0, v1, v5, v6}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NOT_CHANGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    .line 17
    new-instance v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    const-string v1, "GROUP_IMAGE"

    invoke-direct {v0, v1, v6, v7}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->GROUP_IMAGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    .line 18
    new-instance v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    const-string v1, "ME_IMAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->ME_IMAGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    .line 12
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_UPDATED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    aput-object v2, v0, v1

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_DELETED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NONE_PROFILE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NOT_CHANGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->GROUP_IMAGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->ME_IMAGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->$VALUES:[Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23
    iput p3, p0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->code:I

    .line 24
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->$VALUES:[Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v0}, [Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->code:I

    return v0
.end method
