.class public Lcom/sec/chaton/io/entry/MessageType4Entry;
.super Lcom/sec/chaton/io/entry/Entry;
.source "MessageType4Entry.java"


# instance fields
.field mEntryType:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Lcom/sec/chaton/io/entry/Entry;-><init>()V

    return-void
.end method


# virtual methods
.method public getDisplayMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    const-string v0, ""

    return-object v0
.end method

.method public getEntryType()Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/chaton/io/entry/MessageType4Entry;->mEntryType:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    return-object v0
.end method

.method public isPushType()Z
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/chaton/io/entry/MessageType4Entry;->mEntryType:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    sget-object v1, Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;->ENTRY_TYPE_PUSH:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    if-ne v0, v1, :cond_0

    .line 22
    const/4 v0, 0x1

    .line 25
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEntryType(Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;)V
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lcom/sec/chaton/io/entry/MessageType4Entry;->mEntryType:Lcom/sec/chaton/io/entry/MessageType4Entry$EntryType;

    .line 14
    return-void
.end method
