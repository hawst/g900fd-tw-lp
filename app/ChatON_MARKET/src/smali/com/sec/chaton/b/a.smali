.class public Lcom/sec/chaton/b/a;
.super Ljava/lang/Object;
.source "ContextMenuWrapper.java"

# interfaces
.implements Landroid/view/ContextMenu;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/view/ContextMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ContextMenu;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/chaton/b/a;->a:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    .line 29
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/b/a;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 35
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 42
    return-object p0
.end method

.method public add(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/b/a;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/b/a;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/b/a;->a:Landroid/content/Context;

    invoke-virtual {v0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 9

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Landroid/view/ContextMenu;->addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I

    move-result v0

    return v0
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/b/a;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/b/a;->addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/chaton/b/a;->a:Landroid/content/Context;

    invoke-virtual {v0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/chaton/b/a;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/ContextMenu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0}, Landroid/view/ContextMenu;->clear()V

    .line 88
    return-void
.end method

.method public clearHeader()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0}, Landroid/view/ContextMenu;->clearHeader()V

    .line 183
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0}, Landroid/view/ContextMenu;->close()V

    .line 143
    return-void
.end method

.method public findItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public hasVisibleItems()Z
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0}, Landroid/view/ContextMenu;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/ContextMenu;->isShortcutKey(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public performIdentifierAction(II)Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/ContextMenu;->performIdentifierAction(II)Z

    move-result v0

    return v0
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/ContextMenu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    return v0
.end method

.method public removeGroup(I)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->removeGroup(I)V

    .line 103
    return-void
.end method

.method public removeItem(I)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 98
    return-void
.end method

.method public setGroupCheckable(IZZ)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/ContextMenu;->setGroupCheckable(IZZ)V

    .line 108
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/ContextMenu;->setGroupEnabled(IZ)V

    .line 118
    return-void
.end method

.method public setGroupVisible(IZ)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/ContextMenu;->setGroupVisible(IZ)V

    .line 113
    return-void
.end method

.method public setHeaderIcon(I)Landroid/view/ContextMenu;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->setHeaderIcon(I)Landroid/view/ContextMenu;

    move-result-object v0

    return-object v0
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/ContextMenu;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/ContextMenu;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setHeaderTitle(I)Landroid/view/ContextMenu;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/chaton/b/a;->a(I)Lcom/sec/chaton/b/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/chaton/b/a;->a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;

    move-result-object v0

    return-object v0
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/ContextMenu;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->setHeaderView(Landroid/view/View;)Landroid/view/ContextMenu;

    move-result-object v0

    return-object v0
.end method

.method public setQwertyMode(Z)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0, p1}, Landroid/view/ContextMenu;->setQwertyMode(Z)V

    .line 163
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/chaton/b/a;->b:Landroid/view/ContextMenu;

    invoke-interface {v0}, Landroid/view/ContextMenu;->size()I

    move-result v0

    return v0
.end method
