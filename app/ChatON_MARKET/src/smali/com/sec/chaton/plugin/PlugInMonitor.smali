.class public Lcom/sec/chaton/plugin/PlugInMonitor;
.super Landroid/content/BroadcastReceiver;
.source "PlugInMonitor.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/chaton/plugin/PlugInMonitor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/sec/chaton/plugin/c;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 139
    invoke-direct {p0, p3}, Lcom/sec/chaton/plugin/PlugInMonitor;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const/4 v0, 0x0

    .line 151
    :goto_0
    return-object v0

    .line 143
    :cond_0
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_1

    .line 144
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "PlugIn is installed."

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    :cond_1
    if-eqz p2, :cond_2

    .line 148
    invoke-interface {p2, p1}, Lcom/sec/chaton/plugin/c;->c(Landroid/content/Context;)V

    .line 151
    :cond_2
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/chaton/plugin/g;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/sec/chaton/plugin/c;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 194
    .line 196
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_5

    move v0, v1

    .line 200
    :goto_0
    if-eqz v0, :cond_2

    .line 201
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 202
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v3, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    const-string v4, "PlugIn is enabled."

    aput-object v4, v2, v1

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_0
    if-eqz p2, :cond_1

    .line 205
    invoke-interface {p2, p1}, Lcom/sec/chaton/plugin/c;->f(Landroid/content/Context;)V

    .line 207
    :cond_1
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/chaton/plugin/g;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 215
    :goto_1
    return-object v0

    .line 209
    :cond_2
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_3

    .line 210
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v3, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    const-string v4, "PlugIn is disabled."

    aput-object v4, v2, v1

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_3
    if-eqz p2, :cond_4

    .line 213
    invoke-interface {p2, p1}, Lcom/sec/chaton/plugin/c;->g(Landroid/content/Context;)V

    .line 215
    :cond_4
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/chaton/plugin/g;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 220
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 232
    :goto_0
    return-object v0

    .line 221
    :catch_0
    move-exception v0

    .line 222
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_0

    .line 223
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const-string v3, "Can\'t retrieve package name."

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 226
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    goto :goto_0

    .line 227
    :catch_1
    move-exception v0

    .line 228
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_1

    .line 229
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const-string v3, "Can\'t retrieve package name."

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 232
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Lcom/sec/chaton/plugin/c;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 155
    invoke-direct {p0, p3}, Lcom/sec/chaton/plugin/PlugInMonitor;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const/4 v0, 0x0

    .line 166
    :goto_0
    return-object v0

    .line 159
    :cond_0
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_1

    .line 160
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "PlugIn is removed."

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :cond_1
    if-eqz p2, :cond_2

    .line 163
    invoke-interface {p2, p1}, Lcom/sec/chaton/plugin/c;->d(Landroid/content/Context;)V

    .line 166
    :cond_2
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/chaton/plugin/g;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 238
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.intent.extra.REPLACING"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 244
    :cond_0
    :goto_0
    return v0

    .line 239
    :catch_0
    move-exception v1

    .line 240
    sget-object v2, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v2, v2, Lcom/sec/common/c/a;->e:Z

    if-eqz v2, :cond_0

    .line 241
    sget-object v2, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v3, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private c(Landroid/content/Context;Lcom/sec/chaton/plugin/c;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 170
    invoke-direct {p0, p3}, Lcom/sec/chaton/plugin/PlugInMonitor;->b(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    const/4 v0, 0x0

    .line 180
    :goto_0
    return-object v0

    .line 174
    :cond_0
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_1

    .line 175
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "PlugIn is replaced."

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_1
    if-eqz p2, :cond_2

    .line 178
    invoke-interface {p2, p1}, Lcom/sec/chaton/plugin/c;->e(Landroid/content/Context;)V

    .line 180
    :cond_2
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/chaton/plugin/g;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d(Landroid/content/Context;Lcom/sec/chaton/plugin/c;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 184
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 185
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "PlugIn data is cleared."

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_0
    if-eqz p2, :cond_1

    .line 188
    invoke-interface {p2, p1}, Lcom/sec/chaton/plugin/c;->h(Landroid/content/Context;)V

    .line 190
    :cond_1
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/chaton/plugin/g;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    .prologue
    .line 29
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 33
    invoke-direct {p0, p2}, Lcom/sec/chaton/plugin/PlugInMonitor;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    .line 35
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_5

    .line 40
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const-string v2, "PlugInMonitor.onReceive()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, " Action: "

    aput-object v6, v2, v5

    const/4 v5, 0x1

    aput-object v3, v2, v5

    const/4 v5, 0x2

    const-string v6, ", Data:"

    aput-object v6, v2, v5

    const/4 v5, 0x3

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    aput-object v6, v2, v5

    const/4 v5, 0x4

    const-string v6, ", Package: "

    aput-object v6, v2, v5

    const/4 v5, 0x5

    aput-object v4, v2, v5

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 46
    const-string v0, " ===== Bundle\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    invoke-virtual {v5}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 48
    const-string v1, " Key: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    const-string v1, ", Value: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    invoke-virtual {v5, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 54
    instance-of v7, v1, [Ljava/lang/String;

    if-eqz v7, :cond_3

    .line 55
    const-string v0, "{"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v1

    .line 57
    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    array-length v7, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v7, :cond_2

    aget-object v8, v0, v1

    .line 58
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v8, v9, v10

    const/4 v8, 0x1

    const-string v10, ","

    aput-object v10, v9, v8

    invoke-static {v9}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 61
    :cond_2
    const-string v0, "}"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    :goto_3
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 64
    :cond_3
    invoke-virtual {v5, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 69
    :cond_4
    const-string v0, " ===== Bundle"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_5
    invoke-static {v4}, Lcom/sec/chaton/plugin/h;->a(Ljava/lang/String;)Lcom/sec/chaton/plugin/h;

    move-result-object v5

    .line 76
    if-eqz v5, :cond_c

    .line 77
    const/4 v2, 0x0

    .line 78
    const/4 v1, 0x0

    .line 81
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/chaton/plugin/g;->a(Lcom/sec/chaton/plugin/h;)Lcom/sec/chaton/plugin/d;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/plugin/c;
    :try_end_0
    .catch Lcom/sec/chaton/plugin/f; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_4
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->b:Z

    if-eqz v1, :cond_6

    .line 91
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v6, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "PlugInType: "

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v5, v7, v8

    const/4 v8, 0x2

    const-string v9, ", PlugIn: "

    aput-object v9, v7, v8

    const/4 v8, 0x3

    aput-object v0, v7, v8

    invoke-static {v7}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_6
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 96
    invoke-direct {p0, p1, v0, p2}, Lcom/sec/chaton/plugin/PlugInMonitor;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/c;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 119
    :goto_5
    if-eqz v0, :cond_0

    .line 120
    sget-object v1, Lcom/sec/chaton/plugin/g;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 122
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->b:Z

    if-eqz v1, :cond_7

    .line 123
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "Send broadcast. Intent: "

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_7
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    sget-object v6, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v6, v6, Lcom/sec/common/c/a;->e:Z

    if-eqz v6, :cond_e

    .line 84
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/plugin/f;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 100
    :cond_8
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 101
    invoke-direct {p0, p1, v0, p2}, Lcom/sec/chaton/plugin/PlugInMonitor;->b(Landroid/content/Context;Lcom/sec/chaton/plugin/c;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_5

    .line 105
    :cond_9
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 106
    invoke-direct {p0, p1, v0, p2}, Lcom/sec/chaton/plugin/PlugInMonitor;->c(Landroid/content/Context;Lcom/sec/chaton/plugin/c;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_5

    .line 110
    :cond_a
    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 111
    invoke-direct {p0, p1, v0, p2}, Lcom/sec/chaton/plugin/PlugInMonitor;->d(Landroid/content/Context;Lcom/sec/chaton/plugin/c;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_5

    .line 115
    :cond_b
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 116
    invoke-direct {p0, p1, v0, v4}, Lcom/sec/chaton/plugin/PlugInMonitor;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/c;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_5

    .line 132
    :cond_c
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 133
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/PlugInMonitor;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "It isn\'t supported plugin."

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    move-object v0, v2

    goto/16 :goto_5

    :cond_e
    move-object v0, v1

    goto/16 :goto_4
.end method
