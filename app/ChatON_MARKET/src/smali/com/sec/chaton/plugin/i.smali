.class public Lcom/sec/chaton/plugin/i;
.super Ljava/lang/Object;
.source "STranslatorPlugIn.java"

# interfaces
.implements Lcom/sec/chaton/plugin/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/chaton/plugin/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/plugin/i;->i(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method private a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 28
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.translator"

    invoke-static {v0, v1}, Lcom/sec/common/util/i;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private i(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 32
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/i;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 36
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.translator"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 44
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 45
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 46
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    const-string v2, "STranslator is disabled by user."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_0
    iput-boolean v4, p0, Lcom/sec/chaton/plugin/i;->b:Z

    .line 93
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 38
    iput-boolean v4, p0, Lcom/sec/chaton/plugin/i;->b:Z

    goto :goto_0

    .line 52
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 53
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_2

    .line 54
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    const-string v2, "STranslator is disabled by developer."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_2
    iput-boolean v4, p0, Lcom/sec/chaton/plugin/i;->b:Z

    goto :goto_0

    .line 62
    :cond_3
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 63
    const-string v1, "com.sec.android.app.translator.TRANSLATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_6

    .line 67
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->b:Z

    if-eqz v1, :cond_4

    .line 68
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    const-string v3, "STranslator is available."

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/pm/ResolveInfo;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/plugin/i;->b:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 76
    :catch_1
    move-exception v0

    .line 77
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_5

    .line 78
    sget-object v1, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 81
    :cond_5
    iput-boolean v4, p0, Lcom/sec/chaton/plugin/i;->b:Z

    goto :goto_0

    .line 74
    :cond_6
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/sec/chaton/plugin/i;->b:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 87
    :cond_7
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_8

    .line 88
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    const-string v2, "STranslator isn\'t installed."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_8
    iput-boolean v4, p0, Lcom/sec/chaton/plugin/i;->b:Z

    goto :goto_0
.end method


# virtual methods
.method public b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/chaton/plugin/i;->b:Z

    return v0
.end method

.method public c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 102
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 103
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    const-string v2, "STranslatorPlugIn.onInstalled()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/i;->i(Landroid/content/Context;)V

    .line 107
    return-void
.end method

.method public d(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 111
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 112
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    const-string v2, "STranslatorPlugIn.onUnInstalled()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/i;->i(Landroid/content/Context;)V

    .line 116
    return-void
.end method

.method public e(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 120
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 121
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    const-string v2, "STranslatorPlugIn.onReplaced()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/i;->i(Landroid/content/Context;)V

    .line 125
    return-void
.end method

.method public f(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 129
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 130
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    const-string v2, "STranslatorPlugIn.onEnabled()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/i;->i(Landroid/content/Context;)V

    .line 134
    return-void
.end method

.method public g(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 138
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 139
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    const-string v2, "STranslatorPlugIn.onDisabled()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/i;->i(Landroid/content/Context;)V

    .line 143
    return-void
.end method

.method public h(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 147
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 148
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/i;->a:Ljava/lang/String;

    const-string v2, "STranslatorPlugIn.onDataCleared()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_0
    return-void
.end method
