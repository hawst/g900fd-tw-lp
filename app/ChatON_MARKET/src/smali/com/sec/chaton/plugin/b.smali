.class public Lcom/sec/chaton/plugin/b;
.super Ljava/lang/Object;
.source "CommonPushPlugIn.java"

# interfaces
.implements Lcom/sec/chaton/plugin/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/chaton/plugin/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/plugin/b;->i(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 28
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "spp_update_is"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method private a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.spp.push"

    invoke-static {v0, v1}, Lcom/sec/common/util/i;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private i(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 41
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.spp.push"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 49
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 50
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 51
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    const-string v2, "CommonPush is disabled by user."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    :cond_0
    iput-boolean v3, p0, Lcom/sec/chaton/plugin/b;->b:Z

    .line 81
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    .line 43
    iput-boolean v3, p0, Lcom/sec/chaton/plugin/b;->b:Z

    goto :goto_0

    .line 57
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 58
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_2

    .line 59
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    const-string v2, "CommonPush is disabled by developer."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_2
    iput-boolean v3, p0, Lcom/sec/chaton/plugin/b;->b:Z

    goto :goto_0

    .line 66
    :cond_3
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_4

    .line 67
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    const-string v2, "CommonPush is available."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/plugin/b;->b:Z

    goto :goto_0

    .line 75
    :cond_5
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_6

    .line 76
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    const-string v2, "CommonPush isn\'t installed."

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_6
    iput-boolean v3, p0, Lcom/sec/chaton/plugin/b;->b:Z

    goto :goto_0
.end method


# virtual methods
.method public b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/sec/chaton/plugin/b;->b:Z

    return v0
.end method

.method public c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 90
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    const-string v2, "CommonPushPlugIn.onInstalled()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/b;->i(Landroid/content/Context;)V

    .line 95
    return-void
.end method

.method public d(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 99
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 100
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    const-string v2, "CommonPushPlugIn.onUnInstalled()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/plugin/b;->a()V

    .line 104
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/b;->i(Landroid/content/Context;)V

    .line 105
    return-void
.end method

.method public e(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 109
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 110
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    const-string v2, "CommonPushPlugIn.onReplaced()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/plugin/b;->a()V

    .line 114
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/b;->i(Landroid/content/Context;)V

    .line 115
    return-void
.end method

.method public f(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 119
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 120
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    const-string v2, "CommonPushPlugIn.onEnabled()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/b;->i(Landroid/content/Context;)V

    .line 124
    return-void
.end method

.method public g(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 128
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 129
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    const-string v2, "CommonPushPlugIn.onDisabled()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/chaton/plugin/b;->i(Landroid/content/Context;)V

    .line 133
    return-void
.end method

.method public h(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 137
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 138
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/chaton/plugin/b;->a:Ljava/lang/String;

    const-string v2, "CommonPushPlugIn.onDataCleared()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    return-void
.end method
