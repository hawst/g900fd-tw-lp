.class public Lcom/sec/chaton/plugin/g;
.super Ljava/lang/Object;
.source "PlugInManager.java"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static i:Lcom/sec/chaton/plugin/g;


# instance fields
.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/chaton/plugin/h;",
            "Lcom/sec/chaton/plugin/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/chaton/plugin/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/plugin/g;->h:Ljava/lang/String;

    .line 21
    const-string v0, "com.sec.chaton.action.PLUGIN_INSTALLED"

    sput-object v0, Lcom/sec/chaton/plugin/g;->a:Ljava/lang/String;

    .line 22
    const-string v0, "com.sec.chaton.action.PLUGIN_UNINSTALLED"

    sput-object v0, Lcom/sec/chaton/plugin/g;->b:Ljava/lang/String;

    .line 23
    const-string v0, "com.sec.chaton.action.PLUGIN_REPLACED"

    sput-object v0, Lcom/sec/chaton/plugin/g;->c:Ljava/lang/String;

    .line 24
    const-string v0, "com.sec.chaton.action.PLUGIN_ENABLED"

    sput-object v0, Lcom/sec/chaton/plugin/g;->d:Ljava/lang/String;

    .line 25
    const-string v0, "com.sec.chaton.action.PLUGIN_DISABLED"

    sput-object v0, Lcom/sec/chaton/plugin/g;->e:Ljava/lang/String;

    .line 26
    const-string v0, "com.sec.chaton.action.PLUGIN_DATA_CLEARED"

    sput-object v0, Lcom/sec/chaton/plugin/g;->f:Ljava/lang/String;

    .line 28
    const-string v0, "plugInType"

    sput-object v0, Lcom/sec/chaton/plugin/g;->g:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/plugin/g;->j:Ljava/util/Map;

    .line 61
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/plugin/g;
    .locals 2

    .prologue
    .line 64
    const-class v1, Lcom/sec/chaton/plugin/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/plugin/g;->i:Lcom/sec/chaton/plugin/g;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/sec/chaton/plugin/g;

    invoke-direct {v0}, Lcom/sec/chaton/plugin/g;-><init>()V

    sput-object v0, Lcom/sec/chaton/plugin/g;->i:Lcom/sec/chaton/plugin/g;

    .line 68
    :cond_0
    sget-object v0, Lcom/sec/chaton/plugin/g;->i:Lcom/sec/chaton/plugin/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method a(Lcom/sec/chaton/plugin/h;)Lcom/sec/chaton/plugin/d;
    .locals 3

    .prologue
    .line 72
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/plugin/g;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    :try_start_0
    iget-object v0, p1, Lcom/sec/chaton/plugin/h;->e:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/plugin/d;

    .line 78
    iget-object v1, p0, Lcom/sec/chaton/plugin/g;->j:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 88
    :goto_0
    return-object v0

    .line 79
    :catch_0
    move-exception v0

    .line 80
    new-instance v1, Lcom/sec/chaton/plugin/f;

    const-string v2, "Can\'t initialize PlugIn."

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/plugin/f;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 81
    :catch_1
    move-exception v0

    .line 82
    new-instance v1, Lcom/sec/chaton/plugin/f;

    const-string v2, "Can\'t initialize PlugIn."

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/plugin/f;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/plugin/g;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/plugin/d;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z
    .locals 4

    .prologue
    .line 93
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/sec/chaton/plugin/g;->a(Lcom/sec/chaton/plugin/h;)Lcom/sec/chaton/plugin/d;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/chaton/plugin/d;->b(Landroid/content/Context;)Z
    :try_end_0
    .catch Lcom/sec/chaton/plugin/f; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 99
    :goto_0
    return v0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_0

    .line 96
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/plugin/g;->h:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/plugin/f;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 99
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
