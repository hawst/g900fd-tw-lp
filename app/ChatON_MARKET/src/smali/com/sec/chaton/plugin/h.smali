.class public final enum Lcom/sec/chaton/plugin/h;
.super Ljava/lang/Enum;
.source "PlugInManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/plugin/h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/plugin/h;

.field public static final enum b:Lcom/sec/chaton/plugin/h;

.field public static final enum c:Lcom/sec/chaton/plugin/h;

.field private static final synthetic f:[Lcom/sec/chaton/plugin/h;


# instance fields
.field d:[Ljava/lang/String;

.field e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/chaton/plugin/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 31
    new-instance v0, Lcom/sec/chaton/plugin/h;

    const-string v1, "ChatONV"

    const-class v2, Lcom/sec/chaton/plugin/a;

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "com.coolots.chaton"

    aput-object v4, v3, v5

    const-string v4, "com.coolots.chatonforcanada"

    aput-object v4, v3, v6

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/chaton/plugin/h;-><init>(Ljava/lang/String;ILjava/lang/Class;[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    .line 32
    new-instance v0, Lcom/sec/chaton/plugin/h;

    const-string v1, "CommonPush"

    const-class v2, Lcom/sec/chaton/plugin/b;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "com.sec.spp.push"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/chaton/plugin/h;-><init>(Ljava/lang/String;ILjava/lang/Class;[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/plugin/h;->b:Lcom/sec/chaton/plugin/h;

    .line 33
    new-instance v0, Lcom/sec/chaton/plugin/h;

    const-string v1, "STranslator"

    const-class v2, Lcom/sec/chaton/plugin/i;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "com.sec.android.app.translator"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/chaton/plugin/h;-><init>(Ljava/lang/String;ILjava/lang/Class;[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/plugin/h;->c:Lcom/sec/chaton/plugin/h;

    .line 30
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/plugin/h;

    sget-object v1, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/plugin/h;->b:Lcom/sec/chaton/plugin/h;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/plugin/h;->c:Lcom/sec/chaton/plugin/h;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/chaton/plugin/h;->f:[Lcom/sec/chaton/plugin/h;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;ILjava/lang/Class;[Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/chaton/plugin/d;",
            ">;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput-object p4, p0, Lcom/sec/chaton/plugin/h;->d:[Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/sec/chaton/plugin/h;->e:Ljava/lang/Class;

    .line 41
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/plugin/h;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-static {}, Lcom/sec/chaton/plugin/h;->values()[Lcom/sec/chaton/plugin/h;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 45
    iget-object v6, v0, Lcom/sec/chaton/plugin/h;->d:[Ljava/lang/String;

    array-length v7, v6

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_1

    aget-object v8, v6, v1

    .line 46
    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 52
    :goto_2
    return-object v0

    .line 45
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 44
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 52
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/plugin/h;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/chaton/plugin/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/plugin/h;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/plugin/h;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/chaton/plugin/h;->f:[Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0}, [Lcom/sec/chaton/plugin/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/plugin/h;

    return-object v0
.end method
