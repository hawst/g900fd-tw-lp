.class public final enum Lcom/sec/chaton/w;
.super Ljava/lang/Enum;
.source "HandleIntent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/w;

.field public static final enum b:Lcom/sec/chaton/w;

.field public static final enum c:Lcom/sec/chaton/w;

.field public static final enum d:Lcom/sec/chaton/w;

.field public static final enum e:Lcom/sec/chaton/w;

.field private static final synthetic g:[Lcom/sec/chaton/w;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    new-instance v0, Lcom/sec/chaton/w;

    const-string v1, "UKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/w;->a:Lcom/sec/chaton/w;

    .line 63
    new-instance v0, Lcom/sec/chaton/w;

    const-string v1, "API"

    invoke-direct {v0, v1, v4, v3}, Lcom/sec/chaton/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/w;->b:Lcom/sec/chaton/w;

    .line 64
    new-instance v0, Lcom/sec/chaton/w;

    const-string v1, "SHARE"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/w;->c:Lcom/sec/chaton/w;

    .line 65
    new-instance v0, Lcom/sec/chaton/w;

    const-string v1, "FORWARD"

    invoke-direct {v0, v1, v6, v5}, Lcom/sec/chaton/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/w;->d:Lcom/sec/chaton/w;

    .line 66
    new-instance v0, Lcom/sec/chaton/w;

    const-string v1, "INSIDE"

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/chaton/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/w;->e:Lcom/sec/chaton/w;

    .line 61
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/w;

    sget-object v1, Lcom/sec/chaton/w;->a:Lcom/sec/chaton/w;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/w;->b:Lcom/sec/chaton/w;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/w;->c:Lcom/sec/chaton/w;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/w;->d:Lcom/sec/chaton/w;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/w;->e:Lcom/sec/chaton/w;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/chaton/w;->g:[Lcom/sec/chaton/w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 71
    iput p3, p0, Lcom/sec/chaton/w;->f:I

    .line 72
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/w;
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/sec/chaton/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/w;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/w;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/chaton/w;->g:[Lcom/sec/chaton/w;

    invoke-virtual {v0}, [Lcom/sec/chaton/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/w;

    return-object v0
.end method
