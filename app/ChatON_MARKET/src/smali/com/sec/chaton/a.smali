.class Lcom/sec/chaton/a;
.super Landroid/os/Handler;
.source "AdminMenu.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/AdminMenu;


# direct methods
.method constructor <init>(Lcom/sec/chaton/AdminMenu;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/sec/chaton/a;->a:Lcom/sec/chaton/AdminMenu;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 260
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 261
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3eb

    if-ne v0, v1, :cond_1

    .line 262
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 266
    sget-object v1, Lcom/sec/chaton/global/GlobalApplication;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 268
    const/4 v0, 0x0

    .line 274
    :cond_0
    if-eqz v0, :cond_2

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/a;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->a(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v0

    const-string v1, "Push Status - Available"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 281
    :cond_1
    :goto_0
    return-void

    .line 277
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/a;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->a(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;

    move-result-object v0

    const-string v1, "Push Status - Unvailable"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
