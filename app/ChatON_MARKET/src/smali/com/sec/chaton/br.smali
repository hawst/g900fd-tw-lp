.class Lcom/sec/chaton/br;
.super Ljava/lang/Object;
.source "TabActivity.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/TabActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 433
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 429
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 370
    if-eqz p3, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, p3}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Landroid/database/Cursor;)V

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, v1}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Z)Z

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, v1}, Lcom/sec/chaton/TabActivity;->d(Lcom/sec/chaton/TabActivity;Z)Z

    .line 421
    :cond_1
    :goto_0
    return-void

    .line 382
    :cond_2
    if-ne p1, v1, :cond_4

    .line 383
    if-eqz p3, :cond_3

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;I)I

    .line 385
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, p3}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Landroid/database/Cursor;)V

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->c(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    if-lez v0, :cond_3

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "buddy_tab_badge_update"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 391
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->d(Lcom/sec/chaton/TabActivity;)V

    goto :goto_0

    .line 394
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_7

    .line 395
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/TabActivity;->b(Lcom/sec/chaton/TabActivity;I)I

    .line 397
    if-eqz p3, :cond_6

    .line 398
    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    const-string v1, "inbox_unread_count"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/TabActivity;->c(Lcom/sec/chaton/TabActivity;I)I

    goto :goto_1

    .line 401
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, p3}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Landroid/database/Cursor;)V

    .line 405
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->d(Lcom/sec/chaton/TabActivity;)V

    goto :goto_0

    .line 408
    :cond_7
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 409
    if-eqz p3, :cond_8

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/TabActivity;->d(Lcom/sec/chaton/TabActivity;I)I

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, p3}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Landroid/database/Cursor;)V

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->e(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    if-lez v0, :cond_8

    .line 413
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "buddy_tab_badge_update"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 417
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/br;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->d(Lcom/sec/chaton/TabActivity;)V

    goto/16 :goto_0
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 425
    return-void
.end method
