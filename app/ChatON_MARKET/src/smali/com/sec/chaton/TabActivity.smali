.class public Lcom/sec/chaton/TabActivity;
.super Lcom/sec/chaton/FlexibleActivity;
.source "TabActivity.java"

# interfaces
.implements Lcom/sec/chaton/buddy/fi;
.implements Lcom/sec/chaton/buddy/ih;
.implements Lcom/sec/chaton/msgbox/ag;
.implements Lcom/sec/chaton/trunk/co;
.implements Lcom/sec/chaton/userprofile/bn;
.implements Lcom/sec/chaton/util/cn;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:I


# instance fields
.field private A:Z

.field private B:I

.field private C:I

.field private D:Landroid/view/View;

.field private E:Landroid/view/View;

.field private F:Landroid/view/View;

.field private G:Landroid/view/View;

.field private H:Landroid/widget/LinearLayout$LayoutParams;

.field private I:Landroid/widget/LinearLayout$LayoutParams;

.field private J:I

.field private K:I

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:Lcom/sec/common/a/d;

.field private Q:Landroid/database/ContentObserver;

.field private R:Landroid/database/ContentObserver;

.field private S:Landroid/database/ContentObserver;

.field private T:Lcom/sec/chaton/e/a/v;

.field private U:Landroid/content/BroadcastReceiver;

.field private V:Landroid/view/ViewGroup;

.field private W:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private X:Landroid/content/BroadcastReceiver;

.field private Y:Landroid/content/BroadcastReceiver;

.field private Z:Landroid/content/BroadcastReceiver;

.field private aa:Lcom/sec/chaton/util/bp;

.field private ab:Lcom/sec/chaton/util/bs;

.field private ac:Lcom/sec/chaton/util/bs;

.field private ad:Lcom/sec/chaton/util/bs;

.field private ae:Lcom/sec/chaton/util/bs;

.field private af:Lcom/sec/chaton/util/bs;

.field private ag:Lcom/sec/chaton/util/bs;

.field private ah:Lcom/sec/chaton/util/bs;

.field private ai:Lcom/sec/chaton/util/bs;

.field private aj:Lcom/sec/chaton/util/bs;

.field private ak:Lcom/sec/chaton/util/bs;

.field private al:Lcom/sec/chaton/util/bs;

.field private am:Lcom/sec/chaton/util/bs;

.field private an:Lcom/sec/chaton/util/bs;

.field private ao:Landroid/os/Handler;

.field private ap:Lcom/sec/chaton/bx;

.field private aq:Landroid/support/v4/app/Fragment;

.field private ar:I

.field private as:Lcom/coolots/sso/a/d;

.field b:Landroid/view/View$OnTouchListener;

.field c:Landroid/os/Handler;

.field d:Landroid/os/Handler;

.field public e:Landroid/os/Handler;

.field private h:Lcom/sec/chaton/v;

.field private i:Lcom/sec/common/a/d;

.field private j:Landroid/content/BroadcastReceiver;

.field private k:Landroid/content/BroadcastReceiver;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:Landroid/view/View;

.field private w:Landroid/view/ViewTreeObserver;

.field private x:Lcom/sec/chaton/e/a/u;

.field private y:Lcom/sec/chaton/io/entry/GetVersionNotice;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169
    const-class v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    .line 259
    const/high16 v0, 0x24010000

    sput v0, Lcom/sec/chaton/TabActivity;->g:I

    .line 261
    const-string v0, "baseIntent"

    sput-object v0, Lcom/sec/chaton/TabActivity;->a:Ljava/lang/String;

    .line 262
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/FlexibleActivity;-><init>()V

    .line 301
    iput-boolean v0, p0, Lcom/sec/chaton/TabActivity;->z:Z

    .line 303
    iput-boolean v0, p0, Lcom/sec/chaton/TabActivity;->A:Z

    .line 326
    new-instance v0, Lcom/sec/chaton/ap;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/ap;-><init>(Lcom/sec/chaton/TabActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->Q:Landroid/database/ContentObserver;

    .line 338
    new-instance v0, Lcom/sec/chaton/bb;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/bb;-><init>(Lcom/sec/chaton/TabActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->R:Landroid/database/ContentObserver;

    .line 350
    new-instance v0, Lcom/sec/chaton/bl;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/bl;-><init>(Lcom/sec/chaton/TabActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->S:Landroid/database/ContentObserver;

    .line 366
    new-instance v0, Lcom/sec/chaton/br;

    invoke-direct {v0, p0}, Lcom/sec/chaton/br;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->T:Lcom/sec/chaton/e/a/v;

    .line 436
    new-instance v0, Lcom/sec/chaton/bs;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bs;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->U:Landroid/content/BroadcastReceiver;

    .line 587
    new-instance v0, Lcom/sec/chaton/bt;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bt;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->b:Landroid/view/View$OnTouchListener;

    .line 930
    new-instance v0, Lcom/sec/chaton/bu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bu;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->W:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1149
    new-instance v0, Lcom/sec/chaton/bv;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bv;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->X:Landroid/content/BroadcastReceiver;

    .line 1166
    new-instance v0, Lcom/sec/chaton/bw;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bw;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->Y:Landroid/content/BroadcastReceiver;

    .line 1188
    new-instance v0, Lcom/sec/chaton/aq;

    invoke-direct {v0, p0}, Lcom/sec/chaton/aq;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->Z:Landroid/content/BroadcastReceiver;

    .line 1596
    new-instance v0, Lcom/sec/chaton/ar;

    invoke-direct {v0, p0}, Lcom/sec/chaton/ar;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->ab:Lcom/sec/chaton/util/bs;

    .line 1616
    new-instance v0, Lcom/sec/chaton/as;

    invoke-direct {v0, p0}, Lcom/sec/chaton/as;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->ac:Lcom/sec/chaton/util/bs;

    .line 1632
    new-instance v0, Lcom/sec/chaton/at;

    invoke-direct {v0, p0}, Lcom/sec/chaton/at;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->ad:Lcom/sec/chaton/util/bs;

    .line 1663
    new-instance v0, Lcom/sec/chaton/av;

    invoke-direct {v0, p0}, Lcom/sec/chaton/av;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->ae:Lcom/sec/chaton/util/bs;

    .line 1678
    new-instance v0, Lcom/sec/chaton/aw;

    invoke-direct {v0, p0}, Lcom/sec/chaton/aw;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->af:Lcom/sec/chaton/util/bs;

    .line 1692
    new-instance v0, Lcom/sec/chaton/ax;

    invoke-direct {v0, p0}, Lcom/sec/chaton/ax;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->ag:Lcom/sec/chaton/util/bs;

    .line 1701
    new-instance v0, Lcom/sec/chaton/ay;

    invoke-direct {v0, p0}, Lcom/sec/chaton/ay;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->ah:Lcom/sec/chaton/util/bs;

    .line 1709
    new-instance v0, Lcom/sec/chaton/az;

    invoke-direct {v0, p0}, Lcom/sec/chaton/az;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->ai:Lcom/sec/chaton/util/bs;

    .line 1740
    new-instance v0, Lcom/sec/chaton/ba;

    invoke-direct {v0, p0}, Lcom/sec/chaton/ba;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->aj:Lcom/sec/chaton/util/bs;

    .line 1760
    new-instance v0, Lcom/sec/chaton/bc;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bc;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->ak:Lcom/sec/chaton/util/bs;

    .line 1775
    new-instance v0, Lcom/sec/chaton/bd;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bd;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->al:Lcom/sec/chaton/util/bs;

    .line 1792
    new-instance v0, Lcom/sec/chaton/be;

    invoke-direct {v0, p0}, Lcom/sec/chaton/be;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->am:Lcom/sec/chaton/util/bs;

    .line 1813
    new-instance v0, Lcom/sec/chaton/bf;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bf;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->an:Lcom/sec/chaton/util/bs;

    .line 2148
    new-instance v0, Lcom/sec/chaton/bj;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/bj;-><init>(Lcom/sec/chaton/TabActivity;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->c:Landroid/os/Handler;

    .line 2184
    new-instance v0, Lcom/sec/chaton/bk;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/bk;-><init>(Lcom/sec/chaton/TabActivity;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->d:Landroid/os/Handler;

    .line 2305
    new-instance v0, Lcom/sec/chaton/bm;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bm;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->e:Landroid/os/Handler;

    .line 2593
    new-instance v0, Lcom/sec/chaton/bn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bn;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->ao:Landroid/os/Handler;

    .line 3324
    new-instance v0, Lcom/sec/chaton/bp;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bp;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->as:Lcom/coolots/sso/a/d;

    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/util/bp;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    return-object v0
.end method

.method private A()Z
    .locals 3

    .prologue
    .line 1964
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1966
    const-string v1, "callRestart"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic B(Lcom/sec/chaton/TabActivity;)Lcom/coolots/sso/a/d;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->as:Lcom/coolots/sso/a/d;

    return-object v0
.end method

.method private B()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1980
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "disclaimer_UID"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic C(Lcom/sec/chaton/TabActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->ao:Landroid/os/Handler;

    return-object v0
.end method

.method private C()Z
    .locals 4

    .prologue
    const/high16 v3, 0x100000

    const/4 v0, 0x0

    .line 1986
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1988
    const-string v2, "finish"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    and-int/2addr v1, v3

    if-eq v1, v3, :cond_0

    .line 1991
    const/4 v0, 0x1

    .line 1993
    :cond_0
    return v0
.end method

.method static synthetic D(Lcom/sec/chaton/TabActivity;)Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->j:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method private D()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2346
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 2347
    invoke-virtual {p0, v1}, Lcom/sec/chaton/TabActivity;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.chaton.settings.ActivityPasswordLockSet"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 2348
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getClassName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/sec/chaton/TabActivity;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private E()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2376
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2377
    const-string v0, "initializeUpdateFactor"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2379
    :cond_0
    iput-boolean v2, p0, Lcom/sec/chaton/TabActivity;->L:Z

    .line 2380
    iput-boolean v2, p0, Lcom/sec/chaton/TabActivity;->M:Z

    .line 2381
    iput-boolean v2, p0, Lcom/sec/chaton/TabActivity;->N:Z

    .line 2382
    iput-boolean v2, p0, Lcom/sec/chaton/TabActivity;->O:Z

    .line 2383
    return-void
.end method

.method static synthetic E(Lcom/sec/chaton/TabActivity;)Z
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->D()Z

    move-result v0

    return v0
.end method

.method private F()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2391
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "update_disclaimer_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic F(Lcom/sec/chaton/TabActivity;)Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->z:Z

    return v0
.end method

.method private G()V
    .locals 3

    .prologue
    .line 2541
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.chaton.ACTION_DISMISS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2542
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 2543
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "more_tab_badge_update"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 2544
    return-void
.end method

.method static synthetic G(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->G()V

    return-void
.end method

.method static synthetic H(Lcom/sec/chaton/TabActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->F()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private H()V
    .locals 3

    .prologue
    .line 2580
    const-string v0, "showPasswordLockActivity"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2582
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 2583
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2584
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2585
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2586
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2588
    invoke-virtual {p0, v1}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 2590
    :cond_0
    return-void
.end method

.method private I()Z
    .locals 4

    .prologue
    .line 2745
    const/4 v1, 0x0

    .line 2746
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    .line 2748
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2749
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2754
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2755
    return v0

    .line 2750
    :catch_0
    move-exception v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    .line 2751
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2750
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static synthetic I(Lcom/sec/chaton/TabActivity;)Z
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->J()Z

    move-result v0

    return v0
.end method

.method private J()Z
    .locals 4

    .prologue
    .line 2759
    const/4 v0, 0x0

    .line 2762
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 2764
    const/4 v0, 0x1

    .line 2770
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVAvaiable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2771
    return v0

    .line 2766
    :catch_0
    move-exception v1

    .line 2767
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic J(Lcom/sec/chaton/TabActivity;)Z
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->I()Z

    move-result v0

    return v0
.end method

.method private K()Z
    .locals 3

    .prologue
    .line 2775
    const/4 v0, 0x0

    .line 2776
    new-instance v1, Lcom/coolots/sso/a/a;

    invoke-direct {v1}, Lcom/coolots/sso/a/a;-><init>()V

    .line 2778
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 2780
    const/4 v0, 0x1

    .line 2786
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVInstalled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2787
    return v0

    .line 2782
    :catch_0
    move-exception v1

    .line 2783
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic K(Lcom/sec/chaton/TabActivity;)Z
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->K()Z

    move-result v0

    return v0
.end method

.method static synthetic L(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/chaton/TabActivity;->s:I

    return v0
.end method

.method private L()Z
    .locals 3

    .prologue
    .line 3379
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chatonv_critical_update"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method static synthetic M(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/chaton/TabActivity;->t:I

    return v0
.end method

.method private M()V
    .locals 3

    .prologue
    .line 3419
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3420
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->w:Landroid/view/ViewTreeObserver;

    .line 3421
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->w:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->W:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 3425
    :cond_0
    const v0, 0x7f07049f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 3427
    const v1, 0x7f07049e

    invoke-virtual {p0, v1}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3428
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 3429
    const v1, 0x7f020417

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3433
    :cond_1
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    const v1, 0x7f070008

    if-ne v0, v1, :cond_3

    .line 3434
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 3435
    iget v1, p0, Lcom/sec/chaton/TabActivity;->ar:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    .line 3436
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->n()V

    .line 3443
    :cond_2
    :goto_0
    return-void

    .line 3437
    :cond_3
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    const v1, 0x7f070009

    if-ne v0, v1, :cond_2

    .line 3438
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 3439
    iget v1, p0, Lcom/sec/chaton/TabActivity;->ar:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgbox/MsgboxFragment;

    .line 3440
    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->d()V

    goto :goto_0
.end method

.method static synthetic N(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/chaton/TabActivity;->u:I

    return v0
.end method

.method private N()V
    .locals 10

    .prologue
    const v9, 0x7f070009

    const v8, 0x7f070008

    const/16 v7, 0x8

    const/4 v6, 0x0

    const-wide v4, 0x3fc99999a0000000L    # 0.20000000298023224

    .line 3447
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->V:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 3451
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/sec/chaton/TabActivity;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3452
    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/TabActivity;->C:I

    .line 3457
    :cond_0
    const-string v1, ""

    .line 3458
    iget v2, p0, Lcom/sec/chaton/TabActivity;->ar:I

    if-ne v2, v8, :cond_7

    .line 3459
    const-string v1, "splitview_position_buddy"

    .line 3463
    :cond_1
    :goto_0
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3464
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const/high16 v3, 0x447a0000    # 1000.0f

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    iget v3, p0, Lcom/sec/chaton/TabActivity;->C:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3469
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->H:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/chaton/TabActivity;->C:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    .line 3470
    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    const v3, 0x7f020416

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3471
    iget v2, p0, Lcom/sec/chaton/TabActivity;->B:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    cmpg-double v2, v0, v4

    if-ltz v2, :cond_4

    :cond_3
    iget v2, p0, Lcom/sec/chaton/TabActivity;->B:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    cmpg-double v0, v0, v4

    if-gez v0, :cond_8

    .line 3473
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->D:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 3474
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 3481
    :goto_1
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    if-ne v0, v8, :cond_9

    .line 3482
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 3483
    iget v1, p0, Lcom/sec/chaton/TabActivity;->ar:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    .line 3484
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->o()V

    .line 3494
    :cond_5
    :goto_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->f()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3495
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->w:Landroid/view/ViewTreeObserver;

    .line 3496
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->w:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->W:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 3499
    :cond_6
    return-void

    .line 3460
    :cond_7
    iget v2, p0, Lcom/sec/chaton/TabActivity;->ar:I

    if-ne v2, v9, :cond_1

    .line 3461
    const-string v1, "splitview_position_chat"

    goto/16 :goto_0

    .line 3476
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->D:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3477
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 3486
    :cond_9
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    if-ne v0, v9, :cond_5

    .line 3487
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 3488
    iget v1, p0, Lcom/sec/chaton/TabActivity;->ar:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgbox/MsgboxFragment;

    .line 3489
    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->e()V

    goto :goto_2
.end method

.method private O()I
    .locals 4

    .prologue
    .line 3525
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 3526
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    .line 3528
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xd

    if-lt v2, v3, :cond_0

    .line 3529
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 3530
    iget v0, v0, Landroid/graphics/Point;->x:I

    .line 3533
    :goto_0
    return v0

    .line 3532
    :cond_0
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 3533
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method private P()I
    .locals 1

    .prologue
    .line 3537
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;I)I
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/sec/chaton/TabActivity;->q:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/chaton/TabActivity;->j:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 500
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/TabActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 501
    sget v1, Lcom/sec/chaton/TabActivity;->g:I

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 503
    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 507
    invoke-static {p0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 508
    sget-object v1, Lcom/sec/chaton/TabActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 510
    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 487
    invoke-static {p0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 489
    if-ne p1, v2, :cond_0

    .line 490
    const-string v1, "callChatList"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 494
    :goto_0
    return-object v0

    .line 492
    :cond_0
    const-string v1, "callChatList"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private a(ILjava/lang/Class;)Landroid/support/v4/app/Fragment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;)",
            "Landroid/support/v4/app/Fragment;"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 548
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 549
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 550
    invoke-virtual {v2, p1}, Landroid/widget/FrameLayout;->setId(I)V

    .line 552
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 553
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 554
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->V:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 560
    :goto_0
    invoke-virtual {v1, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 562
    const/4 v1, 0x0

    .line 565
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 568
    if-nez v0, :cond_0

    .line 569
    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 571
    invoke-virtual {v1, p1, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 575
    :cond_0
    if-eqz v1, :cond_1

    .line 576
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 580
    :cond_1
    if-eqz v1, :cond_2

    .line 581
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 578
    :cond_2
    return-object v0

    .line 556
    :cond_3
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 557
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 580
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 581
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 580
    :cond_4
    throw v0
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/chaton/TabActivity;->D:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;Landroid/widget/LinearLayout$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/chaton/TabActivity;->H:Landroid/widget/LinearLayout$LayoutParams;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;Lcom/sec/chaton/io/entry/GetVersionNotice;)Lcom/sec/chaton/io/entry/GetVersionNotice;
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/chaton/TabActivity;->y:Lcom/sec/chaton/io/entry/GetVersionNotice;

    return-object p1
.end method

.method private a(DIIZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const-wide v3, 0x3fc99999a0000000L    # 0.20000000298023224

    const/4 v2, 0x0

    .line 3541
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->H:Landroid/widget/LinearLayout$LayoutParams;

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 3542
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->I:Landroid/widget/LinearLayout$LayoutParams;

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 3543
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, p1, v0

    if-lez v0, :cond_0

    .line 3544
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->H:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f000000    # 0.5f

    int-to-float v2, p4

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 3545
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->I:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->H:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    sub-int v1, p4, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 3563
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->F:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->H:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3564
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->G:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->I:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3566
    return-void

    .line 3547
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    const v1, 0x7f020416

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3548
    iget v0, p0, Lcom/sec/chaton/TabActivity;->B:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    cmpg-double v0, p1, v3

    if-ltz v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/sec/chaton/TabActivity;->B:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    cmpg-double v0, p1, v3

    if-gez v0, :cond_4

    .line 3550
    :cond_2
    if-eqz p5, :cond_3

    .line 3551
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    const v1, 0x7f020417

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3553
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3554
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->H:Landroid/widget/LinearLayout$LayoutParams;

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 3555
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->I:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0902f4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int v1, p4, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    goto :goto_0

    .line 3557
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3558
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->H:Landroid/widget/LinearLayout$LayoutParams;

    iput p3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 3559
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->I:Landroid/widget/LinearLayout$LayoutParams;

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 3560
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->I:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/high16 v5, 0x4000000

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1447
    invoke-static {p1, p0}, Lcom/sec/chaton/u;->a(Landroid/content/Intent;Landroid/content/Context;)Lcom/sec/chaton/v;

    move-result-object v0

    .line 1449
    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->h:Lcom/sec/chaton/v;

    .line 1452
    sget-object v1, Lcom/sec/chaton/bq;->a:[I

    invoke-virtual {v0}, Lcom/sec/chaton/v;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1533
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1455
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/chaton/TabActivity;->b(Landroid/content/Intent;)V

    goto :goto_0

    .line 1460
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/TabActivity;->c(Landroid/content/Intent;)V

    goto :goto_0

    .line 1471
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/common/actionbar/a;->d(I)V

    goto :goto_0

    .line 1479
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/common/actionbar/a;->d(I)V

    goto :goto_0

    .line 1485
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1486
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1492
    :pswitch_6
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->d(I)V

    .line 1494
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/settings2/SettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1495
    const-string v1, "gotoAlert"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1496
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1502
    :pswitch_7
    invoke-direct {p0, v0, p1}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/v;Landroid/content/Intent;)V

    goto :goto_0

    .line 1505
    :pswitch_8
    const-class v0, Lcom/sec/chaton/buddy/AddBuddyActivity;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1506
    invoke-virtual {p1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1507
    invoke-virtual {p0, p1}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1511
    :pswitch_9
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1513
    invoke-static {v0}, Lcom/sec/chaton/e/a/af;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1514
    const-string v1, "specialuserid"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1515
    const-class v0, Lcom/sec/chaton/buddy/SpecialBuddyActivity;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1519
    :goto_1
    invoke-virtual {p1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1520
    invoke-virtual {p0, p1}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1517
    :cond_1
    const-class v0, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_1

    .line 1524
    :pswitch_a
    const-string v0, "local_backup_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1525
    if-eqz p1, :cond_0

    sget-object v0, Lcom/sec/chaton/util/ak;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1526
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/localbackup/ActivitySecretKey;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1527
    const-string v1, "password_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1528
    invoke-virtual {p0, v0, v4}, Lcom/sec/chaton/TabActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1529
    invoke-static {}, Lcom/sec/chaton/util/ak;->a()Lcom/sec/chaton/util/ak;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/chaton/util/ak;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ak;->a(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 1452
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 3008
    invoke-static {}, Lcom/sec/chaton/e/b/e;->a()Lcom/sec/chaton/e/b/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/b/e;->c()Landroid/os/Message;

    move-result-object v0

    .line 3009
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 3010
    invoke-static {}, Lcom/sec/chaton/e/b/e;->a()Lcom/sec/chaton/e/b/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/e/b/e;->b()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3011
    return-void
.end method

.method private a(Landroid/view/MotionEvent;I)V
    .locals 6

    .prologue
    .line 3504
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3505
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->w:Landroid/view/ViewTreeObserver;

    .line 3506
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->w:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->W:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 3508
    invoke-static {p0}, Lcom/sec/chaton/TabActivity;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3509
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/TabActivity;->C:I

    .line 3514
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 3515
    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->H:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    sub-int/2addr v0, p2

    add-int v3, v1, v0

    .line 3518
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, v3

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/chaton/TabActivity;->C:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v1, v0

    .line 3520
    iget v4, p0, Lcom/sec/chaton/TabActivity;->C:I

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/TabActivity;->a(DIIZ)V

    .line 3522
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;DIIZ)V
    .locals 0

    .prologue
    .line 167
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/TabActivity;->a(DIIZ)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/sec/chaton/TabActivity;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;Landroid/view/MotionEvent;I)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/TabActivity;->a(Landroid/view/MotionEvent;I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/sec/chaton/TabActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/v;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1536
    invoke-static {p0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 1537
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->J()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1538
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->I()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1540
    const-string v0, "PASSWORD_LOCK"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/TabActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1541
    const-string v1, "LOCK_STATE"

    invoke-static {}, Lcom/sec/chaton/util/p;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1542
    invoke-static {}, Lcom/sec/chaton/util/p;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1543
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1544
    const-string v0, "psswordLock was enabled"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/TabActivity;->b(Lcom/sec/chaton/v;Landroid/content/Intent;)V

    .line 1574
    :cond_1
    :goto_0
    return-void

    .line 1548
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 1549
    const-string v0, "showPasswordLockActivity for Result"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    :cond_3
    invoke-static {v3}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 1552
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1553
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1554
    const-class v1, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1555
    const/high16 v0, 0x4000000

    invoke-virtual {p2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1556
    const-string v0, "requestMode"

    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1557
    const-string v0, "MODE"

    const-string v1, "HOME"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1559
    const/4 v0, 0x5

    invoke-virtual {p0, p2, v0}, Lcom/sec/chaton/TabActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1564
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1565
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1568
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->K()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1569
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1570
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2359
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2360
    const-string v0, "showDisclaimer"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2361
    const-string v0, "update_disclaimer_status"

    const-string v1, "RUN"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2362
    const-string v0, "agree_disclaimer"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2363
    const-string v0, "disclaimer_ID"

    invoke-static {v0, p1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/ActivityDisclaimer;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2366
    const-string v1, "disclaimerUID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2367
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2368
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 2372
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;)Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->l:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/TabActivity;Z)Z
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/chaton/TabActivity;->m:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/TabActivity;I)I
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/sec/chaton/TabActivity;->s:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/TabActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/TabActivity;Landroid/widget/LinearLayout$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/chaton/TabActivity;->I:Landroid/widget/LinearLayout$LayoutParams;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->x:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2014
    const-string v0, "callChatList"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2016
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_0

    .line 2017
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Shortcut or Notification: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2019
    :cond_0
    if-eqz v0, :cond_3

    .line 2020
    const-string v0, "specialbuddy"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2023
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    .line 2024
    iput-boolean v4, p0, Lcom/sec/chaton/TabActivity;->A:Z

    .line 2025
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/common/actionbar/a;->d(I)V

    .line 2026
    const v0, 0x7f070009

    const-class v1, Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 2053
    :cond_1
    :goto_0
    return-void

    .line 2028
    :cond_2
    const-string v0, "callChatList"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2029
    const-class v0, Lcom/sec/chaton/chat/ChatActivity;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2030
    const/high16 v0, 0x4000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2032
    invoke-virtual {p0, p1}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2039
    :cond_3
    const-string v0, "chatType"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2041
    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_1

    .line 2042
    invoke-static {p0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/notification/a;->b()V

    .line 2043
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 2044
    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const-string v1, "Content"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/bi;

    invoke-direct {v2, p0}, Lcom/sec/chaton/bi;-><init>(Lcom/sec/chaton/TabActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 2050
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private b(Lcom/sec/chaton/v;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 2456
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/TabActivity;->c(Lcom/sec/chaton/v;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2458
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008d

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2494
    :goto_0
    return-void

    .line 2462
    :cond_0
    sget-object v0, Lcom/sec/chaton/bq;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/v;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2464
    :pswitch_0
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    const-string v1, "chatonno"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v4, "Push Name"

    const-string v6, ""

    invoke-virtual {v1, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2467
    :pswitch_1
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    const-string v1, "chatonno"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Push Name"

    const-string v4, ""

    invoke-virtual {v1, v2, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move v2, v6

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2470
    :pswitch_2
    const-string v0, "chatonno"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2471
    if-nez v3, :cond_1

    const-string v0, "groupId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2472
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "groupId"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 2473
    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v3, v0

    .line 2475
    :cond_1
    if-eqz v3, :cond_2

    array-length v0, v3

    if-ne v0, v6, :cond_2

    .line 2476
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    aget-object v3, v3, v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v4, "Push Name"

    const-string v6, ""

    invoke-virtual {v1, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2478
    :cond_2
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v4, "Push Name"

    const-string v6, ""

    invoke-virtual {v1, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2482
    :pswitch_3
    const-string v0, "chatonno"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2483
    if-nez v3, :cond_3

    const-string v0, "groupId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2484
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "groupId"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 2485
    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v3, v0

    .line 2487
    :cond_3
    if-eqz v3, :cond_4

    array-length v0, v3

    if-ne v0, v6, :cond_4

    .line 2488
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    aget-object v3, v3, v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Push Name"

    const-string v4, ""

    invoke-virtual {v1, v2, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move v2, v6

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2490
    :cond_4
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Push Name"

    const-string v4, ""

    invoke-virtual {v1, v2, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move v2, v6

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2462
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 3367
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/ChatONVUpgradeDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3368
    const-string v1, "isCritical"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3369
    const-string v1, "isFromHome"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3372
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3374
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 3376
    return-void
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 914
    if-nez p0, :cond_0

    move v0, v1

    .line 928
    :goto_0
    return v0

    :cond_0
    move-object v0, p0

    .line 919
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 921
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 922
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 923
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 925
    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    if-ne v0, v4, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget v2, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 926
    goto :goto_0

    .line 928
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/TabActivity;Z)Z
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/chaton/TabActivity;->n:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/chaton/TabActivity;->q:I

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/TabActivity;I)I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/chaton/TabActivity;->s:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/chaton/TabActivity;->s:I

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/TabActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/chaton/TabActivity;->F:Landroid/view/View;

    return-object p1
.end method

.method private c(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2106
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 2107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Shortcut or Notification: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "callChatList"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2109
    :cond_0
    const-string v0, "callForward"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2110
    const-string v0, "callForward"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2111
    const-class v0, Lcom/sec/chaton/forward/ChatForwardActivity;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2112
    const/high16 v0, 0x4000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2113
    invoke-virtual {p0, p1}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 2115
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/TabActivity;Z)Z
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/chaton/TabActivity;->o:Z

    return p1
.end method

.method private c(Lcom/sec/chaton/v;Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2498
    .line 2499
    sget-object v0, Lcom/sec/chaton/bq;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/v;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 2533
    :cond_0
    :goto_0
    return v1

    .line 2503
    :pswitch_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "chatonno"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/e/a/d;->h(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 2504
    goto :goto_0

    .line 2512
    :pswitch_1
    const-string v0, "chatonno"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2513
    if-nez v0, :cond_2

    const-string v3, "groupId"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2514
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "groupId"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 2515
    new-array v3, v1, [Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v3, v0

    .line 2517
    :goto_1
    if-eqz v3, :cond_0

    move v0, v1

    .line 2518
    :goto_2
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 2519
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    aget-object v5, v3, v0

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/d;->h(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    move v1, v2

    .line 2521
    goto :goto_0

    .line 2518
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move-object v3, v0

    goto :goto_1

    .line 2499
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic d(Lcom/sec/chaton/TabActivity;I)I
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/sec/chaton/TabActivity;->r:I

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/TabActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/chaton/TabActivity;->G:Landroid/view/View;

    return-object p1
.end method

.method static synthetic d(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->l()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/TabActivity;Z)Z
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/chaton/TabActivity;->p:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/chaton/TabActivity;->r:I

    return v0
.end method

.method static synthetic e(Lcom/sec/chaton/TabActivity;I)I
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/sec/chaton/TabActivity;->J:I

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/TabActivity;Z)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/sec/chaton/TabActivity;->b(Z)V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/TabActivity;I)I
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/sec/chaton/TabActivity;->K:I

    return p1
.end method

.method static synthetic f(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->n()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/TabActivity;Z)Z
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/chaton/TabActivity;->N:Z

    return p1
.end method

.method static synthetic g(Lcom/sec/chaton/TabActivity;I)I
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/sec/chaton/TabActivity;->C:I

    return p1
.end method

.method static synthetic g(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->M()V

    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/TabActivity;Z)Z
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/chaton/TabActivity;->L:Z

    return p1
.end method

.method static synthetic h(Lcom/sec/chaton/TabActivity;I)I
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/sec/chaton/TabActivity;->B:I

    return p1
.end method

.method static synthetic h(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->N()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/TabActivity;Z)Z
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/chaton/TabActivity;->O:Z

    return p1
.end method

.method static synthetic i(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/chaton/TabActivity;->J:I

    return v0
.end method

.method static synthetic i(Lcom/sec/chaton/TabActivity;I)I
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/sec/chaton/TabActivity;->u:I

    return p1
.end method

.method static synthetic i(Lcom/sec/chaton/TabActivity;Z)Z
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/chaton/TabActivity;->M:Z

    return p1
.end method

.method static synthetic j(Lcom/sec/chaton/TabActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    sget-object v0, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/chaton/TabActivity;->K:I

    return v0
.end method

.method static synthetic l(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->O()I

    move-result v0

    return v0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->a()Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 364
    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->P()I

    move-result v0

    return v0
.end method

.method private m()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 444
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->m:Z

    if-eqz v0, :cond_2

    .line 445
    iput-boolean v8, p0, Lcom/sec/chaton/TabActivity;->m:Z

    .line 446
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->p:Z

    if-eqz v0, :cond_0

    .line 447
    iput-boolean v8, p0, Lcom/sec/chaton/TabActivity;->p:Z

    .line 448
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->x:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x2

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const-string v5, "inbox_unread_count > 0"

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->n:Z

    if-eqz v0, :cond_1

    .line 451
    iput-boolean v8, p0, Lcom/sec/chaton/TabActivity;->n:Z

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->x:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x1

    sget-object v3, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    const-string v5, "buddy_is_new=\'Y\' AND buddy_is_hide=\'N\'"

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->o:Z

    if-eqz v0, :cond_2

    .line 455
    iput-boolean v8, p0, Lcom/sec/chaton/TabActivity;->o:Z

    .line 456
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->x:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x3

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const-string v5, "isNew=\'Y\'"

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_2
    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/TabActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->F:Landroid/view/View;

    return-object v0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 462
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "new_post_on_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/TabActivity;->t:I

    .line 464
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->l()V

    .line 466
    return-void
.end method

.method static synthetic o(Lcom/sec/chaton/TabActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->G:Landroid/view/View;

    return-object v0
.end method

.method private o()V
    .locals 12

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    const/16 v11, 0x8

    const/4 v10, -0x1

    const/4 v5, 0x0

    .line 617
    const v0, 0x7f0704a2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 618
    const v0, 0x7f07049d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 621
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/chaton/TabActivity;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 622
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/TabActivity;->C:I

    .line 627
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->P()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/TabActivity;->B:I

    .line 628
    const v0, 0x7f07049e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->D:Landroid/view/View;

    .line 629
    const v0, 0x7f07049f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    .line 630
    const v0, 0x7f07049c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->F:Landroid/view/View;

    .line 631
    const v0, 0x7f0704a0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->G:Landroid/view/View;

    .line 632
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->F:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->H:Landroid/widget/LinearLayout$LayoutParams;

    .line 633
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->G:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->I:Landroid/widget/LinearLayout$LayoutParams;

    .line 635
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v6, v0, Landroid/content/res/Configuration;->orientation:I

    .line 636
    if-eqz v3, :cond_3

    .line 637
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->D:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 639
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 640
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->b:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 641
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    iget-object v7, p0, Lcom/sec/chaton/TabActivity;->b:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 642
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 643
    const v0, 0x7f0704a0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 644
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 647
    const-string v0, ""

    .line 650
    iget v8, p0, Lcom/sec/chaton/TabActivity;->ar:I

    const v9, 0x7f070008

    if-eq v8, v9, :cond_1

    iget v8, p0, Lcom/sec/chaton/TabActivity;->ar:I

    const v9, 0x7f070009

    if-ne v8, v9, :cond_7

    .line 651
    :cond_1
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 652
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 653
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 654
    iget v1, p0, Lcom/sec/chaton/TabActivity;->ar:I

    const v4, 0x7f070008

    if-ne v1, v4, :cond_5

    .line 655
    const-string v1, "splitview_position_buddy"

    .line 656
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    .line 657
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->o()V

    move-object v0, v1

    .line 666
    :cond_2
    :goto_1
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 667
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    .line 668
    cmpl-float v1, v0, v2

    if-nez v1, :cond_6

    .line 669
    const/high16 v0, 0x3e800000    # 0.25f

    .line 675
    :goto_2
    iget v1, p0, Lcom/sec/chaton/TabActivity;->C:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 676
    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    const v2, 0x7f020416

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 677
    float-to-double v1, v0

    iget v4, p0, Lcom/sec/chaton/TabActivity;->C:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/TabActivity;->a(DIIZ)V

    .line 704
    :cond_3
    :goto_3
    return-void

    .line 624
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->O()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/TabActivity;->C:I

    goto/16 :goto_0

    .line 658
    :cond_5
    iget v1, p0, Lcom/sec/chaton/TabActivity;->ar:I

    const v4, 0x7f070009

    if-ne v1, v4, :cond_2

    .line 659
    const-string v1, "splitview_position_chat"

    .line 661
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgbox/MsgboxFragment;

    .line 662
    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->e()V

    move-object v0, v1

    goto :goto_1

    .line 671
    :cond_6
    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    goto :goto_2

    .line 679
    :cond_7
    invoke-virtual {v3, v11}, Landroid/view/View;->setVisibility(I)V

    .line 680
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->E:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 681
    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 682
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    const v3, 0x7f07000a

    if-ne v0, v3, :cond_a

    .line 683
    const/4 v0, 0x1

    if-ne v6, v0, :cond_8

    .line 686
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->V:Landroid/view/ViewGroup;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v5, v10, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 687
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {v0, v5, v10, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 691
    :cond_8
    const v2, 0x3f1f7cee    # 0.623f

    .line 697
    :cond_9
    :goto_4
    iget v0, p0, Lcom/sec/chaton/TabActivity;->C:I

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 698
    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->V:Landroid/view/ViewGroup;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v10, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 699
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v5, v10, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 692
    :cond_a
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    const v3, 0x7f07000b

    if-ne v0, v3, :cond_9

    move v2, v1

    .line 693
    goto :goto_4

    :cond_b
    move v0, v2

    goto/16 :goto_2
.end method

.method static synthetic p(Lcom/sec/chaton/TabActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->D:Landroid/view/View;

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->w:Landroid/view/ViewTreeObserver;

    .line 1018
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->w:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->W:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1019
    return-void
.end method

.method static synthetic q(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    return v0
.end method

.method private q()V
    .locals 5

    .prologue
    .line 1025
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aw;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1026
    const-string v1, "SELECT g._id, group_name, group_relation_buddy FROM buddy_group g left outer join grouprelation r on g._id = r.group_relation_group order by g._id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1027
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1028
    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1029
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1030
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1031
    const-string v2, "group_name"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1032
    const-string v3, "group_relation_buddy"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1034
    const-string v4, "Favorites"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    if-nez v3, :cond_0

    .line 1035
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/sec/chaton/e/a/f;->b(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 1036
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove garbage group "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", name : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1040
    :catch_0
    move-exception v0

    .line 1041
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    :cond_1
    return-void
.end method

.method static synthetic r(Lcom/sec/chaton/TabActivity;)I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/chaton/TabActivity;->C:I

    return v0
.end method

.method private r()V
    .locals 3

    .prologue
    .line 1341
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1342
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1344
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1345
    const-string v1, "isSyncContacts"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1346
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 1347
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1348
    const-string v0, "SA was NOT registered"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1364
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "first_time_after_regi"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 1365
    return-void
.end method

.method static synthetic s(Lcom/sec/chaton/TabActivity;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->V:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private s()V
    .locals 5

    .prologue
    .line 1386
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    .line 1387
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0331

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1388
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1390
    invoke-static {}, Lcom/sec/chaton/util/ak;->a()Lcom/sec/chaton/util/ak;

    move-result-object v3

    .line 1391
    invoke-virtual {v3, v2}, Lcom/sec/chaton/util/ak;->a(Ljava/lang/String;)V

    .line 1392
    sget-object v2, Lcom/sec/chaton/u;->h:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/sec/chaton/util/ak;->b(Ljava/lang/String;)V

    .line 1393
    sget-object v2, Lcom/sec/chaton/util/ak;->f:Ljava/lang/String;

    sget v4, Lcom/sec/chaton/util/ak;->c:I

    invoke-virtual {v3, v0, v1, v2, v4}, Lcom/sec/chaton/util/ak;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1394
    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1577
    const-string v0, ""

    .line 1579
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1580
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Intent Action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1585
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1586
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Intent Category: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1591
    :goto_1
    return-object v0

    .line 1582
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Intent Action: Null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1588
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Intent Category: Null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic t(Lcom/sec/chaton/TabActivity;)Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->M:Z

    return v0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 1841
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] initView, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1844
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 1849
    :cond_0
    return-void
.end method

.method static synthetic u(Lcom/sec/chaton/TabActivity;)Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->N:Z

    return v0
.end method

.method private v()V
    .locals 8

    .prologue
    const-wide/32 v3, 0xea60

    const-wide/32 v6, 0x5265c00

    .line 1853
    new-instance v0, Lcom/sec/chaton/util/bp;

    invoke-direct {v0}, Lcom/sec/chaton/util/bp;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    .line 1855
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "get_chaton_crypto_key"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->ak:Lcom/sec/chaton/util/bs;

    const-string v5, "last_sync_time_get_chaton_crypto_key"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1857
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "local_calculation"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->ab:Lcom/sec/chaton/util/bs;

    const-string v5, "last_sync_time_on_start"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1858
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "get_push_status"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->ad:Lcom/sec/chaton/util/bs;

    const-string v5, "last_sync_time_get_push_status"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1860
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "version_notice"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->ag:Lcom/sec/chaton/util/bs;

    const-wide/32 v3, 0x1499700

    const-string v5, "last_sync_time_get_version_notice"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1863
    const-string v0, "com.sec.spp.push"

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 1864
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "spp_update_check"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->ah:Lcom/sec/chaton/util/bs;

    const-wide/32 v3, 0xf731400

    const-string v5, "last_sync_time_get_spp_update_check"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1874
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "notification"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->af:Lcom/sec/chaton/util/bs;

    const-wide/32 v3, 0x1b7740

    const-string v5, "last_sync_time_notification"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1875
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "get_all_buddies"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->ac:Lcom/sec/chaton/util/bs;

    const-wide/32 v3, 0xa4cb80

    const-string v5, "last_sync_time_get_all_buddies"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1877
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "recommendee_sync"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->ai:Lcom/sec/chaton/util/bs;

    const-string v5, "last_recommendee_sync_time1"

    move-wide v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1883
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "more_app_sync"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->aj:Lcom/sec/chaton/util/bs;

    const-string v5, "last_more_app_sync_time"

    move-wide v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1888
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1889
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "chatonv_available"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->an:Lcom/sec/chaton/util/bs;

    const-string v5, "last_chatonv_available_time"

    move-wide v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1892
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "chatonv_update_check"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->al:Lcom/sec/chaton/util/bs;

    const-string v5, "last_chatonv_upgrade_check"

    move-wide v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1895
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    const-string v1, "set_compatibility"

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->am:Lcom/sec/chaton/util/bs;

    const-wide/32 v3, 0xdbba0

    const-string v5, "last_compatibility_time"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 1898
    return-void

    .line 1867
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 1868
    const-string v0, "don\'t need to add poliing scheduler for SPP"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1870
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->x()V

    goto :goto_0
.end method

.method static synthetic v(Lcom/sec/chaton/TabActivity;)Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->O:Z

    return v0
.end method

.method static synthetic w(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/io/entry/GetVersionNotice;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->y:Lcom/sec/chaton/io/entry/GetVersionNotice;

    return-object v0
.end method

.method private w()V
    .locals 3

    .prologue
    .line 1902
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->j:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 1903
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1904
    const-string v1, "com.sec.chaton.ACTION_EXIT_APP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1906
    new-instance v1, Lcom/sec/chaton/bg;

    invoke-direct {v1, p0}, Lcom/sec/chaton/bg;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v1, p0, Lcom/sec/chaton/TabActivity;->j:Landroid/content/BroadcastReceiver;

    .line 1916
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 1919
    :cond_0
    return-void
.end method

.method private x()V
    .locals 2

    .prologue
    .line 1923
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->k:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 1924
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1925
    const-string v1, "action_noti_spp_update"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1927
    new-instance v1, Lcom/sec/chaton/bh;

    invoke-direct {v1, p0}, Lcom/sec/chaton/bh;-><init>(Lcom/sec/chaton/TabActivity;)V

    iput-object v1, p0, Lcom/sec/chaton/TabActivity;->k:Landroid/content/BroadcastReceiver;

    .line 1949
    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/TabActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1951
    :cond_0
    return-void
.end method

.method static synthetic x(Lcom/sec/chaton/TabActivity;)Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->L:Z

    return v0
.end method

.method static synthetic y(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->E()V

    return-void
.end method

.method private y()Z
    .locals 2

    .prologue
    .line 1956
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic z(Lcom/sec/chaton/TabActivity;)Lcom/sec/chaton/util/bs;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->ae:Lcom/sec/chaton/util/bs;

    return-object v0
.end method

.method private z()Z
    .locals 3

    .prologue
    .line 1960
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateIsCritical"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a()Landroid/widget/BaseAdapter;
    .locals 2

    .prologue
    .line 2794
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->ap:Lcom/sec/chaton/bx;

    if-nez v0, :cond_0

    .line 2795
    new-instance v0, Lcom/sec/chaton/bx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/bx;-><init>(Lcom/sec/chaton/TabActivity;Lcom/sec/chaton/ap;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->ap:Lcom/sec/chaton/bx;

    .line 2798
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->ap:Lcom/sec/chaton/bx;

    return-object v0
.end method

.method protected a(I)V
    .locals 5

    .prologue
    const v4, 0x7f0704a1

    .line 2807
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 2808
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 2810
    const/4 v1, 0x0

    .line 2812
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aq:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_1

    .line 2813
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aq:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/sec/chaton/by;

    if-eqz v0, :cond_0

    .line 2814
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aq:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/sec/chaton/by;

    invoke-interface {v0}, Lcom/sec/chaton/by;->c()V

    .line 2816
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aq:Landroid/support/v4/app/Fragment;

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 2819
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2820
    invoke-virtual {v2, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2821
    if-eqz v0, :cond_2

    .line 2822
    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 2826
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 2849
    :cond_3
    :goto_0
    if-eqz v1, :cond_5

    .line 2850
    instance-of v0, v1, Lcom/sec/chaton/by;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 2851
    check-cast v0, Lcom/sec/chaton/by;

    invoke-interface {v0}, Lcom/sec/chaton/by;->b()V

    .line 2853
    :cond_4
    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 2856
    :cond_5
    iput-object v1, p0, Lcom/sec/chaton/TabActivity;->aq:Landroid/support/v4/app/Fragment;

    .line 2857
    if-eqz v1, :cond_6

    .line 2858
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getId()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    .line 2861
    :cond_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2862
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->o()V

    .line 2865
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->supportInvalidateOptionsMenu()V

    .line 2867
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2869
    return-void

    .line 2828
    :pswitch_0
    const v0, 0x7f070008

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    goto :goto_0

    .line 2832
    :pswitch_1
    const v0, 0x7f070009

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2834
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2835
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/sec/chaton/chat/EmptyChatFragment;

    invoke-virtual {p0, v4, v0, v2}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_0

    .line 2841
    :pswitch_2
    const v0, 0x7f07000a

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    goto :goto_0

    .line 2845
    :pswitch_3
    const v0, 0x7f07000b

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    goto :goto_0

    .line 2826
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(ILandroid/content/Intent;Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3235
    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 3236
    if-eqz p2, :cond_0

    .line 3237
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 3239
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 3241
    const v2, 0x7f0704a1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 3245
    instance-of v0, v0, Lcom/sec/chaton/trunk/TrunkView;

    if-eqz v0, :cond_1

    .line 3247
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 3251
    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 3252
    return-void
.end method

.method public a(IZ)V
    .locals 2

    .prologue
    .line 3175
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3176
    if-eqz p2, :cond_0

    .line 3177
    const-string v1, "mSmallImageNum"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3179
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 3180
    return-void
.end method

.method public a(Landroid/database/Cursor;Z)V
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 3036
    sput-boolean v10, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 3038
    if-eqz p2, :cond_3

    .line 3039
    invoke-static {p0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 3040
    const-string v1, "callChatList"

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-object v6, v0

    .line 3047
    :goto_0
    const-string v0, "inbox_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 3048
    const-string v0, "buddy_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 3049
    const-string v0, "inbox_chat_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 3050
    const-string v0, "Y"

    const-string v1, "inbox_valid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 3051
    const-string v0, "relation_buddy_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3053
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 3057
    :goto_1
    const-string v2, "inboxNO"

    invoke-virtual {v6, v2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3058
    const-string v2, "chatType"

    invoke-virtual {v6, v2, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3059
    const-string v2, "inboxValid"

    invoke-virtual {v6, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3060
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3061
    const-string v1, "groupId"

    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3067
    :cond_0
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 3070
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v9}, Lcom/sec/chaton/e/z;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "buddy_no"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "rowid DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 3071
    if-eqz v1, :cond_e

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_e

    .line 3072
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 3073
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v8

    move-object v0, v8

    .line 3076
    :goto_2
    if-eqz v1, :cond_1

    .line 3077
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3084
    :cond_1
    :goto_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3085
    const-string v1, "buddyNO"

    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3086
    new-array v7, v10, [Ljava/lang/String;

    aput-object v0, v7, v11

    .line 3087
    const-string v1, "receivers"

    invoke-virtual {v6, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 3091
    :cond_2
    if-eqz p2, :cond_5

    .line 3102
    const-string v0, "[MSGBOX] isForward"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3171
    :goto_4
    return-void

    .line 3042
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3043
    const-string v1, "msgbox"

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-object v6, v0

    goto/16 :goto_0

    .line 3076
    :catchall_0
    move-exception v0

    :goto_5
    if-eqz v7, :cond_4

    .line 3077
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 3076
    :cond_4
    throw v0

    .line 3105
    :cond_5
    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    if-ne v12, v1, :cond_9

    .line 3106
    if-eqz v7, :cond_6

    array-length v1, v7

    if-nez v1, :cond_7

    :cond_6
    if-nez v0, :cond_7

    .line 3107
    const-string v0, "[MSGBOX] ONEtoONE / fail to find"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 3111
    :cond_7
    if-nez v0, :cond_a

    array-length v1, v7

    if-lez v1, :cond_a

    .line 3112
    aget-object v0, v7, v11

    .line 3118
    :cond_8
    :goto_6
    const-string v1, "0999"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    .line 3119
    if-eqz v1, :cond_b

    .line 3120
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[MSGBOX] checkIsBuddyAndIsInBox(), spbd_intent, exist special buddy table : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3121
    const-string v0, "specialbuddy"

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3136
    :cond_9
    :goto_7
    const-string v0, "inbox_last_chat_type"

    const-string v1, "inbox_last_chat_type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3137
    const-string v0, "inbox_session_id"

    const-string v1, "inbox_session_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3138
    const-string v0, "inbox_last_msg_no"

    const-string v1, "inbox_last_msg_no"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v6, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3139
    const-string v0, "inbox_title"

    const-string v1, "inbox_title"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3140
    const-string v0, "_id"

    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3141
    const-string v0, "inbox_server_ip"

    const-string v1, "inbox_server_ip"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3142
    const-string v0, "inbox_server_port"

    const-string v1, "inbox_server_port"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3143
    const-string v0, "inbox_title_fixed"

    const-string v1, "inbox_title_fixed"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3144
    const-string v0, "inbox_last_msg_sender"

    const-string v1, "inbox_last_msg_sender"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3145
    const-string v0, "inbox_last_temp_msg"

    const-string v1, "inbox_last_temp_msg"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3146
    const-string v0, "buddy_name"

    const-string v1, "buddy_name"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3147
    const-string v0, "inbox_trunk_unread_count"

    const-string v1, "inbox_trunk_unread_count"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3148
    const-string v0, "buddy_no"

    const-string v1, "buddy_no"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3149
    const-string v0, "inbox_last_timestamp"

    const-string v1, "inbox_last_timestamp"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v6, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3150
    const-string v0, "inbox_enable_noti"

    const-string v1, "inbox_enable_noti"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3151
    const-string v0, "inbox_unread_count"

    const-string v1, "inbox_unread_count"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3152
    const-string v0, "inbox_is_entered"

    const-string v1, "inbox_is_entered"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3154
    const-string v0, "lasst_session_merge_time"

    const-string v1, "lasst_session_merge_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v6, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3155
    const-string v0, "inbox_participants"

    const-string v1, "inbox_participants"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3156
    const-string v0, "inbox_last_tid"

    const-string v1, "inbox_last_tid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3157
    const-string v0, "inbox_enable_translate"

    const-string v1, "inbox_enable_translate"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3158
    const-string v0, "translate_outgoing_message"

    const-string v1, "translate_outgoing_message"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3159
    const-string v0, "inbox_translate_my_language"

    const-string v1, "inbox_translate_my_language"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3160
    const-string v0, "inbox_translate_buddy_language"

    const-string v1, "inbox_translate_buddy_language"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3161
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 3164
    const v0, 0x7f070009

    const-class v1, Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {p0, v0, v6, v1}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    goto/16 :goto_4

    .line 3113
    :cond_a
    if-nez v0, :cond_8

    .line 3115
    const-string v0, ""

    move-object v0, v9

    goto/16 :goto_6

    .line 3123
    :cond_b
    const-string v1, "buddy_show_phone_number"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 3124
    const-string v2, "buddy_extra_info"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3125
    const-string v3, "buddy_msisdns"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 3126
    const-string v4, "is_buddy"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 3128
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[MSGBOX] onclick addExtrasToIntent "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", extras="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3129
    const-string v0, "showPhoneNumber"

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3130
    const-string v0, "extraInfo"

    invoke-virtual {v6, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3131
    const-string v0, "msisdns"

    invoke-virtual {v6, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3132
    const-string v1, "is_buddy"

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v10

    :goto_8
    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_7

    :cond_c
    move v0, v11

    goto :goto_8

    .line 3168
    :cond_d
    invoke-virtual {p0, v6}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_4

    .line 3076
    :catchall_1
    move-exception v0

    move-object v7, v1

    goto/16 :goto_5

    :cond_e
    move-object v0, v8

    goto/16 :goto_2

    :cond_f
    move-object v0, v8

    goto/16 :goto_3

    :cond_10
    move-object v0, v7

    goto/16 :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3193
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3194
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3195
    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3196
    const-string v1, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3197
    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3198
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 3214
    :goto_0
    return-void

    .line 3201
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3202
    const-string v1, "BUDDY_DIALOG_BUDDY_NO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3203
    const-string v1, "BUDDY_DIALOG_BUDDY_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3207
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3212
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/trunk/c/g;IZ)V
    .locals 2

    .prologue
    .line 3293
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3294
    const-class v1, Lcom/sec/chaton/trunk/TrunkPageActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 3295
    const-string v1, "sessionId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3296
    const-string v1, "inboxNo"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3297
    const-string v1, "itemId"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3298
    const-string v1, "isvalid"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3299
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 3301
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 3318
    iput-boolean p1, p0, Lcom/sec/chaton/TabActivity;->A:Z

    .line 3319
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 3015
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/calllog/view/CallLogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3016
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 3017
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 3391
    const v0, 0x7f0704a2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3392
    const v0, 0x7f0704a0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3397
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3217
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3219
    const-string v1, "buddyId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3220
    const-string v1, "url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3221
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 3222
    return-void
.end method

.method public c(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2571
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2572
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 2573
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 2574
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getClassName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "HomeActivity"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2575
    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 3021
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/msgbox/MsgboxSelectionActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3023
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3024
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/TabActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3029
    :goto_0
    return-void

    .line 3026
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 3183
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/userprofile/EditProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3184
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 3185
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 2608
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f070008

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    .line 2610
    if-eqz v0, :cond_1

    .line 2611
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dispatchKeyEvent : KeyCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dispatchKeyEvent : Current Tab ID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/TabActivity;->ar:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2615
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x54

    if-ne v1, v2, :cond_1

    .line 2616
    iget v1, p0, Lcom/sec/chaton/TabActivity;->ar:I

    if-nez v1, :cond_0

    .line 2617
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->l()V

    .line 2619
    :cond_0
    const/4 v0, 0x1

    .line 2622
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/sec/chaton/FlexibleActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 3188
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/userprofile/MyInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3189
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 3190
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 3272
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f070008

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    .line 3273
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->d()V

    .line 3274
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 3314
    iget-boolean v0, p0, Lcom/sec/chaton/TabActivity;->A:Z

    return v0
.end method

.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 3408
    const v0, 0x7f0c00fa

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 3403
    const v0, 0x7f0c00f9

    return v0
.end method

.method public h()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 3384
    const v0, 0x7f0704a2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3385
    const v0, 0x7f0704a0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3387
    return-void
.end method

.method public i()I
    .locals 1

    .prologue
    .line 3413
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const v3, 0x7f070009

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 2396
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/FlexibleActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2397
    packed-switch p1, :pswitch_data_0

    .line 2451
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2412
    :pswitch_1
    if-ne p2, v1, :cond_1

    .line 2413
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;Z)V

    .line 2414
    const-string v0, "setting_backup_enable"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 2415
    :cond_1
    if-nez p2, :cond_0

    .line 2416
    const-string v0, "auto_backup_on"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 2422
    :pswitch_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 2423
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/sec/chaton/chat/EmptyChatFragment;

    invoke-virtual {p0, v3, v0, v1}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 2424
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    if-ne v0, v3, :cond_0

    .line 2426
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/TabActivity;->ar:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/msgbox/MsgboxFragment;

    .line 2427
    invoke-virtual {v0}, Lcom/sec/chaton/msgbox/MsgboxFragment;->f()V

    goto :goto_0

    .line 2435
    :pswitch_3
    const/4 v0, 0x0

    .line 2436
    if-eqz p3, :cond_2

    .line 2437
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "requestMode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/v;

    .line 2440
    :cond_2
    if-eqz v0, :cond_0

    .line 2444
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 2445
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "REQ_CODE_DIRECT_CHATONV_CALL, mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2448
    :cond_3
    invoke-direct {p0, v0, p3}, Lcom/sec/chaton/TabActivity;->b(Lcom/sec/chaton/v;Landroid/content/Intent;)V

    goto :goto_0

    .line 2397
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 3573
    iget v0, p0, Lcom/sec/chaton/TabActivity;->ar:I

    const v1, 0x7f070008

    if-ne v0, v1, :cond_1

    .line 3574
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 3575
    iget v1, p0, Lcom/sec/chaton/TabActivity;->ar:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    .line 3576
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v1

    if-lez v1, :cond_0

    .line 3577
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a()Z

    .line 3584
    :goto_0
    return-void

    .line 3579
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/FlexibleActivity;->onBackPressed()V

    goto :goto_0

    .line 3582
    :cond_1
    invoke-super {p0}, Lcom/sec/chaton/FlexibleActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 3281
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3282
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->N()V

    .line 3283
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->o()V

    .line 3285
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/FlexibleActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 3286
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 720
    const-string v0, "VerificationLog"

    const-string v1, "ChatON onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    invoke-static {p0}, Lcom/sec/common/g;->a(Landroid/content/Context;)V

    .line 727
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 730
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 731
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    .line 735
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/FlexibleActivity;->onCreate(Landroid/os/Bundle;)V

    .line 737
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 741
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 742
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 743
    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/TabActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v3, 0x7f030117

    invoke-virtual {v1, v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 744
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 745
    const v0, 0x7f07049c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->V:Landroid/view/ViewGroup;

    .line 747
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->o()V

    .line 751
    :cond_1
    const v0, 0x7f070008

    const-class v1, Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/TabActivity;->a(ILjava/lang/Class;)Landroid/support/v4/app/Fragment;

    .line 752
    const v0, 0x7f070009

    const-class v1, Lcom/sec/chaton/msgbox/MsgboxFragment;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/TabActivity;->a(ILjava/lang/Class;)Landroid/support/v4/app/Fragment;

    .line 753
    const v0, 0x7f07000a

    const-class v1, Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/TabActivity;->a(ILjava/lang/Class;)Landroid/support/v4/app/Fragment;

    .line 754
    const v0, 0x7f07000b

    const-class v1, Lcom/sec/chaton/PlusFragment;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/TabActivity;->a(ILjava/lang/Class;)Landroid/support/v4/app/Fragment;

    .line 757
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 760
    invoke-static {}, Lcom/sec/chaton/util/ck;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 761
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_2

    .line 762
    const-string v0, "There isn\'t enough memory for executing ChatON."

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01e2

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 765
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->finish()V

    .line 909
    :cond_3
    :goto_0
    return-void

    .line 771
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->C()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 772
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_5

    .line 773
    const-string v0, "HomeActivity is requested finish."

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->finish()V

    goto :goto_0

    .line 780
    :cond_6
    sget-boolean v0, Lcom/sec/chaton/c/a;->d:Z

    if-eqz v0, :cond_8

    .line 781
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_7

    .line 782
    const-string v0, "Welcome, First Time after update ChatON."

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    :cond_7
    invoke-static {}, Lcom/sec/chaton/util/am;->s()V

    .line 786
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->q()V

    .line 787
    const-string v0, "chaton_version"

    sget-object v1, Lcom/sec/chaton/c/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    sput-boolean v5, Lcom/sec/chaton/c/a;->d:Z

    .line 791
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "buddy_get_all_buddy_mode_instance"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 792
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "restore_livepartner_user_list"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 797
    :cond_8
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->z()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 798
    invoke-static {v4}, Lcom/sec/chaton/util/p;->b(Z)V

    goto :goto_0

    .line 803
    :cond_9
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->y()Z

    move-result v0

    if-nez v0, :cond_a

    .line 805
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->finish()V

    .line 808
    invoke-static {p0}, Lcom/sec/chaton/HomeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 816
    :cond_a
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->w()V

    .line 819
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->v()V

    .line 822
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->E()V

    .line 825
    if-nez p1, :cond_c

    .line 826
    sget-object v0, Lcom/sec/chaton/TabActivity;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 827
    if-nez v0, :cond_b

    move-object v0, v2

    .line 830
    :cond_b
    invoke-direct {p0, v0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Intent;)V

    .line 833
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->h:Lcom/sec/chaton/v;

    sget-object v1, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    if-ne v0, v1, :cond_d

    invoke-static {}, Lcom/sec/chaton/event/f;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 834
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/event/EventDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 837
    :cond_d
    const-string v0, "enteredtab"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 846
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->u()V

    .line 849
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "first_time_after_regi"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_e

    .line 850
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->r()V

    .line 852
    const-string v0, "local_backup_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 854
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->s()V

    .line 863
    :cond_e
    const-string v0, "onCreate : set setShowPasswordLock value"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_from_chaton"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-ne v0, v4, :cond_10

    .line 866
    invoke-static {v5}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 877
    :goto_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 878
    const-string v1, "password_lock_finish"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 879
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->Z:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 883
    const-string v0, "wifi_80_port"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 884
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "wifi_port"

    invoke-static {}, Lcom/sec/chaton/util/bi;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 888
    :cond_f
    iput-boolean v4, p0, Lcom/sec/chaton/TabActivity;->m:Z

    .line 889
    iput-boolean v4, p0, Lcom/sec/chaton/TabActivity;->p:Z

    .line 890
    iput-boolean v4, p0, Lcom/sec/chaton/TabActivity;->n:Z

    .line 891
    iput-boolean v4, p0, Lcom/sec/chaton/TabActivity;->o:Z

    .line 893
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->T:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->x:Lcom/sec/chaton/e/a/u;

    .line 895
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->S:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 896
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->Q:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 897
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->R:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 901
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 902
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->p()V

    .line 904
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/TabActivity;->J:I

    .line 905
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/TabActivity;->K:I

    goto/16 :goto_0

    .line 868
    :cond_10
    if-eqz p1, :cond_11

    invoke-static {}, Lcom/sec/chaton/util/p;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 869
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "savedInstanceState"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 872
    :cond_11
    invoke-static {v4}, Lcom/sec/chaton/util/p;->b(Z)V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1254
    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->aq:Landroid/support/v4/app/Fragment;

    .line 1256
    invoke-super {p0}, Lcom/sec/chaton/FlexibleActivity;->onDestroy()V

    .line 1257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onDestroy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1259
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->P:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->P:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1260
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->P:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 1263
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/sec/chaton/util/cb;->d(Landroid/content/Context;)V

    .line 1266
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->j:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 1267
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1268
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->j:Landroid/content/BroadcastReceiver;

    .line 1273
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->k:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    .line 1274
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1275
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->k:Landroid/content/BroadcastReceiver;

    .line 1279
    :cond_2
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->Z:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1280
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->X:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1286
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->i:Lcom/sec/common/a/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->i:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1287
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->i:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 1291
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/TabActivity;->z:Z

    .line 1294
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->S:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1295
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->Q:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1296
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->R:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1299
    return-void

    .line 1282
    :catch_0
    move-exception v0

    .line 1283
    sget-object v0, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    const-string v1, "receiver not registered properly"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1303
    invoke-super {p0, p1}, Lcom/sec/chaton/FlexibleActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 1304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onNewIntent, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1307
    invoke-virtual {p0, p1}, Lcom/sec/chaton/TabActivity;->setIntent(Landroid/content/Intent;)V

    .line 1310
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1311
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->finish()V

    .line 1335
    :cond_0
    :goto_0
    return-void

    .line 1317
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->A()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1318
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->finish()V

    .line 1320
    invoke-static {p0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1324
    :cond_2
    sget-object v0, Lcom/sec/chaton/TabActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 1326
    if-nez v0, :cond_3

    .line 1329
    :goto_1
    invoke-direct {p0, p1}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Intent;)V

    .line 1332
    invoke-static {}, Lcom/sec/chaton/util/p;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1333
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Z)V

    goto :goto_0

    :cond_3
    move-object p1, v0

    goto :goto_1
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1231
    invoke-super {p0}, Lcom/sec/chaton/FlexibleActivity;->onPause()V

    .line 1233
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/TabActivity;->l:Z

    .line 1235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    :try_start_0
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->U:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1242
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/TabActivity;->Y:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1250
    :goto_0
    return-void

    .line 1247
    :catch_0
    move-exception v0

    .line 1248
    sget-object v0, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    const-string v1, "in ONPause .receiver not registered properly"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1091
    invoke-super {p0}, Lcom/sec/chaton/FlexibleActivity;->onResume()V

    .line 1092
    const-string v0, "VerificationLog"

    const-string v1, "ChatON onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1098
    iput-boolean v5, p0, Lcom/sec/chaton/TabActivity;->l:Z

    .line 1100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104
    invoke-static {}, Lcom/sec/chaton/util/ck;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1105
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_0

    .line 1106
    const-string v0, "There isn\'t enough memory for executing ChatON."

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1108
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01e2

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1109
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->finish()V

    .line 1147
    :goto_0
    return-void

    .line 1114
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->H()V

    .line 1118
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->m()V

    .line 1120
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1121
    const-string v1, "mypage_badge_update"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1122
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->U:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 1126
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1127
    const-string v1, "more_tab_badge_update"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1128
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->Y:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 1134
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "new_feature_ready_to_show"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1135
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "new_feature_count"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1136
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/event/NewFeature;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1137
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "new_feature_url_list"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1138
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    if-lez v0, :cond_2

    .line 1139
    invoke-virtual {p0, v1}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 1141
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "new_feature_ready_to_show"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1143
    :cond_3
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1144
    const-string v1, "buddy_tab_badge_update"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1145
    const-string v1, "buddy_tab_name_update"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1146
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/TabActivity;->X:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    goto/16 :goto_0
.end method

.method protected onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1047
    invoke-super {p0}, Lcom/sec/chaton/FlexibleActivity;->onStart()V

    .line 1048
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStart, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1053
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateAppsReady"

    const-string v2, "NO"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1054
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/UpgradeDialog;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1055
    const-string v2, "isCritical"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1056
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1057
    const-string v2, "isReadyApps"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1058
    invoke-virtual {p0, v1}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 1062
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->c:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    .line 1087
    :cond_0
    :goto_0
    return-void

    .line 1064
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->F()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RUN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1066
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->B()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/TabActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1077
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/TabActivity;->L()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1078
    invoke-direct {p0, v3}, Lcom/sec/chaton/TabActivity;->b(Z)V

    goto :goto_0

    .line 1083
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    if-eqz v0, :cond_0

    .line 1084
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->aa:Lcom/sec/chaton/util/bp;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bp;->a()V

    goto :goto_0
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 2629
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2631
    invoke-super {p0, p1}, Lcom/sec/chaton/FlexibleActivity;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2651
    invoke-super {p0, p1}, Lcom/sec/chaton/FlexibleActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 2652
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2741
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2654
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->P:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->P:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2655
    goto :goto_0

    .line 2658
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2659
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0175

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2660
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0029

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2661
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2662
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b02c1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2663
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b02c2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2666
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/CharSequence;

    .line 2667
    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    .line 2669
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 2670
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b040e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 2671
    new-instance v3, Lcom/sec/chaton/bo;

    invoke-direct {v3, p0}, Lcom/sec/chaton/bo;-><init>(Lcom/sec/chaton/TabActivity;)V

    invoke-virtual {v2, v0, v3}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 2726
    invoke-virtual {v2}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/TabActivity;->P:Lcom/sec/common/a/d;

    .line 2727
    iget-object v0, p0, Lcom/sec/chaton/TabActivity;->P:Lcom/sec/common/a/d;

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setCanceledOnTouchOutside(Z)V

    move v0, v1

    .line 2728
    goto/16 :goto_0

    .line 2730
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings2/SettingActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2731
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 2732
    goto/16 :goto_0

    .line 2736
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityGeneral;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2737
    invoke-virtual {p0, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 2738
    goto/16 :goto_0

    .line 2652
    :pswitch_data_0
    .packed-switch 0x7f070579
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onSupportPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2636
    const v0, 0x7f070579

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2637
    const v1, 0x7f07057a

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    .line 2638
    if-eqz v0, :cond_0

    .line 2639
    sget-boolean v1, Lcom/sec/chaton/buddy/BuddyFragment;->l:Z

    if-ne v1, v3, :cond_1

    iget v1, p0, Lcom/sec/chaton/TabActivity;->ar:I

    const v2, 0x7f070008

    if-ne v1, v2, :cond_1

    .line 2640
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2646
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/FlexibleActivity;->onSupportPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 2642
    :cond_1
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 2549
    const-string v0, "onUserLeaveHint"

    sget-object v1, Lcom/sec/chaton/TabActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2553
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 2554
    invoke-virtual {p0}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 2559
    :goto_0
    return-void

    .line 2556
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
