.class public Lcom/sec/chaton/buddy/cz;
.super Ljava/lang/Object;
.source "BuddyFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field s:Lcom/sec/chaton/e/a/u;

.field final synthetic t:Lcom/sec/chaton/buddy/BuddyFragment;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 2

    .prologue
    .line 6710
    iput-object p1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6671
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->a:I

    .line 6672
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->b:I

    .line 6673
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->c:I

    .line 6674
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->d:I

    .line 6676
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->e:I

    .line 6677
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->f:I

    .line 6680
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->g:I

    .line 6684
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->h:I

    .line 6686
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->i:I

    .line 6688
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->j:I

    .line 6689
    const/16 v0, 0x33

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->k:I

    .line 6691
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->l:I

    .line 6692
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->m:I

    .line 6694
    const/16 v0, 0x3d

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->n:I

    .line 6695
    const/16 v0, 0x3e

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->o:I

    .line 6696
    const/16 v0, 0x3f

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->p:I

    .line 6697
    const/16 v0, 0x40

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->q:I

    .line 6700
    const/16 v0, 0x43

    iput v0, p0, Lcom/sec/chaton/buddy/cz;->r:I

    .line 6708
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    .line 6711
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    if-nez v0, :cond_0

    .line 6712
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    .line 6714
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 6717
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/u;->a()V

    .line 6718
    return-void
.end method

.method public a(I)V
    .locals 11

    .prologue
    const/16 v10, 0xc

    const/16 v9, 0xb

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 6821
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBuddyList() : sortStyle="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", range="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 6829
    const-string v1, ""

    .line 6841
    if-eq p1, v9, :cond_0

    if-eq p1, v10, :cond_0

    const/16 v3, 0xd

    if-ne p1, v3, :cond_14

    .line 6842
    :cond_0
    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 6843
    invoke-static {}, Lcom/sec/chaton/e/i;->d()Landroid/net/Uri;

    move-result-object v3

    .line 6847
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->ad(Lcom/sec/chaton/buddy/BuddyFragment;)[Ljava/lang/String;

    move-result-object v1

    .line 6849
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 6851
    array-length v5, v1

    if-lez v5, :cond_3

    .line 6852
    const-string v5, ""

    .line 6853
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "buddy_no IN ( "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 6854
    :goto_0
    array-length v6, v1

    if-ge v0, v6, :cond_1

    .line 6855
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6854
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6858
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6865
    const-string v7, "group_type, buddy_name COLLATE LOCALIZED ASC"

    move-object v5, v1

    .line 7108
    :goto_1
    if-eqz v3, :cond_2

    .line 7109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "URI : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 7110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WHERE : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 7112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ORDER BY : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 7117
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    move v1, p1

    move-object v4, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 7125
    :cond_3
    return-void

    .line 6876
    :cond_4
    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_6

    .line 6877
    invoke-static {}, Lcom/sec/chaton/e/i;->e()Landroid/net/Uri;

    move-result-object v3

    .line 6879
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->ad(Lcom/sec/chaton/buddy/BuddyFragment;)[Ljava/lang/String;

    move-result-object v1

    .line 6880
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 6882
    array-length v5, v1

    if-lez v5, :cond_3

    .line 6883
    const-string v5, ""

    .line 6884
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "buddy_no IN ( "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 6885
    :goto_2
    array-length v6, v1

    if-ge v0, v6, :cond_5

    .line 6886
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6885
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6889
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6893
    const-string v7, "group_type, buddy_name COLLATE LOCALIZED ASC"

    move-object v5, v1

    .line 6895
    goto/16 :goto_1

    :cond_6
    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    if-ne v3, v8, :cond_a

    .line 6896
    invoke-static {}, Lcom/sec/chaton/e/i;->a()Landroid/net/Uri;

    move-result-object v3

    .line 6898
    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ae(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 6899
    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ad(Lcom/sec/chaton/buddy/BuddyFragment;)[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 6900
    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ad(Lcom/sec/chaton/buddy/BuddyFragment;)[Ljava/lang/String;

    move-result-object v4

    .line 6901
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 6903
    array-length v6, v4

    if-lez v6, :cond_17

    .line 6904
    const-string v1, ""

    .line 6905
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "buddy_no NOT IN ( "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6906
    :goto_3
    array-length v6, v4

    if-ge v0, v6, :cond_7

    .line 6907
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v4, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6906
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 6910
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    move-object v1, v0

    .line 6919
    :cond_8
    :goto_5
    const-string v7, "group_type, buddy_name COLLATE LOCALIZED ASC"

    move-object v5, v1

    goto/16 :goto_1

    .line 6913
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->af(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "inboxNO"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 6914
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no NOT IN (SELECT participants_buddy_no FROM participant WHERE participants_inbox_no = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->af(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "inboxNO"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    .line 6921
    :cond_a
    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_b

    .line 6922
    invoke-static {}, Lcom/sec/chaton/e/m;->a()Landroid/net/Uri;

    move-result-object v3

    .line 6924
    const-string v7, "buddy_name COLLATE LOCALIZED ASC "

    .line 6926
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "group_relation_group = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyFragment;->f:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    goto/16 :goto_1

    .line 6929
    :cond_b
    invoke-static {}, Lcom/sec/chaton/e/i;->b()Landroid/net/Uri;

    move-result-object v3

    .line 6932
    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ae(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 6933
    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ad(Lcom/sec/chaton/buddy/BuddyFragment;)[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_10

    .line 6934
    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ad(Lcom/sec/chaton/buddy/BuddyFragment;)[Ljava/lang/String;

    move-result-object v4

    .line 6935
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 6937
    array-length v6, v4

    if-lez v6, :cond_d

    .line 6938
    const-string v1, ""

    .line 6939
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "buddy_no NOT IN ( "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6940
    :goto_6
    array-length v6, v4

    if-ge v0, v6, :cond_c

    .line 6941
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v4, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6940
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 6944
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6954
    :cond_d
    :goto_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v4, 0xf

    if-eq v0, v4, :cond_e

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v4, 0xe

    if-ne v0, v4, :cond_f

    .line 6955
    :cond_e
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 6956
    const-string v0, ""

    .line 6966
    :goto_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "CASE WHEN group_name IS NOT NULL AND group_type!=1 THEN 1 WHEN buddy_extra_info LIKE \'%voip=1%\' THEN 1 ELSE 0 END"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6972
    :cond_f
    if-ne p1, v10, :cond_12

    .line 6973
    const-string v7, "group_type, CASE WHEN group_name IS NULL THEN 1 ELSE 0 END,group_name COLLATE LOCALIZED ASC, relation_point DESC, buddy_hanzitopinyin COLLATE LOCALIZED ASC"

    move-object v5, v1

    goto/16 :goto_1

    .line 6947
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->af(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "inboxNO"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 6948
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no NOT IN (SELECT participants_buddy_no FROM participant WHERE participants_inbox_no = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->af(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "inboxNO"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_7

    .line 6959
    :cond_11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 6975
    :cond_12
    if-ne p1, v9, :cond_13

    .line 6977
    const-string v7, "group_type, CASE WHEN group_name IS NULL THEN 1 ELSE 0 END,group_name, buddy_hanzitopinyin COLLATE LOCALIZED ASC"

    move-object v5, v1

    goto/16 :goto_1

    .line 6978
    :cond_13
    const/16 v0, 0xd

    if-ne p1, v0, :cond_16

    .line 6979
    const-string v7, "group_type, CASE WHEN group_name IS NULL THEN 1 ELSE 0 END,group_name, buddy_name COLLATE LOCALIZED ASC"

    move-object v5, v1

    goto/16 :goto_1

    .line 7094
    :cond_14
    const/16 v0, 0x15

    if-ne p1, v0, :cond_15

    .line 7095
    invoke-static {}, Lcom/sec/chaton/e/i;->g()Landroid/net/Uri;

    move-result-object v3

    .line 7096
    const-string v7, "group_type, buddy_name COLLATE LOCALIZED ASC"

    move-object v5, v1

    goto/16 :goto_1

    :cond_15
    move-object v7, v2

    move-object v5, v1

    move-object v3, v2

    goto/16 :goto_1

    :cond_16
    move-object v7, v2

    move-object v5, v1

    goto/16 :goto_1

    :cond_17
    move-object v0, v1

    goto/16 :goto_4
.end method

.method public a(Lcom/sec/chaton/buddy/a/c;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 6722
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->f:I

    sget-object v3, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    const-string v5, "group_relation_group = 1 "

    move-object v2, p1

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6723
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 7129
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->n:I

    invoke-static {}, Lcom/sec/chaton/e/i;->c()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "buddy_no = ? "

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v6, v2

    move-object v2, p1

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 7130
    return-void
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 6758
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->e:I

    invoke-static {}, Lcom/sec/chaton/e/i;->i()Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6760
    return-void
.end method

.method public b(Lcom/sec/chaton/buddy/a/c;)V
    .locals 7

    .prologue
    .line 6727
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->b:I

    sget-object v3, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    const-string v4, "group_relation_group = 1 AND group_relation_buddy = ? "

    const/4 v2, 0x1

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 6728
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 7133
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->o:I

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const-string v5, "buddy_no = ? "

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v6, v2

    move-object v2, p1

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 7134
    return-void
.end method

.method public c()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 6763
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->r:I

    invoke-static {}, Lcom/sec/chaton/e/i;->j()Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6764
    return-void
.end method

.method public c(Lcom/sec/chaton/buddy/a/c;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 6732
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->j:I

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const-string v5, "buddy_no=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    move-object v2, p1

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6733
    return-void
.end method

.method public d()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 6770
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->i:I

    invoke-static {}, Lcom/sec/chaton/e/ai;->a()Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6771
    return-void
.end method

.method public d(Lcom/sec/chaton/buddy/a/c;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 6736
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->k:I

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const-string v5, "buddy_no=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    move-object v2, p1

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6737
    return-void
.end method

.method public e()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 6786
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->h:I

    invoke-static {}, Lcom/sec/chaton/e/ag;->a()Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v7, "recommend_timestamp"

    const-wide/16 v8, 0x0

    invoke-virtual {v5, v7, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v4

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6787
    return-void
.end method

.method public e(Lcom/sec/chaton/buddy/a/c;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 6741
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0999"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6742
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 6743
    const-string v0, "is_hide"

    const-string v1, "Y"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6744
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->p:I

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const-string v5, "buddy_no IN ( ? )"

    new-array v6, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 6750
    :goto_0
    return-void

    .line 6746
    :cond_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 6747
    const-string v0, "buddy_is_hide"

    const-string v1, "Y"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6748
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->p:I

    sget-object v3, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    const-string v5, "buddy_no IN ( ? )"

    new-array v6, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public f()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 6793
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 6795
    const-string v0, "buddy_is_profile_updated"

    const-string v1, "N"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6796
    const-string v0, "buddy_is_new"

    const-string v1, "N"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6797
    const-string v0, "buddy_is_status_updated"

    const-string v1, "N"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6798
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->l:I

    sget-object v3, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 6801
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 6802
    const-string v0, "isNew"

    const-string v1, "N"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6803
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->l:I

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 6805
    return-void
.end method

.method public g()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 6808
    const-string v0, "setNewGroupClear() : "

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 6809
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 6810
    const-string v0, "group_is_new"

    const-string v1, "N"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6813
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->m:I

    sget-object v3, Lcom/sec/chaton/e/n;->a:Landroid/net/Uri;

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 6814
    return-void
.end method

.method public h()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 6817
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->q:I

    invoke-static {}, Lcom/sec/chaton/e/i;->h()Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6818
    return-void
.end method

.method public i()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 7140
    .line 7141
    invoke-static {}, Lcom/sec/chaton/e/i;->b()Landroid/net/Uri;

    move-result-object v3

    .line 7142
    const-string v5, ""

    .line 7143
    const-string v0, ""

    .line 7145
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->af(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 7146
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 7148
    array-length v0, v1

    if-lez v0, :cond_1

    .line 7149
    const-string v0, ""

    .line 7150
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "buddy_no NOT IN ( "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 7151
    const/4 v0, 0x0

    :goto_0
    array-length v6, v1

    if-ge v0, v6, :cond_0

    .line 7152
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7155
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 7159
    :cond_1
    const-string v7, "group_type, CASE WHEN group_name IS NULL THEN 1 ELSE 0 END,group_name, buddy_name COLLATE LOCALIZED ASC"

    .line 7161
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    const/16 v1, 0x46

    move-object v4, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 7176
    return-void
.end method

.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 8134
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 8169
    :cond_0
    :goto_0
    return-void

    .line 8138
    :cond_1
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->b:I

    if-ne p1, v0, :cond_0

    .line 8142
    const/4 v0, -0x1

    if-eq p3, v0, :cond_2

    .line 8153
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->n(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Toast;

    move-result-object v0

    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 8154
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->n(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 8155
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->n(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 8158
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Y(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto :goto_0

    .line 8164
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0074

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 8166
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 7990
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 8038
    :cond_0
    :goto_0
    return-void

    .line 7994
    :cond_1
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->a:I

    if-ne p1, v0, :cond_0

    .line 7995
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 7997
    invoke-virtual {p3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 8001
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 8003
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    const v1, 0x7f0b0070

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 8021
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->n(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 8022
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->n(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Toast;->setDuration(I)V

    .line 8023
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->n(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 8026
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Y(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto :goto_0

    .line 8032
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 8034
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v8, 0x2

    const v10, 0x7f0b00b9

    const/16 v6, 0x8

    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 7212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onQueryComplete() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 7218
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    .line 7985
    :cond_0
    :goto_0
    return-void

    .line 7222
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, v7}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Lcom/sec/chaton/buddy/BuddyFragment;Z)Z

    .line 7224
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->S(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 7226
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->e:I

    if-ne p1, v0, :cond_14

    .line 7229
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7230
    const-string v0, "count"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 7233
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 7236
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->d(Lcom/sec/chaton/buddy/BuddyFragment;I)I

    .line 7241
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->ae(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 7242
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v2, v10, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7248
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v2, v10, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7252
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    if-eq v1, v6, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    if-eq v1, v8, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/16 v2, 0x3e8

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->am(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->am(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "voip"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 7254
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->k(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 7255
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v2, v10, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7260
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BUDDY COUNT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v2

    sub-int v2, v0, v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 7261
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    sub-int/2addr v0, v1

    if-lez v0, :cond_10

    .line 7263
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->an(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 7864
    :cond_4
    :goto_2
    const/16 v0, 0xb

    if-eq p1, v0, :cond_5

    const/16 v0, 0xc

    if-eq p1, v0, :cond_5

    const/16 v0, 0x15

    if-eq p1, v0, :cond_5

    const/16 v0, 0xd

    if-ne p1, v0, :cond_5b

    .line 7866
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->M(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 7871
    const/16 v0, 0xb

    if-eq p1, v0, :cond_6

    const/16 v0, 0xc

    if-eq p1, v0, :cond_6

    const/16 v0, 0xd

    if-ne p1, v0, :cond_59

    .line 7872
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    .line 7873
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v1, 0x12

    if-eq v0, v1, :cond_7

    .line 7874
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Z(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/ag;->b()V

    .line 7876
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    .line 7927
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->af(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->af(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_9

    .line 7929
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 7935
    :cond_9
    if-eqz p3, :cond_a

    .line 7936
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 7939
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aD(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/ViewStub;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 7940
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-eq v0, v9, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_b

    .line 7941
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    sub-int/2addr v0, v1

    if-ge v0, v9, :cond_5c

    .line 7942
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aD(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/ViewStub;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/view/ViewStub;)V

    .line 7953
    :cond_b
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ae(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v0

    if-nez v0, :cond_5f

    .line 7954
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-eq v0, v8, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v1, 0xf

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v1, 0xe

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v1, 0x13

    if-ne v0, v1, :cond_5e

    .line 7955
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->k(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 7956
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v10, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7978
    :cond_d
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->am(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->am(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "voip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 7979
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->k(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 7980
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v10, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7984
    :cond_e
    const-string v0, "VerificationLog"

    const-string v1, "ChatON Executed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 7245
    :cond_f
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v2, v10, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 7266
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v9, :cond_12

    .line 7267
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ao(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 7268
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->k(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/chaton/widget/ClearableEditText;->setVisibility(I)V

    .line 7269
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ap(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    .line 7270
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aq(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 7297
    :cond_11
    :goto_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ar(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/db;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 7298
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ar(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/db;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/sec/chaton/buddy/db;->b(I)V

    goto/16 :goto_2

    .line 7273
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_13

    .line 7274
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0078

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 7276
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 7278
    const-string v1, "receivers"

    new-array v2, v7, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 7280
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v7, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 7281
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_6

    .line 7283
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_11

    .line 7286
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->an(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto :goto_6

    .line 7302
    :cond_14
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->i:I

    if-ne p1, v0, :cond_1b

    .line 7304
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7305
    const-string v0, "count"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 7307
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 7308
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->e(Lcom/sec/chaton/buddy/BuddyFragment;I)I

    .line 7310
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->ae(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 7313
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_15

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    if-eq v1, v8, :cond_15

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_15

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/16 v2, 0x13

    if-eq v1, v2, :cond_15

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/16 v2, 0xf

    if-eq v1, v2, :cond_15

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/16 v2, 0xe

    if-ne v1, v2, :cond_18

    .line 7314
    :cond_15
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v2, v10, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7325
    :goto_7
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_16

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    if-eq v1, v6, :cond_16

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_16

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    if-eq v1, v8, :cond_16

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    const/16 v2, 0x3e8

    if-ne v1, v2, :cond_17

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->am(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_17

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->am(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "voip"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 7327
    :cond_16
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->k(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v1

    if-eqz v1, :cond_17

    .line 7328
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v2, v10, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7333
    :cond_17
    if-lez v0, :cond_1a

    .line 7334
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, v9}, Lcom/sec/chaton/buddy/BuddyFragment;->c(Lcom/sec/chaton/buddy/BuddyFragment;Z)Z

    .line 7335
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->an(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto/16 :goto_2

    .line 7317
    :cond_18
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v2, v10, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 7321
    :cond_19
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v2, v10, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 7337
    :cond_1a
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, v7}, Lcom/sec/chaton/buddy/BuddyFragment;->c(Lcom/sec/chaton/buddy/BuddyFragment;Z)Z

    goto/16 :goto_2

    .line 7343
    :cond_1b
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->g:I

    if-ne p1, v0, :cond_1f

    .line 7344
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1c

    .line 7345
    const-string v0, "GET_PARTICIPANTS : cursor is not null, but cursor count is zero"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7349
    :cond_1c
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 7350
    const-string v1, "participants_buddy_no"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 7352
    :cond_1d
    :goto_8
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 7353
    invoke-interface {p3, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_1d

    .line 7354
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 7357
    :cond_1e
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 7359
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;[Ljava/lang/String;)[Ljava/lang/String;

    .line 7360
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->b()V

    goto/16 :goto_2

    .line 7365
    :cond_1f
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->h:I

    if-ne p1, v0, :cond_21

    .line 7366
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 7368
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1, v7}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;I)I

    .line 7377
    :try_start_0
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7378
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    const-string v2, "count"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7380
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 7383
    if-ne v0, v9, :cond_20

    .line 7389
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->as(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->b(I)V

    goto/16 :goto_2

    .line 7380
    :catchall_0
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 7391
    :cond_20
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v7}, Lcom/sec/chaton/buddy/BuddyFragment;->b(I)V

    goto/16 :goto_2

    .line 7414
    :cond_21
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->f:I

    if-ne p1, v0, :cond_23

    .line 7415
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 7417
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 7419
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "favorites count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7421
    const/16 v1, 0x32

    if-lt v0, v1, :cond_22

    .line 7423
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v9, [Ljava/lang/Object;

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 7428
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 7431
    :cond_22
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 7433
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 7435
    const-string v1, "group_relation_group"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7436
    const-string v1, "group_relation_buddy"

    invoke-virtual {p2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7438
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v2

    iget v2, v2, Lcom/sec/chaton/buddy/cz;->a:I

    sget-object v3, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, p2, v3, v0}, Lcom/sec/chaton/e/a/u;->startInsert(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;)V

    goto/16 :goto_2

    .line 7440
    :cond_23
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->j:I

    if-ne p1, v0, :cond_26

    .line 7441
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7442
    const-string v0, "islike"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 7444
    const-string v0, "likecount"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 7445
    if-eqz v0, :cond_24

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_25

    :cond_24
    move v0, v7

    .line 7450
    :goto_9
    const-string v2, "buddy_no"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 7451
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 7453
    if-nez v1, :cond_4

    .line 7459
    new-instance v1, Lcom/sec/chaton/d/h;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 7460
    invoke-virtual {v1, v2}, Lcom/sec/chaton/d/h;->e(Ljava/lang/String;)V

    .line 7463
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 7464
    const-string v1, "islike"

    const-string v3, "Y"

    invoke-virtual {v4, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7465
    const-string v1, "likecount"

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7466
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->j:I

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buddy_no=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_2

    .line 7448
    :cond_25
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_9

    .line 7468
    :cond_26
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->k:I

    if-ne p1, v0, :cond_2a

    .line 7469
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7470
    const-string v0, "islike"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 7472
    const-string v0, "likecount"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 7474
    if-eqz v0, :cond_27

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_29

    :cond_27
    move v0, v7

    .line 7479
    :goto_a
    const-string v2, "buddy_no"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 7480
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 7482
    if-eqz v1, :cond_4

    .line 7486
    new-instance v1, Lcom/sec/chaton/d/h;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 7487
    invoke-virtual {v1, v2}, Lcom/sec/chaton/d/h;->f(Ljava/lang/String;)V

    .line 7490
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 7491
    const-string v1, "islike"

    const-string v3, "N"

    invoke-virtual {v4, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7492
    if-lez v0, :cond_28

    .line 7493
    const-string v1, "likecount"

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7496
    :cond_28
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->s:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/cz;->k:I

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buddy_no=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_2

    .line 7477
    :cond_29
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_a

    .line 7500
    :cond_2a
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->o:I

    if-ne p1, v0, :cond_2d

    .line 7501
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2c

    .line 7516
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 7517
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/SpecialBuddyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7518
    const-string v1, "specialuserid"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7519
    const-string v1, "speicalusername"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7520
    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7522
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 7525
    :cond_2b
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7526
    const-string v1, "specialuserid"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7527
    const-string v1, "speicalusername"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7528
    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7530
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 7533
    :cond_2c
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/sec/chaton/buddy/a/c;

    goto/16 :goto_2

    .line 7549
    :cond_2d
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->n:I

    if-ne p1, v0, :cond_35

    .line 7550
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_31

    .line 7554
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->at(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 7555
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 7556
    const-string v1, "callChatList"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7559
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 7560
    const-string v1, "receivers"

    new-array v2, v9, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 7561
    const-string v1, "content_type"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->au(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 7562
    const-string v1, "download_uri"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->av(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7564
    const-string v1, "sub_content"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->aw(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7565
    const-string v1, "forward_sender_name"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->ax(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7566
    const-string v1, "is_forward_mode"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->ay(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7570
    sget-object v1, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->az(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/w;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 7583
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 7584
    :cond_2e
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aA(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-eqz v0, :cond_2f

    .line 7585
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 7586
    const-string v1, "callChatList"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7587
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 7588
    const-string v1, "receivers"

    new-array v2, v9, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 7589
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 7592
    :cond_2f
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 7593
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7594
    const-string v1, "PROFILE_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7595
    const-string v1, "PROFILE_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7596
    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 7598
    const-string v1, "PROFILE_BUDDY_FROM_CHATINFO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->aB(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7601
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 7604
    :cond_30
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7605
    const-string v1, "BUDDY_DIALOG_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7606
    const-string v1, "BUDDY_DIALOG_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7607
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 7612
    :cond_31
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v0

    .line 7615
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_33

    .line 7616
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7617
    const-string v2, "PROFILE_BUDDY_NO"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7618
    if-eqz v0, :cond_32

    .line 7619
    const-string v2, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7621
    :cond_32
    const-string v0, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 7623
    const-string v0, "PROFILE_BUDDY_FROM_CHATINFO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->aB(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7626
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 7629
    :cond_33
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7630
    const-string v2, "BUDDY_DIALOG_BUDDY_NO"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7632
    if-eqz v0, :cond_34

    .line 7633
    const-string v2, "BUDDY_DIALOG_BUDDY_NAME"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7635
    :cond_34
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 7640
    :cond_35
    const/16 v0, 0xb

    if-ne p1, v0, :cond_3f

    .line 7642
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_36

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3a

    .line 7645
    :cond_36
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    .line 7670
    :goto_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/util/ArrayList;)V

    .line 7672
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v6, :cond_37

    .line 7673
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aC(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 7674
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3d

    .line 7676
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 7677
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aD(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/ViewStub;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/view/ViewStub;)V

    .line 7686
    :cond_37
    :goto_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->F(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v9, :cond_39

    .line 7688
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_39

    .line 7689
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 7690
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aF(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v0

    if-ne v0, v9, :cond_3e

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aG(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v0

    if-ne v0, v9, :cond_3e

    .line 7691
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aH(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 7692
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aH(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 7694
    :cond_38
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aI(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 7695
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->v(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setEnabled(Z)V

    .line 7703
    :cond_39
    :goto_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->F(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    .line 7647
    :cond_3a
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v8, :cond_3b

    .line 7649
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    goto/16 :goto_b

    .line 7651
    :cond_3b
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v9, :cond_3c

    .line 7653
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->c(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    goto/16 :goto_b

    .line 7665
    :cond_3c
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->d(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    goto/16 :goto_b

    .line 7679
    :cond_3d
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 7680
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aE(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto/16 :goto_c

    .line 7697
    :cond_3e
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aI(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 7698
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aD(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/ViewStub;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/view/ViewStub;)V

    goto :goto_d

    .line 7708
    :cond_3f
    const/16 v0, 0xc

    if-ne p1, v0, :cond_45

    .line 7715
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_40

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_42

    .line 7716
    :cond_40
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    .line 7724
    :goto_e
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v6, :cond_41

    .line 7725
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aC(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 7726
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_44

    .line 7728
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 7730
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aD(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/ViewStub;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/view/ViewStub;)V

    .line 7739
    :cond_41
    :goto_f
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->F(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    .line 7717
    :cond_42
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v8, :cond_43

    .line 7718
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    goto :goto_e

    .line 7721
    :cond_43
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->e(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    goto :goto_e

    .line 7732
    :cond_44
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 7734
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aE(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto :goto_f

    .line 7742
    :cond_45
    const/16 v0, 0xd

    if-ne p1, v0, :cond_4b

    .line 7743
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_46

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_48

    .line 7744
    :cond_46
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    .line 7750
    :goto_10
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v6, :cond_47

    .line 7751
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4a

    .line 7752
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aC(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 7753
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 7755
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aD(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/ViewStub;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/view/ViewStub;)V

    .line 7764
    :cond_47
    :goto_11
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->F(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    .line 7745
    :cond_48
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v8, :cond_49

    .line 7746
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    goto :goto_10

    .line 7748
    :cond_49
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    goto :goto_10

    .line 7757
    :cond_4a
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aC(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 7758
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 7761
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aE(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto :goto_11

    .line 7765
    :cond_4b
    const/16 v0, 0x15

    if-ne p1, v0, :cond_4f

    .line 7770
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->k(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7771
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 7772
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, v7}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Z)Z

    .line 7774
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->K(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_4c

    .line 7775
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    .line 7777
    :cond_4c
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 7778
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->H(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 7779
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->F(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 7780
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->M(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto/16 :goto_2

    .line 7783
    :cond_4d
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V

    .line 7786
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aJ(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4e

    .line 7787
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 7788
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->H(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 7796
    :goto_12
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aJ(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->aK(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    .line 7791
    :cond_4e
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 7792
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->H(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_12

    .line 7801
    :cond_4f
    const/16 v0, 0x46

    if-ne p1, v0, :cond_51

    .line 7804
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_50

    .line 7806
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7808
    const-string v1, "ACTIVITY_PURPOSE"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 7809
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->ad(Lcom/sec/chaton/buddy/BuddyFragment;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 7810
    const-string v1, "chatType"

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->aL(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 7812
    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_2

    .line 7814
    :cond_50
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->n(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Toast;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0078

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7815
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->n(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/Toast;->setDuration(I)V

    .line 7816
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->n(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 7820
    :cond_51
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->q:I

    if-ne p1, v0, :cond_57

    .line 7821
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_52

    .line 7822
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 7823
    const/4 v0, -0x1

    invoke-interface {p3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 7824
    :goto_13
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_52

    .line 7825
    const-string v0, "buddy_no"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 7826
    invoke-static {}, Lcom/sec/chaton/buddy/BuddyFragment;->r()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_13

    .line 7830
    :cond_52
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/chaton/buddy/BuddyFragment;->r()Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 7831
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_53
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_56

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 7832
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_54
    :goto_14
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 7833
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_54

    .line 7834
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move v2, v7

    .line 7836
    :goto_15
    array-length v5, v1

    if-ge v2, v5, :cond_61

    .line 7837
    invoke-static {}, Lcom/sec/chaton/buddy/BuddyFragment;->r()Ljava/util/HashMap;

    move-result-object v5

    aget-object v6, v1, v2

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_55

    move v1, v8

    .line 7842
    :goto_16
    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/a/c;->a(I)V

    goto :goto_14

    .line 7836
    :cond_55
    add-int/lit8 v2, v2, 0x1

    goto :goto_15

    .line 7846
    :cond_56
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->F(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    .line 7849
    :cond_57
    const/16 v0, 0x47

    if-ne p1, v0, :cond_4

    .line 7850
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_4

    .line 7851
    const/4 v0, -0x1

    invoke-interface {p3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 7852
    :cond_58
    :goto_17
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 7853
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 7854
    const-string v1, "group_name"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 7855
    const-string v2, "group_relation_buddy"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 7856
    const-string v3, "Favorites"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_58

    if-nez v2, :cond_58

    .line 7857
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/chaton/e/a/f;->b(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 7858
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remove garbage group "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", name : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_17

    .line 7878
    :cond_59
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aM(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/EditTextWithClearButton;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 7879
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aM(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/EditTextWithClearButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/EditTextWithClearButton;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5a

    .line 7880
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    goto/16 :goto_3

    .line 7882
    :cond_5a
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    goto/16 :goto_3

    .line 7914
    :cond_5b
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->r:I

    if-ne p1, v0, :cond_8

    .line 7915
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 7916
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    const-string v1, "count"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;I)I

    .line 7917
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hide count : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7918
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 7919
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_8

    .line 7920
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    goto/16 :goto_3

    .line 7944
    :cond_5c
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5d

    .line 7945
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aD(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/ViewStub;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/view/ViewStub;)V

    goto/16 :goto_4

    .line 7947
    :cond_5d
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aE(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto/16 :goto_4

    .line 7960
    :cond_5e
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->k(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 7961
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v10, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 7966
    :cond_5f
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->k(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 7968
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_60

    .line 7969
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v10, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 7972
    :cond_60
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->ah(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ai(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->aj(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->ak(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v10, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_61
    move v1, v9

    goto/16 :goto_16
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, -0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 8042
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 8129
    :cond_0
    :goto_0
    return-void

    .line 8045
    :cond_1
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->j:I

    if-ne p1, v0, :cond_4

    .line 8046
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 8048
    if-eq p3, v1, :cond_2

    .line 8059
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->J(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 8119
    :cond_2
    :goto_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 8120
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->y(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->z(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 8121
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->y(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->z(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_9

    .line 8122
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->A(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 8070
    :cond_4
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->k:I

    if-ne p1, v0, :cond_5

    .line 8071
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 8073
    if-eq p3, v1, :cond_2

    .line 8084
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->J(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto :goto_1

    .line 8095
    :cond_5
    iget v0, p0, Lcom/sec/chaton/buddy/cz;->p:I

    if-ne p1, v0, :cond_2

    .line 8096
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/cz;->b()V

    .line 8097
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 8098
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b031f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 8099
    invoke-virtual {p2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0999"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 8100
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 8101
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->X(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/ChoicePanel;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/widget/ChoicePanel;->a(Ljava/lang/String;)V

    .line 8102
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {p2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8103
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {p2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 8105
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_8

    .line 8106
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->y(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->z(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 8107
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->y(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 8108
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->z(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 8114
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Y(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto/16 :goto_1

    .line 8111
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    goto :goto_2

    .line 8124
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/cz;->t:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->A(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method
