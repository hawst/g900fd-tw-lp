.class Lcom/sec/chaton/buddy/cb;
.super Ljava/lang/Object;
.source "BuddyFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/b/d;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 6530
    iput-object p1, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IZLjava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x2

    .line 6536
    if-eqz p2, :cond_5

    .line 6537
    if-ne p1, v4, :cond_2

    .line 6538
    instance-of v0, p3, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 6539
    check-cast p3, Landroid/database/Cursor;

    .line 6541
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 6542
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 6544
    new-instance v0, Lcom/sec/chaton/e/b/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyFragment;->r:Lcom/sec/chaton/e/b/d;

    iget-object v2, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/e/b/h;-><init>(Lcom/sec/chaton/e/b/d;Lcom/sec/chaton/buddy/a/c;IZ)V

    .line 6546
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    invoke-static {v1, v3, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 6615
    :cond_0
    :goto_0
    return-void

    .line 6549
    :cond_1
    new-instance v0, Lcom/sec/chaton/e/b/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyFragment;->r:Lcom/sec/chaton/e/b/d;

    iget-object v2, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/sec/chaton/e/b/h;-><init>(Lcom/sec/chaton/e/b/d;Lcom/sec/chaton/buddy/a/c;IZ)V

    .line 6551
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    invoke-static {v1, v3, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    goto :goto_0

    .line 6554
    :cond_2
    if-ne p1, v3, :cond_3

    .line 6555
    iget-object v0, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6559
    iget-object v0, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->S(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 6560
    iget-object v0, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b013d

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 6562
    iget-object v0, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->b()V

    goto :goto_0

    .line 6565
    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    .line 6566
    instance-of v0, p3, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 6567
    iget-object v0, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    .line 6569
    iget-object v0, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 6603
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6604
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 6605
    sget-object v0, Lcom/sec/chaton/buddy/EmptyFragment;->c:Ljava/lang/String;

    sget-object v2, Lcom/sec/chaton/buddy/EmptyFragment;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6606
    iget-object v0, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const v2, 0x7f070008

    const-class v3, Lcom/sec/chaton/buddy/EmptyFragment;

    invoke-virtual {v0, v2, v1, v3}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 6607
    iget-object v0, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Z(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/ag;

    move-result-object v0

    invoke-virtual {v0, v6, v6}, Lcom/sec/chaton/buddy/ag;->a(II)V

    goto/16 :goto_0

    .line 6613
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/cb;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->S(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto/16 :goto_0
.end method
