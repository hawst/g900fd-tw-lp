.class Lcom/sec/chaton/buddy/cg;
.super Ljava/lang/Object;
.source "BuddyFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/cf;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/cf;)V
    .locals 0

    .prologue
    .line 9113
    iput-object p1, p0, Lcom/sec/chaton/buddy/cg;->a:Lcom/sec/chaton/buddy/cf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 9116
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 9117
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 9118
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/cg;->a:Lcom/sec/chaton/buddy/cf;

    iget-object v0, v0, Lcom/sec/chaton/buddy/cf;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 9129
    :goto_0
    return-void

    .line 9121
    :cond_1
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cg;->a:Lcom/sec/chaton/buddy/cf;

    iget-object v1, v1, Lcom/sec/chaton/buddy/cf;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aO(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 9122
    const-string v1, "group"

    iget-object v4, p0, Lcom/sec/chaton/buddy/cg;->a:Lcom/sec/chaton/buddy/cf;

    iget-object v4, v4, Lcom/sec/chaton/buddy/cf;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyFragment;->af(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/cg;->a:Lcom/sec/chaton/buddy/cf;

    iget-object v5, v5, Lcom/sec/chaton/buddy/cf;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x145

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)I

    .line 9123
    iget-object v0, p0, Lcom/sec/chaton/buddy/cg;->a:Lcom/sec/chaton/buddy/cf;

    iget-object v0, v0, Lcom/sec/chaton/buddy/cf;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aP(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-nez v0, :cond_2

    .line 9124
    iget-object v0, p0, Lcom/sec/chaton/buddy/cg;->a:Lcom/sec/chaton/buddy/cf;

    iget-object v0, v0, Lcom/sec/chaton/buddy/cf;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/cg;->a:Lcom/sec/chaton/buddy/cf;

    iget-object v1, v1, Lcom/sec/chaton/buddy/cf;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/cg;->a:Lcom/sec/chaton/buddy/cf;

    iget-object v2, v2, Lcom/sec/chaton/buddy/cf;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b00b6

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    invoke-static {v1, v3, v2, v4}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto :goto_0

    .line 9126
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/cg;->a:Lcom/sec/chaton/buddy/cf;

    iget-object v0, v0, Lcom/sec/chaton/buddy/cf;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->aP(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method
