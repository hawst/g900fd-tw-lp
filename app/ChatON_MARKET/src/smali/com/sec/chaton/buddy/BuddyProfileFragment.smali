.class public Lcom/sec/chaton/buddy/BuddyProfileFragment;
.super Landroid/support/v4/app/Fragment;
.source "BuddyProfileFragment.java"

# interfaces
.implements Lcom/sec/chaton/poston/j;
.implements Lcom/sec/common/f/f;


# static fields
.field public static final a:Ljava/lang/String;

.field private static aa:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Z

.field private C:I

.field private D:Landroid/widget/ImageView;

.field private E:Landroid/widget/ImageView;

.field private F:Landroid/widget/ImageView;

.field private G:Landroid/widget/ImageView;

.field private H:Landroid/widget/ImageView;

.field private I:Landroid/widget/ImageView;

.field private J:Landroid/widget/ImageView;

.field private K:Landroid/widget/ImageView;

.field private L:Lcom/sec/chaton/buddy/fl;

.field private M:Lcom/sec/chaton/buddy/fl;

.field private N:Lcom/sec/chaton/buddy/fl;

.field private O:Lcom/sec/chaton/buddy/fl;

.field private P:Landroid/widget/ImageButton;

.field private Q:Landroid/widget/TextView;

.field private R:Landroid/widget/TextView;

.field private S:Landroid/widget/ImageButton;

.field private T:Landroid/widget/ImageButton;

.field private U:Landroid/app/ProgressDialog;

.field private V:Ljava/lang/String;

.field private W:Lcom/sec/chaton/poston/d;

.field private X:Landroid/widget/ListView;

.field private Y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/poston/k;",
            ">;"
        }
    .end annotation
.end field

.field private Z:Lcom/sec/chaton/d/v;

.field private aA:Ljava/lang/String;

.field private aB:Ljava/lang/String;

.field private aC:Ljava/lang/String;

.field private aD:Z

.field private aE:Lcom/sec/chaton/d/w;

.field private aF:Landroid/graphics/Bitmap;

.field private aG:Lcom/sec/common/f/c;

.field private aH:Ljava/lang/String;

.field private aI:Ljava/lang/String;

.field private aJ:Ljava/lang/String;

.field private aK:Z

.field private aL:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aM:Z

.field private aN:Z

.field private aO:I

.field private aP:I

.field private aQ:Lcom/coolots/sso/a/a;

.field private aR:Landroid/app/Activity;

.field private aS:Landroid/widget/LinearLayout;

.field private aT:Landroid/widget/LinearLayout;

.field private aU:Landroid/widget/LinearLayout;

.field private aV:Landroid/widget/LinearLayout;

.field private aW:Landroid/widget/LinearLayout;

.field private aX:Landroid/widget/TextView;

.field private aY:Landroid/widget/ImageView;

.field private aZ:Landroid/widget/ImageView;

.field private ab:Landroid/view/View;

.field private ac:Landroid/view/View;

.field private ad:Landroid/widget/ImageView;

.field private ae:Landroid/view/View;

.field private af:Landroid/widget/ImageView;

.field private ag:Landroid/widget/FrameLayout;

.field private ah:Landroid/widget/ImageButton;

.field private ai:Landroid/widget/ImageButton;

.field private aj:Landroid/widget/ImageButton;

.field private ak:Landroid/widget/ImageButton;

.field private al:Landroid/widget/ImageButton;

.field private am:Landroid/widget/TextView;

.field private an:Landroid/widget/TextView;

.field private ao:Landroid/widget/TextView;

.field private ap:Landroid/widget/TextView;

.field private aq:Landroid/widget/LinearLayout;

.field private ar:Landroid/widget/TextView;

.field private as:Landroid/widget/LinearLayout;

.field private at:Landroid/view/inputmethod/InputMethodManager;

.field private au:Landroid/widget/ImageView;

.field private av:Z

.field private aw:Lcom/sec/chaton/d/i;

.field private ax:Ljava/lang/String;

.field private ay:Ljava/lang/String;

.field private az:Ljava/lang/String;

.field b:Z

.field private ba:Ljava/lang/String;

.field private bb:Ljava/lang/String;

.field private bc:Z

.field private bd:Z

.field private be:Landroid/widget/ImageButton;

.field private bf:Landroid/os/Handler;

.field private bg:Landroid/os/Handler;

.field c:Landroid/view/View$OnClickListener;

.field d:Landroid/os/Handler;

.field e:Lcom/sec/chaton/e/a/v;

.field f:Landroid/widget/AbsListView$OnScrollListener;

.field g:Lcom/sec/chaton/e/b/d;

.field h:Landroid/database/ContentObserver;

.field i:Landroid/database/ContentObserver;

.field private j:Z

.field private k:I

.field private l:Lcom/sec/chaton/e/a/u;

.field private m:Landroid/view/Menu;

.field private n:Landroid/view/MenuItem;

.field private o:Landroid/view/MenuItem;

.field private p:Landroid/widget/Toast;

.field private q:Lcom/sec/chaton/buddy/BuddyProfileFragment;

.field private r:Lcom/sec/chaton/buddy/BuddyProfileFragment;

.field private s:Lcom/sec/chaton/buddy/a/c;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160
    const-class v0, Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    .line 255
    const-string v0, ""

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aa:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 157
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 198
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->k:I

    .line 209
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->n:Landroid/view/MenuItem;

    .line 211
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o:Landroid/view/MenuItem;

    .line 222
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->y:Z

    .line 223
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->z:Z

    .line 224
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->A:Z

    .line 225
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->B:Z

    .line 226
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->C:I

    .line 242
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->U:Landroid/app/ProgressDialog;

    .line 251
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Y:Ljava/util/ArrayList;

    .line 296
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->av:Z

    .line 307
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aF:Landroid/graphics/Bitmap;

    .line 308
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    .line 310
    const-string v0, "&size=800"

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aI:Ljava/lang/String;

    .line 311
    const-string v0, "&size=100"

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aJ:Ljava/lang/String;

    .line 314
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aK:Z

    .line 315
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aL:Ljava/util/ArrayList;

    .line 321
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aN:Z

    .line 335
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b:Z

    .line 340
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aS:Landroid/widget/LinearLayout;

    .line 341
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aT:Landroid/widget/LinearLayout;

    .line 342
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aU:Landroid/widget/LinearLayout;

    .line 343
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aV:Landroid/widget/LinearLayout;

    .line 344
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aW:Landroid/widget/LinearLayout;

    .line 346
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aX:Landroid/widget/TextView;

    .line 347
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aY:Landroid/widget/ImageView;

    .line 348
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aZ:Landroid/widget/ImageView;

    .line 349
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/flag/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ba:Ljava/lang/String;

    .line 354
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bc:Z

    .line 355
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bd:Z

    .line 360
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    .line 1117
    new-instance v0, Lcom/sec/chaton/buddy/ey;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ey;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bf:Landroid/os/Handler;

    .line 1313
    new-instance v0, Lcom/sec/chaton/buddy/ez;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ez;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    .line 1769
    new-instance v0, Lcom/sec/chaton/buddy/fa;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fa;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d:Landroid/os/Handler;

    .line 2385
    new-instance v0, Lcom/sec/chaton/buddy/fc;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fc;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e:Lcom/sec/chaton/e/a/v;

    .line 2880
    new-instance v0, Lcom/sec/chaton/buddy/fg;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fg;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f:Landroid/widget/AbsListView$OnScrollListener;

    .line 3228
    new-instance v0, Lcom/sec/chaton/buddy/fh;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fh;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bg:Landroid/os/Handler;

    .line 3524
    new-instance v0, Lcom/sec/chaton/buddy/eu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/eu;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->g:Lcom/sec/chaton/e/b/d;

    .line 4081
    new-instance v0, Lcom/sec/chaton/buddy/ev;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/ev;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h:Landroid/database/ContentObserver;

    .line 4102
    new-instance v0, Lcom/sec/chaton/buddy/ew;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/ew;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->w:Z

    return v0
.end method

.method static synthetic B(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic C(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic D(Lcom/sec/chaton/buddy/BuddyProfileFragment;)I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aO:I

    return v0
.end method

.method static synthetic E(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aN:Z

    return v0
.end method

.method static synthetic F(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->y()Z

    move-result v0

    return v0
.end method

.method static synthetic G(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x()Z

    move-result v0

    return v0
.end method

.method static synthetic H(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->z()Z

    move-result v0

    return v0
.end method

.method static synthetic I(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->v()Z

    move-result v0

    return v0
.end method

.method static synthetic J(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic K(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aQ:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method static synthetic L(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/BuddyProfileFragment;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->r:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    return-object v0
.end method

.method static synthetic M(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aq:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic N(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ba:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic O(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bb:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic P(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aZ:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic Q(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    return-object v0
.end method

.method static synthetic R(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->au:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic S(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/d/i;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    return-object v0
.end method

.method static synthetic T(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ay:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic U(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ax:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic V(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i()Z

    move-result v0

    return v0
.end method

.method static synthetic W(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->p:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic X(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j()V

    return-void
.end method

.method static synthetic Y(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/d/v;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Z:Lcom/sec/chaton/d/v;

    return-object v0
.end method

.method static synthetic Z(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aS:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileFragment;Lcom/sec/chaton/buddy/a/c;)Lcom/sec/chaton/buddy/a/c;
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    return-object p1
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 4

    .prologue
    .line 3442
    .line 3444
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 3445
    const-string v0, "setCoverstory is called in BPF"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3449
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3450
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "coverstory.jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3452
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    .line 3453
    const-string v0, "setCoverstory CoverStory is empty. loadDefaultCoverStory() "

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3454
    invoke-static {p1}, Lcom/sec/chaton/util/f;->a(Landroid/widget/ImageView;)Z

    .line 3455
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->av:Z

    if-eqz v0, :cond_2

    .line 3456
    const-string v0, "getBuddiesCoverStory in case of mProfileFromBuddyDialog "

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3457
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    if-nez v0, :cond_1

    .line 3458
    new-instance v0, Lcom/sec/chaton/d/i;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/i;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    .line 3460
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;)V

    .line 3487
    :cond_2
    :goto_0
    return-void

    .line 3463
    :cond_3
    const-string v1, "setCoverstory"

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3475
    if-eqz p1, :cond_4

    .line 3476
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "coverstory.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3482
    :catch_0
    move-exception v0

    .line 3483
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 3484
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 3478
    :cond_4
    :try_start_1
    const-string v0, "mCoverstoryImg is null. loadDefaultCoverStory()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileFragment;I)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(I)V

    return-void
.end method

.method private a(ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 3685
    if-eqz p1, :cond_0

    .line 3686
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bc:Z

    .line 3703
    :goto_0
    return-void

    .line 3690
    :cond_0
    if-eqz p2, :cond_1

    .line 3691
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ai:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3692
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->am:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3693
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aj:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3694
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->an:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 3696
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ai:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3697
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->am:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3698
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aj:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3699
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->an:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 2905
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2907
    const/high16 v1, 0x10000

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 2909
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z
    .locals 0

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    return p1
.end method

.method static synthetic aa(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    return v0
.end method

.method static synthetic ab(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aA:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ac(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aB:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ad(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/BuddyProfileFragment;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->q:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    return-object v0
.end method

.method static synthetic ae(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->U:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic af(Lcom/sec/chaton/buddy/BuddyProfileFragment;)I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->C:I

    return v0
.end method

.method static synthetic ag(Lcom/sec/chaton/buddy/BuddyProfileFragment;)I
    .locals 2

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aP:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aP:I

    return v0
.end method

.method static synthetic ah(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->H:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic ai(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aH:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic aj(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aI:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ak(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic al(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->I:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic am(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aJ:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic an(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->J:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic ao(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->K:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic ap(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->P:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic aq(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->R:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyProfileFragment;I)I
    .locals 0

    .prologue
    .line 157
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aO:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aT:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private b(I)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 834
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->k:I

    .line 835
    if-ne p1, v8, :cond_13

    .line 837
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bg:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aE:Lcom/sec/chaton/d/w;

    .line 839
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 841
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f070076

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D:Landroid/widget/ImageView;

    .line 842
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f070124

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->H:Landroid/widget/ImageView;

    .line 843
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702b5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->E:Landroid/widget/ImageView;

    .line 844
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702b6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->I:Landroid/widget/ImageView;

    .line 845
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->F:Landroid/widget/ImageView;

    .line 846
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702b8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->J:Landroid/widget/ImageView;

    .line 847
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702b9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->G:Landroid/widget/ImageView;

    .line 848
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702ba

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->K:Landroid/widget/ImageView;

    .line 849
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0700bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->P:Landroid/widget/ImageButton;

    .line 850
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Q:Landroid/widget/TextView;

    .line 851
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->R:Landroid/widget/TextView;

    .line 853
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ad:Landroid/widget/ImageView;

    .line 855
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->E()I

    move-result v0

    if-ne v0, v8, :cond_f

    .line 856
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ad:Landroid/widget/ImageView;

    const v1, 0x7f0200ef

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 857
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 865
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702bb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->S:Landroid/widget/ImageButton;

    .line 866
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->S:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 867
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->T:Landroid/widget/ImageButton;

    .line 868
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->T:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 871
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 872
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    .line 873
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 874
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 879
    :cond_1
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    .line 880
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->E:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    .line 881
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->F:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    .line 882
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->G:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    .line 887
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x:Z

    if-eqz v0, :cond_10

    .line 889
    invoke-virtual {p0, v8}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Z)V

    .line 896
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/f;->a(Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 906
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c()V

    .line 908
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 909
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 910
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Q:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setSelected(Z)V

    .line 912
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 914
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Q:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/buddy/et;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/et;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 924
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 942
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ae:Landroid/view/View;

    const v1, 0x7f0702ce

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    .line 947
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ae:Landroid/view/View;

    const v1, 0x7f0702cd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->au:Landroid/widget/ImageView;

    .line 950
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->au:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Landroid/widget/ImageView;)V

    .line 951
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 953
    iput-boolean v8, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    .line 954
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Z:Lcom/sec/chaton/d/v;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/v;->a(Ljava/lang/String;)V

    .line 955
    new-instance v0, Lcom/sec/chaton/poston/d;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Y:Ljava/util/ArrayList;

    const v4, 0x7f03012a

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/poston/d;-><init>(Landroid/widget/ListView;Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    .line 957
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/poston/d;->a(Lcom/sec/chaton/poston/j;)V

    .line 959
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    invoke-virtual {v0, v1, v10, v7}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 961
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 962
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    .line 964
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 965
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v1, v10, v7}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 966
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 967
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setScrollbarFadingEnabled(Z)V

    .line 968
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 970
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aq:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 972
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mProfileImageHistoryDone : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->A:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mProfileAllDone : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->z:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->A:Z

    if-nez v0, :cond_4

    .line 975
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aE:Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    const-string v2, "160"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/w;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    :cond_4
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->z:Z

    if-nez v0, :cond_5

    .line 978
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aE:Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->d(Ljava/lang/String;)V

    .line 1000
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1001
    const/4 v1, -0x3

    if-eq v1, v0, :cond_6

    const/4 v1, -0x2

    if-ne v1, v0, :cond_7

    .line 1002
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1006
    :cond_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1008
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->am:Landroid/widget/TextView;

    .line 1009
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702ca

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->an:Landroid/widget/TextView;

    .line 1010
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ao:Landroid/widget/TextView;

    .line 1011
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ap:Landroid/widget/TextView;

    .line 1014
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ah:Landroid/widget/ImageButton;

    .line 1015
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ah:Landroid/widget/ImageButton;

    if-eqz v0, :cond_8

    .line 1016
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ah:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1023
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ai:Landroid/widget/ImageButton;

    .line 1024
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ai:Landroid/widget/ImageButton;

    if-eqz v0, :cond_9

    .line 1025
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ai:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1026
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->am:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1029
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aj:Landroid/widget/ImageButton;

    .line 1030
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aj:Landroid/widget/ImageButton;

    if-eqz v0, :cond_a

    .line 1031
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aj:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1032
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->an:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1037
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ak:Landroid/widget/ImageButton;

    .line 1038
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ak:Landroid/widget/ImageButton;

    if-eqz v0, :cond_b

    .line 1039
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ak:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1040
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ak:Landroid/widget/ImageButton;

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1041
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ao:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1044
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->al:Landroid/widget/ImageButton;

    .line 1045
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->al:Landroid/widget/ImageButton;

    if-eqz v0, :cond_c

    .line 1046
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->al:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1047
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->al:Landroid/widget/ImageButton;

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1048
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1051
    :cond_c
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u()V

    .line 1055
    :cond_d
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->A:Z

    if-eqz v0, :cond_e

    .line 1056
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->C:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_e

    .line 1057
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->P:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1113
    :cond_e
    :goto_4
    return-void

    .line 859
    :cond_f
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 860
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ad:Landroid/widget/ImageView;

    const v1, 0x7f0200ed

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 861
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 892
    :cond_10
    invoke-virtual {p0, v7}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Z)V

    goto/16 :goto_1

    .line 897
    :catch_0
    move-exception v0

    .line 898
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 899
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 901
    :catch_1
    move-exception v0

    .line 902
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 903
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 927
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->R:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 928
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 929
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->R:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setSelected(Z)V

    .line 931
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->R:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/buddy/ex;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ex;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_3

    .line 1061
    :cond_13
    const/4 v0, 0x2

    if-ne p1, v0, :cond_e

    .line 1063
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    .line 1064
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 1066
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030116

    invoke-virtual {v0, v1, v10, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1076
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setContentView(Landroid/view/View;)V

    .line 1078
    const v0, 0x7f0700c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D:Landroid/widget/ImageView;

    .line 1079
    const v0, 0x7f0700aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Q:Landroid/widget/TextView;

    .line 1080
    const v0, 0x7f0700c6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aY:Landroid/widget/ImageView;

    .line 1081
    const v0, 0x7f0700c7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aZ:Landroid/widget/ImageView;

    .line 1083
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1084
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Q:Landroid/widget/TextView;

    const v2, 0x7f0b00b4

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1089
    :goto_5
    const v0, 0x7f070496

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aX:Landroid/widget/TextView;

    .line 1091
    const v0, 0x7f070497

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aS:Landroid/widget/LinearLayout;

    .line 1092
    const v0, 0x7f070498

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aT:Landroid/widget/LinearLayout;

    .line 1093
    const v0, 0x7f070499

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aU:Landroid/widget/LinearLayout;

    .line 1094
    const v0, 0x7f07049a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aV:Landroid/widget/LinearLayout;

    .line 1095
    const v0, 0x7f07049b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aW:Landroid/widget/LinearLayout;

    .line 1097
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aS:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1098
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aT:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1099
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aU:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1100
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aV:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1101
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aW:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1105
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 1109
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bf:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1110
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v7}, Lcom/sec/chaton/d/h;->e(Ljava/lang/String;Z)I

    goto/16 :goto_4

    .line 1086
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Q:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->g(Ljava/lang/String;)V

    return-void
.end method

.method private b(Z)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3751
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->w()Z

    move-result v3

    .line 3752
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x()Z

    move-result v4

    .line 3753
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->z()Z

    move-result v5

    .line 3757
    if-eqz p1, :cond_6

    .line 3758
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processMultiDeviceCase : buddyPhoneNumberAvailable = "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v6, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3759
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t()Z

    move-result p1

    .line 3761
    if-nez p1, :cond_6

    .line 3762
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v6, "phone"

    invoke-virtual {v0, v6}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 3763
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.SENDTO"

    const-string v8, "smsto:+000"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0, v6}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 3769
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->v()Z

    move-result v6

    .line 3775
    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    .line 3776
    if-eqz v5, :cond_1

    .line 3777
    if-eqz v6, :cond_0

    .line 3778
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(ZZ)V

    .line 3779
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bd:Z

    .line 3807
    :goto_1
    return-void

    .line 3782
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(ZZ)V

    goto :goto_1

    .line 3786
    :cond_1
    if-eqz v6, :cond_2

    .line 3787
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(ZZ)V

    .line 3788
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bd:Z

    goto :goto_1

    .line 3791
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(ZZ)V

    goto :goto_1

    .line 3795
    :cond_3
    if-eqz v3, :cond_5

    .line 3796
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->y()Z

    move-result v1

    if-nez v1, :cond_4

    if-eqz v6, :cond_4

    .line 3797
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(ZZ)V

    goto :goto_1

    .line 3800
    :cond_4
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(ZZ)V

    goto :goto_1

    .line 3804
    :cond_5
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(ZZ)V

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method private b(ZZ)V
    .locals 4

    .prologue
    const v3, 0x7f0200f6

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 3710
    if-eqz p1, :cond_0

    .line 3712
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ai:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3713
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->am:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3714
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aj:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3715
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->an:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3716
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ak:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3717
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->al:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3718
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->al:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 3719
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ao:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3720
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3744
    :goto_0
    return-void

    .line 3723
    :cond_0
    if-eqz p2, :cond_1

    .line 3724
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ai:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3725
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->am:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3726
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aj:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3727
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->an:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3728
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ak:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3729
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->al:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3730
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ao:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3731
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 3733
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ai:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3734
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->am:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3735
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aj:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3736
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->an:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3737
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ak:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3738
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->al:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3739
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->al:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 3740
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ao:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3741
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z
    .locals 0

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x:Z

    return p1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 2352
    const/4 v0, 0x0

    .line 2357
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 2358
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    .line 2360
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_0

    .line 2361
    const-string v3, "[isMatchNumber] matching"

    sget-object v4, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2364
    :cond_0
    if-le v1, v5, :cond_3

    if-le v2, v5, :cond_3

    .line 2365
    add-int/lit8 v1, v1, -0x8

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 2366
    add-int/lit8 v2, v2, -0x8

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 2367
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2368
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[isMatchNumber] matching for contact Number : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2371
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[isMatchNumber] found match number : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2372
    const/4 v0, 0x1

    .line 2382
    :goto_0
    return v0

    .line 2375
    :cond_2
    const-string v1, "[isMatchNumber] phone number not matched"

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2379
    :cond_3
    const-string v1, "[isMatchNumber] phone number length is less than 8"

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyProfileFragment;I)I
    .locals 0

    .prologue
    .line 157
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->C:I

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aU:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Ljava/lang/String;)V

    return-void
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 4048
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4049
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 4050
    if-eqz p1, :cond_1

    .line 4051
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 4058
    :cond_0
    :goto_0
    return-void

    .line 4054
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z
    .locals 0

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->w:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyProfileFragment;I)I
    .locals 0

    .prologue
    .line 157
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aP:I

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aW:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bb:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z
    .locals 0

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aM:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Q:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->as:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 404
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aq:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 405
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Z:Lcom/sec/chaton/d/v;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/v;->a(Ljava/lang/String;)V

    .line 407
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z
    .locals 0

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aN:Z

    return p1
.end method

.method private e(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 384
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x13

    if-lt v1, v2, :cond_1

    const-string v1, "10"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 385
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 386
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chaton id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :cond_0
    :goto_0
    return v0

    .line 389
    :cond_1
    const-string v1, "0"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 390
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 391
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pin number : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 395
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "phone number: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aX:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ax:Ljava/lang/String;

    return-object p1
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 755
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 756
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00040001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 760
    :cond_0
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aQ:Lcom/coolots/sso/a/a;

    .line 762
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aa:Ljava/lang/String;

    .line 763
    new-instance v0, Lcom/sec/chaton/d/v;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/v;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Z:Lcom/sec/chaton/d/v;

    .line 766
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ac:Landroid/view/View;

    const v1, 0x7f070456

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aq:Landroid/widget/LinearLayout;

    .line 767
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ac:Landroid/view/View;

    const v1, 0x7f070509

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ar:Landroid/widget/TextView;

    .line 768
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ac:Landroid/view/View;

    const v1, 0x7f070507

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->as:Landroid/widget/LinearLayout;

    .line 769
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->as:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 772
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702bd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ag:Landroid/widget/FrameLayout;

    .line 773
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    const v1, 0x7f0702be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->af:Landroid/widget/ImageView;

    .line 774
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ag:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 820
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aA:Ljava/lang/String;

    .line 821
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aB:Ljava/lang/String;

    .line 822
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    .line 824
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j:Z

    .line 825
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    .line 826
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b6

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->U:Landroid/app/ProgressDialog;

    .line 827
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/random/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->az:Ljava/lang/String;

    .line 828
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ay:Ljava/lang/String;

    .line 829
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z
    .locals 0

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->B:Z

    return p1
.end method

.method private f(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1268
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ba:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1269
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1278
    :cond_0
    :goto_0
    return v0

    .line 1273
    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ba:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".png"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1275
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    return-object p1
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1260
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l()V

    .line 1261
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    .line 1262
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/f;->a(Ljava/lang/String;)V

    .line 1263
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c(Z)V

    return-void
.end method

.method private g(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1283
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ba:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1284
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1286
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 1288
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1289
    if-eqz v0, :cond_0

    .line 1290
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aZ:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1292
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aZ:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1301
    :cond_0
    :goto_0
    return-void

    .line 1295
    :catch_0
    move-exception v0

    .line 1296
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    .line 1297
    :catch_1
    move-exception v0

    .line 1298
    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aK:Z

    return v0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->v:Ljava/lang/String;

    return-object p1
.end method

.method private h(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1307
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1308
    invoke-virtual {v0, p1}, Lcom/sec/chaton/d/h;->h(Ljava/lang/String;)V

    .line 1309
    return-void
.end method

.method private h()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1723
    const/4 v0, 0x1

    .line 1725
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v2

    .line 1726
    const/4 v3, -0x3

    if-eq v3, v2, :cond_0

    const/4 v3, -0x2

    if-ne v3, v2, :cond_1

    .line 1727
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0205

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 1734
    :cond_1
    return v0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2344
    .line 2346
    const-string v0, "\\D+"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2348
    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->k()V

    return-void
.end method

.method private i()Z
    .locals 3

    .prologue
    .line 2334
    const/4 v0, 0x0

    .line 2336
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2337
    const/4 v0, 0x1

    .line 2340
    :cond_0
    return v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 2683
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2684
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2685
    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2686
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2688
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->m()V

    return-void
.end method

.method private j(Ljava/lang/String;)V
    .locals 7

    .prologue
    const v6, 0x7f0b00d8

    const/4 v5, 0x0

    const-wide/16 v2, 0x0

    .line 3951
    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 3953
    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c(Ljava/lang/String;)V

    .line 3955
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aM:Z

    if-nez v0, :cond_2

    .line 3957
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3958
    const-string v1, "phone"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3959
    const-string v1, "name"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3961
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "phoneNum: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3962
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NAME: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3965
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3966
    const-string v1, "email"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3969
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.INSERT"

    invoke-direct {v1, v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 3970
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 3971
    const-string v0, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3972
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3973
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 3976
    :cond_1
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4008
    :goto_0
    return-void

    .line 3977
    :catch_0
    move-exception v0

    .line 3978
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 3979
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v6, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 3984
    :cond_2
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->w:Z

    if-nez v0, :cond_5

    .line 3985
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aN:Z

    if-eqz v0, :cond_4

    move-wide v0, v2

    .line 3992
    :goto_1
    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    .line 3993
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aO:I

    int-to-long v0, v0

    .line 3996
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3998
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3999
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4002
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 4003
    :catch_1
    move-exception v0

    .line 4004
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 4005
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v6, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 3988
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->k()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/sec/chaton/account/i;->a(J)J

    move-result-wide v0

    goto :goto_1

    :cond_5
    move-wide v0, v2

    goto :goto_1
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aY:Landroid/widget/ImageView;

    return-object v0
.end method

.method private k(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 4109
    const-string v0, ""

    .line 4111
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/q;->a()Ljava/lang/String;

    move-result-object v0

    .line 4113
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4114
    new-instance v1, Lcom/sec/chaton/util/a;

    invoke-static {v0}, Lcom/sec/chaton/util/a;->b(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v0}, Lcom/sec/chaton/util/a;->c(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/util/a;-><init>([B[B)V

    .line 4118
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/a;->b([B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/a;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 4123
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 4124
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "paramBefore: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, ", paramAfter: "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4126
    :cond_0
    return-object v0

    .line 4116
    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Fail in getting a key"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 4119
    :catch_0
    move-exception v0

    .line 4120
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Encryption Error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 2693
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2694
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 2695
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2697
    :cond_0
    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aV:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 2914
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 2915
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2916
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->U:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2919
    :cond_0
    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 2922
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 2923
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2924
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->U:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2927
    :cond_0
    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    .line 3019
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3021
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 3022
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3023
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3024
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3026
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->startActivity(Landroid/content/Intent;)V

    .line 3028
    :cond_0
    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x:Z

    return v0
.end method

.method static synthetic o(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/a/c;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    return-object v0
.end method

.method private o()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3031
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3032
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v1

    .line 3033
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3034
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, "|"

    invoke-direct {v2, v1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3036
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3037
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 3038
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 3040
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x13

    if-lt v3, v4, :cond_0

    const-string v3, "10"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3041
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chaton id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3049
    :goto_1
    const-string v1, ""

    goto :goto_0

    .line 3042
    :cond_0
    const-string v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3043
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pin number : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 3045
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "msisdn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3046
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3053
    :cond_2
    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 3497
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l()V

    .line 3498
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 3499
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;)Lcom/sec/chaton/d/a/h;

    .line 3500
    return-void
.end method

.method static synthetic q(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    return-object v0
.end method

.method private q()V
    .locals 3

    .prologue
    .line 3506
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l()V

    .line 3508
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    .line 3509
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/f;->a(Ljava/lang/String;I)V

    .line 3510
    return-void
.end method

.method static synthetic r(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    return-object v0
.end method

.method private r()V
    .locals 3

    .prologue
    .line 3515
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3517
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l()V

    .line 3518
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d:Landroid/os/Handler;

    invoke-static {v1}, Lcom/sec/chaton/d/n;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/n;

    move-result-object v1

    .line 3519
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3520
    const-string v2, "true"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/d/n;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3521
    return-void
.end method

.method static synthetic s(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/fl;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    return-object v0
.end method

.method private s()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3630
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 3634
    :goto_0
    if-eqz v0, :cond_4

    .line 3636
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t()Z

    move-result v3

    .line 3637
    if-nez v3, :cond_3

    .line 3639
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v4, "phone"

    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 3640
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.SENDTO"

    const-string v6, "smsto:+000"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 3646
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "processNoChatONVCase : buddyPhoneNumberAvailable = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3648
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 3649
    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3651
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processNoChatONVCase : buddyPhoneNumberAvailable2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3657
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processNoChatONVCase : buddyPhoneNumberAvailable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " @ isContactShow = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3658
    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(ZZ)V

    .line 3659
    return-void

    :cond_0
    move v0, v2

    .line 3630
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 3654
    goto :goto_2

    :cond_2
    move v1, v3

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v0

    move v0, v2

    goto :goto_1
.end method

.method static synthetic t(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/fl;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    return-object v0
.end method

.method private t()Z
    .locals 4

    .prologue
    .line 3666
    .line 3668
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 3669
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-nez v0, :cond_0

    .line 3670
    const/4 v0, 0x0

    .line 3676
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is3GCallAvailable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3678
    return v0

    .line 3673
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    const-string v3, "tel:+000"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic u(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/fl;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    return-object v0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 3813
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    if-nez v0, :cond_0

    .line 3839
    :goto_0
    return-void

    .line 3817
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v0

    if-eqz v0, :cond_1

    .line 3820
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showphonenumber : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3822
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->w:Z

    if-nez v0, :cond_2

    .line 3823
    const-string v0, "refreshCommunicationButtons : is not multi-device"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3824
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s()V

    goto :goto_0

    .line 3827
    :cond_2
    const-string v0, "refreshCommunicationButtons : is multi-device"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3828
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o()Ljava/util/List;

    move-result-object v0

    .line 3830
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 3831
    const/4 v0, 0x0

    .line 3837
    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Z)V

    goto :goto_0

    .line 3834
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method static synthetic v(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/fl;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    return-object v0
.end method

.method private v()Z
    .locals 3

    .prologue
    .line 3846
    const/4 v0, 0x0

    .line 3848
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    if-eqz v1, :cond_0

    .line 3850
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3851
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->D()Ljava/lang/String;

    move-result-object v1

    .line 3853
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3854
    const-string v2, "voip=1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 3855
    const/4 v0, 0x1

    .line 3864
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVBuddy() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3865
    return v0

    .line 3859
    :catch_0
    move-exception v1

    .line 3860
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic w(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->g()V

    return-void
.end method

.method private w()Z
    .locals 3

    .prologue
    .line 3873
    const/4 v0, 0x0

    .line 3876
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 3878
    const/4 v0, 0x1

    .line 3884
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVSupportedDevice() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3885
    return v0

    .line 3880
    :catch_0
    move-exception v1

    .line 3881
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic x(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->p()V

    return-void
.end method

.method private x()Z
    .locals 4

    .prologue
    .line 3893
    const/4 v0, 0x0

    .line 3896
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 3898
    const/4 v0, 0x1

    .line 3904
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVAvaiable() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3905
    return v0

    .line 3900
    :catch_0
    move-exception v1

    .line 3901
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic y(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->q()V

    return-void
.end method

.method private y()Z
    .locals 3

    .prologue
    .line 3913
    const/4 v0, 0x0

    .line 3916
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aQ:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 3918
    const/4 v0, 0x1

    .line 3924
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVInstalled() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3925
    return v0

    .line 3920
    :catch_0
    move-exception v1

    .line 3921
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic z(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->r()V

    return-void
.end method

.method private z()Z
    .locals 4

    .prologue
    .line 3933
    const/4 v1, 0x0

    .line 3936
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aQ:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 3937
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isReadyToCall() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3942
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVReadyToCall() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3943
    return v0

    .line 3938
    :catch_0
    move-exception v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    .line 3939
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 3938
    :catch_1
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    .line 3113
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    add-int/lit8 v5, p1, -0x1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/f;->a(Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Ljava/lang/String;I)V

    .line 3114
    return-void
.end method

.method public a(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3118
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->B:Z

    if-eqz v0, :cond_1

    .line 3119
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3120
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 3129
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aP:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aP:I

    .line 3130
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aP:I

    if-nez v0, :cond_1

    .line 3131
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c()V

    .line 3134
    :cond_1
    return-void

    .line 3121
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3122
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    goto :goto_0

    .line 3123
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3124
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    goto :goto_0

    .line 3125
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3126
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    goto :goto_0
.end method

.method a(Lcom/sec/chaton/a/a/f;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2285
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_2

    .line 2286
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPostONList;

    .line 2287
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2288
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/PostONList;->hasmore:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aA:Ljava/lang/String;

    .line 2289
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/PostONList;->endtime:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aB:Ljava/lang/String;

    .line 2290
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->isblind:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    .line 2292
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b()V

    .line 2330
    :cond_1
    :goto_0
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    .line 2331
    return-void

    .line 2294
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->m()V

    .line 2296
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aq:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    .line 2297
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aq:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2300
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2302
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2303
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    if-eqz v0, :cond_4

    .line 2304
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/d;->notifyDataSetChanged()V

    .line 2307
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    .line 2308
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_6

    .line 2309
    const-string v0, "Buddy name is null"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2311
    :cond_6
    const v0, 0x7f0b00b4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    .line 2313
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0187

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2314
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ar:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2315
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->as:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2316
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->af:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2317
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ag:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 2326
    :cond_8
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    if-eqz v0, :cond_1

    .line 2327
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/d;->notifyDataSetChanged()V

    goto :goto_0

    .line 2318
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2319
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->as:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 2320
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 2321
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2322
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ar:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2323
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->as:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Lcom/sec/chaton/poston/k;)V
    .locals 5

    .prologue
    .line 2702
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2744
    :goto_0
    return-void

    .line 2706
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v2, Lcom/sec/chaton/poston/PostONDetailActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2707
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2708
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2709
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2710
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->f:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2711
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2712
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/chaton/poston/d;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2713
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->g:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2714
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->o:Ljava/lang/String;

    const-string v2, "BUDDY_PAGE"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2715
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->i:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2716
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->l:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2718
    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2719
    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/a/aa;

    .line 2720
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/poston/d;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2721
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_1

    .line 2722
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMultimediaDownload has geo tag, content.getUrl() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2724
    :cond_1
    sget-object v3, Lcom/sec/chaton/poston/PostONDetailFragment;->m:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 2727
    :cond_2
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_3

    .line 2728
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMultimediaDownload has Multimedia, content.getUrl() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / content.getMetaType() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2730
    :cond_3
    sget-object v3, Lcom/sec/chaton/poston/PostONDetailFragment;->j:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2731
    sget-object v3, Lcom/sec/chaton/poston/PostONDetailFragment;->k:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 2742
    :cond_4
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2743
    const/16 v0, 0xa

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3491
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/fi;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/sec/chaton/buddy/fi;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3493
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2748
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2775
    :goto_0
    return-void

    .line 2752
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2753
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2754
    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2755
    const-string v1, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2756
    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2757
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2760
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2761
    const-string v1, "BUDDY_DIALOG_BUDDY_NO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2762
    const-string v1, "BUDDY_DIALOG_BUDDY_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2772
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2773
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 623
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->n:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 624
    if-eqz p1, :cond_2

    .line 625
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->n:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 627
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 637
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 638
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 639
    if-eqz p1, :cond_3

    .line 640
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 650
    :cond_1
    :goto_1
    return-void

    .line 630
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->n:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 632
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 644
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->be:Landroid/widget/ImageButton;

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public a()Z
    .locals 7

    .prologue
    const v6, 0x7f0902b8

    const v5, 0x7f0902b7

    .line 2154
    const-string v0, "Buddy didn\'t set Coverstory "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2155
    invoke-static {}, Lcom/sec/chaton/e/a/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 2156
    if-eqz v0, :cond_1

    .line 2157
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/coverstory/random/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2158
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/coverstory/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2159
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->au:Landroid/widget/ImageView;

    invoke-static {v0, v3, v1, v2, v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;)Z

    move-result v1

    .line 2160
    if-nez v1, :cond_0

    .line 2163
    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ax:Ljava/lang/String;

    .line 2164
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2168
    :cond_0
    const/4 v0, 0x1

    .line 2178
    :goto_0
    return v0

    .line 2174
    :cond_1
    const-string v0, " Random ERROR !!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2177
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/i;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2178
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Landroid/net/Uri;
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 3563
    .line 3570
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[phoneCall] Original number : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3571
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 3572
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[phoneCall] After getPhoneDigitNumber : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3574
    const-string v0, "[phoneCall] Call Number Find Start"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3576
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 3577
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "number"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 3578
    if-eqz v6, :cond_2

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 3579
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3580
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 3581
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[phoneCall] ContactSaved id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3583
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "contact_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3585
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v0

    if-lez v0, :cond_2

    .line 3587
    :cond_0
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3588
    const-string v0, "data1"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3589
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3590
    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3591
    invoke-direct {p0, v8, v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3592
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v7

    .line 3598
    :cond_1
    if-eqz v1, :cond_2

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3599
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3606
    :cond_2
    if-eqz v7, :cond_3

    .line 3607
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[phoneCall] Call Number Found : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 3612
    :cond_3
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3613
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v0, v7

    .line 3617
    :goto_0
    if-nez v0, :cond_5

    .line 3618
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 3619
    const-string v1, "[phoneCall] mContactSaved is false"

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3622
    :cond_5
    return-object v0

    .line 3598
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_6

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_6

    .line 3599
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3598
    :cond_6
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 3609
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 3610
    :goto_1
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 3612
    if-eqz v1, :cond_7

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_7

    .line 3613
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    move-object v0, v7

    .line 3615
    goto :goto_0

    .line 3612
    :catchall_1
    move-exception v0

    move-object v6, v7

    :goto_2
    if-eqz v6, :cond_8

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_8

    .line 3613
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3612
    :cond_8
    throw v0

    :catchall_2
    move-exception v0

    goto :goto_2

    :catchall_3
    move-exception v0

    move-object v6, v1

    goto :goto_2

    .line 3609
    :catch_1
    move-exception v0

    move-object v1, v7

    goto :goto_1
.end method

.method b()V
    .locals 15

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2183
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    const-string v2, "true"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2184
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 2185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is blinded"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2188
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->af:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2189
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ag:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 2192
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2193
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    if-eqz v0, :cond_1

    .line 2194
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/d;->notifyDataSetChanged()V

    .line 2198
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aq:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 2199
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aq:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2201
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    .line 2203
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    .line 2204
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_4

    .line 2205
    const-string v0, "Buddy name is null"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2207
    :cond_4
    const v0, 0x7f0b00b4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    .line 2209
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0187

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2210
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ar:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2213
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->as:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2216
    iput-boolean v5, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    .line 2282
    :goto_0
    return-void

    .line 2218
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->af:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2219
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ag:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 2222
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2224
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/z;->b(Landroid/content/ContentResolver;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 2225
    :try_start_1
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_a

    .line 2227
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2229
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2230
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ar:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2231
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->as:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2233
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aq:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_7

    .line 2234
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aq:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2237
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    if-eqz v0, :cond_8

    .line 2238
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2239
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/d;->notifyDataSetChanged()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2269
    :cond_8
    if-eqz v13, :cond_9

    .line 2270
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 2273
    :cond_9
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    if-nez v0, :cond_d

    .line 2274
    new-instance v0, Lcom/sec/chaton/poston/d;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Y:Ljava/util/ArrayList;

    const v4, 0x7f03012a

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/poston/d;-><init>(Landroid/widget/ListView;Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    .line 2276
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/poston/d;->a(Lcom/sec/chaton/poston/j;)V

    .line 2277
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_0

    .line 2244
    :cond_a
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->as:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2246
    :goto_2
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2247
    const-string v0, "buddy_no"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2249
    const-string v0, "joined_name"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2250
    const-string v0, "poston"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2251
    const-string v0, "timestamp"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2252
    const-string v0, "unread_comment_count"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2253
    const-string v0, "read_comment_count"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2254
    const-string v0, "isread"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2255
    const-string v0, "multimedia_list"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2256
    const-string v1, "poston_id"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2257
    const-string v1, "joined_no"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 2259
    invoke-static {v0}, Lcom/sec/chaton/e/a/z;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 2261
    iget-object v14, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Y:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/poston/k;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v12}, Lcom/sec/chaton/poston/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    .line 2264
    :catch_0
    move-exception v0

    move-object v1, v13

    .line 2265
    :goto_3
    :try_start_3
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_b

    .line 2266
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2269
    :cond_b
    if-eqz v1, :cond_9

    .line 2270
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 2269
    :catchall_0
    move-exception v0

    move-object v13, v1

    :goto_4
    if-eqz v13, :cond_c

    .line 2270
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 2269
    :cond_c
    throw v0

    .line 2279
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/d;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 2269
    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v13, v1

    goto :goto_4

    .line 2264
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public b(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 3138
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->B:Z

    if-eqz v0, :cond_5

    .line 3139
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3140
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 3149
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aP:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aP:I

    .line 3150
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aP:I

    if-nez v0, :cond_1

    .line 3151
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c()V

    .line 3157
    :cond_1
    :goto_1
    return-void

    .line 3141
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3142
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    goto :goto_0

    .line 3143
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3144
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    goto :goto_0

    .line 3145
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3146
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    goto :goto_0

    .line 3155
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aZ:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3059
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->c()Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 3063
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3066
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->c()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 3067
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3070
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->c()Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 3071
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3074
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->c()Z

    move-result v0

    if-ne v0, v2, :cond_3

    .line 3075
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3078
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->P:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3079
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 4015
    iput-boolean v7, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aM:Z

    .line 4019
    :try_start_0
    const-string v0, "[contactSavedCheck] Contact Saved Find Start"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4021
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 4022
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "number"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 4023
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 4024
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4025
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aM:Z

    .line 4026
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aO:I

    .line 4027
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[contactSavedCheck] Found in Contact Number : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4028
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[contactSavedCheck] ContactSaved id : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aO:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4034
    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4035
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4039
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aM:Z

    if-nez v0, :cond_2

    .line 4040
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[contactSavedCheck] No PhoneNumber in Contact : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4041
    iput v7, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aO:I

    .line 4043
    :cond_2
    return-void

    .line 4031
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 4032
    :goto_1
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4034
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 4035
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4034
    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 4031
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public d()V
    .locals 6

    .prologue
    .line 3108
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->L:Lcom/sec/chaton/buddy/fl;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->M:Lcom/sec/chaton/buddy/fl;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->N:Lcom/sec/chaton/buddy/fl;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->O:Lcom/sec/chaton/buddy/fl;

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/f;->b(Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Ljava/lang/String;)V

    .line 3109
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c()V

    .line 3110
    return-void
.end method

.method public d(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 4063
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    const-string v3, "group_relation_buddy=?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object p1, v4, v7

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4065
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 4066
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4067
    const-string v0, "group_relation_group"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4068
    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v6

    .line 4074
    :goto_0
    if-eqz v1, :cond_1

    .line 4075
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4077
    :cond_1
    return v0

    :cond_2
    move v0, v7

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    .line 2839
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2842
    const/16 v0, 0x9

    if-ne p1, v0, :cond_1

    if-ne p2, v1, :cond_1

    .line 2843
    const-string v0, "Write PostON from Write page Success"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2845
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    .line 2846
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j:Z

    .line 2848
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "HAS_MORE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aA:Ljava/lang/String;

    .line 2849
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "END_TIME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aB:Ljava/lang/String;

    .line 2850
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "IS_BLIND"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    .line 2851
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b()V

    .line 2852
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    .line 2878
    :cond_0
    :goto_0
    return-void

    .line 2855
    :cond_1
    const/16 v0, 0xa

    if-ne p1, v0, :cond_2

    if-ne p2, v1, :cond_2

    .line 2857
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "IS_DELETED"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2858
    const-string v0, "Delete PostON from Detail page Success"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2860
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    .line 2861
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j:Z

    .line 2863
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "HAS_MORE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aA:Ljava/lang/String;

    .line 2864
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "END_TIME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aB:Ljava/lang/String;

    .line 2865
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "IS_BLIND"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aC:Ljava/lang/String;

    .line 2866
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b()V

    .line 2867
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aD:Z

    goto :goto_0

    .line 2871
    :cond_2
    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 2872
    const-string v0, "PROFILE_EDIT_BUDDY_NAME"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2873
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_RENAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2874
    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    .line 2875
    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->V:Ljava/lang/String;

    .line 2876
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->Q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 366
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 368
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    .line 370
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 535
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 541
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 675
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 677
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 679
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "?uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "param"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aH:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 691
    :cond_0
    :goto_0
    iput-object p0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->q:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    .line 692
    iput-object p0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->r:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    .line 693
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l:Lcom/sec/chaton/e/a/u;

    .line 694
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->p:Landroid/widget/Toast;

    .line 696
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 697
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    .line 698
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    .line 699
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_SUGGESTION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aK:Z

    .line 700
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_PROFILE_LOAD_DONE"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->z:Z

    .line 701
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_PROFILE_IMAGE_LOAD_DONE"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->A:Z

    .line 702
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_PROFILE_IMAGE_COUNT"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->C:I

    .line 703
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_FROM_BUDDYDIALOG"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->av:Z

    .line 713
    :cond_1
    :goto_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 714
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "*********** BUDDY NO : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 717
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 718
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "*********** BUDDY NAME : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 722
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "*********** BUDDY SUGGESTION : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aK:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 726
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "*********** BUDDY FROM BUDDY DIALOG : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->av:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 730
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 733
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 734
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    .line 737
    :cond_7
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->av:Z

    if-nez v0, :cond_8

    .line 738
    new-instance v0, Lcom/sec/chaton/d/i;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/i;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    .line 739
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;)V

    .line 742
    :cond_8
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 743
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v6, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 744
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v6, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 750
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->at:Landroid/view/inputmethod/InputMethodManager;

    .line 751
    return-void

    .line 680
    :catch_0
    move-exception v0

    .line 681
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 682
    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 704
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 705
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    .line 706
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->u:Ljava/lang/String;

    .line 707
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_SUGGESTION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aK:Z

    .line 708
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_PROFILE_LOAD_DONE"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->z:Z

    .line 709
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_PROFILE_IMAGE_LOAD_DONE"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->A:Z

    .line 710
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_FROM_BUDDYDIALOG"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->av:Z

    goto/16 :goto_1
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 6

    .prologue
    .line 2780
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 2782
    new-instance v1, Lcom/sec/chaton/b/a;

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-direct {v1, v0, p1}, Lcom/sec/chaton/b/a;-><init>(Landroid/content/Context;Landroid/view/ContextMenu;)V

    .line 2784
    instance-of v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    if-nez v0, :cond_1

    .line 2835
    :cond_0
    :goto_0
    return-void

    .line 2788
    :cond_1
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 2790
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget v2, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    .line 2791
    if-eqz v0, :cond_0

    .line 2794
    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->c()Ljava/lang/String;

    move-result-object v2

    .line 2795
    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->f()Ljava/lang/String;

    .line 2796
    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->a()Ljava/lang/String;

    move-result-object v0

    .line 2799
    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aa:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2800
    const v2, 0x7f0b0094

    invoke-virtual {v1, v2}, Lcom/sec/chaton/b/a;->a(I)Lcom/sec/chaton/b/a;

    .line 2801
    const/4 v2, 0x0

    const/16 v3, 0x7d0

    const/4 v4, 0x1

    const v5, 0x7f0b0138

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/chaton/b/a;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/fd;

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/buddy/fd;-><init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 545
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 546
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->m:Landroid/view/Menu;

    .line 550
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 551
    const v0, 0x7f0f0014

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 553
    const v0, 0x7f070597

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->n:Landroid/view/MenuItem;

    .line 555
    const v0, 0x7f070598

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o:Landroid/view/MenuItem;

    .line 557
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x:Z

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Z)V

    .line 560
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 654
    const-string v0, "onCreateView()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    invoke-virtual {p0, v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->setHasOptionsMenu(Z)V

    .line 658
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    const v1, 0x7f0b019d

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 660
    const v0, 0x7f030070

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ab:Landroid/view/View;

    .line 661
    const v0, 0x7f03013b

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ac:Landroid/view/View;

    .line 663
    const v0, 0x7f030071

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ae:Landroid/view/View;

    .line 665
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    .line 666
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f()V

    .line 668
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x3

    invoke-static {}, Lcom/sec/chaton/e/i;->c()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "buddy_no = ? "

    new-array v6, v4, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->ae:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bg:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bg:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 459
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->bg:Landroid/os/Handler;

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    if-eqz v0, :cond_1

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->c()V

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->g()V

    .line 465
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->i()V

    .line 466
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aw:Lcom/sec/chaton/d/i;

    .line 470
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l:Lcom/sec/chaton/e/a/u;

    if-eqz v0, :cond_2

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l:Lcom/sec/chaton/e/a/u;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/a/u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 474
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    if-eqz v0, :cond_3

    .line 475
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    invoke-virtual {v0, v2}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aG:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 479
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->au:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 480
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->au:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 481
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->au:Landroid/widget/ImageView;

    .line 484
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 486
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D:Landroid/widget/ImageView;

    .line 489
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->E:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 490
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->E:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 491
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->E:Landroid/widget/ImageView;

    .line 494
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->F:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    .line 495
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->F:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 496
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->F:Landroid/widget/ImageView;

    .line 499
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->G:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 500
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->G:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 501
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->G:Landroid/widget/ImageView;

    .line 504
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->P:Landroid/widget/ImageButton;

    if-eqz v0, :cond_9

    .line 505
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->P:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 506
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->P:Landroid/widget/ImageButton;

    .line 509
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aE:Lcom/sec/chaton/d/w;

    if-eqz v0, :cond_a

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aE:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->f()V

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aE:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->b()V

    .line 512
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aE:Lcom/sec/chaton/d/w;

    .line 515
    :cond_a
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 516
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h:Landroid/database/ContentObserver;

    if-eqz v0, :cond_b

    .line 517
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 519
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i:Landroid/database/ContentObserver;

    if-eqz v0, :cond_c

    .line 520
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 524
    :cond_c
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 526
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->m()V

    .line 528
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->q:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    .line 529
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->r:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    .line 530
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 374
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 376
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->aR:Landroid/app/Activity;

    .line 377
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    .line 589
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 590
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 619
    :cond_0
    :goto_0
    return v8

    .line 594
    :pswitch_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v8, :cond_1

    .line 595
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00040002"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 598
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x:Z

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    sget-object v3, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    const-string v4, "group_relation_group = 1 AND group_relation_buddy = ? "

    new-array v5, v8, [Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->t:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 600
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x:Z

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Z)V

    .line 601
    iput-boolean v7, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x:Z

    goto :goto_0

    .line 609
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x:Z

    if-nez v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->s:Lcom/sec/chaton/buddy/a/c;

    sget-object v3, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    const-string v5, "group_relation_group = 1 "

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x:Z

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Z)V

    .line 612
    iput-boolean v8, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->x:Z

    goto :goto_0

    .line 590
    nop

    :pswitch_data_0
    .packed-switch 0x7f070597
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 443
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 452
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 564
    const-string v0, "onPrepareOptionsMenu()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    return-void
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 412
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 415
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 416
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j:Z

    if-eqz v0, :cond_2

    .line 417
    iput-boolean v8, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j:Z

    .line 422
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->n()V

    .line 424
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->y:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->v:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 425
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->v:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x6

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    iput-boolean v8, p0, Lcom/sec/chaton/buddy/BuddyProfileFragment;->y:Z

    .line 434
    :cond_1
    return-void

    .line 419
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e()V

    goto :goto_0
.end method
