.class Lcom/sec/chaton/buddy/fn;
.super Ljava/lang/Object;
.source "BuddyProfileImageViewFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x258

    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mProfileListView Position: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    .line 302
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mProfileListView view: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    iget-object v2, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 308
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->setHasOptionsMenu(Z)V

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    add-int/lit8 v2, p3, 0x1

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;I)I

    .line 311
    iget-object v0, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a:Ljava/lang/String;

    .line 312
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "profile_f_mine_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 316
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    const v5, 0x7f0b017f

    invoke-virtual {v4, v5}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 318
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->g(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/common/f/c;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    iget-object v2, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;Landroid/view/View;)V

    .line 330
    if-nez p3, :cond_4

    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    iget-object v2, v2, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 332
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 333
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 335
    if-eqz v0, :cond_4

    .line 336
    iget-object v1, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 337
    const/4 v0, 0x1

    .line 342
    :goto_0
    if-nez v0, :cond_1

    .line 343
    new-instance v0, Ljava/io/File;

    iget-object v1, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    iget-object v2, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 345
    iget-object v1, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 359
    :cond_1
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BuddyProfileImageView: dirCachePath+FullfileName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    return-void

    .line 347
    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v1, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    iget-object v2, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->ThumbfileName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 349
    iget-object v1, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 352
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    new-instance v1, Lcom/sec/common/f/c;

    invoke-direct {v1}, Lcom/sec/common/f/c;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Lcom/sec/common/f/c;)Lcom/sec/common/f/c;

    .line 354
    new-instance v0, Lcom/sec/chaton/userprofile/bq;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->id:Ljava/lang/String;

    iget-object v5, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    iget-object v6, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/userprofile/bq;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 355
    iget-object v1, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->g(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/buddy/fn;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    iget-object v1, v7, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method
