.class Lcom/sec/chaton/buddy/dialog/u;
.super Landroid/os/Handler;
.source "MeDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/MeDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/MeDialog;)V
    .locals 0

    .prologue
    .line 519
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 522
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 814
    :cond_0
    :goto_0
    return-void

    .line 526
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 548
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 549
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v2, v3, :cond_0

    .line 551
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetProfileImageList;

    .line 552
    if-eqz v0, :cond_0

    .line 556
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->profileimagelist:Ljava/util/ArrayList;

    invoke-static {v2, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 557
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 558
    :cond_2
    const-string v0, "profile_image_status"

    const-string v2, "deleted"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;Z)Z

    .line 560
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;I)V

    .line 561
    const-string v0, "profile_small_image0"

    invoke-static {v0, v7}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 529
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 530
    if-eqz v0, :cond_0

    .line 534
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_0

    .line 535
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BuddyProfile;

    .line 541
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;Lcom/sec/chaton/io/entry/inner/BuddyProfile;)V

    goto :goto_0

    .line 564
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;Z)Z

    .line 566
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x4

    if-le v0, v2, :cond_b

    .line 567
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->b(Lcom/sec/chaton/buddy/dialog/MeDialog;I)I

    .line 568
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->c(Lcom/sec/chaton/buddy/dialog/MeDialog;I)I

    .line 569
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->b(Lcom/sec/chaton/buddy/dialog/MeDialog;)Landroid/widget/ImageButton;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 570
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->b(Lcom/sec/chaton/buddy/dialog/MeDialog;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 577
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 578
    const-string v2, "1"

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 579
    const-string v0, "profile_image_status"

    const-string v2, "updated"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x4

    if-ge v0, v2, :cond_5

    .line 581
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v6}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;I)V

    .line 582
    const-string v0, "profile_small_image3"

    invoke-static {v0, v7}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v6, :cond_6

    .line 585
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;I)V

    .line 586
    const-string v0, "profile_small_image2"

    invoke-static {v0, v7}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v4, :cond_7

    .line 589
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;I)V

    .line 590
    const-string v0, "profile_small_image1"

    invoke-static {v0, v7}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    :goto_2
    move v6, v1

    .line 621
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->c(Lcom/sec/chaton/buddy/dialog/MeDialog;)I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    .line 623
    const-string v0, "1"

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 625
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/MeDialog;->d(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 626
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_8

    .line 627
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 631
    :cond_8
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "profile_image_update_client"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 633
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/MeDialog;->d(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "profile_t_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "mine_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 634
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v4}, Lcom/sec/chaton/buddy/dialog/MeDialog;->d(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "profile_f_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mine_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 636
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/MeDialog;->e(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "myprofile.png_"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 637
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v8}, Lcom/sec/chaton/buddy/dialog/MeDialog;->e(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v8}, Lcom/sec/chaton/buddy/dialog/MeDialog;->f(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpeg_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 639
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 640
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 641
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_9

    .line 642
    invoke-static {v7, v0}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    :cond_9
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 646
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 647
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_a

    .line 648
    invoke-static {v4, v3}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    const-string v3, "profile_small_image0"

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v4}, Lcom/sec/chaton/buddy/dialog/MeDialog;->g(Lcom/sec/chaton/buddy/dialog/MeDialog;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V

    .line 621
    :goto_4
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_3

    .line 573
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->c(Lcom/sec/chaton/buddy/dialog/MeDialog;I)I

    goto/16 :goto_1

    .line 594
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;I)V

    .line 595
    const-string v0, "profile_image_status"

    const-string v2, "deleted"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    const-string v0, "profile_small_image0"

    invoke-static {v0, v7}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v6, :cond_d

    .line 598
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v6}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;I)V

    .line 599
    const-string v0, "profile_small_image3"

    invoke-static {v0, v7}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v4, :cond_e

    .line 602
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;I)V

    .line 603
    const-string v0, "profile_small_image2"

    invoke-static {v0, v7}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v5, :cond_7

    .line 606
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;I)V

    .line 607
    const-string v0, "profile_small_image1"

    invoke-static {v0, v7}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 659
    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profileimageurl: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    const-string v3, "1"

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 663
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v6, v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;ILcom/sec/chaton/io/entry/inner/ProfileImage;)V

    goto/16 :goto_4

    .line 666
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    add-int/lit8 v3, v6, 0x1

    invoke-static {v0, v3, v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;ILcom/sec/chaton/io/entry/inner/ProfileImage;)V

    goto/16 :goto_4

    .line 678
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 680
    const-string v1, "CoverStoryControl.METHOD_GET_COVERSTORY"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    if-nez v0, :cond_11

    .line 682
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY (httpEntry == null)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 686
    :cond_11
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_18

    .line 688
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/sec/chaton/io/entry/inner/CoverStory;

    .line 689
    if-eqz v8, :cond_0

    .line 693
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "coverstory_metaid"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 694
    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 696
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 697
    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 698
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->e(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mycoverstory."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 699
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_12

    .line 700
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY Same coverstory~ PASS!!!"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 703
    :cond_12
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY Same metaid but there is no coverstoryfile~!!!"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    :cond_13
    iget-object v0, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 712
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Ljava/lang/String;)Z

    move-result v0

    .line 713
    if-nez v0, :cond_14

    .line 714
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->h(Lcom/sec/chaton/buddy/dialog/MeDialog;)Lcom/sec/chaton/d/i;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0902b7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0902b8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 724
    const-string v0, "coverstory_contentid"

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    :goto_5
    const-string v0, "mypage_coverstory_state"

    const-string v1, "updated"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    const-string v0, "coverstory_metaid"

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 727
    :cond_15
    new-instance v0, Lcom/sec/chaton/poston/a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->host:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metacontents:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->f(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Buddy"

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->e(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "mycoverstory.jpg"

    iget-object v7, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->j(Lcom/sec/chaton/buddy/dialog/MeDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->i(Lcom/sec/chaton/buddy/dialog/MeDialog;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_5

    .line 736
    :cond_16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 737
    const-string v0, "previousMetaid is null. when setting coverstory first, pass ~!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 739
    :cond_17
    const-string v0, "Set RandomCoverStory as randomId coverStoryInfo.metaid is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 745
    :cond_18
    const-string v1, "CoverStoryControl.METHOD_GET_COVERSTORY httpEntry fail"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_0

    .line 748
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY HttpResultCode.NO_CONTENT"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 759
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 763
    if-nez v0, :cond_19

    .line 764
    const-string v0, "MeDialog CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD (httpEntry == null)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 767
    :cond_19
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1a

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_1a

    .line 768
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;

    .line 769
    const-string v6, "mycoverstory.jpg"

    .line 770
    const-string v0, "coverstory_file_name"

    invoke-static {v0, v6}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    const-string v0, "mypage_coverstory_state"

    const-string v2, "updated"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    new-instance v0, Lcom/sec/chaton/poston/a;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;->fileurl:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->f(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->e(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/MeDialog;->k(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->j(Lcom/sec/chaton/buddy/dialog/MeDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->i(Lcom/sec/chaton/buddy/dialog/MeDialog;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 777
    :cond_1a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_0

    .line 780
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Deleted item on MeDialog mContentId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->k(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->k(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/h;->b(Ljava/lang/String;)V

    .line 782
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->k(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 784
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v0, v7}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/buddy/dialog/MeDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 786
    :cond_1b
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a()Z

    goto/16 :goto_0

    .line 792
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 796
    if-nez v0, :cond_1c

    .line 797
    const-string v0, "CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD_RANDOM (httpEntry == null)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 800
    :cond_1c
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_0

    .line 801
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;

    .line 802
    const-string v6, "mycoverstory.jpg"

    .line 803
    const-string v0, "coverstory_file_name"

    invoke-static {v0, v6}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    new-instance v0, Lcom/sec/chaton/poston/a;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;->fileurl:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->f(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->e(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->j(Lcom/sec/chaton/buddy/dialog/MeDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/u;->a:Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->i(Lcom/sec/chaton/buddy/dialog/MeDialog;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 526
    nop

    :sswitch_data_0
    .sparse-switch
        0x197 -> :sswitch_0
        0x19b -> :sswitch_1
        0xbb9 -> :sswitch_2
        0xbbd -> :sswitch_3
        0xbbe -> :sswitch_4
    .end sparse-switch
.end method
