.class Lcom/sec/chaton/buddy/dialog/al;
.super Ljava/lang/Object;
.source "SpecialBuddyDialog.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V
    .locals 0

    .prologue
    .line 633
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 718
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 708
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 15

    .prologue
    .line 637
    packed-switch p1, :pswitch_data_0

    .line 703
    :goto_0
    :pswitch_0
    return-void

    .line 640
    :pswitch_1
    if-eqz p3, :cond_3

    :try_start_0
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 641
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 642
    iget-object v14, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    const-string v1, "buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "description"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "followcount"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "likecount"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "msgstatus"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "photoloaded"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "status"

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "url"

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "weburl"

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "islike"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "tel"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "usertype"

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {v1 .. v13}, Lcom/sec/chaton/buddy/a/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    invoke-static {v14, v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/d;

    .line 656
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    const-string v1, "islike"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "Y"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-static {v2, v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 665
    :cond_0
    :goto_2
    if-eqz p3, :cond_1

    .line 666
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 669
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)V

    goto/16 :goto_0

    .line 656
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 661
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z

    .line 662
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 665
    :catchall_0
    move-exception v1

    if-eqz p3, :cond_4

    .line 666
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 665
    :cond_4
    throw v1

    .line 673
    :pswitch_2
    if-eqz p3, :cond_7

    :try_start_2
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_7

    .line 674
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 675
    iget-object v13, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    const-string v1, "buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "description"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "followcount"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "likecount"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "msgstatus"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "photoloaded"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "status"

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "url"

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "weburl"

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "tel"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "usertype"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {v1 .. v12}, Lcom/sec/chaton/buddy/a/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    invoke-static {v13, v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/d;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 687
    :cond_5
    :goto_3
    if-eqz p3, :cond_6

    .line 688
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 691
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)V

    goto/16 :goto_0

    .line 684
    :cond_7
    :try_start_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/d;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    .line 687
    :catchall_1
    move-exception v1

    if-eqz p3, :cond_8

    .line 688
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 687
    :cond_8
    throw v1

    .line 694
    :pswitch_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/al;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)V

    goto/16 :goto_0

    .line 637
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 713
    return-void
.end method
