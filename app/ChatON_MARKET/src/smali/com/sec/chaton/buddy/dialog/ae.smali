.class Lcom/sec/chaton/buddy/dialog/ae;
.super Ljava/lang/Object;
.source "SpecialBuddyDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/util/ba;

.field final synthetic b:Landroid/graphics/Bitmap;

.field final synthetic c:Lcom/sec/chaton/buddy/dialog/ad;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/ad;Lcom/sec/chaton/util/ba;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/ae;->c:Lcom/sec/chaton/buddy/dialog/ad;

    iput-object p2, p0, Lcom/sec/chaton/buddy/dialog/ae;->a:Lcom/sec/chaton/util/ba;

    iput-object p3, p0, Lcom/sec/chaton/buddy/dialog/ae;->b:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ae;->a:Lcom/sec/chaton/util/ba;

    sget-object v1, Lcom/sec/chaton/util/ba;->a:Lcom/sec/chaton/util/ba;

    if-ne v0, v1, :cond_2

    .line 278
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ae;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ae;->c:Lcom/sec/chaton/buddy/dialog/ad;

    iget-object v0, v0, Lcom/sec/chaton/buddy/dialog/ad;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ae;->b:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ae;->c:Lcom/sec/chaton/buddy/dialog/ad;

    iget-object v0, v0, Lcom/sec/chaton/buddy/dialog/ad;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ae;->c:Lcom/sec/chaton/buddy/dialog/ad;

    iget-object v0, v0, Lcom/sec/chaton/buddy/dialog/ad;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ae;->c:Lcom/sec/chaton/buddy/dialog/ad;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/ad;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 294
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ae;->c:Lcom/sec/chaton/buddy/dialog/ad;

    iget-object v0, v0, Lcom/sec/chaton/buddy/dialog/ad;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    .line 295
    return-void

    .line 286
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ae;->c:Lcom/sec/chaton/buddy/dialog/ad;

    iget-object v0, v0, Lcom/sec/chaton/buddy/dialog/ad;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b2

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 289
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ae;->a:Lcom/sec/chaton/util/ba;

    sget-object v1, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    if-ne v0, v1, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ae;->c:Lcom/sec/chaton/buddy/dialog/ad;

    iget-object v0, v0, Lcom/sec/chaton/buddy/dialog/ad;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
