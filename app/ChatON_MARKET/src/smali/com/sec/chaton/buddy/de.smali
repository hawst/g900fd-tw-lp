.class Lcom/sec/chaton/buddy/de;
.super Ljava/lang/Object;
.source "BuddyGroupDialog.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dc;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dc;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/sec/chaton/buddy/de;->a:Lcom/sec/chaton/buddy/dc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 239
    const/4 v2, 0x6

    if-ne p2, v2, :cond_2

    .line 240
    iget-object v2, p0, Lcom/sec/chaton/buddy/de;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dc;->b(Lcom/sec/chaton/buddy/dc;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 241
    iget-object v1, p0, Lcom/sec/chaton/buddy/de;->a:Lcom/sec/chaton/buddy/dc;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dc;->dismiss()V

    .line 250
    :goto_0
    sput-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 254
    :goto_1
    return v0

    .line 243
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/buddy/de;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dc;->c(Lcom/sec/chaton/buddy/dc;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x1e

    if-le v2, v3, :cond_1

    .line 244
    iget-object v2, p0, Lcom/sec/chaton/buddy/de;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dc;->a(Lcom/sec/chaton/buddy/dc;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0031

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 246
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/de;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dc;->d(Lcom/sec/chaton/buddy/dc;)V

    .line 247
    iget-object v1, p0, Lcom/sec/chaton/buddy/de;->a:Lcom/sec/chaton/buddy/dc;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dc;->dismiss()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 254
    goto :goto_1
.end method
