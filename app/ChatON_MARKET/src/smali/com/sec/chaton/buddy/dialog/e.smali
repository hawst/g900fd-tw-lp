.class Lcom/sec/chaton/buddy/dialog/e;
.super Landroid/os/Handler;
.source "BuddyDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V
    .locals 0

    .prologue
    .line 2799
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, -0x1

    const/4 v4, 0x1

    const/16 v3, 0x8

    const/4 v5, 0x0

    .line 2803
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 2805
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 2952
    :cond_0
    :goto_0
    return-void

    .line 2809
    :sswitch_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    .line 2810
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_2

    .line 2811
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->y(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2812
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    new-instance v1, Lcom/sec/chaton/buddy/a/c;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v3, v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Lcom/sec/chaton/buddy/a/c;)Lcom/sec/chaton/buddy/a/c;

    .line 2814
    :cond_1
    new-instance v0, Lcom/sec/chaton/e/b/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->j:Lcom/sec/chaton/e/b/d;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->y(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-direct {v0, v1, v2, v7, v5}, Lcom/sec/chaton/e/b/h;-><init>(Lcom/sec/chaton/e/b/d;Lcom/sec/chaton/buddy/a/c;IZ)V

    .line 2815
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    invoke-static {v1, v7, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    goto :goto_0

    .line 2817
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2824
    :sswitch_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    .line 2826
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2827
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "************** RESULT CODE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ADD BUDDY"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2828
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_3

    .line 2829
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b027d

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v4, v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2838
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0, v6}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->setResult(I)V

    .line 2839
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 2842
    :cond_3
    const-string v1, ""

    .line 2843
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*********** ADD BUDDY RESULT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2844
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*********** ADD BUDDY RESULT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2845
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e83

    if-ne v1, v2, :cond_4

    .line 2846
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2857
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2859
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->H(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2860
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2862
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->J(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2863
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->K(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2864
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2865
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2866
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2867
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->P(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2868
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2869
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 2847
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e84

    if-ne v1, v2, :cond_5

    .line 2848
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2849
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v1, 0x3e85

    if-ne v0, v1, :cond_6

    .line 2850
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2852
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v0, v0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2853
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    .line 2855
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2873
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->H(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2874
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2876
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->J(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2877
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->K(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2878
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2879
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2880
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2881
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2883
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 2884
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->P(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 2886
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2894
    :sswitch_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    .line 2895
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_9

    .line 2896
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "****** UNBLOCK : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2897
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0141

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v4, v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2899
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 2901
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2909
    :sswitch_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    .line 2910
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "****** IGNORE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2911
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_a

    .line 2912
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b020c

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v4, v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2913
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0, v6}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->setResult(I)V

    .line 2914
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 2916
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2917
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->V(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    goto/16 :goto_0

    .line 2924
    :sswitch_4
    if-eqz v0, :cond_0

    .line 2930
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_0

    .line 2932
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/FlagImageURLEntry;

    .line 2933
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/io/entry/FlagImageURLEntry;->fileurl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2934
    new-instance v1, Lcom/sec/chaton/buddy/fk;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/FlagImageURLEntry;->fileurl:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Z(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aa(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".png"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/sec/chaton/buddy/fk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2935
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/common/f/c;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/e;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ab(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 2938
    :catch_0
    move-exception v0

    .line 2939
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 2943
    :catchall_0
    move-exception v0

    throw v0

    .line 2940
    :catch_1
    move-exception v0

    .line 2941
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 2805
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_3
        0x12f -> :sswitch_1
        0x144 -> :sswitch_4
        0x25a -> :sswitch_0
        0x25b -> :sswitch_2
    .end sparse-switch
.end method
