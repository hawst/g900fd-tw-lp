.class Lcom/sec/chaton/buddy/ib;
.super Ljava/lang/Object;
.source "SpecialBuddyFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/a/d;

.field final synthetic b:Lcom/sec/chaton/buddy/SpecialBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;)V
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Lcom/sec/chaton/buddy/ib;->b:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iput-object p2, p0, Lcom/sec/chaton/buddy/ib;->a:Lcom/sec/chaton/buddy/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 596
    iget-object v0, p0, Lcom/sec/chaton/buddy/ib;->a:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/d;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 597
    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 598
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 600
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 601
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 602
    iget-object v0, p0, Lcom/sec/chaton/buddy/ib;->b:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->startActivity(Landroid/content/Intent;)V

    .line 603
    return-void
.end method
