.class public Lcom/sec/chaton/buddy/ag;
.super Landroid/widget/BaseExpandableListAdapter;
.source "BuddyAdapter.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# static fields
.field public static a:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public static b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/util/av;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private C:I

.field private D:I

.field private E:Z

.field private F:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

.field private G:Landroid/view/View$OnClickListener;

.field c:Landroid/database/ContentObserver;

.field private d:Lcom/sec/widget/FastScrollableExpandableListView;

.field private e:Landroid/view/LayoutInflater;

.field private f:Landroid/content/Context;

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:I

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:[Ljava/lang/String;

.field private o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Lcom/sec/chaton/buddy/aq;

.field private s:Lcom/sec/chaton/buddy/ao;

.field private t:Lcom/sec/chaton/buddy/ap;

.field private u:Lcom/sec/chaton/buddy/BuddyFragment;

.field private v:Landroid/graphics/Bitmap;

.field private w:I

.field private final x:I

.field private y:I

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/widget/FastScrollableExpandableListView;Landroid/content/Context;Ljava/util/ArrayList;ILjava/util/ArrayList;IZZLcom/sec/chaton/buddy/aq;ZLcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/widget/FastScrollableExpandableListView;",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/a;",
            ">;I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;>;IZZ",
            "Lcom/sec/chaton/buddy/aq;",
            "Z",
            "Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 161
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 91
    iput-object v1, p0, Lcom/sec/chaton/buddy/ag;->p:Ljava/lang/String;

    .line 92
    iput-object v1, p0, Lcom/sec/chaton/buddy/ag;->q:Ljava/lang/String;

    .line 104
    iput-object v1, p0, Lcom/sec/chaton/buddy/ag;->v:Landroid/graphics/Bitmap;

    .line 106
    iput v0, p0, Lcom/sec/chaton/buddy/ag;->w:I

    .line 110
    iput v3, p0, Lcom/sec/chaton/buddy/ag;->x:I

    .line 127
    iput v0, p0, Lcom/sec/chaton/buddy/ag;->C:I

    .line 128
    iput v0, p0, Lcom/sec/chaton/buddy/ag;->D:I

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/ag;->E:Z

    .line 1053
    new-instance v0, Lcom/sec/chaton/buddy/al;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/al;-><init>(Lcom/sec/chaton/buddy/ag;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ag;->G:Landroid/view/View$OnClickListener;

    .line 1563
    new-instance v0, Lcom/sec/chaton/buddy/an;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/an;-><init>(Lcom/sec/chaton/buddy/ag;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ag;->c:Landroid/database/ContentObserver;

    .line 163
    iput-object p1, p0, Lcom/sec/chaton/buddy/ag;->d:Lcom/sec/widget/FastScrollableExpandableListView;

    .line 164
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ag;->e:Landroid/view/LayoutInflater;

    .line 165
    iput-object p2, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    .line 166
    iput-object p3, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    .line 167
    iput p4, p0, Lcom/sec/chaton/buddy/ag;->h:I

    .line 168
    iput-object p5, p0, Lcom/sec/chaton/buddy/ag;->i:Ljava/util/ArrayList;

    .line 169
    iput p6, p0, Lcom/sec/chaton/buddy/ag;->j:I

    .line 170
    iput-boolean p7, p0, Lcom/sec/chaton/buddy/ag;->k:Z

    .line 171
    iput-boolean p8, p0, Lcom/sec/chaton/buddy/ag;->l:Z

    .line 172
    iput-boolean p10, p0, Lcom/sec/chaton/buddy/ag;->m:Z

    .line 174
    iput-object p9, p0, Lcom/sec/chaton/buddy/ag;->r:Lcom/sec/chaton/buddy/aq;

    .line 177
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/ag;->z:Ljava/lang/String;

    .line 178
    iput-object p11, p0, Lcom/sec/chaton/buddy/ag;->F:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    .line 180
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/ag;->d()V

    .line 181
    if-eqz p2, :cond_0

    .line 182
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/buddy/ag;->c:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 184
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 229
    sget-object v0, Lcom/sec/chaton/buddy/ag;->a:Landroid/support/v4/util/LruCache;

    if-eqz v0, :cond_0

    .line 230
    sget-object v0, Lcom/sec/chaton/buddy/ag;->a:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 232
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/content/Context;Landroid/widget/CheckBox;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    invoke-static/range {p0 .. p5}, Lcom/sec/chaton/buddy/ag;->b(Landroid/content/Context;Landroid/widget/CheckBox;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    invoke-static {p0, p1, p2, p3}, Lcom/sec/chaton/buddy/ag;->b(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/ag;)Lcom/sec/chaton/buddy/ap;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->t:Lcom/sec/chaton/buddy/ap;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 217
    sget-object v0, Lcom/sec/chaton/buddy/ag;->a:Landroid/support/v4/util/LruCache;

    if-eqz v0, :cond_0

    .line 218
    sget-object v0, Lcom/sec/chaton/buddy/ag;->a:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    :cond_0
    return-void
.end method

.method private static b(Landroid/content/Context;Landroid/widget/CheckBox;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, -0x2

    .line 1293
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 1294
    const/high16 v1, 0x42300000    # 44.0f

    .line 1295
    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 1297
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1300
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1301
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1303
    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1307
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1309
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1317
    invoke-virtual {p1, v1}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1318
    invoke-virtual {p1, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1322
    invoke-virtual {p5, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1323
    const v1, 0x7f0202cf

    invoke-virtual {p5, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1324
    const v1, 0x7f020020

    invoke-virtual {p5, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1326
    new-instance v1, Lcom/sec/chaton/buddy/am;

    invoke-direct {v1}, Lcom/sec/chaton/buddy/am;-><init>()V

    invoke-virtual {p5, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1336
    invoke-virtual {p5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1362
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1363
    invoke-virtual {v0, p5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1365
    return-object v0
.end method

.method private static b(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, -0x2

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1369
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1371
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1372
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1375
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1376
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 1377
    const/high16 v3, 0x41600000    # 14.0f

    .line 1378
    mul-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 1379
    invoke-virtual {v1, v4, v4, v2, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1383
    :cond_0
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1384
    const/16 v3, 0x15

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1386
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1387
    const v3, 0x7f0200ef

    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1388
    invoke-virtual {p1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1390
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391
    const v3, 0x7f0200ed

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1392
    invoke-virtual {p2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1394
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1395
    const v0, 0x7f0200e7

    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1396
    invoke-virtual {p3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1398
    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1399
    invoke-virtual {v2, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1400
    invoke-virtual {v2, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1402
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1404
    return-object v1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/ag;)Lcom/sec/chaton/buddy/ao;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->s:Lcom/sec/chaton/buddy/ao;

    return-object v0
.end method

.method public static b(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 223
    invoke-static {p0}, Lcom/sec/chaton/buddy/ag;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/chaton/buddy/ag;->a:Landroid/support/v4/util/LruCache;

    if-eqz v0, :cond_0

    .line 224
    sget-object v0, Lcom/sec/chaton/buddy/ag;->a:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/ag;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    return-object v0
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 1015
    const/4 v0, 0x0

    .line 1016
    new-instance v1, Lcom/coolots/sso/a/a;

    invoke-direct {v1}, Lcom/coolots/sso/a/a;-><init>()V

    .line 1018
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1023
    :goto_0
    return v0

    .line 1019
    :catch_0
    move-exception v1

    .line 1020
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1541
    sget-object v0, Lcom/sec/chaton/buddy/ag;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 1542
    sget-object v0, Lcom/sec/chaton/buddy/ag;->b:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1544
    :cond_0
    return-void
.end method

.method public static g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1547
    sget-object v0, Lcom/sec/chaton/buddy/ag;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 1548
    sget-object v0, Lcom/sec/chaton/buddy/ag;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1550
    :cond_0
    return-void
.end method

.method private h(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0094

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/chaton/buddy/ag;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1009
    const/4 v0, 0x1

    .line 1011
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(F)I
    .locals 2

    .prologue
    .line 1572
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 1573
    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 1574
    return v0
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1027
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->d:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_0

    .line 1028
    iput-object v2, p0, Lcom/sec/chaton/buddy/ag;->d:Lcom/sec/widget/FastScrollableExpandableListView;

    .line 1030
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->c:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1031
    iput-object v2, p0, Lcom/sec/chaton/buddy/ag;->F:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    .line 1032
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    .line 244
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 1471
    iput p1, p0, Lcom/sec/chaton/buddy/ag;->C:I

    .line 1472
    iput p2, p0, Lcom/sec/chaton/buddy/ag;->D:I

    .line 1473
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->d:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->invalidateViews()V

    .line 1474
    return-void
.end method

.method protected a(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/chaton/buddy/ag;->u:Lcom/sec/chaton/buddy/BuddyFragment;

    .line 238
    return-void
.end method

.method public a(Lcom/sec/chaton/buddy/ao;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/chaton/buddy/ag;->s:Lcom/sec/chaton/buddy/ao;

    .line 150
    return-void
.end method

.method public a(Lcom/sec/chaton/buddy/ap;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/chaton/buddy/ag;->t:Lcom/sec/chaton/buddy/ap;

    .line 154
    return-void
.end method

.method public a(Lcom/sec/chaton/buddy/aq;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/chaton/buddy/ag;->r:Lcom/sec/chaton/buddy/aq;

    .line 158
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 251
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/ag;->l:Z

    .line 252
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1433
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ag;->o:Ljava/util/HashMap;

    move v1, v2

    .line 1435
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1436
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 1437
    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->o:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1435
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1443
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ag;->n:[Ljava/lang/String;

    move v1, v2

    .line 1444
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 1445
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    const/4 v3, 0x4

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    const/4 v3, 0x7

    if-ne v0, v3, :cond_3

    .line 1446
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->n:[Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v0, v1

    .line 1444
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1447
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    if-ne v0, v5, :cond_4

    .line 1448
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->n:[Ljava/lang/String;

    const-string v3, "\u2605"

    aput-object v3, v0, v1

    goto :goto_2

    .line 1449
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    const/4 v3, -0x2

    if-ne v0, v3, :cond_5

    .line 1450
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->n:[Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v0, v1

    goto :goto_2

    .line 1451
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    const/4 v3, 0x6

    if-ne v0, v3, :cond_6

    .line 1452
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->n:[Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v0, v1

    goto :goto_2

    .line 1454
    :cond_6
    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->n:[Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    goto :goto_2

    .line 1458
    :cond_7
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/chaton/buddy/ag;->z:Ljava/lang/String;

    .line 248
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 1558
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/ag;->E:Z

    .line 1559
    return-void
.end method

.method public c()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1517
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1035
    sget-object v0, Lcom/sec/chaton/buddy/ag;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1521
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/buddy/ag;->b:Ljava/util/HashMap;

    .line 1524
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "islike = \'Y\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1525
    if-eqz v0, :cond_1

    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1526
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1528
    :cond_0
    sget-object v1, Lcom/sec/chaton/buddy/ag;->b:Ljava/util/HashMap;

    const-string v2, "buddy_no"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1529
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-nez v1, :cond_0

    .line 1534
    :cond_1
    if-eqz v0, :cond_2

    .line 1535
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1538
    :cond_2
    return-void

    .line 1534
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_3

    .line 1535
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1534
    :cond_3
    throw v0

    .line 1531
    :catch_0
    move-exception v0

    move-object v0, v6

    .line 1534
    :goto_2
    if-eqz v0, :cond_2

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    goto :goto_1

    .line 1531
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1461
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 1462
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/buddy/ag;->p:Ljava/lang/String;

    .line 1463
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->d:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->invalidateViews()V

    .line 1466
    :cond_1
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1488
    iput-object p1, p0, Lcom/sec/chaton/buddy/ag;->q:Ljava/lang/String;

    .line 1489
    return-void
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2

    .prologue
    .line 281
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildType(II)I
    .locals 1

    .prologue
    .line 1507
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 1508
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChildTypeCount()I
    .locals 1

    .prologue
    .line 1513
    const/4 v0, 0x2

    return v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    .line 520
    .line 521
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 523
    if-nez p4, :cond_e

    .line 524
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 525
    iget-object v2, p0, Lcom/sec/chaton/buddy/ag;->e:Landroid/view/LayoutInflater;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/buddy/ag;->getChildType(II)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_8

    const v1, 0x7f030125

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v2, v1, p5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 531
    :goto_1
    new-instance v3, Lcom/sec/chaton/buddy/ar;

    move-object v1, v2

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-direct {v3, v1, v4}, Lcom/sec/chaton/buddy/ar;-><init>(Landroid/view/ViewGroup;Landroid/content/Context;)V

    .line 533
    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/buddy/ag;->getChildType(II)I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_c

    .line 535
    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 536
    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    .line 537
    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    .line 538
    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    .line 539
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x13

    if-ge v7, v8, :cond_0

    .line 540
    iget-object v7, v3, Lcom/sec/chaton/buddy/ar;->l:Landroid/widget/TableLayout;

    invoke-virtual {v7, v1, v4, v5, v6}, Landroid/widget/TableLayout;->setPadding(IIII)V

    .line 543
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 544
    iget-object v1, v3, Lcom/sec/chaton/buddy/ar;->l:Landroid/widget/TableLayout;

    const v4, 0x7f0202de

    invoke-virtual {v1, v4}, Landroid/widget/TableLayout;->setBackgroundResource(I)V

    .line 549
    :goto_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 550
    iget v1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    .line 551
    iget-object v1, v3, Lcom/sec/chaton/buddy/ar;->o:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 571
    :cond_1
    :goto_3
    iget-boolean v1, p0, Lcom/sec/chaton/buddy/ag;->k:Z

    if-nez v1, :cond_44

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/ag;->m:Z

    if-eqz v1, :cond_44

    .line 572
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v4, "0999"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_44

    .line 573
    iget-object v1, v3, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ag;->G:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object p4, v2

    move-object v2, v3

    .line 584
    :goto_4
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 586
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 588
    iget-boolean v1, p0, Lcom/sec/chaton/buddy/ag;->k:Z

    if-eqz v1, :cond_12

    .line 589
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "0999"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0094

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    const/16 v3, 0x3e8

    if-eq v1, v3, :cond_f

    iget v1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    const/4 v3, 0x1

    if-eq v1, v3, :cond_f

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 595
    :cond_2
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 596
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_5

    .line 597
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0094

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "0999"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    const/16 v3, 0x3e8

    if-eq v1, v3, :cond_5

    iget v1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    const/4 v3, 0x1

    if-eq v1, v3, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 599
    :cond_3
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 600
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0094

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "0999"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 601
    :cond_4
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->G:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 644
    :cond_5
    :goto_5
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->D()Ljava/lang/String;

    move-result-object v1

    .line 645
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->f:Landroid/widget/ImageView;

    if-eqz v3, :cond_6

    .line 646
    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->s:Lcom/sec/chaton/buddy/ao;

    if-eqz v3, :cond_15

    const-string v3, "unknown_buddy"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 647
    new-instance v1, Landroid/widget/TableRow$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 648
    const/16 v3, 0x15

    iput v3, v1, Landroid/widget/TableRow$LayoutParams;->gravity:I

    .line 649
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 651
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 652
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 653
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->f:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 654
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->f:Landroid/widget/ImageView;

    new-instance v3, Lcom/sec/chaton/buddy/aj;

    invoke-direct {v3, p0, v0}, Lcom/sec/chaton/buddy/aj;-><init>(Lcom/sec/chaton/buddy/ag;Lcom/sec/chaton/buddy/a/c;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 666
    :cond_6
    :goto_6
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 669
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    .line 670
    new-instance v3, Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v5

    if-nez v1, :cond_16

    const/4 v1, 0x0

    :goto_7
    const/4 v6, 0x2

    invoke-direct {v3, v4, v5, v1, v6}, Lcom/sec/chaton/buddy/a/b;-><init>(ILjava/lang/String;II)V

    .line 672
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->m()Z

    move-result v1

    if-eqz v1, :cond_17

    .line 673
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    iget-object v4, v2, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0201bc

    invoke-static {v4, v5}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 674
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v4, v2, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 715
    :goto_8
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 716
    const/4 v1, 0x0

    .line 717
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 718
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    .line 720
    :cond_7
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 721
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ")"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v3, v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 730
    :goto_9
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->d()Ljava/lang/String;

    move-result-object v1

    .line 732
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 734
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_29

    .line 735
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 737
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 738
    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 739
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_a

    .line 525
    :cond_8
    iget v1, p0, Lcom/sec/chaton/buddy/ag;->j:I

    goto/16 :goto_0

    .line 528
    :cond_9
    iget-object v2, p0, Lcom/sec/chaton/buddy/ag;->e:Landroid/view/LayoutInflater;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/buddy/ag;->getChildType(II)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_a

    const v1, 0x7f030128

    :goto_b
    const/4 v3, 0x0

    invoke-virtual {v2, v1, p5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto/16 :goto_1

    :cond_a
    iget v1, p0, Lcom/sec/chaton/buddy/ag;->j:I

    goto :goto_b

    .line 546
    :cond_b
    const v1, 0x7f0202de

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_2

    .line 555
    :cond_c
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 556
    iget v1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_d

    .line 557
    iget-object v1, v3, Lcom/sec/chaton/buddy/ar;->o:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 559
    :cond_d
    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 560
    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    .line 561
    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    .line 562
    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    .line 564
    iget-object v7, v3, Lcom/sec/chaton/buddy/ar;->l:Landroid/widget/TableLayout;

    invoke-virtual {v7, v1, v4, v5, v6}, Landroid/widget/TableLayout;->setPadding(IIII)V

    .line 565
    iget-object v1, v3, Lcom/sec/chaton/buddy/ar;->l:Landroid/widget/TableLayout;

    const v4, 0x7f02040e

    invoke-virtual {v1, v4}, Landroid/widget/TableLayout;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 578
    :cond_e
    invoke-virtual {p4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/buddy/ar;

    move-object v2, v1

    goto/16 :goto_4

    .line 606
    :cond_f
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 607
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->t:Lcom/sec/chaton/buddy/ap;

    if-eqz v1, :cond_11

    .line 608
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_10

    .line 609
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    new-instance v3, Lcom/sec/chaton/buddy/ah;

    invoke-direct {v3, p0, v0}, Lcom/sec/chaton/buddy/ah;-><init>(Lcom/sec/chaton/buddy/ag;Lcom/sec/chaton/buddy/a/c;)V

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 621
    :cond_10
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    new-instance v3, Lcom/sec/chaton/buddy/ai;

    invoke-direct {v3, p0, v0}, Lcom/sec/chaton/buddy/ai;-><init>(Lcom/sec/chaton/buddy/ag;Lcom/sec/chaton/buddy/a/c;)V

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 628
    :cond_11
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->d:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/widget/FastScrollableExpandableListView;->a(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 629
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 630
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ag;->d:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v4, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->a([Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_5

    .line 635
    :cond_12
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "0999"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_13

    iget v1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    const/16 v3, 0x3e8

    if-eq v1, v3, :cond_14

    iget v1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    const/4 v3, 0x1

    if-eq v1, v3, :cond_14

    .line 636
    :cond_13
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->G:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 639
    :cond_14
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_5

    .line 661
    :cond_15
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->f:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 670
    :cond_16
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    goto/16 :goto_7

    .line 676
    :cond_17
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_18

    .line 677
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    iget-object v4, v2, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0201bc

    invoke-static {v4, v5}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 679
    :cond_18
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v4, v2, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 681
    :cond_19
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0094

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 692
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "chaton_id"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/bt;->c(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 699
    :cond_1a
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->u()I

    move-result v5

    invoke-virtual {v1, v3, v4, v5}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;I)V

    goto/16 :goto_8

    .line 724
    :cond_1b
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 725
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto/16 :goto_9

    .line 743
    :cond_1c
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 744
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 745
    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 760
    :cond_1d
    :goto_c
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 763
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->m()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 764
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 775
    :cond_1e
    :goto_d
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->r:Lcom/sec/chaton/buddy/aq;

    invoke-interface {v1, v0}, Lcom/sec/chaton/buddy/aq;->b(Lcom/sec/chaton/buddy/a/c;)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 776
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 777
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->m()Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 778
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080019

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 790
    :goto_e
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 791
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->h:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 797
    :cond_1f
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    if-eqz v1, :cond_2f

    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 798
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_20

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->F()I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_21

    .line 799
    :cond_20
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 804
    :cond_21
    :goto_f
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 806
    iget v1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    const/16 v3, 0x3e8

    if-ne v1, v3, :cond_35

    .line 807
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->z:Ljava/lang/String;

    if-eqz v1, :cond_34

    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->z:Ljava/lang/String;

    const-string v3, "voip"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 808
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_31

    .line 809
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->F()I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_22

    .line 810
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->F()I

    move-result v1

    const/4 v3, 0x1

    if-eq v1, v3, :cond_30

    .line 811
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 872
    :cond_22
    :goto_10
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0094

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_23

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "0999"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_23

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 873
    :cond_23
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 886
    :cond_24
    :goto_11
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "0999"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3d

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/ag;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 887
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->k:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 888
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_25

    .line 889
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200e7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 890
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/ag;->a(F)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 897
    :cond_25
    :goto_12
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->q:Ljava/lang/String;

    if-eqz v1, :cond_26

    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->q:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_26

    .line 899
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    move-result-object v1

    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/chaton/util/at;->c(Ljava/lang/String;)Z

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3e

    .line 900
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    move-result-object v1

    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/chaton/util/at;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/ag;->A:Ljava/util/ArrayList;

    .line 901
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->A:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->q:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/at;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 902
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/buddy/ag;->B:Ljava/util/ArrayList;

    .line 903
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->d()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 904
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/chaton/util/at;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/chaton/buddy/ag;->A:Ljava/util/ArrayList;

    .line 905
    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->B:Ljava/util/ArrayList;

    iget-object v4, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v1, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 907
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->b()I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_26

    .line 908
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    .line 909
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08001a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->b()I

    move-result v4

    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->d()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->c()I

    move-result v5

    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->d()I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    const/16 v6, 0x21

    invoke-interface {v1, v3, v4, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 911
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 912
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->e()V

    .line 946
    :cond_26
    :goto_13
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 948
    iget v1, p0, Lcom/sec/chaton/buddy/ag;->y:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_27

    .line 949
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->o:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 950
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x1

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 952
    iget v3, p0, Lcom/sec/chaton/buddy/ag;->C:I

    if-ne v3, p1, :cond_42

    iget v3, p0, Lcom/sec/chaton/buddy/ag;->D:I

    if-ne v3, p2, :cond_42

    .line 953
    iget-boolean v3, p0, Lcom/sec/chaton/buddy/ag;->E:Z

    if-eqz v3, :cond_41

    .line 954
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->n:Landroid/view/View;

    const v4, 0x7f020413

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 958
    :goto_14
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->n:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 959
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->n:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 960
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->o:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 977
    :cond_27
    :goto_15
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-static {v1}, Landroid/sec/multiwindow/MultiWindowManager;->isMultiWindowServiceEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 978
    new-instance v1, Lcom/sec/chaton/buddy/ak;

    invoke-direct {v1, p0, v0}, Lcom/sec/chaton/buddy/ak;-><init>(Lcom/sec/chaton/buddy/ag;Lcom/sec/chaton/buddy/a/c;)V

    invoke-virtual {p4, v1}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1002
    :cond_28
    invoke-virtual {p4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1004
    return-object p4

    .line 749
    :cond_29
    const-string v1, ""

    goto/16 :goto_c

    .line 754
    :cond_2a
    if-eqz v1, :cond_2b

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1d

    .line 755
    :cond_2b
    const-string v1, ""

    goto/16 :goto_c

    .line 765
    :cond_2c
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->v()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 766
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_d

    .line 782
    :cond_2d
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_e

    .line 785
    :cond_2e
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 786
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_e

    .line 802
    :cond_2f
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_f

    .line 814
    :cond_30
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 815
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_22

    .line 816
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200ed

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 817
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/ag;->a(F)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    goto/16 :goto_10

    .line 822
    :cond_31
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_32

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/ag;->h(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 823
    :cond_32
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 824
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_22

    .line 825
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200ed

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 826
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/ag;->a(F)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    goto/16 :goto_10

    .line 829
    :cond_33
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_10

    .line 834
    :cond_34
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_10

    .line 838
    :cond_35
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_37

    .line 839
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->F()I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_22

    .line 840
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->F()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_36

    .line 841
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 842
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_22

    .line 843
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200ed

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 844
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/ag;->a(F)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    goto/16 :goto_10

    .line 847
    :cond_36
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_10

    .line 852
    :cond_37
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_38

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/ag;->h(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_39

    .line 853
    :cond_38
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 854
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_22

    .line 855
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200ed

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 856
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/ag;->a(F)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    goto/16 :goto_10

    .line 859
    :cond_39
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_10

    .line 868
    :cond_3a
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_10

    .line 875
    :cond_3b
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->E()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3c

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->E()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3c

    .line 876
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 877
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_24

    .line 878
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200ef

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 879
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/ag;->a(F)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    goto/16 :goto_11

    .line 882
    :cond_3c
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_11

    .line 893
    :cond_3d
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->k:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_12

    .line 919
    :cond_3e
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->q:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 920
    if-ltz v4, :cond_26

    .line 921
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->q:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/ag;->q:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v1

    .line 922
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    .line 924
    invoke-interface {v1, v4}, Landroid/text/Spannable;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/sec/chaton/util/be;->a(C)I

    move-result v5

    .line 925
    add-int/lit8 v6, v3, -0x1

    invoke-interface {v1, v6}, Landroid/text/Spannable;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/sec/chaton/util/be;->a(C)I

    move-result v6

    .line 929
    const/4 v7, 0x1

    if-lt v5, v7, :cond_3f

    const/16 v7, 0xa

    if-gt v5, v7, :cond_3f

    .line 930
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/sec/chaton/util/be;->a(Ljava/lang/String;I)I

    move-result v5

    .line 931
    sub-int/2addr v4, v5

    .line 933
    :cond_3f
    const/4 v5, 0x1

    if-lt v6, v5, :cond_40

    const/16 v5, 0xa

    if-gt v6, v5, :cond_40

    .line 934
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v6, v3, -0x1

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v7

    invoke-static {v5, v6, v7}, Lcom/sec/chaton/util/be;->a(Ljava/lang/String;II)I

    move-result v5

    .line 935
    add-int/2addr v3, v5

    .line 938
    :cond_40
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08001a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v6, 0x21

    invoke-interface {v1, v5, v4, v3, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 939
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_13

    .line 956
    :cond_41
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->n:Landroid/view/View;

    const v4, 0x7f020412

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_14

    .line 962
    :cond_42
    iget-boolean v3, p0, Lcom/sec/chaton/buddy/ag;->E:Z

    if-eqz v3, :cond_43

    .line 963
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->o:Landroid/view/View;

    const v4, 0x7f020415

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 967
    :goto_16
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->n:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 968
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->o:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 969
    iget-object v1, v2, Lcom/sec/chaton/buddy/ar;->o:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_15

    .line 965
    :cond_43
    iget-object v3, v2, Lcom/sec/chaton/buddy/ar;->o:Landroid/view/View;

    const v4, 0x7f020414

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_16

    :cond_44
    move-object p4, v2

    move-object v2, v3

    goto/16 :goto_4
.end method

.method public getChildrenCount(I)I
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .locals 2

    .prologue
    .line 276
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupType(I)I
    .locals 1

    .prologue
    .line 1493
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/ag;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getGroupTypeCount()I
    .locals 1

    .prologue
    .line 1498
    const/4 v0, 0x2

    return v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 297
    .line 300
    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/ag;->getGroupType(I)I

    move-result v0

    if-nez v0, :cond_4

    .line 301
    if-nez p3, :cond_3

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->e:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/buddy/ag;->h:I

    invoke-virtual {v0, v1, p4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 303
    new-instance v2, Lcom/sec/chaton/buddy/as;

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v2, v0}, Lcom/sec/chaton/buddy/as;-><init>(Landroid/view/ViewGroup;)V

    .line 304
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object p3, v1

    move-object v1, v2

    .line 342
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    .line 345
    iget-object v2, v1, Lcom/sec/chaton/buddy/as;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->b()Ljava/lang/String;

    move-result-object v2

    .line 349
    iget-object v3, v1, Lcom/sec/chaton/buddy/as;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0094

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 350
    iget-object v3, v1, Lcom/sec/chaton/buddy/as;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/ag;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b03c2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 351
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 352
    array-length v3, v2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    aget-object v3, v2, v7

    const-string v4, "live"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " \u202a"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->c()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ") "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\u202c"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v2, v2, v7

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 359
    iget-object v2, v1, Lcom/sec/chaton/buddy/as;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    :cond_0
    :goto_1
    if-eqz p2, :cond_7

    .line 372
    iget-object v0, v1, Lcom/sec/chaton/buddy/as;->a:Landroid/widget/TextView;

    const v2, 0x7f0202d3

    invoke-virtual {v0, v6, v6, v2, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 379
    :goto_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    iget v0, p0, Lcom/sec/chaton/buddy/ag;->y:I

    if-ne v0, v7, :cond_1

    .line 383
    iget-object v0, v1, Lcom/sec/chaton/buddy/as;->d:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 385
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/ag;->E:Z

    if-eqz v0, :cond_8

    .line 386
    iget-object v0, v1, Lcom/sec/chaton/buddy/as;->d:Landroid/view/View;

    const v2, 0x7f020415

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 396
    :cond_1
    :goto_3
    invoke-virtual {p3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 399
    :cond_2
    :goto_4
    return-object p3

    .line 306
    :cond_3
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/as;

    move-object v1, v0

    goto/16 :goto_0

    .line 309
    :cond_4
    if-nez p3, :cond_2

    .line 310
    new-instance p3, Landroid/view/View;

    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->e:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 311
    const/16 v0, 0x8

    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 362
    :cond_5
    iget-object v2, v1, Lcom/sec/chaton/buddy/as;->a:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->c()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 365
    :cond_6
    iget-object v2, v1, Lcom/sec/chaton/buddy/as;->a:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->c()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 375
    :cond_7
    iget-object v0, v1, Lcom/sec/chaton/buddy/as;->a:Landroid/widget/TextView;

    const v2, 0x7f0202d1

    invoke-virtual {v0, v6, v6, v2, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_2

    .line 388
    :cond_8
    iget-object v0, v1, Lcom/sec/chaton/buddy/as;->d:Landroid/view/View;

    const v2, 0x7f020414

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_3
.end method

.method public getPositionForSection(I)I
    .locals 2

    .prologue
    .line 1410
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->n:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 1411
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->o:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->n:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1413
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 1419
    const/4 v0, 0x0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1426
    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->n:[Ljava/lang/String;

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x1

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 2

    .prologue
    .line 291
    iget-object v1, p0, Lcom/sec/chaton/buddy/ag;->r:Lcom/sec/chaton/buddy/aq;

    iget-object v0, p0, Lcom/sec/chaton/buddy/ag;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-interface {v1, v0}, Lcom/sec/chaton/buddy/aq;->b(Lcom/sec/chaton/buddy/a/c;)Z

    move-result v0

    return v0
.end method
