.class Lcom/sec/chaton/buddy/fw;
.super Landroid/os/Handler;
.source "BuddyRecommendFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V
    .locals 0

    .prologue
    .line 1132
    iput-object p1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const v7, 0x7f0b0037

    const/4 v6, 0x1

    const v4, 0x7f0b016c

    const v3, 0x7f0b00be

    const/4 v5, 0x0

    .line 1135
    const-string v0, ""

    .line 1136
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1138
    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1264
    :goto_0
    return-void

    .line 1141
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1142
    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1145
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x12f

    if-ne v1, v2, :cond_a

    .line 1146
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1147
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_3

    .line 1149
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetBuddyList;

    .line 1150
    if-eqz v0, :cond_2

    .line 1151
    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetBuddyList;->buddy:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Buddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1153
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b027d

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1154
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->h(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/buddy/gm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/gm;->notifyDataSetChanged()V

    .line 1252
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->t(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;I)I

    .line 1253
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->k(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)I

    move-result v0

    if-lez v0, :cond_c

    .line 1257
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->m(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    goto/16 :goto_0

    .line 1157
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_4

    .line 1158
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1160
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1161
    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/fx;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/fx;-><init>(Lcom/sec/chaton/buddy/fw;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_1

    .line 1184
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADD BUDDY RESULT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e83

    if-ne v1, v2, :cond_5

    .line 1187
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1207
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/fy;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/fy;-><init>(Lcom/sec/chaton/buddy/fw;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_1

    .line 1189
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e84

    if-ne v1, v2, :cond_6

    .line 1190
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1192
    :cond_6
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e85

    if-ne v1, v2, :cond_7

    .line 1194
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1198
    :cond_7
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0xc355

    if-ne v0, v1, :cond_8

    .line 1199
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0289

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1203
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1228
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/fz;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/fz;-><init>(Lcom/sec/chaton/buddy/fw;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_1

    .line 1244
    :cond_a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_b

    .line 1245
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b003b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 1247
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v3, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 1259
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->m(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 1260
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->n(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    .line 1261
    iget-object v0, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->m(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fw;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    goto/16 :goto_0
.end method
