.class public Lcom/sec/chaton/buddy/a/d;
.super Ljava/lang/Object;
.source "SpecialBuddyItem.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/d;->f:Ljava/lang/String;

    .line 56
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/d;->f:Ljava/lang/String;

    .line 109
    iput-object p1, p0, Lcom/sec/chaton/buddy/a/d;->a:Ljava/lang/String;

    .line 110
    iput-object p2, p0, Lcom/sec/chaton/buddy/a/d;->b:Ljava/lang/String;

    .line 111
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/d;->m:Ljava/lang/String;

    .line 112
    const-string v0, "y"

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/d;->g:Ljava/lang/String;

    .line 113
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/d;->f:Ljava/lang/String;

    .line 94
    iput-object p1, p0, Lcom/sec/chaton/buddy/a/d;->a:Ljava/lang/String;

    .line 95
    iput-object p2, p0, Lcom/sec/chaton/buddy/a/d;->b:Ljava/lang/String;

    .line 96
    iput-object p3, p0, Lcom/sec/chaton/buddy/a/d;->c:Ljava/lang/String;

    .line 97
    iput-object p4, p0, Lcom/sec/chaton/buddy/a/d;->d:Ljava/lang/String;

    .line 98
    iput-object p5, p0, Lcom/sec/chaton/buddy/a/d;->e:Ljava/lang/String;

    .line 99
    iput-object p6, p0, Lcom/sec/chaton/buddy/a/d;->f:Ljava/lang/String;

    .line 100
    iput-object p7, p0, Lcom/sec/chaton/buddy/a/d;->g:Ljava/lang/String;

    .line 101
    iput-object p8, p0, Lcom/sec/chaton/buddy/a/d;->h:Ljava/lang/String;

    .line 102
    iput-object p9, p0, Lcom/sec/chaton/buddy/a/d;->i:Ljava/lang/String;

    .line 103
    iput-object p10, p0, Lcom/sec/chaton/buddy/a/d;->j:Ljava/lang/String;

    .line 104
    iput-object p11, p0, Lcom/sec/chaton/buddy/a/d;->l:Ljava/lang/String;

    .line 105
    iput-object p12, p0, Lcom/sec/chaton/buddy/a/d;->m:Ljava/lang/String;

    .line 106
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/d;->f:Ljava/lang/String;

    .line 78
    iput-object p1, p0, Lcom/sec/chaton/buddy/a/d;->a:Ljava/lang/String;

    .line 79
    iput-object p2, p0, Lcom/sec/chaton/buddy/a/d;->b:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/sec/chaton/buddy/a/d;->c:Ljava/lang/String;

    .line 81
    iput-object p4, p0, Lcom/sec/chaton/buddy/a/d;->d:Ljava/lang/String;

    .line 82
    iput-object p5, p0, Lcom/sec/chaton/buddy/a/d;->e:Ljava/lang/String;

    .line 83
    iput-object p6, p0, Lcom/sec/chaton/buddy/a/d;->f:Ljava/lang/String;

    .line 84
    iput-object p7, p0, Lcom/sec/chaton/buddy/a/d;->g:Ljava/lang/String;

    .line 85
    iput-object p8, p0, Lcom/sec/chaton/buddy/a/d;->h:Ljava/lang/String;

    .line 86
    iput-object p9, p0, Lcom/sec/chaton/buddy/a/d;->i:Ljava/lang/String;

    .line 87
    iput-object p10, p0, Lcom/sec/chaton/buddy/a/d;->j:Ljava/lang/String;

    .line 88
    iput-object p11, p0, Lcom/sec/chaton/buddy/a/d;->k:Ljava/lang/String;

    .line 89
    iput-object p12, p0, Lcom/sec/chaton/buddy/a/d;->l:Ljava/lang/String;

    .line 90
    iput-object p13, p0, Lcom/sec/chaton/buddy/a/d;->m:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/sec/chaton/buddy/a/d;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/buddy/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;
    .locals 13

    .prologue
    .line 50
    new-instance v0, Lcom/sec/chaton/buddy/a/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/sec/chaton/buddy/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;
    .locals 14

    .prologue
    .line 47
    new-instance v0, Lcom/sec/chaton/buddy/a/d;

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/sec/chaton/buddy/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->h:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->i:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->k:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/d;->l:Ljava/lang/String;

    return-object v0
.end method

.method public m()Lcom/sec/chaton/buddy/a/e;
    .locals 6

    .prologue
    .line 172
    invoke-static {}, Lcom/sec/chaton/buddy/a/e;->values()[Lcom/sec/chaton/buddy/a/e;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 173
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/e;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/a/d;->m:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 178
    :goto_1
    return-object v0

    .line 172
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 178
    :cond_1
    sget-object v0, Lcom/sec/chaton/buddy/a/e;->a:Lcom/sec/chaton/buddy/a/e;

    goto :goto_1
.end method
