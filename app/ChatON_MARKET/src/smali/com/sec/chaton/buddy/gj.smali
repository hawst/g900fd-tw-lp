.class Lcom/sec/chaton/buddy/gj;
.super Ljava/lang/Object;
.source "BuddyRecommendFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V
    .locals 0

    .prologue
    .line 703
    iput-object p1, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 2

    .prologue
    .line 777
    const-string v0, "onDeleteComplete"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 771
    const-string v0, "onInsertComplete"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 713
    const-string v0, "onQueryComplete"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->j(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_2

    .line 762
    :cond_0
    if-eqz p3, :cond_1

    .line 763
    :goto_0
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 766
    :cond_1
    return-void

    .line 719
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 757
    if-eqz p3, :cond_3

    .line 758
    :try_start_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 762
    :cond_3
    :goto_1
    if-eqz p3, :cond_1

    goto :goto_0

    .line 721
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;I)I

    .line 722
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->h(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/buddy/gm;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 723
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->h(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/buddy/gm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/gm;->notifyDataSetChanged()V

    .line 725
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->k(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)I

    move-result v0

    if-lez v0, :cond_6

    .line 729
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->m(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 762
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_5

    .line 763
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 762
    :cond_5
    throw v0

    .line 731
    :cond_6
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->m(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 732
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->n(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    .line 733
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->m(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    goto :goto_1

    .line 738
    :pswitch_1
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_7

    .line 739
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->o(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 741
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->o(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 742
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->q(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->p(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 743
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 744
    iget-object v0, p0, Lcom/sec/chaton/buddy/gj;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0, p3, p2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Landroid/database/Cursor;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 719
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 2

    .prologue
    .line 708
    const-string v0, "onUpdateComplete"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    return-void
.end method
