.class Lcom/sec/chaton/buddy/ei;
.super Ljava/lang/Object;
.source "BuddyInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V
    .locals 0

    .prologue
    .line 1071
    iput-object p1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1078
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1250
    :cond_0
    :goto_0
    return-void

    .line 1087
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1090
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1091
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1092
    const-string v1, "callChatList"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1094
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1095
    const-string v1, "receivers"

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1096
    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivity(Landroid/content/Intent;)V

    .line 1097
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1102
    :sswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "************ BUDDY NO : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "************ BUDDY ORG NO : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1108
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 1109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1120
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call Number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1123
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1126
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1127
    :catch_0
    move-exception v0

    .line 1128
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1112
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 1116
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_1

    .line 1131
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/util/List;

    move-result-object v0

    .line 1132
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_6

    .line 1133
    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1140
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call Number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1142
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1145
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1146
    :catch_1
    move-exception v0

    .line 1147
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1137
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tel:+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    .line 1149
    :cond_6
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 1150
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1151
    const-string v1, "PN_DIALOG_BUDDY_TYPE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1152
    const-string v1, "PN_DIALOG_BUDDY_MSISDNS"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1153
    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1160
    :sswitch_2
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 1161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RAWCONTACTID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mContactId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->g(Lcom/sec/chaton/buddy/BuddyInfoFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z

    .line 1165
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1166
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->h(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1167
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1169
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1170
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1173
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1177
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/util/List;

    move-result-object v1

    .line 1178
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_b

    .line 1179
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "+"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1180
    iget-object v2, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1183
    :cond_a
    iget-object v2, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "+"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1185
    :cond_b
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 1186
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1187
    const-string v1, "PN_DIALOG_BUDDY_TYPE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1188
    const-string v1, "PN_DIALOG_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1189
    const-string v1, "PN_DIALOG_BUDDY_MSISDNS"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1191
    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 1192
    const-string v1, "PN_DIALOG_BUDDY_SAMSUNGEMAIL"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1195
    :cond_c
    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1203
    :sswitch_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1204
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1205
    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1207
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->j(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->k(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->l(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1209
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->m(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1210
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->o(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1211
    if-eqz v0, :cond_0

    .line 1212
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b008a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 1216
    :catch_2
    move-exception v0

    .line 1217
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1221
    :cond_e
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1222
    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1227
    :sswitch_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1228
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1229
    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1231
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->j(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->k(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->l(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1233
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->m(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1234
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->o(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1235
    if-eqz v0, :cond_0

    .line 1236
    iget-object v0, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b008a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 1240
    :catch_3
    move-exception v0

    .line 1241
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1245
    :cond_10
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1246
    iget-object v1, p0, Lcom/sec/chaton/buddy/ei;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1087
    :sswitch_data_0
    .sparse-switch
        0x7f0700e3 -> :sswitch_0
        0x7f0700e7 -> :sswitch_3
        0x7f0700e9 -> :sswitch_4
        0x7f0700eb -> :sswitch_2
        0x7f0702b1 -> :sswitch_1
    .end sparse-switch
.end method
