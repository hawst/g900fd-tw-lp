.class Lcom/sec/chaton/buddy/co;
.super Ljava/lang/Object;
.source "BuddyFragment.java"

# interfaces
.implements Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 10441
    iput-object p1, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/view/View;ILjava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x6a

    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 10446
    if-eqz p3, :cond_0

    move-object v0, p3

    .line 10447
    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0094

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10448
    if-nez p2, :cond_0

    .line 10449
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 10450
    const-string v1, "callMyPageTab"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 10451
    iget-object v1, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    .line 10565
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p3

    .line 10453
    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "0999"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 10454
    if-nez p2, :cond_2

    .line 10455
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/sec/chaton/buddy/SpecialBuddyActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10456
    const-string v2, "specialuserid"

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10457
    const-string v2, "speicalusername"

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10458
    const-string v0, "specialBuddyAdded"

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {p3}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/af;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 10459
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 10460
    :cond_2
    if-ne p2, v4, :cond_6

    .line 10461
    iget-object v1, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 10462
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Z(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/ag;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 10463
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Z(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/ag;

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/buddy/ag;->g(Ljava/lang/String;)V

    .line 10464
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aw;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v0, p3

    .line 10465
    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/e/a/af;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 10467
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/buddy/cz;->d(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 10469
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Z(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/ag;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 10470
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Z(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/ag;

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/buddy/ag;->f(Ljava/lang/String;)V

    .line 10471
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aw;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v0, p3

    .line 10472
    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/e/a/af;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 10474
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/buddy/cz;->c(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 10476
    :cond_6
    if-ne p2, v5, :cond_7

    .line 10477
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-static {v0, p3, v6}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/a/c;I)V

    goto/16 :goto_0

    .line 10478
    :cond_7
    if-ne p2, v7, :cond_0

    .line 10479
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyFragment;->q:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {p3}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    move-object v0, p3

    .line 10482
    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->m()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 10483
    if-nez p2, :cond_9

    .line 10484
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10485
    const-string v2, "PROFILE_BUDDY_NO"

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10486
    const-string v0, "PROFILE_BUDDY_NAME"

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {p3}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10487
    const-string v0, "PROFILE_BUDDY_PROFILE_LOAD_DONE"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 10488
    const-string v0, "PROFILE_BUDDY_PROFILE_IMAGE_LOAD_DONE"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 10489
    const/high16 v0, 0x14000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 10490
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 10491
    :cond_9
    if-ne p2, v4, :cond_a

    .line 10492
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/buddy/cz;->a(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 10493
    :cond_a
    if-ne p2, v5, :cond_0

    .line 10494
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-static {v0, p3, v6}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/a/c;I)V

    goto/16 :goto_0

    .line 10496
    :cond_b
    iget-object v2, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 10497
    if-nez p2, :cond_c

    .line 10498
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10499
    const-string v2, "PROFILE_BUDDY_NO"

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10500
    const-string v0, "PROFILE_BUDDY_NAME"

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {p3}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10501
    const-string v0, "PROFILE_BUDDY_PROFILE_LOAD_DONE"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 10502
    const-string v0, "PROFILE_BUDDY_PROFILE_IMAGE_LOAD_DONE"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 10503
    const/high16 v0, 0x14000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 10504
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 10505
    :cond_c
    if-ne p2, v4, :cond_d

    .line 10506
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/buddy/cz;->b(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 10507
    :cond_d
    if-ne p2, v5, :cond_e

    .line 10508
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-static {v0, p3, v6}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/a/c;I)V

    goto/16 :goto_0

    .line 10509
    :cond_e
    if-ne p2, v7, :cond_0

    .line 10510
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/buddy/cz;->e(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    :cond_f
    move-object v0, p3

    .line 10513
    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 10514
    if-nez p2, :cond_11

    .line 10515
    new-instance v2, Lcom/sec/chaton/buddy/a/b;

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v4

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v5, v1, [Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_10

    move v0, v1

    :goto_1
    const/16 v5, 0x12

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/sec/chaton/buddy/a/b;-><init>(ILjava/lang/String;II)V

    .line 10517
    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-class v4, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10518
    const-string v0, "ACTIVITY_PURPOSE"

    const/16 v4, 0x12

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 10519
    const-string v0, "groupInfo"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 10520
    const-string v2, "GROUP_PROFILE_NAME"

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10521
    const-string v2, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {p3}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 10522
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_10
    move-object v0, p3

    .line 10515
    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v5, v1, [Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 10523
    :cond_11
    if-ne p2, v4, :cond_12

    .line 10524
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    const/16 v1, 0x73

    invoke-static {v0, p3, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/a/c;I)V

    goto/16 :goto_0

    .line 10525
    :cond_12
    if-ne p2, v5, :cond_0

    .line 10526
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/cp;

    invoke-direct {v2, p0, p3}, Lcom/sec/chaton/buddy/cp;-><init>(Lcom/sec/chaton/buddy/co;Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 10546
    :cond_13
    if-nez p2, :cond_14

    .line 10547
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10548
    const-string v2, "PROFILE_BUDDY_NO"

    move-object v0, p3

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10549
    const-string v0, "PROFILE_BUDDY_NAME"

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {p3}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10550
    const-string v0, "PROFILE_BUDDY_PROFILE_LOAD_DONE"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 10551
    const-string v0, "PROFILE_BUDDY_PROFILE_IMAGE_LOAD_DONE"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 10552
    const/high16 v0, 0x14000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 10553
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 10554
    :cond_14
    if-ne p2, v4, :cond_15

    .line 10555
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/buddy/cz;->a(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 10556
    :cond_15
    if-ne p2, v5, :cond_16

    .line 10557
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-static {v0, p3, v6}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/a/c;I)V

    goto/16 :goto_0

    .line 10558
    :cond_16
    if-ne p2, v7, :cond_0

    .line 10559
    iget-object v0, p0, Lcom/sec/chaton/buddy/co;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    check-cast p3, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/buddy/cz;->e(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0
.end method
