.class public Lcom/sec/chaton/buddy/BuddyProfileImageView;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "BuddyProfileImageView.java"


# static fields
.field public static a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, ""

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "PROFILE_BUDDY_IMAGE_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageView;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "PROFILE_BUDDY_IMAGE_TYPE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 34
    new-instance v3, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-direct {v3, v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 63
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_PROFILE_IMAGE_CALL_PREVIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    const-string v1, "PROFILE_BUDDY_PROFILE_IMAGE_CALL_PREVUEW_VALUE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-static {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a(Landroid/app/Activity;)V

    .line 67
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 54
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_PROFILE_IMAGE_CALL_PREVIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    const-string v1, "PROFILE_BUDDY_PROFILE_IMAGE_CALL_PREVUEW_VALUE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-static {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a(Landroid/app/Activity;)V

    .line 58
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 40
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 46
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 42
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageView;->finish()V

    goto :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
