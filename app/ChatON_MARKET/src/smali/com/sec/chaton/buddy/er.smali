.class Lcom/sec/chaton/buddy/er;
.super Ljava/lang/Object;
.source "BuddyProfileEditNameActivity.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)V
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/sec/chaton/buddy/er;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 322
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 292
    return-void
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 301
    if-ne p1, v0, :cond_0

    .line 302
    if-ne p3, v0, :cond_0

    .line 303
    const-string v0, "onUpdateComplete()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    check-cast p2, Ljava/lang/String;

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/buddy/er;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->c(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b007c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/buddy/er;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->b(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 317
    :cond_0
    return-void
.end method
