.class Lcom/sec/chaton/buddy/iq;
.super Ljava/lang/Object;
.source "SpecialBuddyRecommendListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/ip;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/ip;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 351
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 352
    iget-object v2, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v0, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ip;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/io;

    iget-object v0, v0, Lcom/sec/chaton/buddy/io;->a:Ljava/lang/String;

    iput-object v0, v2, Lcom/sec/chaton/buddy/ip;->b:Ljava/lang/String;

    .line 353
    iget-object v2, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v0, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ip;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/io;

    iget-object v0, v0, Lcom/sec/chaton/buddy/io;->b:Ljava/lang/String;

    iput-object v0, v2, Lcom/sec/chaton/buddy/ip;->c:Ljava/lang/String;

    .line 355
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v1, v1, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/SpecialBuddyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 357
    const-string v1, "specialuserid"

    iget-object v2, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v2, v2, Lcom/sec/chaton/buddy/ip;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 358
    const-string v1, "speicalusername"

    iget-object v2, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v2, v2, Lcom/sec/chaton/buddy/ip;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 361
    iget-object v1, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v1, v1, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->startActivity(Landroid/content/Intent;)V

    .line 371
    :goto_0
    return-void

    .line 364
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v1, v1, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 365
    const-string v1, "specialuserid"

    iget-object v2, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v2, v2, Lcom/sec/chaton/buddy/ip;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    const-string v1, "speicalusername"

    iget-object v2, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v2, v2, Lcom/sec/chaton/buddy/ip;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 367
    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 369
    iget-object v1, p0, Lcom/sec/chaton/buddy/iq;->a:Lcom/sec/chaton/buddy/ip;

    iget-object v1, v1, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
