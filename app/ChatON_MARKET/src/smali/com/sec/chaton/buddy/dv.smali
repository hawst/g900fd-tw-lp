.class Lcom/sec/chaton/buddy/dv;
.super Ljava/lang/Object;
.source "BuddyGroupProfileFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V
    .locals 0

    .prologue
    .line 730
    iput-object p1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/high16 v5, 0x4000000

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 735
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 923
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 737
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 738
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 739
    const-string v1, "groupInfo"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 740
    const-string v1, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 741
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 745
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyEditNickNameActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 746
    const-string v1, "PROFILE_ID"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 747
    const-string v1, "PROFILE_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 748
    const-string v1, "PROFILE_BUDDY_MODE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 749
    const-string v1, "PROFILE_BUDDY_LIST"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->j()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 750
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 753
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    iget v0, v0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    if-lez v0, :cond_0

    .line 754
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mGroupInfo.getId(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 758
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/TabActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 760
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 761
    const-string v1, "callChatList"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 762
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 763
    const-string v1, "receivers"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->j()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 764
    const-string v1, "groupId"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 765
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 768
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 770
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 771
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 772
    const-string v1, "receivers"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->j()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 773
    const-string v1, "groupId"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 775
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    .line 776
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 781
    :pswitch_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    iget v0, v0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    if-lez v0, :cond_0

    .line 782
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mGroupInfo.getId(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 786
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/TabActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 788
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 789
    const-string v1, "callChatList"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 790
    const-string v1, "broadcast2_feature"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 791
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 795
    :goto_1
    const-string v1, "receivers"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->j()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 796
    const-string v1, "groupId"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 797
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 793
    :cond_2
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 800
    :cond_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 802
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 803
    const-string v1, "broadcast2_feature"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 804
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 808
    :goto_2
    const-string v1, "receivers"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->j()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 809
    const-string v1, "groupId"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 811
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    .line 812
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 806
    :cond_4
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    .line 819
    :pswitch_5
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 820
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->o(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 821
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->p(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 831
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-gt v0, v4, :cond_5

    .line 832
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 841
    :catch_0
    move-exception v0

    .line 842
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 834
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/coolots/sso/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_6

    .line 835
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v6}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 837
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03ad

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/coolots/sso/a/a;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v6}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 845
    :cond_7
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 846
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 849
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->q(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 850
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 851
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 857
    :pswitch_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 858
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->o(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 859
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->p(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 869
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-gt v0, v4, :cond_9

    .line 870
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 879
    :catch_1
    move-exception v0

    .line 880
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 872
    :cond_9
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/coolots/sso/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_a

    .line 873
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v6}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 875
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03ae

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/coolots/sso/a/a;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v6}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 883
    :cond_b
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 884
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 887
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->q(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 888
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 889
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 895
    :pswitch_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Clicked the groupProfileImage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Clicked the groupProfileImage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v0

    .line 902
    :try_start_4
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    .line 909
    :cond_d
    :goto_3
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->r(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Ljava/io/File;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_group_profile.png_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 910
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "photoFile="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_e

    .line 912
    iget-object v0, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a()V

    goto/16 :goto_0

    .line 903
    :catch_2
    move-exception v1

    .line 905
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_d

    .line 906
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_3

    .line 914
    :cond_e
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/GroupProfileImageViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 915
    const-string v1, "GROUP_PROFILE_ID"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 916
    const-string v1, "GROUP_PROFILE_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 917
    iget-object v1, p0, Lcom/sec/chaton/buddy/dv;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 735
    :pswitch_data_0
    .packed-switch 0x7f070076
        :pswitch_7
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
