.class Lcom/sec/chaton/buddy/dialog/ab;
.super Landroid/os/Handler;
.source "SpecialBuddyDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V
    .locals 0

    .prologue
    .line 721
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v1, 0x2

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 724
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 726
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x13f

    if-ne v2, v3, :cond_4

    .line 727
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v3, :cond_3

    .line 728
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 729
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;

    .line 730
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 731
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2, v4}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z

    .line 732
    iget-object v2, v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;->count:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 733
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;->count:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v2, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;I)I

    .line 734
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 735
    const-string v0, "likecount"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    const-string v0, "islike"

    const-string v2, "Y"

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->n(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "buddy_no=\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 739
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    .line 749
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0, v8}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)V

    .line 920
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->p(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    .line 921
    return-void

    .line 743
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    .line 744
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 745
    const-string v0, "likecount"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    const-string v0, "islike"

    const-string v2, "N"

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->n(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "buddy_no=\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 750
    :cond_4
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x140

    if-ne v2, v3, :cond_8

    .line 751
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v3, :cond_7

    .line 752
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 753
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;

    .line 754
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Z

    move-result v2

    if-ne v2, v4, :cond_6

    .line 755
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2, v8}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z

    .line 756
    iget-object v2, v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;->count:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 757
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;->count:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v2, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;I)I

    .line 758
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 759
    const-string v0, "likecount"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    const-string v0, "islike"

    const-string v2, "N"

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->n(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "buddy_no=\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 763
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    .line 773
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0, v8}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)V

    goto/16 :goto_1

    .line 767
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    .line 768
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 769
    const-string v0, "likecount"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    const-string v0, "islike"

    const-string v2, "N"

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->n(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "buddy_no=\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_2

    .line 774
    :cond_8
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x13c

    if-ne v1, v2, :cond_d

    .line 775
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_c

    .line 777
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;

    .line 778
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "categoryList.bFollowCount : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->followcount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " categoryList.nLikeCount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->likecount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SpecialBuddyDialog"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->likecount:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;I)I

    .line 780
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 781
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 782
    new-instance v2, Lcom/sec/chaton/io/entry/inner/SpecialUser;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/inner/SpecialUser;-><init>()V

    .line 783
    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->followcount:Ljava/lang/String;

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->followcount:Ljava/lang/String;

    .line 784
    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->likecount:Ljava/lang/String;

    iput-object v3, v2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->likecount:Ljava/lang/String;

    .line 787
    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v5}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/a/d;Lcom/sec/chaton/io/entry/inner/SpecialUser;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/chaton/e/a/d;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/buddy/a/d;)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 789
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "com.sec.chaton.provider"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 797
    :cond_9
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 798
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->contentcategory:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/ContentCategory;

    .line 800
    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ContentCategory;->name:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 802
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->contentcategory:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 803
    const-string v1, ", "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 790
    :catch_0
    move-exception v1

    .line 791
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 792
    :catch_1
    move-exception v1

    .line 793
    invoke-virtual {v1}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_3

    .line 807
    :cond_b
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 808
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 811
    :cond_c
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 813
    :cond_d
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x132

    if-ne v1, v2, :cond_e

    .line 815
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 816
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check buddy isConnectionSuccess()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fault="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BUDDY PROFILE"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 870
    :cond_e
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x13d

    if-ne v1, v2, :cond_12

    .line 871
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 874
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    if-eqz v1, :cond_16

    .line 875
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v1

    .line 878
    :goto_5
    if-nez v1, :cond_f

    .line 879
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v1

    .line 882
    :cond_f
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    .line 883
    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v3, :cond_10

    .line 884
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->f(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z

    .line 885
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03ef

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 887
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->finish()V

    goto/16 :goto_1

    .line 889
    :cond_10
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_11

    .line 890
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Follow LivePartner, faultCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SpecialBuddyDialog"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03ec

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 895
    :cond_12
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x13e

    if-ne v1, v2, :cond_2

    .line 896
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 897
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    .line 900
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    if-eqz v2, :cond_13

    .line 901
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v6

    .line 904
    :cond_13
    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_14

    .line 905
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0, v8}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->f(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z

    .line 906
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 907
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/af;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    .line 908
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03ee

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v6, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 910
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->finish()V

    goto/16 :goto_1

    .line 912
    :cond_14
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_15

    .line 913
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unFollow LivePartner, faultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SpecialBuddyDialog"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    :cond_15
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ab;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03ed

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v6, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    :cond_16
    move-object v1, v6

    goto/16 :goto_5
.end method
