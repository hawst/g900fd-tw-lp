.class Lcom/sec/chaton/buddy/ec;
.super Ljava/lang/Object;
.source "BuddyInfoFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/net/Uri;

.field final synthetic c:Lcom/sec/chaton/buddy/BuddyInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1555
    iput-object p1, p0, Lcom/sec/chaton/buddy/ec;->c:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    iput-object p2, p0, Lcom/sec/chaton/buddy/ec;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/chaton/buddy/ec;->b:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1558
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1559
    const-string v1, "phone"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ec;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1560
    const-string v1, "name"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ec;->c:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1562
    iget-object v1, p0, Lcom/sec/chaton/buddy/ec;->c:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1563
    const-string v1, "email"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ec;->c:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1571
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.INSERT"

    iget-object v3, p0, Lcom/sec/chaton/buddy/ec;->b:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1572
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1573
    const-string v0, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1576
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ec;->c:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1581
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1582
    return-void

    .line 1577
    :catch_0
    move-exception v0

    .line 1578
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1579
    iget-object v0, p0, Lcom/sec/chaton/buddy/ec;->c:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00d8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
