.class public Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;
.super Lcom/sec/chaton/base/BaseActivity;
.source "BuddyProfileEditNameActivity.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Ljava/lang/String;

.field c:Lcom/sec/chaton/d/h;

.field d:Lcom/sec/chaton/e/b/d;

.field e:Lcom/sec/chaton/e/a/v;

.field private f:Landroid/content/Context;

.field private g:Lcom/sec/chaton/widget/ClearableEditText;

.field private h:Ljava/lang/String;

.field private i:Lcom/sec/chaton/e/a/u;

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Landroid/app/ProgressDialog;

.field private n:Landroid/text/TextWatcher;

.field private o:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 70
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->b:Ljava/lang/String;

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->m:Landroid/app/ProgressDialog;

    .line 174
    new-instance v0, Lcom/sec/chaton/buddy/ep;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ep;-><init>(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->d:Lcom/sec/chaton/e/b/d;

    .line 206
    new-instance v0, Lcom/sec/chaton/buddy/eq;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/eq;-><init>(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->n:Landroid/text/TextWatcher;

    .line 287
    new-instance v0, Lcom/sec/chaton/buddy/er;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/er;-><init>(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->e:Lcom/sec/chaton/e/a/v;

    .line 355
    new-instance v0, Lcom/sec/chaton/buddy/es;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/es;-><init>(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->o:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->m:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 105
    iput-object p0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->f:Landroid/content/Context;

    .line 114
    const v0, 0x7f0700c3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setInputType(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 119
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->f:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 120
    if-eqz v0, :cond_0

    .line 121
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v1, Lcom/sec/chaton/buddy/en;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/en;-><init>(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->n:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    const v1, 0x7f0b0031

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setMaxLength(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v1, Lcom/sec/chaton/buddy/eo;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/eo;-><init>(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 172
    return-void

    .line 123
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->b()V

    return-void
.end method

.method private a(Lcom/sec/chaton/widget/ClearableEditText;)V
    .locals 3

    .prologue
    .line 279
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->f:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 280
    if-eqz v0, :cond_0

    .line 281
    invoke-virtual {p1}, Lcom/sec/chaton/widget/ClearableEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 283
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->j:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Lcom/sec/chaton/widget/ClearableEditText;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    return-object v0
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->clearFocus()V

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->a(Lcom/sec/chaton/widget/ClearableEditText;)V

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->h:Ljava/lang/String;

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->c:Lcom/sec/chaton/d/h;

    const-string v1, "update"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->h:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 250
    new-instance v0, Lcom/sec/chaton/e/b/l;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->d:Lcom/sec/chaton/e/b/d;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->k:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/sec/chaton/e/b/l;-><init>(Lcom/sec/chaton/e/b/d;Ljava/lang/String;Ljava/lang/String;I)V

    .line 251
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    invoke-static {v1, v6, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 257
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 258
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 260
    const-string v2, "PROFILE_BUDDY_RENAME"

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 263
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->setResult(ILandroid/content/Intent;)V

    .line 264
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->finish()V

    .line 265
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->f:Landroid/content/Context;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 335
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 338
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 339
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 340
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->startActivity(Landroid/content/Intent;)V

    .line 343
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->m:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 81
    const-string v0, "onCreate()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const v0, 0x7f030020

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->setContentView(I)V

    .line 85
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->k:Ljava/lang/String;

    .line 86
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->l:Ljava/lang/String;

    .line 88
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->e:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->i:Lcom/sec/chaton/e/a/u;

    .line 90
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->m:Landroid/app/ProgressDialog;

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->m:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 93
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->o:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->c:Lcom/sec/chaton/d/h;

    .line 95
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->a()V

    .line 96
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 348
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onDestroy()V

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->m:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 351
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->m:Landroid/app/ProgressDialog;

    .line 353
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->n:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->b(Landroid/text/TextWatcher;)V

    .line 101
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onPause()V

    .line 102
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 329
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onResume()V

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->g:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->n:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 331
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->c()V

    .line 332
    return-void
.end method
