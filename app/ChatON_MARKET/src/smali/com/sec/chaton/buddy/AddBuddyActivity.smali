.class public Lcom/sec/chaton/buddy/AddBuddyActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "AddBuddyActivity.java"


# instance fields
.field a:Lcom/sec/chaton/buddy/AddBuddyFragment;

.field private b:Landroid/view/Menu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyActivity;->b:Landroid/view/Menu;

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 30
    const-string v0, "com.sec.chaton.action.ADD_BUDDY"

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 35
    :cond_0
    new-instance v0, Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyActivity;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    .line 37
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyActivity;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 122
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    invoke-static {p0}, Lcom/sec/chaton/buddy/AddBuddyActivity;->a(Landroid/app/Activity;)V

    .line 126
    :cond_0
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const v4, 0x7f0b0065

    const/4 v2, 0x1

    .line 93
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v1

    .line 97
    if-eqz v1, :cond_0

    .line 98
    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/a;->a(Z)V

    .line 99
    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/a;->d(Z)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030030

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 102
    invoke-virtual {v1, v0}, Lcom/sec/common/actionbar/a;->a(Landroid/view/View;)V

    .line 103
    invoke-virtual {v1}, Lcom/sec/common/actionbar/a;->g()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 104
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 107
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    invoke-static {p0}, Lcom/sec/chaton/buddy/AddBuddyActivity;->a(Landroid/app/Activity;)V

    .line 111
    :cond_1
    return-void
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 57
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 69
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_1
    return v0

    .line 59
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyActivity;->finish()V

    .line 60
    const/4 v0, 0x1

    goto :goto_1

    .line 62
    :sswitch_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyActivity;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyActivity;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f07055c -> :sswitch_1
    .end sparse-switch
.end method

.method public onSupportPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 44
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 45
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 47
    const/high16 v1, 0x7f0f0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 48
    const v0, 0x7f07055c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 49
    iput-object p1, p0, Lcom/sec/chaton/buddy/AddBuddyActivity;->b:Landroid/view/Menu;

    .line 51
    :cond_0
    return v2
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onWindowFocusChanged(Z)V

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyActivity;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 78
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyActivity;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a()V

    .line 82
    :cond_0
    return-void
.end method
