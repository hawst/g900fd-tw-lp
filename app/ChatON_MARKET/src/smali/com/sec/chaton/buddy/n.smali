.class Lcom/sec/chaton/buddy/n;
.super Ljava/lang/Object;
.source "AddBuddyFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/AddBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 0

    .prologue
    .line 1445
    iput-object p1, p0, Lcom/sec/chaton/buddy/n;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1449
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1450
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/n;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1451
    const-string v1, "PROFILE_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/n;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->e(Lcom/sec/chaton/buddy/AddBuddyFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1452
    const-string v1, "PROFILE_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/n;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->f(Lcom/sec/chaton/buddy/AddBuddyFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1453
    const-string v1, "PROFILE_BUDDY_SUGGESTION"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1454
    iget-object v1, p0, Lcom/sec/chaton/buddy/n;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v1, v0, v4}, Lcom/sec/chaton/buddy/AddBuddyFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1463
    :goto_0
    return-void

    .line 1457
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/n;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1458
    const-string v1, "BUDDY_DIALOG_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/n;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->e(Lcom/sec/chaton/buddy/AddBuddyFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1459
    const-string v1, "BUDDY_DIALOG_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/n;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->f(Lcom/sec/chaton/buddy/AddBuddyFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1460
    const-string v1, "BUDDY_DIALOG_BUDDY_FROM_SUGGESTION"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1461
    iget-object v1, p0, Lcom/sec/chaton/buddy/n;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v1, v0, v4}, Lcom/sec/chaton/buddy/AddBuddyFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
