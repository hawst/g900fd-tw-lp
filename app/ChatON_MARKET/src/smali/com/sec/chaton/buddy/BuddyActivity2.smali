.class public Lcom/sec/chaton/buddy/BuddyActivity2;
.super Lcom/sec/chaton/base/BaseMultiPaneActivity;
.source "BuddyActivity2.java"


# instance fields
.field private a:Lcom/sec/chaton/base/d;

.field private b:Lcom/sec/chaton/buddy/BuddyFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseMultiPaneActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 132
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyActivity2;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 135
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyActivity2;->startActivity(Landroid/content/Intent;)V

    .line 140
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)Lcom/sec/chaton/base/c;
    .locals 1

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseMultiPaneActivity;->a(Ljava/lang/String;)Lcom/sec/chaton/base/c;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyActivity2;->a:Lcom/sec/chaton/base/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyActivity2;->a:Lcom/sec/chaton/base/d;

    invoke-interface {v0}, Lcom/sec/chaton/base/d;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 74
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/base/BaseMultiPaneActivity;->onBackPressed()V

    .line 78
    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseMultiPaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 126
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 127
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseMultiPaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v0, 0x7f030019

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyActivity2;->setContentView(I)V

    .line 28
    if-nez p1, :cond_2

    .line 29
    new-instance v0, Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyActivity2;->b:Lcom/sec/chaton/buddy/BuddyFragment;

    .line 30
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyActivity2;->b:Lcom/sec/chaton/buddy/BuddyFragment;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyActivity2;->a:Lcom/sec/chaton/base/d;

    .line 31
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyActivity2;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyActivity2;->b(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 32
    const-string v1, "android.intent.action.PICK"

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyActivity2;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 34
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 36
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyActivity2;->b:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->setArguments(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyActivity2;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 39
    const v1, 0x7f070013

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyActivity2;->b:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 40
    const v1, 0x7f07000d

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyActivity2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 44
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 46
    :cond_2
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 111
    invoke-super {p0}, Lcom/sec/chaton/base/BaseMultiPaneActivity;->onResume()V

    .line 115
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 118
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyActivity2;->a()V

    .line 119
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 51
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyActivity2;->finish()V

    .line 53
    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseMultiPaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
