.class public Lcom/sec/chaton/buddy/SpecialBuddyFragment;
.super Landroid/support/v4/app/Fragment;
.source "SpecialBuddyFragment.java"


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/Button;

.field private C:Landroid/widget/ImageButton;

.field private D:Landroid/widget/ImageButton;

.field private E:Landroid/widget/ImageButton;

.field private F:Landroid/widget/ImageButton;

.field private G:Landroid/widget/ImageButton;

.field private H:Landroid/view/ViewGroup;

.field private I:Landroid/view/View;

.field private J:Landroid/view/View;

.field private K:Landroid/view/View;

.field private L:Landroid/widget/TextView;

.field private M:Landroid/widget/ImageView;

.field private N:Landroid/view/View;

.field private O:Landroid/widget/TextView;

.field private P:Landroid/widget/ImageView;

.field private Q:Landroid/widget/LinearLayout;

.field private R:Landroid/widget/TextView;

.field private S:Landroid/view/ViewGroup;

.field private T:Landroid/widget/Button;

.field private U:Landroid/widget/Button;

.field private V:Landroid/widget/Button;

.field private W:Landroid/widget/Button;

.field private X:Z

.field private Y:Landroid/app/Activity;

.field private Z:Landroid/graphics/Bitmap;

.field a:Landroid/view/View$OnClickListener;

.field private aa:Landroid/view/Menu;

.field private ab:Landroid/view/MenuItem;

.field private ac:Landroid/view/MenuItem;

.field private ad:Landroid/widget/LinearLayout;

.field private ae:Landroid/widget/ImageView;

.field private af:Z

.field b:Lcom/sec/chaton/e/a/v;

.field c:Lcom/sec/chaton/e/b/d;

.field d:Landroid/os/Handler;

.field e:Landroid/database/ContentObserver;

.field private f:Lcom/sec/chaton/e/a/u;

.field private g:Lcom/sec/chaton/buddy/a/d;

.field private h:Lcom/sec/chaton/d/n;

.field private i:Landroid/app/ProgressDialog;

.field private j:Landroid/widget/Toast;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:I

.field private o:J

.field private p:Z

.field private q:Z

.field private r:Landroid/widget/ImageView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Z

.field private v:Landroid/view/View;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 92
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 123
    iput v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m:I

    .line 124
    iput v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->n:I

    .line 125
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->o:J

    .line 126
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p:Z

    .line 127
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->q:Z

    .line 171
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->X:Z

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Z:Landroid/graphics/Bitmap;

    .line 1022
    new-instance v0, Lcom/sec/chaton/buddy/hq;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/hq;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    .line 1229
    new-instance v0, Lcom/sec/chaton/buddy/hr;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/hr;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b:Lcom/sec/chaton/e/a/v;

    .line 1346
    new-instance v0, Lcom/sec/chaton/buddy/hs;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/hs;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->c:Lcom/sec/chaton/e/b/d;

    .line 1372
    new-instance v0, Lcom/sec/chaton/buddy/ht;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ht;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d:Landroid/os/Handler;

    .line 1895
    new-instance v0, Lcom/sec/chaton/buddy/hw;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/hw;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->e:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->S:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic B(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->T:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic C(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->U:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic D(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->V:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic E(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->W:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic F(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->s:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic G(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->R:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)I
    .locals 0

    .prologue
    .line 92
    iput p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;J)J
    .locals 0

    .prologue
    .line 92
    iput-wide p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->o:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1872
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/forward/ChatForwardActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1873
    const-string v1, "content_type"

    invoke-virtual {p2}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1874
    const-string v1, "inboxNO"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1875
    if-lez p5, :cond_0

    .line 1876
    const-string v1, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1879
    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1880
    const-string v1, "download_uri"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1890
    :goto_0
    const-string v1, "is_forward_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1891
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1892
    return-object v0

    .line 1882
    :cond_1
    const-string v1, "download_uri"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1885
    const-string v1, "sub_content"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Landroid/content/Context;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 92
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Landroid/content/Context;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Z:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/d;
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g:Lcom/sec/chaton/buddy/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l:Ljava/lang/String;

    return-object p1
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 436
    if-eqz p1, :cond_1

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->x:Landroid/widget/TextView;

    const v1, 0x7f0b03c7

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->x:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(J)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 425
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->w:Landroid/widget/TextView;

    const v1, 0x7f0b03c6

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 432
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->w:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;Z)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/a/d;Z)V

    return-void
.end method

.method private a(Lcom/sec/chaton/buddy/a/d;Z)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 446
    if-eqz p1, :cond_6

    .line 447
    if-eqz p2, :cond_7

    .line 448
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->setHasOptionsMenu(Z)V

    .line 452
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 455
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    :cond_0
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Ljava/lang/String;)V

    .line 482
    if-eqz p2, :cond_1

    .line 488
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->X:Z

    if-eqz v0, :cond_8

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->H:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 490
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->H:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 491
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->y:Landroid/widget/TextView;

    const v1, 0x7f02029a

    invoke-virtual {v0, v4, v4, v1, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 499
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 500
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->o:J

    .line 501
    iget-wide v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->o:J

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(J)V

    .line 504
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->r:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 505
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 506
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/buddy/hy;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/hy;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/util/ad;->a(Ljava/lang/String;Lcom/sec/chaton/util/bb;)V

    .line 568
    :cond_3
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 569
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m:I

    .line 570
    iget v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(I)V

    .line 574
    :cond_4
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_9

    .line 575
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 576
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->L:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 577
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->I:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 578
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->L:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 579
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/buddy/ia;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ia;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 592
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/buddy/ib;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/buddy/ib;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 605
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_a

    .line 606
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 607
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->O:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->l()Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 608
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->J:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 609
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->O:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 610
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/buddy/ic;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ic;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 619
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    const-string v3, "tel:+000"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 620
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/buddy/id;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/id;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 639
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->z:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 640
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->z:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 642
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b()V

    .line 656
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->q:Z

    if-eqz v0, :cond_b

    .line 657
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 663
    :goto_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 664
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->c(Z)V

    .line 670
    :cond_6
    :goto_5
    return-void

    .line 450
    :cond_7
    invoke-virtual {p0, v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->setHasOptionsMenu(Z)V

    goto/16 :goto_0

    .line 494
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->H:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 495
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_1

    .line 589
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 590
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->I:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 636
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 637
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->J:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 659
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_4

    .line 667
    :cond_c
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Z)V

    goto :goto_5
.end method

.method private a(Lcom/sec/chaton/buddy/ii;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1918
    sget-object v0, Lcom/sec/chaton/buddy/hx;->b:[I

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/ii;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1932
    :goto_0
    return-void

    .line 1920
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j:Landroid/widget/Toast;

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 1921
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 1922
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1926
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j:Landroid/widget/Toast;

    const v1, 0x7f0b002a

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 1927
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 1928
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1918
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 404
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 407
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 409
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/buddy/hl;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/hl;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f0300e3

    .line 358
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ab:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ac:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 360
    if-eqz p1, :cond_1

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ab:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setActionView(I)Landroid/view/MenuItem;

    .line 362
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ac:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setActionView(I)Landroid/view/MenuItem;

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ab:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 365
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ac:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z
    .locals 0

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->r:Landroid/widget/ImageView;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 673
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p:Z

    if-eqz v0, :cond_1

    .line 674
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->c()V

    .line 675
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->B:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 680
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 681
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->D:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 693
    :goto_1
    return-void

    .line 678
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->C:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 683
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 684
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->B:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 688
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->E:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 689
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 690
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 691
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->D:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1

    .line 686
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->C:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_2
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;J)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(J)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->c(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ab:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ac:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 374
    if-eqz p1, :cond_1

    .line 376
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ab:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ac:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ab:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 381
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ac:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Z:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 696
    sget-object v0, Lcom/sec/chaton/buddy/hx;->a:[I

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/d;->m()Lcom/sec/chaton/buddy/a/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 714
    :goto_0
    return-void

    .line 698
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->E:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 699
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 702
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->E:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 703
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 706
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->E:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 707
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 710
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->E:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 711
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 696
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 390
    if-eqz p1, :cond_1

    .line 391
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ae:Landroid/widget/ImageView;

    const v1, 0x7f020015

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ae:Landroid/widget/ImageView;

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->i:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private d()V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const v10, 0x7f09019d

    const v9, 0x7f07014c

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 720
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 721
    const v1, 0x7f030147

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 725
    const v0, 0x7f07051e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->x:Landroid/widget/TextView;

    .line 726
    iget v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(I)V

    .line 730
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 731
    const v0, 0x7f07052b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->s:Landroid/widget/TextView;

    .line 732
    const v0, 0x7f07052c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ae:Landroid/widget/ImageView;

    .line 733
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ae:Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/chaton/buddy/ie;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/ie;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 749
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p:Z

    if-eqz v0, :cond_3

    .line 750
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ae:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 757
    :goto_0
    const v0, 0x7f07052e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->v:Landroid/view/View;

    .line 758
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 759
    const v2, 0x7f0b018c

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 764
    :cond_0
    const v0, 0x7f07051c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t:Landroid/widget/TextView;

    .line 765
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 768
    const v0, 0x7f07051d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->w:Landroid/widget/TextView;

    .line 769
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 770
    iget-wide v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->o:J

    invoke-direct {p0, v2, v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(J)V

    .line 775
    :cond_1
    const v0, 0x7f07051b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->r:Landroid/widget/ImageView;

    .line 776
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 779
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->r:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 782
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 783
    const v0, 0x7f070523

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->B:Landroid/widget/Button;

    .line 784
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->B:Landroid/widget/Button;

    new-instance v2, Lcom/sec/chaton/buddy/if;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/if;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 794
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->B:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 810
    :goto_1
    const v0, 0x7f07051f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F:Landroid/widget/ImageButton;

    .line 811
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/chaton/buddy/hm;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/hm;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 821
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 823
    const v0, 0x7f070520

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->E:Landroid/widget/ImageButton;

    .line 824
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->E:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/chaton/buddy/hn;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/hn;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 834
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->E:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 836
    const v0, 0x7f070521

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->G:Landroid/widget/ImageButton;

    .line 837
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->G:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/chaton/buddy/ho;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/ho;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 847
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->G:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 850
    const v0, 0x7f070525

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->I:Landroid/view/View;

    .line 851
    const v0, 0x7f070527

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->J:Landroid/view/View;

    .line 852
    const v0, 0x7f070522

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->D:Landroid/widget/ImageButton;

    .line 853
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->D:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/chaton/buddy/hp;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/hp;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 863
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->D:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 864
    const v0, 0x7f07045f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Q:Landroid/widget/LinearLayout;

    .line 866
    const v0, 0x7f070524

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    .line 867
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setFocusable(Z)V

    .line 868
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    const v2, 0x7f0702d7

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 869
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->L:Landroid/widget/TextView;

    .line 870
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    const v2, 0x7f07014b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->M:Landroid/widget/ImageView;

    .line 871
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->M:Landroid/widget/ImageView;

    const v2, 0x7f0202e9

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 872
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->M:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 873
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->M:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 874
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->M:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 876
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 877
    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    .line 878
    iget-object v3, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    .line 879
    iget-object v4, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 880
    iget-object v5, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    const v6, 0x7f0202dc

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 881
    iget-object v5, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->K:Landroid/view/View;

    invoke-virtual {v5, v0, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 883
    const v0, 0x7f070526

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    .line 884
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setFocusable(Z)V

    .line 885
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    const v2, 0x7f0702d7

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 886
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->O:Landroid/widget/TextView;

    .line 887
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    const v2, 0x7f07014b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->P:Landroid/widget/ImageView;

    .line 888
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->P:Landroid/widget/ImageView;

    const v2, 0x7f0202e2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 889
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->P:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 890
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->P:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 891
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->P:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 893
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 894
    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    .line 895
    iget-object v3, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    .line 896
    iget-object v4, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 897
    iget-object v5, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    const v6, 0x7f0202dc

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 898
    iget-object v5, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->N:Landroid/view/View;

    invoke-virtual {v5, v0, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 900
    const v0, 0x7f070528

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->z:Landroid/widget/TextView;

    .line 902
    const v0, 0x7f070529

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->H:Landroid/view/ViewGroup;

    .line 903
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->H:Landroid/view/ViewGroup;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->y:Landroid/widget/TextView;

    .line 905
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->y:Landroid/widget/TextView;

    const v2, 0x7f0b0279

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v2, v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 908
    const v0, 0x7f07052a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->A:Landroid/widget/TextView;

    .line 909
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ad:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 910
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ad:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 912
    iput-boolean v8, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->af:Z

    .line 913
    return-void

    .line 752
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ae:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 796
    :cond_4
    const v0, 0x7f070523

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->C:Landroid/widget/ImageButton;

    .line 797
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->C:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/chaton/buddy/ig;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/ig;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 807
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->C:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z
    .locals 0

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p:Z

    return p1
.end method

.method private d(Z)Z
    .locals 1

    .prologue
    .line 1907
    invoke-static {}, Lcom/sec/common/util/i;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1908
    if-eqz p1, :cond_0

    .line 1909
    sget-object v0, Lcom/sec/chaton/buddy/ii;->a:Lcom/sec/chaton/buddy/ii;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/ii;)V

    .line 1911
    :cond_0
    const/4 v0, 0x0

    .line 1913
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->O:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 919
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g:Lcom/sec/chaton/buddy/a/d;

    if-nez v0, :cond_1

    .line 921
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 922
    const v1, 0x7f030139

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 924
    const v0, 0x7f0704ff

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->s:Landroid/widget/TextView;

    .line 925
    const v0, 0x7f070500

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->R:Landroid/widget/TextView;

    .line 927
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 928
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->s:Landroid/widget/TextView;

    const v2, 0x7f0b00b4

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 933
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->s:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMarqueeRepeatLimit(I)V

    .line 934
    const v0, 0x7f070501

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->S:Landroid/view/ViewGroup;

    .line 938
    const v0, 0x7f070502

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->T:Landroid/widget/Button;

    .line 939
    const v0, 0x7f070503

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->U:Landroid/widget/Button;

    .line 940
    const v0, 0x7f070504

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->V:Landroid/widget/Button;

    .line 941
    const v0, 0x7f070505

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->W:Landroid/widget/Button;

    .line 943
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->T:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 944
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->U:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 945
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->V:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 946
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->W:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 953
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 954
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->T:Landroid/widget/Button;

    const v2, 0x7f0b0081

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 955
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->U:Landroid/widget/Button;

    const v2, 0x7f0b0080

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 956
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->V:Landroid/widget/Button;

    const v2, 0x7f0b003a

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 957
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->W:Landroid/widget/Button;

    const v2, 0x7f0b00e7

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 959
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ad:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 960
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ad:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 962
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j()V

    .line 964
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 965
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/h;->f(Ljava/lang/String;Z)I

    .line 967
    :cond_1
    return-void

    .line 930
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->s:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 931
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->s:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Z)V

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1202
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1203
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->c(Ljava/lang/String;)V

    .line 1204
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Z)V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g:Lcom/sec/chaton/buddy/a/d;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1207
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1208
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->d(Ljava/lang/String;)V

    .line 1209
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z
    .locals 0

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->q:Z

    return p1
.end method

.method private h()V
    .locals 3

    .prologue
    .line 1218
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j()V

    .line 1219
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    .line 1220
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/f;->a(Ljava/lang/String;I)V

    .line 1221
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j()V

    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1224
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j()V

    .line 1225
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    .line 1226
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/f;->a(Ljava/lang/String;)V

    .line 1227
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->f()V

    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 1803
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->i:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1804
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1806
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p:Z

    return v0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 1809
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->i:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1811
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1813
    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g()V

    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ae:Landroid/widget/ImageView;

    return-object v0
.end method

.method private l()V
    .locals 0

    .prologue
    .line 1823
    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    return-object v0
.end method

.method private m()V
    .locals 3

    .prologue
    .line 1852
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1853
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1856
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1857
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1858
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1859
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1860
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->startActivity(Landroid/content/Intent;)V

    .line 1862
    :cond_1
    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->h()V

    return-void
.end method

.method static synthetic p(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->i()V

    return-void
.end method

.method static synthetic q(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->e()V

    return-void
.end method

.method static synthetic r(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->af:Z

    return v0
.end method

.method static synthetic s(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d()V

    return-void
.end method

.method static synthetic t(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k()V

    return-void
.end method

.method static synthetic u(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m:I

    return v0
.end method

.method static synthetic v(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->f:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic w(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/d/n;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->h:Lcom/sec/chaton/d/n;

    return-object v0
.end method

.method static synthetic x(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Q:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic y(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l()V

    return-void
.end method

.method static synthetic z(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)J
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->o:J

    return-wide v0
.end method


# virtual methods
.method a(Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/c;
    .locals 21

    .prologue
    .line 1835
    new-instance v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/buddy/a/d;->f()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-direct/range {v0 .. v20}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZI)V

    .line 1848
    return-object v0
.end method

.method a(Lcom/sec/chaton/buddy/a/d;Lcom/sec/chaton/io/entry/inner/SpecialUser;)Lcom/sec/chaton/buddy/a/d;
    .locals 12

    .prologue
    .line 1827
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->specialuserid:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->name:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->description:Ljava/lang/String;

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->c()Ljava/lang/String;

    move-result-object v2

    :goto_2
    iget-object v3, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->followcount:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->d()Ljava/lang/String;

    move-result-object v3

    :goto_3
    iget-object v4, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->likecount:Ljava/lang/String;

    if-nez v4, :cond_4

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->e()Ljava/lang/String;

    move-result-object v4

    :goto_4
    iget-object v5, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->msgstatus:Ljava/lang/String;

    if-nez v5, :cond_5

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->f()Ljava/lang/String;

    move-result-object v5

    :goto_5
    iget-object v6, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->photoloaded:Ljava/lang/String;

    if-nez v6, :cond_6

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->g()Ljava/lang/String;

    move-result-object v6

    :goto_6
    iget-object v7, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->status:Ljava/lang/String;

    if-nez v7, :cond_7

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->h()Ljava/lang/String;

    move-result-object v7

    :goto_7
    iget-object v8, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->url:Ljava/lang/String;

    if-nez v8, :cond_8

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->i()Ljava/lang/String;

    move-result-object v8

    :goto_8
    iget-object v9, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->weburl:Ljava/lang/String;

    if-nez v9, :cond_9

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->j()Ljava/lang/String;

    move-result-object v9

    :goto_9
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->k()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->tel:Ljava/lang/String;

    if-nez v11, :cond_a

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->l()Ljava/lang/String;

    move-result-object v11

    :goto_a
    invoke-static/range {v0 .. v11}, Lcom/sec/chaton/buddy/a/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v0

    .line 1830
    return-object v0

    .line 1827
    :cond_0
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->specialuserid:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->name:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->description:Ljava/lang/String;

    goto :goto_2

    :cond_3
    iget-object v3, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->followcount:Ljava/lang/String;

    goto :goto_3

    :cond_4
    iget-object v4, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->likecount:Ljava/lang/String;

    goto :goto_4

    :cond_5
    iget-object v5, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->msgstatus:Ljava/lang/String;

    goto :goto_5

    :cond_6
    iget-object v6, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->photoloaded:Ljava/lang/String;

    goto :goto_6

    :cond_7
    iget-object v7, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->status:Ljava/lang/String;

    goto :goto_7

    :cond_8
    iget-object v8, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->url:Ljava/lang/String;

    goto :goto_8

    :cond_9
    iget-object v9, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->weburl:Ljava/lang/String;

    goto :goto_9

    :cond_a
    iget-object v11, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->tel:Ljava/lang/String;

    goto :goto_a
.end method

.method public a()V
    .locals 2

    .prologue
    .line 1212
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j()V

    .line 1213
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1214
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;)Lcom/sec/chaton/d/a/h;

    .line 1215
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    .line 194
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 195
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 205
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 210
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "specialuserid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    .line 211
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "speicalusername"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l:Ljava/lang/String;

    .line 212
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 213
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/af;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p:Z

    .line 219
    :goto_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CURRENT_POSITION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->n:I

    .line 243
    :cond_0
    :goto_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CURRENT_POSITION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->n:I

    .line 247
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 250
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-static {v0, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->i:Landroid/app/ProgressDialog;

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->i:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 253
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j:Landroid/widget/Toast;

    .line 255
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->f:Lcom/sec/chaton/e/a/u;

    .line 256
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->e:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 258
    return-void

    .line 215
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p:Z

    goto/16 :goto_0

    .line 226
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "specialuserid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "speicalusername"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l:Ljava/lang/String;

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 229
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/af;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p:Z

    .line 235
    :goto_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CURRENT_POSITION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->n:I

    goto/16 :goto_1

    .line 231
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p:Z

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 318
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 319
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    iput-object p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->aa:Landroid/view/Menu;

    .line 321
    const v0, 0x7f0f0032

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 322
    const v0, 0x7f0705b3

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ab:Landroid/view/MenuItem;

    .line 323
    const v0, 0x7f0705b4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ac:Landroid/view/MenuItem;

    .line 325
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Z)V

    .line 327
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 263
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ad:Landroid/widget/LinearLayout;

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ad:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 265
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 266
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ad:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 268
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d()V

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->ad:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->e:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 312
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k()V

    .line 313
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 314
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    .line 200
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 201
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 331
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 333
    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 334
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 354
    :cond_0
    :goto_0
    return v1

    .line 336
    :pswitch_0
    iget-boolean v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    if-eqz v2, :cond_0

    .line 337
    iget-boolean v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    if-nez v2, :cond_1

    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    .line 338
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/h;->f(Ljava/lang/String;)V

    .line 339
    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Z)V

    .line 340
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Z)V

    goto :goto_0

    .line 344
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    if-nez v2, :cond_0

    .line 345
    iget-boolean v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    .line 346
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iget-object v2, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/h;->e(Ljava/lang/String;)V

    .line 347
    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Z)V

    .line 348
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Z)V

    goto :goto_0

    .line 334
    nop

    :pswitch_data_0
    .packed-switch 0x7f0705b3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 276
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 288
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 292
    :cond_0
    invoke-direct {p0, v7}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v3, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d:Landroid/os/Handler;

    invoke-direct {v0, v3}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 294
    iget-object v3, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v4}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;II)V

    .line 297
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p:Z

    if-eqz v0, :cond_2

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->f:Lcom/sec/chaton/e/a/u;

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const-string v5, "buddy_no = ? "

    new-array v6, v1, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->Y:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    invoke-direct {p0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m()V

    .line 307
    return-void

    .line 302
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->f:Lcom/sec/chaton/e/a/u;

    const/4 v4, 0x4

    sget-object v3, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v5, "buddy_no = ? "

    new-array v6, v1, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k:Ljava/lang/String;

    aput-object v1, v6, v7

    move v1, v4

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
