.class Lcom/sec/chaton/buddy/di;
.super Landroid/os/Handler;
.source "BuddyGroupDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dc;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dc;)V
    .locals 0

    .prologue
    .line 542
    iput-object p1, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 545
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 547
    iget-object v0, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 591
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->c(Lcom/sec/chaton/buddy/dc;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 551
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 553
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v2, v3, :cond_2

    .line 554
    iget-object v0, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->f(Lcom/sec/chaton/buddy/dc;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 555
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NEW GROUP NAME : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/buddy/dc;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MEMBER COUNT  : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dc;->f(Lcom/sec/chaton/buddy/dc;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/buddy/dc;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    new-instance v0, Lcom/sec/chaton/e/b/j;

    iget-object v2, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dc;->e:Lcom/sec/chaton/e/b/d;

    iget-object v3, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dc;->f(Lcom/sec/chaton/buddy/dc;)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/sec/chaton/e/b/j;-><init>(Lcom/sec/chaton/e/b/d;Ljava/lang/String;[Ljava/lang/String;I)V

    .line 561
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    invoke-static {v1, v4, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    goto/16 :goto_0

    .line 568
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dc;->g(Lcom/sec/chaton/buddy/dc;)V

    .line 569
    iget-object v2, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/o;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 570
    iget-object v0, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0122

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0139

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/buddy/dj;

    invoke-direct {v3, p0, v1}, Lcom/sec/chaton/buddy/dj;-><init>(Lcom/sec/chaton/buddy/di;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/di;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0
.end method
