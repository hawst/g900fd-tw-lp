.class Lcom/sec/chaton/buddy/ac;
.super Ljava/lang/Object;
.source "AddBuddyFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/AddBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 0

    .prologue
    .line 591
    iput-object p1, p0, Lcom/sec/chaton/buddy/ac;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 595
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    .line 596
    const/16 v1, 0x42

    if-eq p2, v1, :cond_0

    const/16 v1, 0x17

    if-ne p2, v1, :cond_2

    .line 597
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/ac;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a()V

    .line 598
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 599
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    const-string v2, "00030023"

    invoke-virtual {v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 601
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/ac;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->c(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/widget/AdaptableEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/widget/AdaptableEditText;->requestFocus()Z

    .line 602
    iget-object v1, p0, Lcom/sec/chaton/buddy/ac;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/view/View;)V

    .line 606
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
