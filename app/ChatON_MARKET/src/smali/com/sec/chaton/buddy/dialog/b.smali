.class Lcom/sec/chaton/buddy/dialog/b;
.super Landroid/os/Handler;
.source "BuddyDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V
    .locals 0

    .prologue
    .line 1563
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    .line 1566
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1567
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 1568
    const-string v0, "[BuddyDialog] mContext is null "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1951
    :cond_1
    :goto_0
    return-void

    .line 1575
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1577
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1578
    if-eqz v0, :cond_1

    .line 1582
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_3

    .line 1584
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BuddyProfile;

    .line 1585
    if-eqz v0, :cond_1

    .line 1595
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    goto :goto_0

    .line 1600
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1602
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1604
    if-nez v0, :cond_4

    .line 1605
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 1606
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1607
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 1612
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_20

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_20

    .line 1614
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetProfileImageList;

    .line 1615
    if-nez v0, :cond_5

    .line 1616
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 1617
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1618
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1623
    :cond_5
    iget-object v11, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->profileimagelist:Ljava/util/ArrayList;

    .line 1624
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->listcount:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;I)I

    .line 1628
    if-eqz v11, :cond_6

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 1629
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b()V

    .line 1630
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 1631
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1632
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1637
    :cond_7
    if-eqz v11, :cond_9

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_9

    .line 1638
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;I)I

    .line 1639
    const/4 v2, 0x0

    .line 1640
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_31

    .line 1641
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    .line 1642
    const-string v3, "1"

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1643
    const/4 v0, 0x1

    .line 1649
    :goto_2
    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I

    move-result v0

    if-nez v0, :cond_9

    .line 1650
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b()V

    .line 1651
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 1652
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1653
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1640
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1661
    :cond_9
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1663
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(I)V

    .line 1666
    const/4 v0, 0x0

    move v9, v0

    :goto_3
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v9, v0, :cond_11

    .line 1667
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    .line 1668
    const-string v0, "1"

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1670
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Profile URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1672
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/f;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1676
    if-eqz v0, :cond_a

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->update_msec:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1677
    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->update_msec:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1678
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "***********Timestamp unmatch need to reload main image : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->update_msec:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TIME"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1679
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    iget-object v2, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1680
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Ljava/lang/String;)V

    .line 1687
    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 1688
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I

    .line 1689
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    iget-object v2, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    new-instance v0, Lcom/sec/chaton/buddy/fk;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->g(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x1f4

    const/16 v4, 0x1f4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v6}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v6, v6, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/buddy/fk;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    .line 1691
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->j(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1692
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1699
    :cond_c
    :goto_5
    iget-object v0, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->update_msec:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1700
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    iget-object v2, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->update_msec:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/f;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1711
    :cond_d
    :goto_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Ljava/lang/String;)V

    .line 1763
    :cond_e
    :goto_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a()V

    .line 1666
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto/16 :goto_3

    .line 1682
    :cond_f
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "***********Timestamp match : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->update_msec:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TIME"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_4

    .line 1702
    :catch_0
    move-exception v0

    .line 1703
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    .line 1707
    :catchall_0
    move-exception v0

    throw v0

    .line 1693
    :cond_10
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1694
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1695
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1696
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_5

    .line 1704
    :catch_1
    move-exception v0

    .line 1705
    :try_start_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1766
    :cond_11
    :goto_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v10}, Lcom/sec/chaton/util/f;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1768
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I

    move-result v0

    if-nez v0, :cond_12

    .line 1769
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 1772
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1773
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->d(Ljava/lang/String;)V

    .line 1776
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I

    move-result v0

    const/4 v1, 0x4

    if-le v0, v1, :cond_1

    .line 1777
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->t(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 1713
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Profile URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1715
    const/4 v0, 0x1

    if-ne v9, v0, :cond_18

    .line 1716
    :try_start_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 1717
    :cond_15
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I

    .line 1718
    new-instance v0, Lcom/sec/chaton/buddy/fk;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3c

    const/16 v4, 0x3c

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v6}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v6, v6, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/buddy/fk;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 1719
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1720
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "1:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1726
    :cond_16
    :goto_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_7

    .line 1752
    :catch_2
    move-exception v0

    .line 1753
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_7

    .line 1757
    :catchall_1
    move-exception v0

    throw v0

    .line 1721
    :cond_17
    :try_start_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1722
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1723
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1724
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "1:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_9

    .line 1754
    :catch_3
    move-exception v0

    .line 1755
    :try_start_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_8

    .line 1727
    :cond_18
    const/4 v0, 0x2

    if-ne v9, v0, :cond_1c

    .line 1728
    :try_start_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 1729
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I

    .line 1730
    new-instance v0, Lcom/sec/chaton/buddy/fk;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3c

    const/16 v4, 0x3c

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v6}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v6, v6, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/buddy/fk;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 1731
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1732
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1738
    :cond_1a
    :goto_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1733
    :cond_1b
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1734
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1735
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1736
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 1739
    :cond_1c
    const/4 v0, 0x3

    if-ne v9, v0, :cond_e

    .line 1740
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 1741
    :cond_1d
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I

    .line 1742
    new-instance v0, Lcom/sec/chaton/buddy/fk;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->r(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3c

    const/16 v4, 0x3c

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v6}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v6, v6, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/buddy/fk;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 1743
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1744
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "3:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1750
    :cond_1e
    :goto_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1745
    :cond_1f
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1746
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1747
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1748
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "3:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_b

    .line 1782
    :cond_20
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_21

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_21

    .line 1783
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b()V

    .line 1786
    :cond_21
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 1787
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->d(Ljava/lang/String;)V

    .line 1789
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I

    move-result v0

    const/4 v1, 0x4

    if-le v0, v1, :cond_1

    .line 1790
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->t(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 1797
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1801
    const-string v1, "CoverStoryControl.METHOD_GET_COVERSTORY"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1802
    if-nez v0, :cond_22

    .line 1803
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 1804
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY (httpEntry == null)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1810
    :cond_22
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2a

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_2a

    .line 1812
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/io/entry/inner/CoverStory;

    .line 1813
    if-nez v1, :cond_23

    .line 1815
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY dialogCoverStory is null !!!"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1819
    :cond_23
    iget-object v0, v1, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    if-eqz v0, :cond_27

    .line 1820
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v3, v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/coverstory/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 1823
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1825
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/d;->i(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1826
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CoverStoryControl.METHOD_GET_COVERSTORY previousMetaId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "### new MetaId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1828
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_25

    .line 1829
    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 1830
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "coverstory.jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1831
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_24

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_24

    .line 1832
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY Same coverstory~!!!"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1835
    :cond_24
    const-string v2, "CoverStoryControl.METHOD_GET_COVERSTORY Same metaid but there is no coverstoryfile~!!!"

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1840
    :cond_25
    :try_start_a
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/e/a/d;->c(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_4
    .catch Landroid/content/OperationApplicationException; {:try_start_a .. :try_end_a} :catch_5

    .line 1849
    :goto_c
    iget-object v0, v1, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    if-eqz v0, :cond_26

    .line 1851
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v3, v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1852
    if-nez v0, :cond_1

    .line 1853
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->v(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/i;

    move-result-object v0

    iget-object v2, v1, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0902b7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0902b8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1855
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 1841
    :catch_4
    move-exception v0

    .line 1843
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_c

    .line 1844
    :catch_5
    move-exception v0

    .line 1846
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_c

    .line 1860
    :cond_26
    new-instance v0, Lcom/sec/chaton/poston/a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/CoverStory;->host:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/CoverStory;->metacontents:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "buddy"

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 1862
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->w(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 1866
    :cond_27
    const-string v0, "Buddy didn\'t set Coverstory "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1867
    invoke-static {}, Lcom/sec/chaton/e/a/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 1868
    if-eqz v0, :cond_29

    .line 1869
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 1870
    if-nez v1, :cond_28

    .line 1871
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->v(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/i;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0902b7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0902b8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1873
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 1876
    :cond_28
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Set RandomCoverStory randomId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1878
    :cond_29
    const-string v0, " Random ERROR !!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1882
    :cond_2a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_2b

    .line 1885
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 1886
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY NO_CONTENT"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1890
    :cond_2b
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2c

    .line 1891
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY network ERROR"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1895
    :cond_2c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1896
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "coverstory.jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1897
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_2d

    .line 1898
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CoverStoryControl.METHOD_GET_COVERSTORY mBuddyCoverStoryFile : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "coverstory.jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1900
    :cond_2d
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1901
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Ljava/lang/String;)Z

    move-result v0

    .line 1902
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1903
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CoverStoryControl.METHOD_GET_COVERSTORY result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1913
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1919
    if-nez v0, :cond_2e

    .line 1920
    const-string v0, "CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD (httpEntry == null)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1923
    :cond_2e
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2f

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2f

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2f

    .line 1924
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;

    .line 1925
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v3, v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/coverstory/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 1926
    const-string v6, "coverstory.jpg"

    .line 1928
    new-instance v0, Lcom/sec/chaton/poston/a;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;->fileurl:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    sget-object v3, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1929
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->w(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1930
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1932
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 1936
    :cond_2f
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_1

    .line 1939
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Deleted item on BuddyDialog mContentId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1940
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/h;->b(Ljava/lang/String;)V

    .line 1941
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 1943
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 1945
    :cond_30
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/b;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c()Z

    goto/16 :goto_0

    :cond_31
    move v0, v2

    goto/16 :goto_2

    .line 1575
    nop

    :sswitch_data_0
    .sparse-switch
        0x19b -> :sswitch_0
        0x19c -> :sswitch_1
        0xbb9 -> :sswitch_2
        0xbbd -> :sswitch_3
    .end sparse-switch
.end method
