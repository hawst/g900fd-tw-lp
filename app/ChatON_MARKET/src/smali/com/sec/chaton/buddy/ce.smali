.class Lcom/sec/chaton/buddy/ce;
.super Landroid/database/ContentObserver;
.source "BuddyFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 8696
    iput-object p1, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 8699
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContentObserver.isShow() = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->C(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", selfChange = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 8700
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8738
    :cond_0
    :goto_0
    return-void

    .line 8703
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->X(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/ChoicePanel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 8705
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->X(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/ChoicePanel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/ChoicePanel;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 8711
    :cond_2
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContentObserver.isShow() = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->C(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Entered Observer"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 8712
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    iput-boolean v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->h:Z

    .line 8713
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->C(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 8714
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 8715
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->d()V

    .line 8716
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->c()V

    .line 8717
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->b()V

    .line 8719
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 8721
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->e()V

    .line 8726
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->Z(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/ag;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 8727
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->G(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 8734
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 8735
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->e()V

    .line 8736
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->as(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->b(I)V

    goto/16 :goto_0

    .line 8706
    :catch_0
    move-exception v0

    .line 8708
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "choice panel invalidate exception : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 8731
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Lcom/sec/chaton/buddy/BuddyFragment;Z)Z

    .line 8732
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isChanged : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ce;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->aN(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
