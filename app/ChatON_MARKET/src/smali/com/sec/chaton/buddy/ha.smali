.class Lcom/sec/chaton/buddy/ha;
.super Ljava/lang/Object;
.source "BuddyRecommendListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 338
    instance-of v0, p2, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    if-nez v0, :cond_0

    .line 359
    :goto_0
    return-void

    .line 341
    :cond_0
    new-instance v0, Lcom/sec/chaton/buddy/hg;

    invoke-direct {v0, p2}, Lcom/sec/chaton/buddy/hg;-><init>(Landroid/view/View;)V

    .line 342
    iget-object v1, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    iget-object v2, v0, Lcom/sec/chaton/buddy/hg;->c:Landroid/widget/ImageView;

    const v3, 0x7f07000e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 343
    iget-object v1, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    iget-object v0, v0, Lcom/sec/chaton/buddy/hg;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 345
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 346
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 347
    const-string v1, "PROFILE_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    const-string v1, "PROFILE_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 349
    const-string v1, "PROFILE_BUDDY_SUGGESTION"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 350
    iget-object v1, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 353
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 354
    const-string v1, "BUDDY_DIALOG_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    const-string v1, "BUDDY_DIALOG_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 356
    const-string v1, "BUDDY_DIALOG_BUDDY_FROM_SUGGESTION"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 357
    iget-object v1, p0, Lcom/sec/chaton/buddy/ha;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
