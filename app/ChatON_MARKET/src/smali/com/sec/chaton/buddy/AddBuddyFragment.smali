.class public Lcom/sec/chaton/buddy/AddBuddyFragment;
.super Landroid/support/v4/app/Fragment;
.source "AddBuddyFragment.java"


# static fields
.field private static f:I


# instance fields
.field private A:Landroid/widget/Button;

.field private B:Landroid/view/View;

.field private C:Landroid/view/View;

.field private D:Landroid/view/View;

.field private E:Landroid/view/View;

.field private F:Landroid/view/View;

.field private G:Landroid/view/ViewGroup;

.field private H:Landroid/view/ViewGroup;

.field private I:Landroid/view/ViewGroup;

.field private J:Landroid/view/ViewGroup;

.field private K:Landroid/widget/TextView;

.field private L:Landroid/widget/TextView;

.field private M:Landroid/widget/TextView;

.field private final N:I

.field private final O:I

.field private P:Landroid/widget/ImageButton;

.field private Q:[Ljava/lang/String;

.field private R:Lcom/sec/chaton/buddy/ae;

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/String;

.field private final U:Ljava/lang/String;

.field private V:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

.field private W:Lcom/sec/chaton/buddy/TellFriendsFragment;

.field private X:Landroid/app/Activity;

.field private Y:Ljava/lang/String;

.field private Z:Landroid/text/TextWatcher;

.field final a:I

.field private aa:Landroid/text/TextWatcher;

.field private ab:Landroid/text/TextWatcher;

.field b:Landroid/database/ContentObserver;

.field c:Landroid/view/View$OnClickListener;

.field d:Landroid/os/Handler;

.field e:Landroid/widget/AdapterView$OnItemClickListener;

.field private g:I

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private k:[Ljava/lang/CharSequence;

.field private l:[Ljava/lang/CharSequence;

.field private m:[Ljava/lang/CharSequence;

.field private n:Ljava/lang/String;

.field private o:Landroid/view/View;

.field private p:Lcom/sec/chaton/widget/AdaptableEditText;

.field private q:Landroid/widget/ImageButton;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/view/View;

.field private t:Lcom/sec/chaton/widget/AdaptableEditText;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/app/ProgressDialog;

.field private w:Landroid/widget/Button;

.field private x:Landroid/widget/Button;

.field private y:Landroid/widget/Button;

.field private z:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 108
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->a:I

    .line 130
    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->v:Landroid/app/ProgressDialog;

    .line 154
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->N:I

    .line 155
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->O:I

    .line 163
    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->R:Lcom/sec/chaton/buddy/ae;

    .line 166
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "country_letter"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->U:Ljava/lang/String;

    .line 168
    new-instance v0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->V:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    .line 169
    new-instance v0, Lcom/sec/chaton/buddy/TellFriendsFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/TellFriendsFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->W:Lcom/sec/chaton/buddy/TellFriendsFragment;

    .line 201
    new-instance v0, Lcom/sec/chaton/buddy/a;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/a;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->b:Landroid/database/ContentObserver;

    .line 1147
    new-instance v0, Lcom/sec/chaton/buddy/j;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/j;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->c:Landroid/view/View$OnClickListener;

    .line 1496
    new-instance v0, Lcom/sec/chaton/buddy/p;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/p;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->d:Landroid/os/Handler;

    .line 1640
    new-instance v0, Lcom/sec/chaton/buddy/t;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/t;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Z:Landroid/text/TextWatcher;

    .line 1667
    new-instance v0, Lcom/sec/chaton/buddy/u;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/u;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->aa:Landroid/text/TextWatcher;

    .line 1711
    new-instance v0, Lcom/sec/chaton/buddy/v;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/v;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->ab:Landroid/text/TextWatcher;

    .line 1872
    new-instance v0, Lcom/sec/chaton/buddy/w;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/w;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->e:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1989
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/buddy/ae;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->R:Lcom/sec/chaton/buddy/ae;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/AddBuddyFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->T:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1349
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->F:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1350
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->F:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1353
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->z:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 1354
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->z:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 1357
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 1358
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->z:Landroid/widget/Button;

    .line 1359
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->C:Landroid/view/View;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->F:Landroid/view/View;

    .line 1360
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->G:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    .line 1374
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->z:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 1375
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->F:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1376
    return-void

    .line 1361
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->y:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 1362
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->y:Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->z:Landroid/widget/Button;

    .line 1363
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->D:Landroid/view/View;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->F:Landroid/view/View;

    .line 1364
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->H:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    goto :goto_0

    .line 1365
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->w:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 1366
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->w:Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->z:Landroid/widget/Button;

    .line 1367
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->B:Landroid/view/View;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->F:Landroid/view/View;

    .line 1368
    iput-object v3, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    goto :goto_0

    .line 1370
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->z:Landroid/widget/Button;

    .line 1371
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->E:Landroid/view/View;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->F:Landroid/view/View;

    .line 1372
    iput-object v3, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    goto :goto_0
.end method

.method private a(Landroid/widget/EditText;Z)V
    .locals 3

    .prologue
    .line 1622
    if-nez p1, :cond_1

    .line 1638
    :cond_0
    :goto_0
    return-void

    .line 1627
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1631
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1633
    if-eqz p2, :cond_3

    .line 1634
    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0

    .line 1636
    :cond_3
    invoke-virtual {p1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/widget/EditText;Z)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/widget/EditText;Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/AddBuddyFragment;Lcom/sec/chaton/io/entry/GetBuddyList;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/io/entry/GetBuddyList;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/AddBuddyFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/AddBuddyFragment;Z)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Z)V

    return-void
.end method

.method private a(Lcom/sec/chaton/io/entry/GetBuddyList;)V
    .locals 12

    .prologue
    const v11, 0x7f07014c

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1379
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1380
    if-eqz p1, :cond_4

    iget-object v0, p1, Lcom/sec/chaton/io/entry/GetBuddyList;->buddy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 1381
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030088

    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1384
    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1385
    const v1, 0x7f0b01b8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1386
    new-instance v1, Lcom/sec/chaton/buddy/k;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/k;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1397
    iget-object v0, p1, Lcom/sec/chaton/io/entry/GetBuddyList;->buddy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Buddy;

    .line 1401
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030120

    iget-object v4, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v4, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 1403
    const v1, 0x7f07014b

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1404
    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-virtual {v2, v1, v5}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 1405
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 1407
    invoke-virtual {v4, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1408
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1409
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1410
    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1411
    new-instance v5, Lcom/sec/chaton/buddy/m;

    invoke-direct {v5, p0, v0}, Lcom/sec/chaton/buddy/m;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;Lcom/sec/chaton/io/entry/inner/Buddy;)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1422
    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    .line 1423
    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    .line 1424
    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    .line 1425
    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v7

    .line 1428
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v8

    if-nez v8, :cond_3

    .line 1429
    const v8, 0x7f0202dc

    invoke-virtual {v4, v8}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1430
    invoke-virtual {v4, v2, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 1436
    :goto_1
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1437
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->S:Ljava/lang/String;

    .line 1440
    :cond_1
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1441
    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->T:Ljava/lang/String;

    .line 1445
    :cond_2
    new-instance v0, Lcom/sec/chaton/buddy/n;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/n;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1466
    const v0, 0x7f0702e1

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1467
    const v2, 0x7f02028d

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1468
    const v1, 0x7f0202cf

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1469
    const v1, 0x7f020020

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1470
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1471
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1472
    new-instance v1, Lcom/sec/chaton/buddy/o;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/o;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1483
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1487
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1488
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030080

    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto/16 :goto_0

    .line 1432
    :cond_3
    invoke-virtual {v4, v10, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    .line 1493
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1494
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 978
    :try_start_0
    invoke-static {p1, p2}, Lcom/sec/common/util/i;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 979
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 990
    :goto_0
    return-void

    .line 980
    :catch_0
    move-exception v0

    .line 981
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 982
    const v1, 0x7f0b01fa

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01f9

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/i;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/i;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 996
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 997
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 999
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Q:[Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1000
    invoke-direct {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->e()I

    move-result v0

    const v1, 0x7f070210

    if-ne v0, v1, :cond_1

    .line 1001
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Z)V

    .line 1008
    :goto_0
    return-void

    .line 1003
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->c(Z)V

    goto :goto_0

    .line 1006
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->d(Z)V

    goto :goto_0
.end method

.method static synthetic b()I
    .locals 1

    .prologue
    .line 88
    sget v0, Lcom/sec/chaton/buddy/AddBuddyFragment;->f:I

    return v0
.end method

.method static synthetic b(I)I
    .locals 0

    .prologue
    .line 88
    sput p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->f:I

    return p0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    return-object v0
.end method

.method private b(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1026
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1027
    const-string v0, "EditText Phone is empty"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1081
    :goto_0
    return-void

    .line 1030
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->v:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1032
    new-instance v4, Lcom/sec/chaton/d/h;

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->d:Landroid/os/Handler;

    invoke-direct {v4, v0}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1052
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1053
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->U:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1054
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->j:Ljava/util/Map;

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->U:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    .line 1055
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1056
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/AdaptableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1068
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, "+"

    :goto_2
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Y:Ljava/lang/String;

    .line 1069
    if-eqz p1, :cond_6

    .line 1070
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    if-eqz v1, :cond_5

    move v1, v2

    :goto_3
    invoke-virtual {v4, v0, v1}, Lcom/sec/chaton/d/h;->c(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1058
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1061
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1064
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/AdaptableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1065
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addBuddyInternalByPhone orgnum="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", country code="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1068
    :cond_4
    const-string v1, ""

    goto/16 :goto_2

    :cond_5
    move v1, v3

    .line 1070
    goto :goto_3

    .line 1076
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    if-eqz v1, :cond_7

    :goto_4
    invoke-virtual {v4, v0, v2}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;Z)Lcom/sec/chaton/d/a/h;

    goto/16 :goto_0

    :cond_7
    move v2, v3

    goto :goto_4
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/widget/AdaptableEditText;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 953
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 954
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->f()V

    .line 958
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1086
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1087
    const-string v0, "EditText Id is empty"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    :goto_0
    return-void

    .line 1090
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->v:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1091
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1092
    new-instance v1, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->d:Landroid/os/Handler;

    invoke-direct {v1, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1093
    const-string v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1094
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0296

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1095
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->v:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1096
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 1102
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addBuddyInternalById orgnum="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Y:Ljava/lang/String;

    .line 1104
    if-eqz p1, :cond_3

    .line 1105
    invoke-virtual {v1, v0, v4}, Lcom/sec/chaton/d/h;->c(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1111
    :cond_3
    invoke-virtual {v1, v0, v4}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;Z)Lcom/sec/chaton/d/a/h;

    goto/16 :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/widget/AdaptableEditText;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 961
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 962
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->e()V

    .line 966
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1121
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->v:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1122
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Q:[Ljava/lang/String;

    aget-object v0, v0, v4

    .line 1123
    new-instance v1, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->d:Landroid/os/Handler;

    invoke-direct {v1, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1125
    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->U:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1126
    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->j:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->U:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    .line 1129
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addBuddyInternalFromIntent orgnum="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", country code="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1130
    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Y:Ljava/lang/String;

    .line 1132
    if-eqz p1, :cond_1

    .line 1133
    invoke-virtual {v1, v0, v4}, Lcom/sec/chaton/d/h;->c(Ljava/lang/String;Z)V

    .line 1144
    :goto_0
    return-void

    .line 1139
    :cond_1
    invoke-virtual {v1, v0, v4}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;Z)Lcom/sec/chaton/d/a/h;

    goto :goto_0
.end method

.method private e()I
    .locals 1

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->w:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1336
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->w:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getId()I

    move-result v0

    .line 1344
    :goto_0
    return v0

    .line 1337
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1338
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getId()I

    move-result v0

    goto :goto_0

    .line 1339
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1340
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getId()I

    move-result v0

    goto :goto_0

    .line 1341
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->y:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1342
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->y:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getId()I

    move-result v0

    goto :goto_0

    .line 1344
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/AddBuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->S:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/AddBuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->T:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->v:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/AddBuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/AddBuddyFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Q:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->c()V

    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->d()V

    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->q:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->P:Landroid/widget/ImageButton;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1616
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/widget/EditText;Z)V

    .line 1617
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/widget/EditText;Z)V

    .line 1619
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->u:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1012
    if-lez p1, :cond_2

    .line 1013
    const/16 v0, 0x64

    if-lt p1, v0, :cond_1

    .line 1014
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->u:Landroid/widget/TextView;

    const v1, 0x7f0b005a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1018
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->u:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1023
    :cond_0
    :goto_1
    return-void

    .line 1016
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->u:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1020
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->u:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v7, 0x0

    .line 806
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0028

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "\nwww.chaton.com/invite.html"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 808
    if-eq p2, v4, :cond_1

    .line 929
    :cond_0
    :goto_0
    return-void

    .line 811
    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 855
    :sswitch_0
    if-ne p2, v4, :cond_0

    .line 856
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 861
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 862
    if-eqz v6, :cond_4

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 863
    const-string v0, "display_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 864
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 865
    const-string v0, "has_phone_number"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 867
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 870
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "contact_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 872
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 873
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 874
    const-string v2, "data1"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 876
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 906
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 907
    :goto_2
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 909
    if-eqz v1, :cond_0

    .line 910
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 814
    :sswitch_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 816
    const-string v1, "PARAMS_COUNTRY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 817
    if-eqz v1, :cond_2

    .line 818
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 819
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    .line 832
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->r:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 833
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[addBuddy] mCountryCallingCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " selectedCountryName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 845
    :cond_2
    const-string v1, "PARAMS_COUNTRY_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 846
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->r:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 847
    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 878
    :cond_3
    :try_start_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v10, :cond_5

    .line 879
    invoke-direct {p0, v7, v8}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 909
    :cond_4
    :goto_3
    if-eqz v6, :cond_0

    .line 910
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 881
    :cond_5
    :try_start_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    new-instance v3, Lcom/sec/chaton/buddy/g;

    invoke-direct {v3, p0, v1, v8}, Lcom/sec/chaton/buddy/g;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 909
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v6, :cond_6

    .line 910
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 909
    :cond_6
    throw v0

    .line 891
    :cond_7
    :try_start_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 892
    const v1, 0x7f0b013b

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/h;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/h;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 917
    :sswitch_2
    if-ne p2, v4, :cond_0

    .line 918
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 919
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->I:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    .line 909
    :catchall_1
    move-exception v0

    move-object v6, v7

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v6, v1

    goto :goto_4

    .line 906
    :catch_1
    move-exception v0

    move-object v1, v7

    goto/16 :goto_2

    .line 811
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x10 -> :sswitch_1
    .end sparse-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    .line 192
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 193
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 933
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 934
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 935
    invoke-direct {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->d()V

    .line 943
    :cond_0
    :goto_0
    return-void

    .line 936
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 937
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 938
    invoke-direct {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->c()V

    goto :goto_0

    .line 939
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 940
    invoke-direct {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->c()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/16 v7, 0x66

    const/16 v6, 0x65

    const/16 v5, 0x64

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 292
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ADD_BUDDY_TYPE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v6, :cond_1

    .line 296
    iput v6, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->g:I

    .line 308
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 309
    new-array v1, v4, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Q:[Ljava/lang/String;

    .line 315
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->v:Landroid/app/ProgressDialog;

    .line 316
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->v:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 317
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->h:Ljava/util/Map;

    .line 318
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->i:Ljava/util/Map;

    .line 319
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->j:Ljava/util/Map;

    .line 320
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0d0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->k:[Ljava/lang/CharSequence;

    .line 321
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->l:[Ljava/lang/CharSequence;

    .line 322
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->m:[Ljava/lang/CharSequence;

    .line 323
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->k:[Ljava/lang/CharSequence;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 324
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->h:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->k:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->l:[Ljava/lang/CharSequence;

    aget-object v3, v3, v0

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->i:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->m:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->k:[Ljava/lang/CharSequence;

    aget-object v3, v3, v0

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->j:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->m:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->l:[Ljava/lang/CharSequence;

    aget-object v3, v3, v0

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 297
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ADD_BUDDY_TYPE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 298
    iput v5, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->g:I

    goto/16 :goto_0

    .line 299
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ADD_BUDDY_TYPE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x67

    if-ne v1, v2, :cond_3

    .line 300
    const/16 v1, 0x67

    iput v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->g:I

    goto/16 :goto_0

    .line 301
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ADD_BUDDY_TYPE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v7, :cond_4

    .line 302
    iput v7, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->g:I

    goto/16 :goto_0

    .line 304
    :cond_4
    iput v5, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->g:I

    goto/16 :goto_0

    .line 310
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, [Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 311
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Q:[Ljava/lang/String;

    goto/16 :goto_1

    .line 328
    :cond_6
    new-instance v0, Lcom/sec/chaton/buddy/ae;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ae;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->R:Lcom/sec/chaton/buddy/ae;

    .line 331
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 332
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 334
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->V:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    if-eqz v1, :cond_8

    .line 335
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "recomned_receive"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v4, :cond_7

    .line 336
    const v1, 0x7f070212

    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->V:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 338
    :cond_7
    const v1, 0x7f07021e

    iget-object v2, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->W:Lcom/sec/chaton/buddy/TellFriendsFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 339
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 342
    :cond_8
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const v10, 0x7f070216

    const/4 v9, -0x2

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 346
    const-string v0, "onCreateView..."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v7, :cond_0

    .line 349
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030020"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 352
    :cond_0
    const v0, 0x7f030054

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 363
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 365
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v5, "smsto:9000000000"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v0, v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 366
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 368
    const/high16 v5, 0x10000

    invoke-virtual {v1, v0, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v7, :cond_1

    move v1, v2

    .line 369
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 370
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const v5, 0x7f0b01f1

    invoke-virtual {p0, v5}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 371
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 387
    :cond_1
    new-instance v0, Lcom/sec/chaton/buddy/af;

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-direct {v0, p0, v1, v4}, Lcom/sec/chaton/buddy/af;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 407
    const v0, 0x7f070218

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->o:Landroid/view/View;

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->o:Landroid/view/View;

    const v1, 0x7f070519

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/AdaptableEditText;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    .line 409
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    new-array v1, v7, [Landroid/text/InputFilter;

    new-instance v4, Lcom/sec/chaton/util/w;

    iget-object v5, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    const/16 v6, 0x14

    invoke-direct {v4, v5, v6}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->o:Landroid/view/View;

    const v1, 0x7f07051a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->q:Landroid/widget/ImageButton;

    .line 412
    const v0, 0x7f07021c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->s:Landroid/view/View;

    .line 413
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->s:Landroid/view/View;

    const v1, 0x7f070519

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/AdaptableEditText;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    new-array v1, v7, [Landroid/text/InputFilter;

    new-instance v4, Lcom/sec/chaton/util/w;

    iget-object v5, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    const/16 v6, 0x40

    invoke-direct {v4, v5, v6}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 416
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->s:Landroid/view/View;

    const v1, 0x7f07051a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->P:Landroid/widget/ImageButton;

    .line 419
    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->r:Landroid/widget/TextView;

    .line 420
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 421
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->r:Landroid/widget/TextView;

    const v1, 0x7f02029a

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 423
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->r:Landroid/widget/TextView;

    const v1, 0x7f0b02f5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(I)V

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setInputType(I)V

    .line 434
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->P:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 436
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Z:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Z:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 441
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    const-string v1, "example@samsung.com"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 443
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 444
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v9, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 445
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/AdaptableEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 446
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/AdaptableEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 447
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0902eb

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v1

    invoke-direct {v0, v1, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 449
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->P:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 450
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 453
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    const/16 v1, 0xd1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setInputType(I)V

    .line 455
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    new-instance v1, Lcom/sec/chaton/buddy/l;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/l;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->P:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/buddy/x;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/x;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->q:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/buddy/y;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/y;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 502
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    new-instance v1, Lcom/sec/chaton/buddy/z;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/z;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    new-instance v1, Lcom/sec/chaton/buddy/aa;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/aa;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 547
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    new-instance v1, Lcom/sec/chaton/buddy/ab;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ab;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 571
    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 573
    const v0, 0x7f07020c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->J:Landroid/view/ViewGroup;

    .line 574
    const v0, 0x7f07020d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->w:Landroid/widget/Button;

    .line 575
    const v0, 0x7f07020e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->u:Landroid/widget/TextView;

    .line 578
    const v0, 0x7f07020f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    .line 579
    const v0, 0x7f070210

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    .line 580
    const v0, 0x7f070211

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->y:Landroid/widget/Button;

    .line 586
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->w:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 587
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 589
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->y:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/buddy/ac;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ac;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 610
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->y:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/buddy/ad;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ad;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 628
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    new-instance v1, Lcom/sec/chaton/buddy/b;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/b;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 638
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    new-instance v1, Lcom/sec/chaton/buddy/c;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/c;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 649
    const v0, 0x7f070212

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->B:Landroid/view/View;

    .line 650
    const v0, 0x7f07021e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->E:Landroid/view/View;

    .line 651
    const v0, 0x7f070213

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->C:Landroid/view/View;

    .line 652
    const v0, 0x7f07021a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->D:Landroid/view/View;

    .line 654
    const v0, 0x7f070219

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->G:Landroid/view/ViewGroup;

    .line 655
    const v0, 0x7f07021d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->H:Landroid/view/ViewGroup;

    .line 657
    const v0, 0x7f070215

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->K:Landroid/widget/TextView;

    .line 658
    const v0, 0x7f070217

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->L:Landroid/widget/TextView;

    .line 659
    const v0, 0x7f07021b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->M:Landroid/widget/TextView;

    .line 661
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->K:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/buddy/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/d;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 672
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->L:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/buddy/e;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/e;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 683
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->M:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/buddy/f;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/f;-><init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 728
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 730
    if-nez v0, :cond_4

    .line 731
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->J:Landroid/view/ViewGroup;

    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 733
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getPaddingTop()I

    move-result v1

    .line 734
    iget-object v4, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getPaddingBottom()I

    move-result v4

    .line 735
    iget-object v5, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    const v6, 0x7f020405

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 736
    iget-object v5, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    invoke-virtual {v5, v2, v1, v2, v4}, Landroid/widget/Button;->setPadding(IIII)V

    .line 744
    :cond_4
    if-eqz p3, :cond_5

    .line 745
    const-string v1, "current_tab"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 748
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v1, v2, :cond_8

    .line 749
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/view/View;)V

    .line 771
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->Q:[Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 772
    const v0, 0x7f07020b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 773
    const v0, 0x7f070214

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 777
    invoke-direct {p0, v7}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Z)V

    .line 778
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/view/View;)V

    .line 782
    :cond_6
    new-instance v0, Lcom/sec/chaton/d/h;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->b()V

    .line 783
    return-object v3

    .line 369
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 750
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->y:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v1, v2, :cond_9

    .line 751
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->y:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/view/View;)V

    goto :goto_1

    .line 752
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v1, v2, :cond_a

    .line 753
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/view/View;)V

    goto :goto_1

    .line 755
    :cond_a
    iget v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->g:I

    const/16 v2, 0x64

    if-ne v1, v2, :cond_b

    .line 756
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->x:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/view/View;)V

    goto :goto_1

    .line 758
    :cond_b
    iget v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->g:I

    const/16 v2, 0x67

    if-ne v1, v2, :cond_c

    .line 759
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/view/View;)V

    goto :goto_1

    .line 763
    :cond_c
    if-eqz v0, :cond_d

    .line 764
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->w:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/view/View;)V

    goto :goto_1

    .line 766
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->A:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/view/View;)V

    goto :goto_1
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    .line 198
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 199
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 791
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 792
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->b:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 793
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 795
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a()V

    .line 796
    return-void
.end method

.method public onResume()V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v10, 0x1

    .line 213
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 214
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 216
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->R:Lcom/sec/chaton/buddy/ae;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/ae;->a()V

    .line 218
    iget-object v1, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->b:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v10, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 219
    if-ne v0, v10, :cond_3

    .line 224
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->X:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type=\'200\' AND timestamp>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "recommend_timestamp"

    const-wide/16 v8, 0x0

    invoke-virtual {v4, v5, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 227
    if-eqz v1, :cond_8

    .line 228
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 232
    :goto_0
    if-eqz v1, :cond_0

    .line 233
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 237
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(I)V

    .line 267
    :goto_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 268
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 269
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a()V

    .line 288
    :cond_1
    :goto_2
    return-void

    .line 232
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_2

    .line 233
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 232
    :cond_2
    throw v0

    .line 239
    :cond_3
    invoke-virtual {p0, v7}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(I)V

    goto :goto_1

    .line 271
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-direct {p0, v0, v10}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/widget/EditText;Z)V

    .line 274
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-direct {p0, v0, v10}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/widget/EditText;Z)V

    goto :goto_2

    .line 280
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->p:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-direct {p0, v0, v10}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/widget/EditText;Z)V

    .line 283
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/buddy/AddBuddyFragment;->t:Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-direct {p0, v0, v10}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Landroid/widget/EditText;Z)V

    goto :goto_2

    .line 232
    :catchall_1
    move-exception v0

    goto :goto_3

    :cond_8
    move v0, v7

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 800
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 801
    const-string v0, "current_tab"

    invoke-direct {p0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->e()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 802
    return-void
.end method
