.class public Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;
.super Landroid/support/v4/app/Fragment;
.source "BuddyGroupProfileFragment.java"

# interfaces
.implements Lcom/sec/chaton/buddy/db;
.implements Lcom/sec/chaton/buddy/dq;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/ImageButton;

.field private B:Ljava/io/File;

.field private C:Ljava/io/File;

.field private D:Ljava/lang/String;

.field private E:Z

.field private F:Z

.field private G:Landroid/net/Uri;

.field private H:Landroid/widget/Toast;

.field private I:Z

.field private J:Landroid/app/ProgressDialog;

.field private K:[Ljava/lang/String;

.field private L:Landroid/widget/ImageView;

.field private M:Ljava/io/File;

.field private N:Lcom/coolots/sso/a/a;

.field private O:Landroid/app/Activity;

.field private P:Z

.field private Q:Landroid/os/Handler;

.field private R:Landroid/view/View$OnTouchListener;

.field public b:I

.field c:Ljava/lang/String;

.field d:Lcom/sec/chaton/e/b/d;

.field e:Landroid/view/View$OnClickListener;

.field f:Lcom/sec/chaton/e/a/v;

.field private g:Lcom/sec/chaton/buddy/a/b;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private n:Landroid/content/Context;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/ImageButton;

.field private q:Landroid/widget/Button;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/view/View;

.field private u:Ljava/lang/String;

.field private v:Lcom/sec/chaton/e/a/u;

.field private w:Landroid/widget/Toast;

.field private x:Landroid/widget/ImageButton;

.field private y:Landroid/widget/ImageButton;

.field private z:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 92
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 110
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h:I

    .line 111
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i:I

    .line 112
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->j:I

    .line 113
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->k:I

    .line 114
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->l:I

    .line 115
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->m:I

    .line 141
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->c:Ljava/lang/String;

    .line 142
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/profile/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->B:Ljava/io/File;

    .line 144
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->D:Ljava/lang/String;

    .line 145
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->E:Z

    .line 146
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->F:Z

    .line 149
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->I:Z

    .line 154
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->M:Ljava/io/File;

    .line 157
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->P:Z

    .line 632
    new-instance v0, Lcom/sec/chaton/buddy/ds;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ds;-><init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->Q:Landroid/os/Handler;

    .line 703
    new-instance v0, Lcom/sec/chaton/buddy/du;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/du;-><init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->d:Lcom/sec/chaton/e/b/d;

    .line 730
    new-instance v0, Lcom/sec/chaton/buddy/dv;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dv;-><init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    .line 1276
    new-instance v0, Lcom/sec/chaton/buddy/dz;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dz;-><init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->R:Landroid/view/View$OnTouchListener;

    .line 1327
    new-instance v0, Lcom/sec/chaton/buddy/ea;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ea;-><init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f:Lcom/sec/chaton/e/a/v;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->w:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;Z)Z
    .locals 0

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->I:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->Q:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->o:Landroid/widget/TextView;

    return-object v0
.end method

.method private d()Lcom/sec/chaton/buddy/BuddyFragment;
    .locals 2

    .prologue
    .line 396
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "com.sec.chaton:single_pane_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/BuddyFragment;
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->d()Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->J:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private e()V
    .locals 7

    .prologue
    const v5, 0x7f07014c

    const v4, 0x7f07007e

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/16 v6, 0x8

    .line 403
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->N:Lcom/coolots/sso/a/a;

    .line 405
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 406
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "groupInfo"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/b;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    .line 407
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    .line 414
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v3, 0x7f070077

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->o:Landroid/widget/TextView;

    .line 415
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v3, 0x7f070078

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->p:Landroid/widget/ImageButton;

    .line 416
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->p:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v3, 0x7f07007f

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->q:Landroid/widget/Button;

    .line 419
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->q:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 421
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->r:Landroid/view/View;

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->r:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s:Landroid/widget/TextView;

    .line 428
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/b;->c()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    .line 430
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0b019f

    invoke-virtual {p0, v4}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v3, 0x7f070076

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->L:Landroid/widget/ImageView;

    .line 433
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->L:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 457
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;

    const v3, 0x7f0b03cc

    new-array v4, v2, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p0, v3, v4}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 458
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->o:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v3, 0x7f07007c

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    .line 478
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v3, 0x7f07007d

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    .line 479
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v3, 0x7f07007a

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->x:Landroid/widget/ImageButton;

    .line 480
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v3, 0x7f07007b

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->y:Landroid/widget/ImageButton;

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 483
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    .line 486
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    sget-object v4, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->N:Lcom/coolots/sso/a/a;

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_2
    move v0, v1

    .line 489
    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_6

    .line 490
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    move v2, v1

    .line 489
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 409
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "groupInfo"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/b;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    goto/16 :goto_0

    .line 425
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->t:Landroid/view/View;

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s:Landroid/widget/TextView;

    goto/16 :goto_1

    .line 494
    :cond_6
    if-eqz v2, :cond_7

    .line 495
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 496
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 540
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 542
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->x:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 543
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->y:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 546
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->R:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 547
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->R:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->x:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->R:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->y:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->R:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 551
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->J:Landroid/app/ProgressDialog;

    .line 552
    return-void

    .line 498
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 499
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3

    .line 502
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 503
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3

    .line 506
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 507
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    return-object v0
.end method

.method private f()Z
    .locals 5

    .prologue
    .line 927
    const/4 v1, 0x0

    .line 930
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->N:Lcom/coolots/sso/a/a;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 931
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 936
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    return v0

    .line 932
    :catch_0
    move-exception v0

    move-object v4, v0

    move v0, v1

    move-object v1, v4

    .line 933
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[ChatONV] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 932
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1091
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->L:Landroid/widget/ImageView;

    const v1, 0x7f0201bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1092
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1096
    :goto_0
    return-void

    .line 1093
    :catch_0
    move-exception v0

    .line 1094
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->N:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method private h()V
    .locals 8

    .prologue
    .line 1210
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1211
    :cond_0
    const-string v0, "[deleteTempFolder] External Storage Is Not Available or Writable!"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1212
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->H:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 1213
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v1, 0x7f0b002b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->H:Landroid/widget/Toast;

    .line 1215
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->H:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1238
    :cond_2
    :goto_0
    return-void

    .line 1218
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v1

    .line 1219
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 1220
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->H:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1226
    :cond_4
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/profile/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1227
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 1228
    if-eqz v2, :cond_2

    .line 1229
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 1230
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/profile/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    .line 1231
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Delete File] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/profile/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1229
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1234
    :catch_0
    move-exception v0

    .line 1235
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1251
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 1252
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1253
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->E:Z

    .line 1254
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->F:Z

    .line 1262
    :goto_0
    return-void

    .line 1255
    :cond_0
    const-string v1, "mounted_ro"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1256
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->E:Z

    .line 1257
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->F:Z

    goto :goto_0

    .line 1259
    :cond_1
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->E:Z

    .line 1260
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->F:Z

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1434
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1437
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1438
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1439
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1440
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivity(Landroid/content/Intent;)V

    .line 1442
    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private k()Z
    .locals 4

    .prologue
    .line 1466
    const/4 v0, 0x0

    .line 1469
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1471
    const/4 v0, 0x1

    .line 1477
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVAvaiable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    return v0

    .line 1473
    :catch_0
    move-exception v1

    .line 1474
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->u:Ljava/lang/String;

    return-object v0
.end method

.method private l()Z
    .locals 3

    .prologue
    .line 1482
    const/4 v0, 0x0

    .line 1485
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->N:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1487
    const/4 v0, 0x1

    .line 1493
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVInstalled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1494
    return v0

    .line 1489
    :catch_0
    move-exception v1

    .line 1490
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->v:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->w:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Z
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->k()Z

    move-result v0

    return v0
.end method

.method static synthetic p(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Z
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic q(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Z
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->l()Z

    move-result v0

    return v0
.end method

.method static synthetic r(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->M:Ljava/io/File;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->G:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->L:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 942
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->B:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_0

    .line 943
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->B:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 955
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h()V

    .line 957
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tmp_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpeg_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->D:Ljava/lang/String;

    .line 958
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->B:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->D:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->C:Ljava/io/File;

    .line 959
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->C:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v1

    .line 960
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Create File] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->B:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->D:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->C:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->G:Landroid/net/Uri;

    .line 966
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->c()Z

    move-result v1

    if-nez v1, :cond_4

    .line 967
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->H:Landroid/widget/Toast;

    if-nez v0, :cond_2

    .line 968
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v1, 0x7f0b002b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->H:Landroid/widget/Toast;

    .line 970
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->H:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1087
    :cond_3
    :goto_0
    return-void

    .line 976
    :cond_4
    const/4 v1, 0x0

    .line 977
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 978
    const v2, 0x7f0d0006

    .line 979
    const v1, 0x7f0b0126

    .line 986
    :goto_1
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/buddy/dx;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/dx;-><init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/buddy/dw;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/dw;-><init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 1065
    if-eqz v0, :cond_5

    .line 1066
    const v0, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/buddy/dy;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dy;-><init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 1073
    :cond_5
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1079
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->I:Z

    if-eqz v0, :cond_3

    .line 1080
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1083
    :catch_0
    move-exception v0

    .line 1085
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 982
    :cond_6
    const v2, 0x7f0d001d

    .line 983
    const v0, 0x7f0b0204

    move v4, v1

    move v1, v0

    move v0, v4

    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 1363
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1099
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_group_profile.png_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1101
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1106
    :goto_0
    const-string v1, "saveBitmapToFile : fileName"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    const/4 v1, 0x0

    .line 1109
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 1110
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x5a

    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1111
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v2}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1116
    if-eqz v1, :cond_0

    .line 1117
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 1123
    :cond_0
    :goto_1
    return-void

    .line 1102
    :catch_0
    move-exception v1

    .line 1104
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 1112
    :catch_1
    move-exception v0

    .line 1113
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1116
    if-eqz v1, :cond_0

    .line 1117
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 1119
    :catch_2
    move-exception v0

    .line 1120
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 1115
    :catchall_0
    move-exception v0

    .line 1116
    if-eqz v1, :cond_1

    .line 1117
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1115
    :cond_1
    :goto_3
    throw v0

    .line 1119
    :catch_3
    move-exception v1

    .line 1120
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_3

    .line 1119
    :catch_4
    move-exception v0

    .line 1120
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 1266
    const/4 v0, 0x0

    .line 1267
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->M:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_group_profile.png_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1268
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 1269
    const/4 v0, 0x1

    .line 1272
    :cond_0
    return v0
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 1367
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    .line 1368
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b019f

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1369
    return-void
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 1241
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i()V

    .line 1242
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->E:Z

    return v0
.end method

.method public c(I)V
    .locals 3

    .prologue
    .line 1428
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mGroup member Count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1429
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    .line 1430
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b019f

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1431
    return-void
.end method

.method protected c()Z
    .locals 1

    .prologue
    .line 1246
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i()V

    .line 1247
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->F:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 238
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 239
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cm;->a(Landroid/app/Activity;)V

    .line 240
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v0, -0x1

    const/16 v4, 0x258

    const/4 v3, 0x1

    .line 1128
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1129
    packed-switch p1, :pswitch_data_0

    .line 1206
    :cond_0
    :goto_0
    return-void

    .line 1132
    :pswitch_0
    if-ne p2, v0, :cond_0

    .line 1133
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "temp_file_path"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1135
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1136
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_1

    .line 1137
    const-string v0, "Crop return null!"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1143
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "temp_file_path"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/16 v3, 0x258

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/util/ad;->b(Landroid/content/Context;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    const/16 v2, 0x258

    const/16 v3, 0x258

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1144
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->C:Ljava/io/File;

    invoke-static {v1, v2}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 1145
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->L:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1146
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1149
    :catch_0
    move-exception v0

    .line 1150
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1151
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g()V

    goto :goto_0

    .line 1157
    :pswitch_1
    if-nez p3, :cond_2

    .line 1158
    const-string v0, "Crop Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1161
    :cond_2
    if-ne p2, v0, :cond_0

    .line 1162
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->G:Landroid/net/Uri;

    .line 1163
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1164
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->G:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1165
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1166
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1167
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1168
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1169
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1170
    const-string v1, "groupname"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1171
    const-string v1, "isgroup"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1172
    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1177
    :pswitch_2
    if-ne p2, v0, :cond_3

    .line 1178
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1179
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->G:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1180
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1181
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1182
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1183
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1184
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1185
    const-string v1, "groupname"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1186
    const-string v1, "isgroup"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1187
    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1189
    :cond_3
    const-string v0, "Camera Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1195
    :pswitch_3
    if-ne p2, v0, :cond_0

    .line 1196
    const-string v0, "PROFILE_EDIT_BUDDY_NAME"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_GROUP_RENAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1198
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/a/b;->a(Ljava/lang/String;)V

    .line 1199
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1129
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 165
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    .line 166
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 177
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 178
    const-string v0, "onCreate()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 181
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "LAUNCH_AS_CHILD"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->P:Z

    .line 186
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->P:Z

    if-nez v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v1, 0x7f03003f

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(I)V

    .line 188
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->setHasOptionsMenu(Z)V

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    const v1, 0x7f0b0197

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 197
    new-instance v0, Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyFragment;-><init>()V

    .line 198
    if-nez p1, :cond_5

    .line 199
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 200
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 203
    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 205
    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->setArguments(Landroid/os/Bundle;)V

    .line 211
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f070013

    const-string v3, "com.sec.chaton:single_pane_fragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 227
    :cond_2
    :goto_2
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->v:Lcom/sec/chaton/e/a/u;

    .line 233
    return-void

    .line 183
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "LAUNCH_AS_CHILD"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->P:Z

    goto :goto_0

    .line 208
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_1

    .line 214
    :cond_5
    const-string v0, "CAPTURE_IMAGE_URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 217
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->G:Landroid/net/Uri;

    .line 220
    :cond_6
    const-string v0, "PROFILE_IMAGE_TEMP_FILE_URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 222
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 223
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->C:Ljava/io/File;

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 557
    const-string v0, "onCreateOptionsMenu()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 561
    const v0, 0x7f0f002f

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 567
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const v5, 0x7f07007e

    const v7, 0x7f070013

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/16 v6, 0x8

    .line 255
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->P:Z

    if-eqz v0, :cond_b

    .line 256
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->setHasOptionsMenu(Z)V

    .line 264
    const v0, 0x7f03001c

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 266
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->N:Lcom/coolots/sso/a/a;

    .line 267
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 268
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "groupInfo"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/b;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    .line 269
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    .line 275
    :cond_0
    :goto_0
    const v0, 0x7f070077

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->o:Landroid/widget/TextView;

    .line 276
    const v0, 0x7f070078

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->p:Landroid/widget/ImageButton;

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->p:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    const v0, 0x7f07007f

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->q:Landroid/widget/Button;

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->q:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 284
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->r:Landroid/view/View;

    .line 285
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->r:Landroid/view/View;

    const v2, 0x7f07014c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s:Landroid/widget/TextView;

    .line 290
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/b;->c()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    .line 292
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0b019f

    invoke-virtual {p0, v5}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " ("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v5, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    const v0, 0x7f070076

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->L:Landroid/widget/ImageView;

    .line 295
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->L:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->o:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    const v0, 0x7f07007c

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    .line 300
    const v0, 0x7f07007d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    .line 301
    const v0, 0x7f07007a

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->x:Landroid/widget/ImageButton;

    .line 302
    const v0, 0x7f07007b

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->y:Landroid/widget/ImageButton;

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    .line 306
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    sget-object v5, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v2, v5}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->N:Lcom/coolots/sso/a/a;

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_1
    move v0, v1

    move v2, v3

    .line 309
    :goto_2
    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_5

    .line 310
    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    move v2, v1

    .line 309
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 271
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "groupInfo"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/b;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    goto/16 :goto_0

    .line 287
    :cond_4
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->t:Landroid/view/View;

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->t:Landroid/view/View;

    const v2, 0x7f07014c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s:Landroid/widget/TextView;

    goto/16 :goto_1

    .line 314
    :cond_5
    if-eqz v2, :cond_6

    .line 315
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 330
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->x:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->y:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->R:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 337
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->R:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->x:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->R:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->y:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->R:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->J:Landroid/app/ProgressDialog;

    .line 343
    new-instance v0, Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyFragment;-><init>()V

    .line 344
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 345
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 348
    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 350
    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->setArguments(Landroid/os/Bundle;)V

    .line 357
    :goto_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 358
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "com.sec.chaton:single_pane_fragment"

    invoke-virtual {v1, v7, v0, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :goto_5
    move-object v0, v4

    .line 368
    :goto_6
    return-object v0

    .line 318
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_3

    .line 322
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_3

    .line 326
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_3

    .line 353
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_4

    .line 360
    :cond_a
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "com.sec.chaton:single_pane_fragment"

    invoke-virtual {v1, v7, v0, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_5

    .line 368
    :cond_b
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    goto :goto_6
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 171
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    .line 173
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 586
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 611
    const/4 v0, 0x0

    .line 614
    :goto_0
    return v0

    .line 590
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/dr;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dr;-><init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 614
    const/4 v0, 0x1

    goto :goto_0

    .line 586
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x7f0705af -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 377
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 378
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 571
    const-string v0, "onPrepareOptionsMenu()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 582
    return-void
.end method

.method public onResume()V
    .locals 9

    .prologue
    const v8, 0x7f0b019f

    const/4 v1, 0x0

    const/16 v7, 0x8

    .line 1374
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 1376
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->j()V

    .line 1378
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v8}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1380
    const/4 v2, 0x0

    .line 1382
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->M:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v5}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-static {v5, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_group_profile.png_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1388
    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1389
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1390
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->L:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1395
    :goto_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 1397
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1398
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v8}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1401
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1402
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->N:Lcom/coolots/sso/a/a;

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1403
    :cond_1
    const/4 v0, 0x1

    move v2, v0

    move v0, v1

    .line 1404
    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 1405
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->K:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    move v2, v1

    .line 1404
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1383
    :catch_0
    move-exception v0

    .line 1385
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v0, v2

    goto/16 :goto_0

    .line 1392
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->L:Landroid/widget/ImageView;

    const v2, 0x7f0201bc

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 1409
    :cond_4
    if-eqz v2, :cond_5

    .line 1410
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1411
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1424
    :goto_3
    return-void

    .line 1413
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1414
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3

    .line 1417
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1418
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3

    .line 1421
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1422
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 382
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->G:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 385
    const-string v0, "CAPTURE_IMAGE_URI"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->G:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->C:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 389
    const-string v0, "PROFILE_IMAGE_TEMP_FILE_URI"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->C:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 245
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 247
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->P:Z

    if-nez v0, :cond_0

    .line 248
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e()V

    .line 250
    :cond_0
    return-void
.end method
