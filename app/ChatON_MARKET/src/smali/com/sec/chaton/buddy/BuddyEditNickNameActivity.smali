.class public Lcom/sec/chaton/buddy/BuddyEditNickNameActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "BuddyEditNickNameActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;-><init>()V

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 42
    invoke-static {p0}, Lcom/sec/chaton/buddy/BuddyEditNickNameActivity;->a(Landroid/app/Activity;)V

    .line 44
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-static {p0}, Lcom/sec/chaton/buddy/BuddyEditNickNameActivity;->a(Landroid/app/Activity;)V

    .line 52
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 58
    invoke-static {p0}, Lcom/sec/chaton/buddy/BuddyEditNickNameActivity;->a(Landroid/app/Activity;)V

    .line 59
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 25
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 31
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 27
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyEditNickNameActivity;->finish()V

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
