.class Lcom/sec/chaton/buddy/gi;
.super Landroid/database/ContentObserver;
.source "BuddyRecommendFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 663
    iput-object p1, p0, Lcom/sec/chaton/buddy/gi;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 666
    const-string v0, "normalBuddyContentObserver onChange"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    iget-object v0, p0, Lcom/sec/chaton/buddy/gi;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->h(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/buddy/gm;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 668
    iget-object v0, p0, Lcom/sec/chaton/buddy/gi;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->i(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v3, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v5, "type=\'200\'"

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    iget-object v0, p0, Lcom/sec/chaton/buddy/gi;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->i(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/4 v1, 0x2

    sget-object v3, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v5, "type=\'100\' AND rank > 0"

    const-string v7, "rank COLLATE LOCALIZED ASC"

    move-object v4, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    iget-object v0, p0, Lcom/sec/chaton/buddy/gi;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->h(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/buddy/gm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/gm;->notifyDataSetChanged()V

    .line 676
    :cond_0
    return-void
.end method
