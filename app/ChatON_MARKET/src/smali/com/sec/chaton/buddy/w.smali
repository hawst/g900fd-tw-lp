.class Lcom/sec/chaton/buddy/w;
.super Ljava/lang/Object;
.source "AddBuddyFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/AddBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 0

    .prologue
    .line 1872
    iput-object p1, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x7f0e0000

    const/4 v3, 0x1

    .line 1876
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1878
    iget-object v1, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    const v2, 0x7f0b01f1

    invoke-virtual {v1, v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1879
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 1880
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030025"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 1883
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0028

    new-array v2, v3, [Ljava/lang/Object;

    const-string v3, "\nwww.chaton.com/invite.html"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1884
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1886
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1887
    const-string v3, "android.intent.action.SENDTO"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "smsto:"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1888
    const-string v3, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    .line 1889
    const-string v3, "android.intent.category.BROWSABLE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    .line 1890
    const-string v3, "sms_body"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1893
    const/high16 v0, 0x10000

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 1895
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 1896
    iget-object v0, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->startActivity(Landroid/content/Intent;)V

    .line 1986
    :cond_1
    :goto_0
    return-void

    .line 1898
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0146

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1900
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    const v2, 0x7f0b01f3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1901
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_4

    .line 1902
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030027"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 1905
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/tellfriends/TwitterSubMenuActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1906
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    const v2, 0x7f0b01f4

    invoke-virtual {v1, v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1907
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_6

    .line 1908
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030028"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 1911
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/tellfriends/WeiboSubMenuActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1913
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    const v2, 0x7f0b01f5

    invoke-virtual {v1, v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1914
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_8

    .line 1915
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030028"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 1918
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/buddy/w;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/tellfriends/RenrenSubMenuActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
