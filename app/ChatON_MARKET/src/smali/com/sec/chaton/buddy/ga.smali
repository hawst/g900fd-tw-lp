.class Lcom/sec/chaton/buddy/ga;
.super Landroid/os/Handler;
.source "BuddyRecommendFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V
    .locals 0

    .prologue
    .line 1267
    iput-object p1, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const v3, 0x7f0b00be

    const/4 v5, 0x0

    .line 1270
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1272
    iget-object v1, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1298
    :goto_0
    return-void

    .line 1275
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1276
    iget-object v1, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1279
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x64

    if-ne v1, v2, :cond_3

    .line 1280
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_2

    .line 1281
    iget-object v0, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b020c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1286
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v3, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1289
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_4

    .line 1290
    iget-object v0, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b003b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1295
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/ga;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v3, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method
