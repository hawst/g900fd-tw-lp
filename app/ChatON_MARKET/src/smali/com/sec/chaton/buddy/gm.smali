.class Lcom/sec/chaton/buddy/gm;
.super Lcom/sec/widget/ax;
.source "BuddyRecommendFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Landroid/content/Context;ILandroid/database/Cursor;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1310
    iput-object p1, p0, Lcom/sec/chaton/buddy/gm;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    .line 1311
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/sec/widget/ax;-><init>(Landroid/content/Context;ILandroid/database/Cursor;Ljava/lang/String;)V

    .line 1312
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/buddy/gm;->f:Landroid/view/LayoutInflater;

    .line 1313
    return-void
.end method


# virtual methods
.method protected a(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const v2, 0x7f07014c

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1318
    .line 1323
    if-nez p3, :cond_1

    .line 1324
    iget-object v0, p0, Lcom/sec/chaton/buddy/gm;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f030088

    invoke-virtual {v0, v1, p4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1325
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1337
    :goto_0
    const v2, 0x7f02029a

    invoke-virtual {v1, v5, v5, v2, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 1338
    const/16 v2, 0xc8

    if-ne p1, v2, :cond_0

    .line 1339
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/gm;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01ba

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/gm;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->k(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1341
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1343
    iget-object v2, p0, Lcom/sec/chaton/buddy/gm;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->k(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)I

    move-result v2

    const/16 v3, 0xa

    if-le v2, v3, :cond_2

    .line 1345
    iget-object v1, p0, Lcom/sec/chaton/buddy/gm;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->u(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1347
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1348
    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setDescendantFocusability(I)V

    .line 1380
    :cond_0
    :goto_1
    return-object v0

    .line 1331
    :cond_1
    check-cast p3, Landroid/widget/LinearLayout;

    .line 1332
    invoke-virtual {p3, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v1, v0

    move-object v0, p3

    goto :goto_0

    .line 1351
    :cond_2
    invoke-virtual {v1, v5, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 1352
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1354
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 1385
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/gn;

    .line 1386
    iget v1, p0, Lcom/sec/chaton/buddy/gm;->b:I

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1387
    iget v2, p0, Lcom/sec/chaton/buddy/gm;->c:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1388
    iget v2, p0, Lcom/sec/chaton/buddy/gm;->d:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1389
    iget v4, p0, Lcom/sec/chaton/buddy/gm;->e:I

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1391
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "recommend_timestamp"

    const-wide/16 v7, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 1392
    iget-object v6, v0, Lcom/sec/chaton/buddy/gn;->a:Landroid/widget/ImageView;

    const v7, 0x7f02028d

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1393
    iget-object v6, v0, Lcom/sec/chaton/buddy/gn;->b:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1395
    iget-object v1, v0, Lcom/sec/chaton/buddy/gn;->c:Landroid/widget/ImageView;

    const v6, 0x7f0202cf

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1396
    iget-object v1, v0, Lcom/sec/chaton/buddy/gn;->c:Landroid/widget/ImageView;

    const v6, 0x7f07000e

    invoke-virtual {v1, v6, v3}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 1397
    iget-object v1, v0, Lcom/sec/chaton/buddy/gn;->c:Landroid/widget/ImageView;

    const v6, 0x7f07000f

    invoke-virtual {v1, v6, v2}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 1398
    iget-object v1, v0, Lcom/sec/chaton/buddy/gn;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1400
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1401
    iget-object v1, v0, Lcom/sec/chaton/buddy/gn;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1405
    iget-object v6, p0, Lcom/sec/chaton/buddy/gm;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/gm;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->v(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)J

    move-result-wide v1

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v1, v1, v7

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/buddy/gm;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->v(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)J

    move-result-wide v1

    :goto_0
    invoke-static {v6, v1, v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;J)J

    .line 1406
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v1, v4

    if-lez v1, :cond_2

    .line 1407
    iget-object v1, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 1408
    iget-object v2, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 1409
    iget-object v4, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 1410
    iget-object v5, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    .line 1412
    iget-object v6, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    const v7, 0x7f0202de

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1413
    iget-object v6, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v6, v5, v1, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 1432
    :cond_0
    :goto_1
    invoke-static {p2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/buddy/gn;->a:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v3, v2}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;Z)V

    .line 1433
    return-void

    .line 1405
    :cond_1
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_0

    .line 1415
    :cond_2
    iget-object v1, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 1416
    iget-object v2, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 1417
    iget-object v4, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 1418
    iget-object v5, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    .line 1420
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1421
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x10

    if-lt v6, v7, :cond_3

    .line 1422
    iget-object v6, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1426
    :goto_2
    iget-object v6, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v6, v5, v1, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    .line 1424
    :cond_3
    iget-object v6, v0, Lcom/sec/chaton/buddy/gn;->d:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1437
    invoke-super {p0, p1, p2, p3}, Lcom/sec/widget/ax;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1438
    new-instance v1, Lcom/sec/chaton/buddy/gn;

    invoke-direct {v1, v0}, Lcom/sec/chaton/buddy/gn;-><init>(Landroid/view/View;)V

    .line 1440
    iget-object v2, v1, Lcom/sec/chaton/buddy/gn;->c:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/gm;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->w(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1442
    iget v2, p0, Lcom/sec/chaton/buddy/gm;->d:I

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    const/16 v3, 0xc8

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1447
    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1449
    return-object v0
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 1454
    if-eqz p1, :cond_0

    .line 1456
    const-string v0, "buddy_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/gm;->b:I

    .line 1457
    const-string v0, "buddy_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/gm;->c:I

    .line 1458
    const-string v0, "type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/gm;->d:I

    .line 1459
    const-string v0, "timestamp"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/gm;->e:I

    .line 1461
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/widget/ax;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
