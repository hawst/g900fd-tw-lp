.class public Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;
.super Landroid/app/Activity;
.source "SpamConfirmDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field a:Landroid/content/Context;

.field c:Landroid/os/Handler;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 89
    new-instance v0, Lcom/sec/chaton/buddy/dialog/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/z;-><init>(Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->c:Landroid/os/Handler;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->c:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->e:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/f;->b(Ljava/lang/String;I)V

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->a()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->e:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 153
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 156
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->startActivity(Landroid/content/Intent;)V

    .line 161
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 121
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 122
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 124
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->finish()V

    .line 128
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 42
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->requestWindowFeature(I)Z

    .line 45
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->a:Landroid/content/Context;

    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 48
    invoke-virtual {p0, v4}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->setFinishOnTouchOutside(Z)V

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SCD_DIALOG_BUDDY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->d:Ljava/lang/String;

    .line 54
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SCD_DIALOG_BUDDY_NO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->e:Ljava/lang/String;

    .line 56
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01b4

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01b5

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/dialog/y;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dialog/y;-><init>(Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/dialog/x;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dialog/x;-><init>(Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 79
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 149
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->b()V

    .line 150
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 133
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 143
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
