.class Lcom/sec/chaton/buddy/p;
.super Landroid/os/Handler;
.source "AddBuddyFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/AddBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 0

    .prologue
    .line 1496
    iput-object p1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/16 v7, 0x12f

    const v6, 0x7f0b01bc

    const/16 v5, 0x133

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1499
    const-string v0, ""

    .line 1500
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1502
    iget v1, p1, Landroid/os/Message;->what:I

    if-eq v1, v7, :cond_0

    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v5, :cond_1

    .line 1503
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1595
    :cond_1
    :goto_0
    return-void

    .line 1506
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v5, :cond_4

    move v1, v2

    .line 1507
    :goto_1
    iget-object v4, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/AddBuddyFragment;->g(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1509
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1510
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v4, :cond_6

    .line 1511
    iget-object v1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a()V

    .line 1512
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v7, :cond_5

    .line 1513
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetBuddyList;

    .line 1514
    if-eqz v0, :cond_3

    .line 1515
    iget-object v1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetBuddyList;->buddy:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Buddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1517
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b027d

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/AddBuddyFragment;->f(Lcom/sec/chaton/buddy/AddBuddyFragment;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1518
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0

    :cond_4
    move v1, v3

    .line 1506
    goto :goto_1

    .line 1522
    :cond_5
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v5, :cond_1

    .line 1523
    iget-object v1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetBuddyList;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Lcom/sec/chaton/io/entry/GetBuddyList;)V

    goto :goto_0

    .line 1526
    :cond_6
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v4, :cond_7

    .line 1527
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->h(Lcom/sec/chaton/buddy/AddBuddyFragment;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1528
    iget-object v1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/q;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/q;-><init>(Lcom/sec/chaton/buddy/p;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1541
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADD BUDDY RESULT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1543
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e83

    if-ne v1, v2, :cond_8

    .line 1544
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1575
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1546
    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e84

    if-ne v1, v2, :cond_9

    .line 1547
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1549
    :cond_9
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e85

    if-ne v1, v2, :cond_a

    .line 1551
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1560
    :cond_a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e8a

    if-ne v1, v2, :cond_b

    .line 1561
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0265

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1562
    :cond_b
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e8b

    if-ne v1, v2, :cond_c

    .line 1563
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0296

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1564
    :cond_c
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e8c

    if-ne v1, v2, :cond_d

    .line 1565
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1566
    :cond_d
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v1, 0x3e8d

    if-ne v0, v1, :cond_e

    .line 1567
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1571
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1579
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b002a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/buddy/s;

    invoke-direct {v3, p0, v1}, Lcom/sec/chaton/buddy/s;-><init>(Lcom/sec/chaton/buddy/p;Z)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/p;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/r;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/r;-><init>(Lcom/sec/chaton/buddy/p;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0
.end method
