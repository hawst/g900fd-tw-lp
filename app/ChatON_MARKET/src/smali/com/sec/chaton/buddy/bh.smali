.class Lcom/sec/chaton/buddy/bh;
.super Ljava/lang/Object;
.source "BuddyFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 2753
    iput-object p1, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2756
    iget-object v1, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 2757
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-nez v0, :cond_0

    .line 2758
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    .line 2760
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_6

    .line 2761
    sput-boolean v4, Lcom/sec/chaton/buddy/BuddyFragment;->l:Z

    .line 2762
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->w(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2763
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->w(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2771
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 2772
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 2774
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->x(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2775
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->x(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2780
    :cond_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v4, :cond_5

    .line 2781
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->y(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->z(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_7

    .line 2782
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->A(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2789
    :cond_5
    :goto_1
    return-void

    .line 2766
    :cond_6
    sput-boolean v3, Lcom/sec/chaton/buddy/BuddyFragment;->l:Z

    .line 2767
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->w(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2768
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->w(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 2784
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/bh;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->A(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
