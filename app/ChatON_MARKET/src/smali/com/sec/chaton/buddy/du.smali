.class Lcom/sec/chaton/buddy/du;
.super Ljava/lang/Object;
.source "BuddyGroupProfileFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/b/d;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V
    .locals 0

    .prologue
    .line 703
    iput-object p1, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IZLjava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 708
    packed-switch p1, :pswitch_data_0

    .line 727
    :goto_0
    return-void

    .line 710
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 713
    :pswitch_1
    instance-of v0, p3, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 714
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 715
    const-string v0, "group_name"

    iget-object v2, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->l(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    iget-object v0, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->m(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->l(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/n;->a:Landroid/net/Uri;

    const-string v5, "_id = ? "

    new-array v6, v1, [Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v7}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 720
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/Toast;

    move-result-object v0

    if-nez v0, :cond_1

    .line 721
    iget-object v0, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    iget-object v2, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0196

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->l(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v8

    invoke-virtual {v3, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 723
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/du;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->n(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 708
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
