.class Lcom/sec/chaton/buddy/dn;
.super Landroid/os/Handler;
.source "BuddyGroupMemberEditFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 216
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->d(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 224
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2

    .line 225
    new-instance v0, Lcom/sec/chaton/e/b/i;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->b:Lcom/sec/chaton/e/b/d;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->e(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->c(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Ljava/util/HashSet;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/e/b/i;-><init>(Lcom/sec/chaton/e/b/d;ILjava/util/Collection;Z)V

    .line 226
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    invoke-static {v1, v5, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0089

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 233
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 234
    iget-object v1, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 236
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/o;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0139

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/do;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/do;-><init>(Lcom/sec/chaton/buddy/dn;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dn;->a:Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0
.end method
