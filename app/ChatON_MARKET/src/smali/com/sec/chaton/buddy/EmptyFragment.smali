.class public Lcom/sec/chaton/buddy/EmptyFragment;
.super Landroid/support/v4/app/Fragment;
.source "EmptyFragment.java"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;


# instance fields
.field private d:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "No_Buddy_Selection"

    sput-object v0, Lcom/sec/chaton/buddy/EmptyFragment;->a:Ljava/lang/String;

    .line 18
    const-string v0, "No_Chat_Selection"

    sput-object v0, Lcom/sec/chaton/buddy/EmptyFragment;->b:Ljava/lang/String;

    .line 19
    const-string v0, "Empty_Flag"

    sput-object v0, Lcom/sec/chaton/buddy/EmptyFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/EmptyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/EmptyFragment;->d:Landroid/os/Bundle;

    .line 28
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 32
    const v0, 0x7f030039

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 33
    const v0, 0x7f07014b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 35
    iget-object v2, p0, Lcom/sec/chaton/buddy/EmptyFragment;->d:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    .line 36
    iget-object v2, p0, Lcom/sec/chaton/buddy/EmptyFragment;->d:Landroid/os/Bundle;

    sget-object v3, Lcom/sec/chaton/buddy/EmptyFragment;->c:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 38
    sget-object v3, Lcom/sec/chaton/buddy/EmptyFragment;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 39
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/EmptyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02034b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 45
    :cond_0
    :goto_0
    return-object v1

    .line 40
    :cond_1
    sget-object v3, Lcom/sec/chaton/buddy/EmptyFragment;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/EmptyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02034d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
