.class Lcom/sec/chaton/buddy/hk;
.super Landroid/os/AsyncTask;
.source "GroupProfileImageViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/sec/chaton/buddy/hk;->b:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    iput-object p2, p0, Lcom/sec/chaton/buddy/hk;->a:Ljava/io/File;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/chaton/buddy/hk;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/sec/chaton/buddy/hk;->b:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hk;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 535
    if-eqz v0, :cond_0

    .line 545
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 551
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 552
    if-eqz p1, :cond_0

    .line 553
    iget-object v0, p0, Lcom/sec/chaton/buddy/hk;->b:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->a(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 564
    :goto_0
    return-void

    .line 556
    :cond_0
    const-class v1, Lcom/sec/chaton/buddy/GroupProfileImageViewActivity;

    monitor-enter v1

    .line 557
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/hk;->b:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/sec/chaton/buddy/hk;->b:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0b016c

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 559
    iget-object v0, p0, Lcom/sec/chaton/buddy/hk;->b:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->a(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const v2, 0x7f0201bc

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 561
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 528
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/hk;->a([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 528
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/hk;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
