.class Lcom/sec/chaton/buddy/dialog/ak;
.super Ljava/lang/Object;
.source "SpecialBuddyDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 487
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 598
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 492
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 547
    :sswitch_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->f(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    goto :goto_0

    .line 494
    :sswitch_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 495
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "profile_add_special_buddy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SpecialBuddyDialog"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0, v7}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    .line 500
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->f(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    goto :goto_0

    .line 505
    :sswitch_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_3

    .line 506
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "profile_ignore_special_buddy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SpecialBuddyDialog"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0, v7}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    .line 512
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->h(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    goto/16 :goto_0

    .line 519
    :sswitch_4
    sput-boolean v7, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 520
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_4

    .line 521
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "profile_special_chat, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SpecialBuddyDialog"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    :cond_4
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 525
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 526
    const-string v1, "callChatList"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 527
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 528
    const-string v1, "receivers"

    new-array v2, v7, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 529
    const-string v1, "specialbuddy"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 530
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 531
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->finish()V

    goto/16 :goto_0

    .line 550
    :sswitch_5
    sput-boolean v7, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 551
    sget-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_UPDATED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v0

    .line 554
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_5

    .line 555
    sget-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NONE_PROFILE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v0

    .line 557
    :cond_5
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 558
    const-string v2, "PROFILE_BUDDY_NO"

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 559
    const-string v2, "PROFILE_BUDDY_BIGIMAGE_STATUS"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 560
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v0, v1, v8}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->startActivityForResult(Landroid/content/Intent;I)V

    .line 561
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->finish()V

    goto/16 :goto_0

    .line 564
    :sswitch_6
    sput-boolean v7, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 565
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/SpecialBuddyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 566
    const-string v1, "specialuserid"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 567
    const-string v1, "speicalusername"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->k(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 568
    const-string v1, "specialBuddyAdded"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 570
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 571
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->finish()V

    goto/16 :goto_0

    .line 574
    :sswitch_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0, v7}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0, v7}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->b(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)V

    .line 576
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 577
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/h;->f(Ljava/lang/String;)V

    .line 579
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 580
    const-string v0, "likecount"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    const-string v0, "islike"

    const-string v2, "N"

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->n(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "buddy_no=\'"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v9, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v9}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "\'"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 592
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->l(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v7

    :goto_2
    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)V

    goto/16 :goto_0

    .line 584
    :cond_6
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/h;->e(Ljava/lang/String;)V

    .line 586
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 587
    const-string v0, "likecount"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    const-string v0, "islike"

    const-string v2, "Y"

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->n(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "buddy_no=\'"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v9, p0, Lcom/sec/chaton/buddy/dialog/ak;->a:Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-static {v9}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "\'"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    move v0, v8

    .line 592
    goto :goto_2

    .line 492
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0700d1 -> :sswitch_1
        0x7f070533 -> :sswitch_7
        0x7f070537 -> :sswitch_5
        0x7f07053b -> :sswitch_4
        0x7f07053c -> :sswitch_0
        0x7f07053d -> :sswitch_3
        0x7f07053e -> :sswitch_2
        0x7f07053f -> :sswitch_6
    .end sparse-switch
.end method
