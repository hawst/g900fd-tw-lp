.class public Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;
.super Landroid/support/v4/app/Fragment;
.source "BuddyProfileImageViewFragment.java"


# static fields
.field public static final e:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Landroid/widget/ProgressBar;

.field private C:I

.field private D:Landroid/widget/ImageView;

.field private E:Ljava/lang/Boolean;

.field private F:Ljava/lang/Boolean;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:I

.field private J:I

.field private K:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/ProfileImage;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Z

.field private P:Landroid/app/Activity;

.field private Q:Landroid/os/Handler;

.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field f:Landroid/widget/ImageView;

.field g:Landroid/widget/AdapterView$OnItemClickListener;

.field h:I

.field private i:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

.field private j:Landroid/app/ProgressDialog;

.field private k:Lcom/sec/chaton/d/w;

.field private l:Z

.field private m:Z

.field private n:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private o:Landroid/widget/GridView;

.field private p:Landroid/widget/FrameLayout;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/FrameLayout;

.field private s:Landroid/widget/FrameLayout;

.field private t:Landroid/widget/LinearLayout;

.field private u:Lcom/sec/chaton/buddy/fj;

.field private v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcom/sec/common/f/c;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const-class v0, Lcom/sec/chaton/buddy/BuddyProfileImageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 143
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 71
    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->b:Ljava/lang/String;

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->c:Ljava/lang/String;

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->d:Ljava/lang/String;

    .line 78
    iput-boolean v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->l:Z

    .line 79
    iput-boolean v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->m:Z

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->x:Ljava/lang/String;

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->G:Ljava/lang/String;

    .line 113
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    .line 114
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->I:I

    .line 119
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    .line 120
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->M:Ljava/lang/String;

    .line 121
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->N:Ljava/lang/String;

    .line 122
    iput-boolean v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->O:Z

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f:Landroid/widget/ImageView;

    .line 295
    new-instance v0, Lcom/sec/chaton/buddy/fn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fn;-><init>(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->g:Landroid/widget/AdapterView$OnItemClickListener;

    .line 506
    new-instance v0, Lcom/sec/chaton/buddy/fo;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fo;-><init>(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->Q:Landroid/os/Handler;

    .line 979
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->h:I

    .line 144
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 147
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 71
    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->b:Ljava/lang/String;

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->c:Ljava/lang/String;

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->d:Ljava/lang/String;

    .line 78
    iput-boolean v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->l:Z

    .line 79
    iput-boolean v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->m:Z

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->x:Ljava/lang/String;

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->G:Ljava/lang/String;

    .line 113
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    .line 114
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->I:I

    .line 119
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    .line 120
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->M:Ljava/lang/String;

    .line 121
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->N:Ljava/lang/String;

    .line 122
    iput-boolean v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->O:Z

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f:Landroid/widget/ImageView;

    .line 295
    new-instance v0, Lcom/sec/chaton/buddy/fn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fn;-><init>(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->g:Landroid/widget/AdapterView$OnItemClickListener;

    .line 506
    new-instance v0, Lcom/sec/chaton/buddy/fo;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fo;-><init>(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->Q:Landroid/os/Handler;

    .line 979
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->h:I

    .line 148
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    .line 149
    iput-object p2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->M:Ljava/lang/String;

    .line 150
    iput-object p3, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->N:Ljava/lang/String;

    .line 151
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;I)I
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->I:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->t:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Lcom/sec/common/f/c;)Lcom/sec/common/f/c;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->w:Lcom/sec/common/f/c;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->K:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(I)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 940
    const/4 v1, 0x0

    .line 941
    const/4 v0, 0x0

    .line 945
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v2

    if-nez v2, :cond_1

    .line 946
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 947
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 948
    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 950
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 951
    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 952
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 953
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 955
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 972
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 973
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 977
    :cond_1
    return-void

    .line 958
    :cond_2
    if-ne p1, v6, :cond_0

    .line 959
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 960
    const v0, 0x3f333333    # 0.7f

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 961
    const/16 v0, 0x11

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 963
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 964
    const v2, 0x3e99999a    # 0.3f

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 965
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 967
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a(Ljava/lang/String;Landroid/view/View;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 365
    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    .line 368
    if-nez v0, :cond_1

    .line 369
    const-string v0, "BuddyProfileImageView: mItem == null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_0
    :goto_1
    return-void

    .line 372
    :cond_1
    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 373
    const-string v2, "BuddyProfileImageView: mItem.imageId == null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->selectedImage:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 365
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 375
    :cond_3
    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 376
    const-string v2, "1"

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 377
    const v0, 0x7f0701eb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f:Landroid/widget/ImageView;

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 402
    :catch_0
    move-exception v0

    goto :goto_1

    .line 382
    :cond_4
    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->selectedImage:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 385
    :cond_5
    const-string v2, "1"

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 390
    :cond_6
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->b()V

    .line 391
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 392
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 397
    :cond_7
    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->selectedImage:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;I)I
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->J:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->z:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->I:I

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->A:Ljava/lang/String;

    return-object p1
.end method

.method private c()V
    .locals 3

    .prologue
    .line 445
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->k:Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    const-string v2, "600"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/w;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->g()V

    .line 447
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->J:I

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->G:Ljava/lang/String;

    return-object p1
.end method

.method private d()V
    .locals 5

    .prologue
    .line 666
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a:Ljava/lang/String;

    .line 667
    new-instance v0, Lcom/sec/chaton/buddy/fj;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->P:Landroid/app/Activity;

    const v2, 0x7f03004e

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->v:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->w:Lcom/sec/common/f/c;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/buddy/fj;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->u:Lcom/sec/chaton/buddy/fj;

    .line 668
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->u:Lcom/sec/chaton/buddy/fj;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 670
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->J:I

    if-ge v0, v1, :cond_0

    .line 671
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->u:Lcom/sec/chaton/buddy/fj;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/fj;->a(I)Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    move-result-object v1

    .line 672
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 673
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->I:I

    .line 677
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->P:Landroid/app/Activity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b017f

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->I:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 681
    :cond_0
    return-void

    .line 670
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->P:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->y:Ljava/lang/String;

    return-object p1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 691
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->t:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 695
    :goto_0
    return-void

    .line 692
    :catch_0
    move-exception v0

    .line 693
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 699
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    const v1, 0x7f0201bb

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 703
    :goto_0
    return-void

    .line 700
    :catch_0
    move-exception v0

    .line 701
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->w:Lcom/sec/common/f/c;

    return-object v0
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 707
    .line 709
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->N:Ljava/lang/String;

    const-string v1, "FULL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 717
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->M:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 718
    new-instance v1, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->M:Ljava/lang/String;

    invoke-direct {v1, v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    .line 729
    :goto_0
    if-ne v0, v2, :cond_1

    .line 731
    new-instance v0, Lcom/sec/chaton/buddy/fp;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/fp;-><init>(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/io/File;)V

    .line 759
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 820
    :cond_0
    :goto_1
    return-void

    .line 761
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f()V

    goto :goto_1

    .line 765
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f()V

    goto :goto_1

    .line 768
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->N:Ljava/lang/String;

    const-string v1, "THUMB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 771
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "buddy_f_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->M:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    .line 773
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->M:Ljava/lang/String;

    invoke-direct {v0, v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 776
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->O:Z

    move v1, v2

    .line 782
    :goto_2
    if-ne v1, v2, :cond_5

    .line 784
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 785
    new-instance v1, Lcom/sec/chaton/buddy/fq;

    invoke-direct {v1, p0, v0}, Lcom/sec/chaton/buddy/fq;-><init>(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/io/File;)V

    .line 814
    new-array v0, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    :cond_4
    move v1, v2

    .line 779
    goto :goto_2

    .line 817
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f()V

    goto/16 :goto_1

    :cond_6
    move v1, v3

    goto :goto_2

    :cond_7
    move v0, v3

    goto/16 :goto_0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->D:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->K:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->M:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->O:Z

    return v0
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->x:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->v:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->d()V

    return-void
.end method

.method static synthetic p(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->B:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->j:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 417
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    .line 420
    const-string v1, "1"

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0701eb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f:Landroid/widget/ImageView;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 430
    :catch_0
    move-exception v0

    .line 431
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 131
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 132
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->P:Landroid/app/Activity;

    .line 133
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 908
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 909
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a(I)V

    .line 910
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 157
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->b(Landroid/support/v4/app/Fragment;Z)V

    .line 158
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v3, 0x258

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 170
    invoke-static {p0, v2}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 172
    const v0, 0x7f0300e2

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->P:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->j:Landroid/app/ProgressDialog;

    .line 176
    const v0, 0x7f070322

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    .line 177
    const v0, 0x7f0703f3

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->p:Landroid/widget/FrameLayout;

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->p:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 179
    const v0, 0x7f070124

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->D:Landroid/widget/ImageView;

    .line 180
    const v0, 0x7f0703f6

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->t:Landroid/widget/LinearLayout;

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 193
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->w:Lcom/sec/common/f/c;

    .line 194
    const v0, 0x7f0703fa

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o:Landroid/widget/GridView;

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->g:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 198
    const v0, 0x7f070321

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->q:Landroid/widget/LinearLayout;

    .line 199
    const v0, 0x7f0703f5

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->r:Landroid/widget/FrameLayout;

    .line 200
    const v0, 0x7f0703f8

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->s:Landroid/widget/FrameLayout;

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->P:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 203
    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a(I)V

    .line 211
    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->setHasOptionsMenu(Z)V

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->v:Ljava/util/ArrayList;

    .line 214
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->Q:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->k:Lcom/sec/chaton/d/w;

    .line 215
    const v0, 0x7f0703f9

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->B:Landroid/widget/ProgressBar;

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a:Ljava/lang/String;

    .line 217
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->E:Ljava/lang/Boolean;

    .line 218
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->F:Ljava/lang/Boolean;

    .line 220
    if-eqz p3, :cond_0

    const-string v0, "array"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "mProfileImageUrl"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 221
    const-string v0, "mRepresentPImageId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->G:Ljava/lang/String;

    .line 222
    const-string v0, "mCurrentPImageId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    .line 223
    const-string v0, "array"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->v:Ljava/util/ArrayList;

    .line 224
    const-string v0, "mProfileImageUrl"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->A:Ljava/lang/String;

    .line 225
    const-string v0, "mCurrentPosition"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->I:I

    .line 226
    const-string v0, "mTotalProfileImageCount"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->J:I

    .line 227
    const-string v0, "mSmallImageNum"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->C:I

    .line 230
    const-string v0, "mBuddyNo"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    .line 231
    const-string v0, "mbuddyImageID"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->M:Ljava/lang/String;

    .line 232
    const-string v0, "mbuddyImageType"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->N:Ljava/lang/String;

    .line 233
    const-string v0, "mNoSuchImage"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->O:Z

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mCurrentPImageId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mProfileImageUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a:Ljava/lang/String;

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->P:Landroid/app/Activity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b017f

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->I:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 246
    new-instance v0, Lcom/sec/chaton/userprofile/bq;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->D:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->A:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a()Ljava/lang/String;

    move-result-object v5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buddy_f_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/userprofile/bq;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->w:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 256
    :goto_0
    return-object v7

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->B:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 252
    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->setHasOptionsMenu(Z)V

    .line 253
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->c()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 878
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->w:Lcom/sec/common/f/c;

    if-eqz v0, :cond_0

    .line 879
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->w:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 881
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 882
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 884
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 885
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->P:Landroid/app/Activity;

    .line 140
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 284
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 285
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 287
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->n:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 289
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 261
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 264
    new-instance v1, Lcom/sec/chaton/buddy/fm;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/fm;-><init>(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->n:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 270
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->n:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->P:Landroid/app/Activity;

    const v1, 0x7f0b017f

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 274
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 889
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 890
    const-string v0, "mCurrentPImageId"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->H:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    const-string v0, "mRepresentPImageId"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->G:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    const-string v0, "mProfileImageUrl"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    const-string v0, "array"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->v:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 894
    const-string v0, "mCurrentPosition"

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->I:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 895
    const-string v0, "mTotalProfileImageCount"

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->J:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 896
    const-string v0, "mSmallImageNum"

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->C:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 898
    const-string v0, "mBuddyNo"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->L:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    const-string v0, "mbuddyImageID"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->M:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    const-string v0, "mbuddyImageType"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->N:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    const-string v0, "mNoSuchImage"

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->O:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 902
    return-void
.end method
