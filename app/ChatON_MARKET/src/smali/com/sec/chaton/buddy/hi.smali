.class Lcom/sec/chaton/buddy/hi;
.super Ljava/lang/Object;
.source "GroupProfileImageViewFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const v4, 0x7f0b0414

    const/4 v3, 0x0

    .line 318
    packed-switch p2, :pswitch_data_0

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 322
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 323
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 325
    iget-object v1, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 326
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 327
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 346
    :catch_0
    move-exception v0

    .line 347
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 348
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 349
    sget-object v1, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 355
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 356
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 357
    const-string v1, "output"

    iget-object v2, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 359
    const-string v1, "outputFormat"

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 361
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 362
    :catch_1
    move-exception v0

    .line 363
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 364
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 365
    sget-object v1, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 372
    :pswitch_2
    const-string v0, "onClick Delete profile iamge"

    sget-object v1, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;)V

    .line 375
    const-string v0, ""

    .line 377
    :try_start_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .line 384
    :cond_2
    :goto_1
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->f(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Ljava/io/File;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_group_profile.png_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 385
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->a(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0201bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Z)V

    .line 389
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/hi;->a:Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 378
    :catch_2
    move-exception v1

    .line 380
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_2

    .line 381
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 318
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
