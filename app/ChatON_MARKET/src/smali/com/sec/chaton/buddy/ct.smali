.class Lcom/sec/chaton/buddy/ct;
.super Ljava/lang/Object;
.source "BuddyFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 1565
    iput-object p1, p0, Lcom/sec/chaton/buddy/ct;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 1585
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1569
    if-nez p2, :cond_2

    .line 1570
    iget-object v1, p0, Lcom/sec/chaton/buddy/ct;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/util/bt;->c()V

    .line 1572
    iget-object v1, p0, Lcom/sec/chaton/buddy/ct;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->r(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ct;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1573
    iget-object v1, p0, Lcom/sec/chaton/buddy/ct;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableExpandableListView;->getFirstVisiblePosition()I

    move-result v1

    sput v1, Lcom/sec/chaton/buddy/BuddyFragment;->c:I

    .line 1574
    iget-object v1, p0, Lcom/sec/chaton/buddy/ct;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/widget/FastScrollableExpandableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1575
    if-nez v1, :cond_1

    :goto_0
    sput v0, Lcom/sec/chaton/buddy/BuddyFragment;->d:I

    .line 1581
    :cond_0
    :goto_1
    return-void

    .line 1575
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0

    .line 1579
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/ct;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bt;->b()V

    goto :goto_1
.end method
