.class Lcom/sec/chaton/buddy/j;
.super Ljava/lang/Object;
.source "AddBuddyFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/AddBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 0

    .prologue
    .line 1147
    iput-object p1, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x7f0e0000

    const/4 v3, 0x1

    .line 1150
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1331
    :goto_0
    return-void

    .line 1154
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1166
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->c(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/widget/AdaptableEditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/AdaptableEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1158
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/registration/CountryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1159
    const-string v1, "ADD_BUDDY_TYPE"

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1160
    const-string v1, "needBackKey"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1163
    iget-object v1, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1169
    :sswitch_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a()V

    .line 1171
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 1172
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030021"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 1179
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/view/View;)V

    goto :goto_0

    .line 1182
    :sswitch_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a()V

    .line 1184
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 1185
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030022"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 1191
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/view/View;)V

    goto :goto_0

    .line 1198
    :sswitch_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_3

    .line 1199
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030023"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 1202
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->c(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/widget/AdaptableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->requestFocus()Z

    .line 1203
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->c(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/widget/AdaptableEditText;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/widget/EditText;Z)V

    .line 1204
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/view/View;)V

    goto/16 :goto_0

    .line 1212
    :sswitch_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_4

    .line 1213
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030024"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 1216
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->d(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/widget/AdaptableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/AdaptableEditText;->requestFocus()Z

    .line 1217
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->d(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/widget/AdaptableEditText;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/widget/EditText;Z)V

    .line 1218
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/view/View;)V

    goto/16 :goto_0

    .line 1223
    :sswitch_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1224
    const/4 v1, -0x3

    if-eq v1, v0, :cond_5

    const/4 v1, -0x2

    if-ne v1, v0, :cond_6

    .line 1225
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1228
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/j;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Z)V

    goto/16 :goto_0

    .line 1154
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f070073 -> :sswitch_0
        0x7f0701e5 -> :sswitch_6
        0x7f07020d -> :sswitch_2
        0x7f07020f -> :sswitch_3
        0x7f070210 -> :sswitch_4
        0x7f070211 -> :sswitch_5
        0x7f070216 -> :sswitch_1
        0x7f0702e1 -> :sswitch_6
    .end sparse-switch
.end method
