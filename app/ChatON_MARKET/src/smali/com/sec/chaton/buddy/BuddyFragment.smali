.class public Lcom/sec/chaton/buddy/BuddyFragment;
.super Landroid/support/v4/app/Fragment;
.source "BuddyFragment.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupCollapseListener;
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;
.implements Lcom/sec/chaton/base/d;
.implements Lcom/sec/chaton/buddy/ao;
.implements Lcom/sec/chaton/buddy/ap;
.implements Lcom/sec/chaton/buddy/aq;
.implements Lcom/sec/chaton/by;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field private static bQ:Z

.field private static bq:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static c:I

.field public static d:I

.field public static e:Z

.field public static i:Z

.field public static k:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static l:Z

.field public static m:Z

.field public static n:Ljava/lang/String;

.field public static o:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/ScrollView;

.field private B:Landroid/widget/LinearLayout;

.field private C:Lcom/sec/widget/FastScrollableExpandableListView;

.field private D:Lcom/sec/chaton/util/bt;

.field private E:Landroid/widget/TextView;

.field private F:Lcom/sec/widget/EditTextWithClearButton;

.field private G:Lcom/sec/chaton/d/w;

.field private H:Landroid/view/ViewStub;

.field private I:Landroid/view/View;

.field private J:Landroid/widget/ImageView;

.field private K:Landroid/widget/TextView;

.field private L:Landroid/widget/TextView;

.field private M:Landroid/widget/LinearLayout;

.field private N:Landroid/widget/Button;

.field private O:Landroid/view/View;

.field private P:Landroid/widget/CheckedTextView;

.field private Q:Z

.field private R:Z

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/String;

.field private U:I

.field private V:[Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private X:Landroid/widget/LinearLayout;

.field private Y:Landroid/widget/ImageButton;

.field private Z:Landroid/widget/ImageButton;

.field private aA:Z

.field private aB:Z

.field private aC:I

.field private aD:Ljava/lang/String;

.field private aE:Ljava/lang/String;

.field private aF:Ljava/lang/String;

.field private aG:Lcom/sec/chaton/w;

.field private aH:I

.field private aI:Landroid/widget/TextView;

.field private aJ:Ljava/lang/String;

.field private aK:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aL:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aM:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aN:Landroid/widget/Toast;

.field private aO:Lcom/sec/chaton/e/a/u;

.field private aP:Lcom/sec/chaton/buddy/dc;

.field private aQ:Lcom/sec/chaton/util/bp;

.field private aR:Landroid/view/View;

.field private aS:Landroid/widget/TextView;

.field private aT:Landroid/widget/ImageView;

.field private aU:Landroid/view/View;

.field private aV:Landroid/view/View;

.field private aW:Landroid/widget/ImageView;

.field private aX:Landroid/widget/TextView;

.field private aY:Landroid/widget/TextView;

.field private aZ:Ljava/lang/String;

.field private aa:Landroid/widget/ImageButton;

.field private ab:Landroid/widget/ImageButton;

.field private ac:Landroid/os/Bundle;

.field private ad:I

.field private ae:I

.field private af:I

.field private ag:I

.field private ah:[Ljava/lang/String;

.field private ai:[Ljava/lang/String;

.field private aj:[Ljava/lang/String;

.field private ak:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private al:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private final am:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final an:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private final ao:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final ap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private aq:Lcom/sec/chaton/buddy/a/c;

.field private ar:Lcom/sec/chaton/buddy/ag;

.field private as:Lcom/sec/chaton/buddy/cz;

.field private at:Lcom/sec/widget/ChoicePanel;

.field private au:Z

.field private av:Landroid/app/ProgressDialog;

.field private aw:Landroid/app/ProgressDialog;

.field private ax:Lcom/sec/common/a/d;

.field private ay:Z

.field private az:Z

.field private bA:Lcom/sec/chaton/buddy/cw;

.field private bB:Z

.field private bC:I

.field private bD:I

.field private bE:Lcom/sec/chaton/buddy/cx;

.field private bF:Z

.field private bG:Z

.field private bH:Landroid/view/View;

.field private bI:Landroid/view/View;

.field private bJ:Landroid/view/View;

.field private bK:Landroid/view/View;

.field private bL:Landroid/view/View;

.field private bM:Landroid/view/View;

.field private bN:Landroid/view/View;

.field private bO:Landroid/view/View;

.field private final bP:I

.field private bR:Landroid/widget/RelativeLayout;

.field private bS:Landroid/view/View;

.field private bT:Landroid/widget/TextView;

.field private final bU:I

.field private bV:Ljava/lang/String;

.field private bW:Lcom/sec/chaton/buddy/db;

.field private bX:Lcom/sec/chaton/buddy/da;

.field private bY:Landroid/view/View$OnClickListener;

.field private bZ:Landroid/widget/CheckBox;

.field private ba:Z

.field private bb:Lcom/sec/chaton/buddy/a/c;

.field private bc:Z

.field private bd:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private be:Lcom/sec/chaton/buddy/cy;

.field private bf:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private bg:Z

.field private bh:Landroid/view/View;

.field private bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

.field private bj:Landroid/view/View;

.field private bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

.field private bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

.field private bm:Z

.field private bn:Z

.field private bo:Ljava/lang/String;

.field private bp:Lcom/sec/common/a/a;

.field private br:I

.field private bs:I

.field private bt:Lcom/sec/chaton/buddy/a/c;

.field private bu:Landroid/view/MenuItem;

.field private bv:Landroid/view/MenuItem;

.field private bw:Landroid/view/View;

.field private bx:Lcom/sec/chaton/widget/ClearableEditText;

.field private by:Landroid/view/View;

.field private bz:Landroid/widget/ImageButton;

.field private ca:Landroid/text/TextWatcher;

.field private cb:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private cc:Landroid/os/Handler;

.field private cd:Landroid/os/Handler;

.field private ce:I

.field private cf:I

.field private cg:I

.field private ch:I

.field private ci:Lcom/coolots/sso/a/a;

.field private cj:Landroid/os/Handler;

.field private ck:I

.field private cl:I

.field private cm:I

.field private cn:I

.field private co:Z

.field private cp:I

.field private cq:Lcom/sec/chaton/util/bs;

.field private cr:Landroid/os/Handler;

.field private cs:Landroid/content/BroadcastReceiver;

.field private ct:Landroid/content/BroadcastReceiver;

.field private cu:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

.field private cv:Landroid/view/View$OnClickListener;

.field protected f:Lcom/sec/chaton/buddy/a/b;

.field public g:Lcom/sec/chaton/buddy/BuddyFragment;

.field h:Z

.field public j:Lcom/coolots/sso/a/a;

.field p:Lcom/sec/chaton/e/a/v;

.field q:Landroid/os/Handler;

.field r:Lcom/sec/chaton/e/b/d;

.field s:Lcom/sec/chaton/e/b/d;

.field t:Lcom/sec/chaton/e/b/d;

.field u:Landroid/database/ContentObserver;

.field private v:Z

.field private w:I

.field private x:Ljava/lang/String;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 179
    const-class v0, Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    .line 281
    const-string v0, "purposeGroupProfile"

    sput-object v0, Lcom/sec/chaton/buddy/BuddyFragment;->b:Ljava/lang/String;

    .line 409
    sput v1, Lcom/sec/chaton/buddy/BuddyFragment;->c:I

    sput v1, Lcom/sec/chaton/buddy/BuddyFragment;->d:I

    .line 410
    sput-boolean v1, Lcom/sec/chaton/buddy/BuddyFragment;->e:Z

    .line 580
    sput-boolean v1, Lcom/sec/chaton/buddy/BuddyFragment;->i:Z

    .line 599
    sput-boolean v1, Lcom/sec/chaton/buddy/BuddyFragment;->l:Z

    .line 613
    sput-boolean v1, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 616
    const-string v0, "PROFILE_CHAT_ACTION"

    sput-object v0, Lcom/sec/chaton/buddy/BuddyFragment;->n:Ljava/lang/String;

    .line 625
    const-string v0, "PROFILE_LIST_REFRESH_ACTION"

    sput-object v0, Lcom/sec/chaton/buddy/BuddyFragment;->o:Ljava/lang/String;

    .line 645
    sput-boolean v1, Lcom/sec/chaton/buddy/BuddyFragment;->bQ:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 174
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 280
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->v:Z

    .line 306
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    .line 339
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->x:Ljava/lang/String;

    .line 385
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->G:Lcom/sec/chaton/d/w;

    .line 417
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Q:Z

    .line 418
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->R:Z

    .line 448
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    .line 449
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ae:I

    .line 454
    iput v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    .line 457
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ag:I

    .line 461
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ah:[Ljava/lang/String;

    .line 462
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ai:[Ljava/lang/String;

    .line 463
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aj:[Ljava/lang/String;

    .line 466
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    .line 467
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    .line 470
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    .line 471
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->an:Ljava/util/ArrayList;

    .line 474
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ao:Ljava/util/ArrayList;

    .line 475
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ap:Ljava/util/ArrayList;

    .line 482
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    .line 489
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->au:Z

    .line 491
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->av:Landroid/app/ProgressDialog;

    .line 492
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aw:Landroid/app/ProgressDialog;

    .line 493
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ax:Lcom/sec/common/a/d;

    .line 495
    iput-boolean v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    .line 496
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->az:Z

    .line 497
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aA:Z

    .line 498
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aB:Z

    .line 505
    sget-object v0, Lcom/sec/chaton/w;->e:Lcom/sec/chaton/w;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aG:Lcom/sec/chaton/w;

    .line 516
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aK:Ljava/util/ArrayList;

    .line 518
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aL:Ljava/util/ArrayList;

    .line 521
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aM:Ljava/util/HashMap;

    .line 557
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bc:Z

    .line 576
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bm:Z

    .line 577
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bn:Z

    .line 579
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bo:Ljava/lang/String;

    .line 596
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->br:I

    .line 597
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bs:I

    .line 618
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bB:Z

    .line 642
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bP:I

    .line 654
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bU:I

    .line 1487
    new-instance v0, Lcom/sec/chaton/buddy/cs;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/cs;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->p:Lcom/sec/chaton/e/a/v;

    .line 2753
    new-instance v0, Lcom/sec/chaton/buddy/bh;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/bh;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bY:Landroid/view/View$OnClickListener;

    .line 2956
    new-instance v0, Lcom/sec/chaton/buddy/bj;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/bj;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ca:Landroid/text/TextWatcher;

    .line 3304
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    .line 5691
    new-instance v0, Lcom/sec/chaton/buddy/bt;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/bt;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cc:Landroid/os/Handler;

    .line 6300
    new-instance v0, Lcom/sec/chaton/buddy/bx;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/bx;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cd:Landroid/os/Handler;

    .line 6353
    new-instance v0, Lcom/sec/chaton/buddy/bz;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/bz;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->q:Landroid/os/Handler;

    .line 6530
    new-instance v0, Lcom/sec/chaton/buddy/cb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/cb;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->r:Lcom/sec/chaton/e/b/d;

    .line 6618
    new-instance v0, Lcom/sec/chaton/buddy/cc;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/cc;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->s:Lcom/sec/chaton/e/b/d;

    .line 8597
    new-instance v0, Lcom/sec/chaton/buddy/cd;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/cd;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->t:Lcom/sec/chaton/e/b/d;

    .line 8696
    new-instance v0, Lcom/sec/chaton/buddy/ce;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/ce;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->u:Landroid/database/ContentObserver;

    .line 8943
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ci:Lcom/coolots/sso/a/a;

    .line 9088
    new-instance v0, Lcom/sec/chaton/buddy/cf;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/cf;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cj:Landroid/os/Handler;

    .line 9303
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ck:I

    .line 9304
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cl:I

    .line 9305
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9306
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    .line 9309
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    .line 9632
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    .line 9658
    new-instance v0, Lcom/sec/chaton/buddy/ch;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ch;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cq:Lcom/sec/chaton/util/bs;

    .line 9678
    new-instance v0, Lcom/sec/chaton/buddy/ci;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ci;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cr:Landroid/os/Handler;

    .line 9826
    new-instance v0, Lcom/sec/chaton/buddy/cj;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/cj;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cs:Landroid/content/BroadcastReceiver;

    .line 10169
    new-instance v0, Lcom/sec/chaton/buddy/cn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/cn;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ct:Landroid/content/BroadcastReceiver;

    .line 10441
    new-instance v0, Lcom/sec/chaton/buddy/co;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/co;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cu:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    .line 10582
    new-instance v0, Lcom/sec/chaton/buddy/cq;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/cq;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cv:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    return-object v0
.end method

.method private A()V
    .locals 2

    .prologue
    .line 2924
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->au:Z

    if-nez v0, :cond_1

    .line 2925
    const-string v0, "refreshList()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 2926
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_0

    .line 2927
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ca:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->b(Landroid/text/TextWatcher;)V

    .line 2932
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ca:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 2935
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_1

    .line 2938
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/cz;->a(I)V

    .line 2941
    :cond_1
    return-void
.end method

.method static synthetic B(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bZ:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private B()V
    .locals 2

    .prologue
    .line 2944
    const-string v0, "forceRefreshList()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 2945
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_0

    .line 2946
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ca:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->b(Landroid/text/TextWatcher;)V

    .line 2947
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2948
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->au:Z

    .line 2949
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ca:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 2951
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_1

    .line 2952
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/cz;->a(I)V

    .line 2954
    :cond_1
    return-void
.end method

.method private C()Z
    .locals 25

    .prologue
    .line 3090
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 3092
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "status_message"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3093
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Push Name"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3095
    new-instance v1, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0094

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, ""

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const-string v22, ""

    const/16 v23, 0x0

    invoke-direct/range {v1 .. v23}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;I)V

    .line 3121
    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3123
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 3124
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    const/4 v10, 0x0

    new-instance v1, Lcom/sec/chaton/buddy/a/a;

    const/4 v2, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0094

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, -0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v9, v10, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 3125
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    const/4 v2, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 3128
    :cond_0
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic C(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    return v0
.end method

.method static synthetic D(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->am()V

    return-void
.end method

.method private D()Z
    .locals 32

    .prologue
    .line 3722
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "buddy_name ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 3723
    const/16 v24, -0x1

    .line 3724
    const/16 v23, -0x1

    .line 3725
    if-nez v25, :cond_0

    .line 3726
    const/4 v2, 0x0

    .line 3798
    :goto_0
    return v2

    .line 3728
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addSpecialBuddyDataSet "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3729
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 3732
    const-string v2, "buddy_no"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 3733
    const-string v2, "buddy_name"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 3734
    const-string v2, "msgstatus"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 3741
    const-string v2, "isNew"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 3742
    const-string v2, "is_hide"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 3748
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ch:I

    .line 3749
    const/4 v2, -0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3750
    :goto_1
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3752
    const/4 v2, -0x1

    move/from16 v0, v31

    if-eq v0, v2, :cond_1

    move-object/from16 v0, v25

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3753
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ch:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ch:I

    goto :goto_1

    .line 3757
    :cond_1
    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v25

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v25

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "Y"

    move-object/from16 v0, v25

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-direct/range {v2 .. v22}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZI)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3775
    :cond_2
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 3776
    const/4 v4, 0x0

    move/from16 v3, v23

    move/from16 v5, v24

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_4

    .line 3777
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/buddy/a/a;

    .line 3778
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_3

    move v2, v3

    move v3, v4

    .line 3776
    :goto_3
    add-int/lit8 v4, v4, 0x1

    move v5, v3

    move v3, v2

    goto :goto_2

    .line 3780
    :cond_3
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v2

    const/4 v6, 0x1

    if-ne v2, v6, :cond_9

    move v2, v4

    move v3, v5

    .line 3781
    goto :goto_3

    .line 3785
    :cond_4
    if-lez v5, :cond_6

    .line 3786
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    add-int/lit8 v11, v5, 0x1

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    const/4 v3, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b03c2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v10, v11, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 3787
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3797
    :cond_5
    :goto_4
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 3798
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_8

    const/4 v2, 0x1

    goto/16 :goto_0

    .line 3788
    :cond_6
    if-lez v3, :cond_7

    .line 3789
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    add-int/lit8 v11, v3, 0x1

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    const/4 v3, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b03c2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v10, v11, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 3790
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 3792
    :cond_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    const/4 v3, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b03c2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3793
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 3798
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_9
    move v2, v3

    move v3, v5

    goto/16 :goto_3
.end method

.method static synthetic E(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->al()V

    return-void
.end method

.method private E()Z
    .locals 11

    .prologue
    const/4 v8, 0x5

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 3802
    const-string v0, "addBirthdayDataSet"

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3803
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "MM-dd"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 3804
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move v2, v1

    .line 3807
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 3808
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    .line 3810
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v4

    const/4 v6, 0x3

    if-eq v4, v6, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    const/4 v4, 0x6

    if-ne v0, v4, :cond_4

    .line 3812
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 3815
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 3816
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0xa

    if-ne v6, v7, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v8, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3817
    :cond_3
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3807
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 3824
    :cond_5
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    move v8, v1

    .line 3825
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_7

    .line 3826
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    .line 3827
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    if-lez v0, :cond_6

    .line 3828
    iget-object v10, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v4, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v10, v8, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 3829
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v8, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 3835
    :goto_3
    return v5

    .line 3825
    :cond_6
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_2

    :cond_7
    move v5, v1

    .line 3835
    goto :goto_3
.end method

.method static synthetic F(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    return-object v0
.end method

.method private F()Z
    .locals 2

    .prologue
    .line 4603
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4604
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 4606
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private G()V
    .locals 11

    .prologue
    const v10, 0x7f0b00b7

    const/4 v5, 0x1

    const/4 v1, -0x1

    const/4 v6, 0x0

    .line 4633
    const-string v0, "createNewBuddyGroup"

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4634
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 4635
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 4637
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 4638
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 4639
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->m()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 4640
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4641
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4646
    :cond_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 4647
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ag:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ag:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_5

    .line 4648
    :cond_3
    iget-object v9, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v4, v1

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v9, v6, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 4649
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v6, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 4655
    :cond_4
    :goto_1
    return-void

    .line 4651
    :cond_5
    iget-object v9, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v4, v1

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v9, v5, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 4652
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v5, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method static synthetic G(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->H()V

    return-void
.end method

.method static synthetic H(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->O:Landroid/view/View;

    return-object v0
.end method

.method private H()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 4658
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 4661
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->au:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 4732
    :cond_0
    :goto_0
    return-void

    .line 4666
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_0

    move v1, v2

    .line 4670
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 4677
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->isGroupExpanded(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4679
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_2

    .line 4680
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4670
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 4684
    :catch_0
    move-exception v0

    .line 4685
    const-string v0, "isn\'t GroupExpanded"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4695
    :cond_3
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v5, :cond_0

    .line 4696
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "BUDDY_SORT_STYLE"

    iget v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4710
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_5

    .line 4711
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 4712
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "RELATIONSHIP_FOLDER_STATE"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 4714
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "RELATIONSHIP_FOLDER_STATE"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 4716
    :cond_5
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_7

    .line 4717
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    .line 4718
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ALPHABET_FOLDER_STATE"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 4720
    :cond_6
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ALPHABET_FOLDER_STATE"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 4722
    :cond_7
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    .line 4723
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 4724
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CHATONV_FOLDER_STATE"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 4726
    :cond_8
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CHATONV_FOLDER_STATE"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method

.method static synthetic I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    return-object v0
.end method

.method private I()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 4748
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "RELATIONSHIP_FOLDER_STATE"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4752
    if-eqz v0, :cond_0

    .line 4753
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ah:[Ljava/lang/String;

    .line 4759
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ALPHABET_FOLDER_STATE"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4763
    if-eqz v0, :cond_1

    .line 4764
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ai:[Ljava/lang/String;

    .line 4769
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CHATONV_FOLDER_STATE"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4771
    if-eqz v0, :cond_2

    .line 4772
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aj:[Ljava/lang/String;

    .line 4777
    :goto_2
    return-void

    .line 4755
    :cond_0
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ah:[Ljava/lang/String;

    goto :goto_0

    .line 4766
    :cond_1
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ai:[Ljava/lang/String;

    goto :goto_1

    .line 4774
    :cond_2
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aj:[Ljava/lang/String;

    goto :goto_2
.end method

.method private J()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 4781
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->au:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v2, 0x6

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0x12

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 4782
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_d

    .line 4783
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1, v0}, Lcom/sec/widget/FastScrollableExpandableListView;->expandGroup(I)Z

    .line 4782
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4788
    :cond_1
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    .line 4789
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 4790
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    if-ge v0, v6, :cond_2

    .line 4791
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->collapseGroup(I)Z

    .line 4789
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4794
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->expandGroup(I)Z

    goto :goto_2

    .line 4841
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->I()V

    move v0, v1

    .line 4844
    :goto_3
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_5

    .line 4845
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v2, :cond_4

    .line 4846
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v2, v0}, Lcom/sec/widget/FastScrollableExpandableListView;->expandGroup(I)Z

    .line 4844
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 4850
    :cond_5
    const/4 v0, 0x0

    .line 4858
    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    const/16 v3, 0xc

    if-ne v2, v3, :cond_9

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ah:[Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 4859
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ah:[Ljava/lang/String;

    move-object v4, v0

    .line 4877
    :goto_4
    if-eqz v4, :cond_c

    move v2, v1

    .line 4878
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    move v3, v1

    .line 4879
    :goto_6
    array-length v0, v4

    if-ge v3, v0, :cond_6

    .line 4882
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->b()Ljava/lang/String;

    move-result-object v0

    aget-object v5, v4, v3

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 4883
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_6

    .line 4884
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/FastScrollableExpandableListView;->collapseGroup(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4894
    :cond_6
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_8

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ae:I

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    if-eq v0, v3, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    if-lt v0, v6, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    const/4 v3, 0x4

    if-eq v0, v3, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    const/4 v3, 0x7

    if-ne v0, v3, :cond_8

    .line 4896
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_8

    .line 4897
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/FastScrollableExpandableListView;->collapseGroup(I)Z

    .line 4878
    :cond_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 4861
    :cond_9
    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_a

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ai:[Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 4862
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ai:[Ljava/lang/String;

    move-object v4, v0

    goto :goto_4

    .line 4864
    :cond_a
    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    const/16 v3, 0xd

    if-ne v2, v3, :cond_e

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aj:[Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 4865
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aj:[Ljava/lang/String;

    move-object v4, v0

    goto/16 :goto_4

    .line 4888
    :catch_0
    move-exception v0

    .line 4879
    :cond_b
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_6

    .line 4914
    :cond_c
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ae:I

    .line 4915
    :cond_d
    return-void

    :cond_e
    move-object v4, v0

    goto/16 :goto_4
.end method

.method static synthetic J(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->A()V

    return-void
.end method

.method static synthetic K(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    return v0
.end method

.method private K()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 5547
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 5548
    invoke-static {v3}, Lcom/sec/chaton/e/a/f;->b(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v4

    .line 5549
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 5550
    invoke-static {v3}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v6

    move v1, v2

    .line 5552
    :goto_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 5553
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v0}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;

    move-result-object v7

    .line 5554
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v8, "Favorites"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5555
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5552
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 5558
    :cond_1
    const-string v0, "Favorites"

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 5560
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 5561
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Is Group Uploaded"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 5568
    :goto_1
    return-void

    .line 5564
    :cond_2
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cc:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 5565
    const-string v1, "group"

    const/16 v3, 0x148

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZILjava/util/ArrayList;Ljava/util/HashMap;)I

    goto :goto_1
.end method

.method static synthetic L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    return-object v0
.end method

.method private L()V
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 5571
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 5573
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 5575
    invoke-static {v1}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v4

    .line 5576
    invoke-static {v1}, Lcom/sec/chaton/e/a/f;->b(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v5

    move v1, v0

    .line 5580
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 5581
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v6, "Favorites"

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-eqz v0, :cond_1

    .line 5580
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 5586
    :cond_1
    :try_start_1
    new-instance v6, Ljava/io/File;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "_group_profile.png_"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v7, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 5587
    new-instance v7, Ljava/io/File;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "_group_profile.png_"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v8, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 5588
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5589
    invoke-virtual {v6, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    .line 5590
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[LOCAL STORAGE]group image name changed "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " to "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " is "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 5597
    :cond_2
    :goto_2
    :try_start_2
    new-instance v6, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string v7, "//profile"

    invoke-direct {v6, v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5598
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5599
    new-instance v7, Ljava/io/File;

    const-string v8, "%s_big.jpeg_"

    const/4 v0, 0x1

    new-array v9, v0, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 5600
    new-instance v0, Ljava/io/File;

    const-string v8, "%s_big.jpeg_"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v6, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 5601
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 5602
    invoke-virtual {v7, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    .line 5603
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[SDCARD]group image name changed "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " to "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " is "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 5611
    :cond_3
    :goto_3
    :try_start_3
    new-instance v6, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "/profile/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "%s_big.jpeg_"

    const/4 v0, 0x1

    new-array v9, v0, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5612
    new-instance v0, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/profile/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "%s_big.jpeg_"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5613
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 5614
    invoke-virtual {v6, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    .line 5615
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[NEW SDCARD]group image name changed "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " to "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " is "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 5622
    :cond_4
    :goto_4
    :try_start_4
    new-instance v6, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5623
    new-instance v7, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "/"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "/"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5624
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5625
    invoke-virtual {v6, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    .line 5626
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[CACHE DIR]group image name changed "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " to "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " is "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_1

    .line 5628
    :catch_0
    move-exception v0

    .line 5630
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_1

    .line 5637
    :catch_1
    move-exception v0

    .line 5638
    const-string v0, "group image name changed failed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 5640
    :goto_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 5641
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "group image name changed delay "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long/2addr v0, v2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " milles"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 5642
    return-void

    .line 5592
    :catch_2
    move-exception v0

    .line 5594
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 5606
    :catch_3
    move-exception v0

    .line 5608
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 5617
    :catch_4
    move-exception v0

    .line 5619
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 5634
    :cond_5
    invoke-direct {p0, v5}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/util/ArrayList;)V

    .line 5636
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Is Group Profile Image Name Changed"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_5
.end method

.method private M()V
    .locals 3

    .prologue
    .line 8571
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 8572
    if-eqz v0, :cond_0

    .line 8573
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 8577
    :cond_0
    :goto_0
    return-void

    .line 8575
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic M(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->J()V

    return-void
.end method

.method static synthetic N(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ax:Lcom/sec/common/a/d;

    return-object v0
.end method

.method private N()V
    .locals 3

    .prologue
    .line 8583
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 8589
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v1, :cond_0

    .line 8590
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 8595
    :cond_0
    :goto_0
    return-void

    .line 8593
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic O(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ae:I

    return v0
.end method

.method private O()V
    .locals 4

    .prologue
    .line 8664
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 8665
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 8666
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->m()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->w()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8668
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->f()V

    .line 8674
    :cond_3
    return-void
.end method

.method static synthetic P(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cd:Landroid/os/Handler;

    return-object v0
.end method

.method private P()V
    .locals 5

    .prologue
    .line 8678
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 8680
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 8681
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 8682
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->m()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 8683
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 8688
    :cond_2
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0xa

    if-eq v0, v2, :cond_3

    .line 8689
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 8691
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->g()V

    .line 8694
    :cond_3
    return-void
.end method

.method static synthetic Q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aq:Lcom/sec/chaton/buddy/a/c;

    return-object v0
.end method

.method private Q()V
    .locals 3

    .prologue
    .line 8783
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/AddBuddyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8784
    const-string v1, "ADD_BUDDY_TYPE"

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8785
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 8786
    return-void
.end method

.method static synthetic R(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->Y()V

    return-void
.end method

.method private R()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8928
    const/4 v0, 0x1

    .line 8930
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v2

    .line 8931
    const/4 v3, -0x3

    if-eq v3, v2, :cond_0

    const/4 v3, -0x2

    if-ne v3, v2, :cond_1

    .line 8932
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0205

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 8939
    :cond_1
    return v0
.end method

.method static synthetic S(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->Z()V

    return-void
.end method

.method private S()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 8948
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ah()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8949
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8950
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    .line 8958
    :goto_0
    return v0

    .line 8953
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->af()Z

    move-result v1

    if-nez v1, :cond_1

    .line 8954
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8955
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 8958
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private T()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 8963
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->S()Z

    move-result v0

    if-nez v0, :cond_1

    .line 8999
    :cond_0
    :goto_0
    return-void

    .line 8966
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8969
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    invoke-virtual {v0, v7}, Lcom/sec/widget/ChoicePanel;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 8970
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v7

    .line 8971
    :goto_1
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 8972
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8971
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8976
    :cond_2
    :try_start_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 8977
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ci:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 8992
    :goto_2
    if-eqz v0, :cond_3

    .line 8993
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008a

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 8995
    :cond_3
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bc:Z

    if-nez v0, :cond_0

    .line 8996
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 8981
    :cond_4
    :try_start_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 8983
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ci:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_2

    .line 8987
    :catch_0
    move-exception v0

    .line 8988
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 8989
    const/4 v0, -0x1

    goto :goto_2
.end method

.method static synthetic T(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->L()V

    return-void
.end method

.method private U()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 9003
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->S()Z

    move-result v0

    if-nez v0, :cond_1

    .line 9039
    :cond_0
    :goto_0
    return-void

    .line 9006
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9009
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    invoke-virtual {v0, v7}, Lcom/sec/widget/ChoicePanel;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 9010
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v7

    .line 9011
    :goto_1
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 9012
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 9011
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 9016
    :cond_2
    :try_start_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 9017
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ci:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 9032
    :goto_2
    if-eqz v0, :cond_3

    .line 9033
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008a

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 9035
    :cond_3
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bc:Z

    if-nez v0, :cond_0

    .line 9036
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 9021
    :cond_4
    :try_start_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 9023
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ci:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_2

    .line 9027
    :catch_0
    move-exception v0

    .line 9028
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 9029
    const/4 v0, -0x1

    goto :goto_2
.end method

.method static synthetic U(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->K()V

    return-void
.end method

.method static synthetic V(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bb:Lcom/sec/chaton/buddy/a/c;

    return-object v0
.end method

.method private V()V
    .locals 9

    .prologue
    const v8, 0x7f0b0077

    const/16 v7, 0xc8

    const/16 v6, 0xc7

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9139
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    .line 9140
    const-string v1, "Button.OnClickListener() : INVITE BUDDY"

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 9141
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 9142
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mBundle.getStringArray(ChatFragment.EXTRA_PARTICIPANT).length : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 9143
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mBundle.getInt(ChatFragment.KEY_CHAT_TYPE) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v3, "chatType"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 9145
    if-lez v0, :cond_1

    .line 9147
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v2, "chatType"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v2, "chatType"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 9152
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    add-int/2addr v0, v1

    if-le v0, v6, :cond_3

    .line 9154
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 9180
    :cond_1
    :goto_0
    return-void

    .line 9162
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v2, "chatType"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 9163
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    sget-object v2, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    add-int/2addr v0, v1

    if-le v0, v7, :cond_3

    .line 9165
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 9174
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 9176
    const-string v1, "receivers"

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 9177
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 9178
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic W(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->av:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private W()V
    .locals 3

    .prologue
    .line 9183
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 9184
    const-string v1, "blindlist"

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 9185
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 9186
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 9187
    return-void
.end method

.method static synthetic X(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/ChoicePanel;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    return-object v0
.end method

.method private X()V
    .locals 3

    .prologue
    .line 9190
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 9191
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->e()V

    .line 9220
    :cond_0
    :goto_0
    return-void

    .line 9192
    :cond_1
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 9193
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->f()V

    goto :goto_0

    .line 9196
    :cond_2
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_3

    .line 9197
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->T()V

    goto :goto_0

    .line 9199
    :cond_3
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_4

    .line 9200
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->U()V

    goto :goto_0

    .line 9209
    :cond_4
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_5

    .line 9210
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 9211
    :cond_5
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_6

    .line 9212
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->V()V

    goto :goto_0

    .line 9213
    :cond_6
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_7

    .line 9214
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->W()V

    goto :goto_0

    .line 9215
    :cond_7
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_8

    .line 9216
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->c([Ljava/lang/String;)V

    goto :goto_0

    .line 9217
    :cond_8
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9218
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bX:Lcom/sec/chaton/buddy/da;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/buddy/da;->a(Ljava/util/ArrayList;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method private Y()V
    .locals 4

    .prologue
    .line 9271
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 9279
    :goto_0
    return-void

    .line 9274
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->av:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 9275
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->av:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 9277
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->av:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic Y(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->B()V

    return-void
.end method

.method static synthetic Z(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/ag;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    return-object v0
.end method

.method private Z()V
    .locals 1

    .prologue
    .line 9282
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 9288
    :cond_0
    :goto_0
    return-void

    .line 9285
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->av:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 9286
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->av:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aw:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/a/c;)Lcom/sec/chaton/buddy/a/c;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bb:Lcom/sec/chaton/buddy/a/c;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/sec/chaton/buddy/a/c;
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/sec/chaton/buddy/a/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;>;)",
            "Lcom/sec/chaton/buddy/a/c;"
        }
    .end annotation

    .prologue
    .line 8766
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 8767
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 8768
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 8774
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/d/w;)Lcom/sec/chaton/d/w;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->G:Lcom/sec/chaton/d/w;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aZ:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0

    .prologue
    .line 174
    sput-object p0, Lcom/sec/chaton/buddy/BuddyFragment;->bq:Ljava/util/HashMap;

    return-object p0
.end method

.method public static a(Landroid/content/ClipData;Lcom/sec/chaton/buddy/a/c;Landroid/content/Context;)V
    .locals 7

    .prologue
    const/high16 v6, 0x4000000

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 2527
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v2, p2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2528
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2529
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2530
    const-string v0, "chatType"

    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2531
    const-string v0, "groupId"

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2532
    const-string v3, "receivers"

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v4, v1, [Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2537
    :goto_0
    if-eqz p0, :cond_0

    .line 2538
    invoke-virtual {p0}, Landroid/content/ClipData;->getItemCount()I

    .line 2546
    invoke-virtual {p0}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "image/*"

    invoke-static {v0, v3}, Landroid/content/ClipDescription;->compareMimeTypes(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2547
    sget-object v0, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-virtual {v0}, Lcom/sec/chaton/e/w;->a()I

    move-result v0

    .line 2554
    :goto_1
    invoke-virtual {p0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v3

    .line 2556
    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 2557
    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v3

    .line 2558
    sget-object v4, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-virtual {v4}, Lcom/sec/chaton/e/w;->a()I

    move-result v4

    if-ne v0, v4, :cond_3

    .line 2559
    const-string v0, "content_type"

    sget-object v4, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-virtual {v4}, Lcom/sec/chaton/e/w;->a()I

    move-result v4

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2560
    const-string v0, "download_uri"

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2572
    :goto_2
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2575
    const-string v0, "is_forward_mode"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2576
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    sget-object v1, Lcom/sec/chaton/w;->e:Lcom/sec/chaton/w;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2577
    const-string v0, "is_attach_on_the_edittext"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2579
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2580
    const-string v1, "key_clipdata"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2581
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2583
    const-string v0, "buddy_dragdata"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2589
    invoke-virtual {p2, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2590
    :cond_0
    return-void

    .line 2534
    :cond_1
    const-string v0, "chatType"

    sget-object v3, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2535
    const-string v0, "receivers"

    new-array v3, v5, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 2548
    :cond_2
    invoke-virtual {p0}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "video/*"

    invoke-static {v0, v3}, Landroid/content/ClipDescription;->compareMimeTypes(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2549
    sget-object v0, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-virtual {v0}, Lcom/sec/chaton/e/w;->a()I

    move-result v0

    goto/16 :goto_1

    .line 2561
    :cond_3
    sget-object v4, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-virtual {v4}, Lcom/sec/chaton/e/w;->a()I

    move-result v4

    if-ne v0, v4, :cond_4

    .line 2562
    const-string v0, "content_type"

    sget-object v4, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-virtual {v4}, Lcom/sec/chaton/e/w;->a()I

    move-result v4

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2563
    const-string v0, "download_uri"

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 2565
    :cond_4
    const-string v0, "content_type"

    sget-object v4, Lcom/sec/chaton/e/w;->r:Lcom/sec/chaton/e/w;

    invoke-virtual {v4}, Lcom/sec/chaton/e/w;->a()I

    move-result v4

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2566
    const-string v0, "download_uri"

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 2569
    :cond_5
    const-string v0, "content_type"

    sget-object v3, Lcom/sec/chaton/e/w;->r:Lcom/sec/chaton/e/w;

    invoke-virtual {v3}, Lcom/sec/chaton/e/w;->a()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_2

    :cond_6
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Landroid/database/Cursor;Z)V
    .locals 57

    .prologue
    .line 3307
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addBuddiesByNameDataSetFromCursor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3308
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 3309
    new-instance v36, Ljava/util/HashMap;

    invoke-direct/range {v36 .. v36}, Ljava/util/HashMap;-><init>()V

    .line 3312
    const/4 v5, 0x0

    .line 3313
    const/4 v4, 0x2

    .line 3314
    const/4 v3, 0x0

    .line 3319
    const-string v2, "buddy_no"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v37

    .line 3320
    const-string v2, "buddy_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v38

    .line 3321
    const-string v2, "buddy_status_message"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    .line 3322
    const-string v2, "buddy_raw_contact_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v40

    .line 3323
    const-string v2, "buddy_push_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    .line 3324
    const-string v2, "buddy_is_new"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v42

    .line 3325
    const-string v2, "buddy_samsung_email"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    .line 3326
    const-string v2, "buddy_orginal_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v44

    .line 3327
    const-string v2, "buddy_birthday"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v45

    .line 3328
    const-string v2, "buddy_relation_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v46

    .line 3329
    const-string v2, "buddy_profile_status"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v47

    .line 3330
    const-string v2, "buddy_is_profile_updated"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v48

    .line 3331
    const-string v2, "buddy_is_status_updated"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v49

    .line 3332
    const-string v2, "buddy_show_phone_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v50

    .line 3333
    const-string v2, "buddy_extra_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v51

    .line 3334
    const-string v2, "buddy_hanzitopinyin"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v52

    .line 3335
    const-string v2, "buddy_account_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v53

    .line 3336
    const-string v2, "buddy_sainfo"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v54

    .line 3338
    if-eqz p2, :cond_5

    const-string v2, "relation_send"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move/from16 v35, v2

    .line 3339
    :goto_0
    if-eqz p2, :cond_6

    const-string v2, "relation_received"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move/from16 v34, v2

    .line 3340
    :goto_1
    if-eqz p2, :cond_7

    const-string v2, "relation_point"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move/from16 v33, v2

    .line 3341
    :goto_2
    if-eqz p2, :cond_8

    const-string v2, "relation_icon"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move/from16 v32, v2

    .line 3342
    :goto_3
    if-eqz p2, :cond_9

    const-string v2, "relation_increase"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    .line 3343
    :goto_4
    if-eqz p2, :cond_a

    const-string v2, "relation_rank"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    .line 3344
    :goto_5
    const-string v2, "group_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v55

    .line 3345
    const-string v2, "buddy_is_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v56

    .line 3346
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 3348
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    .line 3350
    const/4 v2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-object v2, v3

    move/from16 v28, v4

    move-object/from16 v30, v6

    move v3, v5

    .line 3351
    :cond_0
    :goto_6
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 3353
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v5, 0x3e8

    if-ne v4, v5, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    const-string v5, "voip"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3354
    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "voip=1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3361
    :cond_1
    const/4 v4, -0x1

    move/from16 v0, v55

    if-eq v0, v4, :cond_2

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 3362
    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 3363
    if-nez v4, :cond_0

    .line 3369
    :cond_2
    const/4 v4, -0x1

    move/from16 v0, v52

    if-eq v0, v4, :cond_b

    .line 3370
    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3374
    :goto_7
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3375
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    move-result-object v2

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/chaton/util/at;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3377
    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1a

    .line 3378
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/chaton/buddy/BuddyFragment;->v:Z

    if-eqz v4, :cond_0

    .line 3379
    const-string v27, "."

    .line 3385
    :goto_8
    if-lez v55, :cond_c

    .line 3386
    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3387
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v5, 0xa

    if-ne v4, v5, :cond_c

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v2, v27

    .line 3388
    goto/16 :goto_6

    .line 3338
    :cond_5
    const/4 v2, -0x1

    move/from16 v35, v2

    goto/16 :goto_0

    .line 3339
    :cond_6
    const/4 v2, -0x1

    move/from16 v34, v2

    goto/16 :goto_1

    .line 3340
    :cond_7
    const/4 v2, -0x1

    move/from16 v33, v2

    goto/16 :goto_2

    .line 3341
    :cond_8
    const/4 v2, -0x1

    move/from16 v32, v2

    goto/16 :goto_3

    .line 3342
    :cond_9
    const/4 v2, -0x1

    move/from16 v31, v2

    goto/16 :goto_4

    .line 3343
    :cond_a
    const/4 v2, -0x1

    move/from16 v26, v2

    goto/16 :goto_5

    .line 3372
    :cond_b
    const-string v4, "query without hanzitopinyin"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 3391
    :cond_c
    const/4 v2, -0x1

    move/from16 v0, v56

    if-eq v0, v2, :cond_d

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "Y"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 3392
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    move-object/from16 v2, v27

    .line 3393
    goto/16 :goto_6

    .line 3396
    :cond_d
    const/4 v2, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/util/bf;->a(C)C

    move-result v29

    .line 3398
    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_e

    const/4 v9, 0x1

    :goto_9
    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz p2, :cond_f

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    :goto_a
    if-eqz p2, :cond_10

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    :goto_b
    if-eqz p2, :cond_11

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    :goto_c
    if-eqz p2, :cond_12

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    :goto_d
    if-eqz p2, :cond_13

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    :goto_e
    if-eqz p2, :cond_14

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    :goto_f
    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const-string v20, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    const-string v21, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v2 .. v25}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;)V

    .line 3412
    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3413
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3415
    invoke-static/range {v29 .. v29}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_15

    .line 3416
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3417
    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    add-int/lit8 v10, v28, 0x1

    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v3, v28

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    .line 3418
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3419
    invoke-static/range {v29 .. v29}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    move-object/from16 v0, v36

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v10

    .line 3432
    :goto_10
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move/from16 v28, v2

    move-object/from16 v30, v3

    move/from16 v3, v29

    move-object/from16 v2, v27

    .line 3433
    goto/16 :goto_6

    .line 3398
    :cond_e
    const/4 v9, 0x0

    goto/16 :goto_9

    :cond_f
    const/4 v13, 0x0

    goto/16 :goto_a

    :cond_10
    const/4 v14, 0x0

    goto/16 :goto_b

    :cond_11
    const/4 v15, 0x0

    goto/16 :goto_c

    :cond_12
    const/16 v16, 0x0

    goto/16 :goto_d

    :cond_13
    const/16 v17, 0x0

    goto/16 :goto_e

    :cond_14
    const/16 v18, 0x0

    goto/16 :goto_f

    .line 3421
    :cond_15
    invoke-static/range {v29 .. v29}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/buddy/a/a;

    .line 3422
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 3423
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 3424
    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3425
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v3, v4, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3427
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/chaton/buddy/a/a;

    .line 3428
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/sec/chaton/buddy/a/a;->a(I)V

    .line 3429
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v2, v4, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3430
    invoke-static/range {v29 .. v29}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move/from16 v2, v28

    goto :goto_10

    .line 3435
    :cond_16
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_17

    .line 3436
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    add-int/lit8 v4, v28, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v3, v28

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3437
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3440
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    if-eqz v2, :cond_18

    .line 3441
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/sec/chaton/buddy/db;->b(I)V

    .line 3444
    :cond_18
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v3, 0x3e8

    if-ne v2, v3, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    if-eqz v2, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    const-string v3, "voip"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 3445
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bp:Lcom/sec/common/a/a;

    if-nez v2, :cond_19

    .line 3446
    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bp:Lcom/sec/common/a/a;

    .line 3447
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bp:Lcom/sec/common/a/a;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02be

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 3448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bp:Lcom/sec/common/a/a;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02be

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v2

    const v3, 0x7f0b0042

    new-instance v4, Lcom/sec/chaton/buddy/bk;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/sec/chaton/buddy/bk;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 3454
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bp:Lcom/sec/common/a/a;

    invoke-virtual {v2}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 3458
    :cond_19
    invoke-virtual/range {v36 .. v36}, Ljava/util/HashMap;->clear()V

    .line 3460
    return-void

    :cond_1a
    move-object/from16 v27, v2

    goto/16 :goto_8
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const v4, 0x7f0b0077

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9312
    if-nez p1, :cond_7

    .line 9313
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    .line 9314
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ae:I

    .line 9345
    :goto_0
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v5, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v6, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x13

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x14

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xf

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xe

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_8

    .line 9368
    :cond_1
    iput v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    .line 9398
    :goto_1
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cl:I

    .line 9399
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ck:I

    .line 9400
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v5, :cond_9

    .line 9401
    const/16 v0, 0xc7

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9402
    iput v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    .line 9458
    :cond_2
    :goto_2
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_15

    .line 9459
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_CALLLOG_GROUP"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_CALLLOG_GROUP"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v3, :cond_14

    .line 9460
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    .line 9478
    :goto_3
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x13

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v5, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v6, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_1a

    .line 9489
    :cond_3
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_19

    const-string v0, "chatType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 9490
    const-string v0, "chatType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    .line 9491
    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_18

    .line 9492
    :cond_4
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    .line 9507
    :goto_4
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_6

    .line 9508
    const-string v0, "max"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9509
    const-string v0, "min"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cl:I

    .line 9510
    iput v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    .line 9511
    const-string v0, "except"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->V:[Ljava/lang/String;

    .line 9512
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->V:[Ljava/lang/String;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->V:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1b

    .line 9513
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    .line 9514
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ck:I

    .line 9515
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->V:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9521
    :goto_5
    const-string v0, "single"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ba:Z

    if-ne v0, v3, :cond_1c

    .line 9522
    :cond_5
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    .line 9523
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    .line 9530
    :cond_6
    :goto_6
    return-void

    .line 9317
    :cond_7
    const-string v0, "ACTIVITY_PURPOSE"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    .line 9318
    const-string v0, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ag:I

    .line 9320
    const-string v0, "require"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    .line 9334
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->V:[Ljava/lang/String;

    .line 9336
    const-string v0, "groupInfo"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/b;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->f:Lcom/sec/chaton/buddy/a/b;

    .line 9339
    const-string v0, "PROFILE_BUDDY_FROM_CHATINFO"

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->v:Z

    goto/16 :goto_0

    .line 9394
    :cond_8
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    goto/16 :goto_1

    .line 9403
    :cond_9
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v6, :cond_a

    .line 9404
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9405
    iput v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    goto/16 :goto_2

    .line 9407
    :cond_a
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_b

    .line 9408
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9409
    const v0, 0x7f0b03a7

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    goto/16 :goto_2

    .line 9411
    :cond_b
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_c

    .line 9412
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->br:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9413
    const v0, 0x7f0b03a7

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    goto/16 :goto_2

    .line 9415
    :cond_c
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_10

    .line 9416
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ck:I

    .line 9417
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "chatType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 9418
    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    if-eq v0, v1, :cond_d

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    if-ne v0, v1, :cond_e

    .line 9419
    :cond_d
    const/16 v0, 0xc7

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9420
    iput v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    goto/16 :goto_2

    .line 9421
    :cond_e
    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 9422
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9423
    iput v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    goto/16 :goto_2

    .line 9425
    :cond_f
    iput v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    goto/16 :goto_2

    .line 9427
    :cond_10
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_11

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_12

    .line 9428
    :cond_11
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 9429
    const/16 v0, 0xc7

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9430
    iput v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    goto/16 :goto_2

    .line 9438
    :cond_12
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_13

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_2

    .line 9439
    :cond_13
    const/16 v0, 0xc7

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 9440
    iput v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    goto/16 :goto_2

    .line 9463
    :cond_14
    iput v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    goto/16 :goto_3

    .line 9465
    :cond_15
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_16

    .line 9466
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    goto/16 :goto_3

    .line 9467
    :cond_16
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_17

    .line 9468
    iput v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    goto/16 :goto_3

    .line 9472
    :cond_17
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    goto/16 :goto_3

    .line 9494
    :cond_18
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    goto/16 :goto_4

    .line 9497
    :cond_19
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    goto/16 :goto_4

    .line 9502
    :cond_1a
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    goto/16 :goto_4

    .line 9517
    :cond_1b
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    .line 9518
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ck:I

    goto/16 :goto_5

    .line 9525
    :cond_1c
    iput v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    .line 9526
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    goto/16 :goto_6
.end method

.method private a(Landroid/view/ViewStub;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 10757
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->I:Landroid/view/View;

    if-nez v0, :cond_0

    .line 10758
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->I:Landroid/view/View;

    .line 10760
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->I:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->J:Landroid/widget/ImageView;

    .line 10761
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->J:Landroid/widget/ImageView;

    const v1, 0x7f02034b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 10763
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->I:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->K:Landroid/widget/TextView;

    .line 10764
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->K:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0155

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 10766
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->I:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->L:Landroid/widget/TextView;

    .line 10767
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->L:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 10770
    :cond_0
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 10771
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->B:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 10776
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->A:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 10777
    return-void

    .line 10773
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->B:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;I)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->d(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/view/ViewStub;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/view/ViewStub;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/a/c;I)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/a/c;I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/a/c;II)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/a/c;II)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/buddy/a/c;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 6168
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6170
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6171
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v1, :cond_4

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    .line 6179
    :goto_0
    if-ne v0, v1, :cond_3

    .line 6180
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6181
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 6182
    array-length v0, v0

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->br:I

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_1

    .line 6183
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Lcom/sec/chaton/buddy/a/c;I)V

    .line 6193
    :goto_1
    return-void

    .line 6175
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v1, :cond_4

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->F()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_4

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->F()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    .line 6176
    goto :goto_0

    .line 6185
    :cond_1
    invoke-direct {p0, p1, p2, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/a/c;II)V

    goto :goto_1

    .line 6188
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Lcom/sec/chaton/buddy/a/c;I)V

    goto :goto_1

    .line 6191
    :cond_3
    invoke-direct {p0, p1, p2, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/a/c;II)V

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method private a(Lcom/sec/chaton/buddy/a/c;II)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const v0, 0x7f0203e4

    const/4 v7, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x1

    .line 6227
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SHORTCUT] CONTEXT_MENU_ADD_SHORTCUT item="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", buddy="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6230
    if-nez p3, :cond_0

    move v3, v0

    .line 6238
    :goto_0
    const/16 v0, 0x6a

    if-ne p2, v0, :cond_9

    .line 6240
    if-nez p3, :cond_2

    .line 6241
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-static {v0, v1, v4}, Lcom/sec/chaton/util/ac;->a(Landroid/content/Context;Ljava/lang/String;Lcom/sec/chaton/e/r;)Landroid/content/Intent;

    move-result-object v0

    .line 6253
    :goto_1
    const/16 v1, 0x73

    if-ne p2, v1, :cond_8

    .line 6254
    if-nez p3, :cond_4

    .line 6255
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-static {v0, p1, v1}, Lcom/sec/chaton/util/ac;->a(Landroid/content/Context;Lcom/sec/chaton/buddy/a/c;Lcom/sec/chaton/e/r;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 6279
    :goto_2
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 6280
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/chaton/util/bt;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 6281
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 6282
    if-nez v2, :cond_7

    .line 6283
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0203e6

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 6295
    :goto_3
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0, v3}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 6296
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v2}, Lcom/sec/chaton/util/ac;->a(F)I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v3}, Lcom/sec/chaton/util/ac;->a(F)I

    move-result v3

    invoke-static {v0, v2, v3, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 6297
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3, v0}, Lcom/sec/chaton/util/ac;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 6298
    return-void

    .line 6232
    :cond_0
    if-ne p3, v6, :cond_1

    .line 6233
    const v0, 0x7f0203e8

    move v3, v0

    goto/16 :goto_0

    .line 6234
    :cond_1
    if-ne p3, v5, :cond_a

    .line 6235
    const v0, 0x7f0203e7

    move v3, v0

    goto/16 :goto_0

    .line 6242
    :cond_2
    if-ne p3, v6, :cond_3

    .line 6243
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 6244
    const-string v1, "chatonno"

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6245
    sget-object v1, Lcom/sec/chaton/u;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 6246
    :cond_3
    if-ne p3, v5, :cond_9

    .line 6247
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 6248
    const-string v1, "chatonno"

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6249
    sget-object v1, Lcom/sec/chaton/u;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 6256
    :cond_4
    if-ne p3, v6, :cond_5

    .line 6257
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 6258
    const-string v0, "groupId"

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6259
    const-string v4, "chatonno"

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v5, v7, [Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 6260
    sget-object v0, Lcom/sec/chaton/u;->k:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 6261
    :cond_5
    if-ne p3, v5, :cond_8

    .line 6262
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 6263
    const-string v0, "groupId"

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6264
    const-string v4, "chatonno"

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v5, v7, [Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 6265
    sget-object v0, Lcom/sec/chaton/u;->l:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 6286
    :cond_6
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v2

    .line 6287
    :try_start_3
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 6288
    if-nez v2, :cond_7

    .line 6289
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0203e5

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    goto/16 :goto_3

    .line 6292
    :catch_0
    move-exception v0

    move-object v0, v2

    goto/16 :goto_3

    :catch_1
    move-exception v0

    move-object v0, v2

    goto/16 :goto_3

    :cond_7
    move-object v0, v2

    goto/16 :goto_3

    :cond_8
    move-object v1, v0

    goto/16 :goto_2

    :cond_9
    move-object v0, v2

    goto/16 :goto_1

    :cond_a
    move v3, v0

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 9291
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aN:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 9293
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aN:Landroid/widget/Toast;

    .line 9296
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aN:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 9297
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aN:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 9298
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/a;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 4487
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->E:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 4488
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ce:I

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ch:I

    sub-int/2addr v0, v1

    .line 4489
    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->au:Z

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 4490
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 4492
    :cond_0
    if-le v0, v2, :cond_2

    .line 4493
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->E:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00b9

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4503
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4504
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->an:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4507
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    .line 4508
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/chaton/buddy/a/a;

    invoke-direct {v5, v0}, Lcom/sec/chaton/buddy/a/a;-><init>(Lcom/sec/chaton/buddy/a/a;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 4496
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->E:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b00ba

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 4514
    :cond_3
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 4515
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 4517
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 4518
    new-instance v1, Lcom/sec/chaton/buddy/a/c;

    invoke-direct {v1, v0}, Lcom/sec/chaton/buddy/a/c;-><init>(Lcom/sec/chaton/buddy/a/c;)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4522
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->m()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->aj()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4523
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/chaton/TabActivity;

    if-eqz v1, :cond_4

    .line 4524
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 4525
    const-class v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4526
    new-instance v8, Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v3

    :goto_4
    const/4 v11, 0x2

    invoke-direct {v8, v9, v10, v1, v11}, Lcom/sec/chaton/buddy/a/b;-><init>(ILjava/lang/String;II)V

    .line 4528
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v9, 0x12

    invoke-virtual {v7, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4529
    const-string v1, "groupInfo"

    invoke-virtual {v7, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 4530
    const-string v1, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 4531
    const-string v1, "LAUNCH_AS_CHILD"

    invoke-virtual {v7, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4532
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/TabActivity;

    const v8, 0x7f070008

    const-class v9, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1, v8, v7, v9}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 4533
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4534
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->c(Lcom/sec/chaton/buddy/a/c;)V

    .line 4536
    :cond_5
    invoke-direct {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->c(Z)V

    .line 4537
    invoke-direct {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->d(Z)V

    goto/16 :goto_3

    .line 4526
    :cond_6
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    goto :goto_4

    .line 4543
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->an:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 4547
    :cond_8
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_9

    .line 4548
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aL:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aK:Ljava/util/ArrayList;

    .line 4552
    :cond_9
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_b

    .line 4554
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 4555
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_b

    .line 4556
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->V:[Ljava/lang/String;

    array-length v1, v0

    :goto_5
    if-ge v3, v1, :cond_a

    aget-object v4, v0, v3

    .line 4557
    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aK:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4556
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 4560
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->V:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    .line 4567
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aK:Ljava/util/ArrayList;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_e

    .line 4568
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_c
    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 4569
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aM:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_c

    .line 4570
    const/4 v3, 0x0

    move-object v0, p0

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z

    goto :goto_6

    .line 4573
    :cond_d
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_e

    .line 4574
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4579
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_f

    .line 4580
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->t()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/ag;->a(Z)V

    .line 4581
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/ag;->e(Ljava/lang/String;)V

    .line 4582
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/ag;->notifyDataSetChanged()V

    .line 4585
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_10

    .line 4586
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/buddy/db;->a(I)V

    .line 4590
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_11

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 4591
    sget v0, Lcom/sec/chaton/buddy/BuddyFragment;->c:I

    if-eqz v0, :cond_11

    sget v0, Lcom/sec/chaton/buddy/BuddyFragment;->d:I

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_11

    .line 4592
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    sget v1, Lcom/sec/chaton/buddy/BuddyFragment;->c:I

    sget v2, Lcom/sec/chaton/buddy/BuddyFragment;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/widget/FastScrollableExpandableListView;->setSelectionFromTop(II)V

    .line 4600
    :cond_11
    return-void
.end method

.method private a(Landroid/database/Cursor;)Z
    .locals 51

    .prologue
    .line 3134
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addFavoriteDataSetFromCursor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3135
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 3138
    const-string v2, "group_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 3139
    const-string v2, "buddy_no"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 3140
    const-string v2, "buddy_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 3141
    const-string v2, "buddy_status_message"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 3142
    const-string v2, "buddy_samsung_email"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 3143
    const-string v2, "buddy_push_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 3144
    const-string v2, "buddy_is_new"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 3145
    const-string v2, "relation_send"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    .line 3146
    const-string v2, "relation_received"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v35

    .line 3147
    const-string v2, "buddy_orginal_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    .line 3148
    const-string v2, "buddy_birthday"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v37

    .line 3149
    const-string v2, "buddy_relation_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v38

    .line 3150
    const-string v2, "buddy_raw_contact_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    .line 3151
    const-string v2, "buddy_profile_status"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v40

    .line 3152
    const-string v2, "buddy_is_profile_updated"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    .line 3153
    const-string v2, "buddy_is_status_updated"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v42

    .line 3154
    const-string v2, "relation_point"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    .line 3155
    const-string v2, "relation_icon"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v44

    .line 3156
    const-string v2, "relation_increase"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v45

    .line 3157
    const-string v2, "relation_rank"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v46

    .line 3158
    const-string v2, "buddy_show_phone_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v47

    .line 3159
    const-string v2, "buddy_extra_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v48

    .line 3160
    const-string v2, "buddy_is_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 3161
    const-string v2, "buddy_account_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v49

    .line 3162
    const-string v2, "buddy_sainfo"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v50

    .line 3164
    const/4 v2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3165
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3166
    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    .line 3184
    :cond_0
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 3185
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    const/4 v3, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0061

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3186
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3189
    :cond_1
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 3172
    :cond_2
    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    const/4 v9, 0x1

    :goto_2
    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const-string v20, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    const-string v21, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v2 .. v25}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 3189
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;Z)Z
    .locals 0

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->au:Z

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyFragment;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->V:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic aA(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    return v0
.end method

.method static synthetic aB(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->v:Z

    return v0
.end method

.method static synthetic aC(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->X:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic aD(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->H:Landroid/view/ViewStub;

    return-object v0
.end method

.method static synthetic aE(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->an()V

    return-void
.end method

.method static synthetic aF(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bm:Z

    return v0
.end method

.method static synthetic aG(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bn:Z

    return v0
.end method

.method static synthetic aH(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->I:Landroid/view/View;

    return-object v0
.end method

.method static synthetic aI(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->M:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic aJ(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ao:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic aK(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ap:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic aL(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->U:I

    return v0
.end method

.method static synthetic aM(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/EditTextWithClearButton;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->F:Lcom/sec/widget/EditTextWithClearButton;

    return-object v0
.end method

.method static synthetic aN(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->az:Z

    return v0
.end method

.method static synthetic aO(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cj:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic aP(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aw:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic aQ(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/d/w;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->G:Lcom/sec/chaton/d/w;

    return-object v0
.end method

.method static synthetic aR(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cr:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic aS(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    return v0
.end method

.method static synthetic aT(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic aU(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic aV(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bB:Z

    return v0
.end method

.method static synthetic aW(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->O()V

    return-void
.end method

.method static synthetic aX(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->P()V

    return-void
.end method

.method static synthetic aa(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bo:Ljava/lang/String;

    return-object v0
.end method

.method private aa()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 9544
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v0

    .line 9545
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", def="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ck:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", min="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cl:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", max="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9546
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cl:I

    if-eq v1, v3, :cond_1

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cl:I

    if-ge v0, v1, :cond_1

    .line 9550
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->be:Lcom/sec/chaton/buddy/cy;

    if-eqz v0, :cond_0

    .line 9551
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->be:Lcom/sec/chaton/buddy/cy;

    invoke-interface {v0, v4}, Lcom/sec/chaton/buddy/cy;->a(Z)V

    .line 9569
    :cond_0
    :goto_0
    return-void

    .line 9553
    :cond_1
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    if-eq v1, v3, :cond_3

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ck:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    if-le v0, v1, :cond_3

    .line 9557
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->be:Lcom/sec/chaton/buddy/cy;

    if-eqz v0, :cond_2

    .line 9558
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->be:Lcom/sec/chaton/buddy/cy;

    invoke-interface {v0, v4}, Lcom/sec/chaton/buddy/cy;->a(Z)V

    .line 9560
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ad()V

    goto :goto_0

    .line 9562
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->be:Lcom/sec/chaton/buddy/cy;

    if-eqz v0, :cond_0

    .line 9563
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->be:Lcom/sec/chaton/buddy/cy;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/chaton/buddy/cy;->a(Z)V

    goto :goto_0
.end method

.method static synthetic ab(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bV:Ljava/lang/String;

    return-object v0
.end method

.method private ab()V
    .locals 8

    .prologue
    const v7, 0x7f0b0077

    const/16 v6, 0xc8

    const/16 v3, 0xc7

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9572
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v0

    .line 9573
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9574
    if-nez v0, :cond_0

    .line 9575
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Y:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 9576
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Z:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 9578
    :cond_0
    if-le v0, v3, :cond_1

    .line 9579
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v7, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/CharSequence;)V

    .line 9580
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Y:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 9584
    :goto_0
    if-le v0, v6, :cond_2

    .line 9585
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v7, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/CharSequence;)V

    .line 9586
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 9590
    :goto_1
    return-void

    .line 9582
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Y:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 9588
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1
.end method

.method static synthetic ac(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    return v0
.end method

.method private ac()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 9594
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 9602
    :goto_0
    return v2

    .line 9598
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_2

    .line 9599
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v0

    .line 9601
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "canSelectMore check="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", def="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ck:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", min="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cl:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", max="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9602
    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ck:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    if-ge v0, v3, :cond_1

    move v1, v2

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private ad()V
    .locals 5

    .prologue
    .line 9606
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showSelectionReachedMaximumPopup "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9607
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/CharSequence;)V

    .line 9608
    return-void
.end method

.method static synthetic ad(Lcom/sec/chaton/buddy/BuddyFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->V:[Ljava/lang/String;

    return-object v0
.end method

.method private ae()V
    .locals 6

    .prologue
    .line 9654
    new-instance v0, Lcom/sec/chaton/util/bp;

    invoke-direct {v0}, Lcom/sec/chaton/util/bp;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aQ:Lcom/sec/chaton/util/bp;

    .line 9655
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aQ:Lcom/sec/chaton/util/bp;

    const-string v1, "get_all_buddies_in_buddylist"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cq:Lcom/sec/chaton/util/bs;

    const-wide/32 v3, 0x927c0

    const-string v5, "last_sync_time_get_all_buddies_in_buddylist"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 9656
    return-void
.end method

.method static synthetic ae(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    return v0
.end method

.method static synthetic af(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    return-object v0
.end method

.method private af()Z
    .locals 4

    .prologue
    .line 10200
    const/4 v1, 0x0

    .line 10203
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 10204
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 10209
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 10210
    return v0

    .line 10205
    :catch_0
    move-exception v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    .line 10206
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 10205
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private ag()Z
    .locals 4

    .prologue
    .line 10214
    const/4 v0, 0x0

    .line 10217
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 10219
    const/4 v0, 0x1

    .line 10225
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVAvaiable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 10226
    return v0

    .line 10221
    :catch_0
    move-exception v1

    .line 10222
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic ag(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->au:Z

    return v0
.end method

.method static synthetic ah(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ce:I

    return v0
.end method

.method private ah()Z
    .locals 3

    .prologue
    .line 10230
    const/4 v0, 0x0

    .line 10233
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 10235
    const/4 v0, 0x1

    .line 10241
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVInstalled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 10242
    return v0

    .line 10237
    :catch_0
    move-exception v1

    .line 10238
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic ai(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cf:I

    return v0
.end method

.method private ai()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10306
    sget-boolean v0, Lcom/sec/chaton/c/a;->e:Z

    if-nez v0, :cond_7

    .line 10308
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "buddy_delete_copied_contacts"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 10309
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10310
    const-string v0, "migrationBuddyOperation() (delete all contact)"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 10311
    invoke-static {}, Lcom/sec/chaton/e/a/g;->a()V

    .line 10312
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "buddy_delete_copied_contacts"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 10317
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Is migration hanzitopinyin"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 10319
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->a(Landroid/content/ContentResolver;)V

    .line 10320
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Is migration hanzitopinyin"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 10339
    :cond_1
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "buddy_get_all_buddy_mode_instance"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_3

    .line 10340
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 10341
    const-string v0, "migrationBuddyOperation() execute getallbuddy with mode instance"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 10344
    :cond_2
    new-instance v0, Lcom/sec/chaton/d/h;

    invoke-direct {v0, v5}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    const/16 v1, 0xc8

    invoke-virtual {v0, v3, v1}, Lcom/sec/chaton/d/h;->a(ZI)V

    .line 10345
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "buddy_get_all_buddy_mode_instance"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 10348
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "restore_livepartner_user_list"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_5

    .line 10349
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 10350
    const-string v0, "migrationBuddyOperation() restore livepartner user list"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 10353
    :cond_4
    new-instance v0, Lcom/sec/chaton/d/h;

    invoke-direct {v0, v5}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->a(I)V

    .line 10354
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "restore_livepartner_user_list"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 10358
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "buddy_delete_multi_phonenumber_user"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_7

    .line 10359
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_6

    .line 10360
    const-string v0, "migrationBuddyOperation() delete multidevice multiphone number user\'s ChatON icon in Contact"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 10363
    :cond_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->f(Landroid/content/ContentResolver;)V

    .line 10364
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "buddy_delete_multi_phonenumber_user"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 10368
    :cond_7
    return-void

    .line 10321
    :catch_0
    move-exception v0

    .line 10323
    const-string v0, "Remote exception occured, hanzitopinyin migration is failed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10324
    :catch_1
    move-exception v0

    .line 10326
    const-string v0, "Operation application occured, hanzitopinyin migration is failed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic aj(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    return v0
.end method

.method private aj()Z
    .locals 1

    .prologue
    .line 10603
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bF:Z

    return v0
.end method

.method static synthetic ak(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ch:I

    return v0
.end method

.method private ak()Z
    .locals 2

    .prologue
    .line 10616
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 10617
    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 10622
    :goto_0
    return v0

    .line 10620
    :catch_0
    move-exception v0

    .line 10622
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic al(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bT:Landroid/widget/TextView;

    return-object v0
.end method

.method private al()V
    .locals 1

    .prologue
    .line 10731
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 10732
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 10733
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 10734
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->e()V

    .line 10738
    :cond_0
    return-void
.end method

.method static synthetic am(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    return-object v0
.end method

.method private am()V
    .locals 1

    .prologue
    .line 10741
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 10742
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 10743
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10744
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/a;->f()V

    .line 10748
    :cond_0
    return-void
.end method

.method private an()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 10780
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->I:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 10781
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10784
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->A:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 10785
    return-void
.end method

.method static synthetic an(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->y()V

    return-void
.end method

.method static synthetic ao(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aA:Z

    return v0
.end method

.method static synthetic ap(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->C()Z

    move-result v0

    return v0
.end method

.method static synthetic aq(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->x()V

    return-void
.end method

.method static synthetic ar(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/db;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    return-object v0
.end method

.method static synthetic as(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aH:I

    return v0
.end method

.method static synthetic at(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aB:Z

    return v0
.end method

.method static synthetic au(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aC:I

    return v0
.end method

.method static synthetic av(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aD:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic aw(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ax(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aF:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ay(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bg:Z

    return v0
.end method

.method static synthetic az(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/w;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aG:Lcom/sec/chaton/w;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bo:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->w()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyFragment;I)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->e(I)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->h(Landroid/database/Cursor;)V

    return-void
.end method

.method private b(Lcom/sec/chaton/buddy/a/c;I)V
    .locals 4

    .prologue
    .line 6196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6197
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03da

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6198
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6199
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03dc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6201
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/CharSequence;

    .line 6202
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    .line 6203
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 6204
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 6205
    new-instance v2, Lcom/sec/chaton/buddy/bv;

    invoke-direct {v2, p0, p1, p2}, Lcom/sec/chaton/buddy/bv;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/a/c;I)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 6221
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 6222
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 6223
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 9232
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 9233
    const-string v1, "chaton_buddy_number"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 9234
    const-string v1, "chaton_buddy_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 9235
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 9236
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 9237
    return-void
.end method

.method private b(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5645
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 5646
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Favorites"

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5645
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 5649
    :cond_1
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 5650
    if-eqz v3, :cond_0

    .line 5651
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cc:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v4, v3, v0, v1}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 5654
    :cond_2
    return-void
.end method

.method private b(Z)V
    .locals 10

    .prologue
    const/4 v2, 0x3

    const v7, 0x7f070079

    const/16 v9, 0x8

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 10069
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-nez v0, :cond_1

    .line 10070
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 10071
    const-string v0, "checked buddy count is 0"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 10157
    :cond_0
    :goto_0
    return-void

    .line 10073
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v3

    .line 10074
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v1

    .line 10075
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    if-nez v0, :cond_2

    .line 10076
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v5, 0x7f070097

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    .line 10078
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    if-nez v0, :cond_3

    .line 10079
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v5, 0x7f070096

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    .line 10081
    :cond_3
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v6, :cond_6

    .line 10082
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 10087
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v0, v5, v4}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v0, v5, v6}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 10088
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 10089
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    move v0, v2

    .line 10094
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    if-eqz v1, :cond_a

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->k:Ljava/util/HashMap;

    if-eqz v1, :cond_a

    move v1, v4

    move v3, v4

    move v5, v4

    .line 10097
    :goto_3
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v7

    array-length v7, v7

    if-ge v1, v7, :cond_8

    .line 10098
    sget-object v7, Lcom/sec/chaton/buddy/BuddyFragment;->k:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v8

    aget-object v8, v8, v1

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_7

    .line 10099
    add-int/lit8 v5, v5, 0x1

    .line 10097
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 10084
    :cond_6
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_7
    move v3, v6

    .line 10101
    goto :goto_4

    .line 10104
    :cond_8
    add-int/lit8 v1, v2, -0x1

    if-gt v5, v1, :cond_9

    if-ne v3, v6, :cond_c

    .line 10105
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 10106
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v1, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 10107
    if-eq v2, v0, :cond_a

    .line 10108
    if-eq v3, v6, :cond_a

    .line 10109
    if-eqz p1, :cond_b

    .line 10110
    if-lt v5, v2, :cond_a

    .line 10111
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b03ab

    new-array v7, v6, [Ljava/lang/Object;

    add-int/lit8 v8, v2, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v3, v5, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 10125
    :cond_a
    :goto_5
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->k:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    move v1, v4

    move v3, v4

    move v5, v4

    .line 10128
    :goto_6
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v7

    array-length v7, v7

    if-ge v1, v7, :cond_e

    .line 10129
    sget-object v7, Lcom/sec/chaton/buddy/BuddyFragment;->k:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v8

    aget-object v8, v8, v1

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_d

    .line 10130
    add-int/lit8 v5, v5, 0x1

    .line 10128
    :goto_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 10114
    :cond_b
    if-ne v5, v2, :cond_a

    .line 10115
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b03ab

    new-array v7, v6, [Ljava/lang/Object;

    add-int/lit8 v8, v2, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v3, v5, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_5

    .line 10121
    :cond_c
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 10122
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_5

    :cond_d
    move v3, v6

    .line 10132
    goto :goto_7

    .line 10135
    :cond_e
    add-int/lit8 v1, v0, -0x1

    if-gt v5, v1, :cond_f

    if-ne v3, v6, :cond_11

    .line 10136
    :cond_f
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 10137
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v1, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 10138
    if-eq v2, v0, :cond_0

    .line 10139
    if-eq v3, v6, :cond_0

    .line 10140
    if-eqz p1, :cond_10

    .line 10141
    if-lt v5, v0, :cond_0

    .line 10142
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03ac

    new-array v5, v6, [Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v4

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 10145
    :cond_10
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2, v6}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v1

    if-ne v5, v1, :cond_0

    .line 10146
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03ac

    new-array v5, v6, [Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v4

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 10152
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 10153
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    :cond_12
    move v0, v1

    move v2, v3

    goto/16 :goto_2
.end method

.method private b(Landroid/database/Cursor;)Z
    .locals 31

    .prologue
    .line 3194
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v3, 0x3e8

    if-ne v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    const-string v3, "voip"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3195
    const/4 v2, 0x0

    .line 3301
    :goto_0
    return v2

    .line 3198
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addGroupDataSetFromCursor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3199
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 3201
    const/4 v2, 0x0

    .line 3202
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v4, 0x13

    if-ne v3, v4, :cond_10

    .line 3203
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->f:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v23, v2

    .line 3207
    :goto_1
    const-string v2, "group_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 3208
    const-string v2, "buddy_no"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 3209
    const-string v2, "buddy_email"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 3210
    const-string v2, "buddy_samsung_email"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 3211
    const-string v2, "buddy_is_new"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 3212
    const-string v2, "buddy_extra_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 3213
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 3214
    :cond_1
    :goto_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 3215
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3217
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    .line 3221
    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-direct/range {v2 .. v22}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZI)V

    .line 3223
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 3224
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 3225
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/chaton/buddy/a/c;->a(Z)V

    .line 3226
    const/4 v3, 0x0

    .line 3227
    const/4 v4, 0x0

    .line 3228
    const/4 v5, 0x0

    .line 3229
    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 3230
    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 3232
    :cond_2
    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 3233
    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 3237
    :cond_3
    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 3238
    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 3241
    :cond_4
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v7, 0xf

    if-eq v6, v7, :cond_5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v7, 0xe

    if-ne v6, v7, :cond_6

    .line 3242
    :cond_5
    const/4 v6, 0x0

    .line 3244
    if-eqz v5, :cond_1

    array-length v7, v5

    array-length v9, v4

    if-ne v7, v9, :cond_1

    .line 3247
    array-length v9, v5

    const/4 v7, 0x0

    :goto_3
    if-ge v7, v9, :cond_f

    aget-object v10, v5, v7

    .line 3248
    const-string v11, "voip=1"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 3249
    const/4 v5, 0x1

    .line 3253
    :goto_4
    if-nez v5, :cond_1

    .line 3260
    :cond_6
    if-eqz v3, :cond_9

    if-eqz v4, :cond_9

    .line 3261
    array-length v5, v3

    if-lez v5, :cond_8

    array-length v5, v3

    array-length v6, v4

    if-ne v5, v6, :cond_8

    .line 3262
    const/4 v5, 0x0

    :goto_5
    array-length v6, v3

    if-ge v5, v6, :cond_8

    .line 3263
    aget-object v6, v3, v5

    aget-object v7, v4, v5

    invoke-virtual {v8, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3262
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 3247
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 3266
    :cond_8
    invoke-virtual {v2, v8}, Lcom/sec/chaton/buddy/a/c;->a(Ljava/util/HashMap;)V

    .line 3269
    :cond_9
    if-eqz v23, :cond_a

    .line 3270
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 3271
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->aL:Ljava/util/ArrayList;

    .line 3275
    if-eqz v3, :cond_1

    array-length v2, v3

    if-lez v2, :cond_1

    .line 3276
    array-length v4, v3

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 3277
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/chaton/buddy/BuddyFragment;->aL:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3276
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 3285
    :cond_a
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 3286
    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 3289
    :cond_b
    :try_start_0
    new-instance v3, Lcom/sec/chaton/e/b/k;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/BuddyFragment;->r:Lcom/sec/chaton/e/b/d;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v3, v4, v2}, Lcom/sec/chaton/e/b/k;-><init>(Lcom/sec/chaton/e/b/d;I)V

    .line 3290
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v2

    const/4 v4, -0x1

    invoke-static {v2, v4, v3}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 3291
    :catch_0
    move-exception v2

    .line 3292
    invoke-virtual {v2}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 3296
    :cond_c
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_d

    .line 3297
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    const/4 v3, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b013c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3298
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3301
    :cond_d
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_e

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_f
    move v5, v6

    goto/16 :goto_4

    :cond_10
    move-object/from16 v23, v2

    goto/16 :goto_1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyFragment;Z)Z
    .locals 0

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->az:Z

    return p1
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 10160
    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->k:Ljava/util/HashMap;

    if-nez v1, :cond_1

    .line 10166
    :cond_0
    :goto_0
    return v0

    .line 10163
    :cond_1
    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->k:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 10166
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyFragment;I)I
    .locals 0

    .prologue
    .line 174
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aU:Landroid/view/View;

    return-object v0
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 2148
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00b3

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0024

    new-instance v2, Lcom/sec/chaton/buddy/bg;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/buddy/bg;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;I)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 2161
    return-void
.end method

.method private c(Landroid/database/Cursor;)V
    .locals 54

    .prologue
    .line 3463
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addBuddiesByRelationDataSetFromCursor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3464
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 3466
    const/4 v9, -0x1

    .line 3468
    const/16 v28, 0x2

    .line 3469
    const/4 v2, 0x0

    .line 3471
    const-string v3, "buddy_no"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 3472
    const-string v3, "buddy_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 3473
    const-string v3, "buddy_status_message"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 3474
    const-string v3, "buddy_samsung_email"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 3475
    const-string v3, "buddy_orginal_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    .line 3476
    const-string v3, "buddy_birthday"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v35

    .line 3477
    const-string v3, "buddy_relation_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    .line 3478
    const-string v3, "buddy_raw_contact_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v37

    .line 3479
    const-string v3, "buddy_push_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v38

    .line 3480
    const-string v3, "buddy_is_new"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    .line 3481
    const-string v3, "relation_send"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v40

    .line 3482
    const-string v3, "relation_received"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    .line 3483
    const-string v3, "relation_point"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v42

    .line 3484
    const-string v3, "relation_icon"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    .line 3485
    const-string v3, "relation_increase"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v44

    .line 3486
    const-string v3, "relation_rank"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v45

    .line 3487
    const-string v3, "buddy_profile_status"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v46

    .line 3488
    const-string v3, "buddy_is_profile_updated"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v47

    .line 3489
    const-string v3, "buddy_is_status_updated"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v48

    .line 3490
    const-string v3, "buddy_show_phone_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v49

    .line 3491
    const-string v3, "buddy_extra_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v50

    .line 3492
    const-string v3, "buddy_is_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v51

    .line 3493
    const-string v3, "buddy_account_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v52

    .line 3494
    const-string v3, "buddy_sainfo"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v53

    .line 3497
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 3499
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    .line 3500
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToPrevious()Z

    move/from16 v26, v2

    .line 3501
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3502
    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    .line 3504
    const/4 v2, -0x1

    move/from16 v0, v51

    if-eq v0, v2, :cond_0

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3505
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    goto :goto_0

    .line 3524
    :cond_0
    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const/4 v9, 0x1

    :goto_1
    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const-string v20, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    const-string v21, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v2 .. v25}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;)V

    .line 3537
    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3539
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3542
    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_8

    .line 3543
    add-int/lit8 v2, v26, 0x1

    :goto_2
    move/from16 v26, v2

    move/from16 v9, v27

    .line 3545
    goto/16 :goto_0

    .line 3524
    :cond_1
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 3558
    :cond_2
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 3559
    const/16 v2, 0x1e

    .line 3560
    move/from16 v0, v26

    if-le v0, v2, :cond_3

    move/from16 v26, v2

    .line 3564
    :cond_3
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v26

    if-ge v2, v0, :cond_6

    .line 3566
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b020f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move/from16 v3, v28

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3567
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3586
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    if-eqz v2, :cond_5

    .line 3588
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/sec/chaton/buddy/db;->b(I)V

    .line 3591
    :cond_5
    return-void

    .line 3569
    :cond_6
    if-lez v26, :cond_7

    .line 3570
    new-instance v11, Ljava/util/ArrayList;

    const/4 v2, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-direct {v11, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3571
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    const/4 v10, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b020f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move/from16 v3, v28

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3572
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v3, v10

    .line 3575
    :goto_4
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v26

    if-le v2, v0, :cond_4

    .line 3576
    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, v29

    move/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-direct {v10, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3578
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    add-int/lit8 v4, v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0147

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3579
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_7
    move/from16 v3, v28

    goto :goto_4

    :cond_8
    move/from16 v2, v26

    goto/16 :goto_2
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Landroid/database/Cursor;)V

    return-void
.end method

.method private c(Z)V
    .locals 0

    .prologue
    .line 10599
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bF:Z

    .line 10600
    return-void
.end method

.method private c([Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 9224
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 9225
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 9226
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 9227
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 9228
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyFragment;Z)Z
    .locals 0

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aA:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyFragment;I)I
    .locals 0

    .prologue
    .line 174
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ce:I

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aT:Landroid/widget/ImageView;

    return-object v0
.end method

.method private d(I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 2164
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->R()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2165
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->c(I)V

    .line 2224
    :cond_0
    :goto_0
    return-void

    .line 2168
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v7, :cond_0

    .line 2169
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ag()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2170
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->af()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    move v1, v2

    .line 2172
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 2173
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/d;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2174
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v0

    invoke-static {v4, v6}, Lcom/sec/chaton/e/a/d;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2175
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "voip=1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2176
    add-int/lit8 v1, v1, 0x1

    .line 2172
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2180
    :cond_3
    if-eqz v1, :cond_0

    .line 2183
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v1, v0, :cond_0

    .line 2191
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-gt v0, v7, :cond_4

    .line 2192
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Push Name"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2205
    :goto_2
    if-eqz v0, :cond_0

    .line 2206
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b008a

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2194
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a([Ljava/lang/String;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    .line 2195
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    if-eqz v0, :cond_6

    .line 2196
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-ne v0, v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/widget/FastScrollableExpandableListView;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2197
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Push Name"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v6}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_2

    .line 2199
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Push Name"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_2

    .line 2202
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Push Name"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_2

    .line 2214
    :cond_7
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2215
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2218
    :cond_8
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ah()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2219
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2220
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private d(Landroid/database/Cursor;)V
    .locals 56

    .prologue
    .line 3594
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addChatonvDataSetFromCursor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3595
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 3596
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 3598
    const/16 v26, 0x2

    .line 3600
    const-string v2, "buddy_no"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 3601
    const-string v2, "buddy_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 3602
    const-string v2, "buddy_status_message"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 3603
    const-string v2, "buddy_samsung_email"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 3604
    const-string v2, "buddy_orginal_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 3605
    const-string v2, "buddy_birthday"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    .line 3606
    const-string v2, "buddy_relation_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v35

    .line 3607
    const-string v2, "relation_icon"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    .line 3608
    const-string v2, "relation_increase"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v37

    .line 3609
    const-string v2, "buddy_raw_contact_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v38

    .line 3610
    const-string v2, "buddy_push_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    .line 3611
    const-string v2, "buddy_is_new"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v40

    .line 3612
    const-string v2, "relation_send"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    .line 3613
    const-string v2, "relation_received"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v42

    .line 3614
    const-string v2, "relation_point"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    .line 3615
    const-string v2, "relation_rank"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v44

    .line 3616
    const-string v2, "buddy_profile_status"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v45

    .line 3617
    const-string v2, "buddy_is_profile_updated"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v46

    .line 3618
    const-string v2, "buddy_is_status_updated"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v47

    .line 3619
    const-string v2, "buddy_show_phone_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v48

    .line 3620
    const-string v2, "buddy_extra_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v49

    .line 3621
    const-string v2, "buddy_account_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v50

    .line 3622
    const-string v2, "buddy_sainfo"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v51

    .line 3623
    const-string v2, "group_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v52

    .line 3625
    const-string v2, "group_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v53

    .line 3626
    const-string v2, "buddy_is_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v54

    .line 3627
    const-string v2, "buddy_hanzitopinyin"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v55

    .line 3629
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 3632
    const/4 v2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3633
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3635
    const/4 v2, 0x0

    .line 3637
    const/4 v3, -0x1

    move/from16 v0, v55

    if-eq v0, v3, :cond_5

    .line 3638
    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3642
    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3643
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    move-result-object v2

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/at;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3645
    :cond_1
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 3650
    const/4 v2, -0x1

    move/from16 v0, v53

    if-eq v0, v2, :cond_2

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 3651
    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3652
    if-nez v2, :cond_0

    .line 3657
    :cond_2
    if-lez v53, :cond_3

    .line 3658
    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3659
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_3

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 3663
    :cond_3
    const/4 v2, -0x1

    move/from16 v0, v54

    if-eq v0, v2, :cond_4

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3667
    :cond_4
    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    const/4 v9, 0x1

    :goto_2
    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const-string v20, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    const-string v21, "Y"

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v2 .. v25}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;)V

    .line 3695
    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 3696
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->D()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->D()Ljava/lang/String;

    move-result-object v3

    const-string v4, "voip=1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 3697
    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3701
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 3640
    :cond_5
    const-string v3, "query without hanzitopinyin"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3667
    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 3699
    :cond_7
    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 3705
    :cond_8
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_b

    .line 3706
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    const/4 v10, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b03aa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v3, v26

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3708
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v3, v10

    .line 3710
    :goto_4
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_9

    .line 3711
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/chaton/buddy/a/a;

    add-int/lit8 v4, v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0147

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3713
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3715
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    if-eqz v2, :cond_a

    .line 3716
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/sec/chaton/buddy/db;->b(I)V

    .line 3718
    :cond_a
    return-void

    :cond_b
    move/from16 v3, v26

    goto :goto_4
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->e(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyFragment;Z)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->d(Z)V

    return-void
.end method

.method private d(Lcom/sec/chaton/buddy/a/c;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8916
    new-instance v2, Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v5, v1, [Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const/16 v5, 0x12

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/sec/chaton/buddy/a/b;-><init>(ILjava/lang/String;II)V

    .line 8918
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v4, Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8919
    const-string v0, "ACTIVITY_PURPOSE"

    const/16 v4, 0x13

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8920
    const-string v0, "groupInfo"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 8921
    const-string v2, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 8922
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    .line 8923
    return-void

    .line 8916
    :cond_0
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v5, v1, [Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    array-length v0, v0

    goto :goto_0
.end method

.method private d(Z)V
    .locals 0

    .prologue
    .line 10607
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bG:Z

    .line 10608
    return-void
.end method

.method private d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2705
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2707
    const/4 v0, 0x1

    .line 2711
    :goto_0
    return v0

    .line 2710
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v0

    .line 2711
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Lcom/sec/chaton/buddy/a/c;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyFragment;I)I
    .locals 0

    .prologue
    .line 174
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cf:I

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aV:Landroid/view/View;

    return-object v0
.end method

.method private e(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5657
    .line 5658
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_group_profile.png_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5659
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-direct {v0, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 5660
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 5661
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5662
    invoke-static {v0, p1}, Lcom/sec/chaton/e/a/f;->d(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    .line 5663
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_group_profile.png_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5664
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-direct {v0, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 5666
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v2

    .line 5688
    :cond_1
    :goto_0
    return-object v0

    .line 5671
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 5672
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 5681
    if-eqz v3, :cond_1

    .line 5682
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 5684
    :catch_0
    move-exception v1

    .line 5685
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 5673
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 5674
    :goto_1
    :try_start_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b0149

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 5675
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 5681
    if-eqz v1, :cond_3

    .line 5682
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_3
    :goto_2
    move-object v0, v2

    .line 5676
    goto :goto_0

    .line 5677
    :catch_2
    move-exception v0

    move-object v3, v2

    .line 5678
    :goto_3
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 5681
    if-eqz v3, :cond_4

    .line 5682
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_4
    :goto_4
    move-object v0, v2

    .line 5688
    goto :goto_0

    .line 5680
    :catchall_0
    move-exception v0

    move-object v3, v2

    .line 5681
    :goto_5
    if-eqz v3, :cond_5

    .line 5682
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 5680
    :cond_5
    :goto_6
    throw v0

    .line 5684
    :catch_3
    move-exception v1

    .line 5685
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_6

    .line 5684
    :catch_4
    move-exception v0

    .line 5685
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 5684
    :catch_5
    move-exception v0

    .line 5685
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_4

    .line 5680
    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v3, v1

    goto :goto_5

    .line 5677
    :catch_6
    move-exception v0

    goto :goto_3

    .line 5673
    :catch_7
    move-exception v0

    move-object v1, v3

    goto :goto_1
.end method

.method private e(I)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 2227
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->R()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2228
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->c(I)V

    .line 2287
    :cond_0
    :goto_0
    return-void

    .line 2231
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 2232
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ag()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2233
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->af()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v7

    move v1, v7

    .line 2235
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 2236
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/d;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2237
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v0

    invoke-static {v4, v6}, Lcom/sec/chaton/e/a/d;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2238
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "voip=1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2239
    add-int/lit8 v1, v1, 0x1

    .line 2235
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2243
    :cond_3
    if-eqz v1, :cond_0

    .line 2246
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v1, v0, :cond_0

    .line 2254
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-gt v0, v2, :cond_4

    .line 2255
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v7

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Push Name"

    const-string v8, ""

    invoke-virtual {v4, v6, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2268
    :goto_2
    if-eqz v0, :cond_0

    .line 2269
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b008a

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2257
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a([Ljava/lang/String;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    .line 2258
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    if-eqz v0, :cond_6

    .line 2259
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-ne v0, v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v3, v7, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/widget/FastScrollableExpandableListView;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2260
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Push Name"

    const-string v8, ""

    invoke-virtual {v4, v6, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v6}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_2

    .line 2262
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Push Name"

    const-string v8, ""

    invoke-virtual {v4, v6, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_2

    .line 2265
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Push Name"

    const-string v8, ""

    invoke-virtual {v4, v6, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_2

    .line 2277
    :cond_7
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2278
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2281
    :cond_8
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ah()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2282
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2283
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private e(Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 3854
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3855
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3858
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 3859
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 3860
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0078

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3862
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3864
    const-string v1, "receivers"

    new-array v2, v4, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 3866
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 3868
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 3897
    :cond_0
    :goto_0
    return-void

    .line 3874
    :cond_1
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_2

    .line 3875
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->C()Z

    .line 3881
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/database/Cursor;)Z

    .line 3883
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Landroid/database/Cursor;)Z

    .line 3885
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_3

    .line 3886
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->D()Z

    .line 3888
    :cond_3
    invoke-direct {p0, p1, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/database/Cursor;Z)V

    .line 3889
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Profile Birth Chk"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_4

    .line 3890
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->E()Z

    .line 3892
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->G()V

    .line 3894
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 3895
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->h()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->i(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyFragment;I)I
    .locals 0

    .prologue
    .line 174
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aH:I

    return p1
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    return-object v0
.end method

.method private f(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3915
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3916
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3919
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/database/Cursor;)Z

    .line 3921
    invoke-direct {p0, p1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/database/Cursor;Z)V

    .line 3922
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Profile Birth Chk"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3923
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->E()Z

    .line 3925
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->G()V

    .line 3927
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->j(Landroid/database/Cursor;)V

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 9071
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_1

    .line 9072
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 9073
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_2

    .line 9074
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 9086
    :cond_1
    :goto_0
    return-void

    .line 9077
    :cond_2
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cj:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 9078
    const-string v1, "group"

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x145

    move-object v4, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)I

    .line 9079
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aw:Landroid/app/ProgressDialog;

    if-nez v0, :cond_3

    .line 9080
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v3, v1, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aw:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 9082
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aw:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyFragment;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    return v0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyFragment;I)I
    .locals 0

    .prologue
    .line 174
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    return p1
.end method

.method private g(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 3930
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3931
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3932
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/database/Cursor;Z)V

    .line 3933
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->l(Landroid/database/Cursor;)V

    return-void
.end method

.method private h(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 3937
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3938
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3939
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/database/Cursor;Z)V

    .line 3940
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ab()V

    return-void
.end method

.method private i(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 3945
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3946
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4115
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_0

    .line 4116
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->C()Z

    .line 4118
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aB:Z

    if-nez v0, :cond_1

    .line 4119
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/database/Cursor;)Z

    .line 4121
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/database/Cursor;)Z

    .line 4122
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Landroid/database/Cursor;)Z

    .line 4123
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_2

    .line 4124
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->D()Z

    .line 4126
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->c(Landroid/database/Cursor;)V

    .line 4127
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Profile Birth Chk"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_3

    .line 4128
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->E()Z

    .line 4130
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->G()V

    .line 4132
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 4133
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->h()V

    .line 4135
    :cond_4
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->aa()V

    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aZ:Ljava/lang/String;

    return-object v0
.end method

.method private j(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 4138
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4139
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4141
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_0

    .line 4142
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->C()Z

    .line 4144
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aB:Z

    if-nez v0, :cond_1

    .line 4145
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/database/Cursor;)Z

    .line 4147
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Landroid/database/Cursor;)Z

    .line 4148
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_2

    .line 4149
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->D()Z

    .line 4151
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->d(Landroid/database/Cursor;)V

    .line 4152
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Profile Birth Chk"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_3

    .line 4153
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->E()Z

    .line 4155
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->G()V

    .line 4156
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 4157
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->h()V

    .line 4159
    :cond_4
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/widget/ClearableEditText;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    return-object v0
.end method

.method private k(Landroid/database/Cursor;)V
    .locals 31

    .prologue
    .line 4205
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bd:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 4207
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bd:Ljava/util/ArrayList;

    .line 4210
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bf:Ljava/util/HashMap;

    if-nez v2, :cond_1

    .line 4211
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bf:Ljava/util/HashMap;

    .line 4213
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bd:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 4214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bf:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 4219
    :cond_2
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 4221
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v3, 0x3e8

    if-ne v2, v3, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    const-string v3, "voip"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4222
    const-string v2, "buddy_extra_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "buddy_sainfo"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "buddy_extra_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "voip=1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4229
    :cond_3
    const-string v2, "buddy_is_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    const-string v2, "buddy_is_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    const-string v2, "buddy_is_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 4233
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_5

    const-string v2, "buddy_no"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0999"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 4237
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    move-result-object v2

    const-string v3, "buddy_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/at;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v27

    .line 4238
    const-string v26, ""

    .line 4239
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_6

    .line 4240
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/util/av;

    iget-object v2, v2, Lcom/sec/chaton/util/av;->c:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 4239
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 4244
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bd:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    const-string v3, "buddy_no"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "buddy_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "buddy_status_message"

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "buddy_samsung_email"

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "buddy_orginal_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "buddy_birthday"

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "buddy_relation_hide"

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "Y"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    const/4 v9, 0x0

    :goto_2
    const-string v10, "buddy_raw_contact_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v11, "buddy_push_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "buddy_is_new"

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "Y"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    const/4 v12, 0x1

    :goto_3
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const-string v19, "buddy_profile_status"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const-string v20, "buddy_is_profile_updated"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const-string v21, "Y"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    const/16 v20, 0x1

    :goto_4
    const-string v21, "buddy_is_status_updated"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    const-string v22, "Y"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    const/16 v21, 0x1

    :goto_5
    const-string v22, "buddy_show_phone_number"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const-string v23, "buddy_extra_info"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, "group_relation_group"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    const-string v25, "buddy_msisdns"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    const-string v28, "buddy_account_info"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    const-string v29, "buddy_sainfo"

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v2 .. v29}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ILjava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4251
    const-string v2, "buddy_phonenumber_external_use"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 4253
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyFragment;->bf:Ljava/util/HashMap;

    const-string v3, "buddy_no"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "buddy_phonenumber_external_use"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4244
    :cond_7
    const/4 v9, 0x1

    goto/16 :goto_2

    :cond_8
    const/4 v12, 0x0

    goto/16 :goto_3

    :cond_9
    const/16 v20, 0x0

    goto/16 :goto_4

    :cond_a
    const/16 v21, 0x0

    goto/16 :goto_5

    .line 4260
    :cond_b
    return-void
.end method

.method private l(Landroid/database/Cursor;)V
    .locals 14

    .prologue
    .line 4290
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ao:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4291
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ap:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4293
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 4295
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 4297
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 4482
    :cond_0
    :goto_0
    return-void

    .line 4301
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4302
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 4305
    :cond_2
    const/4 v0, 0x0

    .line 4309
    const/4 v1, 0x2

    .line 4310
    const/4 v3, 0x0

    .line 4314
    iget-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->h:Z

    if-eqz v2, :cond_3

    .line 4315
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->k(Landroid/database/Cursor;)V

    .line 4316
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->h:Z

    .line 4318
    :cond_3
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bd:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bd:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_5

    .line 4319
    :cond_4
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->k(Landroid/database/Cursor;)V

    .line 4325
    :cond_5
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bd:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v0

    :cond_6
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/sec/chaton/buddy/a/c;

    .line 4327
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v9

    .line 4328
    const/4 v4, 0x0

    .line 4329
    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    .line 4331
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->A()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v11, " "

    const-string v12, ""

    invoke-virtual {v9, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 4332
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->B()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, v10}, Lcom/sec/chaton/util/at;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 4336
    :cond_7
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->b()I

    move-result v0

    const/4 v11, -0x1

    if-ne v0, v11, :cond_8

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v10}, Lcom/sec/chaton/util/as;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bf:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bf:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 4337
    :cond_8
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    if-eqz v0, :cond_b

    .line 4339
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v10

    .line 4340
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 4341
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_9
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 4342
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v13

    if-nez v13, :cond_9

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4343
    const/4 v0, 0x1

    .line 4348
    :goto_3
    if-eqz v0, :cond_a

    .line 4357
    :goto_4
    if-eqz v0, :cond_6

    .line 4361
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_6

    .line 4365
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/util/bf;->a(C)C

    move-result v0

    .line 4368
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->y()I

    move-result v4

    const/4 v9, 0x1

    if-eq v4, v9, :cond_6

    .line 4373
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4375
    add-int/lit8 v3, v3, 0x1

    .line 4377
    :goto_5
    invoke-static {}, Lcom/sec/chaton/util/at;->a()Lcom/sec/chaton/util/at;

    invoke-static {}, Lcom/sec/chaton/util/at;->e()V

    move v5, v0

    .line 4378
    goto/16 :goto_1

    :cond_a
    move v4, v0

    .line 4349
    goto :goto_2

    .line 4354
    :cond_b
    const/4 v0, 0x1

    goto :goto_4

    .line 4475
    :cond_c
    if-eqz v5, :cond_0

    .line 4476
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ap:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4477
    iget-object v8, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ao:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/buddy/a/a;

    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/buddy/a/a;-><init>(ILjava/lang/String;IIZZI)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_d
    move v0, v5

    goto :goto_5

    :cond_e
    move v0, v4

    goto :goto_3

    :cond_f
    move v0, v4

    goto :goto_4
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac()Z

    move-result v0

    return v0
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->N()V

    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aN:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->S:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aO:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/util/bt;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->D:Lcom/sec/chaton/util/bt;

    return-object v0
.end method

.method static synthetic r()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 174
    sget-object v0, Lcom/sec/chaton/buddy/BuddyFragment;->bq:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/buddy/BuddyFragment;)Z
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->F()Z

    move-result v0

    return v0
.end method

.method private s()I
    .locals 1

    .prologue
    .line 1359
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1360
    const/4 v0, 0x1

    .line 1362
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic s(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aX:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aY:Landroid/widget/TextView;

    return-object v0
.end method

.method private t()Z
    .locals 2

    .prologue
    .line 1521
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->au:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cp:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 10

    .prologue
    const v9, 0x7f070079

    const/16 v8, 0xb

    const/4 v1, 0x1

    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 1528
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initView() "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1530
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1531
    sget-object v0, Lcom/sec/chaton/buddy/BuddyFragment;->k:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 1532
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->e(Landroid/content/ContentResolver;)Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/BuddyFragment;->k:Ljava/util/HashMap;

    .line 1534
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->h()V

    .line 1536
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->br:I

    .line 1537
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bs:I

    .line 1538
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1539
    :cond_3
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->br:I

    .line 1540
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bs:I

    .line 1543
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070084

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->z:Landroid/widget/LinearLayout;

    .line 1544
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070087

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->A:Landroid/widget/ScrollView;

    .line 1545
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070088

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->B:Landroid/widget/LinearLayout;

    .line 1547
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070086

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->O:Landroid/view/View;

    .line 1549
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070093

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->H:Landroid/view/ViewStub;

    .line 1555
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070081

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->X:Landroid/widget/LinearLayout;

    .line 1557
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f07008e

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->y:Landroid/widget/TextView;

    .line 1558
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "Push Name"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->x:Ljava/lang/String;

    .line 1559
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->y:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1560
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070085

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/FastScrollableExpandableListView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    .line 1561
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, p0}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 1562
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, p0}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnGroupCollapseListener(Landroid/widget/ExpandableListView$OnGroupCollapseListener;)V

    .line 1563
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, p0}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 1564
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, p0}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 1565
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    new-instance v3, Lcom/sec/chaton/buddy/ct;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/ct;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v3}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1589
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f030083

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 1590
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f030083

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v4, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 1591
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v5, 0x7f030022

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v5, v6, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bS:Landroid/view/View;

    .line 1592
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bS:Landroid/view/View;

    const v5, 0x7f0700d6

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bT:Landroid/widget/TextView;

    .line 1593
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v1, :cond_5

    .line 1594
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5, v2}, Lcom/sec/widget/FastScrollableExpandableListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 1595
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bS:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5, v2}, Lcom/sec/widget/FastScrollableExpandableListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 1596
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    const/4 v3, 0x0

    invoke-virtual {v0, v4, v3, v2}, Lcom/sec/widget/FastScrollableExpandableListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 1599
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_6

    .line 1600
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    .line 1607
    :cond_6
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070082

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bw:Landroid/view/View;

    .line 1608
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bw:Landroid/view/View;

    const/16 v3, 0xfa

    const/16 v4, 0xfa

    const/16 v5, 0xfa

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1609
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bw:Landroid/view/View;

    const v3, 0x7f07014c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    .line 1610
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    new-array v3, v1, [Landroid/text/InputFilter;

    new-instance v4, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const/16 v6, 0x1e

    invoke-direct {v4, v5, v6}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v4, v3, v2

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1611
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    const v3, 0x7f0b0375

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ClearableEditText;->setHint(I)V

    .line 1612
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cv:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ClearableEditText;->setClearButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 1613
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bw:Landroid/view/View;

    const v3, 0x7f0702e6

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->by:Landroid/view/View;

    .line 1614
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bw:Landroid/view/View;

    const v3, 0x7f0702d7

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bz:Landroid/widget/ImageButton;

    .line 1615
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bz:Landroid/widget/ImageButton;

    const v3, 0x7f0202b4

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1616
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bz:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0065

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1617
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bw:Landroid/view/View;

    const v3, 0x7f07014d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aI:Landroid/widget/TextView;

    .line 1626
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070083

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    .line 1632
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070091

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->M:Landroid/widget/LinearLayout;

    .line 1633
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070092

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->N:Landroid/widget/Button;

    .line 1636
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v3, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v3, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "BUDDY_SORT_STYLE"

    invoke-interface {v0, v3, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    .line 1637
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    const/16 v3, 0xc

    if-ne v0, v3, :cond_7

    .line 1638
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v3, "BUDDY_ACTIVITY"

    invoke-virtual {v0, v3, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "BUDDY_SORT_STYLE"

    invoke-interface {v0, v3, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1639
    iput v8, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    .line 1643
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070089

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aR:Landroid/view/View;

    .line 1644
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f07008a

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aS:Landroid/widget/TextView;

    .line 1645
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f07008b

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aT:Landroid/widget/ImageView;

    .line 1647
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f07008c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aU:Landroid/view/View;

    .line 1648
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070090

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aV:Landroid/view/View;

    .line 1650
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aU:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1651
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aT:Landroid/widget/ImageView;

    const v3, 0x7f0202d1

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1655
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f07008d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aW:Landroid/widget/ImageView;

    .line 1656
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f07008e

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aX:Landroid/widget/TextView;

    .line 1657
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f07008f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aY:Landroid/widget/TextView;

    .line 1658
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "status_message"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1659
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "Push Name"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1660
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aS:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0094

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1663
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aX:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1664
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aY:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1666
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->G:Lcom/sec/chaton/d/w;

    if-nez v0, :cond_8

    .line 1667
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cr:Landroid/os/Handler;

    invoke-direct {v0, v3}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->G:Lcom/sec/chaton/d/w;

    .line 1670
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aU:Landroid/view/View;

    new-instance v3, Lcom/sec/chaton/buddy/cu;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/cu;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1691
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    const/16 v3, 0xe

    if-ne v0, v3, :cond_9

    .line 1692
    iput v8, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    .line 1695
    :cond_9
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    .line 1697
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/os/Bundle;)V

    .line 1700
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070080

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/ChoicePanel;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    .line 1701
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v1, :cond_a

    .line 1702
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/sec/widget/ChoicePanel;->setIsPluginAvailable_chaton(Z)V

    .line 1706
    :cond_a
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1707
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    invoke-virtual {v0, v7}, Lcom/sec/widget/ChoicePanel;->setVisibility(I)V

    .line 1711
    :cond_b
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bY:Landroid/view/View$OnClickListener;

    iget v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    const-string v6, "voip"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    move v0, v1

    :goto_1
    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/widget/ChoicePanel;->setButtonClickListener(Landroid/view/View$OnClickListener;IZ)V

    .line 1715
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-eqz v0, :cond_c

    .line 1716
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/sec/widget/FastScrollableExpandableListView;->setChoiceMode(I)V

    .line 1717
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/FastScrollableExpandableListView;->setItemsCanFocus(Z)V

    .line 1720
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v7, :cond_c

    .line 1721
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1740
    :cond_c
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v3, 0x9

    if-ne v0, v3, :cond_d

    .line 1742
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v2}, Lcom/sec/widget/FastScrollableExpandableListView;->setChildCheckInverse(Z)V

    .line 1746
    :cond_d
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v3, 0x12

    if-ne v0, v3, :cond_e

    .line 1747
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1750
    :cond_e
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_f

    .line 1751
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1753
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v3, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v3, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1754
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 1755
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 1761
    :cond_f
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v3, 0x14

    if-ne v0, v3, :cond_10

    .line 1762
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v7}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 1765
    :cond_10
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v1, :cond_1b

    .line 1799
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->by:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1800
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bz:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/chaton/buddy/cv;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/cv;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1816
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1817
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070094

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Y:Landroid/widget/ImageButton;

    .line 1818
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070095

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Z:Landroid/widget/ImageButton;

    .line 1819
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070096

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    .line 1820
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070097

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    .line 1821
    new-instance v0, Lcom/sec/chaton/buddy/bb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/bb;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 1849
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Y:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1850
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Z:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1851
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1852
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1853
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1854
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v1, :cond_1a

    .line 1855
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1856
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1857
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1866
    :cond_11
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1963
    :cond_12
    :goto_3
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v1, :cond_1f

    .line 1965
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 1978
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->N:Landroid/widget/Button;

    new-instance v3, Lcom/sec/chaton/buddy/bd;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/bd;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2002
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070098

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    .line 2003
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    const v3, 0x7f0702da

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ImageTextViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    .line 2004
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 2005
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 2006
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 2008
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070099

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    .line 2009
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    const v3, 0x7f0702db

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ImageTextViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

    .line 2010
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    const v3, 0x7f0702dc

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ImageTextViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

    .line 2011
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v3, 0x7f0b0092

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setText(I)V

    .line 2012
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v3, 0x7f0203fa

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setImageResource(I)V

    .line 2013
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v3, 0x7f0b0092

    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2014
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 2015
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 2016
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 2018
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

    if-eqz v0, :cond_13

    .line 2019
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v3, 0x7f0b0277

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setText(I)V

    .line 2020
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v3, 0x7f0203fb

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setImageResource(I)V

    .line 2021
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v3, 0x7f0b0277

    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2022
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setFocusable(Z)V

    .line 2023
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

    new-instance v3, Lcom/sec/chaton/buddy/be;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/be;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2039
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

    if-eqz v0, :cond_14

    .line 2040
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setFocusable(Z)V

    .line 2041
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

    new-instance v3, Lcom/sec/chaton/buddy/bf;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/bf;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v3}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2057
    :cond_14
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->v()V

    .line 2060
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2061
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v1, :cond_15

    .line 2062
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0902ee

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v3

    invoke-direct {v0, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2064
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0902ef

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0902f0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v3

    invoke-virtual {v0, v2, v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2065
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0702e8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2066
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0700a0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2067
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f070090

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2068
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07009a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bH:Landroid/view/View;

    .line 2069
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07009b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bI:Landroid/view/View;

    .line 2070
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0700a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    .line 2071
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0700a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bK:Landroid/view/View;

    .line 2073
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07009e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bL:Landroid/view/View;

    .line 2074
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07009f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bM:Landroid/view/View;

    .line 2075
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0700a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bN:Landroid/view/View;

    .line 2076
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0700a2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bO:Landroid/view/View;

    .line 2083
    :cond_15
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2084
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0700a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bR:Landroid/widget/RelativeLayout;

    .line 2087
    :cond_16
    return-void

    .line 1653
    :cond_17
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aT:Landroid/widget/ImageView;

    const v3, 0x7f0202d3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_18
    move v0, v2

    .line 1711
    goto/16 :goto_1

    .line 1858
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1859
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1860
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 1863
    :cond_1a
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1864
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 1891
    :cond_1b
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v7, :cond_1c

    .line 1893
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1895
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1896
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070094

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Y:Landroid/widget/ImageButton;

    .line 1897
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070095

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Z:Landroid/widget/ImageButton;

    .line 1898
    new-instance v0, Lcom/sec/chaton/buddy/bc;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/bc;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 1912
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Y:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1913
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->Z:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1914
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 1917
    :cond_1c
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v3, 0x3e8

    if-ne v0, v3, :cond_12

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-nez v0, :cond_12

    .line 1921
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070096

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    .line 1922
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070097

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    .line 1923
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1924
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1925
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    .line 1927
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v1, :cond_1e

    .line 1928
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1929
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1930
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_3

    .line 1931
    :cond_1d
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 1932
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1933
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_3

    .line 1936
    :cond_1e
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1937
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_3

    .line 1968
    :cond_1f
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->by:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method static synthetic u(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->Q()V

    return-void
.end method

.method static synthetic v(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->N:Landroid/widget/Button;

    return-object v0
.end method

.method private v()V
    .locals 1

    .prologue
    .line 2291
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(I)V

    .line 2292
    return-void
.end method

.method static synthetic w(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bu:Landroid/view/MenuItem;

    return-object v0
.end method

.method private w()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 2648
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 2650
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v3, 0x0

    move-object v0, p0

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z

    move-result v0

    .line 2651
    if-nez v0, :cond_2

    .line 2657
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    if-eqz v0, :cond_1

    .line 2658
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/buddy/db;->a(I)V

    .line 2660
    :cond_1
    return-void

    .line 2648
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0
.end method

.method static synthetic x(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aM:Ljava/util/HashMap;

    return-object v0
.end method

.method private x()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2795
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    .line 2796
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->z:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2797
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->A:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2798
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->B:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2800
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    .line 2804
    return-void
.end method

.method static synthetic y(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    return-object v0
.end method

.method private y()V
    .locals 13

    .prologue
    const v4, 0x7f030087

    const/16 v2, 0x8

    const/4 v10, 0x0

    const/4 v12, 0x1

    .line 2811
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 2812
    iput v12, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    .line 2814
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "viewBuddyList() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 2816
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ao:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 2817
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_1

    .line 2818
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ap:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2819
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v10}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 2820
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->O:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2825
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->z:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2826
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->A:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2827
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->B:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2835
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-nez v0, :cond_8

    .line 2836
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2837
    new-instance v0, Lcom/sec/chaton/buddy/ag;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->an:Ljava/util/ArrayList;

    const v6, 0x7f030128

    iget v7, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-eqz v7, :cond_9

    move v7, v12

    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->t()Z

    move-result v8

    iget-object v11, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cu:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    move-object v9, p0

    invoke-direct/range {v0 .. v11}, Lcom/sec/chaton/buddy/ag;-><init>(Lcom/sec/widget/FastScrollableExpandableListView;Landroid/content/Context;Ljava/util/ArrayList;ILjava/util/ArrayList;IZZLcom/sec/chaton/buddy/aq;ZLcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    .line 2844
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/buddy/ag;->a(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 2845
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/ag;->a(I)V

    .line 2846
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/ag;->b(Ljava/lang/String;)V

    .line 2856
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2858
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2859
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030029

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v1, v2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2862
    invoke-virtual {v1, v12}, Landroid/view/View;->setFocusable(Z)V

    move-object v0, v1

    .line 2863
    check-cast v0, Landroid/view/ViewGroup;

    const/high16 v2, 0x40000

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 2864
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v10}, Lcom/sec/widget/FastScrollableExpandableListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2865
    const v0, 0x7f070127

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2866
    const v2, 0x1020001

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bZ:Landroid/widget/CheckBox;

    .line 2867
    invoke-virtual {v0, v12}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 2868
    const v1, 0x7f0202dc

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 2869
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09001d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09001f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 2871
    new-instance v1, Lcom/sec/chaton/buddy/bi;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/bi;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2886
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bZ:Landroid/widget/CheckBox;

    invoke-virtual {v0, v12}, Landroid/widget/CheckBox;->setDuplicateParentStateEnabled(Z)V

    .line 2888
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bZ:Landroid/widget/CheckBox;

    invoke-virtual {v0, v12}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 2891
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v12}, Lcom/sec/widget/FastScrollableExpandableListView;->setItemsCanFocus(Z)V

    .line 2893
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/buddy/ag;->a(Lcom/sec/chaton/buddy/ao;)V

    .line 2898
    :cond_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->bQ:Z

    if-ne v0, v12, :cond_5

    .line 2899
    :cond_3
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_4

    .line 2900
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->z()V

    .line 2902
    :cond_4
    sput-boolean v10, Lcom/sec/chaton/buddy/BuddyFragment;->bQ:Z

    .line 2907
    :cond_5
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v12, :cond_6

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_7

    .line 2908
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/buddy/ag;->a(Lcom/sec/chaton/buddy/ap;)V

    .line 2911
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 2914
    :cond_8
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->A()V

    .line 2915
    return-void

    :cond_9
    move v7, v10

    .line 2837
    goto/16 :goto_0

    .line 2840
    :cond_a
    new-instance v0, Lcom/sec/chaton/buddy/ag;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->an:Ljava/util/ArrayList;

    const v6, 0x7f030125

    iget v7, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-eqz v7, :cond_b

    move v7, v12

    :goto_2
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->t()Z

    move-result v8

    iget-object v11, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cu:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    move-object v9, p0

    invoke-direct/range {v0 .. v11}, Lcom/sec/chaton/buddy/ag;-><init>(Lcom/sec/widget/FastScrollableExpandableListView;Landroid/content/Context;Ljava/util/ArrayList;ILjava/util/ArrayList;IZZLcom/sec/chaton/buddy/aq;ZLcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    goto/16 :goto_1

    :cond_b
    move v7, v10

    goto :goto_2
.end method

.method static synthetic z(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    return-object v0
.end method

.method private z()V
    .locals 4

    .prologue
    .line 2918
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03001c

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2919
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1, v0}, Lcom/sec/widget/FastScrollableExpandableListView;->addHeaderView(Landroid/view/View;)V

    .line 2920
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->setItemsCanFocus(Z)V

    .line 2921
    return-void
.end method


# virtual methods
.method public a(F)I
    .locals 2

    .prologue
    .line 10751
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 10752
    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 10753
    return v0
.end method

.method public a([Ljava/lang/String;)Lcom/sec/chaton/buddy/a/c;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2120
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_2

    .line 2121
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/ag;->c()Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    .line 2122
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    move v3, v2

    .line 2123
    :goto_1
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 2124
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    array-length v5, p1

    if-ne v0, v5, :cond_0

    .line 2125
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/util/Set;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get selected buddy group : group name - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2127
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 2133
    :goto_2
    return-object v0

    .line 2123
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    .line 2122
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 2133
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method a(I)V
    .locals 4

    .prologue
    const v3, 0x7f0b0416

    .line 2298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setTitle() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BuddyFragment"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 2299
    const/4 v0, 0x0

    .line 2300
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 2414
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 2415
    if-ltz p1, :cond_1

    .line 2416
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2425
    :cond_1
    if-eqz v0, :cond_2

    .line 2426
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 2429
    :cond_2
    return-void

    .line 2302
    :cond_3
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 2308
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2309
    :cond_4
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    .line 2310
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2318
    :cond_5
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0xf

    if-ne v1, v2, :cond_6

    .line 2323
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2325
    :cond_6
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0xe

    if-ne v1, v2, :cond_7

    .line 2330
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2333
    :cond_7
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_8

    .line 2334
    const v0, 0x7f0b0148

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2350
    :cond_8
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_9

    .line 2351
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2354
    :cond_9
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0x14

    if-eq v1, v2, :cond_0

    .line 2355
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_a

    .line 2363
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ag:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2365
    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2369
    :pswitch_1
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2373
    :pswitch_2
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2384
    :cond_a
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_b

    .line 2386
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2387
    :cond_b
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    .line 2391
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_c

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v2, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ac:Landroid/os/Bundle;

    const-string v2, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 2393
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2394
    :cond_c
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0x12

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0x13

    if-eq v1, v2, :cond_0

    .line 2399
    const v0, 0x7f0b0177

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2400
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_d

    .line 2401
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    .line 2403
    :cond_d
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_e

    .line 2404
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ag:I

    if-lez v0, :cond_f

    .line 2405
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ag:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2410
    :cond_e
    :goto_1
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-nez v1, :cond_0

    .line 2411
    const/4 p1, -0x1

    goto/16 :goto_0

    .line 2407
    :cond_f
    const v0, 0x7f0b013a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2363
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/sec/chaton/buddy/a/c;)V
    .locals 2

    .prologue
    .line 2734
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aq:Lcom/sec/chaton/buddy/a/c;

    .line 2735
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->Y()V

    .line 2736
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->q:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 2737
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bo:Ljava/lang/String;

    .line 2738
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;)Lcom/sec/chaton/d/a/h;

    .line 2739
    return-void
.end method

.method public a(Lcom/sec/chaton/buddy/a/c;Landroid/view/View;)V
    .locals 11

    .prologue
    const v10, 0x7f070079

    const/16 v2, 0x3e8

    const/16 v9, 0x8

    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 9925
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v2, :cond_a

    .line 9926
    :cond_0
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 9927
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-eq v0, v4, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    .line 9929
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v7

    .line 9931
    if-eqz v7, :cond_7

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 9932
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 9934
    :cond_2
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 9935
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 9936
    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 9940
    iget-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    if-eqz v2, :cond_1b

    .line 9941
    const/4 v3, 0x0

    .line 9944
    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p2

    .line 9945
    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 9946
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->d()Z

    move-result v0

    if-nez v0, :cond_3

    move v2, v4

    :goto_2
    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z

    .line 9947
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    move-object v0, p2

    .line 9948
    check-cast v0, Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableExpandableListView;->d()Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v4

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_3
    move v2, v6

    .line 9946
    goto :goto_2

    :cond_4
    move v1, v6

    .line 9948
    goto :goto_3

    .line 9950
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->d()Z

    move-result v2

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z

    move-object v0, p2

    .line 9951
    check-cast v0, Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableExpandableListView;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 9956
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    if-eqz v0, :cond_7

    .line 9957
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/buddy/db;->a(I)V

    :cond_7
    move-object v0, p2

    .line 9960
    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 9961
    check-cast p2, Landroid/widget/CheckBox;

    invoke-virtual {p2, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 10009
    :cond_8
    :goto_4
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v4, :cond_a

    .line 10010
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v4, :cond_17

    .line 10011
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Z)V

    .line 10012
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_16

    .line 10013
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 10014
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 10015
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 10032
    :cond_9
    :goto_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_a

    .line 10033
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    .line 10038
    :cond_a
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_19

    .line 10039
    sput-boolean v4, Lcom/sec/chaton/buddy/BuddyFragment;->l:Z

    .line 10040
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v4, :cond_b

    .line 10041
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bR:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 10042
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bH:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 10043
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 10044
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bI:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 10056
    :cond_b
    :goto_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v4, :cond_d

    .line 10057
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1a

    .line 10058
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 10065
    :cond_d
    :goto_7
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 10066
    :goto_8
    return-void

    .line 9963
    :cond_e
    check-cast p2, Landroid/widget/CheckBox;

    invoke-virtual {p2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_4

    .line 9967
    :cond_f
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-nez v0, :cond_11

    .line 9969
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/ag;->d(Ljava/lang/String;)V

    .line 9971
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v2, :cond_8

    .line 9973
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ba:Z

    if-eqz v0, :cond_10

    .line 9974
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 9976
    :cond_10
    new-array v0, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->c([Ljava/lang/String;)V

    goto :goto_8

    .line 9981
    :cond_11
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v1

    .line 9982
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9983
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v2

    .line 9984
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_13

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ck:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    if-le v0, v3, :cond_13

    .line 9985
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/sec/widget/FastScrollableExpandableListView;->setChildChecked(Ljava/lang/String;Z)V

    .line 9986
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showSelectionReachedMaximumPopup "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9987
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cn:I

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cm:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/CharSequence;)V

    .line 10000
    :cond_12
    :goto_9
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac()Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_15

    .line 10001
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v6}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_4

    .line 9989
    :cond_13
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    move-object v0, p2

    .line 9991
    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_14

    if-eq v1, v2, :cond_14

    .line 9992
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/widget/FastScrollableExpandableListView;->setChildChecked(Ljava/lang/String;Z)V

    .line 9993
    check-cast p2, Landroid/widget/CheckBox;

    invoke-virtual {p2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_9

    .line 9995
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/sec/widget/FastScrollableExpandableListView;->setChildChecked(Ljava/lang/String;Z)V

    .line 9996
    check-cast p2, Landroid/widget/CheckBox;

    invoke-virtual {p2, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_9

    .line 10004
    :cond_15
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_4

    .line 10018
    :cond_16
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    goto/16 :goto_5

    .line 10021
    :cond_17
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_18

    .line 10022
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 10023
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 10024
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 10025
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 10028
    :cond_18
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 10029
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    goto/16 :goto_5

    .line 10047
    :cond_19
    sput-boolean v6, Lcom/sec/chaton/buddy/BuddyFragment;->l:Z

    .line 10048
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v4, :cond_b

    .line 10049
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bH:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 10050
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 10060
    :cond_1a
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    :cond_1b
    move-object v3, v0

    goto/16 :goto_1
.end method

.method public a(Lcom/sec/chaton/buddy/cy;)V
    .locals 0

    .prologue
    .line 9809
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->be:Lcom/sec/chaton/buddy/cy;

    .line 9810
    return-void
.end method

.method public a(Lcom/sec/chaton/buddy/da;)V
    .locals 0

    .prologue
    .line 1102
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bX:Lcom/sec/chaton/buddy/da;

    .line 1103
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2442
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, p1}, Lcom/sec/widget/FastScrollableExpandableListView;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z

    .line 2443
    return-void

    :cond_0
    move v2, v4

    .line 2442
    goto :goto_0
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 6

    .prologue
    .line 2446
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z

    .line 2447
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/a/c;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2742
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 2743
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->b()Ljava/util/HashSet;

    move-result-object v0

    .line 2744
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2745
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v0

    .line 2746
    if-nez v0, :cond_1

    .line 2747
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z

    goto :goto_0

    .line 2751
    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v2, 0x0

    .line 2664
    move v7, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 2665
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/ArrayList;

    move v8, v2

    .line 2666
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_0

    .line 2667
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z

    .line 2666
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 2664
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 2670
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_3

    .line 2690
    :cond_2
    :goto_2
    return-void

    .line 2673
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f070079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2674
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    .line 2675
    sput-boolean v2, Lcom/sec/chaton/buddy/BuddyFragment;->l:Z

    .line 2676
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 2677
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    if-eqz v0, :cond_4

    .line 2678
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/buddy/db;->a(I)V

    .line 2682
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2683
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    .line 2684
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2686
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 9639
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v1

    if-lez v1, :cond_0

    .line 9640
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Z)V

    .line 9641
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f070079

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 9642
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    .line 9645
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 5942
    .line 5946
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "buddy_no=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 5948
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 5949
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 5950
    const-string v0, "islike"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Y"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    move v0, v6

    .line 5955
    :goto_0
    if-eqz v1, :cond_0

    .line 5956
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5960
    :cond_0
    :goto_1
    return v0

    .line 5952
    :catch_0
    move-exception v0

    move-object v0, v7

    .line 5953
    :goto_2
    :try_start_2
    const-string v1, "CursorIndexOob"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 5955
    if-eqz v0, :cond_1

    .line 5956
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v6

    .line 5958
    goto :goto_1

    .line 5955
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v7, :cond_2

    .line 5956
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 5955
    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    move-object v7, v1

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_3

    .line 5952
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_2

    :cond_3
    move v0, v6

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z
    .locals 6

    .prologue
    const v4, 0x7f070079

    const/16 v5, 0x3e8

    const/4 v2, 0x2

    const/16 v3, 0x8

    const/4 v0, 0x1

    .line 2450
    if-eqz p2, :cond_8

    .line 2451
    if-nez p3, :cond_2

    .line 2452
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v1

    .line 2453
    if-nez v1, :cond_1

    .line 2522
    :cond_0
    :goto_0
    return v0

    .line 2456
    :cond_1
    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object p3

    .line 2458
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2459
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v1, :cond_3

    .line 2460
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1, p1, p2}, Lcom/sec/widget/FastScrollableExpandableListView;->setChildChecked(Ljava/lang/String;Z)V

    .line 2462
    :cond_3
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-ne v1, v2, :cond_4

    .line 2463
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    invoke-virtual {v1, p1, p3}, Lcom/sec/widget/ChoicePanel;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 2480
    :cond_4
    :goto_1
    if-nez p4, :cond_0

    .line 2484
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v1, :cond_5

    .line 2485
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v2}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/sec/chaton/buddy/db;->a(I)V

    .line 2488
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v1

    if-nez v1, :cond_9

    .line 2489
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2490
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2491
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f07009a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2492
    if-eqz v1, :cond_6

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v2, v0, :cond_6

    .line 2493
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2518
    :cond_6
    :goto_2
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v1, v5, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    const-string v2, "voip"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 2466
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v1, :cond_4

    .line 2467
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1, p1}, Lcom/sec/widget/FastScrollableExpandableListView;->a(Ljava/lang/String;)Z

    move-result v1

    if-eq v1, p2, :cond_4

    .line 2468
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ad()V

    .line 2469
    const/4 v0, 0x0

    goto :goto_0

    .line 2474
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1, p1, p2}, Lcom/sec/widget/FastScrollableExpandableListView;->setChildChecked(Ljava/lang/String;Z)V

    .line 2475
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-ne v1, v2, :cond_4

    .line 2476
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    invoke-virtual {v1, p1}, Lcom/sec/widget/ChoicePanel;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 2497
    :cond_9
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v1, v0, :cond_a

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v1, v5, :cond_c

    .line 2499
    :cond_a
    if-eqz p5, :cond_b

    .line 2500
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v1

    .line 2501
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v2

    if-ne v2, v0, :cond_b

    if-eqz v1, :cond_b

    .line 2502
    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Z)V

    .line 2506
    :cond_b
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2507
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f07009a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2508
    if-eqz v1, :cond_6

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v2, v0, :cond_6

    .line 2509
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    invoke-virtual {v4}, Lcom/sec/widget/ChoicePanel;->getHeight()I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2510
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 2514
    :cond_c
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public a(Ljava/util/Set;[Ljava/lang/String;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2137
    const/4 v0, 0x1

    move v2, v0

    move v0, v1

    .line 2138
    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_1

    .line 2139
    aget-object v3, p2, v0

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 2140
    if-nez v3, :cond_0

    move v2, v1

    .line 2138
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2144
    :cond_1
    return v2
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 713
    invoke-static {p0, v5}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 715
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 716
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 717
    sget-object v0, Lcom/sec/chaton/buddy/EmptyFragment;->c:Ljava/lang/String;

    sget-object v2, Lcom/sec/chaton/buddy/EmptyFragment;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 718
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const v2, 0x7f070008

    const-class v3, Lcom/sec/chaton/buddy/EmptyFragment;

    invoke-virtual {v0, v2, v1, v3}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 722
    :cond_0
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v5, :cond_2

    .line 723
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_1

    .line 724
    sput-boolean v5, Lcom/sec/chaton/buddy/BuddyFragment;->l:Z

    .line 726
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 728
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bd:Ljava/util/ArrayList;

    .line 729
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bf:Ljava/util/HashMap;

    .line 730
    iput-boolean v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->h:Z

    .line 731
    iput-boolean v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    .line 732
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onTabSelected() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v5, :cond_3

    .line 735
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 738
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    if-ne v0, v5, :cond_4

    .line 739
    invoke-virtual {p0, v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Z)V

    .line 740
    sput-boolean v6, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 743
    :cond_4
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->az:Z

    if-eqz v0, :cond_5

    .line 744
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->b()V

    .line 747
    :cond_5
    iput-boolean v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->az:Z

    .line 750
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aQ:Lcom/sec/chaton/util/bp;

    if-nez v0, :cond_c

    .line 751
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ae()V

    .line 756
    :goto_0
    iput-boolean v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bc:Z

    .line 757
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_6

    .line 758
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v5}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    .line 761
    :cond_6
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Is Group Uploaded"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    .line 762
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->K()V

    .line 766
    :cond_7
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-nez v0, :cond_8

    .line 767
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    .line 771
    :cond_8
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 772
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 773
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 774
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 775
    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 776
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 778
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 779
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ct:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 782
    :cond_9
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aW:Landroid/widget/ImageView;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->c(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 802
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v5, :cond_b

    .line 803
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, v7, v7}, Lcom/sec/chaton/buddy/ag;->a(II)V

    .line 804
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_d

    .line 805
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 811
    :cond_b
    :goto_1
    return-void

    .line 753
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aQ:Lcom/sec/chaton/util/bp;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bp;->a()V

    goto/16 :goto_0

    .line 807
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 8175
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aI:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 8176
    if-lez p1, :cond_2

    .line 8177
    const/16 v0, 0x64

    if-lt p1, v0, :cond_1

    .line 8178
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aI:Landroid/widget/TextView;

    const v1, 0x7f0b005a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 8182
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aI:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 8201
    :cond_0
    :goto_1
    return-void

    .line 8180
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aI:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 8196
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aI:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public b([Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2695
    move v6, v2

    :goto_0
    array-length v0, p1

    if-ge v6, v0, :cond_0

    .line 2696
    aget-object v1, p1, v6

    const/4 v3, 0x0

    move-object v0, p0

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z

    .line 2695
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2698
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    if-eqz v0, :cond_1

    .line 2699
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/buddy/db;->a(I)V

    .line 2701
    :cond_1
    return-void
.end method

.method public b(Lcom/sec/chaton/buddy/a/c;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2716
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 2727
    :cond_0
    :goto_0
    return v0

    .line 2721
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2722
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aJ:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/sec/chaton/buddy/a/c;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2725
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 815
    invoke-static {p0, v2}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 816
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 817
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 822
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 823
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 824
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/TabActivity;->a(Z)V

    .line 841
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->N()V

    .line 843
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    .line 845
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ct:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 847
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onTabUnSelected() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bc:Z

    .line 849
    return-void

    .line 825
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_1

    .line 826
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 829
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_1

    .line 830
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public c(Lcom/sec/chaton/buddy/a/c;)V
    .locals 1

    .prologue
    .line 8778
    new-instance v0, Lcom/sec/chaton/buddy/a/c;

    invoke-direct {v0, p1}, Lcom/sec/chaton/buddy/a/c;-><init>(Lcom/sec/chaton/buddy/a/c;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aq:Lcom/sec/chaton/buddy/a/c;

    .line 8779
    return-void
.end method

.method public c(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 10570
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    const-string v3, "group_relation_buddy=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 10572
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 10573
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 10574
    const-string v0, "group_relation_group"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 10576
    :goto_0
    if-eqz v1, :cond_0

    .line 10577
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 10579
    :cond_0
    return v0

    :cond_1
    move v0, v6

    goto :goto_0
.end method

.method public d()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const v7, 0x7f070008

    const/4 v1, 0x0

    .line 657
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 658
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->an:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bD:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 663
    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bC:I

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v0, -0x1

    if-gt v3, v4, :cond_0

    .line 664
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bC:I

    add-int/lit8 v2, v0, 0x1

    .line 665
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bD:I

    .line 676
    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->an:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 678
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0999"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 679
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 680
    const-class v2, Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 681
    const-string v2, "specialuserid"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 682
    const-string v2, "speicalusername"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 683
    const-string v0, "specialBuddyAdded"

    invoke-virtual {v1, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 684
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const-class v2, Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v0, v7, v1, v2}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 702
    :goto_1
    return-void

    .line 666
    :cond_0
    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bC:I

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v0, v0, -0x1

    if-le v3, v0, :cond_5

    .line 667
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bD:I

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v3, v2, -0x1

    if-gt v0, v3, :cond_1

    .line 669
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bD:I

    add-int/lit8 v0, v0, 0x1

    move v2, v1

    goto :goto_0

    .line 670
    :cond_1
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bD:I

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v2, -0x1

    if-le v0, v2, :cond_5

    .line 671
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v7, v1, v2}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_1

    .line 685
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 686
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 687
    const-class v3, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 688
    new-instance v3, Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v6

    if-nez v6, :cond_3

    :goto_2
    const/4 v0, 0x2

    invoke-direct {v3, v4, v5, v1, v0}, Lcom/sec/chaton/buddy/a/b;-><init>(ILjava/lang/String;II)V

    .line 689
    const-string v0, "ACTIVITY_PURPOSE"

    const/16 v1, 0x12

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 690
    const-string v0, "groupInfo"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 691
    const-string v0, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 692
    const-string v0, "LAUNCH_AS_CHILD"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 693
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const-class v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v0, v7, v2, v1}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 688
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v1

    goto :goto_2

    .line 695
    :cond_4
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 696
    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 697
    const-string v2, "PROFILE_BUDDY_NO"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 698
    const-string v2, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 699
    const-string v0, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 700
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v0, v7, v1, v2}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    goto/16 :goto_1

    :cond_5
    move v0, v1

    move v2, v1

    goto/16 :goto_0
.end method

.method public e()V
    .locals 7

    .prologue
    const/16 v4, 0xc7

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 8791
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    .line 8792
    if-lez v0, :cond_0

    .line 8793
    if-le v0, v4, :cond_1

    .line 8794
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 8853
    :cond_0
    :goto_0
    return-void

    .line 8798
    :cond_1
    if-ne v0, v6, :cond_2

    .line 8799
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/cz;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 8803
    :cond_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8804
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 8809
    :goto_1
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 8810
    const-string v0, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8811
    const-string v0, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8812
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8813
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    invoke-virtual {v0, v3}, Lcom/sec/widget/ChoicePanel;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 8814
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 8815
    :goto_2
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    if-ge v2, v5, :cond_4

    .line 8816
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8815
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 8806
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object v1, v0

    goto :goto_1

    .line 8818
    :cond_4
    const-string v2, "receivers"

    new-array v0, v3, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 8820
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    if-eqz v0, :cond_5

    .line 8821
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-ne v0, v2, :cond_5

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/sec/widget/FastScrollableExpandableListView;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 8822
    const-string v0, "groupId"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bt:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8825
    :cond_5
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aB:Z

    if-eqz v0, :cond_6

    .line 8826
    const-string v0, "content_type"

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aC:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8827
    const-string v0, "download_uri"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aD:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8829
    const-string v0, "sub_content"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aE:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8831
    const-string v0, "forward_sender_name"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aF:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8832
    const-string v0, "is_forward_mode"

    iget-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bg:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8834
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aG:Lcom/sec/chaton/w;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 8838
    :cond_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 8839
    const-string v0, "callChatList"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8842
    :cond_7
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    .line 8844
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 8845
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->n:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 8848
    :cond_8
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bc:Z

    if-nez v0, :cond_0

    .line 8849
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0
.end method

.method public f()V
    .locals 7

    .prologue
    const/16 v4, 0xc8

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 8856
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    .line 8857
    if-lez v0, :cond_0

    .line 8858
    if-le v0, v4, :cond_1

    .line 8859
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 8913
    :cond_0
    :goto_0
    return-void

    .line 8865
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8866
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 8872
    :goto_1
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8873
    const-string v0, "broadcast2_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8874
    const-string v0, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8878
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->at:Lcom/sec/widget/ChoicePanel;

    invoke-virtual {v0, v3}, Lcom/sec/widget/ChoicePanel;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 8879
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 8880
    :goto_3
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    if-ge v2, v5, :cond_4

    .line 8881
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8880
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 8868
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8869
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object v1, v0

    goto :goto_1

    .line 8876
    :cond_3
    const-string v0, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    .line 8883
    :cond_4
    const-string v2, "receivers"

    new-array v0, v3, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 8886
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aB:Z

    if-eqz v0, :cond_5

    .line 8887
    const-string v0, "content_type"

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aC:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8889
    const-string v0, "sub_content"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aE:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8891
    const-string v0, "forward_sender_name"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aF:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8892
    const-string v0, "is_forward_mode"

    iget-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bg:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8894
    const-string v0, "download_uri"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aD:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8895
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aG:Lcom/sec/chaton/w;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 8899
    :cond_5
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 8900
    const-string v0, "callChatList"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8903
    :cond_6
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    .line 8905
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 8906
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->n:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 8909
    :cond_7
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bc:Z

    if-nez v0, :cond_0

    .line 8910
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0
.end method

.method public g()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9244
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_0

    .line 9245
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->b()Ljava/util/HashSet;

    move-result-object v0

    .line 9247
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    goto :goto_0
.end method

.method public h()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 9251
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_0

    .line 9252
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->c()[Ljava/lang/String;

    move-result-object v0

    .line 9254
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 9259
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_0

    .line 9260
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v0

    .line 9262
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 9267
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cb:Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public k()Lcom/sec/widget/FastScrollableExpandableListView;
    .locals 1

    .prologue
    .line 9649
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    return-object v0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 9797
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_0

    .line 9798
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->requestFocus()Z

    .line 9800
    :cond_0
    return-void
.end method

.method public m()V
    .locals 8

    .prologue
    const v7, 0x7f0b0277

    const v6, 0x7f0b0092

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/16 v3, 0x8

    .line 9838
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_1

    .line 9839
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 9840
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 9918
    :cond_0
    :goto_0
    return-void

    .line 9845
    :cond_1
    const-string v0, "for_wifi_only_device"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/chaton/util/am;->w()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 9849
    :cond_2
    iput-boolean v4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bm:Z

    .line 9855
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bn:Z

    .line 9857
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v4, :cond_3

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_b

    .line 9858
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

    if-eqz v0, :cond_4

    .line 9859
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bm:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bn:Z

    if-nez v0, :cond_6

    .line 9860
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 9861
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 9910
    :cond_4
    :goto_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bR:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 9911
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

    if-eqz v0, :cond_0

    .line 9912
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bk:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ImageTextViewGroup;->getVisibility()I

    move-result v0

    if-ne v3, v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bl:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ImageTextViewGroup;->getVisibility()I

    move-result v0

    if-ne v3, v0, :cond_0

    .line 9913
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bR:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 9851
    :cond_5
    iput-boolean v5, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bm:Z

    goto :goto_1

    .line 9862
    :cond_6
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bm:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bn:Z

    if-ne v0, v4, :cond_8

    .line 9863
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 9864
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    if-eqz v0, :cond_7

    .line 9865
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v7}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setText(I)V

    .line 9866
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v1, 0x7f0203fb

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setImageResource(I)V

    .line 9867
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {p0, v7}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 9868
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setFocusable(Z)V

    .line 9869
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    new-instance v1, Lcom/sec/chaton/buddy/cl;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/cl;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 9878
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 9879
    :cond_8
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bm:Z

    if-ne v0, v4, :cond_a

    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bn:Z

    if-nez v0, :cond_a

    .line 9880
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 9881
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    if-eqz v0, :cond_9

    .line 9882
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v6}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setText(I)V

    .line 9883
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    const v1, 0x7f0203fa

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setImageResource(I)V

    .line 9884
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {p0, v6}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 9885
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setFocusable(Z)V

    .line 9886
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bi:Lcom/sec/chaton/widget/ImageTextViewGroup;

    new-instance v1, Lcom/sec/chaton/buddy/cm;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/cm;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ImageTextViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 9896
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 9897
    :cond_a
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bm:Z

    if-ne v0, v4, :cond_4

    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bn:Z

    if-ne v0, v4, :cond_4

    .line 9898
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 9899
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 9903
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 9904
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 9905
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public n()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 10629
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_0

    .line 10630
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/buddy/ag;->b(Z)V

    .line 10631
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/ag;->notifyDataSetChanged()V

    .line 10635
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v3, :cond_4

    .line 10637
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_2

    .line 10638
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bI:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 10639
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bK:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 10649
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10650
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bH:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10671
    :cond_1
    :goto_1
    return-void

    .line 10641
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bI:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10642
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bR:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 10643
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bK:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 10645
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bK:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 10654
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aR:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 10655
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bL:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10656
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bM:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 10659
    :cond_5
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aU:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 10660
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bN:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10661
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bO:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 10664
    :cond_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bR:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10665
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10666
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bK:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public o()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 10674
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_0

    .line 10675
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/buddy/ag;->b(Z)V

    .line 10676
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/ag;->notifyDataSetChanged()V

    .line 10680
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 10682
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_2

    .line 10683
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bH:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 10684
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 10694
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bK:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 10695
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bI:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 10716
    :cond_1
    :goto_1
    return-void

    .line 10686
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bH:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 10687
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bR:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 10688
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 10690
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 10700
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aR:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 10701
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bL:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 10702
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bM:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 10705
    :cond_5
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aU:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 10706
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bN:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 10707
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bO:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 10710
    :cond_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bR:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10711
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 10712
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bK:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 889
    const-string v0, "onActivityCreated()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_b

    .line 899
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    .line 906
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 911
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->p:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aO:Lcom/sec/chaton/e/a/u;

    .line 913
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 915
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 919
    :try_start_0
    const-string v0, "savedInstanceStateCheck"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->e:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 923
    :goto_1
    sget-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->e:Z

    if-eqz v0, :cond_0

    .line 924
    const-string v0, "INDEX"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/chaton/buddy/BuddyFragment;->c:I

    .line 925
    const-string v0, "TOP"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/chaton/buddy/BuddyFragment;->d:I

    .line 929
    :cond_0
    const-string v0, "content_type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 930
    const-string v0, "content_type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aC:I

    .line 931
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Buddy content_type:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aC:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 932
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aB:Z

    .line 934
    :cond_1
    const-string v0, "download_uri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 935
    const-string v0, "download_uri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aD:Ljava/lang/String;

    .line 936
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Buddy download_uri:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aD:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 938
    :cond_2
    const-string v0, "sub_content"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 939
    const-string v0, "sub_content"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aE:Ljava/lang/String;

    .line 940
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Buddy sub_content:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 942
    :cond_3
    const-string v0, "forward_sender_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 943
    const-string v0, "forward_sender_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aF:Ljava/lang/String;

    .line 944
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Buddy mForwardSenderName:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aF:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 948
    :cond_4
    const-string v0, "is_forward_mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 949
    const-string v0, "is_forward_mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bg:Z

    .line 950
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Buddy mIsForward:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bg:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 956
    :cond_5
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 957
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/w;

    check-cast v0, Lcom/sec/chaton/w;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aG:Lcom/sec/chaton/w;

    .line 958
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Buddy mForwardFrom:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aG:Lcom/sec/chaton/w;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 960
    :cond_6
    const-string v0, "null"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 961
    const-string v0, "null"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aZ:Ljava/lang/String;

    .line 962
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Buddy mForwardFrom:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aG:Lcom/sec/chaton/w;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 965
    :cond_7
    const-string v0, "mShowChatONBuddies"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ba:Z

    .line 966
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Buddy mShowChatONBuddies:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ba:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 971
    :cond_8
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->I()V

    .line 972
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->D:Lcom/sec/chaton/util/bt;

    .line 973
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->u()V

    .line 976
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    new-instance v1, Lcom/sec/chaton/buddy/ba;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ba;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 991
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aR:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/buddy/bl;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/bl;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1007
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_9

    .line 1008
    if-nez p1, :cond_9

    .line 1010
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aK:Ljava/util/ArrayList;

    .line 1018
    :cond_9
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_a

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_c

    .line 1019
    :cond_a
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->y()V

    .line 1029
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/buddy/db;

    if-eqz v0, :cond_d

    .line 1030
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/db;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    .line 1077
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v1, Lcom/sec/chaton/buddy/ck;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ck;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1089
    return-void

    .line 901
    :cond_b
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    .line 902
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ag:I

    .line 903
    invoke-static {p0, v3}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    goto/16 :goto_0

    .line 920
    :catch_0
    move-exception v0

    .line 921
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->e:Z

    goto/16 :goto_1

    .line 1021
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->b()V

    goto :goto_2

    .line 1035
    :cond_d
    new-instance v0, Lcom/sec/chaton/buddy/bw;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/bw;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    goto :goto_3
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 8630
    const-string v0, "onActivityResult()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 8631
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 8632
    if-ne p2, v4, :cond_0

    .line 8633
    const-string v0, "Add buddy success"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 8634
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    if-eqz v0, :cond_0

    .line 8635
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->b()V

    .line 8658
    :cond_0
    :goto_0
    return-void

    .line 8643
    :cond_1
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 8644
    if-ne p2, v4, :cond_0

    .line 8645
    const-string v0, "receivers"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 8646
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8648
    const-string v2, "receivers"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 8650
    const-string v0, "inboxNO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->S:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8651
    const-string v0, "buddyNO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->T:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8652
    const-string v0, "chatType"

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->U:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8654
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 8655
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 10386
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 10388
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bA:Lcom/sec/chaton/buddy/cw;

    if-nez v0, :cond_0

    .line 10389
    new-instance v0, Lcom/sec/chaton/buddy/cw;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/buddy/cw;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/ba;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bA:Lcom/sec/chaton/buddy/cw;

    .line 10390
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 10391
    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 10392
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bA:Lcom/sec/chaton/buddy/cw;

    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 10396
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bE:Lcom/sec/chaton/buddy/cx;

    if-nez v0, :cond_1

    .line 10397
    new-instance v0, Lcom/sec/chaton/buddy/cx;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/buddy/cx;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/ba;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bE:Lcom/sec/chaton/buddy/cx;

    .line 10398
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 10399
    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 10400
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bE:Lcom/sec/chaton/buddy/cx;

    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 10403
    :cond_1
    return-void
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 8

    .prologue
    .line 8220
    iput p4, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bC:I

    .line 8221
    iput p3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bD:I

    .line 8224
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->an:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 8228
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8230
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v1, :cond_0

    .line 8231
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v1, p3, p4}, Lcom/sec/chaton/buddy/ag;->a(II)V

    .line 8239
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 8240
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bV:Ljava/lang/String;

    .line 8245
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0094

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 8252
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 8253
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 8254
    const-class v1, Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 8255
    const-string v1, "ACTIVITY_PURPOSE_CALL_MYPAGE"

    const/16 v3, 0x16

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8256
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/TabActivity;

    const v3, 0x7f07000a

    const-class v4, Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v1, v3, v2, v4}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 8259
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v1, :cond_2

    .line 8260
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/ag;->notifyDataSetChanged()V

    .line 8273
    :cond_2
    :goto_0
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_e

    .line 8274
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "0999"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 8276
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 8277
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 8278
    const-class v2, Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 8279
    const-string v2, "specialuserid"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8280
    const-string v2, "speicalusername"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8281
    const-string v0, "specialBuddyAdded"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8282
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const v2, 0x7f070008

    const-class v3, Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v0, v2, v1, v3}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 8543
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->be:Lcom/sec/chaton/buddy/cy;

    if-eqz v0, :cond_4

    .line 8544
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_20

    .line 8545
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->be:Lcom/sec/chaton/buddy/cy;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/chaton/buddy/cy;->a(Z)V

    .line 8551
    :cond_4
    :goto_2
    const/4 v0, 0x1

    :goto_3
    return v0

    .line 8265
    :cond_5
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8266
    const-string v2, "ME_DIALOG_NAME"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8267
    const-string v2, "ME_DIALOG_STATUSMSG"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8268
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 8285
    :cond_6
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8286
    const-string v2, "specialuserid"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8287
    const-string v2, "speicalusername"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8288
    const-string v0, "specialBuddyAdded"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8289
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 8293
    :cond_7
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 8295
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 8297
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->an:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bD:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bC:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/buddy/a/c;

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aq:Lcom/sec/chaton/buddy/a/c;

    .line 8300
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 8301
    const-class v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 8302
    new-instance v3, Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    if-nez v1, :cond_8

    const/4 v1, 0x0

    :goto_4
    const/4 v6, 0x2

    invoke-direct {v3, v4, v5, v1, v6}, Lcom/sec/chaton/buddy/a/b;-><init>(ILjava/lang/String;II)V

    .line 8303
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v4, 0x12

    invoke-virtual {v2, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8304
    const-string v1, "groupInfo"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 8305
    const-string v1, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 8306
    const-string v0, "LAUNCH_AS_CHILD"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8307
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const v1, 0x7f070008

    const-class v3, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 8302
    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    goto :goto_4

    .line 8311
    :cond_9
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v3, Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8312
    const/high16 v1, 0x4000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 8313
    const-string v1, "GROUP_DIALOG_GROUP_NAME"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8314
    const-string v3, "GROUP_DIALOG_CHAT_RECEIVER"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 8316
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 8318
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 8319
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 8320
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_5

    .line 8323
    :cond_a
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 8324
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 8325
    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 8328
    :cond_b
    const-string v3, "GROUP_DIALOG_GROUP_MEMBERS"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8330
    const-string v1, "GROUP_DIALOG_GROUP_ID"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8331
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 8335
    :cond_c
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0094

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 8336
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 8337
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 8338
    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 8339
    const-string v2, "PROFILE_BUDDY_NO"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8340
    const-string v2, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8343
    const-string v0, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8345
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const v2, 0x7f070008

    const-class v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v0, v2, v1, v3}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 8347
    :cond_d
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8348
    const-string v2, "BUDDY_DIALOG_BUDDY_NO"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8349
    const-string v2, "BUDDY_DIALOG_BUDDY_NAME"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8350
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 8351
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 8357
    :cond_e
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 8364
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_f

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_13

    .line 8366
    :cond_f
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v6

    .line 8368
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 8369
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 8371
    :cond_10
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 8372
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 8373
    invoke-virtual {v6, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 8375
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 8378
    iget-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->co:Z

    if-eqz v2, :cond_21

    .line 8380
    const/4 v3, 0x0

    .line 8384
    :goto_6
    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 8385
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0}, Lcom/sec/widget/FastScrollableExpandableListView;->d()Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v2, 0x1

    :goto_7
    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;ZLjava/lang/String;ZZ)Z

    move-result v0

    if-nez v0, :cond_10

    .line 8394
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    if-eqz v0, :cond_3

    .line 8395
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bW:Lcom/sec/chaton/buddy/db;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v1}, Lcom/sec/widget/FastScrollableExpandableListView;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/buddy/db;->a(I)V

    goto/16 :goto_1

    .line 8385
    :cond_12
    const/4 v2, 0x0

    goto :goto_7

    .line 8401
    :cond_13
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    const/16 v2, 0xc7

    if-le v1, v2, :cond_14

    .line 8402
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0077

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0xc7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 8405
    :cond_14
    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aB:Z

    if-eqz v1, :cond_15

    .line 8406
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v3, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8407
    const/high16 v1, 0x4000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 8408
    const-string v1, "chatType"

    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v3}, Lcom/sec/chaton/e/r;->a()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8409
    const-string v3, "receivers"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 8410
    const-string v1, "groupId"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8411
    const-string v0, "content_type"

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aC:I

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8412
    const-string v0, "download_uri"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aD:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8413
    const-string v0, "sub_content"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aE:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8414
    const-string v0, "forward_sender_name"

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aF:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8415
    const-string v0, "is_forward_mode"

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bg:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8417
    sget-object v0, Lcom/sec/chaton/u;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aG:Lcom/sec/chaton/w;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 8419
    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 8421
    :cond_15
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v3, Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 8422
    const/high16 v1, 0x4000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 8423
    const-string v1, "GROUP_DIALOG_GROUP_NAME"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8424
    const-string v3, "GROUP_DIALOG_CHAT_RECEIVER"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 8426
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 8428
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 8429
    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 8430
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_8

    .line 8433
    :cond_16
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 8434
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 8435
    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 8438
    :cond_17
    const-string v3, "GROUP_DIALOG_GROUP_MEMBERS"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8440
    const-string v1, "GROUP_DIALOG_GROUP_ID"

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8441
    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 8457
    :cond_18
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-nez v1, :cond_1e

    .line 8460
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "0999"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 8467
    :cond_19
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v1, :cond_1a

    .line 8468
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/chaton/buddy/ag;->d(Ljava/lang/String;)V

    .line 8497
    :cond_1a
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v2, 0x3e8

    if-ne v1, v2, :cond_1c

    .line 8499
    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ba:Z

    if-eqz v1, :cond_1b

    .line 8500
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8504
    :goto_9
    const/4 v0, 0x1

    goto/16 :goto_3

    .line 8502
    :cond_1b
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->c([Ljava/lang/String;)V

    goto :goto_9

    .line 8507
    :cond_1c
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "0999"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 8511
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/buddy/cz;->b(Ljava/lang/String;)V

    .line 8512
    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aq:Lcom/sec/chaton/buddy/a/c;

    goto/16 :goto_1

    .line 8514
    :cond_1d
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/buddy/cz;->a(Ljava/lang/String;)V

    .line 8515
    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aq:Lcom/sec/chaton/buddy/a/c;

    goto/16 :goto_1

    .line 8525
    :cond_1e
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8527
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    .line 8528
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ac()Z

    move-result v1

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1f

    .line 8529
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 8536
    :goto_a
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aM:Ljava/util/HashMap;

    if-eqz v1, :cond_3

    .line 8537
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aM:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 8532
    :cond_1f
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->P:Landroid/widget/CheckedTextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_a

    .line 8547
    :cond_20
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->be:Lcom/sec/chaton/buddy/cy;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/chaton/buddy/cy;->a(Z)V

    goto/16 :goto_2

    :cond_21
    move-object v3, v0

    goto/16 :goto_6
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 1470
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1471
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_2

    .line 1472
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->F:Lcom/sec/widget/EditTextWithClearButton;

    if-eqz v0, :cond_0

    .line 1473
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->F:Lcom/sec/widget/EditTextWithClearButton;

    iget-object v0, v0, Lcom/sec/widget/EditTextWithClearButton;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 1475
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->al()V

    .line 1476
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    .line 1484
    :cond_1
    :goto_0
    return-void

    .line 1477
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1478
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v2, :cond_1

    .line 1479
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->am()V

    .line 1480
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1481
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x7f0e0000

    const/4 v7, 0x1

    .line 5965
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aq:Lcom/sec/chaton/buddy/a/c;

    .line 5968
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 6164
    :goto_0
    :pswitch_0
    return v7

    .line 5970
    :pswitch_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v7, :cond_0

    .line 5971
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030016"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 5974
    :cond_0
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0999"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5975
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v3, Lcom/sec/chaton/buddy/SpecialBuddyActivity;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5976
    const-string v1, "specialuserid"

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5977
    const-string v1, "speicalusername"

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5978
    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5979
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 5982
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v3, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5984
    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5985
    const-string v1, "PROFILE_BUDDY_NAME"

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5987
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5988
    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5992
    :cond_2
    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 5998
    :pswitch_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v7, :cond_3

    .line 5999
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030011"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 6001
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/buddy/cz;->a(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 6005
    :pswitch_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v7, :cond_4

    .line 6006
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030012"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 6008
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/buddy/cz;->b(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 6012
    :pswitch_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v7, :cond_5

    .line 6013
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030013"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 6015
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_6

    .line 6016
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/buddy/ag;->f(Ljava/lang/String;)V

    .line 6017
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aw;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 6018
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/af;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 6020
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/buddy/cz;->c(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 6024
    :pswitch_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v7, :cond_7

    .line 6025
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030014"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 6027
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_8

    .line 6028
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/buddy/ag;->g(Ljava/lang/String;)V

    .line 6029
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aw;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 6030
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/af;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 6032
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/buddy/cz;->d(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 6036
    :pswitch_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v7, :cond_9

    .line 6037
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030029"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 6039
    :cond_9
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->Y()V

    .line 6040
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/buddy/cz;->e(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 6044
    :pswitch_7
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v7, :cond_a

    .line 6045
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030029"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 6047
    :cond_a
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->Y()V

    .line 6048
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->q:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6052
    :pswitch_8
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v7, :cond_b

    .line 6053
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030017"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 6055
    :cond_b
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->Y()V

    .line 6056
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->q:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    .line 6058
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/sec/chaton/d/f;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 6092
    :pswitch_9
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-direct {p0, v2, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/a/c;I)V

    goto/16 :goto_0

    .line 6096
    :pswitch_a
    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    .line 6098
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-ne v3, v7, :cond_c

    .line 6099
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v3

    const-string v4, "00030018"

    invoke-virtual {v3, v4}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 6102
    :cond_c
    new-instance v3, Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v5

    if-nez v0, :cond_e

    move v0, v1

    :goto_1
    const/4 v6, 0x2

    invoke-direct {v3, v4, v5, v0, v6}, Lcom/sec/chaton/buddy/a/b;-><init>(ILjava/lang/String;II)V

    .line 6104
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 6105
    sput-boolean v7, Lcom/sec/chaton/buddy/BuddyFragment;->bQ:Z

    .line 6108
    :cond_d
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v5, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;

    invoke-direct {v4, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 6109
    const-string v0, "ACTIVITY_PURPOSE"

    const/16 v5, 0x12

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6110
    const-string v0, "groupInfo"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 6111
    const-string v3, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v4, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 6114
    invoke-virtual {p0, v4}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 6102
    :cond_e
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    goto :goto_1

    .line 6119
    :pswitch_b
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v7, :cond_f

    .line 6120
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030019"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 6123
    :cond_f
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0079

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b007a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0037

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/buddy/bu;

    invoke-direct {v3, p0, v2}, Lcom/sec/chaton/buddy/bu;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;Lcom/sec/chaton/buddy/a/c;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 6145
    :pswitch_c
    invoke-direct {p0, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->d(Lcom/sec/chaton/buddy/a/c;)V

    goto/16 :goto_0

    .line 5968
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_a
        :pswitch_4
        :pswitch_5
        :pswitch_9
        :pswitch_6
        :pswitch_7
        :pswitch_c
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 853
    const-string v0, "onCreate()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 856
    iput-object p0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->g:Lcom/sec/chaton/buddy/BuddyFragment;

    .line 857
    new-instance v0, Lcom/sec/chaton/buddy/cz;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/cz;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    .line 858
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aN:Landroid/widget/Toast;

    .line 860
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->u:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 861
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/n;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->u:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 862
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->u:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 864
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->u:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 866
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->u:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 868
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ae()V

    .line 870
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->j:Lcom/coolots/sso/a/a;

    .line 871
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->h()V

    .line 872
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 873
    const-string v1, "buddy_tab_badge_update"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 874
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cs:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 875
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x7

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 5748
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 5752
    new-instance v7, Lcom/sec/chaton/b/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v7, v0, p1}, Lcom/sec/chaton/b/a;-><init>(Landroid/content/Context;Landroid/view/ContextMenu;)V

    .line 5754
    instance-of v0, p3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;

    if-nez v0, :cond_1

    .line 5938
    :cond_0
    :goto_0
    return-void

    .line 5759
    :cond_1
    check-cast p3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;

    .line 5764
    iget-wide v0, p3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->packedPosition:J

    invoke-static {v0, v1}, Landroid/widget/ExpandableListView;->getPackedPositionType(J)I

    move-result v0

    .line 5766
    if-ne v0, v4, :cond_0

    .line 5768
    iget-wide v0, p3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->packedPosition:J

    invoke-static {v0, v1}, Landroid/widget/ExpandableListView;->getPackedPositionGroup(J)I

    move-result v0

    .line 5769
    iget-wide v5, p3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->packedPosition:J

    invoke-static {v5, v6}, Landroid/widget/ExpandableListView;->getPackedPositionChild(J)I

    move-result v1

    .line 5771
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->an:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 5773
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 5776
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 5777
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    .line 5779
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/chaton/b/a;->a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;

    .line 5781
    const/16 v1, 0x6f

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0079

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v2, v1, v3, v5}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 5782
    const/16 v1, 0x73

    const v3, 0x7f0b03da

    invoke-virtual {v7, v2, v1, v10, v3}, Lcom/sec/chaton/b/a;->add(IIII)Landroid/view/MenuItem;

    .line 5783
    const/16 v1, 0x76

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b006e

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v1, v8, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 5784
    const/16 v1, 0x70

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b0068

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v1, v4, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 5936
    :cond_2
    :goto_2
    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aq:Lcom/sec/chaton/buddy/a/c;

    goto/16 :goto_0

    .line 5786
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b0094

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 5789
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/chaton/b/a;->a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;

    .line 5791
    const/16 v1, 0x65

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b019d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v1, v4, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 5797
    const/4 v5, 0x0

    .line 5798
    const/4 v6, 0x0

    move v3, v2

    .line 5801
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v3, v1, :cond_10

    .line 5802
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v1

    if-ne v1, v4, :cond_7

    .line 5803
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 5808
    :goto_4
    if-eqz v1, :cond_f

    .line 5810
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/buddy/a/c;

    .line 5811
    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v3, v4

    :goto_5
    move v5, v2

    .line 5817
    :goto_6
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v5, v1, :cond_e

    .line 5818
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/buddy/a/a;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v1

    if-ne v1, v9, :cond_8

    .line 5819
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 5826
    :goto_7
    if-eqz v1, :cond_d

    .line 5828
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/buddy/a/c;

    .line 5829
    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 5836
    :goto_8
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v5, "0999"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 5837
    if-eqz v4, :cond_6

    .line 5838
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 5839
    const/16 v1, 0x71

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b020d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v1, v8, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 5923
    :cond_6
    :goto_9
    const/16 v1, 0x6a

    const v3, 0x7f0b03da

    invoke-virtual {v7, v2, v1, v10, v3}, Lcom/sec/chaton/b/a;->add(IIII)Landroid/view/MenuItem;

    .line 5925
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "0999"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 5926
    const/16 v1, 0x75

    const v3, 0x7f0b006c

    invoke-virtual {v7, v2, v1, v9, v3}, Lcom/sec/chaton/b/a;->add(IIII)Landroid/view/MenuItem;

    .line 5931
    :goto_a
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "0999"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 5932
    const/16 v1, 0x74

    const/4 v3, 0x6

    const v4, 0x7f0b031d

    invoke-virtual {v7, v2, v1, v3, v4}, Lcom/sec/chaton/b/a;->add(IIII)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 5801
    :cond_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_3

    .line 5817
    :cond_8
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_6

    .line 5841
    :cond_9
    const/16 v1, 0x72

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b020e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v1, v8, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_9

    .line 5845
    :cond_a
    if-eqz v3, :cond_b

    .line 5846
    const/16 v1, 0x67

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b006a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v1, v8, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_9

    .line 5848
    :cond_b
    const/16 v1, 0x66

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0069

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v1, v8, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_9

    .line 5928
    :cond_c
    const/16 v1, 0x68

    const v3, 0x7f0b006b

    invoke-virtual {v7, v2, v1, v9, v3}, Lcom/sec/chaton/b/a;->add(IIII)Landroid/view/MenuItem;

    goto :goto_a

    :cond_d
    move v4, v2

    goto/16 :goto_8

    :cond_e
    move-object v1, v6

    goto/16 :goto_7

    :cond_f
    move v3, v2

    goto/16 :goto_5

    :cond_10
    move-object v1, v5

    goto/16 :goto_4

    :cond_11
    move v1, v2

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 4921
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    .line 4923
    :cond_0
    const v0, 0x7f0f0001

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 4927
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 4928
    const-string v0, "onCreateOptionsMenu()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 4929
    return-void

    .line 4924
    :cond_2
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x12

    if-eq v0, v1, :cond_1

    .line 4925
    const v0, 0x7f0f0021

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 879
    const-string v0, "onCreateView()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    const v0, 0x7f03001d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 882
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ai()V

    .line 884
    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1414
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->D:Lcom/sec/chaton/util/bt;

    .line 1415
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->a()V

    .line 1417
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->u:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1418
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ct:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1419
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cs:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1421
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aP:Lcom/sec/chaton/buddy/dc;

    if-eqz v0, :cond_0

    .line 1422
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aP:Lcom/sec/chaton/buddy/dc;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dc;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1423
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aP:Lcom/sec/chaton/buddy/dc;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dc;->dismiss()V

    .line 1428
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_1

    .line 1436
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    move-object v0, v1

    check-cast v0, Landroid/widget/ExpandableListAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/widget/FastScrollableExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 1437
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 1438
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnGroupCollapseListener(Landroid/widget/ExpandableListView$OnGroupCollapseListener;)V

    .line 1439
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 1440
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 1441
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1442
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    .line 1445
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    if-eqz v0, :cond_2

    .line 1446
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/ag;->a()V

    .line 1447
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/ag;->a(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 1448
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/ag;->a(Lcom/sec/chaton/buddy/ao;)V

    .line 1449
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/ag;->a(Lcom/sec/chaton/buddy/ap;)V

    .line 1450
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/ag;->a(Lcom/sec/chaton/buddy/aq;)V

    .line 1451
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ar:Lcom/sec/chaton/buddy/ag;

    .line 1454
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aW:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1455
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aW:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1456
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aW:Landroid/widget/ImageView;

    .line 1459
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->M:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    .line 1460
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->M:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1461
    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->M:Landroid/widget/LinearLayout;

    .line 1464
    :cond_4
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 1465
    return-void
.end method

.method public onDetach()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 10408
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 10410
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bA:Lcom/sec/chaton/buddy/cw;

    if-eqz v0, :cond_0

    .line 10411
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 10412
    if-eqz v0, :cond_0

    .line 10413
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bA:Lcom/sec/chaton/buddy/cw;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 10417
    :cond_0
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bA:Lcom/sec/chaton/buddy/cw;

    .line 10420
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bE:Lcom/sec/chaton/buddy/cx;

    if-eqz v0, :cond_1

    .line 10421
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 10422
    if-eqz v0, :cond_1

    .line 10423
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bE:Lcom/sec/chaton/buddy/cx;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 10426
    :cond_1
    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bE:Lcom/sec/chaton/buddy/cx;

    .line 10428
    return-void
.end method

.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 1

    .prologue
    .line 8565
    const/4 v0, 0x0

    return v0
.end method

.method public onGroupCollapse(I)V
    .locals 0

    .prologue
    .line 8560
    return-void
.end method

.method public onGroupExpand(I)V
    .locals 0

    .prologue
    .line 8556
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    const/16 v5, 0x8

    const/4 v8, 0x2

    const/4 v3, 0x0

    const/high16 v4, 0x7f0e0000

    const/4 v1, 0x1

    .line 5121
    .line 5123
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 5125
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->N()V

    .line 5127
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    move v1, v3

    .line 5543
    :cond_0
    :goto_0
    return v1

    .line 5131
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 5132
    new-array v2, v8, [Ljava/lang/String;

    const v0, 0x7f0b0134

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const v0, 0x7f0b03aa

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 5135
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    add-int/lit8 v0, v0, -0xb

    .line 5136
    if-ne v0, v8, :cond_1

    move v0, v1

    .line 5139
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    const v4, 0x7f0b0150

    invoke-virtual {p0, v4}, Lcom/sec/chaton/buddy/BuddyFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v3

    new-instance v4, Lcom/sec/chaton/buddy/bp;

    invoke-direct {v4, p0}, Lcom/sec/chaton/buddy/bp;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v3, v2, v0, v4}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b003a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/buddy/bo;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/bo;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ax:Lcom/sec/common/a/d;

    goto :goto_0

    .line 5180
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v1, :cond_3

    .line 5181
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v2, "00030006"

    const-string v3, "00000001"

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5183
    :cond_3
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    .line 5184
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->H()V

    .line 5185
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ae:I

    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    if-eq v0, v2, :cond_0

    .line 5186
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->A()V

    goto/16 :goto_0

    .line 5208
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->Q()V

    goto/16 :goto_0

    .line 5214
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    const/16 v2, 0xc7

    if-le v0, v2, :cond_4

    .line 5215
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0b0077

    new-array v4, v1, [Ljava/lang/Object;

    const/16 v5, 0xc7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-virtual {v0, v2, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 5218
    :cond_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v1, :cond_5

    .line 5219
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v2, "00030007"

    invoke-virtual {v0, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 5222
    :cond_5
    new-instance v0, Lcom/sec/chaton/buddy/dc;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2, p0}, Lcom/sec/chaton/buddy/dc;-><init>(Landroid/content/Context;Lcom/sec/chaton/buddy/BuddyFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aP:Lcom/sec/chaton/buddy/dc;

    .line 5225
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aP:Lcom/sec/chaton/buddy/dc;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/dc;->a(Z)V

    .line 5227
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aP:Lcom/sec/chaton/buddy/dc;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dc;->show()V

    .line 5230
    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->c(Z)V

    goto/16 :goto_0

    .line 5253
    :sswitch_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v1, :cond_6

    .line 5254
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v2, "00030008"

    invoke-virtual {v0, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 5257
    :cond_6
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->H()V

    .line 5259
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5260
    const-string v2, "BUDDY_SORT_STYLE"

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5261
    const-string v2, "ACTIVITY_PURPOSE"

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5263
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5267
    :sswitch_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v1, :cond_7

    .line 5268
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v2, "00030009"

    invoke-virtual {v0, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 5270
    :cond_7
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->H()V

    .line 5272
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5273
    const-string v2, "BUDDY_SORT_STYLE"

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5274
    const-string v2, "ACTIVITY_PURPOSE"

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5276
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5280
    :sswitch_5
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->H()V

    .line 5282
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5283
    const-string v2, "BUDDY_SORT_STYLE"

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5284
    const-string v2, "ACTIVITY_PURPOSE"

    const/16 v3, 0xc

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5285
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5305
    :sswitch_6
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/ActivitySettings;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5307
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5313
    :sswitch_7
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v1, :cond_8

    .line 5314
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v2, "00030015"

    invoke-virtual {v0, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 5317
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->S:Ljava/lang/String;

    .line 5318
    iget v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->U:I

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v4

    .line 5319
    const/4 v2, 0x0

    .line 5321
    new-instance v5, Lcom/sec/chaton/widget/ProfileImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/sec/chaton/widget/ProfileImageView;-><init>(Landroid/content/Context;)V

    .line 5323
    iget v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->U:I

    invoke-static {v6}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v6

    invoke-virtual {v5, v5, v6}, Lcom/sec/chaton/widget/ProfileImageView;->a(Landroid/view/View;Lcom/sec/chaton/e/r;)V

    .line 5325
    sget-object v6, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v4, v6, :cond_9

    .line 5326
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->T:Ljava/lang/String;

    .line 5328
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/buddy/BuddyFragment;->T:Ljava/lang/String;

    invoke-virtual {v6, v5, v7}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 5329
    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->T:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/chaton/widget/ProfileImageView;->setBuddyNo(Ljava/lang/String;)V

    .line 5333
    :cond_9
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6, v0, v4}, Lcom/sec/chaton/util/ac;->a(Landroid/content/Context;Ljava/lang/String;Lcom/sec/chaton/e/r;)Landroid/content/Intent;

    move-result-object v6

    .line 5335
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->W:Ljava/lang/String;

    .line 5338
    sget-object v7, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v4, v7, :cond_10

    .line 5339
    const-string v4, "%s(%d)"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v0, v7, v3

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->V:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 5343
    :goto_1
    :try_start_0
    invoke-virtual {v5}, Lcom/sec/chaton/widget/ProfileImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 5347
    :goto_2
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 5351
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v2}, Lcom/sec/chaton/util/ac;->a(F)I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v4}, Lcom/sec/chaton/util/ac;->a(F)I

    move-result v4

    invoke-static {v0, v2, v4, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 5353
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v6, v3, v0}, Lcom/sec/chaton/util/ac;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 5344
    :catch_0
    move-exception v0

    move-object v0, v2

    goto :goto_2

    .line 5359
    :sswitch_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    if-eqz v0, :cond_0

    .line 5360
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->i()V

    goto/16 :goto_0

    .line 5381
    :sswitch_9
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 5382
    const v2, 0x7f0b01a1

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 5383
    const v2, 0x7f0b0032

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v2

    const v3, 0x7f0b0042

    new-instance v4, Lcom/sec/chaton/buddy/br;

    invoke-direct {v4, p0}, Lcom/sec/chaton/buddy/br;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v2

    const v3, 0x7f0b0039

    new-instance v4, Lcom/sec/chaton/buddy/bq;

    invoke-direct {v4, p0}, Lcom/sec/chaton/buddy/bq;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 5402
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 5409
    :sswitch_a
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v1, :cond_a

    .line 5410
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v2, "00030010"

    invoke-virtual {v0, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 5415
    :cond_a
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/AddBuddyActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5416
    const-string v2, "ADD_BUDDY_TYPE"

    const/16 v3, 0x67

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5417
    const/16 v2, 0x64

    invoke-virtual {p0, v0, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 5421
    :sswitch_b
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/ActivityManageBuddy;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5422
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5426
    :sswitch_c
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 5427
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/registration/ActivityRegist;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5434
    :goto_3
    const-string v2, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    const-string v3, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5437
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 5429
    :cond_b
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/ActivityContactSync;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_3

    .line 5440
    :sswitch_d
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Z)V

    .line 5441
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f070079

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 5442
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    .line 5445
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v1, :cond_d

    .line 5446
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_e

    .line 5447
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 5452
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bH:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 5453
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bI:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 5457
    :cond_d
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0b0411

    invoke-static {v0, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 5449
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 5460
    :sswitch_e
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->X()V

    goto/16 :goto_0

    .line 5464
    :sswitch_f
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5465
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v1, :cond_f

    .line 5466
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v2, "00030019"

    invoke-virtual {v0, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 5469
    :cond_f
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0079

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b007a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0037

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/buddy/bs;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/bs;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b003a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    :cond_10
    move-object v3, v0

    goto/16 :goto_1

    .line 5127
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_5
        0xe -> :sswitch_9
        0x7f07055e -> :sswitch_0
        0x7f07055f -> :sswitch_2
        0x7f070560 -> :sswitch_b
        0x7f070561 -> :sswitch_a
        0x7f070562 -> :sswitch_c
        0x7f070563 -> :sswitch_d
        0x7f0705a6 -> :sswitch_e
        0x7f0705af -> :sswitch_f
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1367
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    .line 1368
    sput-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->l:Z

    .line 1371
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ca:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->b(Landroid/text/TextWatcher;)V

    .line 1379
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->H()V

    .line 1384
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPause() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1385
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 1386
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->N()V

    .line 1387
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 14

    .prologue
    const/16 v13, 0xc

    const/16 v12, 0xb

    const/4 v11, 0x7

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 4934
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 4937
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5117
    :cond_0
    :goto_0
    return-void

    .line 4941
    :cond_1
    const v0, 0x7f07055f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 4942
    const v1, 0x7f070563

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 4943
    const v2, 0x7f07055e

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 4944
    const v3, 0x7f070560

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 4945
    const v4, 0x7f070561

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 4946
    const v5, 0x7f070562

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 4951
    iget v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ce:I

    iget v7, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    sub-int/2addr v6, v7

    .line 4953
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BuddyFragment -> onPrepareOptionsMenu -> buddyCount : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ce:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4954
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BuddyFragment -> onPrepareOptionsMenu -> hideBuddyCount : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4955
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BuddyFragment -> onPrepareOptionsMenu -> normalBuddyCount : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4957
    iget-object v7, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v7, :cond_f

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v7

    if-eqz v7, :cond_f

    .line 4958
    if-eqz v0, :cond_3

    .line 4959
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    if-eq v1, v12, :cond_2

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    if-eq v1, v13, :cond_2

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    const/16 v6, 0xd

    if-ne v1, v6, :cond_e

    .line 4960
    :cond_2
    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4966
    :cond_3
    :goto_1
    if-eqz v2, :cond_4

    .line 4967
    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4970
    :cond_4
    if-eqz v3, :cond_5

    .line 4971
    invoke-interface {v3, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4974
    :cond_5
    if-eqz v4, :cond_6

    .line 4975
    invoke-interface {v4, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4978
    :cond_6
    if-eqz v5, :cond_7

    .line 4979
    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5070
    :cond_7
    :goto_2
    const v0, 0x7f0705a6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bu:Landroid/view/MenuItem;

    .line 5071
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bu:Landroid/view/MenuItem;

    if-eqz v0, :cond_8

    .line 5072
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bu:Landroid/view/MenuItem;

    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5074
    :cond_8
    const v0, 0x7f0705a5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bv:Landroid/view/MenuItem;

    .line 5075
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bv:Landroid/view/MenuItem;

    if-eqz v0, :cond_9

    .line 5076
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bv:Landroid/view/MenuItem;

    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5078
    :cond_9
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v10, :cond_0

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v11, :cond_0

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_a

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->w:I

    if-eqz v0, :cond_0

    :cond_a
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_b

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eq v0, v10, :cond_0

    :cond_b
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x12

    if-eq v0, v1, :cond_0

    .line 5082
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bu:Landroid/view/MenuItem;

    if-eqz v0, :cond_d

    .line 5083
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bu:Landroid/view/MenuItem;

    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5084
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    if-eqz v0, :cond_c

    .line 5085
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-lez v0, :cond_26

    .line 5086
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bu:Landroid/view/MenuItem;

    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 5091
    :cond_c
    :goto_3
    new-instance v0, Lcom/sec/chaton/buddy/bm;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/bm;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/cy;)V

    .line 5103
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bv:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 5104
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 5105
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bv:Landroid/view/MenuItem;

    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5106
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bv:Landroid/view/MenuItem;

    new-instance v1, Lcom/sec/chaton/buddy/bn;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/bn;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 4962
    :cond_e
    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 4983
    :cond_f
    iget v7, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v7, v10, :cond_1c

    .line 4984
    if-eqz v1, :cond_10

    .line 4985
    invoke-interface {v1, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4988
    :cond_10
    if-eqz v2, :cond_11

    .line 4989
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->s()I

    move-result v1

    if-ne v1, v10, :cond_17

    if-lez v6, :cond_17

    .line 4990
    invoke-interface {v2, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4996
    :cond_11
    :goto_4
    if-eqz v0, :cond_13

    .line 4997
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    if-eq v1, v12, :cond_12

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    if-eq v1, v13, :cond_12

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ad:I

    const/16 v2, 0xd

    if-ne v1, v2, :cond_18

    :cond_12
    if-lez v6, :cond_18

    .line 4998
    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5004
    :cond_13
    :goto_5
    if-eqz v3, :cond_19

    if-lez v6, :cond_19

    .line 5005
    invoke-interface {v3, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5012
    :cond_14
    :goto_6
    if-eqz v4, :cond_15

    .line 5013
    invoke-interface {v4, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5016
    :cond_15
    if-eqz v5, :cond_7

    .line 5018
    const-string v0, "for_wifi_only_device"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    invoke-static {}, Lcom/sec/chaton/util/am;->w()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 5019
    :cond_16
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 5020
    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 4992
    :cond_17
    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_4

    .line 5000
    :cond_18
    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_5

    .line 5007
    :cond_19
    if-eqz v3, :cond_14

    .line 5008
    invoke-interface {v3, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_6

    .line 5022
    :cond_1a
    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 5025
    :cond_1b
    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 5030
    :cond_1c
    iget v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v6, v11, :cond_25

    .line 5032
    if-eqz v4, :cond_1d

    .line 5033
    invoke-interface {v4, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5036
    :cond_1d
    if-eqz v2, :cond_1e

    .line 5037
    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5040
    :cond_1e
    if-eqz v0, :cond_1f

    .line 5041
    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5044
    :cond_1f
    if-eqz v3, :cond_20

    .line 5045
    invoke-interface {v3, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5048
    :cond_20
    if-eqz v1, :cond_21

    .line 5049
    invoke-interface {v1, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 5052
    :cond_21
    if-eqz v5, :cond_7

    .line 5054
    const-string v0, "for_wifi_only_device"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_22

    invoke-static {}, Lcom/sec/chaton/util/am;->w()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 5055
    :cond_22
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 5056
    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 5058
    :cond_23
    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 5061
    :cond_24
    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 5067
    :cond_25
    const v0, 0x7f07055d

    invoke-interface {p1, v0, v9}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto/16 :goto_2

    .line 5088
    :cond_26
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bu:Landroid/view/MenuItem;

    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_3
.end method

.method public onResume()V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1208
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 1217
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1220
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aX:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Push Name"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1221
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aY:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "status_message"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1222
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->af()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1223
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aY:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200ed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2, v8, v8, v8}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1224
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aY:Landroid/widget/TextView;

    const/high16 v2, 0x40e00000    # 7.0f

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1227
    :cond_0
    iput-boolean v6, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    .line 1228
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1229
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->az:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1230
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/cz;->d()V

    .line 1231
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/cz;->c()V

    .line 1232
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/cz;->b()V

    .line 1233
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ca:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 1234
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v2, Lcom/sec/chaton/buddy/cr;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/cr;-><init>(Lcom/sec/chaton/buddy/BuddyFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1248
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "###mIsTabSelected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bc:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1250
    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bc:Z

    if-ne v1, v6, :cond_3

    .line 1251
    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->az:Z

    if-eqz v1, :cond_b

    .line 1252
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/cz;->b()V

    .line 1258
    :goto_0
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v1, v6, :cond_1

    .line 1259
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/cz;->e()V

    .line 1263
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aI:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 1264
    if-ne v0, v6, :cond_d

    .line 1269
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type=\'200\' AND timestamp>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "recommend_timestamp"

    const-wide/16 v9, 0x0

    invoke-virtual {v4, v5, v9, v10}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1273
    if-eqz v1, :cond_13

    .line 1274
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 1278
    :goto_1
    if-eqz v1, :cond_2

    .line 1279
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1283
    :cond_2
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->b(I)V

    .line 1294
    :cond_3
    :goto_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v6

    :goto_3
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bm:Z

    .line 1295
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bn:Z

    .line 1298
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->i()I

    move-result v0

    if-nez v0, :cond_4

    .line 1299
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    .line 1303
    :cond_4
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ce:I

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->cg:I

    sub-int/2addr v0, v1

    if-gtz v0, :cond_11

    .line 1304
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bm:Z

    if-ne v0, v6, :cond_10

    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bn:Z

    if-ne v0, v6, :cond_10

    .line 1305
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->I:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 1306
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->I:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 1308
    :cond_5
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-eq v0, v6, :cond_6

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_f

    .line 1309
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->B:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1310
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->M:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1311
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->N:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1334
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1335
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->M()V

    .line 1338
    :cond_7
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->aa()V

    .line 1339
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aM:Ljava/util/HashMap;

    if-eqz v0, :cond_8

    .line 1340
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aM:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1345
    :cond_8
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v0, v6, :cond_a

    .line 1346
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 1347
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bh:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bj:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_12

    .line 1348
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1355
    :cond_a
    :goto_5
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->aW:Landroid/widget/ImageView;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->c(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 1356
    return-void

    .line 1254
    :cond_b
    iput-boolean v7, p0, Lcom/sec/chaton/buddy/BuddyFragment;->az:Z

    goto/16 :goto_0

    .line 1278
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_6
    if-eqz v1, :cond_c

    .line 1279
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1278
    :cond_c
    throw v0

    .line 1285
    :cond_d
    invoke-virtual {p0, v7}, Lcom/sec/chaton/buddy/BuddyFragment;->b(I)V

    goto/16 :goto_2

    :cond_e
    move v0, v7

    .line 1294
    goto/16 :goto_3

    .line 1313
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->B:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_4

    .line 1316
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->M:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1317
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->H:Landroid/view/ViewStub;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/view/ViewStub;)V

    goto :goto_4

    .line 1320
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->A:Landroid/widget/ScrollView;

    invoke-virtual {v0, v11}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_4

    .line 1350
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bJ:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 1278
    :catchall_1
    move-exception v0

    goto :goto_6

    :cond_13
    move v0, v7

    goto/16 :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 9815
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 9816
    sget v0, Lcom/sec/chaton/buddy/BuddyFragment;->c:I

    if-eqz v0, :cond_0

    .line 9817
    const-string v0, "INDEX"

    sget v1, Lcom/sec/chaton/buddy/BuddyFragment;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 9818
    const-string v0, "TOP"

    sget v1, Lcom/sec/chaton/buddy/BuddyFragment;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 9819
    const-string v0, "savedInstanceStateCheck"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 9823
    :goto_0
    return-void

    .line 9821
    :cond_0
    const-string v0, "savedInstanceStateCheck"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 1110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStart() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ay:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 1112
    sget-boolean v1, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    if-ne v1, v3, :cond_0

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v1, v3, :cond_0

    .line 1113
    invoke-virtual {p0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Z)V

    .line 1114
    sput-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 1115
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->B()V

    .line 1121
    :cond_0
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->af:I

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1123
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Profile Birth Chk"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1125
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStart() isShowBirthdayCategory : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1126
    if-eqz v1, :cond_4

    move v1, v0

    .line 1130
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1131
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    .line 1133
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    if-nez v0, :cond_2

    .line 1204
    :cond_1
    :goto_1
    return-void

    .line 1130
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1141
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1142
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_4
    move v1, v0

    .line 1189
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1190
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/a;

    .line 1192
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/a;->d()I

    move-result v0

    if-nez v0, :cond_5

    .line 1193
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1194
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1196
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ak:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->al:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 1189
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1391
    const-string v0, "onStop()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->bx:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyFragment;->ca:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->b(Landroid/text/TextWatcher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1397
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->O()V

    .line 1398
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->P()V

    .line 1403
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 1404
    return-void

    .line 1394
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public p()V
    .locals 2

    .prologue
    .line 10725
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->au:Z

    .line 10726
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->C:Lcom/sec/widget/FastScrollableExpandableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    .line 10727
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyFragment;->as:Lcom/sec/chaton/buddy/cz;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/cz;->a(I)V

    .line 10728
    return-void
.end method

.method public q()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 10790
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyFragment;->A()V

    .line 10791
    return-void
.end method
