.class public Lcom/sec/chaton/buddy/BuddyInfoActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "BuddyInfoActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoActivity;->c()Lcom/sec/chaton/buddy/BuddyInfoFragment;

    move-result-object v0

    return-object v0
.end method

.method protected c()Lcom/sec/chaton/buddy/BuddyInfoFragment;
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;-><init>()V

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 36
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 37
    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 38
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 40
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 46
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 47
    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 48
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 50
    :cond_0
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 22
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 28
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 24
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoActivity;->finish()V

    goto :goto_0

    .line 22
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
