.class Lcom/sec/chaton/buddy/fs;
.super Ljava/lang/Object;
.source "BuddyRecommendFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 339
    new-instance v0, Lcom/sec/chaton/buddy/gn;

    invoke-direct {v0, p2}, Lcom/sec/chaton/buddy/gn;-><init>(Landroid/view/View;)V

    .line 340
    iget-object v1, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    iget-object v2, v0, Lcom/sec/chaton/buddy/gn;->c:Landroid/widget/ImageView;

    const v3, 0x7f07000e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 341
    iget-object v1, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    iget-object v0, v0, Lcom/sec/chaton/buddy/gn;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 343
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 345
    const-string v1, "PROFILE_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 346
    const-string v1, "PROFILE_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    const-string v1, "PROFILE_BUDDY_SUGGESTION"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 348
    iget-object v1, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->startActivity(Landroid/content/Intent;)V

    .line 357
    :goto_0
    return-void

    .line 351
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 352
    const-string v1, "BUDDY_DIALOG_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    const-string v1, "BUDDY_DIALOG_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 354
    const-string v1, "BUDDY_DIALOG_BUDDY_FROM_SUGGESTION"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 355
    iget-object v1, p0, Lcom/sec/chaton/buddy/fs;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
