.class public final enum Lcom/sec/chaton/buddy/a/e;
.super Ljava/lang/Enum;
.source "SpecialBuddyItem.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/buddy/a/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/buddy/a/e;

.field public static final enum b:Lcom/sec/chaton/buddy/a/e;

.field public static final enum c:Lcom/sec/chaton/buddy/a/e;

.field public static final enum d:Lcom/sec/chaton/buddy/a/e;

.field private static final synthetic f:[Lcom/sec/chaton/buddy/a/e;


# instance fields
.field e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/sec/chaton/buddy/a/e;

    const-string v1, "NONE"

    const-string v2, "0"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/buddy/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/buddy/a/e;->a:Lcom/sec/chaton/buddy/a/e;

    .line 10
    new-instance v0, Lcom/sec/chaton/buddy/a/e;

    const-string v1, "CHAT"

    const-string v2, "1"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/buddy/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/buddy/a/e;->b:Lcom/sec/chaton/buddy/a/e;

    .line 11
    new-instance v0, Lcom/sec/chaton/buddy/a/e;

    const-string v1, "CONTENTS"

    const-string v2, "2"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/chaton/buddy/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/buddy/a/e;->c:Lcom/sec/chaton/buddy/a/e;

    .line 12
    new-instance v0, Lcom/sec/chaton/buddy/a/e;

    const-string v1, "CHATCONTENTS"

    const-string v2, "3"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/chaton/buddy/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/buddy/a/e;->d:Lcom/sec/chaton/buddy/a/e;

    .line 8
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/buddy/a/e;

    sget-object v1, Lcom/sec/chaton/buddy/a/e;->a:Lcom/sec/chaton/buddy/a/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/buddy/a/e;->b:Lcom/sec/chaton/buddy/a/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/buddy/a/e;->c:Lcom/sec/chaton/buddy/a/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/buddy/a/e;->d:Lcom/sec/chaton/buddy/a/e;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/buddy/a/e;->f:[Lcom/sec/chaton/buddy/a/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput-object p3, p0, Lcom/sec/chaton/buddy/a/e;->e:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/buddy/a/e;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/sec/chaton/buddy/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/e;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/buddy/a/e;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/chaton/buddy/a/e;->f:[Lcom/sec/chaton/buddy/a/e;

    invoke-virtual {v0}, [Lcom/sec/chaton/buddy/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/buddy/a/e;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/e;->e:Ljava/lang/String;

    return-object v0
.end method
