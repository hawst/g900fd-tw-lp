.class Lcom/sec/chaton/buddy/hr;
.super Ljava/lang/Object;
.source "SpecialBuddyFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 1229
    iput-object p1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 1343
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1333
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 15

    .prologue
    .line 1234
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1235
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1328
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1242
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1245
    :pswitch_1
    if-eqz p3, :cond_3

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1246
    iget-object v14, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    const-string v1, "buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "description"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "followcount"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "likecount"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "msgstatus"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "photoloaded"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "status"

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "url"

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "weburl"

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "islike"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "tel"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "usertype"

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {v1 .. v13}, Lcom/sec/chaton/buddy/a/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    invoke-static {v14, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/d;

    .line 1252
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v2, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1253
    const-string v1, "islike"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Y"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1254
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    .line 1265
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    if-nez v1, :cond_4

    .line 1266
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->q(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1276
    :goto_2
    if-eqz p3, :cond_0

    .line 1277
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1256
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    goto :goto_1

    .line 1261
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    goto :goto_1

    .line 1269
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->r(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1270
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->s(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1273
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v2, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;Z)V

    goto :goto_2

    .line 1283
    :pswitch_2
    if-eqz p3, :cond_6

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_6

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1284
    iget-object v13, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    const-string v1, "buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "description"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "followcount"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "likecount"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "msgstatus"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "photoloaded"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "status"

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "url"

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "weburl"

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "tel"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "usertype"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {v1 .. v12}, Lcom/sec/chaton/buddy/a/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    invoke-static {v13, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/d;

    .line 1294
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    if-nez v1, :cond_7

    .line 1295
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->q(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1306
    :goto_4
    if-eqz p3, :cond_0

    .line 1307
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1290
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v2, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->n(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/d;

    goto :goto_3

    .line 1298
    :cond_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->r(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1299
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->s(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1302
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v2, p0, Lcom/sec/chaton/buddy/hr;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;Z)V

    goto :goto_4

    .line 1242
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 1338
    return-void
.end method
