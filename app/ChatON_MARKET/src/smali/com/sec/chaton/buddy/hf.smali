.class Lcom/sec/chaton/buddy/hf;
.super Landroid/support/v4/widget/CursorAdapter;
.source "BuddyRecommendListFragment.java"


# instance fields
.field a:Landroid/widget/TextView;

.field final synthetic b:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 1

    .prologue
    .line 1006
    iput-object p1, p0, Lcom/sec/chaton/buddy/hf;->b:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    .line 1008
    invoke-direct {p0, p2, p3, p4}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 1000
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/hf;->a:Landroid/widget/TextView;

    .line 1009
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/buddy/hf;->g:Landroid/view/LayoutInflater;

    .line 1010
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 1049
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/hg;

    .line 1050
    iget v1, p0, Lcom/sec/chaton/buddy/hf;->c:I

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1051
    iget v2, p0, Lcom/sec/chaton/buddy/hf;->d:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1052
    iget v2, p0, Lcom/sec/chaton/buddy/hf;->e:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1053
    iget v4, p0, Lcom/sec/chaton/buddy/hf;->f:I

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1055
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "recommend_timestamp"

    const-wide/16 v7, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 1056
    iget-object v6, v0, Lcom/sec/chaton/buddy/hg;->b:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1057
    iget-object v1, v0, Lcom/sec/chaton/buddy/hg;->a:Landroid/widget/ImageView;

    const v6, 0x7f02028d

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1059
    iget-object v1, v0, Lcom/sec/chaton/buddy/hg;->c:Landroid/widget/ImageView;

    const v6, 0x7f07000e

    invoke-virtual {v1, v6, v3}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 1060
    iget-object v1, v0, Lcom/sec/chaton/buddy/hg;->c:Landroid/widget/ImageView;

    const v6, 0x7f07000f

    invoke-virtual {v1, v6, v2}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 1061
    iget-object v1, v0, Lcom/sec/chaton/buddy/hg;->c:Landroid/widget/ImageView;

    const v6, 0x7f0202cf

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1062
    iget-object v1, v0, Lcom/sec/chaton/buddy/hg;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1064
    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hf;->b:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->k(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1065
    iget-object v1, p0, Lcom/sec/chaton/buddy/hf;->b:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->k(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1068
    :cond_0
    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1069
    iget-object v1, v0, Lcom/sec/chaton/buddy/hg;->c:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1071
    iget-object v1, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/chaton/buddy/hf;->b:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->l(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1103
    :goto_0
    invoke-static {p2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/buddy/hg;->a:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v3, v2}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;Z)V

    .line 1104
    return-void

    .line 1074
    :cond_1
    iget-object v1, v0, Lcom/sec/chaton/buddy/hg;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1078
    iget-object v6, p0, Lcom/sec/chaton/buddy/hf;->b:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/hf;->b:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->m(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)J

    move-result-wide v1

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v1, v1, v7

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/buddy/hf;->b:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->m(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)J

    move-result-wide v1

    :goto_1
    invoke-static {v6, v1, v2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;J)J

    .line 1079
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v1, v4

    if-lez v1, :cond_3

    .line 1080
    iget-object v1, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 1081
    iget-object v2, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 1082
    iget-object v4, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 1083
    iget-object v5, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    .line 1085
    iget-object v6, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    const v7, 0x7f0202de

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1086
    iget-object v6, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v6, v5, v1, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 1078
    :cond_2
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_1

    .line 1089
    :cond_3
    iget-object v1, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 1090
    iget-object v2, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 1091
    iget-object v4, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 1092
    iget-object v5, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    .line 1094
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x10

    if-lt v6, v7, :cond_4

    .line 1095
    iget-object v6, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1099
    :goto_2
    iget-object v6, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v6, v5, v1, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_0

    .line 1097
    :cond_4
    iget-object v6, v0, Lcom/sec/chaton/buddy/hg;->d:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1109
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030120

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1110
    new-instance v1, Lcom/sec/chaton/buddy/hg;

    invoke-direct {v1, v0}, Lcom/sec/chaton/buddy/hg;-><init>(Landroid/view/View;)V

    .line 1111
    iget-object v2, v1, Lcom/sec/chaton/buddy/hg;->c:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/hf;->b:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->n(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1119
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1121
    return-object v0
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 1126
    if-eqz p1, :cond_0

    .line 1127
    const-string v0, "buddy_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/hf;->c:I

    .line 1128
    const-string v0, "buddy_no"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/hf;->d:I

    .line 1129
    const-string v0, "type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/hf;->e:I

    .line 1130
    const-string v0, "timestamp"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/hf;->f:I

    .line 1132
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
