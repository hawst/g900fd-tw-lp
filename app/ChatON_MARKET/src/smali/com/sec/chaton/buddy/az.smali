.class Lcom/sec/chaton/buddy/az;
.super Ljava/lang/Object;
.source "BuddyEditNickNameFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)V
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lcom/sec/chaton/buddy/az;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 494
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 466
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 461
    return-void
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 470
    if-ne p1, v0, :cond_0

    .line 471
    if-ne p3, v0, :cond_1

    .line 472
    iget-object v0, p0, Lcom/sec/chaton/buddy/az;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    const v1, 0x7f0b007c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 476
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 477
    iget-object v1, p0, Lcom/sec/chaton/buddy/az;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 479
    iget-object v2, p0, Lcom/sec/chaton/buddy/az;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 480
    const-string v3, "PROFILE_GROUP_RENAME"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 483
    iget-object v0, p0, Lcom/sec/chaton/buddy/az;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 484
    iget-object v0, p0, Lcom/sec/chaton/buddy/az;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 489
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/az;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
