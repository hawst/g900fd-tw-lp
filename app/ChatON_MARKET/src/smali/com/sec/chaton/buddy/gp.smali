.class Lcom/sec/chaton/buddy/gp;
.super Landroid/os/Handler;
.source "BuddyRecommendListFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V
    .locals 0

    .prologue
    .line 655
    iput-object p1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10

    .prologue
    const/4 v6, -0x2

    const/4 v5, -0x3

    const v4, 0x7f0b00be

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 658
    const-string v0, ""

    .line 659
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 661
    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 900
    :cond_0
    :goto_0
    return-void

    .line 664
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->g(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 665
    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->g(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 668
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x12f

    if-ne v1, v3, :cond_b

    .line 669
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 670
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_4

    .line 672
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetBuddyList;

    .line 673
    if-eqz v0, :cond_3

    .line 674
    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetBuddyList;->buddy:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Buddy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 676
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b027d

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 677
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Lcom/sec/chaton/buddy/hf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/hf;->notifyDataSetChanged()V

    .line 679
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->h(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->i(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V

    goto :goto_0

    .line 684
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_5

    .line 685
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 686
    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b016c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/gq;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/gq;-><init>(Lcom/sec/chaton/buddy/gp;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 712
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADD BUDDY RESULT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e83

    if-ne v1, v2, :cond_6

    .line 715
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 735
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b016c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/gr;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/gr;-><init>(Lcom/sec/chaton/buddy/gp;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 717
    :cond_6
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e84

    if-ne v1, v2, :cond_7

    .line 718
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 720
    :cond_7
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e85

    if-ne v1, v2, :cond_8

    .line 722
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 726
    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0xc355

    if-ne v0, v1, :cond_9

    .line 727
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0289

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 731
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 759
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/gs;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/gs;-><init>(Lcom/sec/chaton/buddy/gp;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 774
    :cond_b
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x137

    if-ne v1, v3, :cond_1c

    .line 775
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 776
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_13

    .line 777
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetBuddyList;

    .line 780
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 781
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 783
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetBuddyList;->buddy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move v3, v2

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Buddy;

    .line 784
    iget-object v7, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->result:Ljava/lang/String;

    if-eqz v7, :cond_d

    .line 785
    iget-object v7, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->result:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 787
    if-lez v7, :cond_c

    .line 788
    add-int/lit8 v3, v3, 0x1

    .line 790
    :cond_c
    add-int/lit8 v1, v1, 0x1

    .line 793
    iget-object v7, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    const-string v8, "+"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 794
    iget-object v7, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v7, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 799
    :goto_3
    invoke-static {v0}, Lcom/sec/chaton/d/a/ck;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_d
    move v0, v1

    move v1, v3

    move v3, v1

    move v1, v0

    .line 800
    goto :goto_2

    .line 796
    :cond_e
    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    goto :goto_3

    .line 804
    :cond_f
    :try_start_0
    const-string v0, "com.sec.chaton.provider"

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 805
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->h(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 806
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->i(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 814
    :cond_10
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 815
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b0295

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    sub-int v5, v1, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v9

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 818
    if-nez v3, :cond_12

    .line 819
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 820
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b01fb

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 822
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 808
    :catch_0
    move-exception v0

    .line 809
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    .line 810
    :catch_1
    move-exception v0

    .line 811
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_4

    .line 824
    :cond_12
    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b014f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/gt;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/gt;-><init>(Lcom/sec/chaton/buddy/gp;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 832
    :cond_13
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 834
    if-eq v5, v1, :cond_14

    if-ne v6, v1, :cond_15

    .line 835
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 858
    :goto_5
    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 837
    :cond_15
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v3, 0x3e83

    if-ne v1, v3, :cond_16

    .line 838
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 840
    :cond_16
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v3, 0x3e84

    if-ne v1, v3, :cond_17

    .line 841
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 843
    :cond_17
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v3, 0x3e85

    if-ne v1, v3, :cond_18

    .line 845
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 849
    :cond_18
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0xc355

    if-ne v0, v1, :cond_19

    .line 850
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0289

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 854
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 861
    :cond_1a
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 863
    if-eq v5, v0, :cond_1b

    if-ne v6, v0, :cond_0

    .line 864
    :cond_1b
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 868
    :cond_1c
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_1d

    .line 869
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 871
    :cond_1d
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 872
    const-string v3, ""

    .line 873
    if-eq v5, v1, :cond_1e

    if-ne v6, v1, :cond_1f

    .line 874
    :cond_1e
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 897
    :goto_6
    iget-object v1, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 876
    :cond_1f
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v3, 0x3e83

    if-ne v1, v3, :cond_20

    .line 877
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 879
    :cond_20
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v3, 0x3e84

    if-ne v1, v3, :cond_21

    .line 880
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 882
    :cond_21
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v3, 0x3e85

    if-ne v1, v3, :cond_22

    .line 884
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 888
    :cond_22
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0xc355

    if-ne v0, v1, :cond_23

    .line 889
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0289

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 893
    :cond_23
    iget-object v0, p0, Lcom/sec/chaton/buddy/gp;->a:Lcom/sec/chaton/buddy/BuddyRecommendListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6
.end method
