.class Lcom/sec/chaton/buddy/dt;
.super Ljava/lang/Object;
.source "BuddyGroupProfileFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/ds;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/ds;)V
    .locals 0

    .prologue
    .line 682
    iput-object p1, p0, Lcom/sec/chaton/buddy/dt;->a:Lcom/sec/chaton/buddy/ds;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 685
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 686
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_2

    .line 687
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dt;->a:Lcom/sec/chaton/buddy/ds;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 697
    :cond_1
    :goto_0
    return-void

    .line 690
    :cond_2
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dt;->a:Lcom/sec/chaton/buddy/ds;

    iget-object v1, v1, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 691
    const-string v1, "group"

    iget-object v3, p0, Lcom/sec/chaton/buddy/dt;->a:Lcom/sec/chaton/buddy/ds;

    iget-object v3, v3, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/chaton/buddy/dt;->a:Lcom/sec/chaton/buddy/ds;

    iget-object v5, v5, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->j()[Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x147

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)I

    .line 692
    iget-object v0, p0, Lcom/sec/chaton/buddy/dt;->a:Lcom/sec/chaton/buddy/ds;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;)V

    .line 693
    iget-object v0, p0, Lcom/sec/chaton/buddy/dt;->a:Lcom/sec/chaton/buddy/ds;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/dt;->a:Lcom/sec/chaton/buddy/ds;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 694
    iget-object v0, p0, Lcom/sec/chaton/buddy/dt;->a:Lcom/sec/chaton/buddy/ds;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method
