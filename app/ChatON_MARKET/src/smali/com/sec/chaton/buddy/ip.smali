.class public Lcom/sec/chaton/buddy/ip;
.super Landroid/widget/ArrayAdapter;
.source "SpecialBuddyRecommendListFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/buddy/io;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/io;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field final synthetic d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

.field private e:Landroid/content/Context;

.field private f:Landroid/view/LayoutInflater;

.field private g:Landroid/view/ViewGroup;

.field private h:Landroid/view/View$OnClickListener;

.field private i:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/io;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 401
    iput-object p1, p0, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    .line 402
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 348
    new-instance v0, Lcom/sec/chaton/buddy/iq;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/iq;-><init>(Lcom/sec/chaton/buddy/ip;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ip;->h:Landroid/view/View$OnClickListener;

    .line 374
    new-instance v0, Lcom/sec/chaton/buddy/ir;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ir;-><init>(Lcom/sec/chaton/buddy/ip;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ip;->i:Landroid/view/View$OnClickListener;

    .line 404
    iput-object p2, p0, Lcom/sec/chaton/buddy/ip;->e:Landroid/content/Context;

    .line 405
    iput-object p4, p0, Lcom/sec/chaton/buddy/ip;->a:Ljava/util/ArrayList;

    .line 407
    iget-object v0, p0, Lcom/sec/chaton/buddy/ip;->e:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ip;->f:Landroid/view/LayoutInflater;

    .line 409
    iget-object v0, p0, Lcom/sec/chaton/buddy/ip;->f:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ip;->g:Landroid/view/ViewGroup;

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/buddy/ip;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    invoke-static {p1, v0}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 412
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 418
    if-nez p2, :cond_0

    .line 419
    iget-object v0, p0, Lcom/sec/chaton/buddy/ip;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f030124

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 420
    new-instance v1, Lcom/sec/chaton/buddy/is;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/is;-><init>(Lcom/sec/chaton/buddy/ip;)V

    .line 421
    const v0, 0x7f07014b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/buddy/is;->a:Landroid/widget/ImageView;

    .line 422
    const v0, 0x7f07014c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/chaton/buddy/is;->b:Landroid/widget/TextView;

    .line 423
    const v0, 0x7f0702d7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v1, Lcom/sec/chaton/buddy/is;->c:Landroid/widget/Button;

    .line 424
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 429
    :goto_0
    iget-object v0, v1, Lcom/sec/chaton/buddy/is;->c:Landroid/widget/Button;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 430
    iget-object v0, v1, Lcom/sec/chaton/buddy/is;->c:Landroid/widget/Button;

    const v2, 0x7f02029a

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 431
    iget-object v0, v1, Lcom/sec/chaton/buddy/is;->c:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/chaton/buddy/ip;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 433
    iget-object v0, v1, Lcom/sec/chaton/buddy/is;->b:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 434
    iget-object v0, v1, Lcom/sec/chaton/buddy/is;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/ip;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 435
    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/ip;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/io;

    .line 436
    iget-object v2, v1, Lcom/sec/chaton/buddy/is;->b:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/sec/chaton/buddy/io;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 438
    iget-object v2, v1, Lcom/sec/chaton/buddy/is;->a:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 439
    iget-object v2, v1, Lcom/sec/chaton/buddy/is;->a:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ip;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 440
    iget-object v2, p0, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)I

    move-result v2

    if-ne v2, v6, :cond_1

    .line 441
    iget-object v1, v1, Lcom/sec/chaton/buddy/is;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 442
    iget-object v1, p0, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 444
    iget-object v1, p0, Lcom/sec/chaton/buddy/ip;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v0, v0, Lcom/sec/chaton/buddy/io;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 454
    :goto_1
    return-object p2

    .line 426
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/is;

    move-object v1, v0

    goto :goto_0

    .line 445
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)I

    move-result v2

    if-le v2, v6, :cond_2

    .line 446
    iget-object v2, v1, Lcom/sec/chaton/buddy/is;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 447
    iget-object v2, p0, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 448
    iget-object v2, p0, Lcom/sec/chaton/buddy/ip;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/chaton/buddy/is;->a:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/sec/chaton/buddy/io;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_1

    .line 450
    :cond_2
    iget-object v0, v1, Lcom/sec/chaton/buddy/is;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 451
    iget-object v0, p0, Lcom/sec/chaton/buddy/ip;->d:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
