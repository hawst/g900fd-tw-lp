.class public Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "BuddyGroupMemberEditActivity.java"

# interfaces
.implements Lcom/sec/chaton/buddy/db;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setTitleView() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const/4 v0, 0x0

    .line 36
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ACTIVITY_PURPOSE"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/16 v2, 0x13

    if-ne v1, v2, :cond_0

    .line 38
    const v0, 0x7f0b0416

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 40
    :cond_0
    if-eqz v0, :cond_2

    .line 41
    if-ltz p1, :cond_1

    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 44
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 46
    :cond_2
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;-><init>()V

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;->c(I)V

    .line 24
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 67
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-static {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;->a(Landroid/app/Activity;)V

    .line 71
    :cond_0
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 77
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-static {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;->a(Landroid/app/Activity;)V

    .line 80
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 86
    invoke-static {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;->a(Landroid/app/Activity;)V

    .line 87
    return-void
.end method
