.class Lcom/sec/chaton/buddy/u;
.super Ljava/lang/Object;
.source "AddBuddyFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/AddBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 0

    .prologue
    .line 1667
    iput-object p1, p0, Lcom/sec/chaton/buddy/u;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1671
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 1672
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 1674
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1678
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    const/16 v3, 0x14

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1685
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 1687
    iget-object v0, p0, Lcom/sec/chaton/buddy/u;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->l(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1692
    :goto_0
    new-array v0, v1, [Landroid/text/InputFilter;

    .line 1693
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v1, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v2

    .line 1694
    iget-object v1, p0, Lcom/sec/chaton/buddy/u;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->c(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/widget/AdaptableEditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/AdaptableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1697
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lt v0, v3, :cond_0

    .line 1698
    iget-object v0, p0, Lcom/sec/chaton/buddy/u;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0031

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1703
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/u;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->m(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/u;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->m(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1705
    iget-object v0, p0, Lcom/sec/chaton/buddy/u;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->m(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1708
    :cond_1
    return-void

    .line 1689
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/u;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->l(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method
