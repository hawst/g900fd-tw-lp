.class public Lcom/sec/chaton/buddy/iv;
.super Landroid/os/AsyncTask;
.source "TellFriendsFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/sec/chaton/buddy/it;",
        ">;>;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field b:[Ljava/lang/String;

.field final synthetic c:Lcom/sec/chaton/buddy/TellFriendsFragment;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/buddy/TellFriendsFragment;)V
    .locals 3

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/chaton/buddy/iv;->c:Lcom/sec/chaton/buddy/TellFriendsFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 180
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.android.mms"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.facebook.katana"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.google.android.apps.plus"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "com.twitter.android"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.sina.weibo"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.renren.xiaonei.android"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/chaton/buddy/iv;->b:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Intent;Landroid/content/pm/PackageManager;)Landroid/content/pm/ResolveInfo;
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 261
    return-object v0
.end method

.method protected varargs a([Ljava/lang/Void;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/buddy/it;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/buddy/iv;->c:Lcom/sec/chaton/buddy/TellFriendsFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/TellFriendsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 202
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "text/plain"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 204
    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/buddy/iv;->a(Ljava/util/List;Landroid/content/pm/PackageManager;)V

    .line 206
    iget-object v3, p0, Lcom/sec/chaton/buddy/iv;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 207
    invoke-static {v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 208
    const/high16 v4, 0x10000

    invoke-virtual {v2, v0, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 209
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 210
    new-instance v5, Lcom/sec/chaton/buddy/it;

    invoke-virtual {v0, v2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/sec/chaton/buddy/it;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V

    .line 211
    iget-object v6, p0, Lcom/sec/chaton/buddy/iv;->c:Lcom/sec/chaton/buddy/TellFriendsFragment;

    invoke-virtual {v6}, Lcom/sec/chaton/buddy/TellFriendsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/buddy/it;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 213
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v3, v0}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    .line 214
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 220
    :cond_1
    return-object v1
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/chaton/buddy/iv;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/buddy/iv;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    :cond_0
    return-void
.end method

.method protected a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/buddy/it;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/iv;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/iv;->c:Lcom/sec/chaton/buddy/TellFriendsFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/TellFriendsFragment;->b()V

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/buddy/iv;->c:Lcom/sec/chaton/buddy/TellFriendsFragment;

    iget-object v0, v0, Lcom/sec/chaton/buddy/TellFriendsFragment;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/buddy/iv;->c:Lcom/sec/chaton/buddy/TellFriendsFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/TellFriendsFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/iu;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/iu;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method protected a(Ljava/util/List;Landroid/content/pm/PackageManager;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/buddy/it;",
            ">;",
            "Landroid/content/pm/PackageManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 234
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 236
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/buddy/iv;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 237
    iget-object v2, p0, Lcom/sec/chaton/buddy/iv;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    invoke-virtual {p0, v1, p2}, Lcom/sec/chaton/buddy/iv;->a(Landroid/content/Intent;Landroid/content/pm/PackageManager;)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 239
    if-eqz v2, :cond_0

    .line 240
    new-instance v3, Lcom/sec/chaton/buddy/it;

    invoke-virtual {v2, p2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, p2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/chaton/buddy/it;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V

    .line 241
    iget-object v4, p0, Lcom/sec/chaton/buddy/iv;->c:Lcom/sec/chaton/buddy/TellFriendsFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/TellFriendsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/buddy/it;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 243
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/iv;->a(Ljava/lang/String;)V

    .line 236
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 249
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/iv;->a(Ljava/lang/String;)V

    .line 250
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 177
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/iv;->a([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 177
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/iv;->a(Ljava/util/List;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/chaton/buddy/iv;->c:Lcom/sec/chaton/buddy/TellFriendsFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/TellFriendsFragment;->a()V

    .line 192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/iv;->a:Ljava/util/List;

    .line 193
    return-void
.end method
