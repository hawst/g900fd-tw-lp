.class Lcom/sec/chaton/buddy/ij;
.super Landroid/database/ContentObserver;
.source "SpecialBuddyRecommendListFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 119
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v3, "type=\'100\'"

    const-string v5, "buddy_name ASC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;I)I

    .line 122
    iget-object v1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Lcom/sec/chaton/buddy/ip;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Lcom/sec/chaton/buddy/ip;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/ip;->notifyDataSetChanged()V

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)I

    move-result v1

    if-nez v1, :cond_3

    .line 126
    iget-object v1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 127
    iget-object v1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ListView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 128
    iget-object v1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->e(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1, v6}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;I)I

    .line 131
    iget-object v1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->f(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 143
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 144
    return-void

    .line 133
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->f(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 134
    :goto_0
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 135
    new-instance v1, Lcom/sec/chaton/buddy/io;

    const-string v2, "buddy_no"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "buddy_name"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/buddy/io;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 137
    iget-object v2, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    iget-object v2, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/chaton/buddy/io;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 140
    :cond_4
    iget-object v2, p0, Lcom/sec/chaton/buddy/ij;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->f(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
