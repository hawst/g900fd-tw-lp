.class public Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "BuddyGroupProfileActivity.java"

# interfaces
.implements Lcom/sec/chaton/buddy/db;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 48
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;-><init>()V

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public b(I)V
    .locals 4

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mGroup member Count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BuddyGroupProfileActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/buddy/dq;

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/dq;

    .line 37
    invoke-interface {v0, p1}, Lcom/sec/chaton/buddy/dq;->c(I)V

    .line 40
    :cond_0
    const v0, 0x7f0b03cc

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 41
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 64
    invoke-static {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;->a(Landroid/app/Activity;)V

    .line 65
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 57
    invoke-static {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;->a(Landroid/app/Activity;)V

    .line 58
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 22
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 28
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 24
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;->finish()V

    goto :goto_0

    .line 22
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
