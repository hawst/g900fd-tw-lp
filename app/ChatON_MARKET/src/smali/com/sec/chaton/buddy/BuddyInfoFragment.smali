.class public Lcom/sec/chaton/buddy/BuddyInfoFragment;
.super Landroid/support/v4/app/Fragment;
.source "BuddyInfoFragment.java"


# static fields
.field private static d:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/ImageButton;

.field private B:Landroid/widget/ImageButton;

.field private C:Landroid/view/View;

.field private D:Landroid/view/View;

.field private E:Landroid/view/View;

.field private F:Landroid/view/View;

.field private G:Landroid/widget/ImageButton;

.field private H:Landroid/widget/ImageButton;

.field private I:Landroid/widget/ImageButton;

.field private J:Z

.field private K:Landroid/app/Activity;

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:Lcom/coolots/sso/a/a;

.field private Q:Landroid/view/View;

.field private R:Lcom/sec/chaton/e/a/u;

.field private S:I

.field a:Z

.field b:Landroid/view/View$OnClickListener;

.field c:Lcom/sec/chaton/e/a/v;

.field private e:Lcom/sec/chaton/buddy/a/c;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Landroid/widget/Toast;

.field private n:Landroid/widget/LinearLayout;

.field private o:Landroid/widget/LinearLayout;

.field private p:Landroid/widget/LinearLayout;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/LinearLayout;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/LinearLayout;

.field private y:Landroid/widget/ImageView;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const-class v0, Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 77
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 106
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->k:Z

    .line 107
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->l:Z

    .line 140
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->J:Z

    .line 144
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->M:Z

    .line 145
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->O:Z

    .line 149
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a:Z

    .line 155
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->S:I

    .line 1071
    new-instance v0, Lcom/sec/chaton/buddy/ei;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ei;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b:Landroid/view/View$OnClickListener;

    .line 1311
    new-instance v0, Lcom/sec/chaton/buddy/ej;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ej;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c:Lcom/sec/chaton/e/a/v;

    .line 77
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyInfoFragment;I)I
    .locals 0

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->j:I

    return p1
.end method

.method private a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1700
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://calendar/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1704
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 1709
    :goto_0
    if-nez v6, :cond_0

    .line 1711
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://com.android.calendar/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1713
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 1718
    :goto_1
    return-object v0

    .line 1714
    :catch_0
    move-exception v0

    move-object v0, v6

    goto :goto_1

    .line 1705
    :catch_1
    move-exception v0

    goto :goto_0

    :cond_0
    move-object v0, v6

    goto :goto_1
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 9

    .prologue
    .line 1722
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1725
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1737
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1740
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1742
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1743
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1745
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    rsub-int v1, v1, 0x7f4

    iput v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->S:I

    .line 1746
    const/4 v1, 0x1

    iget v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->S:I

    invoke-virtual {v2, v1, v3}, Ljava/util/Calendar;->add(II)V

    .line 1748
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1750
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1756
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-ge v4, v5, :cond_1

    .line 1757
    const-string v4, "calendar_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1758
    const-string v4, "title"

    const v5, 0x7f0b0157

    invoke-virtual {p0, v5}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    const-string v4, "dtstart"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v7

    int-to-long v7, v7

    add-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1761
    const-string v4, "eventTimezone"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1762
    const-string v4, "duration"

    const-string v5, "P1D"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1763
    const-string v4, "allDay"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1764
    const-string v4, "visibility"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1765
    const-string v4, "transparency"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1766
    const-string v4, "hasAlarm"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1767
    const-string v4, "hasExtendedProperties"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1768
    const-string v4, "hasAttendeeData"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1770
    const-string v4, "02-29"

    invoke-virtual {p3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1771
    const-string v4, "rrule"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FREQ=YEARLY;INTERVAL=4;UNTIL="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ";WKST=SU;BYMONTHDAY="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ";BYMONTH="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1776
    :goto_0
    const-string v0, "lastDate"

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1778
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "events"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1779
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 1804
    :goto_1
    return-object v0

    .line 1773
    :cond_0
    const-string v4, "rrule"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FREQ=YEARLY;UNTIL="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ";WKST=SU;BYMONTHDAY="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ";BYMONTH="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1782
    :cond_1
    const-string v4, "calendar_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1783
    const-string v4, "title"

    const v5, 0x7f0b0157

    invoke-virtual {p0, v5}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1784
    const-string v4, "dtstart"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v7

    int-to-long v7, v7

    add-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1785
    const-string v4, "eventTimezone"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1786
    const-string v4, "duration"

    const-string v5, "P1D"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1787
    const-string v4, "allDay"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1788
    const-string v4, "hasAlarm"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1789
    const-string v4, "hasExtendedProperties"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1790
    const-string v4, "hasAttendeeData"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1792
    const-string v4, "02-29"

    invoke-virtual {p3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1793
    const-string v4, "rrule"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FREQ=YEARLY;INTERVAL=4;UNTIL="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ";WKST=SU;BYMONTHDAY="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ";BYMONTH="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1798
    :goto_2
    const-string v0, "lastDate"

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1800
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_1

    .line 1795
    :cond_2
    const-string v4, "rrule"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FREQ=YEARLY;UNTIL="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ";WKST=SU;BYMONTHDAY="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ";BYMONTH="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyInfoFragment;Lcom/sec/chaton/buddy/a/c;)Lcom/sec/chaton/buddy/a/c;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;JJ)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;JJ)Ljava/lang/String;
    .locals 10

    .prologue
    .line 1810
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "s birthday"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1811
    const/4 v8, 0x0

    .line 1812
    const-string v6, "UnSuccess"

    .line 1813
    const/4 v7, 0x0

    .line 1815
    const-string v0, "content://com.android.calendar/events"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1818
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "calendar_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "description"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "dtstart"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "dtend"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "eventLocation"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "deleted"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1819
    if-eqz v1, :cond_5

    .line 1820
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1822
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 1823
    const/4 v0, 0x0

    move v2, v0

    move v0, v8

    :goto_0
    if-ge v2, v3, :cond_5

    .line 1824
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p2, p3}, Ljava/util/Date;-><init>(J)V

    .line 1825
    new-instance v5, Ljava/util/Date;

    const/4 v7, 0x3

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-direct {v5, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 1826
    const/4 v7, 0x6

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1827
    invoke-virtual {v4, v5}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v4

    if-nez v4, :cond_0

    if-nez v7, :cond_0

    .line 1828
    const/4 v0, 0x1

    .line 1830
    :cond_0
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    if-ne v0, v4, :cond_2

    .line 1831
    const-string v6, "Success"
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v6

    .line 1840
    :goto_1
    if-eqz v1, :cond_1

    .line 1841
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1845
    :cond_1
    :goto_2
    return-object v0

    .line 1834
    :cond_2
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1823
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1837
    :catch_0
    move-exception v0

    move-object v0, v7

    .line 1840
    :goto_3
    if-eqz v0, :cond_3

    .line 1841
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v6

    .line 1843
    goto :goto_2

    .line 1840
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v7, :cond_4

    .line 1841
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1840
    :cond_4
    throw v0

    :catchall_1
    move-exception v0

    move-object v7, v1

    goto :goto_4

    .line 1837
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_3

    :cond_5
    move-object v0, v6

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1550
    .line 1553
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b009f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0222

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/ec;

    invoke-direct {v2, p0, p2, p1}, Lcom/sec/chaton/buddy/ec;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 1584
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Ljava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 893
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e()Z

    move-result v3

    .line 894
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f()Z

    move-result v4

    .line 895
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->h()Z

    move-result v5

    .line 899
    if-eqz p1, :cond_6

    .line 900
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processMultiDeviceCase : buddyPhoneNumberAvailable = "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v6, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i()Z

    move-result p1

    .line 903
    if-nez p1, :cond_6

    .line 904
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    const-string v6, "phone"

    invoke-virtual {v0, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 905
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.SENDTO"

    const-string v8, "smsto:+000"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0, v6}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 911
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d()Z

    move-result v6

    .line 917
    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    .line 918
    if-eqz v5, :cond_1

    .line 919
    if-eqz v6, :cond_0

    .line 920
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(ZZ)V

    .line 921
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->l:Z

    .line 949
    :goto_1
    return-void

    .line 924
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(ZZ)V

    goto :goto_1

    .line 928
    :cond_1
    if-eqz v6, :cond_2

    .line 929
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(ZZ)V

    .line 930
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->l:Z

    goto :goto_1

    .line 933
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(ZZ)V

    goto :goto_1

    .line 937
    :cond_3
    if-eqz v3, :cond_5

    .line 938
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->g()Z

    move-result v1

    if-nez v1, :cond_4

    if-eqz v6, :cond_4

    .line 939
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(ZZ)V

    goto :goto_1

    .line 942
    :cond_4
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(ZZ)V

    goto :goto_1

    .line 946
    :cond_5
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(ZZ)V

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 820
    if-eqz p1, :cond_0

    .line 821
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->k:Z

    .line 822
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->B:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 823
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 824
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 825
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->D:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 826
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->H:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 827
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 828
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 829
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->F:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 852
    :goto_0
    return-void

    .line 832
    :cond_0
    if-eqz p2, :cond_1

    .line 833
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->B:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 834
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 835
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 836
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->D:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 837
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->H:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 838
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 839
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 840
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->F:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 842
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->B:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 843
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 844
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 845
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->D:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 846
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->H:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 847
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 848
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 849
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->F:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 1924
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1926
    const/high16 v1, 0x10000

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 1928
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->M:Z

    return p1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1059
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    const-string v1, "10"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1060
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chaton id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    :goto_0
    return v0

    .line 1062
    :cond_0
    const-string v1, "0"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1063
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pin number : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1066
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "phone number: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const-wide/16 v2, 0x0

    .line 1255
    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 1257
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->N:Z

    if-nez v0, :cond_0

    .line 1259
    invoke-direct {p0, v4, p1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1299
    :goto_0
    return-void

    .line 1278
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->L:Z

    if-nez v0, :cond_3

    .line 1279
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->O:Z

    if-eqz v0, :cond_2

    move-wide v0, v2

    .line 1286
    :goto_1
    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 1287
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->j:I

    int-to-long v0, v0

    .line 1289
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1290
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1293
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v2, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1294
    :catch_0
    move-exception v0

    .line 1295
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1296
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    const v1, 0x7f0b00d8

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1282
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->k()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/sec/chaton/account/i;->a(J)J

    move-result-wide v0

    goto :goto_1

    :cond_3
    move-wide v0, v2

    goto :goto_1
.end method

.method private b(ZZ)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 856
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "drawVButtons : buddyPhoneNumberAvailable = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    if-eqz p1, :cond_0

    .line 859
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->B:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 860
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 861
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 862
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->D:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 863
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->H:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 864
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 865
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 866
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->F:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 889
    :goto_0
    return-void

    .line 869
    :cond_0
    if-eqz p2, :cond_1

    .line 870
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->B:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 871
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 872
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 873
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->D:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 874
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->H:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 875
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 876
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 877
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->F:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 879
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->B:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 880
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 881
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 882
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->D:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 883
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->H:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 884
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 885
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 886
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->F:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->L:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i:Ljava/lang/String;

    return-object p1
.end method

.method private c()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/16 v6, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0702a3

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    const v3, 0x7f0702a4

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->r:Landroid/widget/TextView;

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0702a5

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->o:Landroid/widget/LinearLayout;

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->o:Landroid/widget/LinearLayout;

    const v3, 0x7f0702a6

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->s:Landroid/widget/TextView;

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0702a7

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->p:Landroid/widget/LinearLayout;

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->p:Landroid/widget/LinearLayout;

    const v3, 0x7f0702a8

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->t:Landroid/widget/TextView;

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0702a9

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->q:Landroid/widget/LinearLayout;

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->q:Landroid/widget/LinearLayout;

    const v3, 0x7f0702aa

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->u:Landroid/widget/TextView;

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0702ab

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->v:Landroid/widget/LinearLayout;

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->v:Landroid/widget/LinearLayout;

    const v3, 0x7f0702ac

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->w:Landroid/widget/TextView;

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0702ad

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->x:Landroid/widget/LinearLayout;

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->x:Landroid/widget/LinearLayout;

    const v3, 0x7f0702ae

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->z:Landroid/widget/TextView;

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->x:Landroid/widget/LinearLayout;

    const v3, 0x7f0702af

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->y:Landroid/widget/ImageView;

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->y:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0202d0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 206
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0700e3

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->A:Landroid/widget/ImageButton;

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->A:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/chaton/buddy/eb;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/eb;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0702b1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->B:Landroid/widget/ImageButton;

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->B:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/chaton/buddy/ed;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/ed;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0702b0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->C:Landroid/view/View;

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0700ea

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->D:Landroid/view/View;

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0700eb

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/chaton/buddy/ee;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/ee;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0700e7

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->H:Landroid/widget/ImageButton;

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->H:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->H:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0700e9

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->I:Landroid/widget/ImageButton;

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->I:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0700e6

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->E:Landroid/view/View;

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    const v3, 0x7f0700e8

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->F:Landroid/view/View;

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->F:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->A:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->B:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v0

    if-nez v0, :cond_5

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 275
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->l()V

    .line 277
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->L:Z

    if-nez v0, :cond_7

    .line 278
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->O:Z

    if-eqz v0, :cond_6

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->r:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 286
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/chaton/buddy/ef;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/ef;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 314
    :cond_0
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "************** BIRTHDAY : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    const-string v3, "0000-12-31"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_b

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 394
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->o()I

    move-result v0

    if-lez v0, :cond_10

    .line 395
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->o()I

    move-result v0

    .line 397
    :goto_4
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/c;->n()I

    move-result v3

    if-lez v3, :cond_3

    .line 398
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->n()I

    move-result v1

    .line 401
    :cond_3
    if-nez v0, :cond_f

    if-nez v1, :cond_f

    move v0, v2

    .line 409
    :goto_5
    if-le v2, v0, :cond_e

    .line 446
    :cond_4
    :goto_6
    return-void

    .line 272
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 282
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->r:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 296
    :cond_7
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->p()Ljava/util/List;

    move-result-object v3

    .line 297
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x4

    if-ne v0, v4, :cond_8

    .line 298
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->r:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->s:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->t:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->u:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 302
    :cond_8
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_9

    .line 303
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->r:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->s:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->t:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 306
    :cond_9
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v7, :cond_a

    .line 307
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->r:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->s:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 309
    :cond_a
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 310
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->r:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 318
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    const-string v3, " "

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 320
    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 322
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x5

    if-eq v4, v5, :cond_c

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v4, 0xa

    if-ne v0, v4, :cond_d

    :cond_c
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_d

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 325
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->x:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/chaton/buddy/eg;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/eg;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 337
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->y:Landroid/widget/ImageView;

    new-instance v3, Lcom/sec/chaton/buddy/eh;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/eh;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 364
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->h:Ljava/lang/String;

    .line 365
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a()V

    goto/16 :goto_3

    .line 368
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->y:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->y:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 412
    :cond_e
    if-le v0, v2, :cond_4

    goto/16 :goto_6

    :cond_f
    move v2, v0

    move v0, v1

    goto/16 :goto_5

    :cond_10
    move v0, v1

    goto/16 :goto_4
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1534
    .line 1536
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0221

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1538
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b007d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/ek;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/buddy/ek;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 1546
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->N:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1606
    const/4 v0, 0x0

    .line 1609
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n()I

    move-result v1

    .line 1611
    if-lez v1, :cond_0

    .line 1613
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->h:Ljava/lang/String;

    invoke-direct {p0, v1, v2, p1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(ILjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1619
    :cond_0
    :goto_0
    return-object v0

    .line 1615
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 665
    const/4 v0, 0x0

    .line 667
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    if-eqz v1, :cond_0

    .line 669
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 670
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->D()Ljava/lang/String;

    move-result-object v1

    .line 672
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 673
    const-string v2, "voip=1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 674
    const/4 v0, 0x1

    .line 682
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVBuddy() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    return v0

    .line 678
    :catch_0
    move-exception v1

    .line 679
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->L:Z

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->O:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i:Ljava/lang/String;

    return-object v0
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 688
    const/4 v0, 0x0

    .line 691
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 693
    const/4 v0, 0x1

    .line 699
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVSupportedDevice : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    return v0

    .line 695
    :catch_0
    move-exception v1

    .line 696
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->p()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private f()Z
    .locals 4

    .prologue
    .line 705
    const/4 v0, 0x0

    .line 708
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 710
    const/4 v0, 0x1

    .line 716
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVAvaiable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    return v0

    .line 712
    :catch_0
    move-exception v1

    .line 713
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyInfoFragment;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->j:I

    return v0
.end method

.method private g()Z
    .locals 3

    .prologue
    .line 722
    const/4 v0, 0x0

    .line 725
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->P:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 727
    const/4 v0, 0x1

    .line 733
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVInstalled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    return v0

    .line 729
    :catch_0
    move-exception v1

    .line 730
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private h()Z
    .locals 4

    .prologue
    .line 739
    const/4 v1, 0x0

    .line 742
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->P:Lcom/coolots/sso/a/a;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 743
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 748
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    return v0

    .line 744
    :catch_0
    move-exception v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    .line 745
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 744
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->O:Z

    return v0
.end method

.method private i()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 754
    .line 755
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 756
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 757
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 764
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is3GCallAvailable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    :goto_1
    return v0

    .line 761
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    const-string v3, "tel:+000"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->g()Z

    move-result v0

    return v0
.end method

.method private j()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 770
    const/4 v0, 0x1

    .line 772
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v2

    .line 773
    const/4 v3, -0x3

    if-eq v3, v2, :cond_0

    const/4 v3, -0x2

    if-ne v3, v2, :cond_1

    .line 774
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0205

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 781
    :cond_1
    return v0
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f()Z

    move-result v0

    return v0
.end method

.method private k()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 786
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 788
    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    if-nez v3, :cond_1

    .line 817
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 786
    goto :goto_0

    .line 791
    :cond_1
    if-eqz v0, :cond_5

    .line 792
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processNoChatONVCase : buddyPhoneNumberAvailable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i()Z

    move-result v3

    .line 795
    if-nez v3, :cond_4

    .line 797
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    const-string v4, "phone"

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 798
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.SENDTO"

    const-string v6, "smsto:+000"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 806
    :goto_2
    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 807
    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->m()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 809
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processNoChatONVCase : buddyPhoneNumberAvailable2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processNoChatONVCase : buddyPhoneNumberAvailable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " @ isContactShow = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(ZZ)V

    goto/16 :goto_1

    :cond_2
    move v1, v2

    .line 812
    goto :goto_3

    :cond_3
    move v1, v3

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v3, v0

    move v0, v2

    goto :goto_2
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->h()Z

    move-result v0

    return v0
.end method

.method private l()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 953
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    if-nez v0, :cond_1

    .line 1056
    :cond_0
    :goto_0
    return-void

    .line 962
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 964
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showphonenumber : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " @ getShowPhoneNumber : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    iget-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->L:Z

    if-nez v3, :cond_4

    .line 967
    const-string v3, "refreshCommunicationButtons : is not multi-device"

    sget-object v4, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 969
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->k()V

    .line 1020
    :goto_2
    iget-boolean v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->L:Z

    if-nez v3, :cond_2

    .line 1021
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1022
    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 1032
    :cond_2
    :goto_3
    if-nez v0, :cond_8

    .line 1033
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 962
    goto :goto_1

    .line 972
    :cond_4
    const-string v0, "refreshCommunicationButtons : is multi-device"

    sget-object v3, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->p()Ljava/util/List;

    move-result-object v0

    .line 975
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    .line 981
    :goto_4
    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Z)V

    .line 983
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 984
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 985
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->w:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 986
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->w:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_2

    :cond_5
    move v0, v1

    .line 978
    goto :goto_4

    .line 988
    :cond_6
    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    :cond_7
    move v0, v2

    .line 1025
    goto :goto_3

    .line 1035
    :cond_8
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->L:Z

    if-nez v0, :cond_9

    .line 1036
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1038
    :cond_9
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->p()Ljava/util/List;

    move-result-object v0

    .line 1039
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_a

    .line 1040
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1041
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1042
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1043
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1044
    :cond_a
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_b

    .line 1045
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1046
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1047
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1048
    :cond_b
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_c

    .line 1049
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1050
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1051
    :cond_c
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1052
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d()Z

    move-result v0

    return v0
.end method

.method private m()Z
    .locals 3

    .prologue
    .line 1302
    const/4 v0, 0x0

    .line 1304
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f:Ljava/lang/String;

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1305
    const/4 v0, 0x1

    .line 1308
    :cond_0
    return v0
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->j()Z

    move-result v0

    return v0
.end method

.method private n()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1663
    .line 1664
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const/4 v0, 0x1

    const-string v3, "name"

    aput-object v3, v2, v0

    .line 1665
    const-string v0, "selected=1"

    .line 1666
    const-string v3, "calendars"

    .line 1670
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_0

    .line 1672
    const/4 v0, 0x0

    .line 1676
    :cond_0
    invoke-direct {p0, v2, v0, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1678
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1679
    const-string v0, "name"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1680
    const-string v3, "_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 1683
    :cond_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1684
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1686
    if-eqz v4, :cond_3

    .line 1687
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1691
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move v1, v0

    .line 1696
    :cond_2
    return v1

    .line 1690
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->P:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1854
    .line 1855
    const-string v0, "content://calendar/calendars"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1858
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1862
    if-eqz v0, :cond_0

    .line 1863
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1864
    const-string v6, "content://calendar/"

    :cond_0
    move-object v7, v0

    .line 1868
    :goto_0
    if-nez v6, :cond_3

    .line 1869
    const-string v0, "content://com.android.calendar/calendars"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1871
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1875
    if-eqz v0, :cond_3

    .line 1876
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1877
    const-string v0, "content://com.android.calendar/"

    .line 1882
    :goto_1
    return-object v0

    .line 1859
    :catch_0
    move-exception v0

    .line 1862
    if-eqz v6, :cond_4

    .line 1863
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1864
    const-string v0, "content://calendar/"

    :goto_2
    move-object v7, v6

    move-object v6, v0

    .line 1866
    goto :goto_0

    .line 1862
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 1863
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1864
    const-string v1, "content://calendar/"

    .line 1862
    :cond_1
    throw v0

    .line 1875
    :catchall_1
    move-exception v0

    if-eqz v7, :cond_2

    .line 1876
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1877
    const-string v1, "content://com.android.calendar/"

    .line 1875
    :cond_2
    throw v0

    .line 1872
    :catch_1
    move-exception v0

    .line 1875
    if-eqz v7, :cond_3

    .line 1876
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1877
    const-string v0, "content://com.android.calendar/"

    goto :goto_1

    :cond_3
    move-object v0, v6

    goto :goto_1

    :cond_4
    move-object v0, v6

    goto :goto_2
.end method

.method private p()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1886
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1887
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v1

    .line 1888
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1889
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, "|"

    invoke-direct {v2, v1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1891
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1892
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 1893
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 1895
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x13

    if-lt v3, v4, :cond_0

    const-string v3, "10"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1896
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chaton id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1904
    :goto_1
    const-string v1, ""

    goto :goto_0

    .line 1897
    :cond_0
    const-string v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1898
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pin number : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1900
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "msisdn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1901
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1908
    :cond_2
    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->m()Z

    move-result v0

    return v0
.end method

.method static synthetic q(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->R:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method private q()V
    .locals 3

    .prologue
    .line 1946
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1948
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1949
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1950
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1951
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1953
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->startActivity(Landroid/content/Intent;)V

    .line 1955
    :cond_0
    return-void
.end method

.method static synthetic r(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c()V

    return-void
.end method

.method static synthetic s(Lcom/sec/chaton/buddy/BuddyInfoFragment;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->S:I

    return v0
.end method

.method static synthetic t(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->y:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic v(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->m:Landroid/widget/Toast;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 1588
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    if-nez v0, :cond_1

    .line 1603
    :cond_0
    :goto_0
    return-void

    .line 1591
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0000-12-31"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1594
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1595
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1597
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_2

    .line 1598
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1601
    :cond_2
    new-instance v1, Lcom/sec/chaton/buddy/el;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/el;-><init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/buddy/el;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1913
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1915
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 1916
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->N:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1917
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1918
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->R:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x6

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1921
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 450
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 452
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    .line 453
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 160
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 161
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->R:Lcom/sec/chaton/e/a/u;

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    const v1, 0x7f0b018c

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-static {v0, v2, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->m:Landroid/widget/Toast;

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f:Ljava/lang/String;

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->g:Ljava/lang/String;

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->R:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x3

    invoke-static {}, Lcom/sec/chaton/e/i;->c()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "buddy_no = ? "

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f:Ljava/lang/String;

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->P:Lcom/coolots/sso/a/a;

    .line 178
    const v0, 0x7f03006f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->Q:Landroid/view/View;

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 457
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 459
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->K:Landroid/app/Activity;

    .line 460
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 481
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 485
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 464
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 466
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a()V

    .line 467
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->q()V

    .line 469
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->M:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 470
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->R:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x6

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyInfoFragment;->M:Z

    .line 477
    :cond_0
    return-void
.end method
