.class Lcom/sec/chaton/buddy/ej;
.super Ljava/lang/Object;
.source "BuddyInfoFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V
    .locals 0

    .prologue
    .line 1311
    iput-object p1, p0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 1482
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1472
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 30

    .prologue
    .line 1317
    const/4 v2, 0x3

    move/from16 v0, p1

    if-ne v0, v2, :cond_d

    .line 1318
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buddy information exist : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1319
    if-eqz p3, :cond_3

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 1320
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    move-object/from16 v29, v0

    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    const-string v3, "buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "buddy_status_message"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "buddy_samsung_email"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "buddy_orginal_number"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "buddy_orginal_numbers"

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "buddy_msisdns"

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "buddy_multidevice"

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "Y"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v10, 0x1

    :goto_0
    const-string v11, "buddy_birthday"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_6

    const-string v11, ""

    :goto_1
    const-string v12, "buddy_relation_hide"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "Y"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    const/4 v12, 0x0

    :goto_2
    const-string v13, "buddy_raw_contact_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v14, "buddy_push_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const-string v15, "buddy_is_new"

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const-string v16, "Y"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    const/4 v15, 0x1

    :goto_3
    const-string v16, "relation_send"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const-string v17, "relation_received"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const-string v18, "relation_point"

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    const-string v19, "relation_icon"

    move-object/from16 v0, p3

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const-string v20, "relation_increase"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    const-string v21, "relation_rank"

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    const-string v22, "buddy_profile_status"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    const-string v25, "buddy_show_phone_number"

    move-object/from16 v0, p3

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    const-string v26, "buddy_extra_info"

    move-object/from16 v0, p3

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    const-string v27, "buddy_account_info"

    move-object/from16 v0, p3

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    const-string v28, "buddy_sainfo"

    move-object/from16 v0, p3

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, p3

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v2 .. v28}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v29

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;Lcom/sec/chaton/buddy/a/c;)Lcom/sec/chaton/buddy/a/c;

    .line 1370
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x13

    if-lt v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "10"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1373
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z

    .line 1379
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 1380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z

    .line 1381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->p(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z

    .line 1382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->h(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1383
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1384
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1391
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1392
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1393
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->q(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v2

    const/4 v3, 0x6

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1408
    :cond_2
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->r(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V

    .line 1464
    :cond_3
    :goto_7
    if-eqz p3, :cond_4

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1465
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 1467
    :cond_4
    return-void

    .line 1321
    :cond_5
    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_6
    const-string v11, "buddy_birthday"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    :cond_7
    const/4 v12, 0x1

    goto/16 :goto_2

    :cond_8
    const/4 v15, 0x0

    goto/16 :goto_3

    .line 1376
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z

    goto/16 :goto_4

    .line 1389
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_5

    .line 1396
    :cond_b
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1397
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1398
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->q(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v2

    const/4 v3, 0x6

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1401
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->f(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/util/List;

    move-result-object v3

    .line 1402
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 1403
    sget-object v4, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1404
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "+"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1405
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->q(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v2

    const/4 v3, 0x6

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1412
    :cond_d
    const/4 v2, 0x6

    move/from16 v0, p1

    if-ne v0, v2, :cond_3

    .line 1413
    if-eqz p3, :cond_15

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_15

    .line 1414
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1415
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z

    .line 1416
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const-string v3, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;I)I

    .line 1419
    :try_start_0
    const-string v2, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1420
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "contact_id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 1422
    if-eqz v3, :cond_10

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v2

    if-lez v2, :cond_10

    .line 1424
    :cond_e
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1425
    const-string v2, "data2"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1426
    const/4 v4, 0x2

    if-ne v2, v4, :cond_e

    .line 1427
    const-string v2, "data1"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1428
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 1429
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v4, v2}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1435
    :cond_f
    if-eqz v3, :cond_10

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_10

    .line 1436
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1445
    :cond_10
    if-eqz p3, :cond_11

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_11

    .line 1446
    :goto_8
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 1450
    :cond_11
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ContactSaved id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->g(Lcom/sec/chaton/buddy/BuddyInfoFragment;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ContactSaved number : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->e(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1435
    :catchall_0
    move-exception v2

    if-eqz v3, :cond_12

    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_12

    .line 1436
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1435
    :cond_12
    throw v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1441
    :catch_0
    move-exception v2

    .line 1442
    :try_start_4
    invoke-static {}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1445
    if-eqz p3, :cond_11

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_11

    goto :goto_8

    :catchall_1
    move-exception v2

    if-eqz p3, :cond_13

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_13

    .line 1446
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 1445
    :cond_13
    throw v2

    .line 1455
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z

    .line 1456
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;I)I

    goto/16 :goto_7

    .line 1459
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->c(Lcom/sec/chaton/buddy/BuddyInfoFragment;Z)Z

    .line 1460
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/ej;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;I)I

    goto/16 :goto_7
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 1477
    return-void
.end method
