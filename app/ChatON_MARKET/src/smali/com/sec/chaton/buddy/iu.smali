.class public Lcom/sec/chaton/buddy/iu;
.super Landroid/widget/ArrayAdapter;
.source "TellFriendsFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/buddy/it;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/TellFriendsFragment;

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/buddy/it;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/TellFriendsFragment;Landroid/content/Context;ILjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/buddy/it;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/chaton/buddy/iu;->a:Lcom/sec/chaton/buddy/TellFriendsFragment;

    .line 141
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 143
    iput-object p2, p0, Lcom/sec/chaton/buddy/iu;->b:Landroid/content/Context;

    .line 144
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/buddy/iu;->c:Landroid/view/LayoutInflater;

    .line 145
    iput-object p4, p0, Lcom/sec/chaton/buddy/iu;->d:Ljava/util/List;

    .line 147
    iget-object v0, p1, Lcom/sec/chaton/buddy/TellFriendsFragment;->e:Lcom/sec/chaton/buddy/iv;

    if-nez v0, :cond_0

    .line 148
    new-instance v1, Lcom/sec/chaton/buddy/iv;

    invoke-direct {v1, p1}, Lcom/sec/chaton/buddy/iv;-><init>(Lcom/sec/chaton/buddy/TellFriendsFragment;)V

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Void;

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Void;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/chaton/buddy/iv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/iv;

    iput-object v0, p1, Lcom/sec/chaton/buddy/TellFriendsFragment;->e:Lcom/sec/chaton/buddy/iv;

    .line 150
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/buddy/it;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/chaton/buddy/iu;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/it;

    return-object v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/iu;->a(I)Lcom/sec/chaton/buddy/it;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 154
    .line 155
    iget-object v0, p0, Lcom/sec/chaton/buddy/iu;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/it;

    .line 157
    if-nez p2, :cond_0

    .line 158
    iget-object v1, p0, Lcom/sec/chaton/buddy/iu;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f030124

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 161
    :cond_0
    const v1, 0x7f07014b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 162
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/it;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 164
    const v1, 0x7f07014c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 165
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/it;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    const v0, 0x7f0702d7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 168
    return-object p2
.end method
