.class public Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;
.super Landroid/app/Activity;
.source "ChatONVInstallDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/coolots/sso/a/c;


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field a:Landroid/content/Context;

.field private c:Lcom/coolots/sso/a/a;

.field private d:Landroid/app/ProgressDialog;

.field private e:Lcom/sec/chaton/d/h;

.field private f:Lcom/sec/common/a/d;

.field private g:Lcom/sec/common/a/d;

.field private h:Z

.field private i:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 47
    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    .line 49
    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->f:Lcom/sec/common/a/d;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->h:Z

    .line 356
    new-instance v0, Lcom/sec/chaton/buddy/dialog/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/m;-><init>(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->i:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->f:Lcom/sec/common/a/d;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 103
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 104
    const-string v0, "showAlertDailog"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/dialog/h;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dialog/h;-><init>(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/dialog/g;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dialog/g;-><init>(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->g:Lcom/sec/common/a/d;

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->g:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/buddy/dialog/i;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/i;-><init>(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->g:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 141
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->b()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 144
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->c:Lcom/coolots/sso/a/a;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    .line 151
    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    const v1, 0x7f0b0355

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 157
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->finish()V

    .line 171
    :cond_1
    :goto_0
    return-void

    .line 162
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165
    const-string v1, "is_mapping_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 166
    const-string v1, "is_multi_device_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 168
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->startActivity(Landroid/content/Intent;)V

    .line 169
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->finish()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->e:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 266
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 269
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 270
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 271
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->startActivity(Landroid/content/Intent;)V

    .line 275
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->f:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->h:Z

    return v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 205
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 208
    :pswitch_0
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->c:Lcom/coolots/sso/a/a;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    .line 215
    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 220
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    const v1, 0x7f0b0355

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 221
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->finish()V

    goto :goto_0

    .line 226
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/samsungaccount/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 229
    const-string v1, "is_mapping_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 230
    const-string v1, "is_multi_device_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 232
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->startActivity(Landroid/content/Intent;)V

    .line 233
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->finish()V

    goto :goto_0

    .line 239
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->finish()V

    goto :goto_0

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x7f070135
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 280
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 300
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 56
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    iput-object p0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    .line 62
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 63
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->setFinishOnTouchOutside(Z)V

    .line 94
    :cond_0
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->c:Lcom/coolots/sso/a/a;

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->c:Lcom/coolots/sso/a/a;

    invoke-virtual {v0, p0, p0}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 97
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->i:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->e:Lcom/sec/chaton/d/h;

    .line 99
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a()V

    .line 100
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 176
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 178
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 179
    const-string v0, "onDestroy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->c:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->c:Lcom/coolots/sso/a/a;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->f:Lcom/sec/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->f:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->f:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 188
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->g:Lcom/sec/common/a/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->g:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->g:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 195
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->h:Z

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->e:Lcom/sec/chaton/d/h;

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->e()V

    .line 199
    return-void
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 306
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 307
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveCreateAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_0
    if-ne p1, v3, :cond_1

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->e:Lcom/sec/chaton/d/h;

    const-string v1, "voip"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;IZ)V

    .line 348
    :goto_0
    return-void

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 317
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0309

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/buddy/dialog/k;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dialog/k;-><init>(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/buddy/dialog/j;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dialog/j;-><init>(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->f:Lcom/sec/common/a/d;

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->f:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/buddy/dialog/l;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/l;-><init>(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 341
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->h:Z

    if-nez v0, :cond_3

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->f:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 345
    :cond_3
    const-string v0, "[ChatONV] Fail to create account"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 0

    .prologue
    .line 354
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 260
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 262
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->c()V

    .line 263
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 246
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 251
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 256
    :goto_0
    return-void

    .line 253
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
