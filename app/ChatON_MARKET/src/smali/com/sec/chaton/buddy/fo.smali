.class Lcom/sec/chaton/buddy/fo;
.super Landroid/os/Handler;
.source "BuddyProfileImageViewFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V
    .locals 0

    .prologue
    .line 506
    iput-object p1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const/16 v3, 0x258

    const/16 v11, 0x8

    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 509
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 642
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 515
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 516
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_7

    .line 518
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetProfileImageList;

    .line 519
    if-eqz v0, :cond_0

    .line 523
    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->listcount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;I)I

    .line 524
    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->profileimagelist:Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 525
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->j(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->j(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    move v8, v9

    .line 526
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->j(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_5

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->j(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    .line 529
    iget-object v0, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->k(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 530
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 531
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->l(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 532
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 533
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 534
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 537
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "main profileimageurl : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->m(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    new-instance v0, Lcom/sec/chaton/userprofile/bq;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->m(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a()Ljava/lang/String;

    move-result-object v5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buddy_f_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v6}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/userprofile/bq;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 539
    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->g(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 543
    :cond_3
    const-string v0, "1"

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 544
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 545
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 547
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 548
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    .line 550
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 587
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "profileimageurl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "buddy_t_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 592
    new-instance v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;-><init>()V

    .line 593
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->m(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->id:Ljava/lang/String;

    .line 594
    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    .line 595
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "buddy_t_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->ThumbfileName:Ljava/lang/String;

    .line 596
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "buddy_f_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    .line 597
    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    .line 598
    iget-object v1, v7, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    .line 600
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profileimageurl : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->n(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 526
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_1

    .line 606
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->o(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V

    .line 607
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->p(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 609
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v0, v9}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->setHasOptionsMenu(Z)V

    .line 611
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->q(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 612
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->q(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 616
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 619
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->p(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 620
    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->q(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 621
    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->q(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 624
    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_9

    .line 626
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->r(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)V

    .line 627
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/f;->b(Ljava/lang/String;)V

    .line 638
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 630
    :cond_9
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v1, 0x3aa1

    if-ne v0, v1, :cond_a

    .line 631
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ca

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Unknown"

    aput-object v4, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 633
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/fo;->a:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v9}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 513
    :pswitch_data_0
    .packed-switch 0x19a
        :pswitch_0
    .end packed-switch
.end method
