.class Lcom/sec/chaton/buddy/el;
.super Landroid/os/AsyncTask;
.source "BuddyInfoFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V
    .locals 0

    .prologue
    .line 1486
    iput-object p1, p0, Lcom/sec/chaton/buddy/el;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1494
    aget-object v0, p1, v7

    .line 1496
    :try_start_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1498
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1500
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1502
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1503
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 1504
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1505
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/chaton/buddy/el;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->s(Lcom/sec/chaton/buddy/BuddyInfoFragment;)I

    move-result v1

    invoke-virtual {v4, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 1506
    iget-object v0, p0, Lcom/sec/chaton/buddy/el;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->t(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Ljava/lang/String;

    move-result-object v1

    .line 1507
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    int-to-long v2, v0

    add-long/2addr v2, v5

    .line 1508
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 1509
    iget-object v0, p0, Lcom/sec/chaton/buddy/el;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->a(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Success"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1510
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1515
    :goto_0
    return-object v0

    .line 1512
    :catch_0
    move-exception v0

    .line 1515
    :cond_0
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 1520
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1521
    iget-object v0, p0, Lcom/sec/chaton/buddy/el;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->u(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1522
    iget-object v0, p0, Lcom/sec/chaton/buddy/el;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->u(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/el;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0202d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1529
    :goto_0
    return-void

    .line 1525
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/el;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->u(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1526
    iget-object v0, p0, Lcom/sec/chaton/buddy/el;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->u(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/el;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0202cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1486
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/el;->a([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1486
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/el;->a(Ljava/lang/Boolean;)V

    return-void
.end method
