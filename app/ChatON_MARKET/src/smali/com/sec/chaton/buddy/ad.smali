.class Lcom/sec/chaton/buddy/ad;
.super Ljava/lang/Object;
.source "AddBuddyFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/AddBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 0

    .prologue
    .line 610
    iput-object p1, p0, Lcom/sec/chaton/buddy/ad;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 614
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    .line 615
    const/16 v1, 0x42

    if-eq p2, v1, :cond_0

    const/16 v1, 0x17

    if-ne p2, v1, :cond_2

    .line 616
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/ad;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a()V

    .line 617
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 618
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    const-string v2, "00030024"

    invoke-virtual {v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 620
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/ad;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->d(Lcom/sec/chaton/buddy/AddBuddyFragment;)Lcom/sec/chaton/widget/AdaptableEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/widget/AdaptableEditText;->requestFocus()Z

    .line 621
    iget-object v1, p0, Lcom/sec/chaton/buddy/ad;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v1, p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(Lcom/sec/chaton/buddy/AddBuddyFragment;Landroid/view/View;)V

    .line 625
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
