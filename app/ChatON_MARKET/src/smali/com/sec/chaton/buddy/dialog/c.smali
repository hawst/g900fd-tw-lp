.class Lcom/sec/chaton/buddy/dialog/c;
.super Ljava/lang/Object;
.source "BuddyDialog.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V
    .locals 0

    .prologue
    .line 1989
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2134
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 2135
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 2136
    const/4 v0, -0x1

    if-eq p3, v0, :cond_1

    .line 2137
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->G(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Toast;

    move-result-object v0

    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 2138
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->G(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 2139
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->G(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2141
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 2142
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->E(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2149
    :cond_0
    :goto_0
    return-void

    .line 2145
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0074

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2146
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2107
    if-ne p1, v5, :cond_0

    .line 2108
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 2110
    invoke-virtual {p3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2112
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 2113
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const v1, 0x7f0b0086

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2115
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->G(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 2116
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->G(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Toast;->setDuration(I)V

    .line 2117
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->G(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2118
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0, v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 2119
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->E(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2125
    :cond_0
    :goto_0
    return-void

    .line 2121
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2122
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 30

    .prologue
    .line 1994
    const/4 v2, 0x3

    move/from16 v0, p1

    if-ne v0, v2, :cond_e

    .line 1995
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buddy information exist : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v3, v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1996
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 1999
    if-eqz p3, :cond_d

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_d

    .line 2000
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2001
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    move-object/from16 v29, v0

    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    const-string v3, "buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "buddy_status_message"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "buddy_samsung_email"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "buddy_orginal_number"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "buddy_orginal_numbers"

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "buddy_msisdns"

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "buddy_multidevice"

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "Y"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v10, 0x1

    :goto_0
    const-string v11, "buddy_birthday"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_6

    const-string v11, ""

    :goto_1
    const-string v12, "buddy_relation_hide"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "Y"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    const/4 v12, 0x0

    :goto_2
    const-string v13, "buddy_raw_contact_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v14, "buddy_push_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const-string v15, "buddy_is_new"

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const-string v16, "Y"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    const/4 v15, 0x1

    :goto_3
    const-string v16, "relation_send"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const-string v17, "relation_received"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const-string v18, "relation_point"

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    const-string v19, "relation_icon"

    move-object/from16 v0, p3

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const-string v20, "relation_increase"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    const-string v21, "relation_rank"

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    const-string v22, "buddy_profile_status"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    const-string v25, "buddy_show_phone_number"

    move-object/from16 v0, p3

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    const-string v26, "buddy_extra_info"

    move-object/from16 v0, p3

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    const-string v27, "buddy_account_info"

    move-object/from16 v0, p3

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    const-string v28, "buddy_sainfo"

    move-object/from16 v0, p3

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, p3

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v2 .. v28}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v29

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Lcom/sec/chaton/buddy/a/c;)Lcom/sec/chaton/buddy/a/c;

    .line 2032
    const-string v2, "is_favorite"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2033
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 2039
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x13

    if-lt v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    const-string v3, "10"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->y(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 2040
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 2045
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->z(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 2046
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 2047
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->A(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->g(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 2048
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->B(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 2049
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->y(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    .line 2050
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2051
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v4, v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    .line 2065
    :cond_2
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)V

    .line 2100
    :cond_3
    :goto_7
    if-eqz p3, :cond_4

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2101
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 2103
    :cond_4
    return-void

    .line 2001
    :cond_5
    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_6
    const-string v11, "buddy_birthday"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    :cond_7
    const/4 v12, 0x1

    goto/16 :goto_2

    :cond_8
    const/4 v15, 0x0

    goto/16 :goto_3

    .line 2035
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    goto/16 :goto_4

    .line 2042
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    goto/16 :goto_5

    .line 2054
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->y(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    .line 2055
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2056
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    goto :goto_6

    .line 2060
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->C(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/util/List;

    move-result-object v2

    .line 2061
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 2062
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    goto/16 :goto_6

    .line 2069
    :cond_d
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buddy information not found : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v3, v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2070
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 2071
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 2072
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->D(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    goto/16 :goto_7

    .line 2081
    :cond_e
    const/4 v2, 0x4

    move/from16 v0, p1

    if-ne v0, v2, :cond_3

    .line 2082
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/16 v3, 0x32

    if-lt v2, v3, :cond_f

    .line 2083
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0072

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2084
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2086
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z

    .line 2087
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->E(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v2

    const v3, 0x7f02000f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    .line 2090
    :cond_f
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 2092
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2093
    const-string v3, "group_relation_group"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2094
    const-string v3, "group_relation_buddy"

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/dialog/c;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v3}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->F(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/e/a/u;

    move-result-object v3

    const/4 v4, 0x1

    sget-object v5, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    move-object/from16 v0, p2

    invoke-virtual {v3, v4, v0, v5, v2}, Lcom/sec/chaton/e/a/u;->startInsert(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;)V

    goto/16 :goto_7
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 2130
    return-void
.end method
