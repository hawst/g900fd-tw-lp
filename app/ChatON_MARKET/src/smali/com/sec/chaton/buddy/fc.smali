.class Lcom/sec/chaton/buddy/fc;
.super Ljava/lang/Object;
.source "BuddyProfileFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyProfileFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 2385
    iput-object p1, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2650
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 2651
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 2653
    const/4 v0, -0x1

    if-eq p3, v0, :cond_2

    .line 2655
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/Toast;

    move-result-object v0

    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 2656
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 2657
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2659
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2663
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2664
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    .line 2667
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->n(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Z)V

    .line 2676
    :cond_1
    :goto_0
    return-void

    .line 2671
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0074

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2672
    iget-object v1, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->q(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2604
    if-ne p1, v5, :cond_1

    .line 2605
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 2607
    invoke-virtual {p3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2609
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 2611
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const v1, 0x7f0b0086

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2620
    iget-object v1, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 2621
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Toast;->setDuration(I)V

    .line 2622
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->W(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2624
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0, v5}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2628
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2629
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->X(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    .line 2632
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->n(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Z)V

    .line 2640
    :cond_1
    :goto_0
    return-void

    .line 2636
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2637
    iget-object v1, p0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->q(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 30

    .prologue
    .line 2390
    const/4 v2, 0x3

    move/from16 v0, p1

    if-ne v0, v2, :cond_10

    .line 2391
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buddy information exist : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392
    if-eqz p3, :cond_e

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_e

    .line 2393
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2394
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    move-object/from16 v29, v0

    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    const-string v3, "buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "buddy_status_message"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "buddy_samsung_email"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "buddy_orginal_number"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "buddy_orginal_numbers"

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "buddy_msisdns"

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "buddy_multidevice"

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "Y"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v10, 0x1

    :goto_0
    const-string v11, "buddy_birthday"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_6

    const-string v11, ""

    :goto_1
    const-string v12, "buddy_relation_hide"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "Y"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    const/4 v12, 0x0

    :goto_2
    const-string v13, "buddy_raw_contact_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v14, "buddy_push_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const-string v15, "buddy_is_new"

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const-string v16, "Y"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    const/4 v15, 0x1

    :goto_3
    const-string v16, "relation_send"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const-string v17, "relation_received"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const-string v18, "relation_point"

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    const-string v19, "relation_icon"

    move-object/from16 v0, p3

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const-string v20, "relation_increase"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    const-string v21, "relation_rank"

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    const-string v22, "buddy_profile_status"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    const-string v25, "buddy_show_phone_number"

    move-object/from16 v0, p3

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    const-string v26, "buddy_extra_info"

    move-object/from16 v0, p3

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    const-string v27, "buddy_account_info"

    move-object/from16 v0, p3

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    const-string v28, "buddy_sainfo"

    move-object/from16 v0, p3

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, p3

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v2 .. v28}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v29

    invoke-static {v0, v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileFragment;Lcom/sec/chaton/buddy/a/c;)Lcom/sec/chaton/buddy/a/c;

    .line 2425
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 2437
    const-string v2, "is_favorite"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2438
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2444
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x13

    if-lt v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "10"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 2447
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2453
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->A(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 2454
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2455
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->V(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2456
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->E(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 2457
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2458
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 2459
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->p(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v2

    const/4 v3, 0x6

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 2483
    :cond_2
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2484
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileFragment;I)V

    .line 2596
    :cond_3
    :goto_7
    if-eqz p3, :cond_4

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2597
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 2599
    :cond_4
    return-void

    .line 2394
    :cond_5
    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_6
    const-string v11, "buddy_birthday"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    :cond_7
    const/4 v12, 0x1

    goto/16 :goto_2

    :cond_8
    const/4 v15, 0x0

    goto/16 :goto_3

    .line 2440
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    goto/16 :goto_4

    .line 2450
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    goto/16 :goto_5

    .line 2461
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 2462
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->B(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 2470
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->B(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2471
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->B(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2472
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->p(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v2

    const/4 v3, 0x6

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 2467
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_8

    .line 2476
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->C(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/util/List;

    move-result-object v3

    .line 2477
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 2478
    sget-object v4, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2479
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "+"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 2480
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->p(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v2

    const/4 v3, 0x6

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 2488
    :cond_e
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buddy information not found : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2489
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2491
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2493
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileFragment;I)V

    goto/16 :goto_7

    .line 2495
    :cond_f
    new-instance v2, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->q(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2496
    const-string v3, "BUDDY_DIALOG_BUDDY_NO"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2497
    const-string v3, "BUDDY_DIALOG_BUDDY_NAME"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->r(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2498
    const-string v3, "BUDDY_DIALOG_BUDDY_FROM_PROFILE"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2499
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v3, v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_7

    .line 2505
    :cond_10
    const/4 v2, 0x4

    move/from16 v0, p1

    if-ne v0, v2, :cond_14

    .line 2506
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/16 v3, 0x32

    if-lt v2, v3, :cond_11

    .line 2508
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0072

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2511
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->q(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2513
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2516
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->n(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Z)V

    goto/16 :goto_7

    .line 2518
    :cond_11
    check-cast p2, Lcom/sec/chaton/buddy/a/c;

    .line 2519
    if-eqz p2, :cond_12

    .line 2520
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2521
    const-string v3, "group_relation_group"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2522
    const-string v3, "group_relation_buddy"

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->p(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v3

    const/4 v4, 0x1

    sget-object v5, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    move-object/from16 v0, p2

    invoke-virtual {v3, v4, v0, v5, v2}, Lcom/sec/chaton/e/a/u;->startInsert(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;)V

    goto/16 :goto_7

    .line 2526
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->r(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 2527
    const-string v2, "Invalid buddy info from 3rd party"

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 2529
    :cond_13
    const-string v2, "buddyInfo is null"

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 2535
    :cond_14
    const/4 v2, 0x6

    move/from16 v0, p1

    if-ne v0, v2, :cond_3

    .line 2539
    if-eqz p3, :cond_1c

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1c

    .line 2540
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 2541
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2542
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const-string v3, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;I)I

    .line 2545
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[phoneCall] Original number : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->B(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2546
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->B(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2547
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[phoneCall] After getPhoneDigitNumber : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2549
    const-string v2, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2550
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->q(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "contact_id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 2552
    if-eqz v3, :cond_17

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v2

    if-lez v2, :cond_17

    .line 2554
    :cond_15
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 2555
    const-string v2, "data1"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2556
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_15

    .line 2557
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v4, v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2558
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v5, v8, v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 2559
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v4, v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2565
    :cond_16
    if-eqz v3, :cond_17

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_17

    .line 2566
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2575
    :cond_17
    if-eqz p3, :cond_18

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_18

    .line 2576
    :goto_9
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 2580
    :cond_18
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ContactSaved id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->D(Lcom/sec/chaton/buddy/BuddyProfileFragment;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2581
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ContactSaved number : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->B(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 2565
    :catchall_0
    move-exception v2

    if-eqz v3, :cond_19

    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_19

    .line 2566
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 2565
    :cond_19
    throw v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2571
    :catch_0
    move-exception v2

    .line 2572
    :try_start_4
    sget-object v3, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2575
    if-eqz p3, :cond_18

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_18

    goto :goto_9

    :catchall_1
    move-exception v2

    if-eqz p3, :cond_1a

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1a

    .line 2576
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 2575
    :cond_1a
    throw v2

    .line 2585
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2586
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;I)I

    goto/16 :goto_7

    .line 2589
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileFragment;Z)Z

    .line 2590
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/buddy/fc;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;I)I

    goto/16 :goto_7
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 2645
    return-void
.end method
