.class public Lcom/sec/chaton/buddy/ae;
.super Ljava/lang/Object;
.source "AddBuddyFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field d:Lcom/sec/chaton/e/a/u;

.field final synthetic e:Lcom/sec/chaton/buddy/AddBuddyFragment;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 2

    .prologue
    .line 1763
    iput-object p1, p0, Lcom/sec/chaton/buddy/ae;->e:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1757
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/buddy/ae;->a:I

    .line 1758
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/buddy/ae;->b:I

    .line 1759
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/buddy/ae;->c:I

    .line 1761
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/ae;->d:Lcom/sec/chaton/e/a/u;

    .line 1764
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-static {p1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ae;->d:Lcom/sec/chaton/e/a/u;

    .line 1765
    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1768
    iget-object v0, p0, Lcom/sec/chaton/buddy/ae;->d:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/buddy/ae;->a:I

    invoke-static {}, Lcom/sec/chaton/e/ag;->a()Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v7, "recommend_timestamp"

    const-wide/16 v8, 0x0

    invoke-virtual {v5, v7, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v4

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1769
    return-void
.end method

.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 1868
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1858
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1790
    iget v0, p0, Lcom/sec/chaton/buddy/ae;->a:I

    if-ne p1, v0, :cond_2

    .line 1791
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1792
    invoke-static {v3}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(I)I

    .line 1794
    if-eqz p3, :cond_0

    .line 1795
    :try_start_0
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1796
    const-string v1, "count"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1799
    :cond_0
    if-eqz p3, :cond_1

    .line 1800
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 1804
    :cond_1
    if-ne v0, v4, :cond_4

    .line 1816
    iget-object v0, p0, Lcom/sec/chaton/buddy/ae;->e:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(I)V

    .line 1853
    :cond_2
    :goto_0
    return-void

    .line 1799
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_3

    .line 1800
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 1799
    :cond_3
    throw v0

    .line 1818
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/ae;->e:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/buddy/AddBuddyFragment;->a(I)V

    goto :goto_0
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 1863
    return-void
.end method
