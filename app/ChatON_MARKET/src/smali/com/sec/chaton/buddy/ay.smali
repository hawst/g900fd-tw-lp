.class Lcom/sec/chaton/buddy/ay;
.super Ljava/lang/Object;
.source "BuddyEditNickNameFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/ax;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/ax;)V
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lcom/sec/chaton/buddy/ay;->a:Lcom/sec/chaton/buddy/ax;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 439
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 440
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_2

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ay;->a:Lcom/sec/chaton/buddy/ax;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 449
    :cond_1
    :goto_0
    return-void

    .line 443
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/ay;->a:Lcom/sec/chaton/buddy/ax;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 444
    iget-object v1, p0, Lcom/sec/chaton/buddy/ay;->a:Lcom/sec/chaton/buddy/ax;

    iget-object v1, v1, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a:Lcom/sec/chaton/d/h;

    const-string v2, "update"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/ay;->a:Lcom/sec/chaton/buddy/ax;

    iget-object v4, v4, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->f(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v5, v0, v3}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I

    .line 445
    iget-object v0, p0, Lcom/sec/chaton/buddy/ay;->a:Lcom/sec/chaton/buddy/ax;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 446
    iget-object v0, p0, Lcom/sec/chaton/buddy/ay;->a:Lcom/sec/chaton/buddy/ax;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method
