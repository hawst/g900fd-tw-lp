.class public Lcom/sec/chaton/buddy/fj;
.super Landroid/widget/BaseAdapter;
.source "BuddyProfileHistoryAdapter.java"


# instance fields
.field a:Landroid/view/LayoutInflater;

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:Z

.field private e:Landroid/content/Context;

.field private f:Lcom/sec/common/f/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;",
            ">;",
            "Lcom/sec/common/f/c;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 44
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 29
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/fj;->d:Z

    .line 45
    iput-object p1, p0, Lcom/sec/chaton/buddy/fj;->e:Landroid/content/Context;

    .line 46
    iput p2, p0, Lcom/sec/chaton/buddy/fj;->c:I

    .line 47
    iput-object p3, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    .line 48
    iput-object p4, p0, Lcom/sec/chaton/buddy/fj;->f:Lcom/sec/common/f/c;

    .line 49
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/buddy/fj;->a:Landroid/view/LayoutInflater;

    .line 50
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/fj;->d:Z

    .line 51
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/fj;->a(I)Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 65
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/16 v3, 0x3c

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 70
    .line 72
    if-nez p2, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/buddy/fj;->c:I

    invoke-virtual {v0, v1, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 76
    :cond_0
    const v0, 0x7f0701e9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/ImageView;

    .line 77
    const v0, 0x7f0701e8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 78
    const v0, 0x7f0701ea

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/ImageView;

    .line 79
    const v0, 0x7f0701eb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/widget/ImageView;

    .line 80
    const v0, 0x7f070124

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 82
    const-string v2, "1"

    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iput-object v7, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->thumbImage:Landroid/widget/ImageView;

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->f:Lcom/sec/common/f/c;

    invoke-virtual {v0, v7}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 92
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 93
    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 94
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 96
    new-instance v0, Lcom/sec/chaton/userprofile/bq;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iget-object v2, v2, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "&size=100"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iget-object v5, v4, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iget-object v6, v4, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->ThumbfileName:Ljava/lang/String;

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/userprofile/bq;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v1, p0, Lcom/sec/chaton/buddy/fj;->f:Lcom/sec/common/f/c;

    invoke-virtual {v1, v7, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 99
    const-string v1, "1"

    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 100
    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 117
    return-object p2

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iput-object v9, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->selectedImage:Landroid/widget/ImageView;

    goto/16 :goto_0

    .line 106
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UserProfileImageView.PROFILE_CURRENT_IMAGE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->selectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 110
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UserProfileImageView.PROFILE_CURRENT_IMAGE(NO)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyProfileImageView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/buddy/fj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->selectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1
.end method
