.class Lcom/sec/chaton/buddy/gc;
.super Ljava/lang/Object;
.source "BuddyRecommendFragment.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/af;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/gb;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/gb;)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Lcom/sec/chaton/buddy/gc;->a:Lcom/sec/chaton/buddy/gb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelled()V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sec/chaton/buddy/gc;->a:Lcom/sec/chaton/buddy/gb;

    iget-object v0, v0, Lcom/sec/chaton/buddy/gb;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 467
    return-void
.end method

.method public onComplete()V
    .locals 3

    .prologue
    .line 458
    const-string v0, "onComplete() \t- Login"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/buddy/gc;->a:Lcom/sec/chaton/buddy/gb;

    iget-object v0, v0, Lcom/sec/chaton/buddy/gb;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 460
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/tellfriends/TwitterFriendsPickerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 461
    iget-object v1, p0, Lcom/sec/chaton/buddy/gc;->a:Lcom/sec/chaton/buddy/gb;

    iget-object v1, v1, Lcom/sec/chaton/buddy/gb;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->startActivity(Landroid/content/Intent;)V

    .line 462
    return-void
.end method

.method public onError(I)V
    .locals 2

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/chaton/buddy/gc;->a:Lcom/sec/chaton/buddy/gb;

    iget-object v0, v0, Lcom/sec/chaton/buddy/gb;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/buddy/gc;->a:Lcom/sec/chaton/buddy/gb;

    iget-object v0, v0, Lcom/sec/chaton/buddy/gb;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Z)V

    .line 454
    return-void
.end method
