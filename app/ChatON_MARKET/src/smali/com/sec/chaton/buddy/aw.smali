.class Lcom/sec/chaton/buddy/aw;
.super Ljava/lang/Object;
.source "BuddyEditNickNameFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lcom/sec/chaton/buddy/aw;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 364
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 370
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 375
    iget-object v0, p0, Lcom/sec/chaton/buddy/aw;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 376
    iget-object v0, p0, Lcom/sec/chaton/buddy/aw;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/aw;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/buddy/aw;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->h(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 385
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v1, 0x1f

    if-ne v0, v1, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/buddy/aw;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0143

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 394
    :cond_0
    :goto_1
    return-void

    .line 379
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/aw;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->h(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 391
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/aw;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->h(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method
