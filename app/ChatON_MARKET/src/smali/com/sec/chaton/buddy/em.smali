.class Lcom/sec/chaton/buddy/em;
.super Landroid/os/AsyncTask;
.source "BuddyInfoFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyInfoFragment;)V
    .locals 0

    .prologue
    .line 1622
    iput-object p1, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1627
    iget-object v0, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->d(Lcom/sec/chaton/buddy/BuddyInfoFragment;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/net/Uri;)V
    .locals 5

    .prologue
    const v4, 0x7f0b0083

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1634
    if-nez p1, :cond_0

    .line 1635
    iget-object v0, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1658
    :goto_0
    return-void

    .line 1640
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1642
    if-lez v0, :cond_1

    .line 1648
    iget-object v0, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->u(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1649
    iget-object v0, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->u(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0202d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1650
    iget-object v0, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->v(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/Toast;

    move-result-object v0

    const v1, 0x7f0b0088

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 1651
    iget-object v0, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->v(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Toast;->setDuration(I)V

    .line 1652
    iget-object v0, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->v(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1655
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->b(Lcom/sec/chaton/buddy/BuddyInfoFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/em;->a:Lcom/sec/chaton/buddy/BuddyInfoFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1622
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/em;->a([Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1622
    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/em;->a(Landroid/net/Uri;)V

    return-void
.end method
