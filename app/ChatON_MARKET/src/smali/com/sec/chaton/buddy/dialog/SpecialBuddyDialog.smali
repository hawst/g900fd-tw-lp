.class public Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;
.super Landroid/app/Activity;
.source "SpecialBuddyDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/common/f/f;


# instance fields
.field private A:Landroid/widget/LinearLayout;

.field private B:Landroid/view/ViewGroup;

.field private C:Landroid/widget/Button;

.field private D:Landroid/widget/Button;

.field private E:Landroid/widget/Button;

.field private F:Landroid/widget/Button;

.field private G:Landroid/graphics/Bitmap;

.field a:Landroid/view/View$OnClickListener;

.field b:Lcom/sec/chaton/e/a/v;

.field c:Landroid/os/Handler;

.field private d:Lcom/sec/chaton/e/a/u;

.field private e:Lcom/sec/chaton/buddy/a/d;

.field private f:Landroid/app/ProgressDialog;

.field private g:Landroid/widget/Toast;

.field private h:Landroid/content/Context;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:I

.field private m:Z

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/ProgressBar;

.field private t:Landroid/widget/ProgressBar;

.field private u:Z

.field private v:Landroid/widget/LinearLayout;

.field private w:Landroid/widget/LinearLayout;

.field private x:Landroid/widget/LinearLayout;

.field private y:Landroid/widget/LinearLayout;

.field private z:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 113
    iput v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->k:I

    .line 114
    iput v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->l:I

    .line 115
    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m:Z

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->G:Landroid/graphics/Bitmap;

    .line 484
    new-instance v0, Lcom/sec/chaton/buddy/dialog/ak;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/ak;-><init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    .line 633
    new-instance v0, Lcom/sec/chaton/buddy/dialog/al;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/al;-><init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->b:Lcom/sec/chaton/e/a/v;

    .line 721
    new-instance v0, Lcom/sec/chaton/buddy/dialog/ab;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/ab;-><init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->k:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->G:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->n:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/d;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 424
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m:Z

    if-eqz v0, :cond_0

    .line 425
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->b()V

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->z:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 428
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 429
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 439
    :goto_0
    return-void

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 433
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->z:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 434
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 436
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 471
    if-eqz p1, :cond_0

    .line 472
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->r:Landroid/widget/TextView;

    const v1, 0x7f0b03c7

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 477
    :goto_0
    return-void

    .line 475
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->r:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lcom/sec/chaton/buddy/dialog/am;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1039
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 1040
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g:Landroid/widget/Toast;

    .line 1042
    :cond_0
    sget-object v0, Lcom/sec/chaton/buddy/dialog/ac;->b:[I

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/dialog/am;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1056
    :goto_0
    return-void

    .line 1044
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g:Landroid/widget/Toast;

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 1045
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 1046
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1050
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g:Landroid/widget/Toast;

    const v1, 0x7f0b002a

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 1051
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 1052
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1042
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Z)V
    .locals 12

    .prologue
    const v2, 0x7f030148

    const/16 v11, 0x8

    const/4 v10, -0x1

    const/4 v9, -0x2

    const/4 v8, 0x0

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    if-eqz v0, :cond_7

    .line 198
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 199
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[initView()] isFollowed : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mSpecialBuddyInfoForDialog : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SpecialBuddyDialog"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->setContentView(I)V

    .line 203
    const v0, 0x7f070531

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 204
    const v1, 0x7f070536

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 205
    const-string v2, "window"

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 207
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getOrientation()I

    move-result v3

    .line 208
    if-eqz v3, :cond_1

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 209
    :cond_1
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-direct {v3, v4, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 210
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x3fec000000000000L    # 0.875

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-direct {v0, v10, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 217
    :goto_0
    const v0, 0x7f070533

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->v:Landroid/widget/LinearLayout;

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->v:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    const v0, 0x7f070534

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->o:Landroid/widget/ImageView;

    .line 220
    const v0, 0x7f07053a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->r:Landroid/widget/TextView;

    .line 222
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/d;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/d;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->k:I

    .line 228
    :goto_1
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c()V

    .line 231
    const v0, 0x7f070532

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->p:Landroid/widget/TextView;

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    :goto_2
    const v0, 0x7f070538

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->s:Landroid/widget/ProgressBar;

    .line 239
    const v0, 0x7f070535

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->t:Landroid/widget/ProgressBar;

    .line 242
    const v0, 0x7f070539

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->q:Landroid/widget/TextView;

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 260
    :goto_3
    const v0, 0x7f070537

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->n:Landroid/widget/ImageView;

    .line 261
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->n:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->n:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/d;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 265
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->h()V

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/buddy/dialog/ad;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/ad;-><init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/util/ad;->a(Ljava/lang/String;Lcom/sec/chaton/util/bb;)V

    .line 301
    :cond_2
    const v0, 0x7f07053e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->w:Landroid/widget/LinearLayout;

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->w:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/buddy/dialog/af;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/af;-><init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 312
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->w:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 315
    const v0, 0x7f07053b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->A:Landroid/widget/LinearLayout;

    .line 316
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->A:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/buddy/dialog/ag;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/ag;-><init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 326
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->A:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 329
    const v0, 0x7f07053c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->y:Landroid/widget/LinearLayout;

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->y:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/buddy/dialog/ah;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/ah;-><init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->y:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343
    const v0, 0x7f07053d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->x:Landroid/widget/LinearLayout;

    .line 344
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->x:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/buddy/dialog/ai;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/ai;-><init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 354
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->x:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357
    const v0, 0x7f07053f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->z:Landroid/widget/LinearLayout;

    .line 358
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->z:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/buddy/dialog/aj;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/aj;-><init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 368
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->z:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a()V

    .line 421
    :goto_4
    return-void

    .line 212
    :cond_3
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v2

    int-to-double v4, v2

    const-wide v6, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v4, v6

    double-to-int v2, v4

    invoke-direct {v3, v2, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 213
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09024a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v10, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 225
    :cond_4
    iput v8, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->k:I

    goto/16 :goto_1

    .line 235
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->p:Landroid/widget/TextView;

    const-string v1, "Special Buddy"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 246
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/d;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/d;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->q:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/buddy/dialog/aa;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/aa;-><init>(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_3

    .line 373
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[initView()] isFollowed : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mSpecialBuddyInfoForDialog is null "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SpecialBuddyDialog"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->setContentView(I)V

    .line 377
    const v0, 0x7f070532

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->p:Landroid/widget/TextView;

    .line 378
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 384
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j:Ljava/lang/String;

    if-nez v0, :cond_a

    .line 385
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->p:Landroid/widget/TextView;

    const v1, 0x7f0b00b4

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 391
    :goto_6
    const v0, 0x7f070539

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->q:Landroid/widget/TextView;

    .line 392
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 394
    const v0, 0x7f07053e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->C:Landroid/widget/Button;

    .line 395
    const v0, 0x7f07053d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->D:Landroid/widget/Button;

    .line 396
    const v0, 0x7f070504

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->E:Landroid/widget/Button;

    .line 397
    const v0, 0x7f070505

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->F:Landroid/widget/Button;

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->C:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->D:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->E:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->F:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 404
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->B:Landroid/view/ViewGroup;

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->E:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->F:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 410
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_8

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->C:Landroid/widget/Button;

    const v1, 0x7f0b0081

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->D:Landroid/widget/Button;

    const v1, 0x7f0b0080

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 413
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->E:Landroid/widget/Button;

    const v1, 0x7f0b003a

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->F:Landroid/widget/Button;

    const v1, 0x7f0b00e7

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 417
    :cond_8
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 418
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v8}, Lcom/sec/chaton/d/h;->f(Ljava/lang/String;Z)I

    goto/16 :goto_4

    .line 381
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->p:Landroid/widget/TextView;

    const-string v1, "Special Buddy"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 387
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_6
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->G:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 442
    sget-object v0, Lcom/sec/chaton/buddy/dialog/ac;->a:[I

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/d;->m()Lcom/sec/chaton/buddy/a/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 460
    :goto_0
    return-void

    .line 444
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 445
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 448
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 452
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 456
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 442
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 463
    if-eqz p1, :cond_0

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->o:Landroid/widget/ImageView;

    const v1, 0x7f020015

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 468
    :goto_0
    return-void

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->o:Landroid/widget/ImageView;

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->h:Landroid/content/Context;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 480
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->u:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->b(Z)V

    .line 481
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->k:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(I)V

    .line 482
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->b(Z)V

    return-void
.end method

.method private c(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 602
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 621
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    if-eqz p1, :cond_3

    .line 606
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->v:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 607
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 609
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->t:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->t:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 613
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->v:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    .line 614
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 616
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->t:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->t:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 624
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 625
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->c(Ljava/lang/String;)V

    .line 626
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->u:Z

    return p1
.end method

.method private d(Z)Z
    .locals 1

    .prologue
    .line 1028
    invoke-static {}, Lcom/sec/common/util/i;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1029
    if-eqz p1, :cond_0

    .line 1030
    sget-object v0, Lcom/sec/chaton/buddy/dialog/am;->a:Lcom/sec/chaton/buddy/dialog/am;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Lcom/sec/chaton/buddy/dialog/am;)V

    .line 1032
    :cond_0
    const/4 v0, 0x0

    .line 1035
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 629
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 630
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->d(Ljava/lang/String;)V

    .line 631
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->f()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->a(Z)V

    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 925
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->h:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 934
    :goto_0
    return-void

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->f:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 929
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->h:Landroid/content/Context;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->f:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 932
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;Z)Z
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m:Z

    return p1
.end method

.method private g()V
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->h:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 943
    :cond_0
    :goto_0
    return-void

    .line 940
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->f:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 941
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m:Z

    return v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 946
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->s:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->s:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 949
    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e()V

    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/buddy/a/d;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->e:Lcom/sec/chaton/buddy/a/d;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 952
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->s:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 954
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->s:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 956
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i:Ljava/lang/String;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 987
    const-string v0, "showPasswordLockActivity"

    const-string v1, "SpecialBuddyDialog"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 990
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 991
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 992
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 993
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 995
    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->u:Z

    return v0
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->k:I

    return v0
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->c()V

    return-void
.end method

.method static synthetic p(Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g()V

    return-void
.end method


# virtual methods
.method a(Lcom/sec/chaton/buddy/a/d;Lcom/sec/chaton/io/entry/inner/SpecialUser;)Lcom/sec/chaton/buddy/a/d;
    .locals 12

    .prologue
    .line 960
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->specialuserid:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->name:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->description:Ljava/lang/String;

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->c()Ljava/lang/String;

    move-result-object v2

    :goto_2
    iget-object v3, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->followcount:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->d()Ljava/lang/String;

    move-result-object v3

    :goto_3
    iget-object v4, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->likecount:Ljava/lang/String;

    if-nez v4, :cond_4

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->e()Ljava/lang/String;

    move-result-object v4

    :goto_4
    iget-object v5, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->msgstatus:Ljava/lang/String;

    if-nez v5, :cond_5

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->f()Ljava/lang/String;

    move-result-object v5

    :goto_5
    iget-object v6, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->photoloaded:Ljava/lang/String;

    if-nez v6, :cond_6

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->g()Ljava/lang/String;

    move-result-object v6

    :goto_6
    iget-object v7, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->status:Ljava/lang/String;

    if-nez v7, :cond_7

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->h()Ljava/lang/String;

    move-result-object v7

    :goto_7
    iget-object v8, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->url:Ljava/lang/String;

    if-nez v8, :cond_8

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->i()Ljava/lang/String;

    move-result-object v8

    :goto_8
    iget-object v9, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->weburl:Ljava/lang/String;

    if-nez v9, :cond_9

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->j()Ljava/lang/String;

    move-result-object v9

    :goto_9
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->k()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->tel:Ljava/lang/String;

    if-nez v11, :cond_a

    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/d;->l()Ljava/lang/String;

    move-result-object v11

    :goto_a
    invoke-static/range {v0 .. v11}, Lcom/sec/chaton/buddy/a/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v0

    .line 965
    return-object v0

    .line 960
    :cond_0
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->specialuserid:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->name:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->description:Ljava/lang/String;

    goto :goto_2

    :cond_3
    iget-object v3, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->followcount:Ljava/lang/String;

    goto :goto_3

    :cond_4
    iget-object v4, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->likecount:Ljava/lang/String;

    goto :goto_4

    :cond_5
    iget-object v5, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->msgstatus:Ljava/lang/String;

    goto :goto_5

    :cond_6
    iget-object v6, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->photoloaded:Ljava/lang/String;

    goto :goto_6

    :cond_7
    iget-object v7, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->status:Ljava/lang/String;

    goto :goto_7

    :cond_8
    iget-object v8, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->url:Ljava/lang/String;

    goto :goto_8

    :cond_9
    iget-object v9, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->weburl:Ljava/lang/String;

    goto :goto_9

    :cond_a
    iget-object v11, p2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->tel:Ljava/lang/String;

    goto :goto_a
.end method

.method public a(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1001
    return-void
.end method

.method public b(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1007
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1017
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1018
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1020
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 1022
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->finish()V

    .line 1024
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1013
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 143
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 144
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 145
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SpecialBuddyDialog"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->requestWindowFeature(I)Z

    .line 149
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 150
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->setFinishOnTouchOutside(Z)V

    .line 152
    :cond_1
    iput-object p0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->h:Landroid/content/Context;

    .line 153
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "specialuserid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i:Ljava/lang/String;

    .line 154
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "speicalusername"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j:Ljava/lang/String;

    .line 155
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/af;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m:Z

    .line 157
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CURRENT_POSITION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->l:I

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 160
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->finish()V

    .line 162
    :cond_2
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->b:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d:Lcom/sec/chaton/e/a/u;

    .line 163
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->g()V

    .line 188
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 189
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 193
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 194
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 167
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 169
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->m:Z

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d:Lcom/sec/chaton/e/a/u;

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const-string v5, "buddy_no = ? "

    new-array v6, v1, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i:Ljava/lang/String;

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SpecialBuddyDialog"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->j()V

    .line 178
    return-void

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->d:Lcom/sec/chaton/e/a/u;

    const/4 v4, 0x4

    sget-object v3, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v5, "buddy_no = ? "

    new-array v6, v1, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;->i:Ljava/lang/String;

    aput-object v1, v6, v7

    move v1, v4

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 182
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 183
    return-void
.end method
