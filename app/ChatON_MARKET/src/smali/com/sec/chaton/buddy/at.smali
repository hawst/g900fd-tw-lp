.class Lcom/sec/chaton/buddy/at;
.super Landroid/os/Handler;
.source "BuddyEditNickNameFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 254
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 297
    :goto_0
    return-void

    .line 260
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 262
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "group rename to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    new-instance v0, Lcom/sec/chaton/e/b/j;

    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c:Lcom/sec/chaton/e/b/d;

    iget-object v2, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/b/j;-><init>(Lcom/sec/chaton/e/b/d;Ljava/lang/String;I)V

    .line 265
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Z)V

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 273
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 274
    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 275
    const-string v2, "PROFILE_GROUP_RENAME"

    iget-object v3, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 278
    iget-object v0, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 280
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 281
    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 283
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/o;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 285
    iget-object v0, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0122

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0139

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/au;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/au;-><init>(Lcom/sec/chaton/buddy/at;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/at;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0
.end method
