.class public Lcom/sec/chaton/buddy/dialog/BuddyDialog;
.super Landroid/app/Activity;
.source "BuddyDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/common/f/f;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/ImageView;

.field private B:Landroid/widget/ImageView;

.field private C:Landroid/widget/ImageView;

.field private D:Landroid/widget/ImageView;

.field private E:Landroid/widget/ImageView;

.field private F:Landroid/widget/ImageView;

.field private G:Landroid/widget/ImageView;

.field private H:Landroid/widget/ImageView;

.field private I:Landroid/widget/ImageButton;

.field private J:Landroid/widget/ImageView;

.field private K:Landroid/widget/ImageView;

.field private L:Lcom/sec/chaton/buddy/fl;

.field private M:Lcom/sec/chaton/buddy/fl;

.field private N:Lcom/sec/chaton/buddy/fl;

.field private O:Lcom/sec/chaton/buddy/fl;

.field private P:Landroid/widget/LinearLayout;

.field private Q:Landroid/widget/LinearLayout;

.field private R:Landroid/widget/LinearLayout;

.field private S:Landroid/widget/LinearLayout;

.field private T:Landroid/widget/LinearLayout;

.field private U:Landroid/widget/LinearLayout;

.field private V:Landroid/widget/LinearLayout;

.field private W:Z

.field private X:Z

.field private Y:Lcom/sec/common/f/c;

.field private Z:Ljava/lang/String;

.field private aA:Landroid/widget/LinearLayout;

.field private aB:Z

.field private aC:Z

.field private aD:I

.field private aE:Lcom/coolots/sso/a/a;

.field private aF:Landroid/os/Handler;

.field private aG:Landroid/os/Handler;

.field private aa:Ljava/lang/String;

.field private ab:Ljava/lang/String;

.field private ac:Landroid/widget/Toast;

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:I

.field private ah:Lcom/sec/chaton/e/a/u;

.field private ai:Landroid/widget/TextView;

.field private aj:Landroid/widget/ImageView;

.field private ak:Landroid/widget/ImageView;

.field private al:Landroid/widget/ImageView;

.field private am:Landroid/widget/TextView;

.field private an:Landroid/view/ViewGroup;

.field private ao:Landroid/widget/Button;

.field private ap:Landroid/widget/Button;

.field private aq:Landroid/widget/Button;

.field private ar:Z

.field private as:Z

.field private at:Landroid/view/View;

.field private au:Landroid/view/ViewGroup;

.field private av:Landroid/widget/LinearLayout;

.field private aw:Landroid/widget/LinearLayout;

.field private ax:Landroid/widget/LinearLayout;

.field private ay:Landroid/widget/LinearLayout;

.field private az:Landroid/widget/LinearLayout;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Z

.field h:Lcom/sec/chaton/e/a/v;

.field i:Landroid/os/Handler;

.field j:Lcom/sec/chaton/e/b/d;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Lcom/sec/chaton/d/w;

.field private o:Lcom/sec/chaton/d/i;

.field private p:Lcom/sec/chaton/buddy/a/c;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/LinearLayout;

.field private s:Landroid/widget/ImageView;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Landroid/content/Context;

.field private x:Landroid/widget/ImageView;

.field private y:Landroid/widget/LinearLayout;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    const-class v0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 127
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/flag/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k:Ljava/lang/String;

    .line 160
    iput-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u:Ljava/lang/String;

    .line 161
    iput-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->v:Ljava/lang/String;

    .line 175
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W:Z

    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->X:Z

    .line 180
    const-string v0, "&size=800"

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aa:Ljava/lang/String;

    .line 181
    const-string v0, "&size=100"

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ab:Ljava/lang/String;

    .line 186
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ae:Z

    .line 201
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ar:Z

    .line 202
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->as:Z

    .line 218
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aB:Z

    .line 219
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aC:Z

    .line 220
    iput v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aD:I

    .line 224
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->g:Z

    .line 1563
    new-instance v0, Lcom/sec/chaton/buddy/dialog/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/b;-><init>(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aF:Landroid/os/Handler;

    .line 1989
    new-instance v0, Lcom/sec/chaton/buddy/dialog/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/c;-><init>(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h:Lcom/sec/chaton/e/a/v;

    .line 2632
    new-instance v0, Lcom/sec/chaton/buddy/dialog/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/d;-><init>(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aG:Landroid/os/Handler;

    .line 2799
    new-instance v0, Lcom/sec/chaton/buddy/dialog/e;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/e;-><init>(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i:Landroid/os/Handler;

    .line 2955
    new-instance v0, Lcom/sec/chaton/buddy/dialog/f;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/f;-><init>(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->j:Lcom/sec/chaton/e/b/d;

    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o()Z

    move-result v0

    return v0
.end method

.method static synthetic B(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ae:Z

    return v0
.end method

.method static synthetic C(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/util/List;
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic D(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d()V

    return-void
.end method

.method static synthetic E(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic F(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ah:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic G(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ac:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic H(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->au:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic I(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->an:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic J(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ax:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic K(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aw:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic L(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->av:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic M(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->az:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic N(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ao:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic O(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ap:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic P(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aq:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic Q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ay:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ai:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->am:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic T(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/View;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->at:Landroid/view/View;

    return-object v0
.end method

.method static synthetic U(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ar:Z

    return v0
.end method

.method static synthetic V(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p()V

    return-void
.end method

.method static synthetic W(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->v()V

    return-void
.end method

.method static synthetic X(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ak:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic Y(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->as:Z

    return v0
.end method

.method static synthetic Z(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;I)I
    .locals 0

    .prologue
    .line 127
    iput p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aD:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Lcom/sec/chaton/buddy/a/c;)Lcom/sec/chaton/buddy/a/c;
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->t:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->t()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2506
    .line 2512
    if-eqz p2, :cond_4

    .line 2516
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2517
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " static copyCoverStoryforRandomImages mOriginPath : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2518
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " static copyCoverStoryforRandomImages mDestPath : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2520
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2521
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 2523
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2524
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2526
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 2527
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 2528
    const-string v0, " static copyCoverStoryforRandomImages already exist..."

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2551
    :cond_1
    :goto_0
    return-void

    .line 2531
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 2533
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2534
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 2535
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_3

    .line 2536
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " static copyCoverStoryforRandomImages copy to : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2538
    :cond_3
    invoke-static {v0, v1}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2547
    :catch_0
    move-exception v0

    .line 2549
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 2542
    :cond_4
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 2543
    const-string v0, " static copyCoverStoryforRandomImages dirInternalPathS is null : "

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2595
    .line 2604
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2605
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "randomDir : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "copyCoverStoryforBuddyProfile"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2606
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mBuddyPath : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "copyCoverStoryforBuddyProfile"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2608
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2609
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 2611
    new-instance v1, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "coverstory.jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2612
    new-instance v2, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".jpg"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2614
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    .line 2618
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2619
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2627
    :goto_0
    invoke-static {v2, v1}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 2629
    :cond_1
    return-void

    .line 2620
    :catch_0
    move-exception v0

    .line 2622
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2623
    :catch_1
    move-exception v0

    .line 2625
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x2

    const v9, 0x7f020344

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 321
    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->setContentView(I)V

    .line 322
    iput-object p0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->w:Landroid/content/Context;

    .line 324
    const v0, 0x7f0700a9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 325
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->w:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 327
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getOrientation()I

    move-result v2

    .line 328
    if-eqz v2, :cond_0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 329
    :cond_0
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-double v3, v1

    const-wide v5, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v3, v5

    double-to-int v1, v3

    invoke-direct {v2, v1, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 335
    :goto_0
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aE:Lcom/coolots/sso/a/a;

    .line 337
    iput-boolean v7, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aB:Z

    .line 338
    iput-boolean v7, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aC:Z

    .line 342
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 344
    const v0, 0x7f0700aa

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q:Landroid/widget/TextView;

    .line 345
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 346
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 349
    :cond_1
    const v0, 0x7f0700ab

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->r:Landroid/widget/LinearLayout;

    .line 351
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 352
    const v0, 0x7f0700ac

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x:Landroid/widget/ImageView;

    .line 353
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W:Z

    if-eqz v0, :cond_7

    .line 354
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x:Landroid/widget/ImageView;

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 359
    :goto_1
    const v0, 0x7f0700ae

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->y:Landroid/widget/LinearLayout;

    .line 360
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 367
    const v0, 0x7f0700af

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    .line 375
    const v0, 0x7f0700b2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->z:Landroid/widget/TextView;

    .line 376
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->z:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    :goto_2
    const v0, 0x7f0700b3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->A:Landroid/widget/ImageView;

    .line 383
    const v0, 0x7f0700b4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->E:Landroid/widget/ImageView;

    .line 384
    const v0, 0x7f0700b0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->J:Landroid/widget/ImageView;

    .line 385
    const v0, 0x7f0700b1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->K:Landroid/widget/ImageView;

    .line 389
    const v0, 0x7f0700b6

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->B:Landroid/widget/ImageView;

    .line 390
    const v0, 0x7f0700b7

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->F:Landroid/widget/ImageView;

    .line 391
    const v0, 0x7f0700b8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->C:Landroid/widget/ImageView;

    .line 392
    const v0, 0x7f0700b9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->G:Landroid/widget/ImageView;

    .line 393
    const v0, 0x7f0700ba

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->D:Landroid/widget/ImageView;

    .line 394
    const v0, 0x7f0700bb

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->H:Landroid/widget/ImageView;

    .line 395
    const v0, 0x7f0700bc

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I:Landroid/widget/ImageButton;

    .line 396
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->A:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    .line 397
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->B:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    .line 398
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->C:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    .line 399
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->D:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 405
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/util/f;->a(Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 406
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Is Web Only buddy : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->E()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->J:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->E()I

    move-result v0

    if-ne v0, v11, :cond_9

    move v0, v7

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 409
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e()Z

    move-result v0

    if-ne v0, v11, :cond_a

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->K:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 421
    :goto_4
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a()V

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 423
    if-nez p1, :cond_4

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n:Lcom/sec/chaton/d/w;

    if-nez v0, :cond_3

    .line 427
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 428
    const-string v0, "[BuddyDialog] mProfileControl is null "

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    :cond_2
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aF:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n:Lcom/sec/chaton/d/w;

    .line 432
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n:Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    const-string v2, "160"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/w;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :cond_4
    const v0, 0x7f0700ad

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->P:Landroid/widget/LinearLayout;

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 440
    const v0, 0x7f0700bd

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Q:Landroid/widget/LinearLayout;

    .line 441
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 443
    const v0, 0x7f0700be

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R:Landroid/widget/LinearLayout;

    .line 444
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 446
    const v0, 0x7f0700c1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S:Landroid/widget/LinearLayout;

    .line 447
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450
    const v0, 0x7f0700bf

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->T:Landroid/widget/LinearLayout;

    .line 451
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->T:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->T:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 454
    const v0, 0x7f0700c0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U:Landroid/widget/LinearLayout;

    .line 455
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 456
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 458
    const v0, 0x7f0700c2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->V:Landroid/widget/LinearLayout;

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 461
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aD:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I:Landroid/widget/ImageButton;

    if-eqz v0, :cond_5

    .line 462
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 466
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m()V

    .line 467
    return-void

    .line 331
    :cond_6
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-double v3, v1

    const-wide v5, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v3, v5

    double-to-int v1, v3

    invoke-direct {v2, v1, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 356
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x:Landroid/widget/ImageView;

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 379
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->z:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_9
    move v0, v8

    .line 407
    goto/16 :goto_3

    .line 412
    :cond_a
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->K:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_4

    .line 415
    :catch_0
    move-exception v0

    .line 416
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_4

    .line 417
    :catch_1
    move-exception v0

    .line 418
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 716
    if-eqz p1, :cond_0

    .line 730
    :goto_0
    return-void

    .line 721
    :cond_0
    if-eqz p2, :cond_1

    .line 722
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 723
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 725
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 726
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 727
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Q:Landroid/widget/LinearLayout;

    const v1, 0x7f0200f7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 849
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 850
    const/high16 v1, 0x10000

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 852
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aB:Z

    return p1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2309
    .line 2315
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2316
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2319
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 2320
    if-eqz v1, :cond_0

    .line 2335
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    array-length v4, v1

    int-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    .line 2336
    aget-object v1, v1, v2

    .line 2338
    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 2339
    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 2340
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadRandomimage filename : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "### pos : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ### randomId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2341
    const/4 v0, 0x1

    .line 2343
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2344
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2345
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadRandomimage randomId : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ### randomFile : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2347
    if-eqz p3, :cond_0

    .line 2348
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 2350
    invoke-static {v3, p0, p1, p2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2363
    :cond_0
    :goto_0
    return v0

    .line 2358
    :cond_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 2359
    const-string v1, "loadRandomimage No random images in file folder "

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;)Z
    .locals 7

    .prologue
    .line 2418
    const/4 v0, 0x0

    .line 2424
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2426
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2427
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 2428
    const-string v1, "checkAndSetCoverStoryRandomImages No random images in file folder "

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2466
    :cond_0
    :goto_0
    return v0

    .line 2433
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2434
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2436
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2437
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 2438
    const-string v1, "checkAndSetCoverStoryRandomImages not exists the random image in file folder #2#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2442
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_4

    .line 2443
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 2444
    const-string v1, "checkAndSetCoverStoryRandomImages file size is 0."

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2446
    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 2449
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 2450
    const-string v0, "checkAndSetCoverStoryRandomImages get the random image in file folder #3#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2452
    :cond_5
    const/4 v0, 0x1

    .line 2454
    if-eqz p4, :cond_6

    .line 2456
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/coverstory/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2457
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p4, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 2458
    invoke-static {p0, p1, p2, v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2461
    :cond_6
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 2462
    const-string v1, "checkAndSetCoverStoryRandomImages set the random image from file folder #4#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic aa(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ab(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->al:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;I)I
    .locals 0

    .prologue
    .line 127
    iput p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->w:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->v:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2555
    .line 2558
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2560
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 2561
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dirInternalPath : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "copyCoverStoryforBuddyProfile"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2562
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mBuddyCoverStoryDir : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "copyCoverStoryforBuddyProfile"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2564
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2565
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 2567
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "coverstory.jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2568
    new-instance v2, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".jpg"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2570
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    .line 2574
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2575
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2583
    :goto_0
    invoke-static {v2, v1}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 2585
    :cond_1
    return-void

    .line 2576
    :catch_0
    move-exception v0

    .line 2578
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2579
    :catch_1
    move-exception v0

    .line 2581
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 8

    .prologue
    .line 760
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f()Z

    move-result v2

    .line 761
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->g()Z

    move-result v3

    .line 762
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i()Z

    move-result v4

    .line 764
    const/4 v1, 0x0

    .line 766
    if-eqz p1, :cond_6

    .line 767
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "processMultiDeviceCase : buddyPhoneNumberAvailable = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->j()Z

    move-result p1

    .line 770
    if-nez p1, :cond_6

    .line 771
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v5, "phone"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 772
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.SENDTO"

    const-string v7, "smsto:+000"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0, v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 773
    const/4 v0, 0x1

    .line 778
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e()Z

    move-result v1

    .line 784
    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    .line 785
    if-eqz v4, :cond_1

    .line 786
    if-eqz v1, :cond_0

    .line 787
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(ZZ)V

    .line 816
    :goto_1
    return-void

    .line 791
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(ZZ)V

    goto :goto_1

    .line 795
    :cond_1
    if-eqz v1, :cond_2

    .line 796
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(ZZ)V

    goto :goto_1

    .line 800
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(ZZ)V

    goto :goto_1

    .line 804
    :cond_3
    if-eqz v2, :cond_5

    .line 805
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h()Z

    move-result v2

    if-nez v2, :cond_4

    if-eqz v1, :cond_4

    .line 806
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(ZZ)V

    goto :goto_1

    .line 809
    :cond_4
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(ZZ)V

    goto :goto_1

    .line 813
    :cond_5
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(ZZ)V

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method private b(ZZ)V
    .locals 4

    .prologue
    const v3, 0x7f0200f6

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 734
    if-eqz p1, :cond_0

    .line 736
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 737
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 738
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->T:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 739
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 740
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 756
    :goto_0
    return-void

    .line 743
    :cond_0
    if-eqz p2, :cond_1

    .line 744
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 745
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 746
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->T:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 747
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 749
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 750
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 751
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->T:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 752
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 753
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aC:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/w;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n:Lcom/sec/chaton/d/w;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->X:Z

    return p1
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 3178
    const/4 v0, 0x0

    .line 3183
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 3184
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    .line 3186
    const-string v3, "[isMatchNumber] matching for original Number"

    sget-object v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3188
    if-le v1, v5, :cond_1

    if-le v2, v5, :cond_1

    .line 3189
    add-int/lit8 v1, v1, -0x8

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 3190
    add-int/lit8 v2, v2, -0x8

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 3191
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[isMatchNumber] matching for contact Number : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3192
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[isMatchNumber] found match number : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3194
    const/4 v0, 0x1

    .line 3202
    :goto_0
    return v0

    .line 3196
    :cond_0
    const-string v1, "[isMatchNumber] phone number not matched"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3199
    :cond_1
    const-string v1, "[isMatchNumber] phone number length is less than 8"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aD:I

    return v0
.end method

.method private d()V
    .locals 8

    .prologue
    const/4 v7, -0x2

    const-wide v5, 0x3feccccccccccccdL    # 0.9

    .line 470
    const v0, 0x7f030021

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->setContentView(I)V

    .line 472
    const v0, 0x7f0700c4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 473
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->w:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 475
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getOrientation()I

    move-result v2

    .line 476
    if-eqz v2, :cond_0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 477
    :cond_0
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-double v3, v1

    mul-double/2addr v3, v5

    double-to-int v1, v3

    invoke-direct {v2, v1, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 483
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->as:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    if-nez v0, :cond_1

    .line 484
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    .line 486
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 488
    const v0, 0x7f0700c8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ai:Landroid/widget/TextView;

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 490
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->as:Z

    if-eqz v0, :cond_3

    .line 491
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ai:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 499
    :goto_1
    const v0, 0x7f0700c5

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aj:Landroid/widget/ImageView;

    .line 503
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aj:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 508
    const v0, 0x7f0700c6

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ak:Landroid/widget/ImageView;

    .line 509
    const v0, 0x7f0700c7

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->al:Landroid/widget/ImageView;

    .line 511
    const v0, 0x7f0700cb

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aA:Landroid/widget/LinearLayout;

    .line 514
    const v0, 0x7f0700c9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->am:Landroid/widget/TextView;

    .line 516
    const v0, 0x7f0700d0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->an:Landroid/view/ViewGroup;

    .line 517
    const v0, 0x7f0700d1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ax:Landroid/widget/LinearLayout;

    .line 518
    const v0, 0x7f0700d2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aw:Landroid/widget/LinearLayout;

    .line 519
    const v0, 0x7f0700d3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->av:Landroid/widget/LinearLayout;

    .line 520
    const v0, 0x7f0700d5

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ay:Landroid/widget/LinearLayout;

    .line 521
    const v0, 0x7f0700d4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->az:Landroid/widget/LinearLayout;

    .line 522
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ax:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 523
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aw:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 524
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->av:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 525
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ay:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 526
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->az:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 528
    const v0, 0x7f0700ca

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->at:Landroid/view/View;

    .line 529
    const v0, 0x7f0700cc

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->au:Landroid/view/ViewGroup;

    .line 530
    const v0, 0x7f0700cd

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ao:Landroid/widget/Button;

    .line 531
    const v0, 0x7f0700ce

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ap:Landroid/widget/Button;

    .line 532
    const v0, 0x7f0700cf

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aq:Landroid/widget/Button;

    .line 533
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ao:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 534
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ap:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 535
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aq:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 539
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u()V

    .line 540
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aG:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 541
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/h;->f(Ljava/lang/String;Z)I

    .line 543
    return-void

    .line 479
    :cond_2
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    int-to-double v3, v1

    mul-double/2addr v3, v5

    double-to-int v1, v3

    invoke-direct {v2, v1, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 493
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ai:Landroid/widget/TextView;

    const v1, 0x7f0b00b4

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 496
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ai:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->g(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W:Z

    return p1
.end method

.method private d(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 546
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    const-string v1, "10"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 547
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chaton id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    :goto_0
    return v0

    .line 549
    :cond_0
    const-string v1, "0"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 550
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pin number : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 553
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "phone number: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h(Ljava/lang/String;)V

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 7

    .prologue
    const v6, 0x7f0b00d8

    const/4 v5, 0x0

    const-wide/16 v2, 0x0

    .line 1275
    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 1277
    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Ljava/lang/String;)V

    .line 1279
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ad:Z

    if-nez v0, :cond_1

    .line 1281
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1282
    const-string v1, "phone"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1283
    const-string v1, "name"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1285
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "phoneNum: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NAME: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1290
    const-string v1, "email"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1293
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.INSERT"

    invoke-direct {v1, v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1294
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1295
    const-string v0, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1296
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    .line 1298
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1330
    :goto_0
    return-void

    .line 1299
    :catch_0
    move-exception v0

    .line 1300
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1301
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1306
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->af:Z

    if-nez v0, :cond_4

    .line 1307
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ae:Z

    if-eqz v0, :cond_3

    move-wide v0, v2

    .line 1314
    :goto_1
    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 1315
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ag:I

    int-to-long v0, v0

    .line 1318
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1320
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1321
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1322
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    .line 1324
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1325
    :catch_1
    move-exception v0

    .line 1326
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1327
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1310
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->k()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/sec/chaton/account/i;->a(J)J

    move-result-wide v0

    goto :goto_1

    :cond_4
    move-wide v0, v2

    goto :goto_1
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 562
    const/4 v0, 0x0

    .line 564
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    if-eqz v1, :cond_0

    .line 566
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 567
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->D()Ljava/lang/String;

    move-result-object v1

    .line 569
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 570
    const-string v2, "voip=1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 571
    const/4 v0, 0x1

    .line 580
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVBuddy() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    return v0

    .line 575
    :catch_0
    move-exception v1

    .line 576
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->af:Z

    return p1
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I
    .locals 2

    .prologue
    .line 127
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m:I

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l:Ljava/lang/String;

    return-object p1
.end method

.method private f()Z
    .locals 3

    .prologue
    .line 586
    const/4 v0, 0x0

    .line 589
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 591
    const/4 v0, 0x1

    .line 597
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVSupportedDevice() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    return v0

    .line 593
    :catch_0
    move-exception v1

    .line 594
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ad:Z

    return p1
.end method

.method private f(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2169
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2170
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2179
    :cond_0
    :goto_0
    return v0

    .line 2174
    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".png"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2175
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2176
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->E:Landroid/widget/ImageView;

    return-object v0
.end method

.method private g(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2183
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2184
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2186
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 2188
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const/16 v2, 0x42

    const/16 v3, 0x2c

    invoke-static {v1, v0, v2, v3}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2189
    if-eqz v0, :cond_0

    .line 2190
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->al:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2192
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->al:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2201
    :cond_0
    :goto_0
    return-void

    .line 2195
    :catch_0
    move-exception v0

    .line 2196
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    .line 2197
    :catch_1
    move-exception v0

    .line 2198
    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private g()Z
    .locals 4

    .prologue
    .line 603
    const/4 v0, 0x0

    .line 606
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 608
    const/4 v0, 0x1

    .line 614
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVAvaiable() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    return v0

    .line 610
    :catch_0
    move-exception v1

    .line 611
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)Z
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ae:Z

    return p1
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Z)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Z)V

    return-void
.end method

.method private h(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3018
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 3019
    invoke-virtual {v0, p1}, Lcom/sec/chaton/d/h;->h(Ljava/lang/String;)V

    .line 3020
    return-void
.end method

.method private h()Z
    .locals 3

    .prologue
    .line 620
    const/4 v0, 0x0

    .line 623
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aE:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 625
    const/4 v0, 0x1

    .line 631
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVInstalled() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    return v0

    .line 627
    :catch_0
    move-exception v1

    .line 628
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aa:Ljava/lang/String;

    return-object v0
.end method

.method private i(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3170
    .line 3172
    const-string v0, "\\D+"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3174
    return-object v0
.end method

.method private i()Z
    .locals 4

    .prologue
    .line 637
    const/4 v1, 0x0

    .line 640
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aE:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 641
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isReadyToCall() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 646
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChatONVReadyToCall() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    return v0

    .line 642
    :catch_0
    move-exception v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    .line 643
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 642
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->A:Landroid/widget/ImageView;

    return-object v0
.end method

.method private j(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 3407
    const-string v0, ""

    .line 3409
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/q;->a()Ljava/lang/String;

    move-result-object v0

    .line 3411
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3412
    new-instance v1, Lcom/sec/chaton/util/a;

    invoke-static {v0}, Lcom/sec/chaton/util/a;->b(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v0}, Lcom/sec/chaton/util/a;->c(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/util/a;-><init>([B[B)V

    .line 3416
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/a;->b([B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/a;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3421
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 3422
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "paramBefore: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, ", paramAfter: "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3424
    :cond_0
    return-object v0

    .line 3414
    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Fail in getting a key"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 3417
    :catch_0
    move-exception v0

    .line 3418
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Encryption Error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private j()Z
    .locals 3

    .prologue
    .line 652
    .line 654
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 655
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-nez v0, :cond_0

    .line 656
    const/4 v0, 0x0

    .line 662
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is3GCallAvailable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    return v0

    .line 659
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CALL"

    const-string v2, "tel:+000"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    return-object v0
.end method

.method private k()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 668
    const/4 v0, 0x1

    .line 670
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v2

    .line 671
    const/4 v3, -0x3

    if-eq v3, v2, :cond_0

    const/4 v3, -0x2

    if-ne v3, v2, :cond_1

    .line 672
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0205

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 679
    :cond_1
    return v0
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    return-object v0
.end method

.method private l()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 684
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 688
    :goto_0
    if-eqz v0, :cond_4

    .line 690
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->j()Z

    move-result v3

    .line 691
    if-nez v3, :cond_3

    .line 693
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "phone"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 694
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.SENDTO"

    const-string v6, "smsto:+000"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 700
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "processNoChatONVCase : buddyPhoneNumberAvailable = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 703
    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 705
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processNoChatONVCase : buddyPhoneNumberAvailable2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processNoChatONVCase : buddyPhoneNumberAvailable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " @ isContactShow = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(ZZ)V

    .line 713
    return-void

    :cond_0
    move v0, v2

    .line 684
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 708
    goto :goto_2

    :cond_2
    move v1, v3

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v0

    move v0, v2

    goto :goto_1
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->F:Landroid/widget/ImageView;

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 819
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    if-nez v0, :cond_0

    .line 846
    :goto_0
    return-void

    .line 824
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v0

    if-eqz v0, :cond_1

    .line 827
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showphonenumber : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->af:Z

    if-nez v0, :cond_2

    .line 830
    const-string v0, "refreshCommunicationButtons : is not multi-device"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->l()V

    goto :goto_0

    .line 834
    :cond_2
    const-string v0, "refreshCommunicationButtons : is multi-device"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n()Ljava/util/List;

    move-result-object v0

    .line 837
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 838
    const/4 v0, 0x0

    .line 844
    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Z)V

    goto :goto_0

    .line 841
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ab:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1333
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1334
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v1

    .line 1335
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1336
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, "|"

    invoke-direct {v2, v1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1338
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1339
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 1340
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 1342
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x13

    if-lt v3, v4, :cond_0

    const-string v3, "10"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1343
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chaton id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351
    :goto_1
    const-string v1, ""

    goto :goto_0

    .line 1344
    :cond_0
    const-string v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1345
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pin number : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1347
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "msisdn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1355
    :cond_2
    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    return-object v0
.end method

.method private o()Z
    .locals 3

    .prologue
    .line 1980
    const/4 v0, 0x0

    .line 1982
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1983
    const/4 v0, 0x1

    .line 1986
    :cond_0
    return v0
.end method

.method static synthetic p(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->G:Landroid/widget/ImageView;

    return-object v0
.end method

.method private p()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 2154
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->au:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2155
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ao:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2156
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->an:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2157
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ax:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2158
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aw:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2159
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->av:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2160
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->az:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2161
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ap:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2162
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aq:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2163
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ay:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2165
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->am:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b015f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2166
    return-void
.end method

.method static synthetic q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/fl;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 2990
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u()V

    .line 2991
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 2992
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;)Lcom/sec/chaton/d/a/h;

    .line 2993
    return-void
.end method

.method static synthetic r(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->H:Landroid/widget/ImageView;

    return-object v0
.end method

.method private r()V
    .locals 3

    .prologue
    .line 2996
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u()V

    .line 2998
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    .line 2999
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/f;->a(Ljava/lang/String;I)V

    .line 3000
    return-void
.end method

.method static synthetic s(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m:I

    return v0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 3003
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u()V

    .line 3004
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/f;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/f;

    move-result-object v0

    .line 3005
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/f;->a(Ljava/lang/String;)V

    .line 3006
    return-void
.end method

.method static synthetic t(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private t()V
    .locals 3

    .prologue
    .line 3009
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3011
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u()V

    .line 3012
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i:Landroid/os/Handler;

    invoke-static {v1}, Lcom/sec/chaton/d/n;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/n;

    move-result-object v1

    .line 3013
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3014
    const-string v2, "true"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/d/n;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3015
    return-void
.end method

.method static synthetic u(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->t:Ljava/lang/String;

    return-object v0
.end method

.method private u()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 3031
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aA:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3032
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->au:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3033
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->an:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3034
    return-void
.end method

.method static synthetic v(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/d/i;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o:Lcom/sec/chaton/d/i;

    return-object v0
.end method

.method private v()V
    .locals 2

    .prologue
    .line 3037
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aA:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3039
    return-void
.end method

.method static synthetic w(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    return-object v0
.end method

.method private w()V
    .locals 3

    .prologue
    .line 3062
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3064
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3065
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3066
    const-string v1, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3067
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 3069
    :cond_0
    return-void
.end method

.method static synthetic x(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Lcom/sec/chaton/buddy/a/c;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    return-object v0
.end method

.method static synthetic z(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->af:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1361
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->c()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 1365
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1370
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->c()Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 1371
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1376
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->c()Z

    move-result v0

    if-ne v0, v2, :cond_3

    .line 1377
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1382
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->c()Z

    move-result v0

    if-ne v0, v2, :cond_4

    .line 1383
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1388
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 1389
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1392
    :cond_0
    return-void

    .line 1367
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_0

    .line 1373
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_1

    .line 1379
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_2

    .line 1385
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_3
.end method

.method public a(I)V
    .locals 6

    .prologue
    .line 1437
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    add-int/lit8 v5, p1, -0x1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/f;->a(Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Ljava/lang/String;I)V

    .line 1438
    return-void
.end method

.method public a(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1442
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->X:Z

    if-eqz v0, :cond_1

    .line 1443
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->A:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1444
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1445
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1457
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m:I

    .line 1458
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m:I

    if-nez v0, :cond_1

    .line 1459
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a()V

    .line 1460
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aC:Z

    .line 1463
    :cond_1
    return-void

    .line 1446
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1447
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1448
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_0

    .line 1449
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1450
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1451
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_0

    .line 1452
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1453
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1454
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2241
    .line 2243
    const/4 v0, 0x0

    .line 2245
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u:Ljava/lang/String;

    .line 2247
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2248
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2251
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 2252
    if-eqz v1, :cond_1

    array-length v2, v1

    if-lez v2, :cond_1

    .line 2253
    array-length v2, v1

    if-ge v7, v2, :cond_7

    .line 2254
    aget-object v1, v1, v7

    .line 2255
    const-string v0, "."

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 2256
    invoke-virtual {v1, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2257
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadRandomimage filename : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "### pos : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ### randomId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2258
    const/4 v1, 0x1

    .line 2268
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2269
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2270
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadRandomimage randomId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ### randomFile : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2272
    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 2273
    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 2274
    invoke-direct {p0, v0, p1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2299
    :cond_0
    :goto_1
    return v1

    .line 2280
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 2281
    const-string v0, "Default Coverstory Image download for only MMS user."

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2284
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2285
    const-string v1, "http://cdn.samsungbigdata.com/itemshop/sms/coverstory/pr_480_480_img06.jpg"

    .line 2287
    new-instance v0, Lcom/sec/chaton/poston/a;

    const-string v2, "MMS"

    const-string v4, "dCoverstory.jpg"

    const-string v5, "buddy"

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2288
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    if-nez v1, :cond_3

    .line 2289
    new-instance v1, Lcom/sec/common/f/c;

    invoke-direct {v1}, Lcom/sec/common/f/c;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    .line 2291
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    move v1, v7

    :cond_4
    move v7, v1

    :cond_5
    :goto_2
    move v1, v7

    .line 2299
    goto :goto_1

    .line 2294
    :cond_6
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 2295
    const-string v0, "loadRandomimage No random images in file folder "

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    move v1, v7

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .prologue
    .line 2368
    const/4 v0, 0x0

    .line 2373
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u:Ljava/lang/String;

    .line 2374
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2376
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2377
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 2378
    const-string v1, "checkAndSetCoverStoryRandomImages No random images in file folder "

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413
    :cond_0
    :goto_0
    return v0

    .line 2383
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2384
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2386
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2387
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 2388
    const-string v1, "checkAndSetCoverStoryRandomImages not exists the random image in file folder #2#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2392
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_4

    .line 2393
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 2394
    const-string v1, "checkAndSetCoverStoryRandomImages file size is 0."

    sget-object v3, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2396
    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 2399
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 2400
    const-string v0, "checkAndSetCoverStoryRandomImages get the random image in file folder #3#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2402
    :cond_5
    const/4 v0, 0x1

    .line 2404
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    if-eqz v2, :cond_6

    .line 2405
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 2406
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2408
    :cond_6
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 2409
    const-string v1, "checkAndSetCoverStoryRandomImages set the random image from file folder #4#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Landroid/net/Uri;
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 3207
    .line 3214
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[phoneCall] Original number : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3215
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 3216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[phoneCall] After getPhoneDigitNumber : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3218
    const-string v0, "[phoneCall] Call Number Find Start"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3220
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 3221
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "number"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 3222
    if-eqz v6, :cond_2

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 3223
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3224
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 3225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[phoneCall] ContactSaved id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3227
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "contact_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3229
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v0

    if-lez v0, :cond_2

    .line 3231
    :cond_0
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3232
    const-string v0, "data1"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3233
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3234
    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3235
    invoke-direct {p0, v8, v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3236
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v7

    .line 3242
    :cond_1
    if-eqz v1, :cond_2

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3243
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3250
    :cond_2
    if-eqz v7, :cond_3

    .line 3251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[phoneCall] Call Number Found : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 3256
    :cond_3
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3257
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v0, v7

    .line 3261
    :goto_0
    if-nez v0, :cond_5

    .line 3262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 3263
    const-string v1, "[phoneCall] mContactSaved is false"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3266
    :cond_5
    return-object v0

    .line 3242
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_6

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_6

    .line 3243
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3242
    :cond_6
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 3253
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 3254
    :goto_1
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 3256
    if-eqz v1, :cond_7

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_7

    .line 3257
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    move-object v0, v7

    .line 3259
    goto :goto_0

    .line 3256
    :catchall_1
    move-exception v0

    move-object v6, v7

    :goto_2
    if-eqz v6, :cond_8

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_8

    .line 3257
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3256
    :cond_8
    throw v0

    :catchall_2
    move-exception v0

    goto :goto_2

    :catchall_3
    move-exception v0

    move-object v6, v1

    goto :goto_2

    .line 3253
    :catch_1
    move-exception v0

    move-object v1, v7

    goto :goto_1
.end method

.method public b()V
    .locals 6

    .prologue
    .line 1432
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/content/Context;)Lcom/sec/chaton/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/f;->b(Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Lcom/sec/chaton/buddy/fl;Ljava/lang/String;)V

    .line 1433
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a()V

    .line 1434
    return-void
.end method

.method public b(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1467
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->X:Z

    if-eqz v0, :cond_5

    .line 1468
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->A:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1469
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1470
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 1482
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m:I

    .line 1483
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->m:I

    if-nez v0, :cond_1

    .line 1484
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a()V

    .line 1485
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aC:Z

    .line 1491
    :cond_1
    :goto_1
    return-void

    .line 1471
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1472
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1473
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_0

    .line 1474
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1475
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1476
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_0

    .line 1477
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1478
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/fl;->a(Z)V

    .line 1479
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->a()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto :goto_0

    .line 1488
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->al:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public c(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 3271
    iput-boolean v7, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ad:Z

    .line 3275
    :try_start_0
    const-string v0, "[contactSavedCheck] Contact Saved Find Start"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3277
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 3278
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "number"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 3279
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 3280
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ad:Z

    .line 3282
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ag:I

    .line 3283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[contactSavedCheck] Found in Contact Number : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3284
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[contactSavedCheck] ContactSaved id : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ag:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3290
    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3291
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3295
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ad:Z

    if-nez v0, :cond_2

    .line 3296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[contactSavedCheck] No PhoneNumber in Contact : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3297
    iput v7, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ag:I

    .line 3299
    :cond_2
    return-void

    .line 3287
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 3288
    :goto_1
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3290
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 3291
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3290
    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 3287
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public c()Z
    .locals 5

    .prologue
    const v4, 0x7f0902b8

    const v3, 0x7f0902b7

    .line 1956
    const-string v0, "Buddy didn\'t set Coverstory "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957
    invoke-static {}, Lcom/sec/chaton/e/a/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 1958
    if-eqz v0, :cond_1

    .line 1959
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 1960
    if-nez v1, :cond_0

    .line 1963
    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->v:Ljava/lang/String;

    .line 1964
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o:Lcom/sec/chaton/d/i;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1968
    :cond_0
    const/4 v0, 0x1

    .line 1976
    :goto_0
    return v0

    .line 1972
    :cond_1
    const-string v0, " Random ERROR !!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o:Lcom/sec/chaton/d/i;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/i;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 857
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 858
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 860
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 862
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    .line 864
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, -0x2

    const/4 v1, -0x3

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 882
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1245
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 884
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W:Z

    if-nez v0, :cond_1

    .line 885
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ah:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    sget-object v3, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    const-string v5, "group_relation_group = 1 "

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x:Landroid/widget/ImageView;

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 888
    iput-boolean v8, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W:Z

    goto :goto_0

    .line 891
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ah:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    sget-object v3, Lcom/sec/chaton/e/m;->a:Landroid/net/Uri;

    const-string v4, "group_relation_group = 1 AND group_relation_buddy = ? "

    new-array v5, v8, [Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 892
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->x:Landroid/widget/ImageView;

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 893
    iput-boolean v7, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W:Z

    goto :goto_0

    .line 898
    :pswitch_2
    sput-boolean v8, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 899
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyEditNickNameActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 900
    const-string v1, "PROFILE_ID"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 901
    const-string v1, "PROFILE_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 902
    const-string v1, "PROFILE_BUDDY_MODE"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 903
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 904
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto :goto_0

    .line 909
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileImageView;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 910
    const-string v1, "PROFILE_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 911
    const-string v1, "PROFILE_BUDDY_IMAGE_ID"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 912
    const-string v1, "PROFILE_BUDDY_IMAGE_TYPE"

    const-string v2, "FULL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 913
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 914
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 918
    :pswitch_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    .line 919
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profileImageId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyProfileImageView;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 922
    const-string v2, "PROFILE_BUDDY_NO"

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 923
    const-string v2, "PROFILE_BUDDY_IMAGE_ID"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 924
    const-string v0, "PROFILE_BUDDY_IMAGE_TYPE"

    const-string v2, "THUMB"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 925
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 926
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 930
    :pswitch_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    .line 931
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profileImageId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyProfileImageView;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 934
    const-string v2, "PROFILE_BUDDY_NO"

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 935
    const-string v2, "PROFILE_BUDDY_IMAGE_ID"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 936
    const-string v0, "PROFILE_BUDDY_IMAGE_TYPE"

    const-string v2, "THUMB"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 937
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 938
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 942
    :pswitch_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O:Lcom/sec/chaton/buddy/fl;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/fl;->b()Ljava/lang/String;

    move-result-object v0

    .line 943
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profileImageId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyProfileImageView;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 946
    const-string v2, "PROFILE_BUDDY_NO"

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 947
    const-string v2, "PROFILE_BUDDY_IMAGE_ID"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 948
    const-string v0, "PROFILE_BUDDY_IMAGE_TYPE"

    const-string v2, "THUMB"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 949
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 950
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 954
    :pswitch_7
    sput-boolean v8, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 955
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 956
    const-string v1, "PROFILE_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 957
    const-string v1, "PROFILE_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 958
    const-string v1, "PROFILE_BUDDY_PROFILE_LOAD_DONE"

    iget-boolean v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aB:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 959
    const-string v1, "PROFILE_BUDDY_PROFILE_IMAGE_LOAD_DONE"

    iget-boolean v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aC:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 960
    const-string v1, "PROFILE_BUDDY_PROFILE_IMAGE_COUNT"

    iget v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aD:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 961
    const-string v1, "PROFILE_BUDDY_FROM_BUDDYDIALOG"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 962
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 964
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 965
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 970
    :pswitch_8
    sput-boolean v8, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 971
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 972
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 973
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 974
    const-string v1, "receivers"

    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 975
    const-string v1, "showPhoneNumber"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 976
    const-string v1, "extraInfo"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 977
    const-string v1, "msisdns"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 978
    const-string v1, "is_buddy"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 979
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 980
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 981
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 985
    :pswitch_9
    sput-boolean v8, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 988
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->af:Z

    if-nez v0, :cond_5

    .line 989
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 990
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 991
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no multi [1] Call Number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call Number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1009
    invoke-static {v8}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 1010
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    .line 1012
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1013
    :catch_0
    move-exception v0

    .line 1014
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 994
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 995
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 996
    if-nez v0, :cond_3

    .line 997
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 999
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no multi [2] Call Number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1001
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1002
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no multi [3] Call Number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1017
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n()Ljava/util/List;

    move-result-object v0

    .line 1018
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v8, :cond_8

    .line 1019
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1020
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1021
    if-nez v0, :cond_6

    .line 1022
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1024
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[r2d209] multi [1] Call Number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call Number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1033
    invoke-static {v8}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 1034
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    .line 1036
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1037
    :catch_1
    move-exception v0

    .line 1038
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1026
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tel:+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1027
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[r2d209] multi [2] Call Number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1040
    :cond_8
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v8, :cond_0

    .line 1041
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1042
    const-string v1, "PN_DIALOG_BUDDY_TYPE"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1043
    const-string v1, "PN_DIALOG_BUDDY_MSISDNS"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1044
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 1045
    invoke-static {v8}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 1046
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 1053
    :pswitch_a
    sput-boolean v8, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 1054
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 1055
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RAWCONTACTID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mContactId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    invoke-static {v8}, Lcom/sec/chaton/util/p;->b(Z)V

    .line 1057
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->af:Z

    if-nez v0, :cond_b

    .line 1058
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ae:Z

    if-eqz v0, :cond_9

    .line 1059
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1061
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1062
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1064
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1068
    :cond_b
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n()Ljava/util/List;

    move-result-object v1

    .line 1069
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_d

    .line 1070
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "+"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1071
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1073
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1075
    :cond_d
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v8, :cond_0

    .line 1076
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1077
    const-string v1, "PN_DIALOG_BUDDY_TYPE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1078
    const-string v1, "PN_DIALOG_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1079
    const-string v1, "PN_DIALOG_BUDDY_MSISDNS"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1081
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 1082
    const-string v1, "PN_DIALOG_BUDDY_SAMSUNGEMAIL"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1085
    :cond_e
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 1086
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 1094
    :pswitch_b
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1095
    if-eq v1, v0, :cond_f

    if-ne v2, v0, :cond_10

    .line 1096
    :cond_f
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1099
    :cond_10
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->q()V

    goto/16 :goto_0

    .line 1104
    :pswitch_c
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1105
    if-eq v1, v0, :cond_11

    if-ne v2, v0, :cond_12

    .line 1106
    :cond_11
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1109
    :cond_12
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->r()V

    goto/16 :goto_0

    .line 1114
    :pswitch_d
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1115
    if-eq v1, v0, :cond_13

    if-ne v2, v0, :cond_14

    .line 1116
    :cond_13
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1119
    :cond_14
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0209

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0247

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    aput-object v5, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/dialog/a;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dialog/a;-><init>(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1130
    :pswitch_e
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1131
    if-eq v1, v0, :cond_15

    if-ne v2, v0, :cond_16

    .line 1132
    :cond_15
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1135
    :cond_16
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s()V

    goto/16 :goto_0

    .line 1141
    :pswitch_f
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/SpamConfirmDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1142
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 1143
    iget-boolean v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->as:Z

    if-eqz v1, :cond_17

    .line 1144
    const-string v1, "SCD_DIALOG_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1152
    :goto_3
    const-string v1, "SCD_DIALOG_BUDDY_NO"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1153
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 1154
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 1146
    :cond_17
    const-string v1, "SCD_DIALOG_BUDDY_NAME"

    const v2, 0x7f0b00b4

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    .line 1149
    :cond_18
    const-string v1, "SCD_DIALOG_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    .line 1159
    :pswitch_10
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ar:Z

    if-ne v0, v8, :cond_19

    .line 1160
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1161
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1163
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/chaton/d/a/ck;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1166
    :try_start_2
    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 1175
    :cond_19
    :goto_4
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 1167
    :catch_2
    move-exception v0

    .line 1168
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    .line 1169
    :catch_3
    move-exception v0

    .line 1170
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_4

    .line 1171
    :catch_4
    move-exception v0

    .line 1172
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 1179
    :pswitch_11
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 1183
    :pswitch_12
    sput-boolean v8, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 1184
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 1185
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1186
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 1211
    :cond_1a
    :goto_5
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 1188
    :cond_1b
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->g()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1190
    :try_start_3
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChatON V call id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1192
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChatON V call name : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Push Name"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1193
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aE:Lcom/coolots/sso/a/a;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v4, "Push Name"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1195
    if-eqz v0, :cond_1a

    .line 1196
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_5

    .line 1202
    :catch_5
    move-exception v0

    .line 1203
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1207
    :cond_1c
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1208
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_5

    .line 1215
    :pswitch_13
    sput-boolean v8, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 1216
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1217
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1218
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    .line 1242
    :cond_1d
    :goto_6
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->finish()V

    goto/16 :goto_0

    .line 1220
    :cond_1e
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->g()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->i()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1222
    :try_start_4
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChatON V call id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->p:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChatON V call name : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Push Name"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aE:Lcom/coolots/sso/a/a;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v4, "Push Name"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1226
    if-eqz v0, :cond_1d

    .line 1227
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    goto/16 :goto_6

    .line 1233
    :catch_6
    move-exception v0

    .line 1234
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1238
    :cond_1f
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1239
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_6

    .line 882
    nop

    :pswitch_data_0
    .packed-switch 0x7f0700ab
        :pswitch_1
        :pswitch_0
        :pswitch_7
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_3
        :pswitch_8
        :pswitch_9
        :pswitch_12
        :pswitch_13
        :pswitch_a
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_10
        :pswitch_b
        :pswitch_0
        :pswitch_b
        :pswitch_f
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 869
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 870
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->X:Z

    if-ne v0, v1, :cond_0

    .line 871
    invoke-direct {p0, v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a(Z)V

    .line 873
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 244
    const-string v0, "onCreate()"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 246
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "imei"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 250
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?uid="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "&"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "param"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Z:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    :cond_0
    :goto_0
    invoke-virtual {p0, v6}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->requestWindowFeature(I)Z

    .line 258
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 259
    invoke-virtual {p0, v7}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->setFinishOnTouchOutside(Z)V

    .line 262
    :cond_1
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->h:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ah:Lcom/sec/chaton/e/a/u;

    .line 263
    invoke-static {p0, v2, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ac:Landroid/widget/Toast;

    .line 264
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUDDY_DIALOG_BUDDY_NO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    .line 266
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUDDY_DIALOG_BUDDY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e:Ljava/lang/String;

    .line 267
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUDDY_DIALOG_BUDDY_FROM_SUGGESTION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ar:Z

    .line 269
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUDDY_DIALOG_BUDDY_FROM_PROFILE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->as:Z

    .line 270
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->as:Z

    if-eqz v0, :cond_2

    .line 271
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 272
    const-string v0, " Buddy dialog is called from buddy profile fragment !!! "

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d()V

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ah:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x3

    invoke-static {}, Lcom/sec/chaton/e/i;->c()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "buddy_no = ? "

    new-array v6, v6, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/random/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u:Ljava/lang/String;

    .line 280
    new-instance v0, Lcom/sec/chaton/d/i;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aF:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/i;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o:Lcom/sec/chaton/d/i;

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o:Lcom/sec/chaton/d/i;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;)V

    .line 282
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    .line 283
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aF:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n:Lcom/sec/chaton/d/w;

    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->b:Ljava/lang/String;

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->u:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c:Ljava/lang/String;

    .line 287
    return-void

    .line 251
    :catch_0
    move-exception v0

    .line 252
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 253
    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3326
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aF:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 3327
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aF:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3328
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aF:Landroid/os/Handler;

    .line 3331
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ah:Lcom/sec/chaton/e/a/u;

    if-eqz v0, :cond_1

    .line 3332
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->ah:Lcom/sec/chaton/e/a/u;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/a/u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3335
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o:Lcom/sec/chaton/d/i;

    if-eqz v0, :cond_2

    .line 3336
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->c()V

    .line 3337
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->g()V

    .line 3338
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->i()V

    .line 3339
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->o:Lcom/sec/chaton/d/i;

    .line 3343
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 3344
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3345
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->s:Landroid/widget/ImageView;

    .line 3348
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    if-eqz v0, :cond_4

    .line 3349
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    invoke-virtual {v0, v2}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 3350
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 3353
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n:Lcom/sec/chaton/d/w;

    if-eqz v0, :cond_5

    .line 3354
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->f()V

    .line 3355
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->b()V

    .line 3356
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->n:Lcom/sec/chaton/d/w;

    .line 3359
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->A:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 3360
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->A:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3361
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->A:Landroid/widget/ImageView;

    .line 3364
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->B:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    .line 3365
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->B:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3366
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->B:Landroid/widget/ImageView;

    .line 3369
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->C:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 3370
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->C:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3371
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->C:Landroid/widget/ImageView;

    .line 3374
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->D:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    .line 3375
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->D:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3376
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->D:Landroid/widget/ImageView;

    .line 3379
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I:Landroid/widget/ImageButton;

    if-eqz v0, :cond_a

    .line 3380
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3381
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I:Landroid/widget/ImageButton;

    .line 3382
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_a

    .line 3383
    const-string v0, "[BuddyDialog] moreImgs is null "

    sget-object v1, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3387
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aj:Landroid/widget/ImageView;

    if-eqz v0, :cond_b

    .line 3388
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aj:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3389
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->aj:Landroid/widget/ImageView;

    .line 3392
    :cond_b
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 3393
    iput-object v2, p0, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->w:Landroid/content/Context;

    .line 3394
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 3313
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 3314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3321
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 3043
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 3045
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->w()V

    .line 3046
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 3303
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 3304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStart, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3309
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 3050
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3053
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 3054
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 3059
    :goto_0
    return-void

    .line 3056
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
