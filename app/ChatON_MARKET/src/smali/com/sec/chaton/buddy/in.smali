.class Lcom/sec/chaton/buddy/in;
.super Ljava/lang/Object;
.source "SpecialBuddyRecommendListFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 286
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 276
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->i(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 246
    if-nez p3, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 249
    :cond_0
    :goto_0
    if-eqz p3, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 250
    new-instance v0, Lcom/sec/chaton/buddy/io;

    const-string v1, "buddy_no"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "buddy_name"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/buddy/io;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget-object v1, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;I)I

    .line 252
    iget-object v1, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)I

    move-result v1

    if-nez v1, :cond_2

    .line 253
    iget-object v1, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 254
    iget-object v1, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ListView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 255
    iget-object v1, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->e(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 261
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->f(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 257
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 259
    iget-object v1, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/chaton/buddy/io;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_1

    .line 264
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Lcom/sec/chaton/buddy/ip;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/buddy/in;->a:Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Lcom/sec/chaton/buddy/ip;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/ip;->notifyDataSetChanged()V

    .line 268
    :cond_4
    if-eqz p3, :cond_5

    .line 269
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 271
    :cond_5
    return-void
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 281
    return-void
.end method
