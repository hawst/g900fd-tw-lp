.class Lcom/sec/chaton/buddy/ax;
.super Landroid/os/Handler;
.source "BuddyEditNickNameFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 402
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 404
    iget-object v0, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 410
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_2

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    iget-object v0, v0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    const v1, 0x7f0b0089

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 416
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 417
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 419
    iget-object v1, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 420
    const-string v2, "PROFILE_BUDDY_RENAME"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    iget-object v2, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->f(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/at;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v1, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 423
    iget-object v1, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 428
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0122

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0139

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/ay;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/ay;-><init>(Lcom/sec/chaton/buddy/ax;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ax;->a:Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0
.end method
