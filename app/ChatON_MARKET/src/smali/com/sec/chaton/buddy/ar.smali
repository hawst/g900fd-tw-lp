.class public Lcom/sec/chaton/buddy/ar;
.super Ljava/lang/Object;
.source "BuddyAdapter.java"


# instance fields
.field a:Landroid/widget/CheckBox;

.field b:Landroid/widget/ImageView;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Landroid/view/ViewGroup;

.field f:Landroid/widget/ImageView;

.field g:Landroid/widget/FrameLayout;

.field h:Landroid/widget/FrameLayout;

.field i:Landroid/widget/ImageView;

.field j:Landroid/widget/ImageView;

.field k:Landroid/widget/ImageView;

.field l:Landroid/widget/TableLayout;

.field m:Landroid/widget/LinearLayout;

.field n:Landroid/view/View;

.field o:Landroid/view/View;

.field p:Landroid/widget/TableLayout;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, -0x1

    const/16 v4, 0x8

    const/4 v3, -0x2

    const/4 v2, 0x0

    .line 1197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1198
    iput-object p1, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    .line 1201
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const v1, 0x7f070016

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->l:Landroid/widget/TableLayout;

    .line 1202
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->b:Landroid/widget/ImageView;

    .line 1203
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->c:Landroid/widget/TextView;

    .line 1204
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    .line 1206
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1207
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1213
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1214
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1216
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1217
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0702ea

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    .line 1219
    new-instance v0, Landroid/widget/TableRow$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 1220
    const/16 v1, 0x15

    iput v1, v0, Landroid/widget/TableRow$LayoutParams;->gravity:I

    .line 1221
    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1223
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const v1, 0x7f070016

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->p:Landroid/widget/TableLayout;

    .line 1224
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->p:Landroid/widget/TableLayout;

    invoke-virtual {v0, v6}, Landroid/widget/TableLayout;->setGravity(I)V

    .line 1226
    new-instance v0, Landroid/widget/CheckBox;

    invoke-direct {v0, p2}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    .line 1227
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 1228
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 1229
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 1231
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    .line 1232
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    .line 1233
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->k:Landroid/widget/ImageView;

    .line 1234
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->f:Landroid/widget/ImageView;

    .line 1236
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1237
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/chaton/buddy/ar;->m:Landroid/widget/LinearLayout;

    .line 1238
    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->m:Landroid/widget/LinearLayout;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1240
    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1241
    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    const v2, 0x7f0200ef

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1242
    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1244
    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1245
    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    const v2, 0x7f0200ed

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1246
    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1248
    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->k:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1249
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->k:Landroid/widget/ImageView;

    const v1, 0x7f0200e7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1250
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1252
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->m:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1253
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->m:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1254
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->m:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1260
    iget-object v6, p0, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ar;->k:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/chaton/buddy/ar;->f:Landroid/widget/ImageView;

    move-object v0, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/buddy/ag;->a(Landroid/content/Context;Landroid/widget/CheckBox;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1288
    :goto_1
    return-void

    .line 1210
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    goto/16 :goto_0

    .line 1263
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0704b2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    .line 1264
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0704b4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->h:Landroid/widget/FrameLayout;

    .line 1269
    new-instance v0, Landroid/widget/CheckBox;

    invoke-direct {v0, p2}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    .line 1270
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 1271
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 1272
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1273
    iput v6, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1274
    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1276
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    .line 1277
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    .line 1278
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->k:Landroid/widget/ImageView;

    .line 1280
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->g:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1282
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->h:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ar;->i:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/ar;->j:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ar;->k:Landroid/widget/ImageView;

    invoke-static {p2, v1, v2, v3}, Lcom/sec/chaton/buddy/ag;->a(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1284
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0704b5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->n:Landroid/view/View;

    .line 1285
    iget-object v0, p0, Lcom/sec/chaton/buddy/ar;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0702e4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/ar;->o:Landroid/view/View;

    goto :goto_1
.end method
