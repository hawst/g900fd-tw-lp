.class public Lcom/sec/chaton/buddy/dialog/MeDialog;
.super Landroid/app/Activity;
.source "MeDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/ImageView;

.field private B:Landroid/widget/ImageView;

.field private C:Landroid/widget/ImageButton;

.field private D:Lcom/sec/chaton/d/i;

.field private E:Landroid/widget/ImageView;

.field private F:Landroid/graphics/drawable/BitmapDrawable;

.field private final G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Lcom/sec/chaton/buddy/fl;

.field private J:Lcom/sec/chaton/buddy/fl;

.field private K:Lcom/sec/chaton/buddy/fl;

.field private L:Lcom/sec/chaton/buddy/fl;

.field private M:Landroid/widget/LinearLayout;

.field private N:Z

.field private O:Lcom/coolots/sso/a/a;

.field private P:I

.field private Q:Landroid/os/Handler;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/io/File;

.field private i:Lcom/sec/chaton/d/w;

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/ProfileImage;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:Lcom/sec/common/f/c;

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/LinearLayout;

.field private s:Landroid/widget/ImageView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/ImageView;

.field private v:Landroid/widget/ImageView;

.field private w:Landroid/widget/ImageView;

.field private x:Landroid/widget/ImageView;

.field private y:Landroid/widget/ImageView;

.field private z:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/sec/chaton/buddy/dialog/MeDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    .line 89
    const-string v0, "160"

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->d:Ljava/lang/String;

    .line 90
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->e:I

    .line 113
    const-string v0, "updated"

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->G:Ljava/lang/String;

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->H:Ljava/lang/String;

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->N:Z

    .line 120
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->P:I

    .line 519
    new-instance v0, Lcom/sec/chaton/buddy/dialog/u;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/u;-><init>(Lcom/sec/chaton/buddy/dialog/MeDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->Q:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/MeDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->H:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/MeDialog;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->j:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 884
    packed-switch p1, :pswitch_data_0

    .line 913
    :goto_0
    return-void

    .line 887
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->u:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020387

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 888
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 889
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/bt;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 910
    :catch_0
    move-exception v0

    .line 911
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 893
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 894
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 898
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 899
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 903
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 904
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 884
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(ILcom/sec/chaton/io/entry/inner/ProfileImage;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 945
    packed-switch p1, :pswitch_data_0

    .line 958
    :goto_0
    return-void

    .line 947
    :pswitch_0
    const-string v3, "profile_small_image1"

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V

    goto :goto_0

    .line 951
    :pswitch_1
    const-string v3, "profile_small_image2"

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V

    goto :goto_0

    .line 955
    :pswitch_2
    const-string v3, "profile_small_image3"

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V

    goto :goto_0

    .line 945
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V
    .locals 8

    .prologue
    .line 917
    .line 918
    if-eqz p5, :cond_3

    .line 919
    const-string v3, "profile_f_"

    .line 925
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mine_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 926
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(ILjava/lang/String;)V

    .line 927
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 928
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, p3, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 930
    :cond_0
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-static {p3, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 932
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->z:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p2

    move-object v5, p4

    move v6, p5

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Landroid/widget/ImageView;Lcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;ZZ)V

    .line 936
    :cond_1
    if-eqz p5, :cond_2

    .line 937
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->b(Ljava/lang/String;)V

    .line 939
    :cond_2
    return-void

    .line 922
    :cond_3
    const-string v3, "profile_t_"

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 448
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MyPageFragment initialize() - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 451
    const/4 v0, 0x0

    .line 453
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "photoFile="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    packed-switch p1, :pswitch_data_0

    move-object v1, v0

    .line 480
    :goto_0
    if-eqz v1, :cond_0

    .line 482
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 484
    if-nez p1, :cond_1

    .line 485
    new-instance v0, Lcom/sec/chaton/userprofile/ck;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/chaton/userprofile/ck;-><init>(Ljava/io/File;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 490
    :goto_1
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->l:Lcom/sec/common/f/c;

    invoke-virtual {v2, v1, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 493
    :cond_0
    return-void

    .line 458
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->u:Landroid/widget/ImageView;

    .line 459
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "profile_small_image0"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-object v1, v0

    .line 460
    goto :goto_0

    .line 462
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    .line 463
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "profile_small_image1"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-object v1, v0

    .line 464
    goto :goto_0

    .line 467
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    .line 468
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "profile_small_image2"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-object v1, v0

    .line 469
    goto :goto_0

    .line 472
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    .line 473
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "profile_small_image3"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-object v1, v0

    .line 474
    goto :goto_0

    .line 488
    :cond_1
    new-instance v0, Lcom/sec/chaton/userprofile/ck;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/chaton/userprofile/ck;-><init>(Ljava/io/File;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    goto :goto_1

    .line 456
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 5

    .prologue
    .line 1020
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->f()V

    .line 1027
    const-string v0, "mycoverstory.jpg"

    .line 1028
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "coverstory_metaid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 1030
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " setCoverstoryAndDownload [dirInternalPath] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " [filename] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->g:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1033
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1034
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1035
    :cond_0
    const-string v0, "setCoverStory there is no mycoverstory.jpg loadDefaultCoverStory()"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->E:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/widget/ImageView;)Z

    .line 1072
    :goto_0
    return-void

    .line 1042
    :cond_1
    const-string v0, "setCoverStory"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    if-eqz p1, :cond_2

    .line 1065
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/mycoverstory.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_0

    .line 1067
    :cond_2
    const-string v0, "loadCoverStoryImages bitmap is null. loadDefaultCoverStory()"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->E:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/widget/ImageView;)Z

    goto :goto_0
.end method

.method private a(Landroid/widget/ImageView;Lcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;ZZ)V
    .locals 10

    .prologue
    .line 999
    const/4 v1, 0x1

    move/from16 v0, p7

    if-ne v0, v1, :cond_0

    .line 1000
    new-instance v1, Lcom/sec/chaton/buddy/fk;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&size=800"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x258

    const/16 v5, 0x258

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "mine_"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v2, p1

    move-object v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/sec/chaton/buddy/fk;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;ZZ)V

    .line 1007
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->l:Lcom/sec/common/f/c;

    invoke-virtual {v2, p5, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1008
    return-void

    .line 1004
    :cond_0
    new-instance v1, Lcom/sec/chaton/buddy/fk;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&size=140"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x258

    const/16 v5, 0x258

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "mine_"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v2, p1

    move-object v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/sec/chaton/buddy/fk;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/MeDialog;I)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/MeDialog;ILcom/sec/chaton/io/entry/inner/ProfileImage;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/MeDialog;ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V
    .locals 0

    .prologue
    .line 76
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/MeDialog;Lcom/sec/chaton/io/entry/inner/BuddyProfile;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Lcom/sec/chaton/io/entry/inner/BuddyProfile;)V

    return-void
.end method

.method private a(Lcom/sec/chaton/io/entry/inner/BuddyProfile;)V
    .locals 2

    .prologue
    .line 505
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/BuddyProfile;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->p:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/sec/chaton/io/entry/inner/BuddyProfile;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 509
    :cond_0
    const v0, 0x7f0704d9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->t:Landroid/widget/TextView;

    .line 510
    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/BuddyProfile;->status:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/sec/chaton/io/entry/inner/BuddyProfile;->status:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->t:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/sec/chaton/io/entry/inner/BuddyProfile;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 517
    :goto_0
    return-void

    .line 515
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->t:Landroid/widget/TextView;

    const v1, 0x7f0b0202

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x2

    .line 160
    const v0, 0x7f030131

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->setContentView(I)V

    .line 162
    const v0, 0x7f0704d4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 163
    const-string v1, "window"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 165
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getOrientation()I

    move-result v2

    .line 166
    if-eqz v2, :cond_0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 167
    :cond_0
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-double v3, v1

    const-wide v5, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v3, v5

    double-to-int v1, v3

    invoke-direct {v2, v1, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    :goto_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v9, :cond_1

    .line 173
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030003"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 176
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->k:Ljava/lang/String;

    .line 178
    const v0, 0x7f0704d5

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->p:Landroid/widget/TextView;

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->n:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    :cond_2
    const v0, 0x7f0704d7

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->q:Landroid/widget/LinearLayout;

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    const v0, 0x7f0704d6

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->r:Landroid/widget/LinearLayout;

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    const v0, 0x7f0704d8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->E:Landroid/widget/ImageView;

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->E:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Landroid/widget/ImageView;)V

    .line 203
    const v0, 0x7f0704d9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->t:Landroid/widget/TextView;

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->o:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    :goto_1
    const v0, 0x7f0700b1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->s:Landroid/widget/ImageView;

    .line 213
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->O:Lcom/coolots/sso/a/a;

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->O:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v9, :cond_7

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 222
    :goto_2
    const v0, 0x7f0704da

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->u:Landroid/widget/ImageView;

    .line 223
    const v0, 0x7f0704db

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->y:Landroid/widget/ImageView;

    .line 225
    const v0, 0x7f0704dd

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    .line 226
    const v0, 0x7f0704de

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->z:Landroid/widget/ImageView;

    .line 227
    const v0, 0x7f0704df

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    .line 228
    const v0, 0x7f0704e0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->A:Landroid/widget/ImageView;

    .line 229
    const v0, 0x7f0704e1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    .line 230
    const v0, 0x7f0704e2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->B:Landroid/widget/ImageView;

    .line 231
    const v0, 0x7f0704e3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->C:Landroid/widget/ImageButton;

    .line 233
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->u:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->I:Lcom/sec/chaton/buddy/fl;

    .line 234
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->J:Lcom/sec/chaton/buddy/fl;

    .line 235
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->K:Lcom/sec/chaton/buddy/fl;

    .line 236
    new-instance v0, Lcom/sec/chaton/buddy/fl;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Lcom/sec/chaton/buddy/fl;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->L:Lcom/sec/chaton/buddy/fl;

    .line 238
    const v0, 0x7f0704dc

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->M:Landroid/widget/LinearLayout;

    .line 239
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->c()V

    .line 241
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 242
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 244
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 246
    if-nez p1, :cond_3

    .line 247
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->e()V

    .line 248
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->d()V

    .line 253
    :cond_3
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->P:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->C:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->C:Landroid/widget/ImageButton;

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->C:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    :cond_4
    return-void

    .line 169
    :cond_5
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-double v3, v1

    const-wide v5, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v3, v5

    double-to-int v1, v3

    invoke-direct {v2, v1, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 209
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->t:Landroid/widget/TextView;

    const v1, 0x7f0b0202

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 219
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->s:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/MeDialog;Z)Z
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/MeDialog;I)I
    .locals 0

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->P:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/MeDialog;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->C:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 962
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 963
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 964
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 965
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "msisdn"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 967
    const-string v3, "profile_image_status"

    const-string v4, "updated"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "profile_t_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mine_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 973
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "profile_f_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "mine_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 976
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "myprofile.png_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 977
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ".jpeg_"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 980
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v5

    const-string v6, "profile_small_image0"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    .line 982
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "changed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "profile_small_image0"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 985
    :cond_0
    invoke-static {v3, v1}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    invoke-static {v0, v4}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 987
    return-void
.end method

.method private b()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 344
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->h:Ljava/io/File;

    const-string v3, "myprofile.png_"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 345
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 377
    :cond_0
    :goto_0
    return v0

    .line 351
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "profile_small_image1"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 352
    if-eqz v1, :cond_2

    .line 353
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "profile_t_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mine_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 354
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 360
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "profile_small_image2"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 361
    if-eqz v1, :cond_3

    .line 362
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "profile_t_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mine_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 363
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 369
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "profile_small_image3"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 370
    if-eqz v1, :cond_4

    .line 371
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "profile_t_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mine_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 372
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 377
    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/dialog/MeDialog;)I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->m:I

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/dialog/MeDialog;I)I
    .locals 0

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->m:I

    return p1
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1132
    const-string v0, ""

    .line 1134
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/q;->a()Ljava/lang/String;

    move-result-object v0

    .line 1136
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1137
    new-instance v1, Lcom/sec/chaton/util/a;

    invoke-static {v0}, Lcom/sec/chaton/util/a;->b(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v0}, Lcom/sec/chaton/util/a;->c(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/util/a;-><init>([B[B)V

    .line 1141
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/a;->b([B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/a;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1146
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 1147
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "paramBefore: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, ", paramAfter: "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    :cond_0
    return-object v0

    .line 1139
    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Fail in getting a key"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1142
    :catch_0
    move-exception v0

    .line 1143
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Encryption Error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f020344

    const/4 v4, 0x0

    .line 382
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->N:Z

    .line 384
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->N:Z

    if-eqz v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->M:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 392
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_small_image0"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 393
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "profile_f_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mine_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v4, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(ILjava/lang/String;)V

    .line 397
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_small_image1"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 399
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "profile_t_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mine_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(ILjava/lang/String;)V

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 410
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_small_image2"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 411
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 412
    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "profile_t_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mine_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(ILjava/lang/String;)V

    .line 413
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 423
    :goto_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_small_image3"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 424
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 425
    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "profile_t_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mine_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(ILjava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 435
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 436
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->C:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->C:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 404
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 417
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 419
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    .line 430
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_3
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->b:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->i:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->d()V

    .line 497
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->g:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 500
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->i:Lcom/sec/chaton/d/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->c(Ljava/lang/String;)V

    .line 501
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->k:Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->F:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 1012
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->F:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1013
    if-eqz v0, :cond_0

    .line 1014
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1017
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/dialog/MeDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->u:Landroid/widget/ImageView;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1082
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1085
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1086
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1087
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1088
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/MeDialog;->startActivity(Landroid/content/Intent;)V

    .line 1090
    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/dialog/MeDialog;)Lcom/sec/chaton/d/i;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/dialog/MeDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->E:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/dialog/MeDialog;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->l:Lcom/sec/common/f/c;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/dialog/MeDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->H:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 5

    .prologue
    const v4, 0x7f0902b8

    const v3, 0x7f0902b7

    .line 864
    const-string v0, "Buddy didn\'t set Coverstory "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    invoke-static {}, Lcom/sec/chaton/e/a/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 866
    if-eqz v0, :cond_1

    .line 867
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Ljava/lang/String;)Z

    move-result v1

    .line 868
    if-nez v1, :cond_0

    .line 869
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->H:Ljava/lang/String;

    .line 874
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/d/i;->b(Ljava/lang/String;)V

    .line 875
    const/4 v0, 0x1

    .line 879
    :goto_0
    return v0

    .line 877
    :cond_1
    const-string v0, " Coverstory Random ERROR !!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/i;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 7

    .prologue
    .line 819
    const/4 v0, 0x0

    .line 824
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->g:Ljava/lang/String;

    .line 825
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 826
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 827
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 828
    const-string v1, "checkAndSetCoverStoryRandomImages No random images in file folder "

    sget-object v2, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    :cond_0
    :goto_0
    return v0

    .line 833
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 834
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 835
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 836
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 837
    const-string v1, "checkAndSetCoverStoryRandomImages not exists the random image in file folder #2#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 841
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_4

    .line 842
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 843
    const-string v1, "checkAndSetCoverStoryRandomImages file size is 0."

    sget-object v3, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 848
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 849
    const-string v0, "checkAndSetCoverStoryRandomImages get the random image in file folder #3#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    :cond_5
    const/4 v0, 0x1

    .line 852
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->E:Landroid/widget/ImageView;

    if-eqz v2, :cond_6

    .line 853
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->E:Landroid/widget/ImageView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 856
    :cond_6
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 857
    const-string v1, "checkAndSetCoverStoryRandomImages set the random image from file folder #4#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 261
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 262
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 264
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->finish()V

    .line 268
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 280
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    :goto_0
    return-void

    .line 286
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 333
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 334
    const-string v1, "callMyPageTab"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 335
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->startActivity(Landroid/content/Intent;)V

    .line 336
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->finish()V

    goto :goto_0

    .line 288
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/userprofile/EditProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 291
    const-string v1, "INTENT_FROM"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 293
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->startActivity(Landroid/content/Intent;)V

    .line 294
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->finish()V

    goto :goto_0

    .line 305
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 306
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->startActivity(Landroid/content/Intent;)V

    .line 307
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->finish()V

    goto :goto_0

    .line 312
    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 313
    const-string v1, "mSmallImageNum"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->startActivity(Landroid/content/Intent;)V

    .line 315
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->finish()V

    goto :goto_0

    .line 319
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 320
    const-string v1, "mSmallImageNum"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 321
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->startActivity(Landroid/content/Intent;)V

    .line 322
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->finish()V

    goto :goto_0

    .line 326
    :pswitch_6
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 327
    const-string v1, "mSmallImageNum"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 328
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->startActivity(Landroid/content/Intent;)V

    .line 329
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->finish()V

    goto/16 :goto_0

    .line 286
    nop

    :pswitch_data_0
    .packed-switch 0x7f0704d6
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 273
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 274
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Z)V

    .line 275
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 125
    const-string v0, "onCreate()"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 127
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 131
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "?uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "param"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_0
    :goto_0
    new-instance v0, Lcom/sec/chaton/d/i;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->Q:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/i;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->a()V

    .line 141
    invoke-virtual {p0, v6}, Lcom/sec/chaton/buddy/dialog/MeDialog;->requestWindowFeature(I)Z

    .line 142
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 143
    invoke-virtual {p0, v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->setFinishOnTouchOutside(Z)V

    .line 146
    :cond_1
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->Q:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->i:Lcom/sec/chaton/d/w;

    .line 147
    iput-boolean v6, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->f:Z

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->g:Ljava/lang/String;

    .line 149
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->l:Lcom/sec/common/f/c;

    .line 151
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ME_DIALOG_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->n:Ljava/lang/String;

    .line 152
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ME_DIALOG_STATUSMSG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->o:Ljava/lang/String;

    .line 154
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->h:Ljava/io/File;

    .line 156
    invoke-direct {p0, v5}, Lcom/sec/chaton/buddy/dialog/MeDialog;->a(Z)V

    .line 157
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 133
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 134
    sget-object v1, Lcom/sec/chaton/buddy/dialog/MeDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1094
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1096
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->u:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1097
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1098
    iput-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->u:Landroid/widget/ImageView;

    .line 1101
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 1102
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1103
    iput-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->v:Landroid/widget/ImageView;

    .line 1106
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 1107
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1108
    iput-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->w:Landroid/widget/ImageView;

    .line 1111
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1112
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1113
    iput-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->x:Landroid/widget/ImageView;

    .line 1116
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->C:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4

    .line 1117
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->C:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1118
    iput-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->C:Landroid/widget/ImageButton;

    .line 1121
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    if-eqz v0, :cond_5

    .line 1122
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->b()V

    .line 1123
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->g()V

    .line 1124
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->h()V

    .line 1125
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->i()V

    .line 1126
    iput-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->D:Lcom/sec/chaton/d/i;

    .line 1128
    :cond_5
    iput-object v1, p0, Lcom/sec/chaton/buddy/dialog/MeDialog;->Q:Landroid/os/Handler;

    .line 1129
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 1076
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1078
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/MeDialog;->g()V

    .line 1079
    return-void
.end method
