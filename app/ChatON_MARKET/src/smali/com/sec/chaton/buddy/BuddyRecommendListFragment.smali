.class public Lcom/sec/chaton/buddy/BuddyRecommendListFragment;
.super Landroid/support/v4/app/Fragment;
.source "BuddyRecommendListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private A:Landroid/view/View$OnClickListener;

.field private B:Landroid/view/View$OnClickListener;

.field private C:Landroid/view/View$OnClickListener;

.field private D:Landroid/os/Handler;

.field private E:Landroid/os/Handler;

.field a:Landroid/database/ContentObserver;

.field b:Landroid/database/ContentObserver;

.field private c:Lcom/sec/chaton/buddy/hf;

.field private d:Landroid/widget/ListView;

.field private e:Landroid/view/ViewStub;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/app/ProgressDialog;

.field private k:Lcom/sec/chaton/d/h;

.field private l:Lcom/sec/chaton/d/n;

.field private m:I

.field private n:Ljava/lang/String;

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Landroid/view/View;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/Button;

.field private t:I

.field private u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private w:Z

.field private x:J

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 118
    iput v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->t:I

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->u:Ljava/util/ArrayList;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->v:Ljava/util/ArrayList;

    .line 122
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->w:Z

    .line 159
    new-instance v0, Lcom/sec/chaton/buddy/go;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/go;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->a:Landroid/database/ContentObserver;

    .line 170
    new-instance v0, Lcom/sec/chaton/buddy/gv;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/gv;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->b:Landroid/database/ContentObserver;

    .line 558
    new-instance v0, Lcom/sec/chaton/buddy/hb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/hb;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->A:Landroid/view/View$OnClickListener;

    .line 572
    new-instance v0, Lcom/sec/chaton/buddy/hc;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/hc;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->B:Landroid/view/View$OnClickListener;

    .line 585
    new-instance v0, Lcom/sec/chaton/buddy/hd;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/hd;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->C:Landroid/view/View$OnClickListener;

    .line 655
    new-instance v0, Lcom/sec/chaton/buddy/gp;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/gp;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->D:Landroid/os/Handler;

    .line 903
    new-instance v0, Lcom/sec/chaton/buddy/gu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/gu;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->E:Landroid/os/Handler;

    .line 990
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)I
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;I)I
    .locals 0

    .prologue
    .line 63
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->t:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;J)J
    .locals 0

    .prologue
    .line 63
    iput-wide p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->x:J

    return-wide p1
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 554
    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "buddy_no=\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 555
    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->n:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 531
    if-nez p1, :cond_0

    .line 532
    iget-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->n:Ljava/lang/String;

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 535
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 536
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->l:Lcom/sec/chaton/d/n;

    const-string v1, "true"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/n;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 538
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 501
    if-nez p1, :cond_0

    .line 502
    iget-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->n:Ljava/lang/String;

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->k:Lcom/sec/chaton/d/h;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;Z)Lcom/sec/chaton/d/a/h;

    .line 517
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c:Lcom/sec/chaton/buddy/hf;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/hf;->notifyDataSetChanged()V

    .line 518
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->n:Ljava/lang/String;

    .line 520
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 521
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 522
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->p:Ljava/lang/String;

    .line 525
    :cond_1
    if-eqz v0, :cond_2

    .line 526
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 528
    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;I)I
    .locals 0

    .prologue
    .line 63
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->o:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Lcom/sec/chaton/buddy/hf;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c:Lcom/sec/chaton/buddy/hf;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->p:Ljava/lang/String;

    return-object p1
.end method

.method private c()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 183
    .line 186
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "type=\'200\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 188
    if-nez v1, :cond_1

    .line 189
    const/4 v0, 0x0

    .line 195
    if-eqz v1, :cond_0

    .line 196
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 200
    :cond_0
    return v0

    .line 192
    :cond_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 195
    if-eqz v1, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_2

    .line 196
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 195
    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)I
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d()I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method private d()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 206
    .line 209
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "type=\'100\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 211
    if-nez v1, :cond_1

    .line 212
    const/4 v0, 0x0

    .line 218
    if-eqz v1, :cond_0

    .line 219
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 223
    :cond_0
    return v0

    .line 215
    :cond_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 218
    if-eqz v1, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_2

    .line 219
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 218
    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->n:Ljava/lang/String;

    return-object v0
.end method

.method private e()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 489
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->m:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 490
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v3, "type=\'100\'"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 495
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 496
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 497
    return v1

    .line 492
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v3, "type=\'200\'"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->o:I

    return v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->f:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1138
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->e:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->f:Landroid/view/View;

    .line 1139
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->f:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->g:Landroid/widget/ImageView;

    .line 1140
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->g:Landroid/widget/ImageView;

    const v1, 0x7f02034b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1141
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->f:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->h:Landroid/widget/TextView;

    .line 1142
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b021b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1143
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->f:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->i:Landroid/widget/TextView;

    .line 1144
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b021c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1145
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->e:Landroid/view/ViewStub;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 1147
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->f()V

    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->q:Landroid/view/View;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->u:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->B:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)J
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->x:J

    return-wide v0
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->A:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 407
    .line 410
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "type=\'200\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 411
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413
    if-nez v1, :cond_1

    .line 423
    if-eqz v1, :cond_0

    .line 424
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    const/4 v0, -0x1

    :try_start_2
    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 419
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 420
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->u:Ljava/util/ArrayList;

    const-string v2, "buddy_no"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 423
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 424
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 423
    :cond_2
    throw v0

    :cond_3
    if-eqz v1, :cond_4

    .line 424
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 428
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_5

    .line 429
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 435
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->l:Lcom/sec/chaton/d/n;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/n;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 431
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 423
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c:Lcom/sec/chaton/buddy/hf;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/buddy/hf;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 389
    if-eqz v0, :cond_0

    .line 390
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 392
    :cond_0
    return-void
.end method

.method public b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 441
    .line 443
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "type=\'200\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 445
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447
    if-nez v1, :cond_1

    .line 458
    if-eqz v1, :cond_0

    .line 459
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    const/4 v0, -0x1

    :try_start_2
    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 453
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 454
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->u:Ljava/util/ArrayList;

    const-string v2, "buddy_no"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 458
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 459
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 458
    :cond_2
    throw v0

    :cond_3
    if-eqz v1, :cond_4

    .line 459
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 463
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_5

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 470
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->k:Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->u:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/h;->b(Ljava/util/ArrayList;Z)V

    goto :goto_0

    .line 466
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    .line 467
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 458
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 326
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 329
    new-instance v0, Lcom/sec/chaton/buddy/hf;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, p0, v1, v4, v2}, Lcom/sec/chaton/buddy/hf;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;Landroid/content/Context;Landroid/database/Cursor;I)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c:Lcom/sec/chaton/buddy/hf;

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c:Lcom/sec/chaton/buddy/hf;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/chaton/buddy/ha;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ha;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 362
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 366
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 368
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->D:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->k:Lcom/sec/chaton/d/h;

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->E:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/n;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/n;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->l:Lcom/sec/chaton/d/n;

    .line 371
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 373
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 631
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 633
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 634
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 652
    :goto_0
    return v6

    .line 636
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 638
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->n:Ljava/lang/String;

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->o:I

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 641
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0209

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0247

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->p:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/he;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/he;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 636
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/16 v3, 0xc8

    const/16 v2, 0x64

    .line 133
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 135
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "RECOMMENDED_BUDDY_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 136
    iput v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->m:I

    .line 137
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->t:I

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "RECOMMENDED_BUDDY_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 139
    iput v3, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->m:I

    .line 140
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->t:I

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 614
    new-instance v0, Lcom/sec/chaton/b/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/b/a;-><init>(Landroid/content/Context;Landroid/view/ContextMenu;)V

    .line 616
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 618
    new-instance v1, Lcom/sec/chaton/buddy/hg;

    iget-object v2, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    invoke-direct {v1, v2}, Lcom/sec/chaton/buddy/hg;-><init>(Landroid/view/View;)V

    .line 619
    iget-object v2, v1, Lcom/sec/chaton/buddy/hg;->c:Landroid/widget/ImageView;

    const v3, 0x7f07000e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->n:Ljava/lang/String;

    .line 620
    iget-object v2, v1, Lcom/sec/chaton/buddy/hg;->c:Landroid/widget/ImageView;

    const v3, 0x7f07000f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->o:I

    .line 621
    iget-object v1, v1, Lcom/sec/chaton/buddy/hg;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->p:Ljava/lang/String;

    .line 623
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/b/a;->a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;

    .line 624
    const/16 v1, 0x64

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b014f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 625
    const/16 v1, 0x65

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0209

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 626
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 381
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v4, "type=\'200\'"

    const-string v6, "timestamp DESC"

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 256
    const v0, 0x7f03006d

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 257
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d:Landroid/widget/ListView;

    .line 259
    const v0, 0x7f07029b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->e:Landroid/view/ViewStub;

    .line 262
    const v0, 0x7f0702a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->q:Landroid/view/View;

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->q:Landroid/view/View;

    const v2, 0x7f0702d8

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->r:Landroid/widget/Button;

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->r:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b020a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->q:Landroid/view/View;

    const v2, 0x7f0702d9

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->s:Landroid/widget/Button;

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->s:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 268
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->e()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->t:I

    .line 270
    const v0, 0x7f0200f0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->y:I

    .line 271
    const v0, 0x7f080019

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->z:I

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->r:Landroid/widget/Button;

    new-instance v2, Lcom/sec/chaton/buddy/gw;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/gw;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 294
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->s:Landroid/widget/Button;

    new-instance v2, Lcom/sec/chaton/buddy/gy;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/gy;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->m:I

    const/16 v2, 0x64

    if-ne v0, v2, :cond_0

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->q:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 315
    :goto_0
    return-object v1

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->q:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 241
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c:Lcom/sec/chaton/buddy/hf;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c:Lcom/sec/chaton/buddy/hf;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/hf;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 247
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 248
    const-string v0, "The Cursor of Adapter was closed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :cond_0
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 399
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c:Lcom/sec/chaton/buddy/hf;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/hf;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 400
    if-eqz v0, :cond_0

    .line 401
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 403
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 229
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->w:Z

    .line 231
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->m:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    .line 232
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->m:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 234
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 147
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->w:Z

    .line 150
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->m:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    .line 151
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->d()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->t:I

    .line 152
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->m:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 154
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->c()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->t:I

    .line 155
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendListFragment;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 321
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 322
    return-void
.end method
