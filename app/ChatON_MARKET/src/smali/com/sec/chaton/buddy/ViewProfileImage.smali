.class public Lcom/sec/chaton/buddy/ViewProfileImage;
.super Lcom/sec/chaton/base/BaseActivity;
.source "ViewProfileImage.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/app/ProgressDialog;

.field private e:Landroid/graphics/Bitmap;

.field private f:Lcom/sec/chaton/multimedia/image/as;

.field private g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/ViewProfileImage;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->d:Landroid/app/ProgressDialog;

    .line 48
    iput-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->e:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/ViewProfileImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->e:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 129
    const v0, 0x7f070322

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->c:Landroid/widget/ImageView;

    .line 131
    new-instance v0, Lcom/sec/chaton/multimedia/image/as;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/image/as;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->f:Lcom/sec/chaton/multimedia/image/as;

    .line 153
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_BIGIMAGE_STATUS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_UPDATED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v1}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_BIGIMAGE_STATUS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NOT_CHANGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v1}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_BIGIMAGE_STATUS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_DELETED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v1}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->b:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->d:Landroid/app/ProgressDialog;

    .line 158
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_NO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/buddy/ix;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ix;-><init>(Lcom/sec/chaton/buddy/ViewProfileImage;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/util/ad;->a(Ljava/lang/String;Lcom/sec/chaton/util/bb;)V

    .line 221
    :goto_0
    return-void

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->b:Landroid/content/Context;

    const v1, 0x7f0b00b2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/ViewProfileImage;)Lcom/sec/chaton/multimedia/image/as;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->f:Lcom/sec/chaton/multimedia/image/as;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 317
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 320
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 321
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 322
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 323
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/ViewProfileImage;->startActivity(Landroid/content/Intent;)V

    .line 325
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->d:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 100
    invoke-static {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->a(Landroid/app/Activity;)V

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 119
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->d:Landroid/app/ProgressDialog;

    .line 122
    invoke-direct {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->a()V

    .line 124
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 125
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    const v0, 0x7f030152

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->setContentView(I)V

    .line 67
    iput-object p0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->b:Landroid/content/Context;

    .line 69
    invoke-direct {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->a()V

    .line 70
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 306
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onPause()V

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 309
    iget-object v1, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 311
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onResume()V

    .line 77
    invoke-static {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->a(Landroid/app/Activity;)V

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/sec/chaton/buddy/iw;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/iw;-><init>(Lcom/sec/chaton/buddy/ViewProfileImage;)V

    iput-object v1, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 90
    iget-object v1, p0, Lcom/sec/chaton/buddy/ViewProfileImage;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 94
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->b()V

    .line 95
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 332
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 333
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/ViewProfileImage;->finish()V

    .line 334
    const/4 v0, 0x1

    .line 336
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
