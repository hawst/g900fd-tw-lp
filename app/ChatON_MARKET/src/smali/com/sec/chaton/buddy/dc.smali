.class public Lcom/sec/chaton/buddy/dc;
.super Landroid/app/AlertDialog;
.source "BuddyGroupDialog.java"


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public a:Lcom/sec/common/a/d;

.field c:Lcom/sec/chaton/e/b/d;

.field d:Landroid/content/DialogInterface$OnDismissListener;

.field e:Lcom/sec/chaton/e/b/d;

.field private f:Landroid/support/v4/app/FragmentActivity;

.field private g:Ljava/lang/String;

.field private h:[Ljava/lang/String;

.field private i:Lcom/sec/chaton/buddy/BuddyFragment;

.field private j:Landroid/app/ProgressDialog;

.field private k:Z

.field private l:Lcom/sec/chaton/widget/ClearableEditText;

.field private m:Landroid/content/Context;

.field private n:Lcom/sec/chaton/util/bo;

.field private o:Landroid/os/Handler;

.field private p:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-class v0, Lcom/sec/chaton/buddy/dc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/dc;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->j:Landroid/app/ProgressDialog;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/dc;->k:Z

    .line 201
    new-instance v0, Lcom/sec/chaton/buddy/dd;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dd;-><init>(Lcom/sec/chaton/buddy/dc;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->n:Lcom/sec/chaton/util/bo;

    .line 444
    new-instance v0, Lcom/sec/chaton/buddy/dg;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dg;-><init>(Lcom/sec/chaton/buddy/dc;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->c:Lcom/sec/chaton/e/b/d;

    .line 532
    new-instance v0, Lcom/sec/chaton/buddy/dh;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dh;-><init>(Lcom/sec/chaton/buddy/dc;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->d:Landroid/content/DialogInterface$OnDismissListener;

    .line 542
    new-instance v0, Lcom/sec/chaton/buddy/di;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/di;-><init>(Lcom/sec/chaton/buddy/dc;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->o:Landroid/os/Handler;

    .line 593
    new-instance v0, Lcom/sec/chaton/buddy/dk;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dk;-><init>(Lcom/sec/chaton/buddy/dc;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->e:Lcom/sec/chaton/e/b/d;

    .line 617
    new-instance v0, Lcom/sec/chaton/buddy/dl;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dl;-><init>(Lcom/sec/chaton/buddy/dc;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->p:Landroid/text/TextWatcher;

    .line 77
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    iput-object p1, p0, Lcom/sec/chaton/buddy/dc;->f:Landroid/support/v4/app/FragmentActivity;

    .line 78
    iput-object p2, p0, Lcom/sec/chaton/buddy/dc;->i:Lcom/sec/chaton/buddy/BuddyFragment;

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->i:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->h:[Ljava/lang/String;

    .line 80
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dc;->a()V

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dc;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->m:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x0

    const/4 v2, -0x1

    const/4 v3, -0x2

    const/4 v8, 0x1

    .line 210
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dc;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->m:Landroid/content/Context;

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->m:Landroid/content/Context;

    const v4, 0x7f0b00e2

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dc;->setTitle(Ljava/lang/CharSequence;)V

    .line 221
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dc;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "layout_inflater"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 222
    const v4, 0x7f03001b

    invoke-virtual {v0, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 223
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dc;->setView(Landroid/view/View;)V

    .line 225
    const v4, 0x7f070075

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->l:Lcom/sec/chaton/widget/ClearableEditText;

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->l:Lcom/sec/chaton/widget/ClearableEditText;

    new-array v4, v8, [Landroid/text/InputFilter;

    new-instance v5, Lcom/sec/chaton/util/w;

    iget-object v6, p0, Lcom/sec/chaton/buddy/dc;->m:Landroid/content/Context;

    const/16 v7, 0x1e

    invoke-direct {v5, v6, v7}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v5, v4, v9

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->l:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dc;->p:Landroid/text/TextWatcher;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->l:Lcom/sec/chaton/widget/ClearableEditText;

    new-array v4, v8, [C

    const/16 v5, 0x2c

    aput-char v5, v4, v9

    const v5, 0x7f0b0133

    invoke-static {v0, v4, v5}, Lcom/sec/widget/at;->a(Lcom/sec/chaton/widget/ClearableEditText;[CI)V

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->l:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v4, Lcom/sec/chaton/buddy/de;

    invoke-direct {v4, p0}, Lcom/sec/chaton/buddy/de;-><init>(Lcom/sec/chaton/buddy/dc;)V

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ClearableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 261
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dc;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 267
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v0, v4, :cond_0

    .line 273
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->m:Landroid/content/Context;

    const v4, 0x7f0b0037

    invoke-virtual {v0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v4, Lcom/sec/chaton/buddy/df;

    invoke-direct {v4, p0}, Lcom/sec/chaton/buddy/df;-><init>(Lcom/sec/chaton/buddy/dc;)V

    invoke-virtual {p0, v3, v0, v4}, Lcom/sec/chaton/buddy/dc;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 285
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->m:Landroid/content/Context;

    const v3, 0x7f0b003a

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v0, v1

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v2, v3, v0}, Lcom/sec/chaton/buddy/dc;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 287
    invoke-virtual {p0, v8}, Lcom/sec/chaton/buddy/dc;->setInverseBackgroundForced(Z)V

    .line 350
    return-void

    :cond_0
    move v10, v3

    move v3, v2

    move v2, v10

    goto :goto_0
.end method

.method private b()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 702
    iget-object v2, p0, Lcom/sec/chaton/buddy/dc;->l:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v2}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 711
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "FAVORITES"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 713
    iget-object v3, p0, Lcom/sec/chaton/buddy/dc;->m:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dc;->m:Landroid/content/Context;

    const v5, 0x7f0b0196

    new-array v6, v0, [Ljava/lang/Object;

    aput-object v2, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 718
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dc;)Z
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dc;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/dc;)Lcom/sec/chaton/widget/ClearableEditText;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->l:Lcom/sec/chaton/widget/ClearableEditText;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x3

    .line 742
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->l:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->g:Ljava/lang/String;

    .line 744
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 746
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_1

    .line 747
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dc;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 754
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->h:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->h:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 755
    new-instance v0, Lcom/sec/chaton/e/b/j;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dc;->c:Lcom/sec/chaton/e/b/d;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dc;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/dc;->h:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/e/b/j;-><init>(Lcom/sec/chaton/e/b/d;Ljava/lang/String;[Ljava/lang/String;I)V

    .line 761
    :goto_1
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    invoke-static {v1, v4, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 762
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->h:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->h:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 763
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dc;->d()V

    .line 767
    :cond_0
    return-void

    .line 749
    :cond_1
    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dc;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 758
    :cond_2
    new-instance v0, Lcom/sec/chaton/e/b/j;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dc;->c:Lcom/sec/chaton/e/b/d;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dc;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/e/b/j;-><init>(Lcom/sec/chaton/e/b/d;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method private d()V
    .locals 4

    .prologue
    .line 770
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->f:Landroid/support/v4/app/FragmentActivity;

    if-nez v0, :cond_0

    .line 779
    :goto_0
    return-void

    .line 773
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->j:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 774
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->f:Landroid/support/v4/app/FragmentActivity;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dc;->f:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dc;->j:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 777
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/dc;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dc;->c()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->f:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 782
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->f:Landroid/support/v4/app/FragmentActivity;

    if-nez v0, :cond_1

    .line 788
    :cond_0
    :goto_0
    return-void

    .line 785
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->j:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 786
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/dc;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->h:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/dc;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dc;->e()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/dc;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->o:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/dc;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dc;->d()V

    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/dc;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/dc;->k:Z

    return v0
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/dc;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/dc;)Lcom/sec/chaton/buddy/BuddyFragment;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/buddy/dc;->i:Lcom/sec/chaton/buddy/BuddyFragment;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/dc;->k:Z

    .line 59
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 142
    const-string v0, "onCreate()"

    sget-object v1, Lcom/sec/chaton/buddy/dc;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 150
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 151
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dc;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 158
    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/dc;->setIcon(I)V

    .line 159
    return-void

    .line 154
    :cond_0
    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dc;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public show()V
    .locals 0

    .prologue
    .line 793
    invoke-super {p0}, Landroid/app/AlertDialog;->show()V

    .line 794
    return-void
.end method
