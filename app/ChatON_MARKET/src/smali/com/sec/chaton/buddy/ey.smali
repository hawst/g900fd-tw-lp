.class Lcom/sec/chaton/buddy/ey;
.super Landroid/os/Handler;
.source "BuddyProfileFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyProfileFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 1117
    iput-object p1, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13

    .prologue
    const v12, 0x7f0b0262

    const v11, 0x7f0b00bd

    const/4 v10, 0x1

    const/16 v9, 0x8

    const/4 v2, 0x0

    .line 1120
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1123
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1124
    iget-object v1, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1253
    :goto_0
    return-void

    .line 1130
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x132

    if-ne v1, v3, :cond_1

    .line 1131
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "check buddy isConnectionSuccess()="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", result="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", fault="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "BUDDY PROFILE"

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1133
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v3, 0x3e85

    if-ne v1, v3, :cond_2

    .line 1134
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1136
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1137
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1138
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1140
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b015a

    new-array v4, v10, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1252
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    goto/16 :goto_0

    .line 1141
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v3, 0x3aa1

    if-ne v1, v3, :cond_3

    .line 1142
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1144
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1145
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1146
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1147
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b00ca

    new-array v4, v10, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1150
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z

    move-result v0

    if-ne v0, v10, :cond_1

    .line 1151
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1152
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1154
    iget-object v2, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/d/a/ck;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1157
    :try_start_0
    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_1

    .line 1158
    :catch_0
    move-exception v0

    .line 1159
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    .line 1160
    :catch_1
    move-exception v0

    .line 1161
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto/16 :goto_1

    .line 1162
    :catch_2
    move-exception v0

    .line 1163
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 1166
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v3, 0xc355

    if-ne v1, v3, :cond_4

    .line 1167
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1169
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1170
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1171
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1173
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0289

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1174
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1175
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    goto/16 :goto_1

    .line 1178
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_d

    .line 1179
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/CheckBuddyList;

    .line 1180
    if-nez v0, :cond_6

    .line 1181
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    .line 1182
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    goto/16 :goto_0

    .line 1186
    :cond_6
    iget-object v3, v0, Lcom/sec/chaton/io/entry/CheckBuddyList;->buddy:Ljava/util/ArrayList;

    .line 1188
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_8

    .line 1189
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    .line 1190
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    goto/16 :goto_0

    :cond_8
    move v1, v2

    .line 1195
    :goto_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 1196
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;

    .line 1201
    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->authenticated:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_9

    .line 1202
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0264

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v7}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1203
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->k(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1195
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1204
    :cond_9
    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/am;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 1207
    iget-object v4, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v5}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0263

    new-array v7, v10, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v8}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1208
    iget-object v4, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1209
    iget-object v4, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)V

    .line 1213
    :goto_4
    iget-object v4, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_3

    .line 1211
    :cond_a
    iget-object v4, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileFragment;Ljava/lang/String;)V

    goto :goto_4

    .line 1215
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z

    move-result v0

    if-ne v0, v10, :cond_c

    .line 1216
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v6}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v4, v11, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1218
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v6}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v4, v12, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1223
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z

    move-result v0

    if-ne v0, v10, :cond_f

    .line 1224
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v3, v10, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v1, v11, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1232
    :cond_e
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1235
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Z

    move-result v0

    if-ne v0, v10, :cond_10

    .line 1236
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1237
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1245
    :goto_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->d(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1226
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v3, v10, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v1, v12, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 1240
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->b(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1241
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->l(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1242
    iget-object v0, p0, Lcom/sec/chaton/buddy/ey;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_6
.end method
