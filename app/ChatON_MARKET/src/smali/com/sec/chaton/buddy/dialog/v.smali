.class Lcom/sec/chaton/buddy/dialog/v;
.super Ljava/lang/Object;
.source "PhoneNumberSelectorDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->a(Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 114
    packed-switch p2, :pswitch_data_0

    .line 147
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->finish()V

    .line 148
    return-void

    .line 116
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 122
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 125
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :cond_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 132
    :pswitch_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->a(Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :pswitch_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->a(Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;Ljava/lang/String;)V

    goto :goto_0

    .line 138
    :pswitch_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->a(Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :pswitch_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/v;->a:Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;->a(Lcom/sec/chaton/buddy/dialog/PhoneNumberSelectorDialog;Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 130
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
