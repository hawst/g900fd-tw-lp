.class Lcom/sec/chaton/buddy/bp;
.super Ljava/lang/Object;
.source "BuddyFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 5139
    iput-object p1, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/high16 v2, 0x7f0e0000

    const/4 v1, 0x1

    .line 5145
    add-int/lit8 v0, p2, 0xb

    packed-switch v0, :pswitch_data_0

    .line 5170
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->N(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 5171
    return-void

    .line 5147
    :pswitch_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 5148
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030006"

    const-string v2, "00000001"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5150
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->c(Lcom/sec/chaton/buddy/BuddyFragment;I)I

    .line 5151
    iget-object v0, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->G(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 5152
    iget-object v0, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->O(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->K(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 5153
    iget-object v0, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->J(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto :goto_0

    .line 5159
    :pswitch_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 5162
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    const/16 v1, 0xd

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->c(Lcom/sec/chaton/buddy/BuddyFragment;I)I

    .line 5163
    iget-object v0, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->G(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 5164
    iget-object v0, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->O(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->K(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 5165
    iget-object v0, p0, Lcom/sec/chaton/buddy/bp;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->J(Lcom/sec/chaton/buddy/BuddyFragment;)V

    goto :goto_0

    .line 5145
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
