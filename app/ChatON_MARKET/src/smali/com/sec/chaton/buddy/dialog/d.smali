.class Lcom/sec/chaton/buddy/dialog/d;
.super Landroid/os/Handler;
.source "BuddyDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V
    .locals 0

    .prologue
    .line 2632
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const v11, 0x7f0b0263

    const v10, 0x7f0b00bd

    const/4 v9, 0x1

    const/4 v2, 0x0

    const/16 v8, 0x8

    .line 2635
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 2637
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x132

    if-ne v1, v3, :cond_0

    .line 2638
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "check buddy isConnectionSuccess()="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", result="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", fault="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "BUDDY PROFILE"

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2640
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v3, 0x3e85

    if-ne v1, v3, :cond_1

    .line 2641
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->H(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2642
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2644
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->J(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2645
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->K(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2646
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2647
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2648
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 2649
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 2650
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->P(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 2652
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2654
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b015a

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2794
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    .line 2795
    :goto_1
    return-void

    .line 2655
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v3, 0x3aa1

    if-ne v1, v3, :cond_2

    .line 2656
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->T(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2657
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->H(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2658
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 2659
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2660
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->J(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2661
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->K(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2662
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2663
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2664
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 2665
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->P(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 2666
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2668
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b00ca

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2671
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v0

    if-ne v0, v9, :cond_0

    .line 2672
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2673
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2675
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/chaton/d/a/ck;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2678
    :try_start_0
    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    .line 2679
    :catch_0
    move-exception v0

    .line 2680
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 2681
    :catch_1
    move-exception v0

    .line 2682
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto/16 :goto_0

    .line 2683
    :catch_2
    move-exception v0

    .line 2684
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 2687
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v3, 0xc355

    if-ne v1, v3, :cond_3

    .line 2688
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->H(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2689
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2690
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2691
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->J(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2692
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->K(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2693
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2694
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2695
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 2696
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->P(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 2697
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2699
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0289

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2700
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2701
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->V(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    goto/16 :goto_0

    .line 2704
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v3, :cond_e

    .line 2705
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/CheckBuddyList;

    .line 2706
    if-nez v0, :cond_5

    .line 2707
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    .line 2708
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->V(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    goto/16 :goto_1

    .line 2712
    :cond_5
    iget-object v3, v0, Lcom/sec/chaton/io/entry/CheckBuddyList;->buddy:Ljava/util/ArrayList;

    .line 2714
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 2715
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->W(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    .line 2716
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->V(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)V

    goto/16 :goto_1

    :cond_7
    move v1, v2

    .line 2721
    :goto_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 2722
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;

    .line 2727
    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->authenticated:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_9

    .line 2728
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v0

    if-ne v0, v9, :cond_8

    .line 2729
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v6}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v4, v10, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2733
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->X(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2721
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2731
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0264

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2734
    :cond_9
    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/am;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 2735
    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v4

    if-ne v4, v9, :cond_a

    .line 2736
    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v5, v10, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2741
    :goto_5
    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->c(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2742
    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->d(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)V

    .line 2746
    :goto_6
    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->f(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_4

    .line 2739
    :cond_a
    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v5, v11, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 2744
    :cond_b
    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/CheckBuddy;->countrycode:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->e(Lcom/sec/chaton/buddy/dialog/BuddyDialog;Ljava/lang/String;)V

    goto :goto_6

    .line 2748
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v0

    if-ne v0, v9, :cond_d

    .line 2749
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v6}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v4, v10, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 2753
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v6}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v4, v11, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 2758
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v0

    if-ne v0, v9, :cond_10

    .line 2759
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v1, v10, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2767
    :cond_f
    :goto_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->H(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2768
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->I(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2769
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->J(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2773
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->U(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v0

    if-ne v0, v9, :cond_11

    .line 2774
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2775
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2787
    :goto_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Q(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2788
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->N(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 2789
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->O(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 2790
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->P(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 2763
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->S(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v4}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->R(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v1, v11, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 2776
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->Y(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Z

    move-result v0

    if-ne v0, v9, :cond_12

    .line 2777
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->X(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2778
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2779
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_8

    .line 2782
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->K(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2783
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->M(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2784
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/d;->a:Lcom/sec/chaton/buddy/dialog/BuddyDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/BuddyDialog;->L(Lcom/sec/chaton/buddy/dialog/BuddyDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_8
.end method
