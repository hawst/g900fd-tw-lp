.class Lcom/sec/chaton/buddy/iy;
.super Ljava/lang/Object;
.source "ViewProfileImage.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/util/ba;

.field final synthetic b:Landroid/graphics/Bitmap;

.field final synthetic c:Lcom/sec/chaton/buddy/ix;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/ix;Lcom/sec/chaton/util/ba;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iput-object p2, p0, Lcom/sec/chaton/buddy/iy;->a:Lcom/sec/chaton/util/ba;

    iput-object p3, p0, Lcom/sec/chaton/buddy/iy;->b:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->a:Lcom/sec/chaton/util/ba;

    sget-object v1, Lcom/sec/chaton/util/ba;->a:Lcom/sec/chaton/util/ba;

    if-ne v0, v1, :cond_3

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    iget-object v1, p0, Lcom/sec/chaton/buddy/iy;->b:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/ViewProfileImage;->a(Lcom/sec/chaton/buddy/ViewProfileImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->a(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->a(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v1, v1, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v1}, Lcom/sec/chaton/buddy/ViewProfileImage;->c(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->a(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->a(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v1, v1, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v1}, Lcom/sec/chaton/buddy/ViewProfileImage;->b(Lcom/sec/chaton/buddy/ViewProfileImage;)Lcom/sec/chaton/multimedia/image/as;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->b(Lcom/sec/chaton/buddy/ViewProfileImage;)Lcom/sec/chaton/multimedia/image/as;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v1, v1, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v1}, Lcom/sec/chaton/buddy/ViewProfileImage;->a(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/as;->a(Landroid/widget/ImageView;)V

    .line 195
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->e(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->e(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 205
    :cond_1
    return-void

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->d(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00b2

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 190
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->a:Lcom/sec/chaton/util/ba;

    sget-object v1, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    if-ne v0, v1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/buddy/iy;->c:Lcom/sec/chaton/buddy/ix;

    iget-object v0, v0, Lcom/sec/chaton/buddy/ix;->a:Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-static {v0}, Lcom/sec/chaton/buddy/ViewProfileImage;->d(Lcom/sec/chaton/buddy/ViewProfileImage;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
