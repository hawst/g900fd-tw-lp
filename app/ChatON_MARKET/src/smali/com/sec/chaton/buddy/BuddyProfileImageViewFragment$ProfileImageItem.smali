.class public Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;
.super Ljava/lang/Object;
.source "BuddyProfileImageViewFragment.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public FullfileName:Ljava/lang/String;

.field public Represent:Ljava/lang/String;

.field public ThumbfileName:Ljava/lang/String;

.field public dirCachePath:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public imageId:Ljava/lang/String;

.field public selectedImage:Landroid/widget/ImageView;

.field public thumbImage:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 491
    new-instance v0, Lcom/sec/chaton/buddy/fr;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/fr;-><init>()V

    sput-object v0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 460
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->a(Landroid/os/Parcel;)V

    .line 464
    return-void
.end method

.method private a(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 482
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->id:Ljava/lang/String;

    .line 483
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    .line 484
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    .line 485
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->ThumbfileName:Ljava/lang/String;

    .line 486
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    .line 487
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    .line 488
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 468
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 475
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->ThumbfileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 479
    return-void
.end method
