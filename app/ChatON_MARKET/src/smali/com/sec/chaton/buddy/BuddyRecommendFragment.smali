.class public Lcom/sec/chaton/buddy/BuddyRecommendFragment;
.super Landroid/support/v4/app/Fragment;
.source "BuddyRecommendFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/LinearLayout;

.field private C:Landroid/view/ViewGroup;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/view/ViewStub;

.field private F:Landroid/view/View;

.field private G:Landroid/widget/ImageView;

.field private H:Landroid/widget/TextView;

.field private I:Landroid/widget/TextView;

.field private J:Landroid/app/Activity;

.field private K:Landroid/widget/LinearLayout;

.field private L:Landroid/widget/LinearLayout;

.field private M:Landroid/view/View$OnClickListener;

.field private N:Landroid/view/View$OnClickListener;

.field private O:Landroid/view/View$OnClickListener;

.field private P:Landroid/view/View$OnClickListener;

.field private Q:Landroid/view/View$OnClickListener;

.field private R:Landroid/os/Handler;

.field private S:Landroid/os/Handler;

.field a:Landroid/database/ContentObserver;

.field b:Lcom/sec/chaton/e/a/v;

.field private c:Lcom/sec/chaton/buddy/gm;

.field private d:Landroid/widget/ListView;

.field private e:Landroid/app/ProgressDialog;

.field private f:Lcom/sec/common/a/d;

.field private g:Lcom/sec/chaton/d/h;

.field private h:Lcom/sec/chaton/d/n;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:I

.field private m:J

.field private n:Z

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcom/sec/chaton/e/a/u;

.field private q:Z

.field private r:Landroid/widget/LinearLayout;

.field private s:Landroid/view/View;

.field private t:Landroid/view/View;

.field private u:Landroid/view/View;

.field private v:Lcom/sec/chaton/settings/tellfriends/an;

.field private w:Lcom/sec/chaton/settings/tellfriends/ap;

.field private x:Lcom/sec/chaton/settings/tellfriends/al;

.field private y:Landroid/widget/LinearLayout;

.field private z:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->f:Lcom/sec/common/a/d;

    .line 126
    iput v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l:I

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->n:Z

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->o:Ljava/util/ArrayList;

    .line 134
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->q:Z

    .line 663
    new-instance v0, Lcom/sec/chaton/buddy/gi;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/gi;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a:Landroid/database/ContentObserver;

    .line 703
    new-instance v0, Lcom/sec/chaton/buddy/gj;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/gj;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b:Lcom/sec/chaton/e/a/v;

    .line 999
    new-instance v0, Lcom/sec/chaton/buddy/gk;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/gk;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->M:Landroid/view/View$OnClickListener;

    .line 1059
    new-instance v0, Lcom/sec/chaton/buddy/gl;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/gl;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->N:Landroid/view/View$OnClickListener;

    .line 1073
    new-instance v0, Lcom/sec/chaton/buddy/ft;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ft;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->O:Landroid/view/View$OnClickListener;

    .line 1097
    new-instance v0, Lcom/sec/chaton/buddy/fu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fu;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->P:Landroid/view/View$OnClickListener;

    .line 1107
    new-instance v0, Lcom/sec/chaton/buddy/fv;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fv;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->Q:Landroid/view/View$OnClickListener;

    .line 1132
    new-instance v0, Lcom/sec/chaton/buddy/fw;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/fw;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->R:Landroid/os/Handler;

    .line 1267
    new-instance v0, Lcom/sec/chaton/buddy/ga;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ga;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->S:Landroid/os/Handler;

    .line 1301
    return-void
.end method

.method private a()I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 832
    const/4 v7, 0x0

    .line 836
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "type=\'200\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 837
    if-eqz v1, :cond_2

    .line 838
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 841
    :goto_0
    if-eqz v1, :cond_0

    .line 842
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 846
    :cond_0
    return v0

    .line 841
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_1

    .line 842
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 841
    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_2
    move v0, v7

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;I)I
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;J)J
    .locals 0

    .prologue
    .line 75
    iput-wide p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->m:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    return-object v0
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 794
    const-string v7, ""

    .line 798
    :try_start_0
    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "buddy_no=\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 800
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 801
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 805
    :goto_0
    if-eqz v1, :cond_0

    .line 806
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 810
    :cond_0
    return-object v0

    .line 805
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_1

    .line 806
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 805
    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_2
    move-object v0, v7

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->i:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/database/Cursor;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 178
    .line 181
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 184
    if-eqz p1, :cond_1

    .line 185
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_0
    move v3, v1

    .line 188
    const v1, 0x7f0300af

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->B:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 189
    const v1, 0x7f07014b

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 190
    const v2, 0x7f0700d6

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 192
    const-string v5, "buddy_name"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 193
    const-string v6, "buddy_no"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 195
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    const v2, 0x7f07000e

    invoke-virtual {v4, v2, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 197
    const v2, 0x7f07000f

    invoke-virtual {v4, v2, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 198
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->O:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    invoke-virtual {v2, v1, v6}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->B:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 202
    add-int/lit8 v1, v3, 0x1

    .line 203
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    .line 208
    :cond_1
    if-eqz p1, :cond_2

    .line 209
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 212
    :cond_2
    return-void

    .line 205
    :catch_0
    move-exception v0

    .line 206
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    if-eqz p1, :cond_2

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz p1, :cond_3

    .line 209
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 208
    :cond_3
    throw v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Landroid/database/Cursor;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Landroid/database/Cursor;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Z)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 990
    if-nez p1, :cond_0

    .line 991
    iget-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->i:Ljava/lang/String;

    .line 993
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 994
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 995
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->h:Lcom/sec/chaton/d/n;

    const-string v1, "true"

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/n;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 997
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 970
    if-nez p1, :cond_0

    .line 971
    iget-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->i:Ljava/lang/String;

    .line 974
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 980
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->g:Lcom/sec/chaton/d/h;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;Z)Lcom/sec/chaton/d/a/h;

    .line 985
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/gm;->notifyDataSetChanged()V

    .line 986
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->i:Ljava/lang/String;

    .line 987
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 550
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->f:Lcom/sec/common/a/d;

    if-nez v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0122

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/gh;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/buddy/gh;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->f:Lcom/sec/common/a/d;

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->f:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 564
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;I)I
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->j:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->k:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1466
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->F:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1467
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->E:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->F:Landroid/view/View;

    .line 1468
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->F:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->G:Landroid/widget/ImageView;

    .line 1469
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->G:Landroid/widget/ImageView;

    const v1, 0x7f02034b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1470
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->F:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->H:Landroid/widget/TextView;

    .line 1471
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->H:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b021b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1472
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->F:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->I:Landroid/widget/TextView;

    .line 1473
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->I:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b021c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1475
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Z)Z
    .locals 0

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->n:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/settings/tellfriends/an;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->v:Lcom/sec/chaton/settings/tellfriends/an;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/settings/tellfriends/ap;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->w:Lcom/sec/chaton/settings/tellfriends/ap;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/settings/tellfriends/al;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->x:Lcom/sec/chaton/settings/tellfriends/al;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/buddy/gm;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->p:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->q:Z

    return v0
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l:I

    return v0
.end method

.method static synthetic l(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->L:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->d:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b()V

    return-void
.end method

.method static synthetic o(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->y:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->N:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->z:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->B:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->j:I

    return v0
.end method

.method static synthetic t(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)I
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a()I

    move-result v0

    return v0
.end method

.method static synthetic u(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->P:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic v(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)J
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->m:J

    return-wide v0
.end method

.method static synthetic w(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->M:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 892
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/e/aw;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 894
    const-string v1, ""

    .line 895
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "recomned_receive"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 896
    const/4 v2, 0x0

    .line 897
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isBuddy =  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isSpecial = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    if-ne v1, v5, :cond_0

    .line 908
    :cond_0
    if-ne v1, v5, :cond_2

    .line 909
    const-string v3, "SELECT * FROM recommendee WHERE type=\'200\' ORDER BY timestamp DESC LIMIT 10; "

    .line 910
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isBuddy =  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " isSpecial = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sql = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    invoke-virtual {v0, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 912
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/gm;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 913
    if-eqz v0, :cond_1

    .line 914
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 932
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/gm;->notifyDataSetChanged()V

    .line 956
    return-void

    .line 927
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    invoke-virtual {v0, v6}, Lcom/sec/chaton/buddy/gm;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 928
    if-eqz v0, :cond_1

    .line 929
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 327
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 328
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, v6, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_special"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v6, :cond_1

    .line 329
    :cond_0
    new-instance v0, Lcom/sec/chaton/buddy/gm;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    const v3, 0x7f030120

    const-string v5, "type"

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/buddy/gm;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Landroid/content/Context;ILandroid/database/Cursor;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->d:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 335
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->d:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/chaton/buddy/fs;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/fs;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 360
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->d:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 364
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v7, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/gm;->notifyDataSetChanged()V

    .line 368
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->R:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->g:Lcom/sec/chaton/d/h;

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->S:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/n;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/n;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->h:Lcom/sec/chaton/d/n;

    .line 370
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    invoke-static {v0, v7}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e:Landroid/app/ProgressDialog;

    .line 371
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 384
    :cond_1
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    .line 217
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 218
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1033
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1035
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 1036
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    const v1, 0x7f0b0205

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1055
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1038
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1040
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->i:Ljava/lang/String;

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->j:I

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 1043
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->i:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1038
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1016
    new-instance v0, Lcom/sec/chaton/b/a;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    invoke-direct {v0, v1, p1}, Lcom/sec/chaton/b/a;-><init>(Landroid/content/Context;Landroid/view/ContextMenu;)V

    .line 1018
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 1020
    new-instance v1, Lcom/sec/chaton/buddy/gn;

    iget-object v2, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    invoke-direct {v1, v2}, Lcom/sec/chaton/buddy/gn;-><init>(Landroid/view/View;)V

    .line 1021
    iget-object v2, v1, Lcom/sec/chaton/buddy/gn;->c:Landroid/widget/ImageView;

    const v3, 0x7f07000e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->i:Ljava/lang/String;

    .line 1022
    iget-object v2, v1, Lcom/sec/chaton/buddy/gn;->c:Landroid/widget/ImageView;

    const v3, 0x7f07000f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->j:I

    .line 1023
    iget-object v1, v1, Lcom/sec/chaton/buddy/gn;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->k:Ljava/lang/String;

    .line 1025
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/b/a;->a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;

    .line 1026
    const/16 v1, 0x64

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0080

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1027
    const/16 v1, 0x65

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0209

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/sec/chaton/b/a;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1028
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 869
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    sget-object v2, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v6, "type"

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f070455

    const v4, 0x7f070453

    const v6, 0x7f07014c

    const v3, 0x7f070451

    const/4 v5, 0x0

    .line 228
    const v0, 0x7f03006a

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 229
    const v0, 0x7f03006c

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->K:Landroid/widget/LinearLayout;

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->K:Landroid/widget/LinearLayout;

    const v2, 0x7f07029d

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->s:Landroid/view/View;

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->t:Landroid/view/View;

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->u:Landroid/view/View;

    .line 244
    :goto_0
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/an;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    invoke-direct {v0, v2}, Lcom/sec/chaton/settings/tellfriends/an;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->v:Lcom/sec/chaton/settings/tellfriends/an;

    .line 245
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/ap;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    invoke-direct {v0, v2}, Lcom/sec/chaton/settings/tellfriends/ap;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->w:Lcom/sec/chaton/settings/tellfriends/ap;

    .line 246
    new-instance v0, Lcom/sec/chaton/settings/tellfriends/al;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    invoke-direct {v0, v2}, Lcom/sec/chaton/settings/tellfriends/al;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->x:Lcom/sec/chaton/settings/tellfriends/al;

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->K:Landroid/widget/LinearLayout;

    const v2, 0x7f07029c

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 252
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 253
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->d:Landroid/widget/ListView;

    .line 255
    const v0, 0x7f03006b

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->L:Landroid/widget/LinearLayout;

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->L:Landroid/widget/LinearLayout;

    const v2, 0x7f07029a

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->C:Landroid/view/ViewGroup;

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->D:Landroid/widget/TextView;

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->D:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01ba

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (0)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->L:Landroid/widget/LinearLayout;

    const v2, 0x7f07029b

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->E:Landroid/view/ViewStub;

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->K:Landroid/widget/LinearLayout;

    const v2, 0x7f0702a0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->B:Landroid/widget/LinearLayout;

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->K:Landroid/widget/LinearLayout;

    const v2, 0x7f07029e

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->y:Landroid/widget/LinearLayout;

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->d:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->K:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->K:Landroid/widget/LinearLayout;

    const v2, 0x7f07029f

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->z:Landroid/view/ViewGroup;

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->z:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->A:Landroid/widget/TextView;

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->A:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->A:Landroid/widget/TextView;

    const v2, 0x7f02029a

    invoke-virtual {v0, v5, v5, v2, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 293
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v2, v3}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->p:Lcom/sec/chaton/e/a/u;

    .line 316
    return-object v1

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->s:Landroid/view/View;

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->t:Landroid/view/View;

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->u:Landroid/view/View;

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 875
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 877
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    if-eqz v0, :cond_0

    .line 878
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/gm;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 879
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 880
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 881
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 882
    const-string v0, "The Cursor of Adapter was closed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    .line 223
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 224
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 75
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 963
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c:Lcom/sec/chaton/buddy/gm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/gm;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 964
    if-eqz v0, :cond_0

    .line 965
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 967
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 653
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 654
    iput-boolean v4, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->q:Z

    .line 655
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 657
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->n:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 658
    const-string v0, "recommend_timestamp"

    iget-wide v1, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->m:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 660
    :cond_0
    iput-boolean v4, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->n:Z

    .line 661
    return-void
.end method

.method public onResume()V
    .locals 12

    .prologue
    const v11, 0x7f070450

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v2, 0x0

    .line 389
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 390
    iput-boolean v9, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->q:Z

    .line 391
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l:I

    .line 393
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->J:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v10, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 399
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->l:I

    if-nez v0, :cond_0

    .line 400
    const-string v0, "recommend_timestamp"

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->p:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x2

    sget-object v3, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v5, "type=\'100\' AND rank > 0"

    const-string v7, "rank COLLATE LOCALIZED ASC"

    move-object v4, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->p:Lcom/sec/chaton/e/a/u;

    sget-object v3, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v5, "type=\'200\'"

    move v1, v10

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    invoke-static {}, Lcom/sec/chaton/c/a;->d()V

    .line 420
    invoke-static {}, Lcom/sec/chaton/c/a;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 421
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    const v1, 0x7f070451

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 440
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->s:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/buddy/gb;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/gb;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->t:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/buddy/gd;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/gd;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 509
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->u:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->u:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/buddy/gf;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/gf;-><init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 547
    return-void

    .line 424
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 425
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 431
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    const v1, 0x7f070451

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/Button;

    if-eqz v0, :cond_3

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 434
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 435
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 322
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 323
    return-void
.end method
