.class Lcom/sec/chaton/buddy/hq;
.super Ljava/lang/Object;
.source "SpecialBuddyFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 1022
    iput-object p1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/16 v1, 0xb

    const/4 v4, 0x1

    .line 1025
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1198
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 1029
    :cond_1
    instance-of v0, p1, Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 1030
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1031
    invoke-virtual {p1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 1038
    :cond_2
    :goto_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    .line 1040
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1148
    :sswitch_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v1, :cond_5

    .line 1149
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a()V

    goto :goto_0

    .line 1034
    :cond_3
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_1

    .line 1042
    :sswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "profile_add_special_buddy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1045
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->h(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1046
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->i(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    goto/16 :goto_0

    .line 1052
    :sswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "profile_ignore_special_buddy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1055
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1058
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->h(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1059
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->k(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1062
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1063
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->l(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1073
    :sswitch_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_4

    .line 1074
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "profile_special_chat, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    :cond_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1077
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1078
    const-string v1, "callChatList"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1079
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1080
    const-string v1, "receivers"

    new-array v2, v4, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1081
    const-string v1, "specialbuddy"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1082
    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->startActivity(Landroid/content/Intent;)V

    .line 1084
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1085
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1102
    :sswitch_5
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 1104
    :try_start_0
    const-string v0, "id"

    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1105
    const-string v0, "name"

    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->n(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1106
    iget-object v6, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const v5, 0x7f0b03c3

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Landroid/content/Context;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1107
    :catch_0
    move-exception v0

    .line 1108
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 1109
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1151
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->o(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    goto/16 :goto_0

    .line 1155
    :sswitch_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v1, :cond_6

    .line 1156
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->o(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    goto/16 :goto_0

    .line 1158
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a()V

    goto/16 :goto_0

    .line 1162
    :sswitch_7
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v1, :cond_7

    .line 1163
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    goto/16 :goto_0

    .line 1165
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1169
    :sswitch_8
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v1, :cond_8

    .line 1170
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1172
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->p(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    goto/16 :goto_0

    .line 1176
    :sswitch_9
    sget-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_UPDATED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v0

    .line 1178
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1179
    iget-object v2, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;I)V

    .line 1180
    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_9

    .line 1181
    sget-object v0, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NONE_PROFILE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v0

    .line 1183
    :cond_9
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/ViewProfileImage;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1184
    const-string v2, "PROFILE_BUDDY_NO"

    iget-object v3, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1185
    const-string v2, "PROFILE_BUDDY_BIGIMAGE_STATUS"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1186
    iget-object v0, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1190
    :sswitch_a
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1191
    const-string v1, "liveprofile"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1192
    const-string v1, "liveprofile_id"

    iget-object v2, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1193
    iget-object v1, p0, Lcom/sec/chaton/buddy/hq;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1040
    :sswitch_data_0
    .sparse-switch
        0x7f070502 -> :sswitch_1
        0x7f070503 -> :sswitch_6
        0x7f070504 -> :sswitch_7
        0x7f070505 -> :sswitch_8
        0x7f07051b -> :sswitch_9
        0x7f07051f -> :sswitch_4
        0x7f070520 -> :sswitch_0
        0x7f070521 -> :sswitch_5
        0x7f070522 -> :sswitch_3
        0x7f070523 -> :sswitch_2
        0x7f070529 -> :sswitch_a
    .end sparse-switch
.end method
