.class public Lcom/sec/chaton/buddy/TellFriendsFragment;
.super Landroid/support/v4/app/ListFragment;
.source "TellFriendsFragment.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field protected b:Landroid/view/ViewGroup;

.field protected c:Landroid/widget/ListView;

.field protected d:Lcom/sec/chaton/buddy/iu;

.field protected e:Lcom/sec/chaton/buddy/iv;

.field protected f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/buddy/it;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/chaton/buddy/TellFriendsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/TellFriendsFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 38
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 98
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->b:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 102
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->d:Lcom/sec/chaton/buddy/iu;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/TellFriendsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/TellFriendsFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/iu;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/iu;->notifyDataSetInvalidated()V

    .line 65
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->f:Ljava/util/List;

    .line 45
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 49
    const v0, 0x7f030106

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 51
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->c:Landroid/widget/ListView;

    .line 52
    const v0, 0x7f07042f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->b:Landroid/view/ViewGroup;

    .line 54
    new-instance v0, Lcom/sec/chaton/buddy/iu;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/TellFriendsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f030124

    iget-object v4, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->f:Ljava/util/List;

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/sec/chaton/buddy/iu;-><init>(Lcom/sec/chaton/buddy/TellFriendsFragment;Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->d:Lcom/sec/chaton/buddy/iu;

    .line 56
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->e:Lcom/sec/chaton/buddy/iv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->e:Lcom/sec/chaton/buddy/iv;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/iv;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->e:Lcom/sec/chaton/buddy/iv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/iv;->cancel(Z)Z

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/TellFriendsFragment;->e:Lcom/sec/chaton/buddy/iv;

    .line 81
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroy()V

    .line 82
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/TellFriendsFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/it;

    .line 70
    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/it;->c()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/TellFriendsFragment;->startActivity(Landroid/content/Intent;)V

    .line 73
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 87
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    .line 88
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 93
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 94
    return-void
.end method
