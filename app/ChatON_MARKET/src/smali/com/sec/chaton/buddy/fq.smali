.class Lcom/sec/chaton/buddy/fq;
.super Landroid/os/AsyncTask;
.source "BuddyProfileImageViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 785
    iput-object p1, p0, Lcom/sec/chaton/buddy/fq;->b:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    iput-object p2, p0, Lcom/sec/chaton/buddy/fq;->a:Ljava/io/File;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 789
    iget-object v0, p0, Lcom/sec/chaton/buddy/fq;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/sec/chaton/buddy/fq;->b:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/fq;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 791
    if-eqz v0, :cond_0

    .line 795
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 801
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 802
    if-eqz p1, :cond_0

    .line 803
    iget-object v0, p0, Lcom/sec/chaton/buddy/fq;->b:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 812
    :goto_0
    return-void

    .line 805
    :cond_0
    const-class v1, Lcom/sec/chaton/buddy/BuddyProfileImageView;

    monitor-enter v1

    .line 806
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/fq;->b:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 807
    iget-object v0, p0, Lcom/sec/chaton/buddy/fq;->b:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->e(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0b016c

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 808
    iget-object v0, p0, Lcom/sec/chaton/buddy/fq;->b:Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;->f(Lcom/sec/chaton/buddy/BuddyProfileImageViewFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v0

    const v2, 0x7f020387

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setImageResource(I)V

    .line 810
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 785
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/fq;->a([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 785
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/buddy/fq;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
