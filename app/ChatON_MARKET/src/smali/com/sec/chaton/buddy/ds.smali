.class Lcom/sec/chaton/buddy/ds;
.super Landroid/os/Handler;
.source "BuddyGroupProfileFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V
    .locals 0

    .prologue
    .line 632
    iput-object p1, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/16 v5, 0x8

    const/4 v1, 0x0

    .line 635
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 637
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 700
    :goto_0
    return-void

    .line 641
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 643
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v3, v4, :cond_8

    .line 644
    new-instance v0, Lcom/sec/chaton/e/b/k;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    iget-object v3, v3, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->d:Lcom/sec/chaton/e/b/d;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/a/b;->a()I

    move-result v4

    invoke-direct {v0, v3, v4}, Lcom/sec/chaton/e/b/k;-><init>(Lcom/sec/chaton/e/b/d;I)V

    .line 645
    iget-object v3, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->c(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Z)V

    .line 646
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v3

    invoke-static {v3, v2, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 648
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 649
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->h(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/coolots/sso/a/a;

    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->g(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_1
    move v0, v1

    .line 651
    :goto_1
    iget-object v3, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 652
    iget-object v3, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->i(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    move v2, v1

    .line 651
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 656
    :cond_3
    if-eqz v2, :cond_5

    .line 657
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->j(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 658
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->k(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 672
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 673
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 675
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0b0089

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 676
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 660
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->j(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 661
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->k(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_2

    .line 664
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->j(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 665
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->k(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_2

    .line 668
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->j(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 669
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->k(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_2

    .line 678
    :cond_8
    iget-object v2, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 679
    iget-object v2, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->e(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 681
    :cond_9
    iget-object v2, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/o;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 682
    iget-object v0, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0139

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/dt;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dt;-><init>(Lcom/sec/chaton/buddy/ds;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ds;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0
.end method
