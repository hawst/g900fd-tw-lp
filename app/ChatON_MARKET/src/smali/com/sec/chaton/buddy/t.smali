.class Lcom/sec/chaton/buddy/t;
.super Ljava/lang/Object;
.source "AddBuddyFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/AddBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/AddBuddyFragment;)V
    .locals 0

    .prologue
    .line 1640
    iput-object p1, p0, Lcom/sec/chaton/buddy/t;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1664
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1658
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1644
    iget-object v0, p0, Lcom/sec/chaton/buddy/t;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->b(Lcom/sec/chaton/buddy/AddBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1645
    iget-object v0, p0, Lcom/sec/chaton/buddy/t;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1646
    iget-object v0, p0, Lcom/sec/chaton/buddy/t;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->j(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    .line 1652
    :cond_0
    :goto_0
    return-void

    .line 1648
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/t;->a:Lcom/sec/chaton/buddy/AddBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/AddBuddyFragment;->k(Lcom/sec/chaton/buddy/AddBuddyFragment;)V

    goto :goto_0
.end method
