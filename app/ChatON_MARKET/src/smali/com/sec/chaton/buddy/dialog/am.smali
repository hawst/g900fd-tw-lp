.class final enum Lcom/sec/chaton/buddy/dialog/am;
.super Ljava/lang/Enum;
.source "SpecialBuddyDialog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/buddy/dialog/am;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/buddy/dialog/am;

.field public static final enum b:Lcom/sec/chaton/buddy/dialog/am;

.field private static final synthetic c:[Lcom/sec/chaton/buddy/dialog/am;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 86
    new-instance v0, Lcom/sec/chaton/buddy/dialog/am;

    const-string v1, "Nonetwork"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/buddy/dialog/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/buddy/dialog/am;->a:Lcom/sec/chaton/buddy/dialog/am;

    .line 87
    new-instance v0, Lcom/sec/chaton/buddy/dialog/am;

    const-string v1, "NetworkErr"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/buddy/dialog/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/buddy/dialog/am;->b:Lcom/sec/chaton/buddy/dialog/am;

    .line 85
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/chaton/buddy/dialog/am;

    sget-object v1, Lcom/sec/chaton/buddy/dialog/am;->a:Lcom/sec/chaton/buddy/dialog/am;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/buddy/dialog/am;->b:Lcom/sec/chaton/buddy/dialog/am;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/chaton/buddy/dialog/am;->c:[Lcom/sec/chaton/buddy/dialog/am;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/buddy/dialog/am;
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/sec/chaton/buddy/dialog/am;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/dialog/am;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/buddy/dialog/am;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/sec/chaton/buddy/dialog/am;->c:[Lcom/sec/chaton/buddy/dialog/am;

    invoke-virtual {v0}, [Lcom/sec/chaton/buddy/dialog/am;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/buddy/dialog/am;

    return-object v0
.end method
