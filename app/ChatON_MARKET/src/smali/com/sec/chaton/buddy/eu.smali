.class Lcom/sec/chaton/buddy/eu;
.super Ljava/lang/Object;
.source "BuddyProfileFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/b/d;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyProfileFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V
    .locals 0

    .prologue
    .line 3524
    iput-object p1, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IZLjava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x2

    .line 3529
    if-eqz p2, :cond_3

    .line 3530
    if-ne p1, v4, :cond_2

    .line 3531
    instance-of v0, p3, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 3532
    check-cast p3, Landroid/database/Cursor;

    .line 3534
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 3535
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 3537
    new-instance v0, Lcom/sec/chaton/e/b/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->g:Lcom/sec/chaton/e/b/d;

    iget-object v2, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/e/b/h;-><init>(Lcom/sec/chaton/e/b/d;Lcom/sec/chaton/buddy/a/c;IZ)V

    .line 3538
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    invoke-static {v1, v3, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 3555
    :cond_0
    :goto_0
    return-void

    .line 3541
    :cond_1
    new-instance v0, Lcom/sec/chaton/e/b/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    iget-object v1, v1, Lcom/sec/chaton/buddy/BuddyProfileFragment;->g:Lcom/sec/chaton/e/b/d;

    iget-object v2, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->o(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/sec/chaton/e/b/h;-><init>(Lcom/sec/chaton/e/b/d;Lcom/sec/chaton/buddy/a/c;IZ)V

    .line 3542
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v1

    invoke-static {v1, v3, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    goto :goto_0

    .line 3545
    :cond_2
    if-ne p1, v3, :cond_0

    .line 3546
    iget-object v0, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    .line 3547
    iget-object v0, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b013d

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->r(Lcom/sec/chaton/buddy/BuddyProfileFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3548
    iget-object v0, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 3549
    iget-object v0, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 3553
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/eu;->a:Lcom/sec/chaton/buddy/BuddyProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileFragment;->j(Lcom/sec/chaton/buddy/BuddyProfileFragment;)V

    goto :goto_0
.end method
