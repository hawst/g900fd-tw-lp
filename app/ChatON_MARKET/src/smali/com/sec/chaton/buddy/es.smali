.class Lcom/sec/chaton/buddy/es;
.super Landroid/os/Handler;
.source "BuddyProfileEditNameActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 358
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 360
    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->c(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 366
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_2

    .line 368
    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->c(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0089

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->d(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->d(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->d(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 371
    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0, v3}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->a(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto :goto_0

    .line 375
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->c(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 376
    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->d(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->d(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->d(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 378
    iget-object v0, p0, Lcom/sec/chaton/buddy/es;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v0, v3}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->a(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto :goto_0
.end method
