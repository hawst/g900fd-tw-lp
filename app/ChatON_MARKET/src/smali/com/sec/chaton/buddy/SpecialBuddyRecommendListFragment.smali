.class public Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;
.super Landroid/support/v4/app/Fragment;
.source "SpecialBuddyRecommendListFragment.java"


# instance fields
.field a:Landroid/view/View;

.field b:Landroid/database/ContentObserver;

.field c:Lcom/sec/chaton/e/a/v;

.field private d:Lcom/sec/chaton/buddy/ip;

.field private e:Landroid/widget/ListView;

.field private f:Landroid/view/View;

.field private g:Landroid/app/ProgressDialog;

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/widget/LinearLayout;

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/buddy/io;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/sec/chaton/e/a/u;

.field private n:Landroid/widget/ImageView;

.field private o:Lcom/sec/chaton/d/h;

.field private p:Landroid/app/Activity;

.field private q:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->k:I

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->l:Ljava/util/ArrayList;

    .line 115
    new-instance v0, Lcom/sec/chaton/buddy/ij;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/ij;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->b:Landroid/database/ContentObserver;

    .line 217
    new-instance v0, Lcom/sec/chaton/buddy/im;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/im;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->q:Landroid/os/Handler;

    .line 242
    new-instance v0, Lcom/sec/chaton/buddy/in;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/in;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c:Lcom/sec/chaton/e/a/v;

    .line 337
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;I)I
    .locals 0

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->k:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->g:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Lcom/sec/chaton/buddy/ip;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->d:Lcom/sec/chaton/buddy/ip;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->k:I

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->n:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->p:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->i:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->g:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->p:Landroid/app/Activity;

    .line 82
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 83
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 319
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 321
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 322
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090245

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 323
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->f:Landroid/view/View;

    invoke-virtual {v1, v2, v0, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 325
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 94
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->q:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->o:Lcom/sec/chaton/d/h;

    .line 97
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->p:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->c:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->m:Lcom/sec/chaton/e/a/u;

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->m:Lcom/sec/chaton/e/a/u;

    const/16 v1, 0x64

    sget-object v3, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v5, "type=\'100\'"

    const-string v7, "buddy_name ASC"

    move-object v4, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->p:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 105
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 149
    const v0, 0x7f030103

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 150
    const v0, 0x7f030102

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 151
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 153
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->j:Ljava/util/ArrayList;

    .line 154
    new-instance v1, Lcom/sec/chaton/buddy/ip;

    iget-object v3, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->p:Landroid/app/Activity;

    const v4, 0x7f030124

    iget-object v5, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->j:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v3, v4, v5}, Lcom/sec/chaton/buddy/ip;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->d:Lcom/sec/chaton/buddy/ip;

    .line 155
    const v1, 0x7f07045e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->n:Landroid/widget/ImageView;

    .line 156
    const v1, 0x102000a

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->e:Landroid/widget/ListView;

    .line 157
    const v1, 0x7f07029b

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->f:Landroid/view/View;

    .line 158
    const v1, 0x7f07045f

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->h:Landroid/widget/LinearLayout;

    .line 159
    const v1, 0x7f070460

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->i:Landroid/widget/LinearLayout;

    .line 162
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->d:Lcom/sec/chaton/buddy/ip;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->e:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->e:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/chaton/buddy/ik;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/ik;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->e:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/chaton/buddy/il;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/il;-><init>(Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 214
    return-object v2
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 311
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->p:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 315
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/SpecialBuddyRecommendListFragment;->p:Landroid/app/Activity;

    .line 88
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 89
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 304
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 306
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 292
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 293
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 298
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 299
    return-void
.end method
