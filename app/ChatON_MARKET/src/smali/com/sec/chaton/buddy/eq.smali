.class Lcom/sec/chaton/buddy/eq;
.super Ljava/lang/Object;
.source "BuddyProfileEditNameActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/chaton/buddy/eq;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 231
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    const/16 v2, 0x1e

    .line 209
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 210
    iget-object v1, p0, Lcom/sec/chaton/buddy/eq;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->b(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setMaxLength(I)V

    .line 211
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lt v1, v2, :cond_0

    .line 212
    iget-object v1, p0, Lcom/sec/chaton/buddy/eq;->a:Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;->c(Lcom/sec/chaton/buddy/BuddyProfileEditNameActivity;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0143

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 214
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 227
    :cond_1
    return-void
.end method
