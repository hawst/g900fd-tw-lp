.class Lcom/sec/chaton/buddy/dg;
.super Ljava/lang/Object;
.source "BuddyGroupDialog.java"

# interfaces
.implements Lcom/sec/chaton/e/b/d;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dc;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dc;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IZLjava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 449
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 529
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    if-eqz p2, :cond_0

    .line 481
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 482
    instance-of v0, p3, Ljava/lang/Integer;

    if-eqz v0, :cond_6

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_6

    .line 484
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->c(Lcom/sec/chaton/buddy/dc;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    const-string v4, ""

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->f(Lcom/sec/chaton/buddy/dc;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_4

    .line 486
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 487
    const/4 v1, -0x3

    if-eq v1, v0, :cond_2

    const/4 v1, -0x2

    if-ne v1, v0, :cond_3

    .line 488
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->g(Lcom/sec/chaton/buddy/dc;)V

    goto :goto_0

    .line 492
    :cond_3
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dc;->h(Lcom/sec/chaton/buddy/dc;)Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 493
    const-string v1, "group"

    iget-object v5, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v5}, Lcom/sec/chaton/buddy/dc;->f(Lcom/sec/chaton/buddy/dc;)[Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x145

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)I

    .line 494
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->i(Lcom/sec/chaton/buddy/dc;)V

    goto :goto_0

    .line 497
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->g(Lcom/sec/chaton/buddy/dc;)V

    .line 498
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dc;->a(Lcom/sec/chaton/buddy/dc;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 499
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 500
    const-string v1, "BUDDY_SORT_STYLE"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 501
    const-string v1, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 504
    iget-object v1, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dc;->j(Lcom/sec/chaton/buddy/dc;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 505
    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 508
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 513
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dc;->g(Lcom/sec/chaton/buddy/dc;)V

    .line 520
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dc;->a(Lcom/sec/chaton/buddy/dc;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v4, 0x7f0b01bc

    invoke-virtual {v1, v4}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v4}, Lcom/sec/chaton/buddy/dc;->e(Lcom/sec/chaton/buddy/dc;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0196

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dc;->k(Lcom/sec/chaton/buddy/dc;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/buddy/dc;->a:Lcom/sec/common/a/d;

    .line 523
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    iget-object v0, v0, Lcom/sec/chaton/buddy/dc;->a:Lcom/sec/common/a/d;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    iget-object v1, v1, Lcom/sec/chaton/buddy/dc;->d:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 524
    iget-object v0, p0, Lcom/sec/chaton/buddy/dg;->a:Lcom/sec/chaton/buddy/dc;

    iget-object v0, v0, Lcom/sec/chaton/buddy/dc;->a:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0
.end method
