.class Lcom/sec/chaton/buddy/ak;
.super Ljava/lang/Object;
.source "BuddyAdapter.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/a/c;

.field final synthetic b:Lcom/sec/chaton/buddy/ag;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/ag;Lcom/sec/chaton/buddy/a/c;)V
    .locals 0

    .prologue
    .line 978
    iput-object p1, p0, Lcom/sec/chaton/buddy/ak;->b:Lcom/sec/chaton/buddy/ag;

    iput-object p2, p0, Lcom/sec/chaton/buddy/ak;->a:Lcom/sec/chaton/buddy/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 982
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 997
    :cond_0
    :goto_0
    return v3

    .line 984
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ak;->a:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ak;->b:Lcom/sec/chaton/buddy/ag;

    invoke-static {v1}, Lcom/sec/chaton/buddy/ag;->c(Lcom/sec/chaton/buddy/ag;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0094

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 985
    iget-object v0, p0, Lcom/sec/chaton/buddy/ak;->a:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0999"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 986
    iget-object v0, p0, Lcom/sec/chaton/buddy/ak;->a:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 987
    invoke-virtual {p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ak;->a:Lcom/sec/chaton/buddy/a/c;

    iget-object v2, p0, Lcom/sec/chaton/buddy/ak;->b:Lcom/sec/chaton/buddy/ag;

    invoke-static {v2}, Lcom/sec/chaton/buddy/ag;->c(Lcom/sec/chaton/buddy/ag;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/content/ClipData;Lcom/sec/chaton/buddy/a/c;Landroid/content/Context;)V

    goto :goto_0

    .line 989
    :cond_1
    new-array v0, v3, [Ljava/lang/String;

    .line 990
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ak;->a:Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 991
    invoke-virtual {p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ak;->a:Lcom/sec/chaton/buddy/a/c;

    iget-object v2, p0, Lcom/sec/chaton/buddy/ak;->b:Lcom/sec/chaton/buddy/ag;

    invoke-static {v2}, Lcom/sec/chaton/buddy/ag;->c(Lcom/sec/chaton/buddy/ag;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Landroid/content/ClipData;Lcom/sec/chaton/buddy/a/c;Landroid/content/Context;)V

    goto :goto_0

    .line 982
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
