.class Lcom/sec/chaton/buddy/hz;
.super Ljava/lang/Object;
.source "SpecialBuddyFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/util/ba;

.field final synthetic b:Landroid/graphics/Bitmap;

.field final synthetic c:Lcom/sec/chaton/buddy/hy;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/hy;Lcom/sec/chaton/util/ba;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/sec/chaton/buddy/hz;->c:Lcom/sec/chaton/buddy/hy;

    iput-object p2, p0, Lcom/sec/chaton/buddy/hz;->a:Lcom/sec/chaton/util/ba;

    iput-object p3, p0, Lcom/sec/chaton/buddy/hz;->b:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 522
    iget-object v0, p0, Lcom/sec/chaton/buddy/hz;->a:Lcom/sec/chaton/util/ba;

    sget-object v1, Lcom/sec/chaton/util/ba;->a:Lcom/sec/chaton/util/ba;

    if-ne v0, v1, :cond_3

    .line 523
    iget-object v0, p0, Lcom/sec/chaton/buddy/hz;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 524
    iget-object v0, p0, Lcom/sec/chaton/buddy/hz;->c:Lcom/sec/chaton/buddy/hy;

    iget-object v0, v0, Lcom/sec/chaton/buddy/hy;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/hz;->b:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 526
    iget-object v0, p0, Lcom/sec/chaton/buddy/hz;->c:Lcom/sec/chaton/buddy/hy;

    iget-object v0, v0, Lcom/sec/chaton/buddy/hy;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/sec/chaton/buddy/hz;->c:Lcom/sec/chaton/buddy/hy;

    iget-object v0, v0, Lcom/sec/chaton/buddy/hy;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/hz;->c:Lcom/sec/chaton/buddy/hy;

    iget-object v1, v1, Lcom/sec/chaton/buddy/hy;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->c(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 552
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/hz;->c:Lcom/sec/chaton/buddy/hy;

    iget-object v0, v0, Lcom/sec/chaton/buddy/hy;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 553
    iget-object v0, p0, Lcom/sec/chaton/buddy/hz;->c:Lcom/sec/chaton/buddy/hy;

    iget-object v0, v0, Lcom/sec/chaton/buddy/hy;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 562
    :cond_1
    return-void

    .line 544
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/hz;->c:Lcom/sec/chaton/buddy/hy;

    iget-object v0, v0, Lcom/sec/chaton/buddy/hy;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00b2

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 547
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/hz;->a:Lcom/sec/chaton/util/ba;

    sget-object v1, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    if-ne v0, v1, :cond_0

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/buddy/hz;->c:Lcom/sec/chaton/buddy/hy;

    iget-object v0, v0, Lcom/sec/chaton/buddy/hy;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
