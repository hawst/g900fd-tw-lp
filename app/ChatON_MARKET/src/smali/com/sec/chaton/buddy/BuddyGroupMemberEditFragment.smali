.class public Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;
.super Landroid/support/v4/app/Fragment;
.source "BuddyGroupMemberEditFragment.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/sec/chaton/e/b/d;

.field private c:Lcom/sec/chaton/buddy/a/b;

.field private d:I

.field private e:Landroid/content/Context;

.field private f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:[Ljava/lang/String;

.field private h:[Ljava/lang/String;

.field private i:Landroid/app/Activity;

.field private j:Landroid/view/View$OnClickListener;

.field private k:Landroid/os/Handler;

.field private l:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 151
    new-instance v0, Lcom/sec/chaton/buddy/dm;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dm;-><init>(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->j:Landroid/view/View$OnClickListener;

    .line 213
    new-instance v0, Lcom/sec/chaton/buddy/dn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dn;-><init>(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->k:Landroid/os/Handler;

    .line 255
    new-instance v0, Lcom/sec/chaton/buddy/dp;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dp;-><init>(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->b:Lcom/sec/chaton/e/b/d;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->d:I

    return v0
.end method

.method private a()Lcom/sec/chaton/buddy/BuddyFragment;
    .locals 2

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "com.sec.chaton:single_pane_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/BuddyFragment;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;Ljava/util/HashSet;)Ljava/util/HashSet;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->f:Ljava/util/HashSet;

    return-object p1
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setTitleView() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const/4 v0, 0x0

    .line 278
    iget v1, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->d:I

    const/16 v2, 0x13

    if-ne v1, v2, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->c:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v0

    .line 282
    :cond_0
    if-eqz v0, :cond_1

    .line 283
    if-ltz p1, :cond_2

    .line 284
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 289
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/base/BaseSinglePaneActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 291
    :cond_1
    return-void

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method private a(Ljava/util/HashSet;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g:[Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a()Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    .line 176
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v0, v2

    .line 180
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    move v1, v2

    .line 182
    :goto_1
    array-length v5, v3

    if-ge v1, v5, :cond_1

    .line 183
    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g:[Ljava/lang/String;

    aget-object v5, v5, v0

    aget-object v6, v3, v1

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 180
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 187
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 190
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->h:[Ljava/lang/String;

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->h:[Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->h:[Ljava/lang/String;

    move v0, v2

    .line 193
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 194
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "before "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move v0, v2

    .line 196
    :goto_4
    array-length v1, v3

    if-ge v0, v1, :cond_4

    .line 197
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "after "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v5, v3, v0

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    move v0, v2

    .line 199
    :goto_5
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a()Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 200
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "added "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a()Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    move v1, v2

    .line 202
    :goto_6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleted "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 207
    :cond_6
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->k:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 208
    const-string v1, "group"

    const/16 v3, 0x149

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->c:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a()Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->h()[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->h:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZILjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)I

    .line 209
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->b()V

    .line 211
    :cond_7
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Lcom/sec/chaton/buddy/BuddyFragment;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a()Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->l:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->l:Landroid/app/ProgressDialog;

    .line 327
    :goto_0
    return-void

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;Ljava/util/HashSet;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a(Ljava/util/HashSet;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->f:Ljava/util/HashSet;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->l:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 333
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->e:Landroid/content/Context;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 345
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 348
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 349
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 350
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->startActivity(Landroid/content/Intent;)V

    .line 353
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Lcom/sec/chaton/buddy/a/b;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->c:Lcom/sec/chaton/buddy/a/b;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->l:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->k:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->h:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->b()V

    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->c()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 95
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    .line 96
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 62
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->e:Landroid/content/Context;

    .line 64
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->d:I

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onCreate() mPurpose="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    const v1, 0x7f03001e

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(I)V

    .line 69
    if-nez p1, :cond_0

    .line 70
    new-instance v0, Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyFragment;-><init>()V

    .line 71
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->setArguments(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f070013

    const-string v3, "com.sec.chaton:single_pane_fragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "groupInfo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/b;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->c:Lcom/sec/chaton/buddy/a/b;

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->g:[Ljava/lang/String;

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->c:Lcom/sec/chaton/buddy/a/b;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/b;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a(I)V

    .line 88
    invoke-virtual {p0, v4}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->setHasOptionsMenu(Z)V

    .line 89
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 114
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 115
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 108
    const-string v0, "onDestroy()"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    .line 103
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 125
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0705a6

    if-ne v1, v2, :cond_2

    .line 126
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    :goto_0
    return v0

    .line 130
    :cond_0
    iget v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->d:I

    packed-switch v0, :pswitch_data_0

    .line 144
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 134
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a()Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->f:Ljava/util/HashSet;

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->f:Ljava/util/HashSet;

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->a(Ljava/util/HashSet;)V

    goto :goto_1

    .line 139
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 140
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->i:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 119
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 121
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 339
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 341
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyGroupMemberEditFragment;->d()V

    .line 342
    return-void
.end method
