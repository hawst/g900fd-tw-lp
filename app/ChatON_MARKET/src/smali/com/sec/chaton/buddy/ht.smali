.class Lcom/sec/chaton/buddy/ht;
.super Landroid/os/Handler;
.source "SpecialBuddyFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V
    .locals 0

    .prologue
    .line 1372
    iput-object p1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14

    .prologue
    const/4 v1, 0x2

    const/16 v4, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v13, 0x0

    .line 1378
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1799
    :cond_0
    :goto_0
    return-void

    .line 1384
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1387
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x13d

    if-ne v2, v3, :cond_4

    .line 1388
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1390
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1393
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    if-eqz v1, :cond_2e

    .line 1394
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v1

    .line 1397
    :goto_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    .line 1399
    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v3, :cond_3

    .line 1400
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v5}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    .line 1401
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03ef

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v13

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1403
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1405
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 1409
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;Z)V

    goto :goto_0

    .line 1412
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Follow LivePartner, faultCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03ec

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v13

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1416
    :cond_4
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x13e

    if-ne v2, v3, :cond_7

    .line 1417
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1419
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1422
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    if-eqz v1, :cond_2d

    .line 1423
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v1

    .line 1426
    :goto_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    .line 1427
    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v3, :cond_6

    .line 1428
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v13}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    .line 1430
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03ee

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v13

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1433
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1434
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/buddy/ih;

    if-eqz v0, :cond_5

    .line 1435
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/ih;

    invoke-interface {v0}, Lcom/sec/chaton/buddy/ih;->f()V

    goto/16 :goto_0

    .line 1441
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1442
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1449
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unFollow LivePartner, faultCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1450
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03ed

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v13

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1453
    :cond_7
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x13f

    if-ne v2, v3, :cond_b

    .line 1454
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v3, :cond_9

    .line 1455
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 1456
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;

    .line 1457
    iget-object v2, v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;->count:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 1458
    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;->count:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v2, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)I

    .line 1459
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1460
    const-string v0, "likecount"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1461
    const-string v0, "islike"

    const-string v2, "Y"

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1462
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->v(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "buddy_no=\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v7}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1464
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)V

    .line 1474
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v13}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->f(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)V

    .line 1475
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    goto/16 :goto_0

    .line 1468
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v13}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    .line 1469
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_a

    .line 1470
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->f(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->e(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)V

    .line 1472
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)V

    goto :goto_3

    .line 1476
    :cond_b
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x140

    if-ne v2, v3, :cond_f

    .line 1477
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v3, :cond_d

    .line 1478
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 1479
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;

    .line 1480
    iget-object v2, v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;->count:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 1481
    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/LivepartnerLikesCountEntry;->count:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v2, v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)I

    .line 1482
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1483
    const-string v0, "likecount"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1484
    const-string v0, "islike"

    const-string v2, "N"

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->v(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "buddy_no=\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v7}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1487
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)V

    .line 1497
    :cond_c
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v13}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->f(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)V

    .line 1498
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    goto/16 :goto_0

    .line 1491
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v5}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    .line 1492
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1493
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->f(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->e(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)V

    .line 1495
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)V

    goto :goto_4

    .line 1499
    :cond_f
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x64

    if-ne v2, v3, :cond_11

    .line 1500
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_10

    .line 1501
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1502
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1503
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->w(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/d/n;

    move-result-object v1

    const-string v2, "true"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/d/n;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 1505
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1508
    :cond_11
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x13c

    if-ne v2, v3, :cond_18

    .line 1509
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->x(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 1510
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->x(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1512
    :cond_12
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->y(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1513
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v1

    if-ne v1, v5, :cond_13

    .line 1520
    :cond_13
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1, v13}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    .line 1521
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_17

    .line 1523
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;

    .line 1524
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_14

    .line 1525
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "categoryList.bFollowCount : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->followcount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", categoryList.nLikeCount : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->likecount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1527
    :cond_14
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->likecount:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 1528
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->likecount:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)I

    .line 1532
    :goto_5
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->followcount:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 1533
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->followcount:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;J)J

    .line 1538
    :goto_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1539
    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1540
    new-instance v2, Lcom/sec/chaton/io/entry/inner/SpecialUser;

    invoke-direct {v2}, Lcom/sec/chaton/io/entry/inner/SpecialUser;-><init>()V

    .line 1541
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetContentCategoryList;->followcount:Ljava/lang/String;

    iput-object v0, v2, Lcom/sec/chaton/io/entry/inner/SpecialUser;->followcount:Ljava/lang/String;

    .line 1542
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->u(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)V

    .line 1543
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->z(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)J

    move-result-wide v3

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->b(Lcom/sec/chaton/buddy/SpecialBuddyFragment;J)V

    .line 1546
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1547
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/a/d;Lcom/sec/chaton/io/entry/inner/SpecialUser;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/d;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/buddy/a/d;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1551
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 1552
    :catch_0
    move-exception v0

    .line 1554
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 1530
    :cond_15
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1, v13}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;I)I

    goto :goto_5

    .line 1535
    :cond_16
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;J)J

    goto :goto_6

    .line 1555
    :catch_1
    move-exception v0

    .line 1557
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto/16 :goto_0

    .line 1614
    :cond_17
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1616
    :cond_18
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x141

    if-ne v2, v3, :cond_1b

    .line 1618
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_1a

    .line 1619
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_19

    .line 1620
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Get Following buddy\'s info isConnectionSuccess()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fault="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1622
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->v(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v4

    sget-object v7, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const-string v9, "buddy_no = ? "

    new-array v10, v5, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v13

    move-object v8, v6

    move-object v11, v6

    invoke-virtual/range {v4 .. v11}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1624
    :cond_1a
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1626
    :cond_1b
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x12f

    if-ne v2, v3, :cond_23

    .line 1627
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1629
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1630
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_1d

    .line 1631
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v5}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    .line 1633
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 1634
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v0

    .line 1635
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b027d

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v0, v4, v13

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1639
    :goto_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->q(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1640
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;Z)V

    .line 1641
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->v(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v4

    const/4 v0, 0x5

    sget-object v7, Lcom/sec/chaton/e/ag;->a:Landroid/net/Uri;

    const-string v8, "buddy_no = ? "

    new-array v9, v5, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v13

    move v5, v0

    invoke-virtual/range {v4 .. v9}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1637
    :cond_1c
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b008b

    invoke-static {v0, v1, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_7

    .line 1642
    :cond_1d
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_1e

    .line 1644
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1645
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/d;->b()Ljava/lang/String;

    move-result-object v0

    .line 1646
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v13

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1647
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b016c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/hu;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/hu;-><init>(Lcom/sec/chaton/buddy/ht;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1670
    :cond_1e
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1f

    .line 1671
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADD BUDDY RESULT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1673
    :cond_1f
    const-string v1, ""

    .line 1674
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    const/16 v3, 0x3e83

    if-ne v2, v3, :cond_20

    .line 1676
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00a9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1690
    :goto_8
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b016c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/hv;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/hv;-><init>(Lcom/sec/chaton/buddy/ht;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1677
    :cond_20
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    const/16 v3, 0x3e84

    if-ne v2, v3, :cond_21

    .line 1679
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_8

    .line 1680
    :cond_21
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v2, 0x3e85

    if-ne v0, v2, :cond_22

    .line 1683
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_8

    .line 1687
    :cond_22
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00be

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_8

    .line 1729
    :cond_23
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x25a

    if-ne v2, v3, :cond_25

    .line 1730
    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1731
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_24

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v2, :cond_24

    .line 1732
    new-instance v0, Lcom/sec/chaton/e/b/h;

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v2, v2, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->c:Lcom/sec/chaton/e/b/d;

    iget-object v3, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/a/d;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1, v13}, Lcom/sec/chaton/e/b/h;-><init>(Lcom/sec/chaton/e/b/d;Lcom/sec/chaton/buddy/a/c;IZ)V

    .line 1733
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 1734
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v13}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    .line 1735
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->q(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1736
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;Z)V

    .line 1737
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1740
    :cond_24
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1742
    :cond_25
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x25b

    if-ne v1, v2, :cond_28

    .line 1743
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_26

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_26

    .line 1744
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1746
    :cond_26
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_27

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_27

    .line 1750
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 1751
    const-string v0, "isNew"

    const-string v1, "Y"

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1752
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->v(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v7

    sget-object v10, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "buddy_no=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move v8, v13

    move-object v9, v6

    move-object v13, v6

    invoke-virtual/range {v7 .. v13}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1755
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0, v5}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Z)Z

    .line 1758
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->q(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1759
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->g(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Lcom/sec/chaton/buddy/a/d;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->j(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;Lcom/sec/chaton/buddy/a/d;Z)V

    .line 1760
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1762
    :cond_27
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->a(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0141

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->m(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v13

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v13}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1765
    :cond_28
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x132

    if-ne v1, v2, :cond_0

    .line 1766
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_29

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->d(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_29

    .line 1767
    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->t(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)V

    .line 1769
    :cond_29
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2a

    .line 1770
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check buddy isConnectionSuccess()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fault="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BUDDY PROFILE"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1773
    :cond_2a
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_2b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e85

    if-ne v1, v2, :cond_2b

    .line 1774
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->A(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1776
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->B(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1777
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->C(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1780
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->D(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/widget/Button;->setVisibility(I)V

    .line 1781
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->E(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/widget/Button;->setVisibility(I)V

    .line 1783
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->G(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b015a

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v13

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1784
    :cond_2b
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2c

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_2c

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v1, 0x3aa1

    if-ne v0, v1, :cond_2c

    .line 1787
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->G(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ca

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v13

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1790
    :cond_2c
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->B(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/widget/Button;->setVisibility(I)V

    .line 1792
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->D(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1793
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->E(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1795
    iget-object v0, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->G(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bd

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/buddy/ht;->a:Lcom/sec/chaton/buddy/SpecialBuddyFragment;

    invoke-static {v4}, Lcom/sec/chaton/buddy/SpecialBuddyFragment;->F(Lcom/sec/chaton/buddy/SpecialBuddyFragment;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v13

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2d
    move-object v1, v6

    goto/16 :goto_2

    :cond_2e
    move-object v1, v6

    goto/16 :goto_1
.end method
