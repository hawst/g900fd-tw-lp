.class public Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;
.super Landroid/support/v4/app/Fragment;
.source "GroupProfileImageViewFragment.java"


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field a:Ljava/lang/String;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/net/Uri;

.field private final e:I

.field private final f:I

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/io/File;

.field private j:Lcom/sec/chaton/multimedia/image/as;

.field private k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private l:Ljava/io/File;

.field private m:Ljava/io/File;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Z

.field private q:Landroid/widget/Toast;

.field private r:Landroid/app/Activity;

.field private s:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    const-class v0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 67
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->e:I

    .line 68
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->f:I

    .line 72
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->a:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/profile/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->i:Ljava/io/File;

    .line 78
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->l:Ljava/io/File;

    .line 80
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->n:Ljava/lang/String;

    .line 82
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->o:Z

    .line 83
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->p:Z

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Lcom/sec/chaton/multimedia/image/as;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->j:Lcom/sec/chaton/multimedia/image/as;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d:Landroid/net/Uri;

    return-object v0
.end method

.method private d()V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->f()V

    .line 234
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->h:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 506
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c:Landroid/widget/ImageView;

    const v1, 0x7f0201bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 507
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 511
    :goto_0
    return-void

    .line 508
    :catch_0
    move-exception v0

    .line 509
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->l:Ljava/io/File;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UserProfile initialize() - img status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "profile_image_status"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 516
    const-string v0, ""

    .line 518
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->h:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 525
    :cond_0
    :goto_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->l:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_group_profile.png_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 526
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "photoFile="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 528
    new-instance v0, Lcom/sec/chaton/buddy/hk;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/buddy/hk;-><init>(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;Ljava/io/File;)V

    .line 566
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 574
    :goto_1
    return-void

    .line 519
    :catch_0
    move-exception v1

    .line 521
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 522
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 568
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->e()V

    .line 569
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    const v1, 0x7f0b00c7

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    goto :goto_1

    .line 572
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->e()V

    goto :goto_1
.end method

.method private g()V
    .locals 8

    .prologue
    .line 578
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 579
    :cond_0
    const-string v0, "[deleteTempFolder] External Storage Is Not Available or Writable!"

    sget-object v1, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->q:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 581
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    const v1, 0x7f0b002b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->q:Landroid/widget/Toast;

    .line 583
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->q:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 606
    :cond_2
    :goto_0
    return-void

    .line 586
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v1

    .line 587
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 588
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->q:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 594
    :cond_4
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/profile/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 595
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 596
    if-eqz v2, :cond_2

    .line 597
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 598
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/profile/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    .line 599
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Delete File] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/profile/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 597
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 602
    :catch_0
    move-exception v0

    .line 603
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 647
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 648
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 649
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->o:Z

    .line 650
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->p:Z

    .line 658
    :goto_0
    return-void

    .line 651
    :cond_0
    const-string v1, "mounted_ro"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 652
    iput-boolean v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->o:Z

    .line 653
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->p:Z

    goto :goto_0

    .line 655
    :cond_1
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->o:Z

    .line 656
    iput-boolean v2, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->p:Z

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 263
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->i:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->i:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 276
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->g()V

    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tmp_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".jpeg_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->n:Ljava/lang/String;

    .line 279
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->i:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->n:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->m:Ljava/io/File;

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->m:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v0

    .line 281
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Create File] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->i:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->m:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d:Landroid/net/Uri;

    .line 287
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 288
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->q:Landroid/widget/Toast;

    if-nez v0, :cond_2

    .line 289
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    const v1, 0x7f0b002b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->q:Landroid/widget/Toast;

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->q:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 409
    :goto_0
    return-void

    .line 297
    :cond_3
    const/4 v0, 0x0

    .line 298
    iget-object v2, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->h:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 299
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 300
    const v0, 0x7f0d000d

    .line 304
    :goto_1
    const v2, 0x7f0b0126

    move v4, v1

    move v1, v2

    move v2, v0

    move v0, v4

    .line 311
    :goto_2
    iget-object v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v3, Lcom/sec/chaton/buddy/hi;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/hi;-><init>(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 395
    if-eqz v0, :cond_4

    .line 396
    const v0, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/buddy/hj;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/hj;-><init>(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 403
    :cond_4
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 405
    :catch_0
    move-exception v0

    .line 407
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 302
    :cond_5
    const v0, 0x7f0d0004

    goto :goto_1

    .line 307
    :cond_6
    const v2, 0x7f0d001d

    .line 308
    const v1, 0x7f0b0204

    goto :goto_2
.end method

.method public a(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 619
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_group_profile.png_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 621
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 628
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 630
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 631
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x5a

    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 632
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v2}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637
    if-eqz v1, :cond_1

    .line 638
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 644
    :cond_1
    :goto_1
    return-void

    .line 622
    :catch_0
    move-exception v1

    .line 624
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 625
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 633
    :catch_1
    move-exception v0

    .line 634
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 637
    if-eqz v1, :cond_1

    .line 638
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 640
    :catch_2
    move-exception v0

    .line 641
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 636
    :catchall_0
    move-exception v0

    .line 637
    if-eqz v1, :cond_2

    .line 638
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 636
    :cond_2
    :goto_3
    throw v0

    .line 640
    :catch_3
    move-exception v1

    .line 641
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_3

    .line 640
    :catch_4
    move-exception v0

    .line 641
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 238
    const-string v0, ""

    .line 240
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {p1, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 247
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 248
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->l:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "_group_profile.png_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 249
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 250
    const/4 v0, 0x1

    .line 253
    :goto_1
    return v0

    .line 241
    :catch_0
    move-exception v1

    .line 243
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 244
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 253
    goto :goto_1
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 609
    invoke-direct {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->h()V

    .line 610
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->o:Z

    return v0
.end method

.method protected c()Z
    .locals 1

    .prologue
    .line 614
    invoke-direct {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->h()V

    .line 615
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->p:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v0, -0x1

    const/16 v4, 0x258

    const/4 v3, 0x1

    .line 419
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 420
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 422
    packed-switch p1, :pswitch_data_0

    .line 484
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 424
    :pswitch_1
    if-ne p2, v0, :cond_0

    .line 425
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "temp_file_path"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 426
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 427
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_1

    .line 428
    const-string v0, "Crop return null!"

    sget-object v1, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 433
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "temp_file_path"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/16 v3, 0x258

    invoke-static {v0, v2, v3}, Lcom/sec/chaton/util/ad;->b(Landroid/content/Context;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    const/16 v2, 0x258

    const/16 v3, 0x258

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 434
    iget-object v2, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->m:Ljava/io/File;

    invoke-static {v1, v2}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 435
    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 436
    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->h:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 439
    :catch_0
    move-exception v0

    .line 440
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 441
    invoke-direct {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->e()V

    goto :goto_0

    .line 447
    :pswitch_2
    if-nez p3, :cond_2

    .line 448
    const-string v0, "Crop Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 451
    :cond_2
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d:Landroid/net/Uri;

    .line 452
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 453
    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 454
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 455
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 456
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 457
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 458
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 459
    const-string v1, "groupname"

    iget-object v2, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 460
    const-string v1, "isgroup"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 461
    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 465
    :pswitch_3
    if-ne p2, v0, :cond_3

    .line 466
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 467
    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 468
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 469
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 470
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 471
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 472
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 473
    const-string v1, "groupname"

    iget-object v2, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 474
    const-string v1, "isgroup"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 475
    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 477
    :cond_3
    const-string v0, "Camera Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 422
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    .line 94
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 95
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    const v1, 0x7f0b01af

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 108
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->setHasOptionsMenu(Z)V

    .line 110
    if-nez p1, :cond_1

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d:Landroid/net/Uri;

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    const-string v0, "CAPTURE_IMAGE_URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 117
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d:Landroid/net/Uri;

    .line 120
    :cond_2
    const-string v0, "PROFILE_IMAGE_TEMP_FILE_URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->m:Ljava/io/File;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 207
    const-string v0, "onCreateOptionsMenu()"

    sget-object v1, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 210
    const v0, 0x7f0f002e

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 211
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 137
    const v0, 0x7f0300ac

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 139
    const v0, 0x7f070320

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->s:Landroid/widget/LinearLayout;

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->s:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 142
    const v0, 0x7f070322

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c:Landroid/widget/ImageView;

    .line 145
    new-instance v0, Lcom/sec/chaton/multimedia/image/as;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/image/as;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->j:Lcom/sec/chaton/multimedia/image/as;

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->j:Lcom/sec/chaton/multimedia/image/as;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "GROUP_PROFILE_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->g:I

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "GROUP_PROFILE_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->h:Ljava/lang/String;

    .line 162
    invoke-direct {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d()V

    .line 163
    return-object v1
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->r:Landroid/app/Activity;

    .line 100
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 101
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 215
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 217
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 226
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 222
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->a()V

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x7f0705ae
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 198
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 203
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 175
    new-instance v1, Lcom/sec/chaton/buddy/hh;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/hh;-><init>(Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;)V

    iput-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 183
    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 187
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 488
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 490
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 491
    const-string v0, "CAPTURE_IMAGE_URI"

    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->d:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->m:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 495
    const-string v0, "PROFILE_IMAGE_TEMP_FILE_URI"

    iget-object v1, p0, Lcom/sec/chaton/buddy/GroupProfileImageViewFragment;->m:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    :cond_1
    return-void
.end method
