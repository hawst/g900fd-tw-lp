.class Lcom/sec/chaton/buddy/dx;
.super Ljava/lang/Object;
.source "BuddyGroupProfileFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)V
    .locals 0

    .prologue
    .line 990
    iput-object p1, p0, Lcom/sec/chaton/buddy/dx;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const v5, 0x7f0b0414

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 993
    packed-switch p2, :pswitch_data_0

    .line 1057
    :goto_0
    return-void

    .line 997
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 998
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1000
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1001
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1002
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1003
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1019
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/dx;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1026
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dx;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;Z)Z

    goto :goto_0

    .line 1020
    :catch_0
    move-exception v0

    .line 1021
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1022
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 1023
    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 1030
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1031
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1032
    const-string v1, "output"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dx;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->s(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1034
    const-string v1, "outputFormat"

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1036
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/dx;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1043
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dx;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;Z)Z

    goto :goto_0

    .line 1037
    :catch_1
    move-exception v0

    .line 1038
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1039
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 1040
    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 1048
    :pswitch_2
    const-string v0, "onClick Delete profile iamge"

    sget-object v1, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dx;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->r(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/buddy/dx;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->f(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Lcom/sec/chaton/buddy/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/b;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_group_profile.png_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1051
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1052
    iget-object v0, p0, Lcom/sec/chaton/buddy/dx;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->t(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0201bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1054
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dx;->a:Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;->a(Lcom/sec/chaton/buddy/BuddyGroupProfileFragment;Z)Z

    goto/16 :goto_0

    .line 993
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
