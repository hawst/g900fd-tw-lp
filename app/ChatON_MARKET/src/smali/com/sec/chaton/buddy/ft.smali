.class Lcom/sec/chaton/buddy/ft;
.super Ljava/lang/Object;
.source "BuddyRecommendFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)V
    .locals 0

    .prologue
    .line 1073
    iput-object p1, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1077
    iget-object v0, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    const v1, 0x7f07000e

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1078
    iget-object v0, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    const v1, 0x7f07000f

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1080
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1081
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/SpecialBuddyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1082
    const-string v1, "specialuserid"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1083
    const-string v1, "speicalusername"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1084
    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1085
    iget-object v1, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->startActivity(Landroid/content/Intent;)V

    .line 1094
    :goto_0
    return-void

    .line 1088
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->a(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/SpecialBuddyDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1089
    const-string v1, "specialuserid"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->b(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1090
    const-string v1, "speicalusername"

    iget-object v2, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->c(Lcom/sec/chaton/buddy/BuddyRecommendFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1091
    const-string v1, "specialBuddyAdded"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1092
    iget-object v1, p0, Lcom/sec/chaton/buddy/ft;->a:Lcom/sec/chaton/buddy/BuddyRecommendFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/buddy/BuddyRecommendFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
