.class Lcom/sec/chaton/buddy/dialog/t;
.super Landroid/os/Handler;
.source "GroupDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/GroupDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/GroupDialog;)V
    .locals 0

    .prologue
    .line 557
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    .line 562
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 615
    :cond_0
    :goto_0
    return-void

    .line 566
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 569
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 571
    if-nez v0, :cond_2

    .line 572
    const-string v0, "CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD_RANDOM (httpEntry == null)"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 575
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_0

    .line 576
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;

    .line 578
    const-string v0, " Groupdialog CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/coverstory/random/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 586
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 587
    new-instance v0, Lcom/sec/chaton/poston/a;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;->fileurl:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->b(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->c(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 594
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 596
    if-nez v0, :cond_3

    .line 597
    const-string v0, "CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD_RANDOM (httpEntry == null)"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 600
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_0

    .line 601
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;

    .line 604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/coverstory/random/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 605
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 606
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD_RANDOM mContentId : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    new-instance v0, Lcom/sec/chaton/poston/a;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;->fileurl:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->b(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v7, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-static {v7}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/t;->a:Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-static {v2}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->c(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 566
    :pswitch_data_0
    .packed-switch 0xbbd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
