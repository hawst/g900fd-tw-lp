.class Lcom/sec/chaton/buddy/by;
.super Ljava/lang/Object;
.source "BuddyFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/bx;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/bx;)V
    .locals 0

    .prologue
    .line 6332
    iput-object p1, p0, Lcom/sec/chaton/buddy/by;->a:Lcom/sec/chaton/buddy/bx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 6335
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 6336
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_2

    .line 6337
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/by;->a:Lcom/sec/chaton/buddy/bx;

    iget-object v0, v0, Lcom/sec/chaton/buddy/bx;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 6346
    :cond_1
    :goto_0
    return-void

    .line 6340
    :cond_2
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/by;->a:Lcom/sec/chaton/buddy/bx;

    iget-object v1, v1, Lcom/sec/chaton/buddy/bx;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->P(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 6341
    const-string v1, "group"

    iget-object v3, p0, Lcom/sec/chaton/buddy/by;->a:Lcom/sec/chaton/buddy/bx;

    iget-object v3, v3, Lcom/sec/chaton/buddy/bx;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v3}, Lcom/sec/chaton/buddy/BuddyFragment;->V(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/buddy/by;->a:Lcom/sec/chaton/buddy/bx;

    iget-object v5, v5, Lcom/sec/chaton/buddy/bx;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v5}, Lcom/sec/chaton/buddy/BuddyFragment;->V(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    new-array v6, v2, [Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    const/16 v6, 0x147

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)I

    .line 6342
    iget-object v0, p0, Lcom/sec/chaton/buddy/by;->a:Lcom/sec/chaton/buddy/bx;

    iget-object v0, v0, Lcom/sec/chaton/buddy/bx;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->W(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/by;->a:Lcom/sec/chaton/buddy/bx;

    iget-object v0, v0, Lcom/sec/chaton/buddy/bx;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->W(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6343
    iget-object v0, p0, Lcom/sec/chaton/buddy/by;->a:Lcom/sec/chaton/buddy/bx;

    iget-object v0, v0, Lcom/sec/chaton/buddy/bx;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->W(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method
