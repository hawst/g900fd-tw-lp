.class public Lcom/sec/chaton/buddy/dialog/GroupDialog;
.super Landroid/app/Activity;
.source "GroupDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Landroid/os/Handler;

.field private b:Ljava/lang/String;

.field private c:Ljava/io/File;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:[Ljava/lang/String;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/ImageView;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/LinearLayout;

.field private o:Landroid/widget/LinearLayout;

.field private p:Landroid/widget/LinearLayout;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/LinearLayout;

.field private s:[Ljava/lang/String;

.field private t:Lcom/coolots/sso/a/a;

.field private u:Landroid/widget/ImageView;

.field private v:Landroid/widget/ImageView;

.field private w:Landroid/widget/ImageView;

.field private x:Lcom/sec/chaton/d/i;

.field private y:Lcom/sec/common/f/c;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/sec/chaton/buddy/dialog/GroupDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->b:Ljava/lang/String;

    .line 557
    new-instance v0, Lcom/sec/chaton/buddy/dialog/t;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/dialog/t;-><init>(Lcom/sec/chaton/buddy/dialog/GroupDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->C:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->k:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/dialog/GroupDialog;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->y:Lcom/sec/common/f/c;

    return-object v0
.end method

.method private e()V
    .locals 12

    .prologue
    const/4 v11, -0x2

    const v10, 0x7f0200f6

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v9, 0x8

    .line 157
    const v0, 0x7f030040

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->setContentView(I)V

    .line 158
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    .line 159
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 160
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030004"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    array-length v0, v0

    if-gtz v0, :cond_2

    .line 285
    :cond_1
    :goto_0
    return-void

    .line 167
    :cond_2
    const v0, 0x7f070159

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 168
    const-string v1, "window"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 170
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getOrientation()I

    move-result v4

    .line 171
    if-eqz v4, :cond_3

    const/4 v5, 0x2

    if-ne v4, v5, :cond_9

    .line 172
    :cond_3
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-double v5, v1

    const-wide v7, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-direct {v4, v1, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 177
    :goto_1
    const v0, 0x7f07015a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->h:Landroid/widget/TextView;

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    array-length v0, v0

    .line 185
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->h:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    :cond_4
    const v0, 0x7f070162

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->i:Landroid/widget/TextView;

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    :cond_5
    const v0, 0x7f07015c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->j:Landroid/widget/LinearLayout;

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    const v0, 0x7f07015d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->k:Landroid/widget/ImageView;

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 203
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->c()Z

    move-result v0

    if-nez v0, :cond_6

    .line 204
    const-string v0, "There is no random image for Group."

    sget-object v1, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->b()Z

    .line 220
    :cond_6
    :goto_3
    const v0, 0x7f070163

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->l:Landroid/widget/ImageView;

    .line 221
    const v0, 0x7f070164

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->m:Landroid/widget/ImageView;

    .line 223
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_c

    .line 225
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->l:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    :goto_4
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Z)V

    .line 233
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->l:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 235
    const v0, 0x7f07015f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->u:Landroid/widget/ImageView;

    .line 236
    const v0, 0x7f070160

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->v:Landroid/widget/ImageView;

    .line 237
    const v0, 0x7f070161

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->w:Landroid/widget/ImageView;

    .line 239
    const v0, 0x7f07015b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->n:Landroid/widget/LinearLayout;

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    const v0, 0x7f070165

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->o:Landroid/widget/LinearLayout;

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    const v0, 0x7f070166

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->p:Landroid/widget/LinearLayout;

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    const v0, 0x7f070167

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->q:Landroid/widget/LinearLayout;

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    const v0, 0x7f070168

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->r:Landroid/widget/LinearLayout;

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 254
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 255
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v4}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 257
    :cond_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    .line 258
    :goto_5
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 259
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    move v3, v2

    .line 258
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 174
    :cond_9
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-double v5, v1

    const-wide v7, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-direct {v4, v1, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 208
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 209
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 210
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 211
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->k:Landroid/widget/ImageView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto/16 :goto_3

    .line 213
    :cond_b
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->c()Z

    move-result v0

    if-nez v0, :cond_6

    .line 214
    const-string v0, "There is no random image for Group even if there is mContentId."

    sget-object v1, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->b()Z

    goto/16 :goto_3

    .line 229
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    goto/16 :goto_4

    .line 263
    :cond_d
    if-eqz v3, :cond_e

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 268
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 274
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 280
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_11
    move v0, v2

    goto/16 :goto_2
.end method

.method private f()V
    .locals 3

    .prologue
    .line 501
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 504
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 505
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 506
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 507
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->startActivity(Landroid/content/Intent;)V

    .line 509
    :cond_0
    return-void
.end method

.method private g()Z
    .locals 4

    .prologue
    .line 528
    const/4 v1, 0x0

    .line 531
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 532
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 537
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVReadyToCall : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    return v0

    .line 533
    :catch_0
    move-exception v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    .line 534
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 533
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private h()Z
    .locals 4

    .prologue
    .line 542
    const/4 v0, 0x0

    .line 545
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 547
    const/4 v0, 0x1

    .line 553
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVAvaiable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    return v0

    .line 549
    :catch_0
    move-exception v1

    .line 550
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private i()Z
    .locals 3

    .prologue
    .line 620
    const/4 v0, 0x0

    .line 623
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v1

    invoke-static {v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 625
    const/4 v0, 0x1

    .line 631
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatONV] isChatONVInstalled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    return v0

    .line 627
    :catch_0
    move-exception v1

    .line 628
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_group_profile.png_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 291
    :try_start_0
    const-string v2, "UTF-8"

    invoke-static {v0, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 298
    :cond_0
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 300
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 301
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_2

    .line 302
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 303
    if-eqz v0, :cond_1

    .line 317
    :goto_1
    return-object v0

    .line 292
    :catch_0
    move-exception v2

    .line 294
    sget-boolean v3, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v3, :cond_0

    .line 295
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 306
    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 308
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 310
    goto :goto_1

    .line 312
    :catch_1
    move-exception v0

    .line 313
    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    .line 317
    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Z
    .locals 7

    .prologue
    .line 727
    const/4 v0, 0x0

    .line 732
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/coverstory/random/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 733
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 735
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 736
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 737
    const-string v1, "checkAndSetCoverStoryRandomImages No random images in file folder "

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    :cond_0
    :goto_0
    return v0

    .line 742
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 743
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 745
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 746
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 747
    const-string v1, "checkAndSetCoverStoryRandomImages not exists the random image in file folder #2#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 752
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_4

    .line 753
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 754
    const-string v1, "checkAndSetCoverStoryRandomImages file size is 0."

    sget-object v3, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 759
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 760
    const-string v0, "checkAndSetCoverStoryRandomImages get the random image in file folder #3#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    :cond_5
    const/4 v0, 0x1

    .line 764
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->k:Landroid/widget/ImageView;

    if-eqz v2, :cond_6

    .line 765
    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->k:Landroid/widget/ImageView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 768
    :cond_6
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 769
    const-string v1, "checkAndSetCoverStoryRandomImages set the random image from file folder #4#"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()Z
    .locals 5

    .prologue
    const v4, 0x7f0902b8

    const v3, 0x7f0902b7

    .line 637
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Buddy didn\'t set Coverstory mContentId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->k:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/widget/ImageView;)Z

    .line 643
    :cond_0
    invoke-static {}, Lcom/sec/chaton/e/a/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 644
    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->z:Ljava/lang/String;

    .line 645
    if-eqz v0, :cond_2

    .line 646
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a(Ljava/lang/String;)Z

    move-result v1

    .line 647
    if-nez v1, :cond_1

    .line 650
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->x:Lcom/sec/chaton/d/i;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    :cond_1
    const/4 v0, 0x1

    .line 662
    :goto_0
    return v0

    .line 658
    :cond_2
    const-string v0, " Coverstory Random ERROR !!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->x:Lcom/sec/chaton/d/i;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/i;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 667
    .line 672
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/coverstory/random/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 673
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 674
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 677
    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 678
    if-eqz v3, :cond_1

    array-length v4, v3

    if-lez v4, :cond_1

    .line 679
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    array-length v6, v3

    int-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    .line 680
    aget-object v3, v3, v4

    .line 682
    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 683
    invoke-virtual {v3, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 684
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "loadRandomimage filename : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "### pos : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ### randomId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 688
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 689
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadRandomimage randomId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ### randomFile : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 691
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-nez v4, :cond_2

    .line 692
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 693
    const-string v1, "checkAndSetCoverStoryRandomImages file size is 0."

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 722
    :cond_1
    :goto_0
    return v0

    .line 697
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 698
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadRandomimage randomFile doesn\'t exit. "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 701
    :cond_3
    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->k:Landroid/widget/ImageView;

    if-eqz v3, :cond_4

    .line 703
    iput-object v5, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->z:Ljava/lang/String;

    .line 704
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->k:Landroid/widget/ImageView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    move v0, v1

    goto :goto_0

    .line 707
    :cond_4
    const-string v1, "loadRandomimage mCoverstoryImg is null"

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 717
    :cond_5
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 718
    const-string v1, "loadRandomimage No coverstory random images in file folder "

    sget-object v2, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public d()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 782
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 783
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GroupDialog Coverstory [saveState()] mContentId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 786
    const-string v1, "COVER_IMAGE_ID"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    return-object v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 146
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 147
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 149
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->finish()V

    .line 153
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v5, 0x12

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 324
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 491
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 326
    :pswitch_1
    sput-boolean v3, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 327
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyEditNickNameActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 328
    const-string v2, "PROFILE_ID"

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    const-string v2, "PROFILE_NAME"

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    const-string v2, "PROFILE_BUDDY_MODE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 331
    const-string v0, "PROFILE_BUDDY_LIST"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 332
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->startActivity(Landroid/content/Intent;)V

    .line 333
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->finish()V

    goto :goto_0

    .line 337
    :pswitch_2
    sput-boolean v3, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 338
    new-instance v1, Lcom/sec/chaton/buddy/a/b;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->e:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    if-nez v4, :cond_1

    :goto_1
    invoke-direct {v1, v2, v3, v0, v5}, Lcom/sec/chaton/buddy/a/b;-><init>(ILjava/lang/String;II)V

    .line 340
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 341
    const-string v2, "ACTIVITY_PURPOSE"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 342
    const-string v2, "groupInfo"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 343
    const-string v1, "GROUP_PROFILE_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    const-string v1, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->startActivity(Landroid/content/Intent;)V

    .line 346
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->finish()V

    goto :goto_0

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 350
    :pswitch_3
    sput-boolean v3, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 351
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 353
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 354
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 355
    const-string v1, "receivers"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 356
    const-string v1, "groupId"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 359
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->startActivity(Landroid/content/Intent;)V

    .line 360
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->finish()V

    goto/16 :goto_0

    .line 364
    :pswitch_4
    sput-boolean v3, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 365
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 367
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 368
    const-string v1, "broadcast2_feature"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 369
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 373
    :goto_2
    const-string v1, "receivers"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 374
    const-string v1, "groupId"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 377
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->startActivity(Landroid/content/Intent;)V

    .line 378
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->finish()V

    goto/16 :goto_0

    .line 371
    :cond_2
    const-string v1, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    .line 387
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/GroupProfileImageViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 388
    const-string v1, "GROUP_PROFILE_ID"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->e:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 389
    const-string v1, "GROUP_PROFILE_NAME"

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 390
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->startActivity(Landroid/content/Intent;)V

    .line 391
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->finish()V

    goto/16 :goto_0

    .line 394
    :pswitch_6
    sput-boolean v3, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 395
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->h()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 397
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 407
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    array-length v0, v0

    if-gt v0, v3, :cond_3

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 410
    if-eqz v0, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 424
    :catch_0
    move-exception v0

    .line 425
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 414
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    array-length v0, v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_4

    .line 415
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 417
    if-eqz v0, :cond_0

    .line 418
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 421
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03ad

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 428
    :cond_5
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 429
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 432
    :cond_6
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 433
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 434
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 443
    :pswitch_7
    sput-boolean v3, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 444
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->h()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 446
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 456
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    array-length v0, v0

    if-gt v0, v3, :cond_7

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 459
    if-eqz v0, :cond_0

    .line 460
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 473
    :catch_1
    move-exception v0

    .line 474
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 463
    :cond_7
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    array-length v0, v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_8

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "Push Name"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 466
    if-eqz v0, :cond_0

    .line 467
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 470
    :cond_8
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03ae

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->t:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 477
    :cond_9
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 478
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 481
    :cond_a
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 482
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 483
    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 324
    :pswitch_data_0
    .packed-switch 0x7f07015b
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 106
    const-string v0, "onCreate()"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 108
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 110
    new-instance v0, Lcom/sec/chaton/d/i;

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->C:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/i;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->x:Lcom/sec/chaton/d/i;

    .line 111
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->y:Lcom/sec/common/f/c;

    .line 112
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->requestWindowFeature(I)Z

    .line 113
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 114
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->setFinishOnTouchOutside(Z)V

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GROUP_DIALOG_GROUP_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d:Ljava/lang/String;

    .line 118
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GROUP_DIALOG_GROUP_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->e:Ljava/lang/String;

    .line 119
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "GROUP_DIALOG_CHAT_RECEIVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->g:[Ljava/lang/String;

    .line 120
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GROUP_DIALOG_GROUP_MEMBERS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->f:Ljava/lang/String;

    .line 121
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GROUP_DIALOG_GROUP_MEMBERS_NO_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->s:[Ljava/lang/String;

    .line 123
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->c:Ljava/io/File;

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/random/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->B:Ljava/lang/String;

    .line 126
    if-eqz p1, :cond_1

    .line 127
    const-string v0, "COVER_IMAGE_BUNDLE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_2

    .line 129
    const-string v1, "COVER_IMAGE_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->A:Ljava/lang/String;

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->z:Ljava/lang/String;

    .line 135
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->e()V

    .line 136
    return-void

    .line 132
    :cond_2
    const-string v0, "GroupDialog Coverstory [onCreate()] mCoverImageID is null"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/GroupDialog;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 513
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 515
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->l:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 517
    iput-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->l:Landroid/widget/ImageView;

    .line 520
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->x:Lcom/sec/chaton/d/i;

    if-eqz v0, :cond_1

    .line 521
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->x:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->g()V

    .line 522
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->x:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->i()V

    .line 523
    iput-object v1, p0, Lcom/sec/chaton/buddy/dialog/GroupDialog;->x:Lcom/sec/chaton/d/i;

    .line 525
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 495
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 497
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->f()V

    .line 498
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 778
    const-string v0, "COVER_IMAGE_BUNDLE"

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/GroupDialog;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 779
    return-void
.end method
