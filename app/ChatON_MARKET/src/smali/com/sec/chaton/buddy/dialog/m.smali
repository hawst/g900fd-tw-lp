.class Lcom/sec/chaton/buddy/dialog/m;
.super Landroid/os/Handler;
.source "ChatONVInstallDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const v5, 0x7f0b016c

    .line 359
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 360
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 405
    :cond_0
    :goto_0
    return-void

    .line 362
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->b(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->b(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-static {v1}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->b(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 366
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_2

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->finish()V

    goto :goto_0

    .line 369
    :cond_2
    const-string v1, "fail to set extra info"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 373
    iget-object v1, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    iget-object v2, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    iget-object v2, v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-virtual {v3}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00b3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-virtual {v4}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b0024

    new-instance v3, Lcom/sec/chaton/buddy/dialog/o;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/dialog/o;-><init>(Lcom/sec/chaton/buddy/dialog/m;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/buddy/dialog/n;

    invoke-direct {v3, p0}, Lcom/sec/chaton/buddy/dialog/n;-><init>(Lcom/sec/chaton/buddy/dialog/m;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->a(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 391
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)Lcom/sec/common/a/d;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/buddy/dialog/p;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/p;-><init>(Lcom/sec/chaton/buddy/dialog/m;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->e(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/m;->a:Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-static {v0}, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;->d(Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 360
    :pswitch_data_0
    .packed-switch 0x131
        :pswitch_0
    .end packed-switch
.end method
