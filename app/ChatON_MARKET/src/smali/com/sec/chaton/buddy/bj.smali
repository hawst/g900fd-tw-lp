.class Lcom/sec/chaton/buddy/bj;
.super Ljava/lang/Object;
.source "BuddyFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/buddy/BuddyFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/buddy/BuddyFragment;)V
    .locals 0

    .prologue
    .line 2956
    iput-object p1, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 3084
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "afterTextChanged() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->C(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3085
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2960
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "beforeTextChanged() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->C(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 2962
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    const/16 v5, 0x15

    const/4 v4, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 2966
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onTextChanged() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->C(Lcom/sec/chaton/buddy/BuddyFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/BuddyFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 2975
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 2976
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->D(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 2977
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->y(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2978
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->z(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2988
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->g(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->F(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 3079
    :cond_0
    :goto_1
    return-void

    .line 2980
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->E(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 2981
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->m()V

    goto :goto_0

    .line 2992
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->i:Z

    if-nez v0, :cond_3

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_3

    .line 2993
    const-string v0, "invalid character(empty string) input from keyboard"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2996
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->G(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 2998
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 3000
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/util/be;->b(C)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/util/be;->c(C)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x25cc

    if-ne v0, v1, :cond_6

    .line 3001
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 3002
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 3004
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->H(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 3008
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Z)Z

    .line 3017
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 3018
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    .line 3026
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/chaton/buddy/cz;->a(I)V

    .line 3078
    :goto_2
    sput-boolean v3, Lcom/sec/chaton/buddy/BuddyFragment;->i:Z

    goto/16 :goto_1

    .line 3029
    :cond_8
    sget-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->i:Z

    if-eqz v0, :cond_b

    .line 3030
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Z)Z

    .line 3031
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->J(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 3038
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->K(Lcom/sec/chaton/buddy/BuddyFragment;)I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_9

    .line 3042
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 3043
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    .line 3057
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 3058
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/widget/FastScrollableExpandableListView;->setVisibility(I)V

    .line 3060
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->H(Lcom/sec/chaton/buddy/BuddyFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3062
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    iget-object v1, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v1}, Lcom/sec/chaton/buddy/BuddyFragment;->F(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v2}, Lcom/sec/chaton/buddy/BuddyFragment;->L(Lcom/sec/chaton/buddy/BuddyFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/buddy/BuddyFragment;->a(Lcom/sec/chaton/buddy/BuddyFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3064
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->M(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 3074
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->J(Lcom/sec/chaton/buddy/BuddyFragment;)V

    .line 3075
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/cz;->b()V

    goto :goto_2

    .line 3067
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 3068
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->f(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/widget/FastScrollableExpandableListView;->setFastScrollEnabled(Z)V

    .line 3070
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/buddy/bj;->a:Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-static {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->I(Lcom/sec/chaton/buddy/BuddyFragment;)Lcom/sec/chaton/buddy/cz;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/chaton/buddy/cz;->a(I)V

    goto :goto_3
.end method
