.class public Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;
.super Landroid/support/v4/app/Fragment;
.source "BuddyEditNickNameFragment.java"


# instance fields
.field a:Lcom/sec/chaton/d/h;

.field b:Landroid/content/Context;

.field c:Lcom/sec/chaton/e/b/d;

.field d:Lcom/sec/chaton/e/a/v;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/view/MenuItem;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lcom/sec/chaton/e/a/u;

.field private j:Landroid/app/ProgressDialog;

.field private k:[Ljava/lang/String;

.field private l:Z

.field private m:Landroid/app/Activity;

.field private n:Landroid/os/Handler;

.field private o:Landroid/text/TextWatcher;

.field private p:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->l:Z

    .line 251
    new-instance v0, Lcom/sec/chaton/buddy/at;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/at;-><init>(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->n:Landroid/os/Handler;

    .line 320
    new-instance v0, Lcom/sec/chaton/buddy/av;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/av;-><init>(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c:Lcom/sec/chaton/e/b/d;

    .line 358
    new-instance v0, Lcom/sec/chaton/buddy/aw;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/aw;-><init>(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->o:Landroid/text/TextWatcher;

    .line 399
    new-instance v0, Lcom/sec/chaton/buddy/ax;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/ax;-><init>(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->p:Landroid/os/Handler;

    .line 456
    new-instance v0, Lcom/sec/chaton/buddy/az;

    invoke-direct {v0, p0}, Lcom/sec/chaton/buddy/az;-><init>(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d:Lcom/sec/chaton/e/a/v;

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 131
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 133
    if-eqz v0, :cond_0

    .line 134
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->h:Ljava/lang/String;

    return-object v0
.end method

.method private b()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 215
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 223
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "FAVORITES"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    const v3, 0x7f0b01bc

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0196

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 229
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->j:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 235
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 238
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_2

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 249
    :cond_1
    :goto_0
    return-void

    .line 242
    :cond_2
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->n:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 243
    const-string v1, "group"

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->h:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->k:[Ljava/lang/String;

    const/16 v6, 0x146

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)I

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->j:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    return-object v0
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 304
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 305
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 306
    const/4 v2, -0x3

    if-eq v2, v1, :cond_0

    const/4 v2, -0x2

    if-ne v2, v1, :cond_2

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 318
    :cond_1
    :goto_0
    return-void

    .line 309
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a:Lcom/sec/chaton/d/h;

    const-string v2, "update"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v5, v0, v3}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I

    .line 310
    new-instance v1, Lcom/sec/chaton/e/b/l;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c:Lcom/sec/chaton/e/b/d;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->g:Ljava/lang/String;

    const/4 v4, 0x2

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/sec/chaton/e/b/l;-><init>(Lcom/sec/chaton/e/b/d;Ljava/lang/String;Ljava/lang/String;I)V

    .line 311
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2, v1}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 521
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 524
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 525
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 526
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 527
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->startActivity(Landroid/content/Intent;)V

    .line 529
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->i:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->f:Landroid/view/MenuItem;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 93
    iput-object p1, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    .line 94
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->g:Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->h:Ljava/lang/String;

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_MODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->l:Z

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PROFILE_BUDDY_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->k:[Ljava/lang/String;

    .line 113
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 114
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00040005"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 118
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 169
    const v0, 0x7f0f002d

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 170
    const v0, 0x7f070575

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->f:Landroid/view/MenuItem;

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->f:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 172
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 173
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 143
    const v0, 0x7f03006e

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 145
    invoke-static {p0, v6}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 147
    const v0, 0x7f0702a2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->o:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    new-array v2, v6, [Landroid/text/InputFilter;

    new-instance v3, Lcom/sec/chaton/util/w;

    iget-object v4, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b:Landroid/content/Context;

    const/16 v5, 0x1e

    invoke-direct {v3, v4, v5}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v3, v2, v7

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    new-array v2, v6, [C

    const/16 v3, 0x2c

    aput-char v3, v2, v7

    const v3, 0x7f0b0133

    invoke-static {v0, v2, v3}, Lcom/sec/widget/at;->a(Landroid/widget/EditText;[CI)V

    .line 160
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v2, v3}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->i:Lcom/sec/chaton/e/a/u;

    .line 161
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->p:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a:Lcom/sec/chaton/d/h;

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b000c

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->j:Landroid/app/ProgressDialog;

    .line 164
    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->a()V

    .line 123
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->j:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 127
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    .line 101
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 177
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f070574

    if-ne v1, v2, :cond_0

    .line 178
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 181
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f070575

    if-ne v1, v2, :cond_4

    .line 182
    iget-boolean v1, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->l:Z

    if-nez v1, :cond_5

    .line 183
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 211
    :goto_0
    return v0

    .line 186
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 187
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/f;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    .line 188
    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v1, :cond_3

    .line 191
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b01bc

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0196

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 195
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->c()V

    .line 201
    :goto_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 202
    const/4 v1, -0x3

    if-eq v1, v0, :cond_4

    const/4 v1, -0x2

    if-eq v1, v0, :cond_4

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_4

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->j:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_4

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 211
    :cond_4
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto/16 :goto_0

    .line 199
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->d()V

    goto :goto_1
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 515
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 516
    iget-object v1, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 517
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 518
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 500
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 501
    const-string v0, "onResume"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->l:Z

    if-nez v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    const v1, 0x7f0b0214

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 510
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->e()V

    .line 511
    return-void

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->m:Landroid/app/Activity;

    const v1, 0x7f0b0245

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/BuddyEditNickNameFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
