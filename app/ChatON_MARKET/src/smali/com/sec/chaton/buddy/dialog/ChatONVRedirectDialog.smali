.class public Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;
.super Landroid/app/Activity;
.source "ChatONVRedirectDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field a:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private f:Lcom/sec/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 38
    const-string v0, "com.sec.android.app.samsungapps"

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->c:Ljava/lang/String;

    .line 39
    const-string v0, "com.android.vending"

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->d:Ljava/lang/String;

    .line 40
    const-string v0, "com.coolots.chaton"

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->e:Ljava/lang/String;

    return-void
.end method

.method private a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 190
    const/high16 v0, 0x10000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 191
    const/high16 v0, 0x4000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 192
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 194
    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 86
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "showAlertDailog"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/dialog/r;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dialog/r;-><init>(Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/buddy/dialog/q;

    invoke-direct {v2, p0}, Lcom/sec/chaton/buddy/dialog/q;-><init>(Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->f:Lcom/sec/common/a/d;

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->f:Lcom/sec/common/a/d;

    new-instance v1, Lcom/sec/chaton/buddy/dialog/s;

    invoke-direct {v1, p0}, Lcom/sec/chaton/buddy/dialog/s;-><init>(Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->f:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 121
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;)Z
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->b()Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 172
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 175
    const/16 v1, 0x80

    :try_start_0
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isInstalled("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 176
    :catch_0
    move-exception v0

    .line 178
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isInstalled("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") false"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 162
    const-string v0, "com.sec.android.app.samsungapps"

    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private c()V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 203
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 204
    const-string v1, "com.sec.android.app.samsungapps"

    const-string v2, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    const-string v1, "directcall"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 206
    const-string v1, "CallerType"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 207
    const-string v1, "GUID"

    const-string v2, "com.coolots.chaton"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->startActivity(Landroid/content/Intent;)V

    .line 209
    return-void
.end method

.method private d()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 219
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "market://details?id=com.coolots.chaton"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 220
    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :goto_0
    return-void

    .line 222
    :catch_0
    move-exception v0

    .line 223
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 224
    const-string v0, "There is no google play in the device"

    sget-object v1, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "http://play.google.com/store/apps/details?id=com.coolots.chaton"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 228
    invoke-direct {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 256
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 259
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 261
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->startActivity(Landroid/content/Intent;)V

    .line 265
    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 157
    :goto_0
    return-void

    .line 130
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->c()V

    .line 150
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->finish()V

    goto :goto_0

    .line 134
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->d()V

    goto :goto_1

    .line 154
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->finish()V

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x7f070138
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 270
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 290
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 46
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    iput-object p0, p0, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->a:Landroid/content/Context;

    .line 51
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 52
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->setFinishOnTouchOutside(Z)V

    .line 82
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->a()V

    .line 83
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 250
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 252
    invoke-direct {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->e()V

    .line 253
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 236
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 246
    :goto_0
    return-void

    .line 243
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
