.class public Lcom/sec/chaton/buddy/a/c;
.super Ljava/lang/Object;
.source "BuddyItem.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private A:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private B:I

.field private C:Ljava/lang/String;

.field private D:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/util/av;",
            ">;"
        }
    .end annotation
.end field

.field private E:I

.field private F:I

.field private G:Z

.field private H:I

.field private I:Ljava/lang/String;

.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:Z

.field private y:I

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    .line 60
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->G:Z

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/buddy/a/c;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    .line 60
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->G:Z

    .line 71
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->e:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->f:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->g:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->h:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->i:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->k:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->l:Z

    .line 78
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->k()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->m:I

    .line 79
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->n:Ljava/lang/String;

    .line 80
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->m()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->o:Z

    .line 82
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->n()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->r:I

    .line 83
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->o()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->s:I

    .line 84
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->p()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->t:I

    .line 85
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->q()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->u:I

    .line 86
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->r()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->v:I

    .line 87
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->s()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->w:I

    .line 89
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->t()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->x:Z

    .line 91
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->u()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->y:I

    .line 93
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->w()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->q:Z

    .line 94
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->v()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->p:Z

    .line 96
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->x()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->A:Ljava/util/HashMap;

    .line 100
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->z()Ljava/util/HashMap;

    move-result-object v1

    .line 101
    if-eqz v1, :cond_0

    .line 102
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 103
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 107
    iget-object v4, p0, Lcom/sec/chaton/buddy/a/c;->A:Ljava/util/HashMap;

    invoke-virtual {v4, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 111
    :cond_0
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->C()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->H:I

    .line 112
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->D()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->I:Ljava/lang/String;

    .line 113
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->E()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->E:I

    .line 114
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->c:Ljava/lang/String;

    .line 115
    invoke-virtual {p1}, Lcom/sec/chaton/buddy/a/c;->F()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    .line 116
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    .line 60
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->G:Z

    .line 119
    iput-object p1, p0, Lcom/sec/chaton/buddy/a/c;->e:Ljava/lang/String;

    .line 120
    iput-object p2, p0, Lcom/sec/chaton/buddy/a/c;->f:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    .line 59
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    .line 60
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->G:Z

    .line 271
    iput-object p1, p0, Lcom/sec/chaton/buddy/a/c;->e:Ljava/lang/String;

    .line 272
    iput-object p2, p0, Lcom/sec/chaton/buddy/a/c;->f:Ljava/lang/String;

    .line 273
    iput-object p3, p0, Lcom/sec/chaton/buddy/a/c;->g:Ljava/lang/String;

    .line 274
    iput-object p4, p0, Lcom/sec/chaton/buddy/a/c;->h:Ljava/lang/String;

    .line 275
    iput-object p5, p0, Lcom/sec/chaton/buddy/a/c;->i:Ljava/lang/String;

    .line 276
    iput-object p6, p0, Lcom/sec/chaton/buddy/a/c;->a:Ljava/lang/String;

    .line 277
    iput-object p7, p0, Lcom/sec/chaton/buddy/a/c;->b:Ljava/lang/String;

    .line 278
    iput-boolean p8, p0, Lcom/sec/chaton/buddy/a/c;->j:Z

    .line 279
    iput-object p9, p0, Lcom/sec/chaton/buddy/a/c;->k:Ljava/lang/String;

    .line 282
    iput-boolean p10, p0, Lcom/sec/chaton/buddy/a/c;->l:Z

    .line 283
    iput p11, p0, Lcom/sec/chaton/buddy/a/c;->m:I

    .line 284
    iput-object p12, p0, Lcom/sec/chaton/buddy/a/c;->n:Ljava/lang/String;

    .line 285
    iput-boolean p13, p0, Lcom/sec/chaton/buddy/a/c;->o:Z

    .line 287
    move/from16 v0, p14

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->r:I

    .line 288
    move/from16 v0, p15

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->s:I

    .line 289
    move/from16 v0, p16

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->t:I

    .line 290
    move/from16 v0, p17

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->u:I

    .line 291
    move/from16 v0, p18

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->v:I

    .line 292
    move/from16 v0, p19

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->w:I

    .line 294
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->x:Z

    .line 296
    move/from16 v0, p20

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->y:I

    .line 298
    move/from16 v0, p21

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->q:Z

    .line 299
    move/from16 v0, p22

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->p:Z

    .line 301
    move/from16 v0, p23

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->H:I

    .line 302
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->I:Ljava/lang/String;

    .line 303
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->c:Ljava/lang/String;

    .line 304
    move/from16 v0, p25

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->E:I

    .line 305
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    .line 60
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->G:Z

    .line 127
    iput-object p1, p0, Lcom/sec/chaton/buddy/a/c;->e:Ljava/lang/String;

    .line 128
    iput-object p2, p0, Lcom/sec/chaton/buddy/a/c;->f:Ljava/lang/String;

    .line 129
    iput-object p3, p0, Lcom/sec/chaton/buddy/a/c;->g:Ljava/lang/String;

    .line 130
    iput-object p4, p0, Lcom/sec/chaton/buddy/a/c;->h:Ljava/lang/String;

    .line 131
    iput-object p5, p0, Lcom/sec/chaton/buddy/a/c;->i:Ljava/lang/String;

    .line 132
    iput-object p6, p0, Lcom/sec/chaton/buddy/a/c;->k:Ljava/lang/String;

    .line 135
    iput-boolean p7, p0, Lcom/sec/chaton/buddy/a/c;->l:Z

    .line 136
    iput p8, p0, Lcom/sec/chaton/buddy/a/c;->m:I

    .line 137
    iput-object p9, p0, Lcom/sec/chaton/buddy/a/c;->n:Ljava/lang/String;

    .line 138
    iput-boolean p10, p0, Lcom/sec/chaton/buddy/a/c;->o:Z

    .line 140
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->x:Z

    .line 142
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZI)V
    .locals 23

    .prologue
    .line 152
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    move/from16 v14, p14

    move/from16 v15, p15

    move/from16 v16, p16

    move/from16 v17, p17

    move/from16 v18, p18

    move/from16 v19, p19

    move/from16 v22, p20

    invoke-direct/range {v0 .. v22}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;I)V

    .line 154
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;I)V
    .locals 2

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    .line 59
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    .line 60
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->G:Z

    .line 168
    iput-object p1, p0, Lcom/sec/chaton/buddy/a/c;->e:Ljava/lang/String;

    .line 169
    iput-object p2, p0, Lcom/sec/chaton/buddy/a/c;->f:Ljava/lang/String;

    .line 170
    iput-object p4, p0, Lcom/sec/chaton/buddy/a/c;->h:Ljava/lang/String;

    .line 171
    iput-object p5, p0, Lcom/sec/chaton/buddy/a/c;->i:Ljava/lang/String;

    .line 172
    iput-object p6, p0, Lcom/sec/chaton/buddy/a/c;->k:Ljava/lang/String;

    .line 173
    iput-object p3, p0, Lcom/sec/chaton/buddy/a/c;->g:Ljava/lang/String;

    .line 177
    iput-boolean p7, p0, Lcom/sec/chaton/buddy/a/c;->l:Z

    .line 178
    iput-object p9, p0, Lcom/sec/chaton/buddy/a/c;->n:Ljava/lang/String;

    .line 179
    iput-boolean p10, p0, Lcom/sec/chaton/buddy/a/c;->o:Z

    .line 180
    iput p8, p0, Lcom/sec/chaton/buddy/a/c;->m:I

    .line 182
    iput p11, p0, Lcom/sec/chaton/buddy/a/c;->r:I

    .line 183
    move/from16 v0, p15

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->v:I

    .line 184
    move/from16 v0, p16

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->w:I

    .line 185
    iput p12, p0, Lcom/sec/chaton/buddy/a/c;->s:I

    .line 186
    iput p13, p0, Lcom/sec/chaton/buddy/a/c;->t:I

    .line 187
    move/from16 v0, p14

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->u:I

    .line 189
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->x:Z

    .line 190
    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->p:Z

    .line 192
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->q:Z

    .line 193
    move/from16 v0, p17

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->y:I

    .line 195
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->I:Ljava/lang/String;

    .line 197
    move/from16 v0, p20

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->H:I

    .line 199
    move/from16 v0, p22

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->E:I

    .line 200
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    .line 59
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    .line 60
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->G:Z

    .line 231
    iput-object p1, p0, Lcom/sec/chaton/buddy/a/c;->e:Ljava/lang/String;

    .line 232
    iput-object p2, p0, Lcom/sec/chaton/buddy/a/c;->f:Ljava/lang/String;

    .line 233
    iput-object p3, p0, Lcom/sec/chaton/buddy/a/c;->g:Ljava/lang/String;

    .line 234
    iput-object p4, p0, Lcom/sec/chaton/buddy/a/c;->h:Ljava/lang/String;

    .line 235
    iput-object p5, p0, Lcom/sec/chaton/buddy/a/c;->i:Ljava/lang/String;

    .line 236
    iput-object p6, p0, Lcom/sec/chaton/buddy/a/c;->k:Ljava/lang/String;

    .line 239
    iput-boolean p7, p0, Lcom/sec/chaton/buddy/a/c;->l:Z

    .line 240
    iput p8, p0, Lcom/sec/chaton/buddy/a/c;->m:I

    .line 241
    iput-object p9, p0, Lcom/sec/chaton/buddy/a/c;->n:Ljava/lang/String;

    .line 242
    iput-boolean p10, p0, Lcom/sec/chaton/buddy/a/c;->o:Z

    .line 244
    iput p11, p0, Lcom/sec/chaton/buddy/a/c;->r:I

    .line 245
    move/from16 v0, p14

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->u:I

    .line 246
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->x:Z

    .line 247
    iput p12, p0, Lcom/sec/chaton/buddy/a/c;->s:I

    .line 248
    iput p13, p0, Lcom/sec/chaton/buddy/a/c;->t:I

    .line 249
    move/from16 v0, p15

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->v:I

    .line 250
    move/from16 v0, p16

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->w:I

    .line 252
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->q:Z

    .line 253
    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->p:Z

    .line 255
    move/from16 v0, p17

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->y:I

    .line 257
    move/from16 v0, p20

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->H:I

    .line 258
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->I:Ljava/lang/String;

    .line 259
    move/from16 v0, p22

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->E:I

    .line 260
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->c:Ljava/lang/String;

    .line 261
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ZIIIIIIIZZILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ILjava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZI",
            "Ljava/lang/String;",
            "ZIIIIIIIZZI",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/util/av;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    .line 59
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    .line 60
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->G:Z

    .line 319
    iput-object p3, p0, Lcom/sec/chaton/buddy/a/c;->g:Ljava/lang/String;

    .line 320
    iput-object p4, p0, Lcom/sec/chaton/buddy/a/c;->h:Ljava/lang/String;

    .line 321
    iput-object p1, p0, Lcom/sec/chaton/buddy/a/c;->e:Ljava/lang/String;

    .line 322
    iput-object p2, p0, Lcom/sec/chaton/buddy/a/c;->f:Ljava/lang/String;

    .line 323
    iput-object p6, p0, Lcom/sec/chaton/buddy/a/c;->k:Ljava/lang/String;

    .line 324
    iput-object p5, p0, Lcom/sec/chaton/buddy/a/c;->i:Ljava/lang/String;

    .line 327
    iput p8, p0, Lcom/sec/chaton/buddy/a/c;->m:I

    .line 328
    iput-object p9, p0, Lcom/sec/chaton/buddy/a/c;->n:Ljava/lang/String;

    .line 329
    iput-boolean p7, p0, Lcom/sec/chaton/buddy/a/c;->l:Z

    .line 330
    iput-boolean p10, p0, Lcom/sec/chaton/buddy/a/c;->o:Z

    .line 332
    move/from16 v0, p14

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->u:I

    .line 333
    iput p11, p0, Lcom/sec/chaton/buddy/a/c;->r:I

    .line 334
    iput p12, p0, Lcom/sec/chaton/buddy/a/c;->s:I

    .line 335
    iput p13, p0, Lcom/sec/chaton/buddy/a/c;->t:I

    .line 336
    move/from16 v0, p15

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->v:I

    .line 337
    move/from16 v0, p16

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->w:I

    .line 339
    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->p:Z

    .line 340
    move/from16 v0, p17

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->y:I

    .line 342
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->q:Z

    .line 343
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->x:Z

    .line 344
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->I:Ljava/lang/String;

    .line 345
    move/from16 v0, p20

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->H:I

    .line 347
    move/from16 v0, p22

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->B:I

    .line 348
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->b:Ljava/lang/String;

    .line 349
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->C:Ljava/lang/String;

    .line 350
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->D:Ljava/util/ArrayList;

    .line 351
    move/from16 v0, p26

    iput v0, p0, Lcom/sec/chaton/buddy/a/c;->E:I

    .line 352
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/sec/chaton/buddy/a/c;->c:Ljava/lang/String;

    .line 353
    return-void
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->C:Ljava/lang/String;

    return-object v0
.end method

.method public B()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/util/av;",
            ">;"
        }
    .end annotation

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->D:Ljava/util/ArrayList;

    return-object v0
.end method

.method public C()I
    .locals 1

    .prologue
    .line 563
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->H:I

    return v0
.end method

.method public D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->I:Ljava/lang/String;

    return-object v0
.end method

.method public E()I
    .locals 1

    .prologue
    .line 576
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->E:I

    return v0
.end method

.method public F()I
    .locals 1

    .prologue
    .line 586
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    return v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 360
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->d:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 590
    iput p1, p0, Lcom/sec/chaton/buddy/a/c;->F:I

    .line 591
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 556
    iput-object p1, p0, Lcom/sec/chaton/buddy/a/c;->A:Ljava/util/HashMap;

    .line 557
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 532
    iput-boolean p1, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    .line 533
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 582
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->I:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->e:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->f:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->h:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->i:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->k:Ljava/lang/String;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 412
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->l:Z

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 416
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->m:I

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->n:Ljava/lang/String;

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 424
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->o:Z

    return v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 428
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->r:I

    return v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 432
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->s:I

    return v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 436
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->t:I

    return v0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 440
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->u:I

    return v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 444
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->v:I

    return v0
.end method

.method public s()I
    .locals 1

    .prologue
    .line 448
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->w:I

    return v0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 484
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->x:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BuddyItem [no="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/a/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/a/c;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/a/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isShowRelationship="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pushName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/buddy/a/c;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isNew="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/buddy/a/c;->o:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", relationSend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/a/c;->r:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", relationReceived="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/a/c;->s:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", relationPoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/a/c;->t:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", relationLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/a/c;->u:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", relationIncrease="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/a/c;->v:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", relationRank="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/a/c;->w:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->y:I

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->p:Z

    return v0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 516
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->q:Z

    return v0
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 524
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/a/c;->z:Z

    return v0
.end method

.method public y()I
    .locals 1

    .prologue
    .line 540
    iget v0, p0, Lcom/sec/chaton/buddy/a/c;->B:I

    return v0
.end method

.method public z()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 544
    iget-object v0, p0, Lcom/sec/chaton/buddy/a/c;->A:Ljava/util/HashMap;

    return-object v0
.end method
