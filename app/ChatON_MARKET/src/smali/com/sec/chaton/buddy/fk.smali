.class public Lcom/sec/chaton/buddy/fk;
.super Lcom/sec/common/f/a;
.source "BuddyProfileImageDispatcherTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/widget/ImageView;

.field private c:I

.field private d:I

.field private e:I

.field private i:I

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0, p2}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 36
    const-string v0, "BuddyProfileImageDispatcherTask"

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->a:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->o:Ljava/lang/String;

    .line 75
    iput p3, p0, Lcom/sec/chaton/buddy/fk;->e:I

    .line 76
    iput p4, p0, Lcom/sec/chaton/buddy/fk;->i:I

    .line 78
    iput-object p5, p0, Lcom/sec/chaton/buddy/fk;->k:Ljava/lang/String;

    .line 79
    iput-object p6, p0, Lcom/sec/chaton/buddy/fk;->l:Ljava/lang/String;

    .line 81
    iput-object p1, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    .line 82
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/fk;->m:Z

    .line 83
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/fk;->j:Z

    .line 86
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/buddy/fk;->c:I

    .line 87
    const v0, 0x7f020387

    iput v0, p0, Lcom/sec/chaton/buddy/fk;->d:I

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0, p2}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 36
    const-string v0, "BuddyProfileImageDispatcherTask"

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->a:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->o:Ljava/lang/String;

    .line 93
    iput p3, p0, Lcom/sec/chaton/buddy/fk;->e:I

    .line 94
    iput p4, p0, Lcom/sec/chaton/buddy/fk;->i:I

    .line 96
    iput-object p5, p0, Lcom/sec/chaton/buddy/fk;->k:Ljava/lang/String;

    .line 97
    iput-object p6, p0, Lcom/sec/chaton/buddy/fk;->l:Ljava/lang/String;

    .line 99
    iput-object p1, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    .line 100
    iput-boolean p7, p0, Lcom/sec/chaton/buddy/fk;->m:Z

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/fk;->j:Z

    .line 104
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/buddy/fk;->c:I

    .line 105
    const v0, 0x7f020387

    iput v0, p0, Lcom/sec/chaton/buddy/fk;->d:I

    .line 106
    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    .line 110
    invoke-direct {p0, p2}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 36
    const-string v0, "BuddyProfileImageDispatcherTask"

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->a:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->o:Ljava/lang/String;

    .line 112
    iput p3, p0, Lcom/sec/chaton/buddy/fk;->e:I

    .line 113
    iput p4, p0, Lcom/sec/chaton/buddy/fk;->i:I

    .line 115
    iput-object p5, p0, Lcom/sec/chaton/buddy/fk;->k:Ljava/lang/String;

    .line 116
    iput-object p6, p0, Lcom/sec/chaton/buddy/fk;->l:Ljava/lang/String;

    .line 118
    iput-object p1, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    .line 119
    iput-boolean p7, p0, Lcom/sec/chaton/buddy/fk;->m:Z

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/fk;->j:Z

    .line 121
    iput-boolean p8, p0, Lcom/sec/chaton/buddy/fk;->n:Z

    .line 122
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->o:Ljava/lang/String;

    .line 125
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/buddy/fk;->c:I

    .line 126
    const v0, 0x7f020387

    iput v0, p0, Lcom/sec/chaton/buddy/fk;->d:I

    .line 127
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 131
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 36
    const-string v0, "BuddyProfileImageDispatcherTask"

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->a:Ljava/lang/String;

    .line 38
    iput-object v2, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/buddy/fk;->o:Ljava/lang/String;

    .line 133
    iput v1, p0, Lcom/sec/chaton/buddy/fk;->e:I

    .line 134
    iput v1, p0, Lcom/sec/chaton/buddy/fk;->i:I

    .line 136
    iput-object p2, p0, Lcom/sec/chaton/buddy/fk;->k:Ljava/lang/String;

    .line 137
    iput-object p3, p0, Lcom/sec/chaton/buddy/fk;->l:Ljava/lang/String;

    .line 139
    iput-object v2, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    .line 140
    iput-boolean v1, p0, Lcom/sec/chaton/buddy/fk;->m:Z

    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/buddy/fk;->j:Z

    .line 142
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 3

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->e()Landroid/widget/ImageView;

    move-result-object v1

    .line 164
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/fk;->j:Z

    if-nez v0, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/buddy/fk;->c:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 166
    iget-object v2, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    if-nez v2, :cond_1

    .line 167
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 174
    :goto_0
    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    .line 175
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 179
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 171
    iget-object v1, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 5

    .prologue
    .line 258
    .line 261
    if-eqz p1, :cond_2

    .line 262
    :try_start_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->f()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 263
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    .line 280
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/sec/chaton/buddy/fk;->b:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 284
    :cond_0
    if-eqz p2, :cond_5

    .line 285
    if-eqz v0, :cond_1

    .line 286
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->e()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 299
    :cond_1
    :goto_1
    return-void

    .line 267
    :cond_2
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/fk;->j:Z

    if-nez v0, :cond_4

    .line 268
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/fk;->m:Z

    if-eqz v0, :cond_3

    .line 269
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/buddy/fk;->d:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 272
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->e()Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 296
    :catch_0
    move-exception v0

    .line 297
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 276
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 288
    :cond_5
    if-eqz v0, :cond_1

    .line 289
    :try_start_1
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 291
    new-instance v2, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v0, v3, v1

    invoke-direct {v2, v3}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 292
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->e()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 294
    const/16 v0, 0x64

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 186
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p0, v0, v1}, Lcom/sec/chaton/buddy/fk;->a(Ljava/util/concurrent/Callable;J)V

    .line 187
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 194
    :try_start_0
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/chaton/buddy/fk;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/buddy/fk;->l:Ljava/lang/String;

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v0, v3, v6

    if-nez v0, :cond_1

    .line 197
    :cond_0
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Lcom/sec/common/util/a/a;->a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 200
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/fk;->j:Z

    if-nez v0, :cond_5

    .line 201
    iget-boolean v0, p0, Lcom/sec/chaton/buddy/fk;->m:Z

    if-nez v0, :cond_4

    .line 202
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 210
    :goto_0
    if-eqz v0, :cond_2

    .line 211
    const/16 v2, 0xa0

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 215
    :cond_2
    iget-boolean v2, p0, Lcom/sec/chaton/buddy/fk;->n:Z

    if-eqz v2, :cond_3

    .line 217
    invoke-static {v0}, Lcom/sec/chaton/util/ad;->a(Landroid/graphics/Bitmap;)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    .line 219
    const-string v3, "myprofile.png_"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/ad;->a(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 221
    if-nez v2, :cond_6

    .line 222
    const-string v2, "saveByteArrayOutputStreamToFileForProfile returns null"

    iget-object v3, p0, Lcom/sec/chaton/buddy/fk;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_3
    :goto_1
    return-object v0

    .line 204
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 207
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->k()Landroid/content/Context;

    move-result-object v0

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const/16 v3, 0x42

    const/16 v4, 0x2c

    invoke-static {v0, v2, v3, v4}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 226
    :cond_6
    const/16 v3, 0x120

    const/16 v4, 0x120

    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 228
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/fk;->o:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 229
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/buddy/fk;->o:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Lcom/sec/chaton/util/bt;->b(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 231
    new-instance v3, Ljava/io/File;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 232
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v3, v3, v6

    if-lez v3, :cond_7

    .line 233
    const-string v2, "profile_image_status"

    const-string v3, "updated"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 240
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 241
    goto :goto_1

    .line 235
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "File length is 0:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProfileUploadTask"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 242
    :catch_1
    move-exception v0

    .line 244
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0149

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 245
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v0, v1

    .line 246
    goto/16 :goto_1

    .line 248
    :catch_2
    move-exception v0

    .line 249
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    .line 250
    goto/16 :goto_1

    .line 251
    :catchall_0
    move-exception v0

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 305
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/chaton/buddy/fk;->a(Landroid/view/View;)V

    .line 307
    if-eqz v0, :cond_0

    .line 308
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 310
    :cond_0
    return-void
.end method

.method public e()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 314
    invoke-super {p0}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public f()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 319
    invoke-super {p0}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/chaton/buddy/fk;->e()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method
