.class Lcom/sec/chaton/userprofile/i;
.super Ljava/lang/Object;
.source "BirthdayFragment.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/BirthdayFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/BirthdayFragment;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 5

    .prologue
    .line 464
    const-string v0, ""

    .line 465
    iget-object v0, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->l(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 466
    iget-object v0, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->l(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 467
    iget-object v0, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->l(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p4}, Ljava/util/Calendar;->set(II)V

    .line 468
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 469
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDateSet::year"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " onDateSet::month="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " onDateSet::dayOfMonth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    :cond_0
    const-string v0, "FULL"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->k(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "FULL_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->k(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 473
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-DD"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->l(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 474
    const/16 v0, 0xa

    if-ge p4, v0, :cond_3

    .line 475
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 479
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x7

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 480
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "temp_full ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/userprofile/BirthdayFragment;->b(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 482
    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->c(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 483
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDateSet::mBirthDay"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->d(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " onDateSet::displayTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->m(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->n(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->m(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 492
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->k(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->a(Ljava/lang/String;)V

    .line 494
    return-void

    .line 477
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 485
    :cond_4
    const-string v0, "SHORT"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->k(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "SHORT_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->k(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 486
    :cond_5
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->l(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 487
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MM-dd"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/BirthdayFragment;->l(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 488
    iget-object v2, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->b(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 489
    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->c(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 490
    iget-object v0, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->n(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/i;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->m(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method
