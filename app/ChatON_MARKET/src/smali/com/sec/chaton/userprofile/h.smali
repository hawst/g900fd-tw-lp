.class Lcom/sec/chaton/userprofile/h;
.super Ljava/lang/Object;
.source "BirthdayFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/BirthdayFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/BirthdayFragment;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/sec/chaton/userprofile/h;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 423
    if-eqz p2, :cond_1

    .line 424
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/userprofile/h;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    const-string v1, "FULL"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->a(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 429
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/h;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->b()V

    .line 438
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/h;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/h;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->k(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->a(Ljava/lang/String;)V

    .line 439
    return-void

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/h;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    const-string v1, "FULL_HIDE"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->a(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 431
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/userprofile/h;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    const-string v1, "SHORT"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->a(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 436
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/h;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->c()V

    goto :goto_1

    .line 434
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/h;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    const-string v1, "SHORT_HIDE"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->a(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2
.end method
