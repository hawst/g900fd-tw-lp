.class Lcom/sec/chaton/userprofile/w;
.super Landroid/os/Handler;
.source "CoverstorySampleFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 207
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 208
    iget-object v1, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->e(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->e(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 209
    iget-object v1, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->e(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 211
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v3, :cond_3

    .line 213
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/CoverStorySampleList;

    .line 214
    iget-object v1, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/CoverStorySampleList;->coverstory:Ljava/util/ArrayList;

    iput-object v3, v1, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->a:Ljava/util/ArrayList;

    .line 215
    const-string v1, "get_coverstory_sample_timestamp"

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/CoverStorySampleList;->timestamp:Ljava/lang/Long;

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 216
    invoke-static {}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->b()Lcom/sec/chaton/e/a/u;

    move-result-object v0

    sget v1, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->b:I

    sget-object v3, Lcom/sec/chaton/e/k;->a:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_0

    .line 223
    invoke-static {}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->b()Lcom/sec/chaton/e/a/u;

    move-result-object v0

    sget v1, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->b:I

    sget-object v3, Lcom/sec/chaton/e/k;->a:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 229
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 230
    iget-object v1, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->e(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->e(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 231
    iget-object v1, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->e(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 233
    :cond_4
    const-string v1, "CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    if-nez v0, :cond_5

    .line 235
    const-string v0, "CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD (httpEntry == null)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 238
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v3, :cond_0

    .line 239
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;

    .line 240
    iget-object v1, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/userprofile/x;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;->fileurl:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/userprofile/w;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->f(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/sec/chaton/userprofile/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0xbbc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
