.class public Lcom/sec/chaton/userprofile/SetCoverstoryFragment;
.super Landroid/support/v4/app/Fragment;
.source "SetCoverstoryFragment.java"

# interfaces
.implements Lcom/sec/common/f/f;


# static fields
.field private static b:Ljava/lang/String;

.field private static n:Lcom/sec/chaton/widget/j;


# instance fields
.field a:Landroid/view/View$OnClickListener;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/Button;

.field private g:Lcom/sec/common/f/c;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Landroid/app/ProgressDialog;

.field private o:Lcom/sec/chaton/d/i;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private s:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 59
    const-string v0, "updated"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->p:Ljava/lang/String;

    .line 61
    const-string v0, "coverstory_sample_changed"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->q:Ljava/lang/String;

    .line 62
    const-string v0, "coverstory_not_changed"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->r:Ljava/lang/String;

    .line 114
    new-instance v0, Lcom/sec/chaton/userprofile/ch;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/ch;-><init>(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->a:Landroid/view/View$OnClickListener;

    .line 190
    new-instance v0, Lcom/sec/chaton/userprofile/ci;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/ci;-><init>(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->s:Landroid/os/Handler;

    .line 70
    iput-object p1, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->i:Ljava/lang/String;

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->j:Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->k:Ljava/lang/String;

    .line 74
    iput-boolean p4, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->l:Z

    .line 75
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SetCoverstoryFragment url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", filename : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;Lcom/sec/chaton/d/i;)Lcom/sec/chaton/d/i;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->o:Lcom/sec/chaton/d/i;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->j:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .locals 8

    .prologue
    .line 242
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EXTRA_NAME_URL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / EXTRA_NAME_FILENAME : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / SetCoverstoryActivity.EXTRA_NAME_ID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_0
    new-instance v0, Lcom/sec/chaton/poston/a;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->i:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/chaton/poston/a;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->h:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->j:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->k:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-object v1, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->g:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 247
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->m:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->s:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Lcom/sec/chaton/d/i;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->o:Lcom/sec/chaton/d/i;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 262
    const-string v0, "CoverStoryDispatcherTask fail"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    return-void
.end method

.method public b(Landroid/view/View;Lcom/sec/common/f/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->l:Z

    if-nez v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->f:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 256
    :goto_0
    const-string v0, "CoverStoryDispatcherTask success"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->f:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 85
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 89
    const v0, 0x7f0300c7

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->c:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->c:Landroid/view/View;

    const v1, 0x7f070376

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->e:Landroid/widget/ImageView;

    .line 94
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0404

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->c:Landroid/view/View;

    const v2, 0x7f070378

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->d:Landroid/view/View;

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->d:Landroid/view/View;

    const v2, 0x7f0702d7

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->f:Landroid/widget/Button;

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->f:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->f:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->n:Lcom/sec/chaton/widget/j;

    .line 103
    sget-object v0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->n:Lcom/sec/chaton/widget/j;

    const v1, 0x7f0b019c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->m:Landroid/app/ProgressDialog;

    .line 105
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->g:Lcom/sec/common/f/c;

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->g:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/f;)V

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/random/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->h:Ljava/lang/String;

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SetCoverstoryFragment [dirInternalPath] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->b()V

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->c:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 170
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->m:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->o:Lcom/sec/chaton/d/i;

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->o:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->h()V

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->o:Lcom/sec/chaton/d/i;

    .line 180
    :cond_1
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 184
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->g:Lcom/sec/common/f/c;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->g:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 188
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 151
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 153
    sget-object v0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->n:Lcom/sec/chaton/widget/j;

    if-nez v0, :cond_0

    .line 154
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->n:Lcom/sec/chaton/widget/j;

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->m:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 158
    sget-object v0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->n:Lcom/sec/chaton/widget/j;

    const v1, 0x7f0b019c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->m:Landroid/app/ProgressDialog;

    .line 161
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 162
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 165
    :cond_2
    return-void
.end method
