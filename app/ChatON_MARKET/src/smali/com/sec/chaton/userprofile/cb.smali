.class Lcom/sec/chaton/userprofile/cb;
.super Ljava/lang/Object;
.source "ProfileImageHistoryFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V
    .locals 0

    .prologue
    .line 599
    iput-object p1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 602
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mProfileListView Position: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;

    .line 605
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mProfileListView view: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z

    move-result v1

    if-nez v1, :cond_2

    const-wide/16 v1, 0x0

    cmp-long v1, p4, v1

    if-nez v1, :cond_2

    .line 609
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->f(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 610
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/a/d;

    move-result-object v0

    if-nez v0, :cond_0

    .line 611
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 612
    const v1, 0x7f0b0204

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b027c

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/userprofile/cc;

    invoke-direct {v3, p0}, Lcom/sec/chaton/userprofile/cc;-><init>(Lcom/sec/chaton/userprofile/cb;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 618
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 620
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 664
    :goto_0
    return-void

    .line 623
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0, v5}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Z)Z

    .line 624
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a()V

    goto :goto_0

    .line 629
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->j(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 631
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v2, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 632
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v2, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 635
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-virtual {v1, v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->setHasOptionsMenu(Z)V

    .line 636
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->k(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 637
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v1}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 638
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 639
    const-string v1, "mIsFirstOptionMenuOpen = true"

    sget-object v2, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v5}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Z)Z

    .line 646
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z

    move-result v1

    if-ne v1, v3, :cond_5

    .line 647
    add-int/lit8 p3, p3, 0x1

    .line 651
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, p3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;I)I

    .line 652
    iget-object v1, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    sput-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->a:Ljava/lang/String;

    .line 654
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    const v4, 0x7f0b017f

    invoke-virtual {v3, v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->f(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 656
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)V

    .line 658
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->j(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 659
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/cb;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-static {v1, v2, v3, v5}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 660
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "item.id :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dirCachePath+FullfileName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
