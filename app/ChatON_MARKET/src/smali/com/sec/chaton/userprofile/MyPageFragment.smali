.class public Lcom/sec/chaton/userprofile/MyPageFragment;
.super Landroid/support/v4/app/Fragment;
.source "MyPageFragment.java"

# interfaces
.implements Lcom/sec/chaton/by;
.implements Lcom/sec/chaton/poston/j;


# static fields
.field private static O:Ljava/lang/String;

.field public static final a:Ljava/lang/String;

.field private static ab:Ljava/lang/String;

.field private static ac:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/ImageView;

.field private C:Landroid/widget/ImageView;

.field private D:Landroid/widget/ImageView;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Landroid/widget/LinearLayout;

.field private final I:Ljava/lang/String;

.field private final J:Ljava/lang/String;

.field private final K:Ljava/lang/String;

.field private final L:Ljava/lang/String;

.field private final M:Ljava/lang/String;

.field private final N:Ljava/lang/String;

.field private final P:I

.field private final Q:I

.field private final R:I

.field private final S:I

.field private final T:I

.field private final U:I

.field private final V:I

.field private final W:I

.field private final X:I

.field private final Y:I

.field private final Z:I

.field private aA:Ljava/io/File;

.field private aB:Ljava/lang/String;

.field private aC:Ljava/io/File;

.field private aD:Landroid/graphics/drawable/BitmapDrawable;

.field private aE:I

.field private aF:I

.field private aG:Ljava/io/File;

.field private aH:Landroid/net/Uri;

.field private aI:Lcom/sec/chaton/userprofile/bm;

.field private aJ:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/userprofile/t;",
            ">;"
        }
    .end annotation
.end field

.field private aK:Lcom/sec/chaton/widget/j;

.field private aL:Landroid/widget/FrameLayout;

.field private aM:Landroid/widget/FrameLayout;

.field private aN:Z

.field private aO:Lcom/coolots/sso/a/a;

.field private aP:Landroid/view/View$OnClickListener;

.field private aQ:Lcom/sec/chaton/util/bs;

.field private aR:Landroid/os/Handler;

.field private aS:Landroid/content/DialogInterface$OnClickListener;

.field private aT:Landroid/content/DialogInterface$OnClickListener;

.field private aU:Landroid/content/DialogInterface$OnClickListener;

.field private aV:Landroid/content/DialogInterface$OnClickListener;

.field private aW:Landroid/os/Handler;

.field private aa:Z

.field private ad:Lcom/sec/chaton/widget/SelectableImageView;

.field private ae:Lcom/sec/chaton/widget/SelectableImageView;

.field private af:Lcom/sec/chaton/widget/SelectableImageView;

.field private ag:Lcom/sec/chaton/widget/SelectableImageView;

.field private ah:Landroid/widget/ImageView;

.field private ai:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/ProfileImage;",
            ">;"
        }
    .end annotation
.end field

.field private aj:Lcom/sec/common/f/c;

.field private ak:Lcom/sec/chaton/userprofile/bq;

.field private al:Z

.field private am:Ljava/io/File;

.field private an:Ljava/lang/String;

.field private ao:Ljava/lang/String;

.field private final ap:I

.field private aq:Ljava/lang/String;

.field private ar:Ljava/lang/String;

.field private as:Z

.field private at:Ljava/io/File;

.field private au:Ljava/io/File;

.field private av:Z

.field private aw:Z

.field private ax:Landroid/net/Uri;

.field private ay:Z

.field private az:Lcom/sec/chaton/util/bp;

.field final b:Ljava/lang/String;

.field c:Landroid/widget/AbsListView$OnScrollListener;

.field private d:Z

.field private e:Z

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Landroid/app/ProgressDialog;

.field private l:Landroid/app/ProgressDialog;

.field private m:Lcom/sec/chaton/d/w;

.field private n:Lcom/sec/chaton/d/i;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/LinearLayout;

.field private q:Lcom/sec/common/a/d;

.field private r:Landroid/widget/LinearLayout;

.field private s:Landroid/widget/LinearLayout;

.field private t:Landroid/view/View;

.field private u:Landroid/widget/ListView;

.field private v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/poston/k;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcom/sec/chaton/poston/d;

.field private x:Lcom/sec/chaton/d/v;

.field private y:Landroid/widget/FrameLayout;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127
    const-class v0, Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    .line 161
    const-string v0, "mypage"

    sput-object v0, Lcom/sec/chaton/userprofile/MyPageFragment;->O:Ljava/lang/String;

    .line 185
    const-string v0, "profile_f_mine_"

    sput-object v0, Lcom/sec/chaton/userprofile/MyPageFragment;->ab:Ljava/lang/String;

    .line 186
    const-string v0, "profile_t_mine_"

    sput-object v0, Lcom/sec/chaton/userprofile/MyPageFragment;->ac:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 116
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 118
    iput-boolean v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->e:Z

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->v:Ljava/util/ArrayList;

    .line 155
    const-string v0, "deleted"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->I:Ljava/lang/String;

    .line 156
    const-string v0, "updated"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->J:Ljava/lang/String;

    .line 157
    const-string v0, "profile"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->K:Ljava/lang/String;

    .line 158
    const-string v0, "coverstory"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->L:Ljava/lang/String;

    .line 159
    const-string v0, "coverstory_sample_changed"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->M:Ljava/lang/String;

    .line 160
    const-string v0, "coverstory_not_changed"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->N:Ljava/lang/String;

    .line 163
    iput v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->P:I

    .line 164
    iput v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->Q:I

    .line 165
    iput v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->R:I

    .line 166
    iput v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->S:I

    .line 168
    iput v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->T:I

    .line 169
    iput v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->U:I

    .line 170
    iput v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->V:I

    .line 171
    iput v5, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->W:I

    .line 172
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->X:I

    .line 173
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->Y:I

    .line 174
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->Z:I

    .line 198
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->an:Ljava/lang/String;

    .line 200
    iput v5, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ap:I

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aq:Ljava/lang/String;

    .line 205
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->b:Ljava/lang/String;

    .line 206
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/profile/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->au:Ljava/io/File;

    .line 207
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->av:Z

    .line 208
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aw:Z

    .line 210
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ay:Z

    .line 216
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aB:Ljava/lang/String;

    .line 217
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/coverstory/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aC:Ljava/io/File;

    .line 237
    new-instance v0, Lcom/sec/chaton/userprofile/am;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/am;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aP:Landroid/view/View$OnClickListener;

    .line 749
    new-instance v0, Lcom/sec/chaton/userprofile/az;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/az;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aQ:Lcom/sec/chaton/util/bs;

    .line 892
    new-instance v0, Lcom/sec/chaton/userprofile/ba;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/ba;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aR:Landroid/os/Handler;

    .line 1920
    new-instance v0, Lcom/sec/chaton/userprofile/bi;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bi;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aS:Landroid/content/DialogInterface$OnClickListener;

    .line 1933
    new-instance v0, Lcom/sec/chaton/userprofile/bj;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bj;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aT:Landroid/content/DialogInterface$OnClickListener;

    .line 1944
    new-instance v0, Lcom/sec/chaton/userprofile/bk;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bk;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aU:Landroid/content/DialogInterface$OnClickListener;

    .line 1980
    new-instance v0, Lcom/sec/chaton/userprofile/bl;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bl;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aV:Landroid/content/DialogInterface$OnClickListener;

    .line 2298
    new-instance v0, Lcom/sec/chaton/userprofile/an;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/an;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aW:Landroid/os/Handler;

    .line 2525
    new-instance v0, Lcom/sec/chaton/userprofile/au;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/au;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->c:Landroid/widget/AbsListView$OnScrollListener;

    return-void
.end method

.method private A()V
    .locals 1

    .prologue
    .line 2608
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aD:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 2609
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aD:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2610
    if-eqz v0, :cond_0

    .line 2611
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2614
    :cond_0
    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/userprofile/MyPageFragment;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->u()V

    return-void
.end method

.method static synthetic B(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aW:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic C(Lcom/sec/chaton/userprofile/MyPageFragment;)Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->h:Z

    return v0
.end method

.method static synthetic D(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic E(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic F(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/d/v;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->x:Lcom/sec/chaton/d/v;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/chaton/userprofile/bm;)Lcom/sec/chaton/userprofile/bm;
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aI:Lcom/sec/chaton/userprofile/bm;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->q:Lcom/sec/common/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/io/File;)Ljava/io/File;
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->am:Ljava/io/File;

    return-object p1
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 245
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    :goto_0
    return-void

    .line 248
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->at:Ljava/io/File;

    const-string v2, "myprofile.png_"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 249
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 250
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "photoFile="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "profile_small_image1"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 254
    iget-boolean v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->as:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "profile_image_status"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "deleted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 255
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->e()V

    goto :goto_0

    .line 257
    :cond_3
    iput-boolean v5, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->al:Z

    .line 258
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/userprofile/bn;

    invoke-interface {v0, p1, v5}, Lcom/sec/chaton/userprofile/bn;->a(IZ)V

    goto :goto_0
.end method

.method private a(ILcom/sec/chaton/io/entry/inner/ProfileImage;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2617
    packed-switch p1, :pswitch_data_0

    .line 2628
    :goto_0
    return-void

    .line 2619
    :pswitch_0
    const-string v3, "profile_small_image1"

    iget-object v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V

    goto :goto_0

    .line 2622
    :pswitch_1
    const-string v3, "profile_small_image2"

    iget-object v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V

    goto :goto_0

    .line 2625
    :pswitch_2
    const-string v3, "profile_small_image3"

    iget-object v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V

    goto :goto_0

    .line 2617
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V
    .locals 6

    .prologue
    .line 2631
    .line 2632
    if-eqz p5, :cond_2

    .line 2633
    const-string v2, "profile_f_"

    .line 2637
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mine_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2638
    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILjava/lang/String;)V

    .line 2639
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2640
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, ""

    invoke-virtual {v0, p3, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2641
    :cond_0
    iget-object v0, p2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-static {p3, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2643
    iget-object v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aq:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p2

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;Z)V

    .line 2650
    :cond_1
    return-void

    .line 2635
    :cond_2
    const-string v2, "profile_t_"

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 1566
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_0

    .line 1567
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[setProfileSmallImages] MyPageFragment initialize() - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1569
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1571
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1572
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[setProfileSmallImages] photoFile="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1575
    :cond_1
    packed-switch p1, :pswitch_data_0

    move-object v1, v0

    .line 1599
    :goto_0
    if-eqz v1, :cond_3

    .line 1600
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_5

    .line 1601
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 1602
    const-string v0, "[setProfileSmallImages] photoFile.exists() && (photoFile.length() > 0) "

    sget-object v3, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    :cond_2
    if-nez p1, :cond_4

    .line 1606
    new-instance v0, Lcom/sec/chaton/userprofile/ck;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/chaton/userprofile/ck;-><init>(Ljava/io/File;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1610
    :goto_1
    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    invoke-virtual {v2, v1, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1622
    :cond_3
    :goto_2
    return-void

    .line 1577
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    .line 1578
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "profile_small_image0"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1582
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    .line 1583
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "profile_small_image1"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1587
    :pswitch_2
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    .line 1588
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "profile_small_image2"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1592
    :pswitch_3
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    .line 1593
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "profile_small_image3"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1608
    :cond_4
    new-instance v0, Lcom/sec/chaton/userprofile/ck;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/chaton/userprofile/ck;-><init>(Ljava/io/File;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    goto :goto_1

    .line 1612
    :cond_5
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_6

    .line 1613
    const-string v1, "[setProfileSmallImages] photoFile not exist "

    sget-object v2, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1616
    :cond_6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1618
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    goto :goto_2

    .line 1575
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 6

    .prologue
    .line 2550
    new-instance v1, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ar:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "mycoverstory."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2551
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    .line 2554
    :try_start_0
    new-instance v0, Lcom/sec/common/e/b;

    invoke-direct {v0}, Lcom/sec/common/e/b;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sec/common/e/b;->a(Ljava/io/File;)Lcom/sec/common/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/e/b;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2555
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2556
    const-string v0, "mypage_coverstory_state"

    const-string v2, "updated"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2579
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 2580
    const-string v0, " Coverstory setCoverstoryAndDownload() MYPAGE_STATUS_UPDATED set"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2605
    :cond_1
    return-void

    .line 2557
    :catch_0
    move-exception v0

    .line 2559
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 2560
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2561
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 2563
    :cond_2
    const-string v0, "coverstory_metaid"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2564
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2565
    const-string v0, "[CoverStory] #3# PrefConst.PREF_MYPAGE_COVERSTORY_METAID set 1"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2571
    :cond_3
    const-string v0, "coverstory_metaid"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2572
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2573
    const-string v0, "[CoverStory] #4# PrefConst.PREF_MYPAGE_COVERSTORY_METAID set 1"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcom/sec/chaton/a/a/f;)V
    .locals 11

    .prologue
    const/4 v7, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1334
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetProfileImageList;

    .line 1335
    if-nez v0, :cond_0

    .line 1440
    :goto_0
    return-void

    .line 1338
    :cond_0
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->profileimagelist:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    .line 1339
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1340
    :cond_1
    const-string v0, "profile_image_status"

    const-string v2, "deleted"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1341
    iput-boolean v5, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->as:Z

    .line 1342
    invoke-direct {p0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    .line 1343
    const-string v0, "profile_small_image0"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1346
    :cond_2
    iput-boolean v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->as:Z

    .line 1349
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x4

    if-le v0, v2, :cond_b

    .line 1350
    const/4 v0, 0x4

    move v6, v0

    .line 1354
    :goto_1
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->x()V

    .line 1355
    const-string v2, "1"

    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1356
    const-string v0, "profile_image_status"

    const-string v2, "updated"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1357
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x4

    if-ge v0, v2, :cond_3

    .line 1358
    invoke-direct {p0, v7}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    .line 1359
    const-string v0, "profile_small_image3"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1361
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v7, :cond_4

    .line 1362
    invoke-direct {p0, v4}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    .line 1363
    const-string v0, "profile_small_image2"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v4, :cond_5

    .line 1366
    invoke-direct {p0, v5}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    .line 1367
    const-string v0, "profile_small_image1"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_2
    move v7, v1

    .line 1386
    :goto_3
    if-ge v7, v6, :cond_12

    .line 1387
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    .line 1389
    const-string v0, "1"

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1391
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aq:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1392
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1393
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 1396
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aq:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/userprofile/MyPageFragment;->ac:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1397
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aq:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/userprofile/MyPageFragment;->ab:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1398
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v8, "profile_image_update_client"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1399
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v4, :cond_7

    .line 1400
    const-string v4, "Save Profile image to Local Directory"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1406
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ar:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "myprofile.png_"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1407
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ar:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->E:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".jpeg_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1408
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1409
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1410
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_8

    .line 1411
    invoke-static {v8, v0}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    :cond_8
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1414
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1415
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1416
    invoke-static {v4, v3}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    :cond_9
    const-string v0, "profile_image_update_client"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1421
    :cond_a
    const-string v3, "profile_small_image0"

    iget-object v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Landroid/widget/ImageView;Z)V

    .line 1386
    :goto_4
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_3

    .line 1352
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v6, v0

    goto/16 :goto_1

    .line 1370
    :cond_c
    invoke-direct {p0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    .line 1371
    const-string v0, "profile_image_status"

    const-string v2, "deleted"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1372
    const-string v0, "profile_small_image0"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1373
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v7, :cond_d

    .line 1374
    invoke-direct {p0, v7}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    .line 1375
    const-string v0, "profile_small_image3"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1377
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v4, :cond_e

    .line 1378
    invoke-direct {p0, v4}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    .line 1379
    const-string v0, "profile_small_image2"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1381
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v5, :cond_5

    .line 1382
    invoke-direct {p0, v5}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    .line 1383
    const-string v0, "profile_small_image1"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1423
    :cond_f
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_10

    .line 1424
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profileimageurl: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1426
    :cond_10
    const-string v3, "1"

    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1427
    invoke-direct {p0, v7, v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;)V

    goto/16 :goto_4

    .line 1429
    :cond_11
    add-int/lit8 v0, v7, 0x1

    invoke-direct {p0, v0, v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILcom/sec/chaton/io/entry/inner/ProfileImage;)V

    goto/16 :goto_4

    .line 1434
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/SelectableImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_13

    .line 1435
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ah:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1437
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ah:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private a(Lcom/sec/chaton/io/entry/inner/ProfileImage;Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;Z)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x258

    .line 2433
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ak:Lcom/sec/chaton/userprofile/bq;

    if-eqz v0, :cond_0

    .line 2434
    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ak:Lcom/sec/chaton/userprofile/bq;

    .line 2436
    :cond_0
    new-instance v0, Lcom/sec/chaton/userprofile/bq;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ao:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "&size=800"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "mine_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aR:Landroid/os/Handler;

    move v4, v3

    move-object v5, p3

    move v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/userprofile/bq;-><init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLandroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ak:Lcom/sec/chaton/userprofile/bq;

    .line 2437
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ak:Lcom/sec/chaton/userprofile/bq;

    invoke-virtual {v0, p4, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 2438
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;I)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Landroid/widget/ImageView;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/chaton/a/a/f;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(Lcom/sec/chaton/a/a/f;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 887
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    const-string v0, "coverstory_contentid"

    invoke-static {v0, p1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    return-void
.end method

.method private a(ILandroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2207
    .line 2209
    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 2224
    :goto_0
    return v0

    .line 2211
    :pswitch_0
    if-eqz p2, :cond_0

    const-string v2, "restart"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 2212
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->v()V

    goto :goto_0

    .line 2217
    :pswitch_1
    if-eqz p2, :cond_0

    const-string v2, "restart"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 2218
    const-string v1, "profile"

    invoke-direct {p0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 2209
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyPageFragment;Z)Z
    .locals 0

    .prologue
    .line 116
    iput-boolean p1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->al:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/MyPageFragment;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aH:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/io/File;)Ljava/io/File;
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aG:Ljava/io/File;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->an:Ljava/lang/String;

    return-object p1
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 2399
    packed-switch p1, :pswitch_data_0

    .line 2430
    :cond_0
    :goto_0
    return-void

    .line 2402
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020387

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2403
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 2405
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2425
    :catch_0
    move-exception v0

    .line 2426
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 2427
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 2408
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020387

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2409
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    .line 2410
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    goto :goto_0

    .line 2413
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020387

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2414
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    .line 2415
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    goto :goto_0

    .line 2418
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020387

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2419
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    .line 2420
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 2399
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(Lcom/sec/chaton/a/a/f;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1444
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_1

    .line 1445
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPostONList;

    .line 1446
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1447
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/PostONList;->hasmore:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->i:Ljava/lang/String;

    .line 1448
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPostONList;->postonlist:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PostONList;->endtime:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->j:Ljava/lang/String;

    .line 1450
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->y()V

    .line 1472
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->p()V

    .line 1473
    iput-boolean v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->h:Z

    .line 1474
    return-void

    .line 1452
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->l:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1453
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1460
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1461
    const/4 v1, -0x3

    if-eq v1, v0, :cond_3

    const/4 v1, -0x2

    if-ne v1, v0, :cond_4

    .line 1462
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1465
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/MyPageFragment;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->n()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/MyPageFragment;I)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/chaton/a/a/f;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/a/a/f;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/MyPageFragment;Z)Z
    .locals 0

    .prologue
    .line 116
    iput-boolean p1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->h:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/MyPageFragment;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->l()V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->e(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1757
    .line 1759
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/random/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1760
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ar:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mycoverstory."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1762
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 1763
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dirInternalPath : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ar:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "copyCoverStoryforMyProfile"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1764
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "myCoverstoryPath : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "copyCoverStoryforMyProfile"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1768
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".jpg"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1769
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    .line 1770
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1771
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 1772
    invoke-static {v1, v2}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1782
    :cond_1
    :goto_0
    return-void

    .line 1774
    :catch_0
    move-exception v0

    .line 1776
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1777
    :catch_1
    move-exception v0

    .line 1779
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/MyPageFragment;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->m()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->f(Ljava/lang/String;)V

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 8

    .prologue
    const v3, 0x7f0b002b

    const/4 v2, 0x1

    .line 1864
    .line 1865
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->g()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1866
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_1

    .line 1867
    const-string v0, "[deleteTempFolder] External Storage Is Not Available or Writable!"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1869
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0, v3}, Lcom/sec/chaton/userprofile/MyPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1894
    :cond_2
    :goto_0
    return-void

    .line 1872
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v1

    .line 1873
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 1874
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0, v3}, Lcom/sec/chaton/userprofile/MyPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1878
    :cond_4
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1879
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 1880
    if-eqz v2, :cond_2

    .line 1881
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 1882
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    .line 1883
    sget-boolean v6, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v6, :cond_5

    .line 1884
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Delete File] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1881
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1888
    :catch_0
    move-exception v0

    .line 1889
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 1890
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/lang/String;)Lcom/sec/chaton/userprofile/bm;
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->g(Ljava/lang/String;)Lcom/sec/chaton/userprofile/bm;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/userprofile/MyPageFragment;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->k()V

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0b0414

    const/4 v2, 0x0

    .line 1995
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1996
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1997
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1999
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2014
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ay:Z

    .line 2015
    return-void

    .line 2002
    :cond_1
    :try_start_0
    const-string v1, "profile"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2003
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2007
    :catch_0
    move-exception v0

    .line 2008
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2009
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 2010
    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 2005
    :cond_2
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/chaton/userprofile/MyPageFragment;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->q()V

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2244
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2245
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2246
    const-string v1, "output"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2247
    const-string v1, "outputFormat"

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2250
    :try_start_0
    const-string v1, "coverstory"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2251
    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2263
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ay:Z

    .line 2264
    return-void

    .line 2253
    :cond_1
    const/4 v1, 0x5

    :try_start_1
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2255
    :catch_0
    move-exception v0

    .line 2256
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2257
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 2258
    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->l:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private g(Ljava/lang/String;)Lcom/sec/chaton/userprofile/bm;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2362
    new-instance v2, Lcom/sec/chaton/userprofile/bm;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/bm;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    .line 2364
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2365
    if-eqz v3, :cond_1

    move v0, v1

    .line 2366
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 2367
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " strArray["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2366
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2369
    :cond_0
    array-length v0, v3

    const/4 v4, 0x3

    if-ne v0, v4, :cond_1

    .line 2370
    const-string v0, "/file"

    .line 2372
    aget-object v4, v3, v1

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 2373
    aget-object v5, v3, v1

    invoke-virtual {v5, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/chaton/userprofile/bm;->a:Ljava/lang/String;

    .line 2374
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v1, v3, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v1, v3, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/chaton/userprofile/bm;->b:Ljava/lang/String;

    .line 2376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " Coverstory Added host ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v2, Lcom/sec/chaton/userprofile/bm;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] metacontents : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v2, Lcom/sec/chaton/userprofile/bm;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2379
    :cond_1
    return-object v2
.end method

.method static synthetic h(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->r:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/sec/chaton/userprofile/MyPageFragment;->O:Ljava/lang/String;

    return-object v0
.end method

.method private h(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2788
    const-string v0, ""

    .line 2790
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/q;->a()Ljava/lang/String;

    move-result-object v0

    .line 2792
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2793
    new-instance v1, Lcom/sec/chaton/util/a;

    invoke-static {v0}, Lcom/sec/chaton/util/a;->b(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v0}, Lcom/sec/chaton/util/a;->c(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/util/a;-><init>([B[B)V

    .line 2797
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/a;->b([B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/a;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2802
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 2803
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "paramBefore: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, ", paramAfter: "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2805
    :cond_0
    return-object v0

    .line 2795
    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Fail in getting a key"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2798
    :catch_0
    move-exception v0

    .line 2799
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Encryption Error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic i(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/d/w;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->m:Lcom/sec/chaton/d/w;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 589
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->az:Lcom/sec/chaton/util/bp;

    if-nez v0, :cond_0

    .line 590
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j()V

    .line 598
    :goto_0
    return-void

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->az:Lcom/sec/chaton/util/bp;

    const-string v1, "get_profile_information"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 593
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->az:Lcom/sec/chaton/util/bp;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bp;->a()V

    goto :goto_0

    .line 595
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->p()V

    goto :goto_0
.end method

.method static synthetic j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->k:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private j()V
    .locals 6

    .prologue
    .line 745
    new-instance v0, Lcom/sec/chaton/util/bp;

    invoke-direct {v0}, Lcom/sec/chaton/util/bp;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->az:Lcom/sec/chaton/util/bp;

    .line 746
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->az:Lcom/sec/chaton/util/bp;

    const-string v1, "get_profile_information"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aQ:Lcom/sec/chaton/util/bs;

    const-wide/16 v3, 0x0

    const-string v5, "last_sync_time_get_mypage_information"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/util/bp;->a(Ljava/lang/String;Lcom/sec/chaton/util/bs;JLjava/lang/String;)V

    .line 747
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->am:Ljava/io/File;

    return-object v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 849
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->E:Ljava/lang/String;

    .line 850
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Push Name"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->F:Ljava/lang/String;

    .line 851
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "status_message"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->G:Ljava/lang/String;

    .line 852
    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/widget/SelectableImageView;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 856
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 857
    const-string v0, "setPostONList()"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->s:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 860
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->r:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 861
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->h:Z

    .line 862
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->x:Lcom/sec/chaton/d/v;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/v;->a(Ljava/lang/String;)V

    .line 863
    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/widget/SelectableImageView;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 866
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 867
    const-string v0, "getMyProfileInfo()"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->m:Lcom/sec/chaton/d/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->c(Ljava/lang/String;)V

    .line 870
    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->C:Landroid/widget/ImageView;

    return-object v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 873
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 874
    const-string v0, "getProfileImageList()"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->m:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->d()V

    .line 877
    return-void
.end method

.method static synthetic o(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aG:Ljava/io/File;

    return-object v0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 880
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 881
    const-string v0, " Coverstory getCoverStory() called"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->a()V

    .line 884
    return-void
.end method

.method static synthetic p(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aH:Landroid/net/Uri;

    return-object v0
.end method

.method private p()V
    .locals 15

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1252
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1253
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "ACTIVITY_PURPOSE_CALL_MYPAGE"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1254
    const/16 v2, 0x16

    if-ne v0, v2, :cond_1

    .line 1330
    :cond_0
    :goto_0
    return-void

    .line 1259
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1263
    :try_start_0
    invoke-static {v0}, Lcom/sec/chaton/e/a/z;->a(Landroid/content/ContentResolver;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v13

    .line 1265
    if-nez v13, :cond_2

    .line 1326
    if-eqz v13, :cond_0

    .line 1327
    :goto_1
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1268
    :cond_2
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_3

    .line 1269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PostON list count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1272
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->s:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1274
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_6

    .line 1275
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->r:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    .line 1276
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->r:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1278
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->s:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1279
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    if-eqz v0, :cond_5

    .line 1280
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/d;->notifyDataSetChanged()V

    .line 1326
    :cond_5
    :goto_2
    if-eqz v13, :cond_0

    goto :goto_1

    .line 1284
    :cond_6
    :goto_3
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1285
    const-string v0, "buddy_no"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1287
    const-string v0, "joined_name"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1288
    const-string v0, "poston"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1289
    const-string v0, "timestamp"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1290
    const-string v0, "unread_comment_count"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1291
    const-string v0, "read_comment_count"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1292
    const-string v0, "isread"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1293
    const-string v0, "multimedia_list"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1294
    const-string v1, "poston_id"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1295
    const-string v1, "joined_no"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1296
    invoke-static {v0}, Lcom/sec/chaton/e/a/z;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 1297
    iget-object v14, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->v:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/poston/k;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->E:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v12}, Lcom/sec/chaton/poston/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1298
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_7

    .line 1299
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PostONMyTable.KEY_BUDDY_NO: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "KEY_JOINED_NAME: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "KEY_POSTON: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1302
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->s:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    .line 1321
    :catch_0
    move-exception v0

    move-object v1, v13

    .line 1322
    :goto_4
    :try_start_2
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_8

    .line 1323
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1326
    :cond_8
    if-eqz v1, :cond_0

    .line 1327
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1305
    :cond_9
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->r:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_a

    .line 1306
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->i:Ljava/lang/String;

    if-eqz v0, :cond_c

    const-string v0, "true"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1307
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->r:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1312
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    if-nez v0, :cond_d

    .line 1313
    new-instance v0, Lcom/sec/chaton/poston/d;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->v:Ljava/util/ArrayList;

    const v4, 0x7f03012a

    iget-object v5, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/poston/d;-><init>(Landroid/widget/ListView;Landroid/content/Context;Ljava/util/ArrayList;ILcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    .line 1315
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/poston/d;->a(Lcom/sec/chaton/poston/j;)V

    .line 1316
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 1326
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v13, :cond_b

    .line 1327
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1326
    :cond_b
    throw v0

    .line 1309
    :cond_c
    :try_start_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->r:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_5

    .line 1318
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0}, Lcom/sec/chaton/poston/d;->notifyDataSetChanged()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 1326
    :catchall_1
    move-exception v0

    move-object v13, v1

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v13, v1

    goto :goto_6

    .line 1321
    :catch_1
    move-exception v0

    goto :goto_4
.end method

.method static synthetic q(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aA:Ljava/io/File;

    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 1478
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aO:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1479
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->B:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1485
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->z:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1493
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->G:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1494
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->A:Landroid/widget/TextView;

    const v1, 0x7f0b0400

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1498
    :goto_1
    return-void

    .line 1482
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->B:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1496
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->A:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic r(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ar:Ljava/lang/String;

    return-object v0
.end method

.method private r()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 1502
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->as:Z

    .line 1503
    const-string v0, "profile_image_update_client"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1507
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_small_image0"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1508
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1509
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aq:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "profile_f_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mine_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v5, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILjava/lang/String;)V

    .line 1512
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_small_image1"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1513
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1514
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v1, v5}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    .line 1515
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aq:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "profile_t_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mine_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v3, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILjava/lang/String;)V

    .line 1520
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "profile_small_image2"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1521
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1522
    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v2, v5}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    .line 1523
    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aq:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "profile_t_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mine_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v2, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILjava/lang/String;)V

    .line 1528
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "profile_small_image3"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1529
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1530
    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v2, v5}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    .line 1531
    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aq:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "profile_t_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mine_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v2, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILjava/lang/String;)V

    .line 1536
    :goto_2
    iget-boolean v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->as:Z

    if-eqz v1, :cond_2

    .line 1537
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "profile_image_status"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1539
    const-string v2, "deleted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    if-eqz v0, :cond_2

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1543
    :cond_2
    return-void

    .line 1517
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v1, v6}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1525
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v1, v6}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    goto :goto_1

    .line 1533
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v1, v6}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method static synthetic s(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/userprofile/bm;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aI:Lcom/sec/chaton/userprofile/bm;

    return-object v0
.end method

.method private s()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1546
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 1547
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MyPageFragment initialize() - img status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "profile_image_status"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1549
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_image_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "updated"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1550
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->at:Ljava/io/File;

    const-string v2, "myprofile.png_"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1551
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1552
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "photoFile="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1554
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1555
    new-instance v1, Lcom/sec/chaton/userprofile/ck;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/sec/chaton/userprofile/ck;-><init>(Ljava/io/File;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1556
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1563
    :goto_0
    return-void

    .line 1558
    :cond_2
    invoke-direct {p0, v4}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    goto :goto_0

    .line 1561
    :cond_3
    invoke-direct {p0, v4}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    goto :goto_0
.end method

.method static synthetic t(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/d/i;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    return-object v0
.end method

.method private t()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1907
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 1908
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1909
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->av:Z

    .line 1910
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aw:Z

    .line 1918
    :goto_0
    return-void

    .line 1911
    :cond_0
    const-string v1, "mounted_ro"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1912
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->av:Z

    .line 1913
    iput-boolean v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aw:Z

    goto :goto_0

    .line 1915
    :cond_1
    iput-boolean v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->av:Z

    .line 1916
    iput-boolean v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aw:Z

    goto :goto_0
.end method

.method static synthetic u(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->E:Ljava/lang/String;

    return-object v0
.end method

.method private u()V
    .locals 3

    .prologue
    .line 1965
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1966
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_2

    .line 1967
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1968
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 1969
    const-string v0, "setCoverstorySample network error!!"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976
    :cond_1
    :goto_0
    return-void

    .line 1972
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/userprofile/CoverstorySampleActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1973
    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method static synthetic v(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    return-object v0
.end method

.method private v()V
    .locals 1

    .prologue
    .line 2229
    const-string v0, "profile"

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->e(Ljava/lang/String;)V

    .line 2231
    return-void
.end method

.method static synthetic w(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->au:Ljava/io/File;

    return-object v0
.end method

.method private w()V
    .locals 2

    .prologue
    .line 2384
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aE:I

    .line 2385
    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aF:I

    .line 2386
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2387
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "1. windows size=width:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aE:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " height:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aF:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2389
    :cond_0
    return-void
.end method

.method static synthetic x(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->an:Ljava/lang/String;

    return-object v0
.end method

.method private x()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2392
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    .line 2393
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    .line 2394
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setVisibility(I)V

    .line 2395
    return-void
.end method

.method static synthetic y(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aS:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method private y()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2442
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "new_post_on_count"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2443
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 2444
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Selected My page totalNumBadge : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "new_post_on_count"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2446
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "mypage_badge_update"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2447
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 2448
    return-void
.end method

.method private z()V
    .locals 2

    .prologue
    .line 2477
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aP:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2478
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ah:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aP:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2480
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    new-instance v1, Lcom/sec/chaton/userprofile/ao;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/ao;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    new-instance v1, Lcom/sec/chaton/userprofile/ap;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/ap;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2493
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    new-instance v1, Lcom/sec/chaton/userprofile/aq;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/aq;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2500
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->C:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/userprofile/ar;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/ar;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2507
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->D:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/userprofile/as;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/as;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2514
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->y:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/sec/chaton/userprofile/at;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/at;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523
    return-void
.end method

.method static synthetic z(Lcom/sec/chaton/userprofile/MyPageFragment;)Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ay:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1627
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aC:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1628
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aC:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 1630
    :cond_0
    const-string v0, "/coverstory/"

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->d(Ljava/lang/String;)V

    .line 1631
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tmp_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpeg_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aB:Ljava/lang/String;

    .line 1632
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aC:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aB:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aA:Ljava/io/File;

    .line 1633
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aA:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v0

    .line 1634
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1635
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Create File] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aC:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aB:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1637
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aA:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    .line 1638
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1639
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b002b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1671
    :cond_2
    :goto_0
    return-void

    .line 1641
    :cond_3
    const v1, 0x7f0b0415

    .line 1642
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1643
    const-string v2, "image/*"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1645
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-ne v0, v4, :cond_4

    .line 1647
    const v0, 0x7f0d000a

    .line 1651
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 1652
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v3

    if-ne v3, v4, :cond_5

    .line 1653
    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aU:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v3}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 1664
    :goto_2
    invoke-virtual {v2}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1666
    :catch_0
    move-exception v0

    .line 1667
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 1668
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 1649
    :cond_4
    const v0, 0x7f0d000b

    goto :goto_1

    .line 1656
    :cond_5
    :try_start_1
    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aV:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v3}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public a(Lcom/sec/chaton/poston/k;)V
    .locals 5

    .prologue
    .line 791
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827
    :goto_0
    return-void

    .line 794
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v2, Lcom/sec/chaton/poston/PostONDetailActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 795
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 796
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 797
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 798
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->f:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 799
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 800
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/chaton/poston/d;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 801
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->g:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 802
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->i:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 803
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->o:Ljava/lang/String;

    const-string v2, "MY_PAGE"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 804
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->l:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 805
    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 806
    invoke-virtual {p1}, Lcom/sec/chaton/poston/k;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/a/aa;

    .line 807
    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/poston/d;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 808
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_1

    .line 809
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMultimediaDownload has geo tag, content.getUrl() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    :cond_1
    sget-object v3, Lcom/sec/chaton/poston/PostONDetailFragment;->m:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 813
    :cond_2
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_3

    .line 814
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMultimediaDownload has Multimedia, content.getUrl() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / content.getMetaType() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    :cond_3
    sget-object v3, Lcom/sec/chaton/poston/PostONDetailFragment;->j:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 817
    sget-object v3, Lcom/sec/chaton/poston/PostONDetailFragment;->k:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/e/a/aa;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 821
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_5

    .line 822
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 826
    :goto_2
    const/16 v0, 0xc

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 824
    :cond_5
    const/high16 v0, 0x14000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 833
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 835
    const-string v1, "buddyId"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 836
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 837
    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 838
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 842
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 846
    :goto_0
    return-void

    .line 845
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/userprofile/bn;

    invoke-interface {v0, p1, p2}, Lcom/sec/chaton/userprofile/bn;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f070396

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 267
    invoke-static {p0, v4}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 269
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 270
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00080001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 272
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->o()V

    .line 281
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->i()V

    .line 283
    iput-boolean v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->d:Z

    .line 284
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "coverstory_user_guide_popup"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 285
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/userprofile/CoverstoryUserGuideActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 286
    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivity(Landroid/content/Intent;)V

    .line 288
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "coverstory_first_set"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_2

    .line 289
    const-string v0, " onTabSelected listCoverStory First @@@@"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aJ:Ljava/util/ArrayList;

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->e()V

    .line 295
    :cond_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 296
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    const v1, 0x7f07000a

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    .line 297
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v0, :cond_4

    .line 298
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->h()V

    .line 304
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    .line 308
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 309
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 310
    invoke-virtual {v1, v6, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 311
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 313
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v0, :cond_5

    .line 314
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 318
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    :cond_3
    return-void

    .line 300
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090178

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/TabActivity;->b(I)V

    goto :goto_0

    .line 316
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public b(Ljava/lang/String;)Z
    .locals 7

    .prologue
    .line 1712
    const/4 v0, 0x0

    .line 1717
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ar:Ljava/lang/String;

    .line 1718
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1719
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1720
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 1721
    const-string v1, "checkAndSetCoverStoryRandomImages No random images in file folder "

    sget-object v2, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1752
    :cond_0
    :goto_0
    return v0

    .line 1726
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ar:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1727
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1728
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1729
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 1730
    const-string v1, "checkAndSetCoverStoryRandomImages not exists the random image in file folder #2#"

    sget-object v2, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1734
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_4

    .line 1735
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 1736
    const-string v1, "checkAndSetCoverStoryRandomImages file size is 0."

    sget-object v3, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1738
    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 1741
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 1742
    const-string v0, "checkAndSetCoverStoryRandomImages get the random image in file folder #3#"

    sget-object v2, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1744
    :cond_5
    const/4 v0, 0x1

    .line 1745
    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    if-eqz v2, :cond_6

    .line 1746
    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 1747
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/MyPageFragment;->c(Ljava/lang/String;)V

    .line 1749
    :cond_6
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 1750
    const-string v1, "checkAndSetCoverStoryRandomImages set the random image from file folder #4#"

    sget-object v2, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 330
    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 335
    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->d:Z

    .line 339
    return-void
.end method

.method public d()Z
    .locals 4

    .prologue
    const v3, 0x7f0902b8

    const v2, 0x7f0902b7

    .line 1675
    const-string v0, "Buddy didn\'t set Coverstory "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1676
    invoke-static {}, Lcom/sec/chaton/e/a/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 1677
    if-eqz v0, :cond_1

    .line 1678
    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(Ljava/lang/String;)Z

    move-result v1

    .line 1679
    if-nez v1, :cond_0

    .line 1680
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1683
    :cond_0
    const-string v1, "coverstory_contentid"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/d/i;->b(Ljava/lang/String;)V

    .line 1686
    const/4 v0, 0x1

    .line 1707
    :goto_0
    return v0

    .line 1690
    :cond_1
    const-string v0, " Random ERROR !!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1694
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mycoverstory."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1695
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1696
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1697
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 1698
    const-string v0, "First time loadDefaultCoverStory in MypageFragment "

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1700
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/widget/ImageView;)Z

    .line 1707
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1702
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 1703
    const-string v0, " downloadRandomCoverStory in setRandomImages "

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/i;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public e()V
    .locals 3

    .prologue
    .line 1791
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0204

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03ff

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/userprofile/bg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/bg;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 1861
    return-void
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 1897
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->t()V

    .line 1898
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->av:Z

    return v0
.end method

.method protected g()Z
    .locals 1

    .prologue
    .line 1902
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->t()V

    .line 1903
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aw:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x258

    const/4 v4, 0x2

    const/4 v0, -0x1

    const/4 v3, 0x1

    .line 2019
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2020
    if-ne p2, v0, :cond_a

    .line 2021
    packed-switch p1, :pswitch_data_0

    .line 2203
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2027
    :pswitch_1
    if-ne p2, v0, :cond_0

    .line 2028
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "temp_file_path"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2029
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2030
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_1

    .line 2031
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_0

    .line 2032
    const-string v0, "Crop return null!"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2038
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->am:Ljava/io/File;

    invoke-static {v1, v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 2039
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->al:Z

    .line 2041
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2042
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->m:Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->am:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2044
    :catch_0
    move-exception v0

    .line 2045
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 2046
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2048
    :cond_2
    invoke-direct {p0, v6}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    goto :goto_0

    .line 2056
    :pswitch_2
    if-ne p2, v0, :cond_0

    .line 2058
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "temp_file_path"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2059
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2060
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3

    .line 2061
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_0

    .line 2062
    const-string v0, "Crop return null!"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2067
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 2068
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2069
    new-instance v2, Lcom/sec/chaton/userprofile/bo;

    invoke-direct {v2, p0, v1, v0}, Lcom/sec/chaton/userprofile/bo;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/io/File;Landroid/net/Uri;)V

    .line 2070
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Lcom/sec/chaton/userprofile/bo;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 2085
    :catch_1
    move-exception v0

    .line 2086
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_4

    .line 2087
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2089
    :cond_4
    invoke-direct {p0, v6}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(I)V

    goto/16 :goto_0

    .line 2096
    :pswitch_3
    if-nez p3, :cond_5

    .line 2097
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2098
    const-string v0, "Crop Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2103
    :cond_5
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    .line 2104
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2105
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 2106
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2107
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2108
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2109
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2110
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2111
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2116
    :pswitch_4
    if-nez p3, :cond_6

    .line 2117
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2118
    const-string v0, "Crop Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2123
    :cond_6
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    .line 2124
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2125
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 2126
    const-string v1, "outputX"

    iget v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aE:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2127
    const-string v1, "outputY"

    iget v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aF:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2128
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2129
    const-string v1, "aspectY"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2130
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2131
    invoke-virtual {p0, v0, v4}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2135
    :pswitch_5
    if-ne p2, v0, :cond_7

    .line 2136
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2137
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 2138
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2139
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2140
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2141
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2142
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2143
    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2145
    :cond_7
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2146
    const-string v0, "Camera Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2152
    :pswitch_6
    if-ne p2, v0, :cond_8

    .line 2153
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2154
    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 2155
    const-string v1, "outputX"

    iget v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aE:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2156
    const-string v1, "outputY"

    iget v2, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aF:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2157
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2158
    const-string v1, "aspectY"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2159
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2160
    invoke-virtual {p0, v0, v4}, Lcom/sec/chaton/userprofile/MyPageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2162
    :cond_8
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 2163
    const-string v0, "Camera Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2170
    :pswitch_7
    if-ne p2, v0, :cond_0

    .line 2172
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "HAS_MORE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->i:Ljava/lang/String;

    .line 2173
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "END_TIME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->j:Ljava/lang/String;

    .line 2174
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->p()V

    goto/16 :goto_0

    .line 2181
    :pswitch_8
    if-ne p2, v0, :cond_0

    .line 2183
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "IS_DELETED"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2185
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "HAS_MORE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->i:Ljava/lang/String;

    .line 2186
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "END_TIME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->j:Ljava/lang/String;

    .line 2187
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->p()V

    goto/16 :goto_0

    .line 2189
    :cond_9
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2191
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aN:Z

    goto/16 :goto_0

    .line 2200
    :cond_a
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(ILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 2021
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 621
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 624
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 628
    if-eqz v0, :cond_1

    .line 629
    const-string v1, "ACTIVITY_PURPOSE_CALL_MYPAGE"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 630
    const/16 v1, 0x16

    if-ne v0, v1, :cond_1

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 637
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->D:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 638
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_2

    .line 639
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->h()V

    .line 643
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 648
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090178

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/TabActivity;->b(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aM:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 347
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 349
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 351
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "?uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "param"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ao:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->k()V

    .line 359
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->i:Ljava/lang/String;

    .line 360
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->j:Ljava/lang/String;

    .line 361
    iput-boolean v6, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->h:Z

    .line 362
    iput-boolean v5, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->al:Z

    .line 363
    iput-boolean v6, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->as:Z

    .line 365
    iput-boolean v5, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aa:Z

    .line 367
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->w()V

    .line 369
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->at:Ljava/io/File;

    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ar:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 377
    :cond_1
    :goto_1
    new-instance v0, Lcom/sec/chaton/d/v;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aR:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/v;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->x:Lcom/sec/chaton/d/v;

    .line 378
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aR:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->m:Lcom/sec/chaton/d/w;

    .line 379
    new-instance v0, Lcom/sec/chaton/d/i;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aR:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/i;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    .line 380
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j()V

    .line 381
    iput-object v7, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->q:Lcom/sec/common/a/d;

    .line 383
    if-nez p1, :cond_3

    .line 384
    iput-object v7, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    .line 402
    :cond_2
    :goto_2
    return-void

    .line 352
    :catch_0
    move-exception v0

    .line 353
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 354
    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 371
    :catch_1
    move-exception v0

    .line 372
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 373
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 387
    :cond_3
    const-string v0, "CAPTURE_IMAGE_URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 388
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 389
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    .line 392
    :cond_4
    const-string v0, "PROFILE_IMAGE_TEMP_FILE_URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 393
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 394
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->am:Ljava/io/File;

    .line 397
    :cond_5
    const-string v0, "COVERSTORY_IMAGE_TEMP_FILE_URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 399
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aA:Ljava/io/File;

    goto :goto_2
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2717
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 2719
    new-instance v1, Lcom/sec/chaton/b/a;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Lcom/sec/chaton/b/a;-><init>(Landroid/content/Context;Landroid/view/ContextMenu;)V

    .line 2720
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 2721
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget v2, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/poston/k;

    .line 2722
    if-nez v0, :cond_0

    .line 2785
    :goto_0
    return-void

    .line 2725
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->c()Ljava/lang/String;

    move-result-object v2

    .line 2726
    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->f()Ljava/lang/String;

    .line 2727
    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->a()Ljava/lang/String;

    move-result-object v3

    .line 2728
    iget-object v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->E:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2729
    const v0, 0x7f0b0094

    invoke-virtual {v1, v0}, Lcom/sec/chaton/b/a;->a(I)Lcom/sec/chaton/b/a;

    .line 2733
    :goto_1
    const/4 v0, 0x1

    const v2, 0x7f0b0138

    invoke-virtual {v1, v5, v5, v0, v2}, Lcom/sec/chaton/b/a;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/sec/chaton/userprofile/av;

    invoke-direct {v1, p0, v3}, Lcom/sec/chaton/userprofile/av;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 2731
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/poston/k;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/b/a;->a(Ljava/lang/CharSequence;)Lcom/sec/chaton/b/a;

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 2681
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2682
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 2683
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2685
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2686
    const v0, 0x7f0f000a

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2687
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 406
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aO:Lcom/coolots/sso/a/a;

    .line 408
    const v0, 0x7f0300cb

    invoke-virtual {p1, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    .line 409
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f07038d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->z:Landroid/widget/TextView;

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f07038f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->A:Landroid/widget/TextView;

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f07038e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->B:Landroid/widget/ImageView;

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f07038b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->C:Landroid/widget/ImageView;

    .line 413
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f070394

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->D:Landroid/widget/ImageView;

    .line 415
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f07038a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/SelectableImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    .line 416
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f070390

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/SelectableImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f070391

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/SelectableImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f070392

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/SelectableImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    .line 420
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f070393

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ah:Landroid/widget/ImageView;

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f070395

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->y:Landroid/widget/FrameLayout;

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f07038c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->H:Landroid/widget/LinearLayout;

    .line 430
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    .line 432
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f070389

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->p:Landroid/widget/LinearLayout;

    .line 433
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->p:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/userprofile/ay;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/ay;-><init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->H:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 441
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aK:Lcom/sec/chaton/widget/j;

    .line 442
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aK:Lcom/sec/chaton/widget/j;

    const v1, 0x7f0b019c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->k:Landroid/app/ProgressDialog;

    .line 443
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aK:Lcom/sec/chaton/widget/j;

    const v1, 0x7f0b0041

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->l:Landroid/app/ProgressDialog;

    .line 447
    const v0, 0x7f03013b

    invoke-virtual {p1, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->g:Landroid/view/View;

    .line 448
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->g:Landroid/view/View;

    const v1, 0x7f070456

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->r:Landroid/widget/LinearLayout;

    .line 449
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->g:Landroid/view/View;

    const v1, 0x7f070507

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->s:Landroid/widget/LinearLayout;

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 451
    const v0, 0x7f0300cc

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->t:Landroid/view/View;

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->t:Landroid/view/View;

    const v1, 0x7f070399

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 456
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->t:Landroid/view/View;

    const v1, 0x7f0702cf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    .line 457
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->e:Z

    if-ne v0, v6, :cond_0

    .line 458
    const-string v0, "updated"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "mypage_coverstory_state"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Landroid/widget/ImageView;)V

    .line 476
    :goto_0
    iput-boolean v8, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->e:Z

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 479
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 480
    new-instance v0, Lcom/sec/chaton/poston/d;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->v:Ljava/util/ArrayList;

    const v4, 0x7f03012a

    iget-object v5, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/poston/d;-><init>(Landroid/widget/ListView;Landroid/content/Context;Ljava/util/ArrayList;ILcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/poston/d;->a(Lcom/sec/chaton/poston/j;)V

    .line 483
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v1, v7, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 484
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    .line 486
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1, v7, v8}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 488
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setScrollbarFadingEnabled(Z)V

    .line 490
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->c:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 493
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 494
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_PURPOSE_CALL_MYPAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 495
    const/16 v1, 0x16

    if-ne v0, v1, :cond_2

    .line 496
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->f:Landroid/view/View;

    const v1, 0x7f070397

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aL:Landroid/widget/FrameLayout;

    .line 497
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aL:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 498
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aL:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 499
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 501
    :cond_1
    iput-boolean v6, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->al:Z

    .line 505
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->s()V

    .line 506
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->q()V

    .line 507
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->z()V

    .line 508
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->t:Landroid/view/View;

    return-object v0

    .line 461
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 462
    const-string v0, " coverstory default image loadDefaultCoverStory()"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/widget/ImageView;)Z

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 667
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 674
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 677
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->l:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 678
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 681
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    if-eqz v0, :cond_2

    .line 682
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 683
    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ad:Lcom/sec/chaton/widget/SelectableImageView;

    .line 685
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    if-eqz v0, :cond_3

    .line 686
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 687
    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ae:Lcom/sec/chaton/widget/SelectableImageView;

    .line 689
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    if-eqz v0, :cond_4

    .line 690
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 691
    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->af:Lcom/sec/chaton/widget/SelectableImageView;

    .line 693
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    if-eqz v0, :cond_5

    .line 694
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 695
    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    .line 697
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 698
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 699
    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    .line 701
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->q:Lcom/sec/common/a/d;

    if-eqz v0, :cond_7

    .line 702
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->q:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 704
    :cond_7
    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->w:Lcom/sec/chaton/poston/d;

    .line 705
    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->c:Landroid/widget/AbsListView$OnScrollListener;

    .line 706
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->x:Lcom/sec/chaton/d/v;

    invoke-virtual {v0}, Lcom/sec/chaton/d/v;->a()V

    .line 707
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->m:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->e()V

    .line 709
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    if-eqz v0, :cond_8

    .line 710
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->b()V

    .line 711
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->f()V

    .line 712
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->g()V

    .line 713
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->d()V

    .line 714
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->h()V

    .line 715
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->i()V

    .line 716
    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    .line 719
    :cond_8
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->A()V

    .line 720
    return-void

    .line 668
    :catch_0
    move-exception v0

    .line 670
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 724
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 725
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aj:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 728
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 729
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 730
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 731
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 732
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 733
    iput-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->u:Landroid/widget/ListView;

    .line 735
    :cond_1
    return-void
.end method

.method public onLowMemory()V
    .locals 0

    .prologue
    .line 739
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onLowMemory()V

    .line 740
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->A()V

    .line 741
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2699
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 2701
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2709
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2704
    :pswitch_0
    const-string v0, "onOptionsItemSelected():mypage_menu_sync"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2705
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->a()V

    .line 2707
    const/4 v0, 0x1

    goto :goto_0

    .line 2701
    :pswitch_data_0
    .packed-switch 0x7f070582
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 2692
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 2693
    const v0, 0x7f070581

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 2694
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 513
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 514
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->k()V

    .line 515
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->q()V

    .line 516
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aK:Lcom/sec/chaton/widget/j;

    if-nez v0, :cond_0

    .line 517
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aK:Lcom/sec/chaton/widget/j;

    .line 519
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->k:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 520
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aK:Lcom/sec/chaton/widget/j;

    const v1, 0x7f0b019c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->k:Landroid/app/ProgressDialog;

    .line 522
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->l:Landroid/app/ProgressDialog;

    if-nez v0, :cond_2

    .line 524
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aK:Lcom/sec/chaton/widget/j;

    const v1, 0x7f0b0041

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->l:Landroid/app/ProgressDialog;

    .line 527
    :cond_2
    const-string v0, "coverstory_sample_changed"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "mypage_coverstory_state"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 528
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Landroid/widget/ImageView;)V

    .line 529
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0403

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 530
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 531
    const-string v0, "mypage_coverstory_state"

    const-string v1, "updated"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    :cond_3
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->d:Z

    if-eqz v0, :cond_8

    .line 535
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->i()V

    .line 543
    const-string v0, "updated"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "mypage_coverstory_state"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 544
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Landroid/widget/ImageView;)V

    .line 560
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aN:Z

    if-eqz v0, :cond_4

    .line 561
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->l()V

    .line 562
    iput-boolean v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aN:Z

    .line 574
    :cond_4
    :goto_1
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->r()V

    .line 576
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ag:Lcom/sec/chaton/widget/SelectableImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/SelectableImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_9

    .line 577
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ah:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 582
    :goto_2
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->al:Z

    if-eqz v0, :cond_5

    .line 583
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyPageFragment;->n()V

    .line 584
    iput-boolean v4, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->al:Z

    .line 586
    :cond_5
    return-void

    .line 546
    :cond_6
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_7

    .line 547
    const-string v0, " coverstory default image loadDefaultCoverStory() in onResume()"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->o:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/widget/ImageView;)Z

    goto :goto_0

    .line 566
    :cond_8
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "coverstory_first_set"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v5, :cond_4

    .line 567
    const-string v0, " onResume !mIsTabSelected listCoverStory First @@@@"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aJ:Ljava/util/ArrayList;

    .line 569
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->n:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->e()V

    goto :goto_1

    .line 579
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ah:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 602
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 604
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 605
    const-string v0, "CAPTURE_IMAGE_URI"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->ax:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->am:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 609
    const-string v0, "PROFILE_IMAGE_TEMP_FILE_URI"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->am:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aA:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 613
    const-string v0, "COVERSTORY_IMAGE_TEMP_FILE_URI"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyPageFragment;->aA:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    :cond_2
    return-void
.end method
