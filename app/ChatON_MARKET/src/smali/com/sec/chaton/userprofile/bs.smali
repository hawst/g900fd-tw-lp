.class Lcom/sec/chaton/userprofile/bs;
.super Ljava/lang/Object;
.source "ProfileImageHistoryFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V
    .locals 0

    .prologue
    .line 1064
    iput-object p1, p0, Lcom/sec/chaton/userprofile/bs;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1068
    packed-switch p2, :pswitch_data_0

    .line 1090
    :goto_0
    return-void

    .line 1070
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bs;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->s(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    goto :goto_0

    .line 1073
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1074
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1075
    const-string v1, "output"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bs;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->t(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1076
    const-string v1, "outputFormat"

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1078
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bs;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1086
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bs;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Z)Z

    goto :goto_0

    .line 1079
    :catch_0
    move-exception v0

    .line 1080
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1081
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 1082
    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 1068
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
