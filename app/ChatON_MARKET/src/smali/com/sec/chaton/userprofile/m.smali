.class Lcom/sec/chaton/userprofile/m;
.super Landroid/os/Handler;
.source "BirthdayImpl.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/j;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/j;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/chaton/userprofile/m;->a:Lcom/sec/chaton/userprofile/j;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/userprofile/m;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "is Finishing : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/m;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/m;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->c(Lcom/sec/chaton/userprofile/j;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/userprofile/m;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->c(Lcom/sec/chaton/userprofile/j;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 210
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 212
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 213
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_3

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/userprofile/m;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->d(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/userprofile/m;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->e(Lcom/sec/chaton/userprofile/j;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/userprofile/m;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->b(Lcom/sec/chaton/userprofile/j;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_0

    .line 219
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/m;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0097

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 210
    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
    .end packed-switch
.end method
