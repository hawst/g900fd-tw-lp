.class public Lcom/sec/chaton/userprofile/q;
.super Landroid/widget/BaseAdapter;
.source "CoverStorySampleAdapter.java"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/userprofile/t;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/LayoutInflater;

.field private c:I

.field private d:Lcom/sec/common/f/c;

.field private e:Landroid/content/Context;

.field private f:Ljava/lang/String;

.field private g:Lcom/sec/chaton/userprofile/s;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/userprofile/t;",
            ">;",
            "Lcom/sec/common/f/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 52
    iput-object p3, p0, Lcom/sec/chaton/userprofile/q;->a:Ljava/util/ArrayList;

    .line 53
    iput p2, p0, Lcom/sec/chaton/userprofile/q;->c:I

    .line 54
    iput-object p4, p0, Lcom/sec/chaton/userprofile/q;->d:Lcom/sec/common/f/c;

    .line 55
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/q;->b:Landroid/view/LayoutInflater;

    .line 56
    iput-object p1, p0, Lcom/sec/chaton/userprofile/q;->e:Landroid/content/Context;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/coverstory/thumb/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/q;->f:Ljava/lang/String;

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/q;)Lcom/sec/chaton/userprofile/s;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/userprofile/q;->g:Lcom/sec/chaton/userprofile/s;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/q;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/userprofile/q;->a:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/userprofile/t;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/chaton/userprofile/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/userprofile/t;

    return-object v0
.end method

.method public a(Lcom/sec/chaton/userprofile/s;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/chaton/userprofile/q;->g:Lcom/sec/chaton/userprofile/s;

    .line 49
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/userprofile/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/sec/chaton/userprofile/q;->a(I)Lcom/sec/chaton/userprofile/t;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 72
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 77
    .line 78
    if-nez p2, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/userprofile/q;->b:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/userprofile/q;->c:I

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 80
    new-instance v2, Lcom/sec/chaton/userprofile/u;

    move-object v0, v1

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-direct {v2, v0}, Lcom/sec/chaton/userprofile/u;-><init>(Landroid/widget/FrameLayout;)V

    .line 81
    iget-object v0, v2, Lcom/sec/chaton/userprofile/u;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    .line 82
    iget-object v0, v2, Lcom/sec/chaton/userprofile/u;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 83
    iget-object v0, v2, Lcom/sec/chaton/userprofile/u;->c:Landroid/widget/FrameLayout;

    new-instance v3, Lcom/sec/chaton/userprofile/r;

    invoke-direct {v3, p0, p1}, Lcom/sec/chaton/userprofile/r;-><init>(Lcom/sec/chaton/userprofile/q;I)V

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v8, v2

    move-object p2, v1

    .line 103
    :goto_0
    iget-object v0, v8, Lcom/sec/chaton/userprofile/u;->b:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 104
    iget-object v0, v8, Lcom/sec/chaton/userprofile/u;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 107
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "coverstory_contentid"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/userprofile/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/userprofile/t;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/t;->a(Lcom/sec/chaton/userprofile/t;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 108
    iget-object v0, v8, Lcom/sec/chaton/userprofile/u;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 113
    :goto_1
    new-instance v0, Lcom/sec/chaton/poston/a;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/userprofile/t;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/t;->b(Lcom/sec/chaton/userprofile/t;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/chaton/poston/a;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/q;->e:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/chaton/userprofile/q;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/userprofile/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/chaton/userprofile/t;

    invoke-static {v6}, Lcom/sec/chaton/userprofile/t;->c(Lcom/sec/chaton/userprofile/t;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/userprofile/q;->a:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/chaton/userprofile/t;

    invoke-static {v7}, Lcom/sec/chaton/userprofile/t;->a(Lcom/sec/chaton/userprofile/t;)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lcom/sec/chaton/userprofile/q;->d:Lcom/sec/common/f/c;

    iget-object v2, v8, Lcom/sec/chaton/userprofile/u;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 116
    invoke-virtual {p2, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 117
    return-object p2

    .line 93
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/userprofile/u;

    move-object v8, v0

    goto :goto_0

    .line 110
    :cond_1
    iget-object v0, v8, Lcom/sec/chaton/userprofile/u;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
