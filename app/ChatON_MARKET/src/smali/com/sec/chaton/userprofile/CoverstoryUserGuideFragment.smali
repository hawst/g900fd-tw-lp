.class public Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;
.super Landroid/support/v4/app/Fragment;
.source "CoverstoryUserGuideFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->a:Landroid/view/View;

    const v1, 0x7f07039a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->c:Landroid/widget/LinearLayout;

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->a:Landroid/view/View;

    const v1, 0x7f07039b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->b:Landroid/widget/ImageView;

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->a:Landroid/view/View;

    const v1, 0x7f07039c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->d:Landroid/widget/TextView;

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<i>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0402

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</i>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->a:Landroid/view/View;

    const v1, 0x7f07039d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->e:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->e:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<i>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0406

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</i>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->c:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 117
    iput-object v2, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->c:Landroid/widget/LinearLayout;

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 125
    iput-object v2, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->b:Landroid/widget/ImageView;

    .line 129
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "coverstory_user_guide_popup"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 131
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 109
    :goto_0
    return-void

    .line 101
    :pswitch_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "coverstory_user_guide_popup"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 103
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch 0x7f07039b
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 55
    const v0, 0x7f0300cd

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->a:Landroid/view/View;

    .line 57
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->b()V

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->a:Landroid/view/View;

    return-object v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 86
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 88
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/CoverstoryUserGuideFragment;->b()V

    .line 90
    return-void
.end method
