.class Lcom/sec/chaton/userprofile/ae;
.super Landroid/os/Handler;
.source "EditProfileFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/EditProfileFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/EditProfileFragment;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 283
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 284
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_2

    .line 285
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Push Name"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/EditProfileFragment;->b(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "status_message"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/EditProfileFragment;->c(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 289
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->d(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->d(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 292
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    iget-object v0, v0, Lcom/sec/chaton/userprofile/EditProfileFragment;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0139

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/userprofile/af;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/af;-><init>(Lcom/sec/chaton/userprofile/ae;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ae;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
    .end packed-switch
.end method
