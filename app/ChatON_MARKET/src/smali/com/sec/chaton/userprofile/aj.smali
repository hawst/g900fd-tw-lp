.class Lcom/sec/chaton/userprofile/aj;
.super Landroid/os/Handler;
.source "MyInfoFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/MyInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/MyInfoFragment;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/sec/chaton/userprofile/aj;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/userprofile/aj;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 455
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 457
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 458
    if-eqz v0, :cond_0

    .line 462
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_2

    .line 463
    const-string v0, "ProfileControl.METHOD_GET_PROFILE_ALL Success"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    iget-object v0, p0, Lcom/sec/chaton/userprofile/aj;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Lcom/sec/chaton/userprofile/MyInfoFragment;)V

    .line 471
    iget-object v0, p0, Lcom/sec/chaton/userprofile/aj;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->b(Lcom/sec/chaton/userprofile/MyInfoFragment;)V

    goto :goto_0

    .line 474
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/aj;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 475
    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b008a

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0024

    new-instance v2, Lcom/sec/chaton/userprofile/al;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/al;-><init>(Lcom/sec/chaton/userprofile/aj;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00dc

    new-instance v2, Lcom/sec/chaton/userprofile/ak;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/ak;-><init>(Lcom/sec/chaton/userprofile/aj;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 492
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/aj;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 493
    iget-object v1, p0, Lcom/sec/chaton/userprofile/aj;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->d(Lcom/sec/chaton/userprofile/MyInfoFragment;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f070382

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 495
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/sec/chaton/a/a/f;

    .line 496
    if-eqz v2, :cond_0

    .line 500
    invoke-virtual {v2}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v3, v4, :cond_0

    .line 502
    iget-object v3, p0, Lcom/sec/chaton/userprofile/aj;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/MyInfoFragment;->e(Lcom/sec/chaton/userprofile/MyInfoFragment;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 505
    invoke-virtual {v2}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;

    .line 506
    if-eqz v2, :cond_4

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;->mapping:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-ne v3, v7, :cond_4

    move v4, v5

    move v6, v5

    .line 507
    :goto_1
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;->MappingInfo:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v4, v3, :cond_5

    .line 508
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;->MappingInfo:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v8, v3, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->phoneNumber:Ljava/lang/String;

    .line 510
    if-eqz v8, :cond_3

    iget-object v3, p0, Lcom/sec/chaton/userprofile/aj;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/MyInfoFragment;->f(Lcom/sec/chaton/userprofile/MyInfoFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 511
    const v3, 0x7f03013a

    const/4 v6, 0x0

    invoke-virtual {v0, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 512
    const v3, 0x7f070506

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 513
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "+"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 514
    iget-object v3, p0, Lcom/sec/chaton/userprofile/aj;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/MyInfoFragment;->e(Lcom/sec/chaton/userprofile/MyInfoFragment;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move v6, v7

    .line 507
    :cond_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_4
    move v6, v5

    .line 519
    :cond_5
    if-nez v6, :cond_6

    .line 520
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 522
    :cond_6
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 455
    nop

    :pswitch_data_0
    .packed-switch 0x19b
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
