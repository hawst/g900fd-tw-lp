.class public Lcom/sec/chaton/userprofile/EditProfileFragment;
.super Landroid/support/v4/app/Fragment;
.source "EditProfileFragment.java"


# instance fields
.field a:Landroid/content/Context;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/EditText;

.field private d:Lcom/sec/chaton/d/w;

.field private e:Landroid/app/ProgressDialog;

.field private f:Landroid/view/MenuItem;

.field private g:Z

.field private h:Landroid/text/TextWatcher;

.field private i:Landroid/text/TextWatcher;

.field private j:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->g:Z

    .line 213
    new-instance v0, Lcom/sec/chaton/userprofile/ac;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/ac;-><init>(Lcom/sec/chaton/userprofile/EditProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->h:Landroid/text/TextWatcher;

    .line 244
    new-instance v0, Lcom/sec/chaton/userprofile/ad;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/ad;-><init>(Lcom/sec/chaton/userprofile/EditProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->i:Landroid/text/TextWatcher;

    .line 274
    new-instance v0, Lcom/sec/chaton/userprofile/ae;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/ae;-><init>(Lcom/sec/chaton/userprofile/EditProfileFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->j:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->f:Landroid/view/MenuItem;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 311
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 314
    if-eqz v0, :cond_0

    .line 315
    iget-object v1, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 316
    iget-object v1, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 319
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/EditProfileFragment;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->e:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/userprofile/EditProfileFragment;)Lcom/sec/chaton/d/w;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->d:Lcom/sec/chaton/d/w;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->a:Landroid/content/Context;

    .line 64
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 111
    const v0, 0x7f0f0004

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 112
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 113
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 68
    const v0, 0x7f0300c9

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 70
    const v0, 0x7f07037a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->b:Landroid/widget/EditText;

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->b:Landroid/widget/EditText;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Push Name"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->b:Landroid/widget/EditText;

    new-array v2, v7, [Landroid/text/InputFilter;

    new-instance v3, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const/16 v5, 0x1e

    invoke-direct {v3, v4, v5}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v3, v2, v6

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->h:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    const v0, 0x7f07037b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "status_message"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    new-array v2, v7, [Landroid/text/InputFilter;

    new-instance v3, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const/16 v5, 0x28

    invoke-direct {v3, v4, v5}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v3, v2, v6

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->i:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 85
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->j:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->d:Lcom/sec/chaton/d/w;

    .line 86
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b000c

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->e:Landroid/app/ProgressDialog;

    .line 88
    invoke-static {p0, v7}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 89
    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 107
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 130
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f070574

    if-ne v1, v2, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 134
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f070575

    if-ne v1, v2, :cond_c

    .line 137
    iget-object v1, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 138
    iget-object v1, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 140
    invoke-static {}, Lcom/sec/chaton/util/y;->b()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 141
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_1

    .line 142
    const-string v1, "Chat ON Log : ON > OFF"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_1
    invoke-static {v3}, Lcom/sec/chaton/util/y;->b(I)V

    .line 145
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Log Off"

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 210
    :goto_0
    return v0

    .line 147
    :cond_2
    invoke-static {v0}, Lcom/sec/chaton/util/y;->b(I)V

    .line 148
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_3

    .line 149
    const-string v1, "Chat ON Log : OFF > ON"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Log On"

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 156
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 157
    iget-object v1, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 159
    invoke-static {}, Lcom/sec/chaton/util/y;->b()I

    move-result v1

    if-ne v1, v4, :cond_6

    .line 160
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_5

    .line 161
    const-string v1, "Chat ON Log : ON > OFF"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_5
    invoke-static {v3}, Lcom/sec/chaton/util/y;->b(I)V

    .line 164
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Log Off"

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 166
    :cond_6
    invoke-static {v4}, Lcom/sec/chaton/util/y;->b(I)V

    .line 167
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_7

    .line 168
    const-string v1, "Chat ON Log : OFF > ON With Save"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_7
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Log On With Save"

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 177
    :cond_8
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-ne v1, v0, :cond_b

    .line 178
    iget-object v1, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "logcollector"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 179
    invoke-static {}, Lcom/sec/chaton/i/a/d;->a()Z

    move-result v1

    if-ne v1, v0, :cond_9

    .line 180
    invoke-static {v3}, Lcom/sec/chaton/i/a/d;->a(Z)V

    .line 181
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Log Collector Test Off"

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 185
    :cond_9
    invoke-static {v0}, Lcom/sec/chaton/i/a/d;->a(Z)V

    .line 186
    invoke-static {}, Lcom/sec/chaton/i/a/d;->b()Lcom/sec/chaton/i/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/i/a/d;->getState()Ljava/lang/Thread$State;

    move-result-object v1

    sget-object v2, Ljava/lang/Thread$State;->RUNNABLE:Ljava/lang/Thread$State;

    if-eq v1, v2, :cond_a

    .line 187
    invoke-static {}, Lcom/sec/chaton/i/a/d;->b()Lcom/sec/chaton/i/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/i/a/d;->start()V

    .line 189
    :cond_a
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Log Collector Test On"

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 196
    :cond_b
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->a()V

    .line 198
    new-instance v1, Lcom/sec/chaton/a/a/o;

    invoke-direct {v1}, Lcom/sec/chaton/a/a/o;-><init>()V

    .line 199
    iget-object v2, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/a/a/o;->a(Ljava/lang/String;)V

    .line 200
    iget-object v2, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/a/a/o;->b(Ljava/lang/String;)V

    .line 201
    iget-object v2, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->d:Lcom/sec/chaton/d/w;

    const-string v3, ""

    invoke-virtual {v2, v1, v3}, Lcom/sec/chaton/d/w;->a(Lcom/sec/chaton/a/a/o;Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 205
    :cond_c
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_d

    .line 206
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 210
    :cond_d
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 117
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->f:Landroid/view/MenuItem;

    .line 119
    const v0, 0x7f070575

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->f:Landroid/view/MenuItem;

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->f:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 121
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->g:Z

    if-ne v0, v2, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->f:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 126
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/EditProfileFragment;->f:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
