.class public Lcom/sec/chaton/userprofile/EditProfileActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "EditProfileActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-direct {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;-><init>()V

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 22
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/EditProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_FROM"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 25
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 29
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 35
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 36
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/EditProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_FROM"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 39
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 43
    :cond_0
    return-void
.end method
