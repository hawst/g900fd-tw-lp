.class Lcom/sec/chaton/userprofile/ch;
.super Ljava/lang/Object;
.source "SetCoverstoryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 119
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SetCoverstoryFragment [url] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->a(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", #### [filename] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->b(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", #### [id] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->c(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 144
    :goto_0
    return-void

    .line 125
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 126
    const/4 v1, -0x3

    if-eq v1, v0, :cond_1

    const/4 v1, -0x2

    if-ne v1, v0, :cond_2

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->d(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    new-instance v1, Lcom/sec/chaton/d/i;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->e(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/chaton/d/i;-><init>(Landroid/os/Handler;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->a(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;Lcom/sec/chaton/d/i;)Lcom/sec/chaton/d/i;

    .line 136
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->f(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Lcom/sec/chaton/d/i;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->c(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/i;->b(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ch;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->d(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x7f0702d7
        :pswitch_0
    .end packed-switch
.end method
