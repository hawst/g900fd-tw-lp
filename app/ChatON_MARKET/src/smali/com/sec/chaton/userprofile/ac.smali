.class Lcom/sec/chaton/userprofile/ac;
.super Ljava/lang/Object;
.source "EditProfileFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/EditProfileFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/EditProfileFragment;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ac;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 222
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ac;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->a(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ac;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->a(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ac;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/EditProfileFragment;->a(Lcom/sec/chaton/userprofile/EditProfileFragment;Z)Z

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ac;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->b(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ac;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->a(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ac;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/userprofile/EditProfileFragment;->a(Lcom/sec/chaton/userprofile/EditProfileFragment;Z)Z

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ac;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/EditProfileFragment;->a(Lcom/sec/chaton/userprofile/EditProfileFragment;Z)Z

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ac;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/EditProfileFragment;->b(Lcom/sec/chaton/userprofile/EditProfileFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ac;->a:Lcom/sec/chaton/userprofile/EditProfileFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/userprofile/EditProfileFragment;->a(Lcom/sec/chaton/userprofile/EditProfileFragment;Z)Z

    goto :goto_0
.end method
