.class Lcom/sec/chaton/userprofile/ci;
.super Landroid/os/Handler;
.source "SetCoverstoryFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ci;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ci;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 238
    :goto_0
    return-void

    .line 196
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 203
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ci;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->d(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ci;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->d(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ci;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->d(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 206
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 207
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_3

    .line 209
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/CoverStoryAdd;

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ci;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->g(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ci;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->b(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 212
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ci;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mycoverstory."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 213
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "samplePath : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "myCoverStoryPath : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 217
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 220
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 222
    :cond_2
    invoke-static {v3, v1}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 224
    const-string v1, "mypage_coverstory_state"

    const-string v2, "coverstory_sample_changed"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const-string v1, "coverstory_metaid"

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/CoverStoryAdd;->metaid:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v0, "coverstory_contentid"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ci;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->c(Lcom/sec/chaton/userprofile/SetCoverstoryFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const-string v0, "coverstory_file_name"

    const-string v1, "mycoverstory.jpg"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ci;->a:Lcom/sec/chaton/userprofile/SetCoverstoryFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/userprofile/cj;

    invoke-interface {v0}, Lcom/sec/chaton/userprofile/cj;->d()V

    goto/16 :goto_0

    .line 230
    :cond_3
    const-string v0, " METHOD_ADD_COVERSTORY fail : "

    invoke-static {}, Lcom/sec/chaton/userprofile/SetCoverstoryFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0xbba
        :pswitch_0
    .end packed-switch
.end method
