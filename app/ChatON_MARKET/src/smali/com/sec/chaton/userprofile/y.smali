.class public Lcom/sec/chaton/userprofile/y;
.super Landroid/app/AlertDialog;
.source "DatePickerWithCheckbox.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/DatePicker$OnDateChangedListener;
.implements Lcom/sec/widget/n;


# instance fields
.field final a:Ljava/util/Calendar;

.field b:I

.field c:I

.field d:I

.field private e:Lcom/sec/widget/CustomDatePicker;

.field private f:Landroid/widget/DatePicker;

.field private final g:Lcom/sec/chaton/userprofile/ab;

.field private final h:Ljava/util/Calendar;

.field private i:I

.field private j:I

.field private k:I

.field private l:Landroid/widget/CheckBox;

.field private m:Landroid/widget/LinearLayout;

.field private n:Landroid/view/View$OnClickListener;

.field private o:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/chaton/userprofile/ab;IIILjava/lang/String;)V
    .locals 9

    .prologue
    const v3, 0x7f0b0038

    const v8, 0x7f07013e

    const/16 v7, 0xb

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 93
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/y;->a:Ljava/util/Calendar;

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/userprofile/y;->b:I

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->a:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/userprofile/y;->c:I

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->a:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/userprofile/y;->d:I

    .line 171
    new-instance v0, Lcom/sec/chaton/userprofile/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/z;-><init>(Lcom/sec/chaton/userprofile/y;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/y;->n:Landroid/view/View$OnClickListener;

    .line 180
    new-instance v0, Lcom/sec/chaton/userprofile/aa;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/aa;-><init>(Lcom/sec/chaton/userprofile/y;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/y;->o:Landroid/content/DialogInterface$OnClickListener;

    .line 95
    iput-object p2, p0, Lcom/sec/chaton/userprofile/y;->g:Lcom/sec/chaton/userprofile/ab;

    .line 96
    iput p3, p0, Lcom/sec/chaton/userprofile/y;->i:I

    .line 97
    iput p4, p0, Lcom/sec/chaton/userprofile/y;->j:I

    .line 98
    iput p5, p0, Lcom/sec/chaton/userprofile/y;->k:I

    .line 99
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0}, Ljava/text/DateFormatSymbols;-><init>()V

    .line 100
    invoke-virtual {v0}, Ljava/text/DateFormatSymbols;->getShortWeekdays()[Ljava/lang/String;

    .line 102
    invoke-static {v2}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    .line 103
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/y;->h:Ljava/util/Calendar;

    .line 107
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v7, :cond_1

    .line 108
    invoke-virtual {p1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0, p0}, Lcom/sec/chaton/userprofile/y;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 109
    const v0, 0x7f0b003a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/y;->o:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/y;->setButton2(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 130
    :goto_0
    new-instance v1, Landroid/widget/ScrollView;

    invoke-direct {v1, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 131
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 132
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v7, :cond_2

    .line 133
    const v3, 0x7f030033

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 134
    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    :goto_1
    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    .line 139
    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/userprofile/y;->setView(Landroid/view/View;IIII)V

    .line 141
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v7, :cond_3

    .line 142
    invoke-virtual {v1, v8}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/widget/CustomDatePicker;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    iget v3, p0, Lcom/sec/chaton/userprofile/y;->i:I

    iget v4, p0, Lcom/sec/chaton/userprofile/y;->j:I

    iget v5, p0, Lcom/sec/chaton/userprofile/y;->k:I

    invoke-virtual {v0, v3, v4, v5, p0}, Lcom/sec/widget/CustomDatePicker;->a(IIILcom/sec/widget/n;)V

    .line 149
    :goto_2
    const v0, 0x1020001

    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/y;->l:Landroid/widget/CheckBox;

    .line 153
    const-string v0, "FULL"

    invoke-virtual {v0, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "FULL_HIDE"

    invoke-virtual {v0, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 155
    const-string v0, "birthday_year_show"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 160
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 163
    const v0, 0x7f07013f

    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/y;->m:Landroid/widget/LinearLayout;

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->m:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/y;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    return-void

    .line 111
    :cond_1
    invoke-virtual {p1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0, p0}, Lcom/sec/chaton/userprofile/y;->setButton2(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 112
    const v0, 0x7f0b003a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/y;->o:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/y;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_0

    .line 136
    :cond_2
    const v3, 0x7f030034

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 137
    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 145
    :cond_3
    invoke-virtual {v1, v8}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DatePicker;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    iget v3, p0, Lcom/sec/chaton/userprofile/y;->i:I

    iget v4, p0, Lcom/sec/chaton/userprofile/y;->j:I

    iget v5, p0, Lcom/sec/chaton/userprofile/y;->k:I

    invoke-virtual {v0, v3, v4, v5, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    goto/16 :goto_2

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 158
    const-string v0, "birthday_year_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_3
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/y;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->l:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private b(III)V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->h:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->h:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->h:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 249
    return-void
.end method


# virtual methods
.method public a(III)V
    .locals 2

    .prologue
    .line 234
    iput p1, p0, Lcom/sec/chaton/userprofile/y;->i:I

    .line 235
    iput p2, p0, Lcom/sec/chaton/userprofile/y;->j:I

    .line 236
    iput p3, p0, Lcom/sec/chaton/userprofile/y;->k:I

    .line 237
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/widget/CustomDatePicker;->a(III)V

    .line 242
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/DatePicker;->updateDate(III)V

    goto :goto_0
.end method

.method public a(Lcom/sec/widget/CustomDatePicker;III)V
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/sec/chaton/userprofile/y;->b:I

    if-lt p2, v0, :cond_0

    .line 306
    iget p2, p0, Lcom/sec/chaton/userprofile/y;->b:I

    .line 308
    iget v0, p0, Lcom/sec/chaton/userprofile/y;->c:I

    if-lt p3, v0, :cond_0

    .line 309
    iget p3, p0, Lcom/sec/chaton/userprofile/y;->c:I

    .line 311
    iget v0, p0, Lcom/sec/chaton/userprofile/y;->d:I

    if-le p4, v0, :cond_0

    .line 312
    iget p4, p0, Lcom/sec/chaton/userprofile/y;->d:I

    .line 317
    :cond_0
    invoke-virtual {p1, p2, p3, p4, p0}, Lcom/sec/widget/CustomDatePicker;->a(IIILcom/sec/widget/n;)V

    .line 318
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->g:Lcom/sec/chaton/userprofile/ab;

    if-eqz v0, :cond_0

    .line 217
    const-string v0, "birthday_year_show"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/y;->l:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->g:Lcom/sec/chaton/userprofile/ab;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "birthday_year_show"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/userprofile/ab;->a(Z)V

    .line 220
    const-string v0, "Birthday year show : "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    invoke-virtual {v0}, Lcom/sec/widget/CustomDatePicker;->clearFocus()V

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->g:Lcom/sec/chaton/userprofile/ab;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    invoke-virtual {v1}, Lcom/sec/widget/CustomDatePicker;->a()I

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    invoke-virtual {v2}, Lcom/sec/widget/CustomDatePicker;->b()I

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    invoke-virtual {v3}, Lcom/sec/widget/CustomDatePicker;->c()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/userprofile/ab;->a(III)V

    .line 229
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/y;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 231
    :cond_0
    return-void

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->clearFocus()V

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/userprofile/y;->g:Lcom/sec/chaton/userprofile/ab;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getYear()I

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/userprofile/ab;->a(III)V

    goto :goto_0
.end method

.method public onDateChanged(Landroid/widget/DatePicker;III)V
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lcom/sec/chaton/userprofile/y;->b:I

    if-lt p2, v0, :cond_0

    .line 285
    iget p2, p0, Lcom/sec/chaton/userprofile/y;->b:I

    .line 287
    iget v0, p0, Lcom/sec/chaton/userprofile/y;->c:I

    if-lt p3, v0, :cond_0

    .line 288
    iget p3, p0, Lcom/sec/chaton/userprofile/y;->c:I

    .line 290
    iget v0, p0, Lcom/sec/chaton/userprofile/y;->d:I

    if-le p4, v0, :cond_0

    .line 291
    iget p4, p0, Lcom/sec/chaton/userprofile/y;->d:I

    .line 296
    :cond_0
    invoke-virtual {p1, p2, p3, p4, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    .line 297
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 268
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 269
    const-string v0, "year"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 270
    const-string v1, "month"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 271
    const-string v2, "day"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 272
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_0

    .line 273
    iget-object v3, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    invoke-virtual {v3, v0, v1, v2, p0}, Lcom/sec/widget/CustomDatePicker;->a(IIILcom/sec/widget/n;)V

    .line 277
    :goto_0
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/userprofile/y;->b(III)V

    .line 278
    return-void

    .line 275
    :cond_0
    iget-object v3, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    invoke-virtual {v3, v0, v1, v2, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 253
    invoke-super {p0}, Landroid/app/AlertDialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    .line 254
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 255
    const-string v1, "year"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    invoke-virtual {v2}, Lcom/sec/widget/CustomDatePicker;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 256
    const-string v1, "month"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    invoke-virtual {v2}, Lcom/sec/widget/CustomDatePicker;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 257
    const-string v1, "day"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/y;->e:Lcom/sec/widget/CustomDatePicker;

    invoke-virtual {v2}, Lcom/sec/widget/CustomDatePicker;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 263
    :goto_0
    return-object v0

    .line 259
    :cond_0
    const-string v1, "year"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getYear()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 260
    const-string v1, "month"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 261
    const-string v1, "day"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/y;->f:Landroid/widget/DatePicker;

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public show()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 198
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/y;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0091

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/y;->setTitle(Ljava/lang/CharSequence;)V

    .line 199
    invoke-virtual {p0, v2}, Lcom/sec/chaton/userprofile/y;->setCanceledOnTouchOutside(Z)V

    .line 200
    invoke-virtual {p0, v2}, Lcom/sec/chaton/userprofile/y;->setIcon(I)V

    .line 201
    invoke-super {p0}, Landroid/app/AlertDialog;->show()V

    .line 212
    return-void
.end method
