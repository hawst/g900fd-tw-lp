.class Lcom/sec/chaton/userprofile/ba;
.super Landroid/os/Handler;
.source "MyPageFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/MyPageFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V
    .locals 0

    .prologue
    .line 892
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const v7, 0x7f0b00dc

    const v6, 0x7f0b0097

    const v5, 0x7f0b0024

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 895
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1246
    :cond_0
    :goto_0
    return-void

    .line 898
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1241
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1242
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 900
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 901
    if-eqz v0, :cond_0

    .line 904
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_2

    .line 905
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->e(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    .line 906
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->f(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    goto :goto_0

    .line 908
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 927
    :sswitch_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->g(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->g(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 928
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->g(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 930
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->h(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 931
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->h(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 933
    :cond_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 935
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/chaton/a/a/f;)V

    goto/16 :goto_0

    .line 938
    :sswitch_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 941
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->g(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->g(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 942
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->g(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 944
    :cond_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 945
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_6

    .line 947
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/chaton/a/a/f;)V

    .line 948
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0188

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 951
    :cond_6
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0x15fa0

    if-ne v0, v1, :cond_7

    .line 952
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0182

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0160

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0037

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/userprofile/bb;

    invoke-direct {v3, p0}, Lcom/sec/chaton/userprofile/bb;-><init>(Lcom/sec/chaton/userprofile/ba;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 965
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00f2

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 970
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/m;

    .line 971
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/m;->a:Z

    if-eqz v0, :cond_8

    .line 973
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->i(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/d/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->c()V

    goto/16 :goto_0

    .line 975
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 976
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 978
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0122

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/userprofile/bd;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/bd;-><init>(Lcom/sec/chaton/userprofile/ba;)V

    invoke-virtual {v1, v5, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/userprofile/bc;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/bc;-><init>(Lcom/sec/chaton/userprofile/ba;)V

    invoke-virtual {v1, v7, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1006
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1007
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_b

    .line 1008
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1009
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1011
    :cond_a
    const-string v1, "profile_image_status"

    const-string v2, "updated"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/chaton/a/a/f;)V

    goto/16 :goto_0

    .line 1015
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1016
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1018
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1019
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 1020
    const-string v0, "MyPageFragment: METHOD_PROFILE_IMAGE_HISTORY_ADD failed"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1028
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1029
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_d

    .line 1030
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/chaton/a/a/f;)V

    .line 1032
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->l(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/widget/SelectableImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/SelectableImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_e

    .line 1033
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->m(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/widget/SelectableImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->l(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/widget/SelectableImageView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/widget/SelectableImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setNextFocusRightId(I)V

    goto/16 :goto_0

    .line 1035
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->m(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/chaton/widget/SelectableImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->n(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/SelectableImageView;->setNextFocusRightId(I)V

    goto/16 :goto_0

    .line 1039
    :sswitch_7
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1040
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1042
    :cond_f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1043
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_17

    .line 1044
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/CoverStoryAdd;

    .line 1046
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/CoverStoryAdd;->metaid:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 1047
    const-string v1, "mypage_coverstory_state"

    const-string v2, "updated"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    const-string v1, "coverstory_metaid"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/CoverStoryAdd;->metaid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/CoverStoryAdd;->contentid:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 1050
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_10

    .line 1051
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CoverStoryControl.METHOD_ADD_COVERSTORY entry.contentid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/CoverStoryAdd;->contentid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    :cond_10
    const-string v1, "coverstory_contentid"

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/CoverStoryAdd;->contentid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    :cond_11
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->o(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->p(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 1061
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->o(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->q(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 1065
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->o(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1066
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    .line 1067
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ChatOnGraphics.MY_COVERSTORY_NAME + ext: mycoverstory."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/MyPageFragment;->r(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mycoverstory."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1069
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1070
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1071
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1073
    :cond_12
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->o(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 1076
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/CoverStoryAdd;->metacontents:Ljava/lang/String;

    const-string v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1077
    array-length v2, v1

    if-lez v2, :cond_13

    .line 1078
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mycoverstory."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v1, v1, v3

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1079
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "entry.metacontents: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/CoverStoryAdd;->metacontents:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " // mFileName: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    const-string v0, "coverstory_file_name"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;Landroid/widget/ImageView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1115
    :cond_14
    :goto_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1116
    const-string v0, "[CoverStory] CoverStoryControl.METHOD_ADD_COVERSTORY"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1055
    :cond_15
    const-string v1, "coverstory_contentid"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1084
    :catch_0
    move-exception v0

    .line 1085
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_16

    .line 1086
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1088
    :cond_16
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(Lcom/sec/chaton/userprofile/MyPageFragment;I)V

    goto :goto_2

    .line 1092
    :cond_17
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0122

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/userprofile/bf;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/bf;-><init>(Lcom/sec/chaton/userprofile/ba;)V

    invoke-virtual {v1, v5, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/userprofile/be;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/be;-><init>(Lcom/sec/chaton/userprofile/ba;)V

    invoke-virtual {v1, v7, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto :goto_2

    .line 1120
    :sswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1121
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "coverstory_metaid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1122
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v2, v3, :cond_1d

    .line 1123
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/sec/chaton/io/entry/inner/CoverStory;

    .line 1124
    iget-object v0, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 1125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CoverStoryControl.METHOD_GET_COVERSTORY previousMetaId : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CoverStoryControl.METHOD_GET_COVERSTORY coverStoryInfo.metaid : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 1130
    iget-object v0, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1131
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->r(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mycoverstory."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1132
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_18

    .line 1133
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY Same coverstory~ PASS!!!"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1136
    :cond_18
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY Same metaid but there is no coverstoryfile~!!!"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    :cond_19
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY Update coverstory~!!!"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    iget-object v0, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 1141
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(Ljava/lang/String;)Z

    move-result v0

    .line 1142
    if-nez v0, :cond_1a

    .line 1143
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0902b7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/userprofile/MyPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0902b8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    :cond_1a
    const-string v0, "coverstory_contentid"

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->contentid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    :goto_3
    const-string v0, "mypage_coverstory_state"

    const-string v1, "updated"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    const-string v0, "coverstory_metaid"

    iget-object v1, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1151
    :cond_1b
    const-string v0, "coverstory_contentid"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    new-instance v0, Lcom/sec/chaton/poston/a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->host:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metacontents:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->u(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/userprofile/MyPageFragment;->h()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v5}, Lcom/sec/chaton/userprofile/MyPageFragment;->r(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "mycoverstory.jpg"

    iget-object v7, v8, Lcom/sec/chaton/io/entry/inner/CoverStory;->metaid:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->v(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_3

    .line 1168
    :cond_1c
    const-string v0, "Set RandomCoverStory as randomId coverStoryInfo.metaid is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->d()Z

    goto/16 :goto_0

    .line 1173
    :cond_1d
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1174
    const-string v0, "CoverStoryControl.METHOD_GET_COVERSTORY http fail"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1179
    :sswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1180
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1e

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 1181
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1183
    :cond_1e
    if-nez v0, :cond_1f

    .line 1184
    const-string v0, "CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD (httpEntry == null)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1187
    :cond_1f
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_0

    .line 1188
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;

    .line 1189
    const-string v6, "mycoverstory.jpg"

    .line 1190
    const-string v0, "coverstory_file_name"

    invoke-static {v0, v6}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    const-string v0, "mypage_coverstory_state"

    const-string v2, "updated"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1192
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "coverstory_contentid"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1193
    new-instance v0, Lcom/sec/chaton/poston/a;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;->fileurl:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->u(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v5}, Lcom/sec/chaton/userprofile/MyPageFragment;->r(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->v(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 1199
    :sswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1203
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_21

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_21

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_21

    .line 1204
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/coverstory/random/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1205
    sget-boolean v2, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v2, :cond_20

    .line 1206
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "METHOD_CONTENT_COVERSTORY_LIST created and folder remove : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208
    :cond_20
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/CoverStorySampleList;

    .line 1210
    const-string v2, "coverstory_first_set"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1211
    const-string v2, "get_coverstory_sample_timestamp"

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/CoverStorySampleList;->timestamp:Ljava/lang/Long;

    invoke-static {v2, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1212
    invoke-static {v1}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1214
    :cond_21
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 1215
    const-string v0, "METHOD_CONTENT_COVERSTORY_LIST failed "

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1220
    :sswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1221
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_22

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 1222
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->j(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1224
    :cond_22
    if-nez v0, :cond_23

    .line 1225
    const-string v0, "CoverStoryControl.METHOD_CONTENT_COVERSTORY_DOWNLOAD_RANDOM (httpEntry == null)"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1228
    :cond_23
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_0

    .line 1229
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;

    .line 1230
    const-string v6, "mycoverstory.jpg"

    .line 1231
    const-string v0, "coverstory_file_name"

    invoke-static {v0, v6}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1233
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "coverstory_contentid"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1234
    new-instance v0, Lcom/sec/chaton/poston/a;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/CoverStoryDownload;->fileurl:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->u(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/poston/a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v4}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v5}, Lcom/sec/chaton/userprofile/MyPageFragment;->r(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/poston/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1235
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->v(Lcom/sec/chaton/userprofile/MyPageFragment;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ba;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 898
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x192 -> :sswitch_4
        0x194 -> :sswitch_5
        0x197 -> :sswitch_6
        0x19b -> :sswitch_1
        0x385 -> :sswitch_2
        0x387 -> :sswitch_3
        0xbb9 -> :sswitch_8
        0xbba -> :sswitch_7
        0xbbc -> :sswitch_a
        0xbbd -> :sswitch_9
        0xbbe -> :sswitch_b
    .end sparse-switch
.end method
