.class public Lcom/sec/chaton/userprofile/MyInfoFragment;
.super Landroid/support/v4/app/Fragment;
.source "MyInfoFragment.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/LinearLayout;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/ImageView;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Landroid/view/View;

.field private m:Landroid/widget/LinearLayout;

.field private n:Landroid/view/View;

.field private o:Landroid/widget/ImageView;

.field private p:Lcom/sec/chaton/d/w;

.field private q:Landroid/view/View;

.field private r:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/userprofile/MyInfoFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 449
    new-instance v0, Lcom/sec/chaton/userprofile/aj;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/aj;-><init>(Lcom/sec/chaton/userprofile/MyInfoFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->r:Landroid/os/Handler;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x4

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "original birthday str:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/userprofile/MyInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 389
    :cond_0
    const-string v0, ""

    .line 444
    :cond_1
    :goto_0
    return-object v0

    .line 392
    :cond_2
    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "-"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 393
    :cond_3
    const-string v0, "\\s"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 394
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trimmed birthday str:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/userprofile/MyInfoFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    :goto_1
    const-string v1, "-|\\/|\\s|\\.|\\,"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 405
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v5, :cond_7

    .line 406
    const/4 v0, 0x0

    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit16 v2, v0, -0x76c

    .line 407
    const/4 v0, 0x1

    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 408
    const/4 v0, 0x2

    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 420
    :goto_2
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v2, v1, v0}, Ljava/util/Date;-><init>(III)V

    .line 423
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 425
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v1, v5, :cond_1

    .line 426
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 427
    :cond_4
    const-string v1, "\\s"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 430
    :cond_5
    const-string v1, "-|\\/|\\s|\\.|\\,"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 432
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v4, :cond_8

    .line 433
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move-object v0, p0

    .line 396
    goto/16 :goto_1

    .line 410
    :cond_7
    const/16 v2, 0x64

    .line 411
    const/4 v0, 0x0

    :try_start_2
    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 412
    const/4 v0, 0x1

    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    goto :goto_2

    .line 414
    :catch_0
    move-exception v0

    .line 416
    const-string v0, "change date format error"

    sget-object v1, Lcom/sec/chaton/userprofile/MyInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    .line 417
    goto/16 :goto_0

    .line 434
    :cond_8
    const/4 v2, 0x2

    :try_start_3
    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v4, :cond_1

    .line 435
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 436
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    goto/16 :goto_0

    .line 439
    :catch_1
    move-exception v0

    .line 441
    const-string v0, "change date format error"

    sget-object v1, Lcom/sec/chaton/userprofile/MyInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    goto/16 :goto_0
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 136
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->i:Ljava/lang/String;

    .line 137
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->j:Ljava/lang/String;

    .line 138
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    invoke-static {}, Lcom/sec/chaton/util/am;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->k:Ljava/lang/String;

    .line 143
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 144
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0c0055

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 150
    :cond_1
    :goto_1
    return-void

    .line 141
    :cond_2
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->k:Ljava/lang/String;

    goto :goto_0

    .line 147
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0c0057

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/MyInfoFragment;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->c:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/userprofile/ag;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/ag;-><init>(Lcom/sec/chaton/userprofile/MyInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/MyInfoFragment;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->e()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->p:Lcom/sec/chaton/d/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->c(Ljava/lang/String;)V

    .line 212
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/MyInfoFragment;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->c()V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/MyInfoFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->p:Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->f(Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/userprofile/MyInfoFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->m:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private e()V
    .locals 7

    .prologue
    const v6, 0x7f0c0055

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "show_phone_number_to_all"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v5, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 230
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->n:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/userprofile/ah;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/ah;-><init>(Lcom/sec/chaton/userprofile/MyInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_4

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setFocusable(Z)V

    .line 278
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 308
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Landroid/widget/TextView;)V

    .line 310
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0c0057

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 247
    :cond_1
    const-string v0, "for_wifi_only_device"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/chaton/util/am;->w()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 248
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 251
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->d:Landroid/widget/TextView;

    const v1, 0x7f0b0273

    invoke-virtual {p0, v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->n:Landroid/view/View;

    new-instance v1, Lcom/sec/chaton/userprofile/ai;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/ai;-><init>(Lcom/sec/chaton/userprofile/MyInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 282
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method static synthetic f(Lcom/sec/chaton/userprofile/MyInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/widget/TextView;)V
    .locals 5

    .prologue
    const v4, 0x7f0b008f

    .line 354
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_type"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 355
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "birthday"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 357
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 359
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setBirthDay birthday : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/userprofile/MyInfoFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    if-eqz p1, :cond_3

    .line 362
    :try_start_0
    const-string v2, "FULL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "FULL_HIDE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 363
    :cond_2
    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 378
    :goto_1
    return-void

    .line 364
    :cond_4
    const-string v2, "SHORT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "SHORT_HIDE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 365
    :cond_5
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 366
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 367
    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 375
    :catch_0
    move-exception v0

    .line 376
    const-string v0, "setBirthDay error"

    sget-object v1, Lcom/sec/chaton/userprofile/MyInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 368
    :cond_6
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b008f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 369
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b008f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 371
    :cond_7
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b008f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 85
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a()V

    .line 86
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->r:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->p:Lcom/sec/chaton/d/w;

    .line 88
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->c()V

    .line 90
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->d()V

    .line 92
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 96
    const v0, 0x7f0300ca

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f07037d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->b:Landroid/widget/LinearLayout;

    .line 100
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->b:Landroid/widget/LinearLayout;

    const-string v1, "#F0EEEA"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f070387

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->c:Landroid/view/View;

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f070388

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->f:Landroid/widget/TextView;

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f070383

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->l:Landroid/view/View;

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f070384

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->e:Landroid/widget/TextView;

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f070385

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->g:Landroid/widget/ImageView;

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f070386

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->h:Landroid/widget/ImageView;

    .line 114
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f07037e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->n:Landroid/view/View;

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f070380

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->o:Landroid/widget/ImageView;

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f070381

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->m:Landroid/widget/LinearLayout;

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    const v1, 0x7f07037f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->d:Landroid/widget/TextView;

    .line 120
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->e()V

    .line 121
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->b()V

    .line 123
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->setHasOptionsMenu(Z)V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->q:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->p:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->a()V

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/userprofile/MyInfoFragment;->p:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->g()V

    .line 162
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 163
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 155
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 156
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 129
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 131
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a()V

    .line 132
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->e()V

    .line 133
    return-void
.end method
