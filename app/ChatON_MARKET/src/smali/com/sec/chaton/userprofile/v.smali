.class Lcom/sec/chaton/userprofile/v;
.super Ljava/lang/Object;
.source "CoverstorySampleFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 173
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 116
    sget v0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->b:I

    if-ne p1, v0, :cond_1

    .line 117
    if-eqz p3, :cond_1

    .line 121
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CoverStory list count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->a(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 126
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 160
    :goto_0
    if-eqz p3, :cond_1

    .line 161
    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 167
    :cond_1
    return-void

    .line 130
    :cond_2
    :goto_2
    :try_start_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 131
    const-string v0, "coverstory_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 132
    const-string v1, "coverstory_thumb_url"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 133
    const-string v2, "coverstory_filename"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 134
    iget-object v3, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->a(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v4, Lcom/sec/chaton/userprofile/t;

    invoke-direct {v4, v0, v1, v2}, Lcom/sec/chaton/userprofile/t;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    sget-boolean v2, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v2, :cond_2

    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CoverStorySampleTable.KEY_COVERSTORY_ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "CoverStorySampleTable.KEY_COVERSTORY_THUMB_URL: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 155
    :catch_0
    move-exception v0

    .line 156
    :try_start_2
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 157
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 160
    :cond_3
    if-eqz p3, :cond_1

    goto :goto_1

    .line 140
    :cond_4
    :try_start_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->b(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Lcom/sec/chaton/userprofile/q;

    move-result-object v0

    if-nez v0, :cond_7

    .line 142
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    new-instance v1, Lcom/sec/chaton/userprofile/q;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f03004f

    iget-object v4, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->a(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v5}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->c(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Lcom/sec/common/f/c;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/userprofile/q;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->a(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;Lcom/sec/chaton/userprofile/q;)Lcom/sec/chaton/userprofile/q;

    .line 148
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->b(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Lcom/sec/chaton/userprofile/q;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/userprofile/q;->a(Lcom/sec/chaton/userprofile/s;)V

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->d(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Landroid/widget/GridView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->b(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Lcom/sec/chaton/userprofile/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 160
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_5

    .line 161
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 160
    :cond_5
    throw v0

    .line 145
    :cond_6
    :try_start_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    new-instance v1, Lcom/sec/chaton/userprofile/q;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f03004e

    iget-object v4, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->a(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v5}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->c(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Lcom/sec/common/f/c;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/userprofile/q;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->a(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;Lcom/sec/chaton/userprofile/q;)Lcom/sec/chaton/userprofile/q;

    goto :goto_3

    .line 151
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/userprofile/v;->a:Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->b(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Lcom/sec/chaton/userprofile/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/q;->notifyDataSetChanged()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 179
    return-void
.end method
