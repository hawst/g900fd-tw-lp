.class public Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;
.super Ljava/lang/Object;
.source "ProfileImageHistoryFragment.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public FullfileName:Ljava/lang/String;

.field public Represent:Ljava/lang/String;

.field public ThumbfileName:Ljava/lang/String;

.field public dirCachePath:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public imageId:Ljava/lang/String;

.field public selectedImage:Landroid/widget/ImageView;

.field public thumbImage:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 862
    new-instance v0, Lcom/sec/chaton/userprofile/cg;

    invoke-direct {v0}, Lcom/sec/chaton/userprofile/cg;-><init>()V

    sput-object v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 831
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 834
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->a(Landroid/os/Parcel;)V

    .line 835
    return-void
.end method

.method private a(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 853
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    .line 854
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    .line 855
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    .line 856
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->ThumbfileName:Ljava/lang/String;

    .line 857
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    .line 858
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    .line 859
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 839
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 844
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 845
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 846
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 847
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->ThumbfileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 848
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 849
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 850
    return-void
.end method
