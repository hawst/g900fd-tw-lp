.class Lcom/sec/chaton/userprofile/cf;
.super Ljava/lang/Object;
.source "ProfileImageHistoryFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V
    .locals 0

    .prologue
    .line 996
    iput-object p1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const v6, 0x7f0b017f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1000
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mProfileListView Position: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;

    .line 1003
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mProfileListView view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    iget-object v3, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    .line 1007
    iget-object v4, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z

    move-result v4

    if-nez v4, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-nez v4, :cond_0

    move v0, v1

    .line 1057
    :goto_0
    return v0

    .line 1011
    :cond_0
    iget-object v4, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->j(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1012
    iget-object v4, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v5, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1013
    iget-object v4, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v5, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1014
    iget-object v4, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4, p3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;I)I

    .line 1015
    iget-object v4, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    sput-object v4, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->a:Ljava/lang/String;

    .line 1017
    iget-object v4, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-virtual {v4, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->setHasOptionsMenu(Z)V

    .line 1018
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->k(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1019
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v1}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1020
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1021
    const-string v1, "mIsFirstOptionMenuOpen = true"

    sget-object v4, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Z)Z

    .line 1026
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-virtual {v5, v6}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v5}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v5}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->f(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 1027
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)V

    .line 1028
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->j(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 1029
    iget-object v1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-static {v1, v4, v5, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1030
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_3

    .line 1031
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "item.id :"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dirCachePath+FullfileName : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1037
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    const v1, 0x7f0d0009

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;I)I

    .line 1038
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 1039
    const-string v0, "profile_image_long_click_main"

    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    :cond_4
    :goto_1
    invoke-virtual {v3, v6}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I

    move-result v1

    iget-object v4, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->q(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 1050
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v1, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "profile_f_mine_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v5}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1053
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-virtual {v3}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1054
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->r(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    :cond_5
    move v0, v2

    .line 1057
    goto/16 :goto_0

    .line 1042
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/userprofile/cf;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    const v1, 0x7f0d0008

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;I)I

    .line 1043
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 1044
    const-string v0, "profile_image_long_click"

    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
