.class public Lcom/sec/chaton/userprofile/bq;
.super Lcom/sec/common/f/a;
.source "ProfileImageDispatcherTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field a:Z

.field private b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private e:I

.field private i:I

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Landroid/widget/ImageView;

.field private m:Landroid/widget/ImageView;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Z

.field private s:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    invoke-direct {p0, p3}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 48
    const-string v0, "ProfileImageDispatcherTask"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/bq;->b:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    .line 59
    iput-object v1, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/bq;->p:Ljava/lang/String;

    .line 119
    iput p4, p0, Lcom/sec/chaton/userprofile/bq;->e:I

    .line 120
    iput p5, p0, Lcom/sec/chaton/userprofile/bq;->i:I

    .line 122
    iput-object p6, p0, Lcom/sec/chaton/userprofile/bq;->j:Ljava/lang/String;

    .line 123
    iput-object p7, p0, Lcom/sec/chaton/userprofile/bq;->k:Ljava/lang/String;

    .line 124
    iput-object p3, p0, Lcom/sec/chaton/userprofile/bq;->n:Ljava/lang/String;

    .line 126
    iput-object p1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    .line 127
    iput-object p2, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    .line 128
    iput-boolean p8, p0, Lcom/sec/chaton/userprofile/bq;->o:Z

    .line 131
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/userprofile/bq;->c:I

    .line 132
    const v0, 0x7f020117

    iput v0, p0, Lcom/sec/chaton/userprofile/bq;->d:I

    .line 133
    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLandroid/os/Handler;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 137
    invoke-direct {p0, p3}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 48
    const-string v0, "ProfileImageDispatcherTask"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/bq;->b:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    .line 59
    iput-object v1, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/bq;->p:Ljava/lang/String;

    .line 139
    iput p4, p0, Lcom/sec/chaton/userprofile/bq;->e:I

    .line 140
    iput p5, p0, Lcom/sec/chaton/userprofile/bq;->i:I

    .line 142
    iput-object p6, p0, Lcom/sec/chaton/userprofile/bq;->j:Ljava/lang/String;

    .line 143
    iput-object p7, p0, Lcom/sec/chaton/userprofile/bq;->k:Ljava/lang/String;

    .line 144
    iput-object p3, p0, Lcom/sec/chaton/userprofile/bq;->n:Ljava/lang/String;

    .line 146
    iput-object p1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    .line 147
    iput-object p2, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    .line 148
    iput-boolean p8, p0, Lcom/sec/chaton/userprofile/bq;->o:Z

    .line 149
    iput-object p9, p0, Lcom/sec/chaton/userprofile/bq;->s:Landroid/os/Handler;

    .line 152
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/userprofile/bq;->c:I

    .line 153
    const v0, 0x7f020117

    iput v0, p0, Lcom/sec/chaton/userprofile/bq;->d:I

    .line 154
    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0, p2}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 48
    const-string v0, "ProfileImageDispatcherTask"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/bq;->b:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    .line 59
    iput-object v1, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/bq;->p:Ljava/lang/String;

    .line 78
    iput p3, p0, Lcom/sec/chaton/userprofile/bq;->e:I

    .line 79
    iput p4, p0, Lcom/sec/chaton/userprofile/bq;->i:I

    .line 81
    iput-object p5, p0, Lcom/sec/chaton/userprofile/bq;->j:Ljava/lang/String;

    .line 82
    iput-object p6, p0, Lcom/sec/chaton/userprofile/bq;->k:Ljava/lang/String;

    .line 83
    iput-object p2, p0, Lcom/sec/chaton/userprofile/bq;->n:Ljava/lang/String;

    .line 85
    iput-object p1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    .line 88
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/userprofile/bq;->c:I

    .line 89
    const v0, 0x7f020117

    iput v0, p0, Lcom/sec/chaton/userprofile/bq;->d:I

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLandroid/os/Handler;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0, p2}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 48
    const-string v0, "ProfileImageDispatcherTask"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/bq;->b:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    .line 59
    iput-object v1, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/bq;->p:Ljava/lang/String;

    .line 96
    iput p3, p0, Lcom/sec/chaton/userprofile/bq;->e:I

    .line 97
    iput p4, p0, Lcom/sec/chaton/userprofile/bq;->i:I

    .line 99
    iput-object p5, p0, Lcom/sec/chaton/userprofile/bq;->j:Ljava/lang/String;

    .line 100
    iput-object p6, p0, Lcom/sec/chaton/userprofile/bq;->k:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Lcom/sec/chaton/userprofile/bq;->n:Ljava/lang/String;

    .line 102
    iput-boolean p7, p0, Lcom/sec/chaton/userprofile/bq;->o:Z

    .line 103
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/bq;->p:Ljava/lang/String;

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/bq;->q:Z

    .line 106
    iput-boolean p7, p0, Lcom/sec/chaton/userprofile/bq;->r:Z

    .line 108
    iput-object p1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    .line 109
    iput-object p8, p0, Lcom/sec/chaton/userprofile/bq;->s:Landroid/os/Handler;

    .line 112
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/userprofile/bq;->c:I

    .line 113
    const v0, 0x7f020117

    iput v0, p0, Lcom/sec/chaton/userprofile/bq;->d:I

    .line 114
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 3

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->e()Landroid/widget/ImageView;

    .line 176
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/userprofile/bq;->c:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 177
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    if-nez v1, :cond_1

    .line 184
    :goto_0
    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    .line 185
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 188
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 181
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 276
    .line 277
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 279
    if-eqz p1, :cond_5

    .line 280
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 281
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    .line 284
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 289
    :goto_0
    iget-object v4, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    if-eqz v4, :cond_0

    .line 290
    iget-object v4, p0, Lcom/sec/chaton/userprofile/bq;->l:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 293
    :cond_0
    if-nez p2, :cond_1

    iget-boolean v4, p0, Lcom/sec/chaton/userprofile/bq;->a:Z

    if-eqz v4, :cond_7

    .line 295
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->e()Landroid/widget/ImageView;

    .line 297
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    if-nez v4, :cond_6

    .line 298
    :cond_2
    iget-object v4, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    if-eqz v4, :cond_3

    .line 299
    iget-object v4, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 301
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->e()Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 328
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bq;->s:Landroid/os/Handler;

    if-eqz v1, :cond_4

    .line 329
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 330
    const-string v4, "download_success"

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v2

    :goto_2
    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 331
    const-string v0, "filename_id"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bq;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bq;->s:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bq;->s:Landroid/os/Handler;

    invoke-static {v2, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 336
    :cond_4
    return-void

    .line 286
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/userprofile/bq;->d:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 287
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 304
    :cond_6
    iget-object v4, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 305
    iget-object v4, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 308
    :cond_7
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 310
    new-instance v5, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/graphics/drawable/Drawable;

    aput-object v4, v6, v3

    aput-object v1, v6, v2

    invoke-direct {v5, v6}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 313
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    if-nez v1, :cond_a

    .line 314
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    if-eqz v1, :cond_9

    .line 315
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 317
    :cond_9
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->e()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 318
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->e()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 325
    :goto_3
    const/16 v1, 0x64

    invoke-virtual {v5, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_1

    .line 320
    :cond_a
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->e()Landroid/widget/ImageView;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 321
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 322
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_b
    move v0, v3

    .line 330
    goto :goto_2
.end method

.method public b()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    :cond_0
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p0, v0, v1}, Lcom/sec/chaton/userprofile/bq;->a(Ljava/util/concurrent/Callable;J)V

    .line 199
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 204
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/chaton/userprofile/bq;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bq;->k:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 211
    :cond_0
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bq;->n:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/util/a/a;->a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 236
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/ad;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 239
    if-eqz v0, :cond_1

    .line 240
    const/16 v2, 0xa0

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 244
    :cond_1
    iget-boolean v2, p0, Lcom/sec/chaton/userprofile/bq;->o:Z

    if-eqz v2, :cond_2

    .line 246
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->k()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/sec/chaton/userprofile/bq;->e:I

    iget v4, p0, Lcom/sec/chaton/userprofile/bq;->i:I

    invoke-static {v2, v1, v3, v4}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/ad;->a(Landroid/graphics/Bitmap;)Ljava/io/ByteArrayOutputStream;

    move-result-object v1

    .line 248
    const-string v2, "myprofile.png_"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/ad;->a(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 250
    if-nez v1, :cond_3

    .line 251
    const-string v1, "saveByteArrayOutputStreamToFileForProfile returns null"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bq;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :cond_2
    :goto_0
    return-object v0

    .line 255
    :cond_3
    const/16 v2, 0x120

    const/16 v3, 0x120

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 257
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/userprofile/bq;->p:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 258
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/userprofile/bq;->p:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/sec/chaton/util/bt;->b(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 260
    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 261
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    .line 262
    const-string v1, "profile_image_status"

    const-string v2, "updated"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 269
    :catch_0
    move-exception v0

    .line 270
    const/4 v0, 0x0

    goto :goto_0

    .line 264
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File length is 0:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ProfileUploadTask"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bq;->m:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 344
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 346
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/chaton/userprofile/bq;->a(Landroid/view/View;)V

    .line 348
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 349
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 351
    :cond_1
    return-void
.end method

.method public e()Landroid/widget/ImageView;
    .locals 2

    .prologue
    .line 355
    invoke-super {p0}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v0

    .line 356
    instance-of v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    if-eqz v1, :cond_0

    .line 357
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/chaton/userprofile/bq;->a:Z

    .line 361
    :goto_0
    check-cast v0, Landroid/widget/ImageView;

    return-object v0

    .line 359
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/chaton/userprofile/bq;->a:Z

    goto :goto_0
.end method

.method public f()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 366
    invoke-super {p0}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Landroid/view/View;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/bq;->e()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method
