.class Lcom/sec/chaton/userprofile/c;
.super Landroid/os/Handler;
.source "BirthdayFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/BirthdayFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/BirthdayFragment;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_1

    .line 242
    const-string v0, "is Finishing "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :cond_1
    :goto_0
    return-void

    .line 246
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->c(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->c(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 250
    :cond_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 251
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 253
    :sswitch_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_4

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->d(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->e(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->a(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->f(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->b(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_0

    .line 261
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0097

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 266
    :sswitch_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_6

    .line 267
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_5

    .line 268
    const-string v0, "ProfileControl.METHOD_GET_PROFILE_ALL Success"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->g(Lcom/sec/chaton/userprofile/BirthdayFragment;)V

    goto/16 :goto_0

    .line 272
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/userprofile/c;->a:Lcom/sec/chaton/userprofile/BirthdayFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 273
    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b008a

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0024

    new-instance v2, Lcom/sec/chaton/userprofile/e;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/e;-><init>(Lcom/sec/chaton/userprofile/c;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00dc

    new-instance v2, Lcom/sec/chaton/userprofile/d;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/d;-><init>(Lcom/sec/chaton/userprofile/c;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 251
    :sswitch_data_0
    .sparse-switch
        0x191 -> :sswitch_0
        0x19b -> :sswitch_1
    .end sparse-switch
.end method
