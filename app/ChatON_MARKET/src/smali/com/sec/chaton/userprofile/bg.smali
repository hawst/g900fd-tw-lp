.class Lcom/sec/chaton/userprofile/bg;
.super Ljava/lang/Object;
.source "MyPageFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/MyPageFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V
    .locals 0

    .prologue
    .line 1792
    iput-object p1, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1796
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->w(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1797
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->w(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 1799
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    const-string v1, "/profile/"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/lang/String;)V

    .line 1800
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tmp_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpeg_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1801
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/MyPageFragment;->w(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/MyPageFragment;->x(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;Ljava/io/File;)Ljava/io/File;

    .line 1802
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->k(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v0

    .line 1803
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 1804
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Create File] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->w(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->x(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->k(Lcom/sec/chaton/userprofile/MyPageFragment;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;Landroid/net/Uri;)Landroid/net/Uri;

    .line 1808
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1809
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    const v2, 0x7f0b002b

    invoke-virtual {v1, v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1859
    :cond_2
    :goto_0
    return-void

    .line 1816
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1817
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1821
    const v0, 0x7f0d001d

    .line 1833
    const v1, 0x7f0b0204

    .line 1834
    iget-object v2, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/userprofile/MyPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 1837
    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/MyPageFragment;->y(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 1843
    const v0, 0x7f0b0039

    new-instance v1, Lcom/sec/chaton/userprofile/bh;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/bh;-><init>(Lcom/sec/chaton/userprofile/bg;)V

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 1849
    invoke-virtual {v2}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1850
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bg;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->z(Lcom/sec/chaton/userprofile/MyPageFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1851
    invoke-virtual {v2}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1854
    :catch_0
    move-exception v0

    .line 1855
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 1856
    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
