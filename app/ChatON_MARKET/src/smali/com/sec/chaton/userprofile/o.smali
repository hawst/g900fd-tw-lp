.class Lcom/sec/chaton/userprofile/o;
.super Ljava/lang/Object;
.source "BirthdayImpl.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/j;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/j;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/sec/chaton/userprofile/o;->a:Lcom/sec/chaton/userprofile/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/userprofile/o;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->h(Lcom/sec/chaton/userprofile/j;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 314
    if-eqz p2, :cond_1

    .line 315
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_year_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 316
    iget-object v0, p0, Lcom/sec/chaton/userprofile/o;->a:Lcom/sec/chaton/userprofile/j;

    const-string v1, "FULL"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    .line 327
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/o;->a:Lcom/sec/chaton/userprofile/j;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/o;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/userprofile/j;->a(Ljava/lang/String;)V

    .line 328
    return-void

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/o;->a:Lcom/sec/chaton/userprofile/j;

    const-string v1, "SHORT"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 321
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_year_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/userprofile/o;->a:Lcom/sec/chaton/userprofile/j;

    const-string v1, "FULL_HIDE"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 324
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/o;->a:Lcom/sec/chaton/userprofile/j;

    const-string v1, "SHORT_HIDE"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
