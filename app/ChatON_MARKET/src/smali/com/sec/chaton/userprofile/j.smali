.class public Lcom/sec/chaton/userprofile/j;
.super Ljava/lang/Object;
.source "BirthdayImpl.java"


# instance fields
.field a:Ljava/text/SimpleDateFormat;

.field b:Ljava/text/SimpleDateFormat;

.field c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Landroid/widget/RelativeLayout;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/CheckBox;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/FrameLayout;

.field private n:Landroid/widget/LinearLayout;

.field private o:Ljava/util/Calendar;

.field private p:Lcom/sec/chaton/d/w;

.field private q:Landroid/app/ProgressDialog;

.field private r:Lcom/sec/chaton/userprofile/y;

.field private s:Landroid/app/Activity;

.field private t:Landroid/view/View$OnClickListener;

.field private u:Landroid/os/Handler;

.field private v:Lcom/sec/chaton/userprofile/ab;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->o:Ljava/util/Calendar;

    .line 167
    new-instance v0, Lcom/sec/chaton/userprofile/l;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/l;-><init>(Lcom/sec/chaton/userprofile/j;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->t:Landroid/view/View$OnClickListener;

    .line 199
    new-instance v0, Lcom/sec/chaton/userprofile/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/m;-><init>(Lcom/sec/chaton/userprofile/j;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->u:Landroid/os/Handler;

    .line 307
    new-instance v0, Lcom/sec/chaton/userprofile/o;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/o;-><init>(Lcom/sec/chaton/userprofile/j;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 344
    new-instance v0, Lcom/sec/chaton/userprofile/p;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/p;-><init>(Lcom/sec/chaton/userprofile/j;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->v:Lcom/sec/chaton/userprofile/ab;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/j;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/j;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/chaton/userprofile/j;->f:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/j;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->q:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/userprofile/j;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->g:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/userprofile/j;)Lcom/sec/chaton/userprofile/y;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->r:Lcom/sec/chaton/userprofile/y;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    const v3, 0x7f0b008f

    const/4 v2, 0x0

    .line 180
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 181
    const-string v0, "FULL"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "FULL_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    :goto_0
    return-void

    .line 183
    :cond_1
    const-string v0, "SHORT"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "SHORT_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->l:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 188
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    const-string v0, "birthday_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 193
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    const-string v0, "birthday_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/userprofile/j;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/j;->i()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/userprofile/j;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->n:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private h()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 231
    const-string v0, "init"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_type"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    .line 234
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    const-string v0, "SHORT_HIDE"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    .line 237
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->g:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 242
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->g:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 245
    :cond_1
    const-string v0, "FULL"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "SHORT"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    :cond_2
    const-string v0, "birthday_show"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 252
    :goto_0
    const-string v0, "FULL"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "FULL_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 253
    :cond_3
    const-string v0, "birthday_year_show"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 258
    :goto_1
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/j;->g()V

    .line 259
    return-void

    .line 249
    :cond_4
    const-string v0, "birthday_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 255
    :cond_5
    const-string v0, "birthday_year_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1
.end method

.method static synthetic i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    return-object v0
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 263
    const/16 v2, 0x7d0

    .line 266
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[editBirthDay] mBirthDay="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget-object v3, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    if-eqz v3, :cond_0

    const-string v3, ""

    iget-object v4, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 269
    iget-object v2, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 271
    aget-object v1, v3, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 272
    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 273
    const/4 v0, 0x2

    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 280
    :cond_0
    invoke-virtual {p0, v2, v1, v0}, Lcom/sec/chaton/userprofile/j;->a(III)V

    .line 281
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/userprofile/j;)Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->o:Ljava/util/Calendar;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->n:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/userprofile/n;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/n;-><init>(Lcom/sec/chaton/userprofile/j;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 299
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/userprofile/j;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->l:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, -0x2

    const v6, 0x7f07014c

    const/4 v5, 0x0

    .line 81
    const v0, 0x7f0300e8

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 84
    const v0, 0x7f070416

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 85
    const v0, 0x7f07050b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    const v3, 0x7f0b0011

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 87
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 88
    new-instance v3, Lcom/sec/chaton/userprofile/k;

    invoke-direct {v3, p0}, Lcom/sec/chaton/userprofile/k;-><init>(Lcom/sec/chaton/userprofile/j;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    const v0, 0x7f07050d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 95
    const v3, 0x7f0b0091

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 96
    iget-object v3, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 97
    const v0, 0x7f07050e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 98
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    const v0, 0x7f07040d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->g:Landroid/widget/RelativeLayout;

    .line 102
    const v0, 0x7f07040f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->h:Landroid/view/View;

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->h:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 104
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 105
    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->h:Landroid/view/View;

    const v3, 0x7f07014d

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 106
    const v3, 0x7f0b0090

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 107
    const v0, 0x7f0b01b0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 108
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 111
    const v0, 0x7f07040e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "birthday_show"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setChecked : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "birthday_show"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->g:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    const v0, 0x7f070410

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->j:Landroid/view/View;

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->j:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 122
    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    const v3, 0x7f0b01b1

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->a:Ljava/text/SimpleDateFormat;

    .line 128
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->b:Ljava/text/SimpleDateFormat;

    .line 130
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->u:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->p:Lcom/sec/chaton/d/w;

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->q:Landroid/app/ProgressDialog;

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mOption : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const v0, 0x7f070411

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->n:Landroid/widget/LinearLayout;

    .line 135
    const v0, 0x7f070412

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->k:Landroid/view/View;

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->k:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->l:Landroid/widget/TextView;

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->k:Landroid/view/View;

    const v1, 0x7f0702ea

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->m:Landroid/widget/FrameLayout;

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->m:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 141
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 142
    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0203b6

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 143
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 144
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v1, v7, v7, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->m:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 148
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/j;->j()V

    .line 149
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/j;->h()V

    .line 151
    return-object v2
.end method

.method public a()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    .line 78
    return-void
.end method

.method protected a(III)V
    .locals 7

    .prologue
    .line 336
    new-instance v0, Lcom/sec/chaton/userprofile/y;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/j;->v:Lcom/sec/chaton/userprofile/ab;

    iget-object v6, p0, Lcom/sec/chaton/userprofile/j;->d:Ljava/lang/String;

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/userprofile/y;-><init>(Landroid/content/Context;Lcom/sec/chaton/userprofile/ab;IIILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->r:Lcom/sec/chaton/userprofile/y;

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->r:Lcom/sec/chaton/userprofile/y;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/userprofile/y;->a(III)V

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->r:Lcom/sec/chaton/userprofile/y;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/y;->show()V

    .line 342
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    .line 74
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 457
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[sendBirthday] mBirthDay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    new-instance v0, Lcom/sec/chaton/a/a/o;

    invoke-direct {v0}, Lcom/sec/chaton/a/a/o;-><init>()V

    .line 461
    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/a/o;->c(Ljava/lang/String;)V

    .line 462
    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->p:Lcom/sec/chaton/d/w;

    invoke-virtual {v1, v0, p1}, Lcom/sec/chaton/d/w;->a(Lcom/sec/chaton/a/a/o;Ljava/lang/String;)V

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->q:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 464
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 160
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->s:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 164
    :cond_0
    return v2
.end method

.method public b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 413
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 414
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 415
    new-instance v2, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 416
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "toDay="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 421
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/j;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    .line 424
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onClick::mBirthDay"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->f:Ljava/lang/String;

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 428
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 431
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 432
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/j;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    .line 434
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->e:Ljava/lang/String;

    const/4 v1, 0x5

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/j;->f:Ljava/lang/String;

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/j;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 437
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 445
    const-string v0, "onResume"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->q:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->q:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->r:Lcom/sec/chaton/userprofile/y;

    if-eqz v0, :cond_1

    .line 472
    iget-object v0, p0, Lcom/sec/chaton/userprofile/j;->r:Lcom/sec/chaton/userprofile/y;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/y;->dismiss()V

    .line 474
    :cond_1
    return-void
.end method
