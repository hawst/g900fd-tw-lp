.class Lcom/sec/chaton/userprofile/ag;
.super Ljava/lang/Object;
.source "MyInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/MyInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/MyInfoFragment;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ag;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 170
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ag;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings2/SettingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 172
    const-string v1, "fragment_choose"

    const-class v2, Lcom/sec/chaton/settings2/PrefFragmentBirthday;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ag;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->startActivity(Landroid/content/Intent;)V

    .line 179
    :goto_0
    return-void

    .line 175
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ag;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/userprofile/BirthdayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 176
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ag;->a:Lcom/sec/chaton/userprofile/MyInfoFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
