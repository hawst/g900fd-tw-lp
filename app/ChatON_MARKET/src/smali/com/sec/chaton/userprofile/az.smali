.class Lcom/sec/chaton/userprofile/az;
.super Ljava/lang/Object;
.source "MyPageFragment.java"

# interfaces
.implements Lcom/sec/chaton/util/bs;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/MyPageFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/MyPageFragment;)V
    .locals 0

    .prologue
    .line 749
    iput-object p1, p0, Lcom/sec/chaton/userprofile/az;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 4

    .prologue
    .line 752
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 753
    const-string v0, "mGetProfileImageList run"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    :cond_0
    const-string v0, "updated"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "mypage_coverstory_state"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 756
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 757
    const-string v0, " Don\'t call getCoverStory(); "

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/az;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->b(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    .line 776
    iget-object v0, p0, Lcom/sec/chaton/userprofile/az;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->c(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    .line 777
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 778
    iget-object v0, p0, Lcom/sec/chaton/userprofile/az;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->d(Lcom/sec/chaton/userprofile/MyPageFragment;)V

    .line 780
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 781
    const-string v0, "mGetProfileImageList finish"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    :cond_3
    const/4 v0, 0x1

    return v0

    .line 761
    :cond_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_5

    .line 762
    const-string v0, " coverstory default image loadDefaultCoverStory() in PollingScheduler"

    sget-object v1, Lcom/sec/chaton/userprofile/MyPageFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/userprofile/az;->a:Lcom/sec/chaton/userprofile/MyPageFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyPageFragment;->a(Lcom/sec/chaton/userprofile/MyPageFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Landroid/widget/ImageView;)Z

    goto :goto_0
.end method
