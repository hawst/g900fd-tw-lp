.class public Lcom/sec/chaton/userprofile/CoverstorySampleFragment;
.super Landroid/support/v4/app/Fragment;
.source "CoverstorySampleFragment.java"

# interfaces
.implements Lcom/sec/chaton/userprofile/s;


# static fields
.field public static b:I

.field private static c:Ljava/lang/String;

.field private static h:Lcom/sec/chaton/widget/j;

.field private static k:Landroid/net/Uri;

.field private static m:Lcom/sec/chaton/e/a/u;


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/CoverStorySample;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/view/View;

.field private e:Landroid/widget/GridView;

.field private f:Lcom/sec/chaton/d/i;

.field private g:Landroid/app/ProgressDialog;

.field private i:Lcom/sec/chaton/userprofile/q;

.field private j:Lcom/sec/common/f/c;

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/userprofile/t;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/lang/String;

.field private o:Lcom/sec/chaton/e/a/v;

.field private p:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->c:Ljava/lang/String;

    .line 48
    sget-object v0, Lcom/sec/chaton/e/k;->a:Landroid/net/Uri;

    sput-object v0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->k:Landroid/net/Uri;

    .line 54
    const/4 v0, 0x1

    sput v0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->b:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->a:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Lcom/sec/chaton/userprofile/v;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/v;-><init>(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->o:Lcom/sec/chaton/e/a/v;

    .line 197
    new-instance v0, Lcom/sec/chaton/userprofile/w;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/w;-><init>(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->p:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;Lcom/sec/chaton/userprofile/q;)Lcom/sec/chaton/userprofile/q;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->i:Lcom/sec/chaton/userprofile/q;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->l:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b()Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->m:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Lcom/sec/chaton/userprofile/q;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->i:Lcom/sec/chaton/userprofile/q;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->j:Lcom/sec/common/f/c;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->g:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->f:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->e()V

    .line 195
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->e:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->g:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/userprofile/CoverstorySampleFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/userprofile/t;)V
    .locals 4

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 252
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 253
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0205

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 271
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->g:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 264
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0902b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 265
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0902b8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 267
    invoke-virtual {p1}, Lcom/sec/chaton/userprofile/t;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->n:Ljava/lang/String;

    .line 268
    iget-object v2, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->f:Lcom/sec/chaton/d/i;

    invoke-virtual {p1}, Lcom/sec/chaton/userprofile/t;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/chaton/d/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 64
    new-instance v0, Lcom/sec/chaton/d/i;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->p:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/i;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->f:Lcom/sec/chaton/d/i;

    .line 65
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->o:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    sput-object v0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->m:Lcom/sec/chaton/e/a/u;

    .line 66
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 70
    const v0, 0x7f0300c8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->d:Landroid/view/View;

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->d:Landroid/view/View;

    const v1, 0x7f070379

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->e:Landroid/widget/GridView;

    .line 73
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->h:Lcom/sec/chaton/widget/j;

    .line 74
    sget-object v0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->h:Lcom/sec/chaton/widget/j;

    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->g:Landroid/app/ProgressDialog;

    .line 75
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->j:Lcom/sec/common/f/c;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->l:Ljava/util/ArrayList;

    .line 78
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->c()V

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->f:Lcom/sec/chaton/d/i;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->f:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->f()V

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->f:Lcom/sec/chaton/d/i;

    invoke-virtual {v0}, Lcom/sec/chaton/d/i;->g()V

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->f:Lcom/sec/chaton/d/i;

    .line 101
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 102
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->g:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->j:Lcom/sec/common/f/c;

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->j:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 92
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 276
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 277
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/CoverstorySampleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 280
    :cond_0
    return-void
.end method
