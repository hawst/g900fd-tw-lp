.class public Lcom/sec/chaton/userprofile/BirthdayFragment;
.super Landroid/support/v4/app/Fragment;
.source "BirthdayFragment.java"


# instance fields
.field a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field b:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field c:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Landroid/widget/RelativeLayout;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/CheckBox;

.field private j:Landroid/widget/RelativeLayout;

.field private k:Landroid/view/View;

.field private l:Landroid/widget/CheckBox;

.field private m:Landroid/view/View;

.field private n:Ljava/text/SimpleDateFormat;

.field private o:Ljava/text/SimpleDateFormat;

.field private p:Ljava/util/Calendar;

.field private q:Lcom/sec/chaton/d/w;

.field private r:Landroid/app/ProgressDialog;

.field private s:Lcom/sec/chaton/userprofile/y;

.field private t:Landroid/view/View;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/FrameLayout;

.field private w:Landroid/widget/LinearLayout;

.field private x:Landroid/view/View$OnClickListener;

.field private y:Landroid/view/View$OnClickListener;

.field private z:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 74
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->p:Ljava/util/Calendar;

    .line 193
    new-instance v0, Lcom/sec/chaton/userprofile/a;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/a;-><init>(Lcom/sec/chaton/userprofile/BirthdayFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->x:Landroid/view/View$OnClickListener;

    .line 200
    new-instance v0, Lcom/sec/chaton/userprofile/b;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/b;-><init>(Lcom/sec/chaton/userprofile/BirthdayFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->y:Landroid/view/View$OnClickListener;

    .line 237
    new-instance v0, Lcom/sec/chaton/userprofile/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/c;-><init>(Lcom/sec/chaton/userprofile/BirthdayFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->z:Landroid/os/Handler;

    .line 391
    new-instance v0, Lcom/sec/chaton/userprofile/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/g;-><init>(Lcom/sec/chaton/userprofile/BirthdayFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 416
    new-instance v0, Lcom/sec/chaton/userprofile/h;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/h;-><init>(Lcom/sec/chaton/userprofile/BirthdayFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->b:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 459
    new-instance v0, Lcom/sec/chaton/userprofile/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/i;-><init>(Lcom/sec/chaton/userprofile/BirthdayFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->c:Landroid/app/DatePickerDialog$OnDateSetListener;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->f:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->r:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/BirthdayFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    const v3, 0x7f0b008f

    const/4 v2, 0x1

    .line 214
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 215
    const-string v0, "FULL"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "FULL_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    :goto_0
    return-void

    .line 217
    :cond_1
    const-string v0, "SHORT"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "SHORT_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 218
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 219
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 220
    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->u:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 222
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->u:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    const-string v0, "birthday_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 227
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->u:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    const-string v0, "birthday_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->g:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->q:Lcom/sec/chaton/d/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->c(Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->j:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 296
    const-string v0, "init"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_type"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    .line 299
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    const-string v0, "SHORT"

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    .line 302
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->g:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->j:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 308
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->g:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->j:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 312
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 315
    :cond_1
    const-string v0, "FULL"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 316
    const-string v0, "birthday_show"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 317
    const-string v0, "birthday_year_show"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 338
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->d()V

    .line 339
    return-void

    .line 320
    :cond_3
    const-string v0, "SHORT"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 321
    const-string v0, "birthday_show"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 322
    const-string v0, "birthday_year_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 325
    :cond_4
    const-string v0, "FULL_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 326
    const-string v0, "birthday_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 327
    const-string v0, "birthday_year_show"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 329
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 330
    :cond_5
    const-string v0, "SHORT_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 331
    const-string v0, "birthday_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 332
    const-string v0, "birthday_year_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 343
    const/16 v2, 0x7d0

    .line 346
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[editBirthDay] mBirthDay="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    iget-object v3, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    if-eqz v3, :cond_0

    const-string v3, ""

    iget-object v4, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 349
    iget-object v2, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 351
    aget-object v1, v3, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 352
    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 353
    const/4 v0, 0x2

    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 360
    :cond_0
    invoke-virtual {p0, v2, v1, v0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->a(III)V

    .line 361
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/userprofile/BirthdayFragment;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->f()V

    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->w:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/userprofile/f;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/f;-><init>(Lcom/sec/chaton/userprofile/BirthdayFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/userprofile/BirthdayFragment;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->e()V

    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/userprofile/BirthdayFragment;)Lcom/sec/chaton/userprofile/y;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->s:Lcom/sec/chaton/userprofile/y;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/userprofile/BirthdayFragment;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->g()V

    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->p:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/userprofile/BirthdayFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/userprofile/BirthdayFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->u:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 569
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 570
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 571
    new-instance v2, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 572
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "toDay="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    return-object v0
.end method

.method protected a(III)V
    .locals 6

    .prologue
    .line 447
    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->c:Landroid/app/DatePickerDialog$OnDateSetListener;

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 448
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->show()V

    .line 457
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 615
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[sendBirthday] mBirthDay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 619
    new-instance v0, Lcom/sec/chaton/a/a/o;

    invoke-direct {v0}, Lcom/sec/chaton/a/a/o;-><init>()V

    .line 620
    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/a/a/o;->c(Ljava/lang/String;)V

    .line 621
    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->q:Lcom/sec/chaton/d/w;

    invoke-virtual {v1, v0, p1}, Lcom/sec/chaton/d/w;->a(Lcom/sec/chaton/a/a/o;Ljava/lang/String;)V

    .line 622
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->r:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 624
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 577
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    .line 580
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onClick::mBirthDay"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->f:Ljava/lang/String;

    .line 582
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 587
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    .line 590
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->e:Ljava/lang/String;

    const/4 v1, 0x5

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->f:Ljava/lang/String;

    .line 591
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 593
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 93
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 181
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 182
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const v8, 0x7f07014d

    const/4 v4, -0x2

    const/4 v7, 0x1

    const v6, 0x7f07014c

    const/4 v5, 0x0

    .line 97
    const v0, 0x7f0300e8

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 100
    const v0, 0x7f07040d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->g:Landroid/widget/RelativeLayout;

    .line 101
    const v0, 0x7f07040f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->h:Landroid/view/View;

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 103
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 104
    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->h:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 105
    const v3, 0x7f0b0090

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 106
    const v0, 0x7f0b01b0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 107
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 110
    const v0, 0x7f070411

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->w:Landroid/widget/LinearLayout;

    .line 111
    const v0, 0x7f070412

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->t:Landroid/view/View;

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->u:Landroid/widget/TextView;

    .line 114
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->t:Landroid/view/View;

    const v1, 0x7f0702ea

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->v:Landroid/widget/FrameLayout;

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 117
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0200b2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 119
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 120
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v1, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 125
    const v0, 0x7f07040e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "birthday_show"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setChecked : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "birthday_show"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->g:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    const v0, 0x7f070413

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->j:Landroid/widget/RelativeLayout;

    .line 135
    const v0, 0x7f070415

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->k:Landroid/view/View;

    .line 136
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 137
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 138
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 139
    const v1, 0x7f0b01b2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 141
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    const v0, 0x7f070414

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "birthday_year_show"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 145
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setYearChecked : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v3, "birthday_year_show"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->b:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->j:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    const v0, 0x7f070410

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->m:Landroid/view/View;

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 155
    const v1, 0x7f0b01b1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->n:Ljava/text/SimpleDateFormat;

    .line 162
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->o:Ljava/text/SimpleDateFormat;

    .line 164
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->z:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->q:Lcom/sec/chaton/d/w;

    .line 165
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->r:Landroid/app/ProgressDialog;

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mOption : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->e()V

    .line 171
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->h()V

    .line 172
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->f()V

    .line 173
    invoke-static {p0, v7}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 174
    return-object v2
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->q:Lcom/sec/chaton/d/w;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->q:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->a()V

    .line 631
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->r:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 632
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->r:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 635
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->s:Lcom/sec/chaton/userprofile/y;

    if-eqz v0, :cond_2

    .line 636
    iget-object v0, p0, Lcom/sec/chaton/userprofile/BirthdayFragment;->s:Lcom/sec/chaton/userprofile/y;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/y;->dismiss()V

    .line 638
    :cond_2
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 639
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 186
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/BirthdayFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 188
    const/4 v0, 0x1

    .line 190
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 602
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 603
    const-string v0, "onResume"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    return-void
.end method
