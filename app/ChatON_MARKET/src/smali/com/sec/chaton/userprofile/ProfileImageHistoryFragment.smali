.class public Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;
.super Landroid/support/v4/app/Fragment;
.source "ProfileImageHistoryFragment.java"


# static fields
.field public static final d:Ljava/lang/String;

.field private static s:Ljava/lang/String;

.field private static t:Ljava/lang/String;


# instance fields
.field private A:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private B:Landroid/widget/GridView;

.field private C:Landroid/widget/FrameLayout;

.field private D:Landroid/widget/LinearLayout;

.field private E:Landroid/widget/FrameLayout;

.field private F:Landroid/widget/FrameLayout;

.field private G:Landroid/widget/LinearLayout;

.field private H:Lcom/sec/chaton/userprofile/bp;

.field private I:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;",
            ">;"
        }
    .end annotation
.end field

.field private J:Lcom/sec/common/f/c;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Landroid/widget/ProgressBar;

.field private N:I

.field private O:Landroid/widget/ImageView;

.field private P:Ljava/lang/Boolean;

.field private Q:Ljava/lang/Boolean;

.field private R:Landroid/widget/ImageView;

.field private S:Landroid/content/Context;

.field private T:Landroid/view/ViewStub;

.field private U:Landroid/view/View;

.field private V:Landroid/widget/ImageView;

.field private W:Landroid/widget/TextView;

.field private X:Landroid/widget/TextView;

.field private Y:Lcom/sec/common/a/d;

.field private Z:Lcom/sec/common/a/d;

.field a:Ljava/lang/String;

.field private aa:Lcom/sec/common/a/d;

.field private ab:Lcom/sec/common/a/d;

.field private ac:Lcom/sec/common/a/d;

.field private ad:Ljava/lang/String;

.field private ae:Ljava/lang/String;

.field private af:I

.field private ag:I

.field private ah:I

.field private ai:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/ProfileImage;",
            ">;"
        }
    .end annotation
.end field

.field private aj:Ljava/lang/String;

.field private ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

.field private al:Lcom/sec/common/a/d;

.field private am:I

.field private an:Z

.field private ao:Z

.field private ap:Ljava/lang/String;

.field private aq:Ljava/lang/String;

.field private ar:Ljava/lang/String;

.field private as:Z

.field private at:Landroid/view/Menu;

.field private au:Landroid/content/DialogInterface$OnClickListener;

.field private av:Landroid/content/DialogInterface$OnClickListener;

.field private aw:Landroid/content/DialogInterface$OnClickListener;

.field private ax:Landroid/os/Handler;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field e:Landroid/widget/AdapterView$OnItemClickListener;

.field f:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

.field private h:Landroid/net/Uri;

.field private final i:I

.field private final j:I

.field private final k:I

.field private l:Z

.field private m:Z

.field private n:Landroid/app/ProgressDialog;

.field private o:Landroid/app/ProgressDialog;

.field private p:Lcom/sec/chaton/d/w;

.field private q:Ljava/lang/String;

.field private r:Ljava/io/File;

.field private u:Ljava/io/File;

.field private v:Ljava/io/File;

.field private w:Ljava/lang/String;

.field private x:Z

.field private y:Z

.field private z:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    const-string v0, "profile_f_mine_"

    sput-object v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->s:Ljava/lang/String;

    .line 125
    const-string v0, "profile_t_mine_"

    sput-object v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->t:Ljava/lang/String;

    .line 140
    const-class v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 201
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 91
    iput v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i:I

    .line 94
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->j:I

    .line 95
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->k:I

    .line 115
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l:Z

    .line 116
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m:Z

    .line 122
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->q:Ljava/lang/String;

    .line 123
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/profile/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->r:Ljava/io/File;

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    .line 132
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u:Ljava/io/File;

    .line 134
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->w:Ljava/lang/String;

    .line 136
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->x:Z

    .line 137
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y:Z

    .line 177
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ad:Ljava/lang/String;

    .line 178
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    .line 179
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    .line 183
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aj:Ljava/lang/String;

    .line 191
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    .line 192
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    .line 193
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aq:Ljava/lang/String;

    .line 194
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ar:Ljava/lang/String;

    .line 195
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->as:Z

    .line 599
    new-instance v0, Lcom/sec/chaton/userprofile/cb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/cb;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e:Landroid/widget/AdapterView$OnItemClickListener;

    .line 988
    new-instance v0, Lcom/sec/chaton/userprofile/ce;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/ce;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->au:Landroid/content/DialogInterface$OnClickListener;

    .line 996
    new-instance v0, Lcom/sec/chaton/userprofile/cf;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/cf;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->f:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 1064
    new-instance v0, Lcom/sec/chaton/userprofile/bs;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bs;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->av:Landroid/content/DialogInterface$OnClickListener;

    .line 1094
    new-instance v0, Lcom/sec/chaton/userprofile/bt;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bt;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aw:Landroid/content/DialogInterface$OnClickListener;

    .line 1136
    new-instance v0, Lcom/sec/chaton/userprofile/bu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bu;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ax:Landroid/os/Handler;

    .line 202
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    .line 203
    return-void
.end method

.method public constructor <init>(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 206
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 91
    iput v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i:I

    .line 94
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->j:I

    .line 95
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->k:I

    .line 115
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l:Z

    .line 116
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m:Z

    .line 122
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->q:Ljava/lang/String;

    .line 123
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/profile/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->r:Ljava/io/File;

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    .line 132
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u:Ljava/io/File;

    .line 134
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->w:Ljava/lang/String;

    .line 136
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->x:Z

    .line 137
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y:Z

    .line 177
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ad:Ljava/lang/String;

    .line 178
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    .line 179
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    .line 183
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aj:Ljava/lang/String;

    .line 191
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    .line 192
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    .line 193
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aq:Ljava/lang/String;

    .line 194
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ar:Ljava/lang/String;

    .line 195
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->as:Z

    .line 599
    new-instance v0, Lcom/sec/chaton/userprofile/cb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/cb;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e:Landroid/widget/AdapterView$OnItemClickListener;

    .line 988
    new-instance v0, Lcom/sec/chaton/userprofile/ce;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/ce;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->au:Landroid/content/DialogInterface$OnClickListener;

    .line 996
    new-instance v0, Lcom/sec/chaton/userprofile/cf;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/cf;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->f:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 1064
    new-instance v0, Lcom/sec/chaton/userprofile/bs;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bs;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->av:Landroid/content/DialogInterface$OnClickListener;

    .line 1094
    new-instance v0, Lcom/sec/chaton/userprofile/bt;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bt;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aw:Landroid/content/DialogInterface$OnClickListener;

    .line 1136
    new-instance v0, Lcom/sec/chaton/userprofile/bu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bu;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ax:Landroid/os/Handler;

    .line 207
    iput p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N:I

    .line 208
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 211
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 91
    iput v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i:I

    .line 94
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->j:I

    .line 95
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->k:I

    .line 115
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l:Z

    .line 116
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m:Z

    .line 122
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->q:Ljava/lang/String;

    .line 123
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/profile/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->r:Ljava/io/File;

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    .line 132
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u:Ljava/io/File;

    .line 134
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->w:Ljava/lang/String;

    .line 136
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->x:Z

    .line 137
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y:Z

    .line 177
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ad:Ljava/lang/String;

    .line 178
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    .line 179
    iput v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    .line 183
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aj:Ljava/lang/String;

    .line 191
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    .line 192
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    .line 193
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aq:Ljava/lang/String;

    .line 194
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ar:Ljava/lang/String;

    .line 195
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->as:Z

    .line 599
    new-instance v0, Lcom/sec/chaton/userprofile/cb;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/cb;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e:Landroid/widget/AdapterView$OnItemClickListener;

    .line 988
    new-instance v0, Lcom/sec/chaton/userprofile/ce;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/ce;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->au:Landroid/content/DialogInterface$OnClickListener;

    .line 996
    new-instance v0, Lcom/sec/chaton/userprofile/cf;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/cf;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->f:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 1064
    new-instance v0, Lcom/sec/chaton/userprofile/bs;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bs;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->av:Landroid/content/DialogInterface$OnClickListener;

    .line 1094
    new-instance v0, Lcom/sec/chaton/userprofile/bt;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bt;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aw:Landroid/content/DialogInterface$OnClickListener;

    .line 1136
    new-instance v0, Lcom/sec/chaton/userprofile/bu;

    invoke-direct {v0, p0}, Lcom/sec/chaton/userprofile/bu;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ax:Landroid/os/Handler;

    .line 212
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    .line 213
    iput-object p2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aq:Ljava/lang/String;

    .line 214
    iput-object p3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ar:Ljava/lang/String;

    .line 215
    iput-boolean v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    .line 216
    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic B(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m:Z

    return v0
.end method

.method static synthetic C(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic D(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic E(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->as:Z

    return v0
.end method

.method static synthetic F(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->K:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic G(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic H(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic I(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic J(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ar:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic K(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->k()V

    return-void
.end method

.method static synthetic L(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic M(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->M:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->at:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aa:Lcom/sec/common/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Q:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 326
    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->am:I

    const v1, 0x7f0d0009

    if-ne v0, v1, :cond_0

    .line 327
    packed-switch p1, :pswitch_data_0

    .line 362
    :goto_0
    return-void

    .line 329
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g()V

    goto :goto_0

    .line 333
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h()V

    goto :goto_0

    .line 336
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e()V

    goto :goto_0

    .line 340
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->f()V

    goto :goto_0

    .line 344
    :cond_0
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 346
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g()V

    goto :goto_0

    .line 350
    :pswitch_5
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h()V

    goto :goto_0

    .line 353
    :pswitch_6
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d()V

    goto :goto_0

    .line 357
    :pswitch_7
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e()V

    goto :goto_0

    .line 327
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 344
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1441
    packed-switch p1, :pswitch_data_0

    .line 1456
    :goto_0
    return-void

    .line 1443
    :pswitch_0
    const-string v0, "profile_small_image1"

    invoke-static {v0, p2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1447
    :pswitch_1
    const-string v0, "profile_small_image2"

    invoke-static {v0, p2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1451
    :pswitch_2
    const-string v0, "profile_small_image3"

    invoke-static {v0, p2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1441
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(ILjava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/ProfileImage;",
            ">;)V"
        }
    .end annotation

    .prologue
    const v3, 0x7f0b017f

    const/16 v4, 0x8

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1460
    const/4 v0, 0x4

    if-ge p1, v0, :cond_4

    if-lez p1, :cond_4

    .line 1461
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mImageNum: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1463
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v0, p1, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1464
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    .line 1465
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    .line 1468
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 1470
    invoke-virtual {p0, v7}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->setHasOptionsMenu(Z)V

    .line 1471
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1473
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    .line 1475
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->P:Ljava/lang/Boolean;

    .line 1476
    iput v8, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N:I

    .line 1511
    :cond_0
    :goto_0
    return-void

    .line 1478
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v7, :cond_2

    .line 1479
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    .line 1480
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    .line 1481
    add-int/lit8 v0, p1, 0x1

    .line 1483
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 1484
    invoke-virtual {p0, v7}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->setHasOptionsMenu(Z)V

    .line 1485
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1488
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    .line 1489
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->P:Ljava/lang/Boolean;

    .line 1490
    iput v8, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N:I

    goto :goto_0

    .line 1492
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1493
    invoke-virtual {p0, v8}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->setHasOptionsMenu(Z)V

    .line 1494
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1497
    :cond_3
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1499
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 1505
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v7, :cond_0

    .line 1507
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    goto/16 :goto_0
.end method

.method private a(Lcom/sec/chaton/a/a/f;Z)V
    .locals 13

    .prologue
    const v12, 0x7f0b017f

    const/4 v11, 0x0

    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 2108
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_11

    .line 2111
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetProfileImageList;

    .line 2112
    if-nez v0, :cond_0

    .line 2284
    :goto_0
    return-void

    .line 2116
    :cond_0
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->listcount:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    .line 2117
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->maxcount:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ah:I

    .line 2118
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->profileimagelist:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    .line 2119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mTotalProfileImageCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mMaxImageCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ah:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2123
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 2124
    const-string v1, "1"

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2125
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2126
    const-string v0, "profile_image_status"

    const-string v1, "updated"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2127
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(I)V

    .line 2141
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    move v2, v3

    .line 2142
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 2143
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    .line 2144
    const-string v1, "1"

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2146
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    .line 2147
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    .line 2148
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ad:Ljava/lang/String;

    .line 2149
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    sput-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->a:Ljava/lang/String;

    .line 2151
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->P:Ljava/lang/Boolean;

    .line 2152
    add-int/lit8 v1, v2, 0x1

    .line 2153
    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-virtual {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v12}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ")"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 2156
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2157
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2158
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 2162
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v4, "profile_image_update_client"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2164
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "profile_t_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "mine_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2166
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "profile_f_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "mine_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2169
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "myprofile.png_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2170
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aj:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".jpeg_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2172
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2173
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2174
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 2175
    invoke-static {v6, v1}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2178
    :cond_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2179
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2180
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2181
    invoke-static {v5, v4}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2186
    :cond_3
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "myprofile.png_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2187
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "profile_small_image0"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2189
    :cond_4
    const-string v1, "profile_small_image0"

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2192
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->K:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1, v4, v9}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2209
    :cond_5
    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "profileimageurl: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2212
    new-instance v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;

    invoke-direct {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;-><init>()V

    .line 2213
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->K:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    .line 2214
    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    iput-object v4, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    .line 2215
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "profile_t_mine_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->ThumbfileName:Ljava/lang/String;

    .line 2216
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "profile_f_mine_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    .line 2217
    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    iput-object v4, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    .line 2218
    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    .line 2220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "profileimageurl : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2222
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2142
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_2

    .line 2130
    :cond_6
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->j()V

    .line 2131
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(I)V

    goto/16 :goto_1

    .line 2134
    :cond_7
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->j()V

    .line 2135
    const-string v0, "profile_small_image1"

    invoke-static {v0, v11}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2136
    const-string v0, "profile_small_image2"

    invoke-static {v0, v11}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2137
    const-string v0, "profile_small_image3"

    invoke-static {v0, v11}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2198
    :cond_8
    const-string v4, "1"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2199
    add-int/lit8 v1, v2, -0x1

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-direct {p0, v1, v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 2202
    :cond_9
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-direct {p0, v2, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(ILjava/lang/String;)V

    .line 2203
    const-string v1, "profile_small_image0"

    invoke-static {v1, v11}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2226
    :cond_a
    if-eqz p2, :cond_b

    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    if-le v0, v9, :cond_b

    .line 2227
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_f

    .line 2228
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    .line 2229
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    .line 2230
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v12}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 2231
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->K:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2232
    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    .line 2240
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2244
    :cond_b
    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N:I

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(ILjava/util/ArrayList;)V

    .line 2246
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_10

    .line 2248
    invoke-virtual {p0, v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->setHasOptionsMenu(Z)V

    .line 2253
    :goto_5
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->k()V

    .line 2254
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    invoke-virtual {v0, v9}, Landroid/widget/GridView;->setEnabled(Z)V

    .line 2255
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->M:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2257
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2258
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2260
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2261
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2264
    :cond_d
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m:Z

    if-ne v0, v9, :cond_e

    .line 2265
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a()V

    .line 2266
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m:Z

    .line 2268
    :cond_e
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 2234
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    .line 2235
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    .line 2236
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v12}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 2237
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->K:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2238
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ai:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    goto/16 :goto_4

    .line 2251
    :cond_10
    invoke-virtual {p0, v9}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->setHasOptionsMenu(Z)V

    goto/16 :goto_5

    .line 2271
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->M:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2272
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2273
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2275
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2276
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2278
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->C:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2279
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u()V

    .line 2280
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2281
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Lcom/sec/chaton/a/a/f;Z)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/a/a/f;Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;ZLcom/sec/chaton/a/a/f;Z)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(ZLcom/sec/chaton/a/a/f;Z)V

    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 704
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 705
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 707
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 708
    new-instance v0, Lcom/sec/chaton/userprofile/ck;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/chaton/userprofile/ck;-><init>(Ljava/io/File;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 709
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 711
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 770
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 771
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;

    .line 773
    if-nez v0, :cond_1

    .line 784
    :cond_0
    return-void

    .line 776
    :cond_1
    iget-object v3, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    const-string v4, "addImage"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 777
    :cond_2
    iget-object v0, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->selectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 770
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 778
    :cond_3
    iget-object v3, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 779
    iget-object v0, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->selectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 781
    :cond_4
    iget-object v0, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->selectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/Boolean;)V
    .locals 10

    .prologue
    .line 2292
    invoke-virtual/range {p6 .. p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&size=800"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2294
    sget-object v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->s:Ljava/lang/String;

    .line 2295
    const/16 v4, 0x320

    move-object v1, v0

    .line 2302
    :goto_0
    if-eqz p2, :cond_0

    .line 2303
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2304
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2305
    new-instance v0, Lcom/sec/chaton/userprofile/bq;

    iget-object v6, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ax:Landroid/os/Handler;

    move-object v1, p4

    move-object v2, p5

    move v5, v4

    invoke-direct/range {v0 .. v9}, Lcom/sec/chaton/userprofile/bq;-><init>(Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLandroid/os/Handler;)V

    .line 2306
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    invoke-virtual {v1, p3, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 2312
    :cond_0
    :goto_1
    return-void

    .line 2297
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&size=140"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2298
    sget-object v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->t:Ljava/lang/String;

    .line 2299
    const/16 v4, 0x8c

    move-object v1, v0

    goto :goto_0

    .line 2308
    :cond_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2309
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 12

    .prologue
    .line 671
    new-instance v10, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profile_t_mine_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v10, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    new-instance v11, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profile_f_mine_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v11, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 679
    invoke-direct {p0, v11}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/io/File;)V

    .line 680
    if-eqz p3, :cond_0

    .line 681
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Ljava/lang/String;)V

    .line 695
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 697
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "photoFile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "photoFileFull="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    return-void

    .line 684
    :cond_1
    invoke-direct {p0, v10}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/io/File;)V

    .line 687
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 688
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 689
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setThumbOrFullProfileImage mUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&size=800"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    new-instance v0, Lcom/sec/chaton/userprofile/bq;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&size=800"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x258

    const/16 v5, 0x258

    iget-object v6, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "profile_f_mine_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v9, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ax:Landroid/os/Handler;

    move v8, p3

    invoke-direct/range {v0 .. v9}, Lcom/sec/chaton/userprofile/bq;-><init>(Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLandroid/os/Handler;)V

    .line 693
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0
.end method

.method private a(ZLcom/sec/chaton/a/a/f;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 791
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    if-nez v0, :cond_1

    .line 793
    new-instance v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;

    invoke-direct {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;-><init>()V

    .line 794
    const-string v1, "addImage"

    iput-object v1, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    .line 796
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 799
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->k()V

    .line 800
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setEnabled(Z)V

    .line 802
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->P:Ljava/lang/Boolean;

    .line 803
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 804
    invoke-direct {p0, p2, p3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/a/a/f;Z)V

    .line 815
    :goto_0
    return-void

    .line 806
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->d()V

    .line 807
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->q()V

    goto :goto_0

    .line 811
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p:Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    const-string v2, "600"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/w;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->r()V

    goto :goto_0
.end method

.method private a(ILandroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1736
    .line 1738
    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 1753
    :goto_0
    return v0

    .line 1740
    :pswitch_0
    if-eqz p2, :cond_0

    const-string v2, "restart"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1741
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m()V

    goto :goto_0

    .line 1746
    :pswitch_1
    if-eqz p2, :cond_0

    const-string v2, "restart"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1747
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n()V

    goto :goto_0

    .line 1738
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Z)Z
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->al:Lcom/sec/common/a/d;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    return-object p1
.end method

.method private b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1514
    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    .line 1515
    const-string v0, "profile_small_image3"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1517
    :cond_0
    const/4 v0, 0x3

    if-ge p1, v0, :cond_1

    .line 1518
    const-string v0, "profile_small_image2"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1520
    :cond_1
    const/4 v0, 0x2

    if-ge p1, v0, :cond_2

    .line 1521
    const-string v0, "profile_small_image1"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1523
    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;I)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(I)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1540
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profilehistory/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1541
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1542
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1543
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "msisdn"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 1545
    const-string v3, "profile_image_status"

    const-string v4, "updated"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1549
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "profile_t_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mine_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1551
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "profile_f_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "mine_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1555
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "myprofile.png_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1556
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1557
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 1560
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "myprofile.png_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1561
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ".jpeg_"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1564
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 1566
    invoke-static {v3, v1}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1567
    invoke-static {v0, v4}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1569
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Z)Z
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->an:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->am:I

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/d/w;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p:Lcom/sec/chaton/d/w;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ac:Lcom/sec/common/a/d;

    return-object p1
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2327
    const-string v0, ""

    .line 2329
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->c()Lcom/sec/chaton/util/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/util/q;->a()Ljava/lang/String;

    move-result-object v0

    .line 2331
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2332
    new-instance v1, Lcom/sec/chaton/util/a;

    invoke-static {v0}, Lcom/sec/chaton/util/a;->b(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v0}, Lcom/sec/chaton/util/a;->c(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/util/a;-><init>([B[B)V

    .line 2336
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/a;->b([B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/a;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2341
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 2342
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "paramBefore: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, ", paramAfter: "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2344
    :cond_0
    return-object v0

    .line 2334
    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Fail in getting a key"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2337
    :catch_0
    move-exception v0

    .line 2338
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Encryption Error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c(I)V
    .locals 5

    .prologue
    const/16 v4, 0x258

    const/4 v3, 0x1

    .line 1780
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1781
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1782
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1783
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1784
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1785
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1786
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1787
    invoke-virtual {p0, v0, p1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1788
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ad:Ljava/lang/String;

    return-object p1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 366
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l()V

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p:Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->e(Ljava/lang/String;)V

    .line 368
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 369
    return-void
.end method

.method private d(I)V
    .locals 5

    .prologue
    const/16 v4, 0x258

    const/4 v3, 0x1

    .line 1791
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1792
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1793
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1794
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1795
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1796
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1797
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1798
    invoke-virtual {p0, v0, p1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1799
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    const v5, 0x7f0b0039

    const v4, 0x7f0b0037

    const v2, 0x7f0b000f

    const/4 v3, 0x1

    .line 374
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ad:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Y:Lcom/sec/common/a/d;

    if-nez v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 378
    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b027b

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/userprofile/bx;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/bx;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    invoke-virtual {v1, v4, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/userprofile/br;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/br;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    invoke-virtual {v1, v5, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 392
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Y:Lcom/sec/common/a/d;

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Y:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 418
    :goto_0
    return-void

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Z:Lcom/sec/common/a/d;

    if-nez v0, :cond_2

    .line 398
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 399
    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b03f6

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/userprofile/bz;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/bz;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    invoke-virtual {v1, v4, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/userprofile/by;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/by;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    invoke-virtual {v1, v5, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 413
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Z:Lcom/sec/common/a/d;

    .line 415
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Z:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private e(I)V
    .locals 8

    .prologue
    const/16 v7, 0x11

    const/4 v6, 0x1

    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 2069
    const/4 v1, 0x0

    .line 2070
    const/4 v0, 0x0

    .line 2072
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2073
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 2074
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2075
    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2077
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2078
    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2082
    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->D:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2099
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->E:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2100
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->F:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2102
    :cond_1
    return-void

    .line 2085
    :cond_2
    if-ne p1, v6, :cond_0

    .line 2086
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2087
    const v0, 0x3f333333    # 0.7f

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2088
    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2090
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2091
    const v2, 0x3e99999a    # 0.3f

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2092
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2094
    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->D:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    return v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 422
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l()V

    .line 425
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Q:Ljava/lang/Boolean;

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p:Lcom/sec/chaton/d/w;

    const-string v1, "-1"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->e(Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 428
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ah:I

    return v0
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 715
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 727
    :goto_0
    return-void

    .line 719
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "profile_f_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mine_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 721
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 722
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 725
    :cond_1
    new-instance v1, Lcom/sec/chaton/multimedia/a/a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "//"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ChatON"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "profile_f_mine_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/sec/chaton/multimedia/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 726
    new-array v0, v5, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aa:Lcom/sec/common/a/d;

    return-object v0
.end method

.method private h()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 733
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 766
    :cond_0
    :goto_0
    return-void

    .line 739
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "profile_f_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mine_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 745
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 749
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/16 v2, 0x258

    const/16 v4, 0x258

    invoke-static {v0, v1, v2, v4}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 750
    const/16 v0, 0xa0

    invoke-virtual {v2, v0}, Landroid/graphics/Bitmap;->setDensity(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 759
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    const v1, 0x7f0b0166

    invoke-virtual {p0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 761
    if-eqz v0, :cond_0

    .line 762
    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 751
    :catch_0
    move-exception v0

    .line 752
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 753
    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    return-object v0
.end method

.method private i()V
    .locals 4

    .prologue
    const v3, 0x7f0b0414

    const/4 v2, 0x0

    .line 1109
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1110
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1112
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1118
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1131
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l:Z

    .line 1132
    return-void

    .line 1122
    :cond_1
    const/4 v1, 0x5

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1123
    :catch_0
    move-exception v0

    .line 1124
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1125
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 1126
    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic j(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1527
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/bt;->b(Ljava/lang/String;)V

    .line 1528
    const-string v0, "profile_image_status"

    const-string v1, "deleted"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1529
    const-string v0, "profile_small_image0"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1530
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1532
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "myprofile.png_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1533
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1534
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1536
    :cond_0
    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    .line 1576
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->a:Ljava/lang/String;

    .line 1577
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->H:Lcom/sec/chaton/userprofile/bp;

    if-nez v0, :cond_1

    .line 1578
    new-instance v0, Lcom/sec/chaton/userprofile/bp;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03004e

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/userprofile/bp;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->H:Lcom/sec/chaton/userprofile/bp;

    .line 1579
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->H:Lcom/sec/chaton/userprofile/bp;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1585
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1586
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    if-ge v0, v1, :cond_0

    .line 1587
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->H:Lcom/sec/chaton/userprofile/bp;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/userprofile/bp;->a(I)Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;

    move-result-object v1

    .line 1588
    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    iget-object v1, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1589
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    .line 1591
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b017f

    invoke-virtual {p0, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 1597
    :cond_0
    return-void

    .line 1581
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->H:Lcom/sec/chaton/userprofile/bp;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/bp;->notifyDataSetChanged()V

    goto :goto_0

    .line 1586
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic k(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->an:Z

    return v0
.end method

.method static synthetic l(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    return v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1600
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/i;->a(Landroid/app/Activity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 1601
    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    return-object v0
.end method

.method private m()V
    .locals 0

    .prologue
    .line 1757
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i()V

    .line 1759
    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    return-object v0
.end method

.method private n()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1762
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1763
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1764
    const-string v1, "output"

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1765
    const-string v1, "outputFormat"

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1768
    const/4 v1, 0x6

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1776
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l:Z

    .line 1777
    return-void

    .line 1769
    :catch_0
    move-exception v0

    .line 1770
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1771
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 1772
    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic o(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ad:Ljava/lang/String;

    return-object v0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 1810
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1811
    const-string v0, "Me"

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201bb

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/ag;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1815
    :goto_0
    return-void

    .line 1812
    :catch_0
    move-exception v0

    .line 1813
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic p(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->am:I

    return v0
.end method

.method private p()V
    .locals 3

    .prologue
    .line 1820
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    const v1, 0x7f0201bb

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setImageResource(I)V

    .line 1821
    const-string v0, "Me"

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201bb

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/ag;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1825
    :goto_0
    return-void

    .line 1822
    :catch_0
    move-exception v0

    .line 1823
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic q(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->au:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method private q()V
    .locals 7

    .prologue
    const/16 v1, 0x8

    const/4 v6, 0x1

    .line 1830
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1831
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1833
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UserProfile initialize() - img status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "profile_image_status"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1836
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_image_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "updated"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N:I

    if-eqz v0, :cond_7

    .line 1841
    :cond_0
    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N:I

    if-ne v0, v6, :cond_3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_small_image1"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1843
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile_f_mine_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "profile_small_image1"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1844
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1874
    :cond_1
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "photoFile="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1876
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1877
    new-instance v1, Lcom/sec/chaton/userprofile/ck;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/sec/chaton/userprofile/ck;-><init>(Ljava/io/File;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1878
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1889
    :goto_1
    return-void

    .line 1847
    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile_t_mine_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "profile_small_image1"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1849
    :cond_3
    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_small_image2"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1851
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile_f_mine_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "profile_small_image2"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1852
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1855
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile_t_mine_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "profile_small_image2"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1858
    :cond_4
    iget v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_small_image3"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1860
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile_f_mine_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "profile_small_image3"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1861
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1864
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile_t_mine_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "profile_small_image3"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1871
    :cond_5
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u:Ljava/io/File;

    const-string v2, "myprofile.png_"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1882
    :cond_6
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o()V

    goto/16 :goto_1

    .line 1886
    :cond_7
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o()V

    goto/16 :goto_1
.end method

.method static synthetic r(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->al:Lcom/sec/common/a/d;

    return-object v0
.end method

.method private r()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1894
    .line 1896
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ar:Ljava/lang/String;

    const-string v1, "FULL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1898
    new-instance v1, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "buddy_f_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aq:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1899
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 1903
    :goto_0
    if-ne v0, v2, :cond_1

    .line 1906
    new-instance v0, Lcom/sec/chaton/userprofile/ck;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/userprofile/ck;-><init>(Ljava/io/File;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1907
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1936
    :cond_0
    :goto_1
    return-void

    .line 1909
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p()V

    goto :goto_1

    .line 1912
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ar:Ljava/lang/String;

    const-string v1, "THUMB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1915
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "buddy_f_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aq:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1916
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1917
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "buddy_t_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aq:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1918
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1920
    iput-boolean v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->as:Z

    move v1, v2

    .line 1926
    :goto_2
    if-ne v1, v2, :cond_4

    .line 1928
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1929
    new-instance v1, Lcom/sec/chaton/userprofile/ck;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v0, v3, v2}, Lcom/sec/chaton/userprofile/ck;-><init>(Ljava/io/File;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1930
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_1

    :cond_3
    move v1, v2

    .line 1923
    goto :goto_2

    .line 1933
    :cond_4
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p()V

    goto/16 :goto_1

    :cond_5
    move v1, v3

    goto :goto_2

    :cond_6
    move v0, v3

    goto/16 :goto_0
.end method

.method private s()V
    .locals 8

    .prologue
    .line 1942
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1943
    :cond_0
    const-string v0, "[deleteTempFolder] External Storage Is Not Available or Writable!"

    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1944
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->z:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 1945
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    const v1, 0x7f0b002b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->z:Landroid/widget/Toast;

    .line 1947
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->z:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1968
    :cond_2
    :goto_0
    return-void

    .line 1950
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v1

    .line 1951
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 1952
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->z:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1956
    :cond_4
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/profile/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1957
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 1958
    if-eqz v2, :cond_2

    .line 1959
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 1960
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/profile/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    .line 1961
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Delete File] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/profile/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1959
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1964
    :catch_0
    move-exception v0

    .line 1965
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method static synthetic s(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i()V

    return-void
.end method

.method static synthetic t(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h:Landroid/net/Uri;

    return-object v0
.end method

.method private t()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1981
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 1982
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1983
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->x:Z

    .line 1984
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y:Z

    .line 1992
    :goto_0
    return-void

    .line 1985
    :cond_0
    const-string v1, "mounted_ro"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1986
    iput-boolean v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->x:Z

    .line 1987
    iput-boolean v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y:Z

    goto :goto_0

    .line 1989
    :cond_1
    iput-boolean v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->x:Z

    .line 1990
    iput-boolean v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y:Z

    goto :goto_0
.end method

.method static synthetic u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private u()V
    .locals 3

    .prologue
    .line 2315
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->U:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2316
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->T:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->U:Landroid/view/View;

    .line 2317
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->U:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->V:Landroid/widget/ImageView;

    .line 2318
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->V:Landroid/widget/ImageView;

    const v1, 0x7f02034e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2319
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->U:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->W:Landroid/widget/TextView;

    .line 2320
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->W:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2321
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->U:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->X:Landroid/widget/TextView;

    .line 2322
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->X:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2324
    :cond_0
    return-void
.end method

.method static synthetic v(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ac:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic w(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->v:Ljava/io/File;

    return-object v0
.end method

.method static synthetic x(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o()V

    return-void
.end method

.method static synthetic y(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic z(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Q:Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 884
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->r:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_0

    .line 885
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->r:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 888
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->s()V

    .line 890
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tmp_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpeg_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->w:Ljava/lang/String;

    .line 891
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->r:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->w:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->v:Ljava/io/File;

    .line 892
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v0

    .line 893
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Create File] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->r:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h:Landroid/net/Uri;

    .line 896
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMdd_HHmmss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 897
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "//Camera//"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h:Landroid/net/Uri;

    .line 903
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c()Z

    move-result v0

    if-nez v0, :cond_4

    .line 904
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->z:Landroid/widget/Toast;

    if-nez v0, :cond_2

    .line 905
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    const v1, 0x7f0b002b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->z:Landroid/widget/Toast;

    .line 907
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->z:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 985
    :cond_3
    :goto_0
    return-void

    .line 913
    :cond_4
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "profile_image_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "updated"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 915
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 916
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 920
    const v0, 0x7f0d0006

    .line 958
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 960
    const v2, 0x7f0b0204

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->av:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v3}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 966
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ab:Lcom/sec/common/a/d;

    if-nez v0, :cond_5

    .line 967
    const v0, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/userprofile/cd;

    invoke-direct {v2, p0}, Lcom/sec/chaton/userprofile/cd;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 973
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ab:Lcom/sec/common/a/d;

    .line 975
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ab:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 977
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->l:Z

    if-eqz v0, :cond_3

    .line 978
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ab:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 981
    :catch_0
    move-exception v0

    .line 983
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 937
    :cond_6
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 938
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 942
    const v0, 0x7f0d001d

    goto :goto_1
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 1971
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->t()V

    .line 1972
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->x:Z

    return v0
.end method

.method protected c()Z
    .locals 1

    .prologue
    .line 1976
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->t()V

    .line 1977
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/16 v1, 0x8

    const/4 v2, -0x1

    .line 1611
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1612
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1614
    if-ne p2, v2, :cond_8

    .line 1615
    packed-switch p1, :pswitch_data_0

    .line 1732
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1619
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 1620
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1622
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 1623
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1626
    :cond_3
    if-ne p2, v2, :cond_0

    .line 1628
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "activity_orientation"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    .line 1629
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/i;->a(Landroid/app/Activity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 1631
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "temp_file_path"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1632
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1633
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tmp_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".jpeg_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->w:Ljava/lang/String;

    .line 1634
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->r:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->w:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->v:Ljava/io/File;

    .line 1635
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_4

    .line 1636
    const-string v0, "Crop return null!"

    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1643
    :cond_4
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->v:Ljava/io/File;

    invoke-static {v1, v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 1645
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p:Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->v:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->a(Ljava/lang/String;)V

    .line 1646
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 1647
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1651
    :catch_0
    move-exception v0

    .line 1652
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1653
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o()V

    goto/16 :goto_0

    .line 1659
    :pswitch_2
    if-nez p3, :cond_5

    .line 1660
    const-string v0, "Crop Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 1662
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 1707
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1708
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h:Landroid/net/Uri;

    .line 1709
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c(I)V

    goto/16 :goto_0

    .line 1714
    :pswitch_3
    if-ne p2, v2, :cond_6

    .line 1715
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1716
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d(I)V

    goto/16 :goto_0

    .line 1718
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_7

    .line 1719
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1721
    :cond_7
    const-string v0, "Camera Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1728
    :cond_8
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(ILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 1615
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 222
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 224
    check-cast p1, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    .line 225
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2064
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2065
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e(I)V

    .line 2066
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 236
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 240
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "?uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "param"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->K:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :cond_0
    :goto_0
    iput-boolean v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->an:Z

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    const v1, 0x7f0b017f

    invoke-virtual {p0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 250
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    if-nez v0, :cond_1

    .line 251
    invoke-static {p0, v5}, Lcom/sec/chaton/base/BaseActivity;->b(Landroid/support/v4/app/Fragment;Z)V

    .line 255
    :goto_1
    return-void

    .line 241
    :catch_0
    move-exception v0

    .line 242
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 243
    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 253
    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->b(Landroid/support/v4/app/Fragment;Z)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 289
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 291
    iput-object p1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->at:Landroid/view/Menu;

    .line 292
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/16 v4, 0x258

    const/16 v3, 0x8

    const/4 v8, 0x0

    .line 441
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    if-nez v0, :cond_2

    .line 442
    invoke-static {p0, v2}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 449
    :goto_0
    const v0, 0x7f0300e2

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 450
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    .line 451
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o:Landroid/app/ProgressDialog;

    .line 452
    const v0, 0x7f070322

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    .line 453
    const v0, 0x7f0703f3

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->C:Landroid/widget/FrameLayout;

    .line 454
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->C:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 455
    const v0, 0x7f070124

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    .line 456
    const v0, 0x7f0703f6

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    .line 457
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 458
    const v0, 0x7f0703f7

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    .line 459
    invoke-virtual {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    .line 462
    const v0, 0x7f0703f4

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->T:Landroid/view/ViewStub;

    .line 470
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    .line 471
    const v0, 0x7f0703fa

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    .line 472
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 476
    const v0, 0x7f070321

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->D:Landroid/widget/LinearLayout;

    .line 477
    const v0, 0x7f0703f5

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->E:Landroid/widget/FrameLayout;

    .line 478
    const v0, 0x7f0703f8

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->F:Landroid/widget/FrameLayout;

    .line 479
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->f:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 481
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->S:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 482
    invoke-direct {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e(I)V

    .line 485
    invoke-virtual {p0, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->setHasOptionsMenu(Z)V

    .line 488
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I:Ljava/util/ArrayList;

    .line 489
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ax:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p:Lcom/sec/chaton/d/w;

    .line 490
    const v0, 0x7f0703f9

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->M:Landroid/widget/ProgressBar;

    .line 491
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->a:Ljava/lang/String;

    .line 492
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->P:Ljava/lang/Boolean;

    .line 493
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Q:Ljava/lang/Boolean;

    .line 497
    if-eqz p3, :cond_5

    const-string v0, "array"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, "mProfileImageUrl"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 498
    const-string v0, "mRepresentPImageId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ad:Ljava/lang/String;

    .line 499
    const-string v0, "mCurrentPImageId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    .line 500
    const-string v0, "array"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I:Ljava/util/ArrayList;

    .line 501
    const-string v0, "mProfileImageUrl"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    .line 502
    const-string v0, "mCurrentPosition"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    .line 503
    const-string v0, "mTotalProfileImageCount"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    .line 504
    const-string v0, "mSmallImageNum"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N:I

    .line 505
    const-string v0, "mSetDefaultImageFlag"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Q:Ljava/lang/Boolean;

    .line 506
    const-string v0, "mMaxImageCount"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ah:I

    .line 509
    const-string v0, "mBuddyMode"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    .line 510
    const-string v0, "mBuddyNo"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    .line 511
    const-string v0, "mbuddyImageID"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aq:Ljava/lang/String;

    .line 512
    const-string v0, "mbuddyImageType"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ar:Ljava/lang/String;

    .line 513
    const-string v0, "mNoSuchImage"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->as:Z

    .line 516
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mCurrentPImageId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mProfileImageUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 519
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 526
    :goto_1
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->k()V

    .line 530
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 531
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 532
    iget-boolean v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    if-nez v0, :cond_4

    .line 533
    new-instance v0, Lcom/sec/chaton/userprofile/bq;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "&size=800"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "profile_f_mine_"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move v5, v4

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/userprofile/bq;-><init>(Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    .line 538
    :goto_2
    if-eqz v0, :cond_0

    .line 539
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 550
    :cond_0
    :goto_3
    if-eqz p3, :cond_1

    .line 551
    const-string v0, "CAPTURE_IMAGE_URI"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 553
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 554
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h:Landroid/net/Uri;

    .line 558
    :cond_1
    return-object v9

    .line 444
    :cond_2
    invoke-static {p0, v8}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    goto/16 :goto_0

    .line 521
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->a:Ljava/lang/String;

    .line 522
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b017f

    invoke-virtual {p0, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 535
    :cond_4
    new-instance v0, Lcom/sec/chaton/userprofile/bq;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->O:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->R:Landroid/widget/ImageView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "&size=800"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "buddy_f_"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "_"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move v5, v4

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/userprofile/bq;-><init>(Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 543
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->M:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 545
    invoke-virtual {p0, v8}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->setHasOptionsMenu(Z)V

    .line 546
    const/4 v0, 0x0

    invoke-direct {p0, v8, v0, v8}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(ZLcom/sec/chaton/a/a/f;Z)V

    goto/16 :goto_3
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1996
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    if-eqz v0, :cond_0

    .line 1997
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 2000
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2001
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2003
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2004
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2007
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Y:Lcom/sec/common/a/d;

    if-eqz v0, :cond_3

    .line 2008
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Y:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 2010
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Z:Lcom/sec/common/a/d;

    if-eqz v0, :cond_4

    .line 2011
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Z:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 2013
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aa:Lcom/sec/common/a/d;

    if-eqz v0, :cond_5

    .line 2014
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aa:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 2016
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ab:Lcom/sec/common/a/d;

    if-eqz v0, :cond_6

    .line 2017
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ab:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 2019
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ac:Lcom/sec/common/a/d;

    if-eqz v0, :cond_7

    .line 2020
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ac:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 2023
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    if-eqz v0, :cond_8

    .line 2024
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2025
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2026
    iput-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B:Landroid/widget/GridView;

    .line 2028
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->p:Lcom/sec/chaton/d/w;

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->e()V

    .line 2030
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 2031
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 229
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ak:Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    .line 232
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 296
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 320
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 298
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g()V

    goto :goto_0

    .line 302
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h()V

    goto :goto_0

    .line 306
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d()V

    goto :goto_0

    .line 311
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->e()V

    goto :goto_0

    .line 316
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->f()V

    goto :goto_0

    .line 296
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 590
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 591
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 593
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->A:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 595
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 259
    const-string v0, "onPrepareOptionMenu..."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 261
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ad:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 262
    const v0, 0x7f0b003c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v4, v4, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02030e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 263
    const v0, 0x7f0b0165

    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v5, v5, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020312

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 264
    const v0, 0x7f0b0180

    invoke-interface {p1, v2, v6, v6, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020310

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 265
    const v0, 0x7f0b005b

    invoke-interface {p1, v2, v7, v7, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020305

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 273
    :goto_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile_f_mine_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    .line 276
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 277
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 278
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 279
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 280
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 282
    :cond_0
    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 283
    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 285
    :cond_1
    return-void

    .line 267
    :cond_2
    const v0, 0x7f0b003c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v4, v4, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02030e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 268
    const v0, 0x7f0b0165

    invoke-virtual {p0, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v5, v5, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020312

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 269
    const v0, 0x7f0b005b

    invoke-interface {p1, v2, v7, v6, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020305

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 270
    const/4 v0, 0x5

    const v1, 0x7f0b024f

    invoke-interface {p1, v2, v0, v7, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02030f

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 564
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 567
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    if-eqz v0, :cond_0

    .line 568
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->g:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 569
    new-instance v1, Lcom/sec/chaton/userprofile/ca;

    invoke-direct {v1, p0}, Lcom/sec/chaton/userprofile/ca;-><init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    iput-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->A:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 575
    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->A:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 579
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2035
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2036
    const-string v0, "mCurrentPImageId"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ae:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2037
    const-string v0, "mRepresentPImageId"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ad:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2038
    const-string v0, "mProfileImageUrl"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039
    const-string v0, "array"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2040
    const-string v0, "mCurrentPosition"

    iget v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->af:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2041
    const-string v0, "mTotalProfileImageCount"

    iget v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ag:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2042
    const-string v0, "mSmallImageNum"

    iget v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2043
    const-string v0, "mSetDefaultImageFlag"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->Q:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2044
    const-string v0, "mMaxImageCount"

    iget v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ah:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2047
    const-string v0, "mBuddyMode"

    iget-boolean v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ao:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2048
    const-string v0, "mBuddyNo"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ap:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2049
    const-string v0, "mbuddyImageID"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->aq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2050
    const-string v0, "mbuddyImageType"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->ar:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2051
    const-string v0, "mNoSuchImage"

    iget-boolean v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->as:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2054
    iget-object v0, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2055
    const-string v0, "CAPTURE_IMAGE_URI"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->h:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2057
    :cond_0
    return-void
.end method
