.class Lcom/sec/chaton/userprofile/bu;
.super Landroid/os/Handler;
.source "ProfileImageHistoryFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V
    .locals 0

    .prologue
    .line 1136
    iput-object p1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13

    .prologue
    const v5, 0x7f0b0097

    const v3, 0x7f0b002a

    const/4 v4, 0x0

    const/4 v12, 0x1

    const/4 v8, 0x0

    .line 1139
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1436
    :cond_0
    :goto_0
    return-void

    .line 1143
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1408
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "download_success"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1409
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    const-string v2, "filename_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1411
    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v2

    if-eqz v2, :cond_7

    if-eqz v0, :cond_7

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1412
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v12}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1413
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v12}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1415
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v0

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1416
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v0

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1418
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v0

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1419
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v0

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1421
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v0

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1422
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v0

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1424
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v0

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1425
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->N(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/view/Menu;

    move-result-object v0

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1427
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1430
    :cond_7
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1431
    const-string v0, "Catch the handler for downloading complete."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1145
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/m;

    .line 1146
    iget-boolean v0, v0, Lcom/sec/chaton/a/a/m;->a:Z

    if-eqz v0, :cond_8

    .line 1148
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/d/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/w;->c()V

    goto/16 :goto_0

    .line 1151
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1152
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1157
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->v(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/a/d;

    move-result-object v0

    if-nez v0, :cond_a

    .line 1158
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 1159
    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v12}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0024

    new-instance v3, Lcom/sec/chaton/userprofile/bw;

    invoke-direct {v3, p0}, Lcom/sec/chaton/userprofile/bw;-><init>(Lcom/sec/chaton/userprofile/bu;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b00dc

    new-instance v3, Lcom/sec/chaton/userprofile/bv;

    invoke-direct {v3, p0}, Lcom/sec/chaton/userprofile/bv;-><init>(Lcom/sec/chaton/userprofile/bu;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 1181
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 1183
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->v(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto/16 :goto_0

    .line 1188
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1189
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_b

    .line 1190
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1191
    const-string v1, "profile_image_status"

    const-string v2, "updated"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1192
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v12, v0, v8}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;ZLcom/sec/chaton/a/a/f;Z)V

    goto/16 :goto_0

    .line 1194
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1195
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1197
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1198
    const-string v0, "UserProfileImageViewFragment: METHOD_PROFILE_IMAGE_HISTORY_ADD failed"

    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1204
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1205
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_e

    .line 1206
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->z(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v12, :cond_d

    .line 1214
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    const v3, 0x7f0b017f

    invoke-virtual {v2, v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 1215
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1216
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1218
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->A(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1219
    const-string v1, "profile_small_image0"

    invoke-static {v1, v4}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1222
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v12, v0, v8}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;ZLcom/sec/chaton/a/a/f;Z)V

    .line 1223
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00e6

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1224
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1226
    :cond_d
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1227
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v12, v0, v8}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;ZLcom/sec/chaton/a/a/f;Z)V

    .line 1228
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00e6

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1233
    :cond_e
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const v2, 0xb34f

    if-ne v1, v2, :cond_f

    .line 1234
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1237
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1238
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v12, v0, v8}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;ZLcom/sec/chaton/a/a/f;Z)V

    goto/16 :goto_0

    .line 1240
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1241
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1243
    :cond_10
    const-string v0, "UserProfileImageViewFragment: METHOD_PROFILE_IMAGE_HISTORY_SET failed"

    sget-object v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1253
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1254
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_12

    .line 1255
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1257
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->B(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z

    move-result v1

    if-ne v1, v12, :cond_11

    .line 1258
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1259
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v12, v0, v8}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;ZLcom/sec/chaton/a/a/f;Z)V

    .line 1268
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0167

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1263
    :cond_11
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->m(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    const v3, 0x7f0b017f

    invoke-virtual {v2, v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 1264
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1265
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v12, v0, v12}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;ZLcom/sec/chaton/a/a/f;Z)V

    goto :goto_1

    .line 1270
    :cond_12
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0xb34f

    if-ne v0, v1, :cond_15

    .line 1271
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1272
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1274
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1275
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1277
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1280
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1281
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0, v8, v4, v8}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;ZLcom/sec/chaton/a/a/f;Z)V

    goto/16 :goto_0

    .line 1284
    :cond_15
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1285
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1287
    :cond_16
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1288
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1290
    :cond_17
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0098

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1296
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1297
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1, v0, v8}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Lcom/sec/chaton/a/a/f;Z)V

    goto/16 :goto_0

    .line 1303
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1304
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_1e

    .line 1306
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetProfileImageList;

    .line 1307
    if-eqz v0, :cond_0

    .line 1311
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v2, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->listcount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;I)I

    .line 1312
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetProfileImageList;->profileimagelist:Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1313
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->C(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->C(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1d

    move v10, v8

    .line 1314
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->C(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v10, v0, :cond_1d

    .line 1315
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->C(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;

    .line 1317
    iget-object v0, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->D(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1318
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v1, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->a(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1319
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->E(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1320
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v1, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1321
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_18

    .line 1322
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 1325
    :cond_18
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "main profileimageurl : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->F(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1326
    new-instance v0, Lcom/sec/chaton/userprofile/bq;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->G(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->H(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/ImageView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->F(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x258

    const/16 v5, 0x258

    iget-object v6, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v6, v6, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "buddy_f_"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v11, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v11}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "_"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v11, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/userprofile/bq;-><init>(Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    .line 1328
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->n(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/common/f/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->j(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 1332
    :cond_19
    const-string v0, "1"

    iget-object v1, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1333
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v1, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->d(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1334
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v1, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1336
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v1, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1337
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1a

    .line 1338
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 1341
    :cond_1a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v1, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_t_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1342
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v2, v2, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "buddy_f_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1346
    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->J(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "FULL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1347
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v3, v3, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "buddy_f_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v3}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->D(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1348
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v4, v4, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "buddy_f_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v4}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->D(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1350
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1351
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 1352
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1353
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1b

    .line 1354
    invoke-static {v3, v0}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1358
    :cond_1b
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1359
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1360
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1361
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 1362
    invoke-static {v2, v1}, Lcom/sec/chaton/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1368
    :cond_1c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "profileimageurl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1370
    new-instance v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;

    invoke-direct {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;-><init>()V

    .line 1371
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageurl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->F(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    .line 1372
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    iget-object v1, v1, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->dirCachePath:Ljava/lang/String;

    .line 1373
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "buddy_t_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->ThumbfileName:Ljava/lang/String;

    .line 1374
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "buddy_f_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->I(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->FullfileName:Ljava/lang/String;

    .line 1375
    iget-object v1, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->represent:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->Represent:Ljava/lang/String;

    .line 1376
    iget-object v1, v9, Lcom/sec/chaton/io/entry/inner/ProfileImage;->profileimageid:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->imageId:Ljava/lang/String;

    .line 1378
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profileimageurl : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment$ProfileImageItem;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1380
    iget-object v1, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->y(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1314
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto/16 :goto_2

    .line 1384
    :cond_1d
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->K(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)V

    .line 1385
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->L(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/GridView;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/widget/GridView;->setEnabled(Z)V

    .line 1386
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->M(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1388
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-virtual {v0, v12}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->setHasOptionsMenu(Z)V

    .line 1390
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1391
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 1394
    :cond_1e
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->M(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1395
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1396
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->u(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1398
    :cond_1f
    iget-object v0, p0, Lcom/sec/chaton/userprofile/bu;->a:Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;->i(Lcom/sec/chaton/userprofile/ProfileImageHistoryFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1143
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x192 -> :sswitch_1
        0x194 -> :sswitch_2
        0x195 -> :sswitch_4
        0x196 -> :sswitch_3
        0x197 -> :sswitch_5
        0x19a -> :sswitch_6
    .end sparse-switch
.end method
