.class Lcom/sec/chaton/userprofile/p;
.super Ljava/lang/Object;
.source "BirthdayImpl.java"

# interfaces
.implements Lcom/sec/chaton/userprofile/ab;


# instance fields
.field final synthetic a:Lcom/sec/chaton/userprofile/j;


# direct methods
.method constructor <init>(Lcom/sec/chaton/userprofile/j;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 379
    const-string v0, "FULL"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_year_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    const-string v1, "SHORT"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    const-string v0, "FULL_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 384
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_year_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    const-string v1, "SHORT_HIDE"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 387
    :cond_2
    const-string v0, "SHORT"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 388
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_year_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 389
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    const-string v1, "FULL"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 391
    :cond_3
    const-string v0, "SHORT_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday_year_show"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 393
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    const-string v1, "FULL_HIDE"

    invoke-static {v0, v1}, Lcom/sec/chaton/userprofile/j;->a(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public a(III)V
    .locals 5

    .prologue
    .line 347
    const-string v0, ""

    .line 348
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->j(Lcom/sec/chaton/userprofile/j;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->j(Lcom/sec/chaton/userprofile/j;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->j(Lcom/sec/chaton/userprofile/j;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDateSet::year"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " onDateSet::month="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " onDateSet::dayOfMonth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-direct {p0}, Lcom/sec/chaton/userprofile/p;->a()V

    .line 355
    const-string v0, "FULL"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "FULL_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 356
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-DD"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->j(Lcom/sec/chaton/userprofile/j;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 357
    const/16 v0, 0xa

    if-ge p3, v0, :cond_2

    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 362
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x7

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 363
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "temp_full ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/userprofile/j;->b(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    .line 365
    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/j;->c(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    .line 366
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDateSet::mBirthDay"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->d(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " onDateSet::displayTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->k(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->l(Lcom/sec/chaton/userprofile/j;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->k(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/userprofile/j;->a(Ljava/lang/String;)V

    .line 376
    return-void

    .line 360
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 368
    :cond_3
    const-string v0, "SHORT"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "SHORT_HIDE"

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->i(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 369
    :cond_4
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->j(Lcom/sec/chaton/userprofile/j;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 370
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v2}, Lcom/sec/chaton/userprofile/j;->j(Lcom/sec/chaton/userprofile/j;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 371
    iget-object v2, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/userprofile/j;->b(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    .line 372
    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1, v0}, Lcom/sec/chaton/userprofile/j;->c(Lcom/sec/chaton/userprofile/j;Ljava/lang/String;)Ljava/lang/String;

    .line 373
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v0}, Lcom/sec/chaton/userprofile/j;->l(Lcom/sec/chaton/userprofile/j;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-static {v1}, Lcom/sec/chaton/userprofile/j;->k(Lcom/sec/chaton/userprofile/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 402
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/j;->c()V

    .line 407
    :goto_0
    return-void

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/userprofile/p;->a:Lcom/sec/chaton/userprofile/j;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/j;->d()V

    goto :goto_0
.end method
