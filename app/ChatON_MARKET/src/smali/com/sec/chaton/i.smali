.class Lcom/sec/chaton/i;
.super Landroid/os/Handler;
.source "AdminMenu.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/AdminMenu;


# direct methods
.method constructor <init>(Lcom/sec/chaton/AdminMenu;)V
    .locals 0

    .prologue
    .line 906
    iput-object p1, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 910
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    .line 911
    invoke-virtual {v0}, Lcom/sec/chaton/chat/fj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 913
    if-nez v0, :cond_0

    .line 988
    :goto_0
    return-void

    .line 919
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/a/a/l;->i:Lcom/sec/chaton/a/a/l;

    if-ne v1, v2, :cond_1

    .line 920
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "result code:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdminMenu"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 922
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    if-ne v1, v2, :cond_2

    .line 923
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "result code:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdminMenu"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 926
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/a/a/l;->l:Lcom/sec/chaton/a/a/l;

    if-ne v1, v2, :cond_3

    .line 927
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "result code:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdminMenu"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 933
    :cond_3
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_5

    .line 934
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v1

    .line 935
    const/16 v2, 0x18

    if-eq v1, v2, :cond_4

    const/16 v2, 0x17

    if-eq v1, v2, :cond_4

    const/16 v2, 0x15

    if-ne v1, v2, :cond_5

    .line 938
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->A(Lcom/sec/chaton/AdminMenu;)Lcom/sec/chaton/d/o;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 939
    iget-object v1, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->A(Lcom/sec/chaton/AdminMenu;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->o()Ljava/lang/String;

    move-result-object v1

    .line 940
    iget-object v2, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v2}, Lcom/sec/chaton/AdminMenu;->A(Lcom/sec/chaton/AdminMenu;)Lcom/sec/chaton/d/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/d/o;->j()V

    .line 943
    iget-object v2, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v2}, Lcom/sec/chaton/AdminMenu;->A(Lcom/sec/chaton/AdminMenu;)Lcom/sec/chaton/d/o;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->b()I

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 953
    :cond_5
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 955
    :pswitch_0
    const-string v1, ""

    .line 956
    iget-boolean v2, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-eqz v2, :cond_a

    .line 957
    instance-of v2, v0, Lcom/sec/chaton/d/a/cz;

    if-eqz v2, :cond_8

    move-object v1, v0

    .line 958
    check-cast v1, Lcom/sec/chaton/d/a/cz;

    invoke-virtual {v1}, Lcom/sec/chaton/d/a/cz;->a()Ljava/lang/String;

    move-result-object v1

    .line 963
    :cond_6
    :goto_1
    iget-object v2, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v2}, Lcom/sec/chaton/AdminMenu;->A(Lcom/sec/chaton/AdminMenu;)Lcom/sec/chaton/d/o;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/chaton/d/o;->c(Ljava/lang/String;)V

    .line 965
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v1

    .line 966
    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2, v1}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/a/a/l;->a:Lcom/sec/chaton/a/a/l;

    if-ne v1, v2, :cond_7

    .line 968
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "result code:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdminMenu"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->B(Lcom/sec/chaton/AdminMenu;)I

    .line 971
    iget-object v0, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->y(Lcom/sec/chaton/AdminMenu;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->C(Lcom/sec/chaton/AdminMenu;)I

    move-result v1

    if-ge v0, v1, :cond_9

    .line 972
    iget-object v0, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Make Dummy ChatRoom "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v2}, Lcom/sec/chaton/AdminMenu;->y(Lcom/sec/chaton/AdminMenu;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 973
    iget-object v0, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->z(Lcom/sec/chaton/AdminMenu;)V

    goto/16 :goto_0

    .line 960
    :cond_8
    instance-of v2, v0, Lcom/sec/chaton/d/a/dh;

    if-eqz v2, :cond_6

    move-object v1, v0

    .line 961
    check-cast v1, Lcom/sec/chaton/d/a/dh;

    iget-object v1, v1, Lcom/sec/chaton/d/a/dh;->d:Ljava/lang/String;

    goto :goto_1

    .line 975
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 976
    iget-object v0, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v2}, Lcom/sec/chaton/AdminMenu;->y(Lcom/sec/chaton/AdminMenu;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "chat rooms maked ..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 979
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->y(Lcom/sec/chaton/AdminMenu;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v1}, Lcom/sec/chaton/AdminMenu;->C(Lcom/sec/chaton/AdminMenu;)I

    move-result v1

    if-ge v0, v1, :cond_b

    .line 980
    iget-object v0, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->z(Lcom/sec/chaton/AdminMenu;)V

    goto/16 :goto_0

    .line 982
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 983
    iget-object v0, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v0}, Lcom/sec/chaton/AdminMenu;->e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/i;->a:Lcom/sec/chaton/AdminMenu;

    invoke-static {v2}, Lcom/sec/chaton/AdminMenu;->y(Lcom/sec/chaton/AdminMenu;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "chat rooms maked ..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 953
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
