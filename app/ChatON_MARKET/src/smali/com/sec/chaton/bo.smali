.class Lcom/sec/chaton/bo;
.super Ljava/lang/Object;
.source "TabActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/TabActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 2671
    iput-object p1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/16 v3, 0x15

    .line 2674
    .line 2675
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/buddy/BuddyFragment;->m:Z

    .line 2676
    packed-switch p2, :pswitch_data_0

    .line 2724
    :cond_0
    :goto_0
    return-void

    .line 2678
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2679
    const-string v1, "ACTIVITY_PURPOSE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2680
    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2681
    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2684
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2685
    const-string v1, "ACTIVITY_PURPOSE"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2686
    const-string v1, "ACTIVITY_PURPOSE_CALL_START_CHAT"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2687
    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2690
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->I(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2691
    iget-object v0, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->J(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2692
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2693
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2694
    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2696
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2697
    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2700
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->K(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2701
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2702
    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2707
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->I(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2708
    iget-object v0, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->J(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2709
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2710
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2711
    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2713
    :cond_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVInstallDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2714
    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2717
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->K(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2718
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/TabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/dialog/ChatONVRedirectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2719
    iget-object v1, p0, Lcom/sec/chaton/bo;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2676
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
