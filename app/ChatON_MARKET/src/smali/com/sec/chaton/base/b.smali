.class final Lcom/sec/chaton/base/b;
.super Ljava/lang/Object;
.source "ActivityHelper.java"

# interfaces
.implements Landroid/view/LayoutInflater$Factory2;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 44
    const-string v0, "TextView"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Lcom/sec/chaton/widget/AdaptableTextView;

    invoke-direct {v0, p3, p4}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    :goto_0
    return-object v0

    .line 48
    :cond_0
    const-string v0, "EditText"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    new-instance v0, Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-direct {v0, p3, p4}, Lcom/sec/chaton/widget/AdaptableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_0

    .line 52
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    const-string v0, "TextView"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    new-instance v0, Lcom/sec/chaton/widget/AdaptableTextView;

    invoke-direct {v0, p2, p3}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    :goto_0
    return-object v0

    .line 35
    :cond_0
    const-string v0, "EditText"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    new-instance v0, Lcom/sec/chaton/widget/AdaptableEditText;

    invoke-direct {v0, p2, p3}, Lcom/sec/chaton/widget/AdaptableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_0

    .line 39
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
