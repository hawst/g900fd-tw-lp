.class public abstract Lcom/sec/chaton/FlexibleActivity;
.super Lcom/sec/common/actionbar/ActionBarFragmentActivity;
.source "FlexibleActivity.java"

# interfaces
.implements Lcom/sec/common/actionbar/b;
.implements Lcom/sec/common/actionbar/d;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z

.field private c:Z

.field private d:Z

.field private e:I

.field private f:I

.field private g:Landroid/database/DataSetObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/chaton/FlexibleActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/FlexibleActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 54
    invoke-direct {p0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;-><init>()V

    .line 41
    new-instance v0, Lcom/sec/chaton/t;

    invoke-direct {v0, p0}, Lcom/sec/chaton/t;-><init>(Lcom/sec/chaton/FlexibleActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/FlexibleActivity;->g:Landroid/database/DataSetObserver;

    .line 55
    iput v1, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    .line 56
    iput v1, p0, Lcom/sec/chaton/FlexibleActivity;->f:I

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/FlexibleActivity;->d:Z

    .line 58
    return-void
.end method

.method private a(Lcom/sec/common/actionbar/a;I)Lcom/sec/common/actionbar/c;
    .locals 2

    .prologue
    .line 206
    const/4 v0, 0x0

    .line 208
    invoke-virtual {p1}, Lcom/sec/common/actionbar/a;->c()I

    move-result v1

    if-le v1, p2, :cond_0

    .line 209
    invoke-virtual {p1, p2}, Lcom/sec/common/actionbar/a;->c(I)Lcom/sec/common/actionbar/c;

    move-result-object v0

    .line 212
    :cond_0
    return-object v0
.end method

.method private a(Lcom/sec/common/actionbar/a;Landroid/view/View;)Lcom/sec/common/actionbar/c;
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p1}, Lcom/sec/common/actionbar/a;->b()Lcom/sec/common/actionbar/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/common/actionbar/c;->a(Landroid/view/View;)Lcom/sec/common/actionbar/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/common/actionbar/c;->a(Lcom/sec/common/actionbar/d;)Lcom/sec/common/actionbar/c;

    move-result-object v0

    .line 222
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/sec/common/actionbar/a;->a(Lcom/sec/common/actionbar/c;Z)V

    .line 224
    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/FlexibleActivity;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/chaton/FlexibleActivity;->b(I)V

    return-void
.end method

.method private b()I
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 228
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/FlexibleActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    .line 233
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(I)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 167
    invoke-virtual {p0}, Lcom/sec/chaton/FlexibleActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v1

    .line 169
    invoke-virtual {p0}, Lcom/sec/chaton/FlexibleActivity;->a()Landroid/widget/BaseAdapter;

    move-result-object v2

    .line 171
    if-ne p1, v5, :cond_1

    .line 172
    invoke-virtual {v1, v5}, Lcom/sec/common/actionbar/a;->a(I)V

    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/FlexibleActivity;->b:Z

    if-nez v0, :cond_0

    .line 175
    invoke-virtual {v1, v2, p0}, Lcom/sec/common/actionbar/a;->a(Landroid/widget/SpinnerAdapter;Lcom/sec/common/actionbar/b;)V

    .line 177
    iput-boolean v5, p0, Lcom/sec/chaton/FlexibleActivity;->b:Z

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    if-ne p1, v4, :cond_0

    .line 180
    iget-boolean v3, p0, Lcom/sec/chaton/FlexibleActivity;->c:Z

    if-eqz v3, :cond_2

    .line 181
    iput-boolean v5, p0, Lcom/sec/chaton/FlexibleActivity;->d:Z

    .line 184
    :cond_2
    invoke-virtual {v1, v4}, Lcom/sec/common/actionbar/a;->a(I)V

    .line 186
    iput-boolean v0, p0, Lcom/sec/chaton/FlexibleActivity;->d:Z

    .line 188
    :goto_1
    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 189
    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/FlexibleActivity;->a(Lcom/sec/common/actionbar/a;I)Lcom/sec/common/actionbar/c;

    move-result-object v3

    .line 191
    if-eqz v3, :cond_3

    .line 192
    invoke-virtual {v3}, Lcom/sec/common/actionbar/c;->a()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v2, v0, v4, v6}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/common/actionbar/c;->a(Landroid/view/View;)Lcom/sec/common/actionbar/c;

    .line 188
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 195
    :cond_3
    invoke-virtual {v2, v0, v6, v6}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/sec/chaton/FlexibleActivity;->a(Lcom/sec/common/actionbar/a;Landroid/view/View;)Lcom/sec/common/actionbar/c;

    goto :goto_2

    .line 199
    :cond_4
    iget-boolean v0, p0, Lcom/sec/chaton/FlexibleActivity;->c:Z

    if-nez v0, :cond_0

    .line 200
    iput-boolean v5, p0, Lcom/sec/chaton/FlexibleActivity;->c:Z

    goto :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    .line 241
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/sec/chaton/FlexibleActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 246
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "setHasEmbeddedTabs"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 247
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 248
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 249
    :catch_0
    move-exception v0

    .line 250
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 251
    sget-object v1, Lcom/sec/chaton/FlexibleActivity;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()Landroid/widget/BaseAdapter;
.end method

.method protected abstract a(I)V
.end method

.method public a(Lcom/sec/common/actionbar/c;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 2

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/sec/chaton/FlexibleActivity;->d:Z

    if-eqz v0, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/FlexibleActivity;->f:I

    .line 129
    invoke-virtual {p1}, Lcom/sec/common/actionbar/c;->b()I

    move-result v0

    .line 131
    iget v1, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    if-eq v1, v0, :cond_0

    .line 132
    iput v0, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    .line 133
    iget v0, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/FlexibleActivity;->a(I)V

    goto :goto_0
.end method

.method public a(IJ)Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/FlexibleActivity;->f:I

    .line 112
    iget v0, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    if-eq v0, p1, :cond_0

    .line 113
    iput p1, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    .line 115
    iget v0, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/FlexibleActivity;->a(I)V

    .line 118
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public b(Lcom/sec/common/actionbar/c;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public c(Lcom/sec/common/actionbar/c;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 149
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 151
    invoke-direct {p0}, Lcom/sec/chaton/FlexibleActivity;->c()V

    .line 153
    invoke-virtual {p0}, Lcom/sec/chaton/FlexibleActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v1

    .line 155
    if-eqz v1, :cond_0

    .line 156
    invoke-direct {p0}, Lcom/sec/chaton/FlexibleActivity;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/FlexibleActivity;->b(I)V

    .line 158
    iget v0, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/common/actionbar/a;->d(I)V

    .line 160
    :cond_0
    return-void

    .line 158
    :cond_1
    iget v0, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-gt v0, v1, :cond_0

    .line 66
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/FlexibleActivity;->setContentView(Landroid/view/View;)V

    .line 69
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/FlexibleActivity;->c()V

    .line 71
    invoke-virtual {p0}, Lcom/sec/chaton/FlexibleActivity;->a()Landroid/widget/BaseAdapter;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/sec/chaton/FlexibleActivity;->g:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 74
    invoke-direct {p0}, Lcom/sec/chaton/FlexibleActivity;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/FlexibleActivity;->b(I)V

    .line 76
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "menuIndex"

    iget v2, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/FlexibleActivity;->f:I

    .line 77
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->onDestroy()V

    .line 104
    invoke-virtual {p0}, Lcom/sec/chaton/FlexibleActivity;->a()Landroid/widget/BaseAdapter;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/sec/chaton/FlexibleActivity;->g:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 106
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 81
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/sec/chaton/FlexibleActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v1

    .line 85
    iget v0, p0, Lcom/sec/chaton/FlexibleActivity;->f:I

    if-ne v0, v2, :cond_1

    .line 86
    iget v0, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/common/actionbar/a;->d(I)V

    .line 91
    :goto_1
    return-void

    .line 86
    :cond_0
    iget v0, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    goto :goto_0

    .line 88
    :cond_1
    iget v0, p0, Lcom/sec/chaton/FlexibleActivity;->f:I

    invoke-virtual {v1, v0}, Lcom/sec/common/actionbar/a;->d(I)V

    .line 89
    iput v2, p0, Lcom/sec/chaton/FlexibleActivity;->f:I

    goto :goto_1
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->onStop()V

    .line 97
    const-string v0, "menuIndex"

    iget v1, p0, Lcom/sec/chaton/FlexibleActivity;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 98
    return-void
.end method
