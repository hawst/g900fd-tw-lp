.class public Lcom/sec/chaton/specialbuddy/l;
.super Ljava/lang/Object;
.source "ChatONLiveMainFragment.java"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    const-string v0, "buddyid"

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/l;->a:Ljava/lang/String;

    .line 298
    const-string v0, "follow"

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/l;->b:Ljava/lang/String;

    .line 305
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/l;->c:Ljava/lang/String;

    .line 306
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/l;->d:Ljava/lang/Boolean;

    .line 307
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ZLcom/sec/chaton/specialbuddy/h;)V
    .locals 0

    .prologue
    .line 293
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/specialbuddy/l;-><init>(Ljava/lang/String;Z)V

    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 310
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/l;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 318
    :cond_0
    :goto_0
    return v0

    .line 314
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/l;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 318
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 322
    invoke-direct {p0}, Lcom/sec/chaton/specialbuddy/l;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 323
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 324
    const-string v0, "getJsonString(), error : hasPairAll() == false "

    invoke-static {}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    :cond_0
    const-string v0, ""

    .line 340
    :goto_0
    return-object v0

    .line 329
    :cond_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 331
    :try_start_0
    const-string v1, "buddyid"

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 332
    const-string v1, "follow"

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/l;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 333
    :catch_0
    move-exception v0

    .line 334
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 335
    invoke-static {}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 337
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method
