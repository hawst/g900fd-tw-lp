.class public abstract Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;
.super Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;
.source "AbstractChatONLiveFragment.java"

# interfaces
.implements Lcom/sec/chaton/base/d;


# instance fields
.field private i:Landroid/widget/FrameLayout;

.field private j:Landroid/widget/FrameLayout;

.field private k:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/sec/chaton/mobileweb/p;)V
    .locals 1

    .prologue
    .line 32
    const v0, 0x7f030101

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/mobileweb/AbstractMobileWebFragment;-><init>(Ljava/lang/String;Lcom/sec/chaton/mobileweb/p;I)V

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;)Z
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k()Z

    move-result v0

    return v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 61
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->f()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->a(Landroid/content/Context;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k:Landroid/app/ProgressDialog;

    .line 62
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/specialbuddy/a;

    invoke-direct {v1, p0}, Lcom/sec/chaton/specialbuddy/a;-><init>(Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/view/View;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 97
    const v0, 0x7f07045c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->i:Landroid/widget/FrameLayout;

    .line 98
    const v0, 0x7f0703f3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->j:Landroid/widget/FrameLayout;

    .line 100
    const v0, 0x7f07045d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->c:Landroid/webkit/WebView;

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->c:Landroid/webkit/WebView;

    return-object v0
.end method

.method protected b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 128
    const-string v0, "javascript:chaton.loadNewer(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 129
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "refreshWebContents() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->a(Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->n()V

    .line 43
    return-void
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->o()V

    .line 53
    return-void
.end method

.method protected d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->f()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 108
    :cond_0
    return-void
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->m()V

    .line 94
    return-void
.end method

.method protected j()V
    .locals 2

    .prologue
    .line 122
    const-string v0, "refreshWebContents()"

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v0, "javascript:chaton.loadNewer()"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->a(Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method l()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 134
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 169
    :goto_0
    return-void

    .line 138
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0042

    new-instance v2, Lcom/sec/chaton/specialbuddy/b;

    invoke-direct {v2, p0}, Lcom/sec/chaton/specialbuddy/b;-><init>(Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 151
    invoke-interface {v0, v3}, Lcom/sec/common/a/d;->setCanceledOnTouchOutside(Z)V

    .line 152
    new-instance v1, Lcom/sec/chaton/specialbuddy/c;

    invoke-direct {v1, p0}, Lcom/sec/chaton/specialbuddy/c;-><init>(Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;)V

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 168
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method m()V
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->j:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->i:Landroid/widget/FrameLayout;

    if-nez v0, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->j:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->i:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 181
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 182
    const v0, 0x7f0b002a

    .line 183
    const/4 v2, -0x3

    if-eq v2, v1, :cond_2

    const/4 v2, -0x2

    if-ne v2, v1, :cond_3

    .line 184
    :cond_2
    const v0, 0x7f0b0205

    .line 186
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
