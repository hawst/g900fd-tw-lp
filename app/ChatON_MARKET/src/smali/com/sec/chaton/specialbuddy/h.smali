.class Lcom/sec/chaton/specialbuddy/h;
.super Landroid/database/ContentObserver;
.source "ChatONLiveMainFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/h;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onChange() SpecialBuddyObserver, live buddy follow/unfollow DB. mIsActivityStarted : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/h;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->a(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/h;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->b(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/h;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->j()V

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/h;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->a(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;Z)Z

    goto :goto_0
.end method
