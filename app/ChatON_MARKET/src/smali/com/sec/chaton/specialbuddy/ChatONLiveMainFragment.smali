.class public Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;
.super Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;
.source "ChatONLiveMainFragment.java"


# static fields
.field private static final i:Ljava/lang/String;


# instance fields
.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Landroid/database/ContentObserver;

.field private n:Landroid/os/Handler;

.field private o:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    sget-object v0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->i:Ljava/lang/String;

    sget-object v1, Lcom/sec/chaton/mobileweb/p;->b:Lcom/sec/chaton/mobileweb/p;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;-><init>(Ljava/lang/String;Lcom/sec/chaton/mobileweb/p;)V

    .line 38
    iput-boolean v2, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->j:Z

    .line 40
    iput-boolean v2, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->l:Z

    .line 42
    new-instance v0, Lcom/sec/chaton/specialbuddy/h;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/specialbuddy/h;-><init>(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->m:Landroid/database/ContentObserver;

    .line 63
    new-instance v0, Lcom/sec/chaton/specialbuddy/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/specialbuddy/i;-><init>(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->n:Landroid/os/Handler;

    .line 171
    new-instance v0, Lcom/sec/chaton/specialbuddy/j;

    invoke-direct {v0, p0}, Lcom/sec/chaton/specialbuddy/j;-><init>(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->o:Landroid/os/Handler;

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->k:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->f:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->j:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->f:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)Z
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->k()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->o:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected b()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->n:Landroid/os/Handler;

    return-object v0
.end method

.method protected j()V
    .locals 4

    .prologue
    .line 233
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->j:Z

    .line 236
    const-string v0, ""

    .line 237
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 238
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 239
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/af;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    .line 240
    new-instance v1, Lcom/sec/chaton/specialbuddy/l;

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->k:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/sec/chaton/specialbuddy/l;-><init>(Ljava/lang/String;ZLcom/sec/chaton/specialbuddy/h;)V

    .line 241
    invoke-virtual {v1}, Lcom/sec/chaton/specialbuddy/l;->a()Ljava/lang/String;

    move-result-object v0

    .line 245
    :cond_0
    invoke-super {p0, v0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->b(Ljava/lang/String;)V

    .line 246
    return-void
.end method

.method protected n()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 251
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->h()Lcom/sec/chaton/mobileweb/p;

    move-result-object v0

    .line 252
    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/p;->a()Ljava/lang/String;

    move-result-object v1

    .line 253
    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/p;->b()Ljava/lang/String;

    move-result-object v2

    .line 255
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    const-string v0, ""

    .line 286
    :goto_0
    return-object v0

    .line 262
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "cleared_timestamp_livepartner_buddies"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v3

    .line 263
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v5, "new_livepartner_count"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 265
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "myid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "chaton_id"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 267
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "badgecount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 268
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "lastcleartime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 270
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "&"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 271
    iget-boolean v3, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->l:Z

    if-eqz v3, :cond_1

    .line 272
    const-string v0, "&view=buddy"

    .line 278
    :cond_1
    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 279
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 285
    :goto_1
    const-string v1, "getRequestUrl(), requestUrl: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 281
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 179
    invoke-super {p0, p1}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onCreate(Landroid/os/Bundle;)V

    .line 180
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_0

    .line 182
    const-string v1, "key_start_with_buddylist"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    const-string v1, "key_start_with_buddylist"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->l:Z

    .line 187
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 188
    sget-object v1, Lcom/sec/chaton/e/ai;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->m:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 189
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 218
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->m:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 221
    invoke-super {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onDestroy()V

    .line 222
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->n:Landroid/os/Handler;

    .line 227
    invoke-super {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onDestroyView()V

    .line 228
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 229
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 193
    invoke-super {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onResume()V

    .line 196
    invoke-static {}, Lcom/sec/common/util/i;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 197
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->l()V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->i()Z

    move-result v0

    .line 202
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 203
    const-string v1, "onResume(), requestedLoadingWeb(%s), isActivityStarted(%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->f:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 204
    sget-object v2, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_2
    if-eqz v0, :cond_3

    .line 208
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->j:Z

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->j()V

    goto :goto_0

    .line 212
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->c(Ljava/lang/String;)V

    goto :goto_0
.end method
