.class Lcom/sec/chaton/specialbuddy/u;
.super Ljava/lang/Object;
.source "SpecialBuddyChatFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V
    .locals 0

    .prologue
    .line 801
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 987
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 969
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const-wide v6, 0x7fffffffffffffffL

    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 804
    packed-switch p1, :pswitch_data_0

    .line 806
    if-eqz p3, :cond_0

    .line 807
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 965
    :cond_0
    :goto_0
    return-void

    .line 812
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)Z

    .line 813
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->g(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    .line 815
    const-string v0, "spbd_refresh, onQueryComplete(), QUERY_INBOX #2"

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    const-string v0, "onQueryCompleted - QUERY_INBOX"

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    if-nez p3, :cond_1

    .line 818
    const-string v0, "onQueryCompleted - QUERY_INBOX, #1 null"

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 821
    :cond_1
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 822
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 823
    const-string v0, "onQueryCompleted - QUERY_INBOX, #2 nothing"

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 826
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->h(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->i(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 827
    :cond_3
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 828
    const-string v0, "onQueryCompleted - QUERY_INBOX, #3 (conditional return)."

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 832
    :cond_4
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 833
    if-nez v0, :cond_5

    .line 834
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 835
    const-string v0, "onQueryCompleted - QUERY_INBOX, #4 fail : moveToFirst."

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 839
    :cond_5
    const-string v0, "inbox_last_chat_type"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 840
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v1, "inbox_no"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->b(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 841
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v1, "Y"

    const-string v2, "inbox_enable_noti"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->b(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)Z

    .line 842
    const-string v0, "weburl"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 843
    const-string v1, "inbox_web_url"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 846
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 848
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onQueryComplete(), use WebUrl from BuddyTable: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v9, v0

    .line 857
    :goto_1
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 858
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    sget-object v1, Lcom/sec/chaton/mobileweb/h;->a:Lcom/sec/chaton/mobileweb/h;

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Lcom/sec/chaton/mobileweb/h;)V

    .line 861
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 866
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onQueryComplete(), no way of getting WebUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 849
    :cond_6
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 851
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onQueryComplete(), use WebUrl from InboxTable: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v9, v1

    goto :goto_1

    .line 852
    :cond_7
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 853
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v2

    .line 854
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onQueryComplete(), use mForwardedWebURL from External: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v5}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v9, v2

    goto/16 :goto_1

    .line 870
    :cond_8
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {v9, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 871
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->l(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v2

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v5}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5, v9}, Lcom/sec/chaton/e/a/n;->a(Lcom/sec/chaton/e/a/u;ILjava/lang/String;Ljava/lang/String;)V

    .line 872
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onQueryComplete(), spbd_weburl.update inbox : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    const-string v2, "onQueryComplete(), recoverty ??"

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    :cond_9
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_a

    .line 877
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onQueryComplete(), spbd_weburl buddy : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onQueryComplete(), spbd_weburl inbox : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onQueryComplete(), spbd_weburl mForwardedWebURL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onQueryComplete(), spbd_weburl. applied weburl : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    :cond_a
    const/16 v0, 0xc

    if-ne v3, v0, :cond_d

    .line 884
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 885
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 888
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->b()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 889
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lcom/sec/chaton/d/o;->c(J)V

    .line 890
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->k()V

    .line 938
    :cond_c
    :goto_2
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 939
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0, v8}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)Z

    .line 941
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 942
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/af;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)Z

    .line 943
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onQueryReturn(), mIsInService: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->g(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    .line 946
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)V

    .line 948
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 949
    const-string v0, "onQueryComplete(), mWebURL is null"

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0, v10}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->b(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Lcom/sec/chaton/mobileweb/h;)V

    .line 961
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->w(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    goto/16 :goto_0

    .line 892
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v1, "inbox_session_id"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->c(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 893
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v1, "inbox_last_msg_no"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/Long;)Ljava/lang/Long;

    .line 898
    const-string v0, "inbox_title"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 899
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hslee_title, mChatTitle from DB query : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 901
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 902
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hslee_title, setTitle() #2 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->n(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->n(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Ljava/lang/String;)V

    .line 906
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v1, "buddy_name"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 907
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hslee_title, mBuddyName from DB query: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->o(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->n(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 909
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->o(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 910
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hslee_title, setTitle() #3 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->n(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->n(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Ljava/lang/String;)V

    .line 914
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v1, "_id"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;I)I

    .line 916
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v1, "buddy_no"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 917
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v1, "Y"

    const-string v2, "inbox_valid"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->c(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)Z

    .line 918
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v1, "inbox_last_timestamp"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;J)J

    .line 920
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v2

    .line 921
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/e/r;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_10

    .line 922
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v2

    .line 925
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 926
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 930
    :cond_11
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->b()I

    move-result v4

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 931
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lcom/sec/chaton/d/o;->c(J)V

    .line 932
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->q(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 933
    const-string v0, "spbd_refresh, onQueryComplete(), QUERY_INBOX, call allowChat() #2-1"

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    iget-object v3, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->r(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v4}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v5}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v6}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->t(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)J

    move-result-wide v6

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;JZ)Z

    goto/16 :goto_2

    .line 951
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 952
    const-string v0, "onQueryComplete(), mBuddyNo is null"

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0, v10}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->c(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Lcom/sec/chaton/mobileweb/h;)V

    goto/16 :goto_3

    .line 955
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->i()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 956
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j()V

    goto/16 :goto_3

    .line 958
    :cond_14
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v9, v2}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->g(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_15
    move-object v9, v10

    goto/16 :goto_1

    .line 804
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    .line 973
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 983
    :cond_0
    :goto_0
    return-void

    .line 977
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 980
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/u;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/sec/chaton/chat/notification/a;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method
