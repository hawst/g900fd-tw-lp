.class Lcom/sec/chaton/specialbuddy/r;
.super Landroid/os/Handler;
.source "SpecialBuddyChatFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V
    .locals 0

    .prologue
    .line 682
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 685
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->c(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 770
    :cond_0
    :goto_0
    return-void

    .line 689
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    .line 690
    invoke-virtual {v0}, Lcom/sec/chaton/chat/fj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 691
    if-nez v0, :cond_2

    .line 692
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 693
    const-string v0, "resultEntry is null"

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 699
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/a/a/l;->i:Lcom/sec/chaton/a/a/l;

    if-ne v1, v2, :cond_3

    .line 701
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 702
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->b()V

    goto :goto_0

    .line 712
    :cond_3
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    if-ne v1, v2, :cond_4

    .line 713
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    sget-object v2, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Lcom/sec/chaton/a/a/l;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 714
    if-eqz v1, :cond_4

    .line 715
    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 720
    :cond_4
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_6

    .line 721
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v1

    .line 723
    const/16 v2, 0x18

    if-eq v1, v2, :cond_5

    const/16 v2, 0x17

    if-eq v1, v2, :cond_5

    const/16 v2, 0x15

    if-ne v1, v2, :cond_6

    .line 724
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 725
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->j()V

    .line 726
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->b()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 731
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "spbd_refresh, messageControl return : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_0

    .line 740
    :sswitch_0
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_0

    .line 741
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/e/r;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v1, v2, :cond_0

    iget-object v0, v0, Lcom/sec/chaton/a/a/k;->b:Lcom/sec/chaton/a/a/n;

    sget-object v1, Lcom/sec/chaton/a/a/n;->d:Lcom/sec/chaton/a/a/n;

    if-ne v0, v1, :cond_0

    goto/16 :goto_0

    .line 748
    :sswitch_1
    check-cast v0, Lcom/sec/chaton/a/a/b;

    .line 749
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/b;->b()I

    move-result v0

    .line 750
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s()Z

    move-result v1

    if-nez v1, :cond_0

    .line 751
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v2, ""

    const-string v3, ""

    invoke-virtual {v1, v0, v2, v3, v5}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)V

    goto/16 :goto_0

    .line 763
    :sswitch_2
    check-cast v0, Lcom/sec/chaton/a/a/d;

    .line 764
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/d;->b()I

    move-result v0

    .line 765
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s()Z

    move-result v1

    if-nez v1, :cond_0

    .line 766
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/r;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    const-string v2, ""

    const-string v3, ""

    invoke-virtual {v1, v0, v2, v3, v5}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)V

    goto/16 :goto_0

    .line 732
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_1
        0x22 -> :sswitch_2
        0x24 -> :sswitch_2
    .end sparse-switch
.end method
