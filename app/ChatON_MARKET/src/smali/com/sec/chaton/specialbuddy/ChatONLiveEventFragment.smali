.class public Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;
.super Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;
.source "ChatONLiveEventFragment.java"


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field i:Z

.field private k:Landroid/os/Handler;

.field private l:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    sget-object v0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->j:Ljava/lang/String;

    sget-object v1, Lcom/sec/chaton/mobileweb/p;->c:Lcom/sec/chaton/mobileweb/p;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;-><init>(Ljava/lang/String;Lcom/sec/chaton/mobileweb/p;)V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->i:Z

    .line 32
    new-instance v0, Lcom/sec/chaton/specialbuddy/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/specialbuddy/d;-><init>(Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->k:Landroid/os/Handler;

    .line 66
    new-instance v0, Lcom/sec/chaton/specialbuddy/e;

    invoke-direct {v0, p0}, Lcom/sec/chaton/specialbuddy/e;-><init>(Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->l:Landroid/os/Handler;

    .line 25
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;)Z
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->k()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->l:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected b()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->k:Landroid/os/Handler;

    return-object v0
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->i:Z

    .line 108
    invoke-super {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->j()V

    .line 109
    return-void
.end method

.method protected n()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 117
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->h()Lcom/sec/chaton/mobileweb/p;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/p;->a()Ljava/lang/String;

    move-result-object v1

    .line 119
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "event_web_url"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "event_id"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 121
    invoke-static {}, Lcom/sec/chaton/util/am;->t()Ljava/lang/String;

    move-result-object v3

    .line 124
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 125
    const-string v0, ""

    .line 152
    :goto_0
    return-object v0

    .line 130
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "eventid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 131
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "iso2="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 133
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "&"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 138
    const-string v3, "?"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 139
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 144
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRequestUrl() #1, tempUrl : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->j:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v2, "{LIVE_MOBILEWEB}"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 146
    const-string v2, "{LIVE_MOBILEWEB}"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 151
    :cond_1
    const-string v1, "getRequestUrl() #2, requestUrl: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->k:Landroid/os/Handler;

    .line 101
    invoke-super {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onDestroyView()V

    .line 102
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 103
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 74
    invoke-super {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onResume()V

    .line 77
    invoke-static {}, Lcom/sec/common/util/i;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->l()V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->i()Z

    move-result v0

    .line 83
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 84
    const-string v1, "onResume(), requestedLoadingWeb(%s), isActivityStarted(%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    iget-boolean v3, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->f:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 85
    sget-object v2, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_2
    if-eqz v0, :cond_3

    .line 89
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->i:Z

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->j()V

    goto :goto_0

    .line 93
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/ChatONLiveEventFragment;->c(Ljava/lang/String;)V

    .line 94
    invoke-static {v5}, Lcom/sec/chaton/event/f;->c(Z)V

    goto :goto_0
.end method
