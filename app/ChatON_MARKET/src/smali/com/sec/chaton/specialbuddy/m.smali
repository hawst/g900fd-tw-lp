.class Lcom/sec/chaton/specialbuddy/m;
.super Landroid/os/Handler;
.source "SpecialBuddyChatFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {v0}, Lcom/sec/chaton/mobileweb/o;->a(I)Lcom/sec/chaton/mobileweb/o;

    move-result-object v1

    .line 163
    if-eqz v1, :cond_0

    .line 167
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 170
    invoke-virtual {v1}, Lcom/sec/chaton/mobileweb/o;->d()Ljava/lang/String;

    move-result-object v2

    .line 171
    const-string v3, "api(%s) with arguments : %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    aput-object v0, v5, v7

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 173
    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    sget-object v2, Lcom/sec/chaton/specialbuddy/n;->b:[I

    invoke-virtual {v1}, Lcom/sec/chaton/mobileweb/o;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 184
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->g()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/mobileweb/s;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 185
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->b(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Landroid/os/Handler;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/chaton/mobileweb/a/a;->a(Landroid/os/Handler;Ljava/lang/String;)Lcom/sec/chaton/mobileweb/a/a;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/String;

    aput-object v0, v2, v6

    invoke-virtual {v1, v2}, Lcom/sec/chaton/mobileweb/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 191
    :pswitch_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/mobileweb/j;->a(Landroid/net/Uri;)Lcom/sec/chaton/mobileweb/j;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/j;->a()Lcom/sec/chaton/mobileweb/l;

    move-result-object v1

    .line 197
    sget-object v2, Lcom/sec/chaton/specialbuddy/n;->a:[I

    invoke-virtual {v1}, Lcom/sec/chaton/mobileweb/l;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 199
    :pswitch_2
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/mobileweb/j;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 200
    if-eqz v0, :cond_0

    .line 201
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 207
    :pswitch_3
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/mobileweb/j;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_0

    .line 209
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 210
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 216
    :pswitch_4
    const-string v0, "live_chat_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 235
    :pswitch_5
    new-instance v1, Lcom/sec/chaton/util/v;

    invoke-direct {v1, v0}, Lcom/sec/chaton/util/v;-><init>(Ljava/lang/String;)V

    .line 238
    :try_start_0
    const-class v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/v;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v0

    .line 245
    :goto_1
    if-nez v3, :cond_2

    .line 246
    const-string v0, "[IN] ENTRY : is null "

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 239
    :catch_0
    move-exception v0

    .line 240
    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v3, v4

    goto :goto_1

    .line 250
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[IN] ENTRY : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v6, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/m;->a:Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    invoke-virtual {v3}, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareEntry;->getRawContents()Ljava/lang/String;

    move-result-object v3

    const v5, 0x7f0b03c4

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Landroid/content/Context;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_5
    .end packed-switch

    .line 197
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
