.class public Lcom/sec/chaton/specialbuddy/g;
.super Ljava/lang/Object;
.source "ChatONLiveHelper.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/chaton/specialbuddy/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    :goto_0
    return-object v0

    .line 39
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 40
    const-string v2, "content"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    .line 41
    const-string v3, "push_message"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    .line 46
    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    .line 65
    const-string v2, "content"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 66
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/specialbuddy/g;->a(Lorg/json/JSONObject;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v0

    goto :goto_0

    .line 85
    :cond_1
    invoke-static {v1, p1}, Lcom/sec/chaton/specialbuddy/g;->a(Lorg/json/JSONObject;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v1

    .line 89
    sget-object v2, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Lorg/json/JSONObject;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 95
    if-nez p0, :cond_0

    .line 111
    :goto_0
    return-object v0

    .line 100
    :cond_0
    :try_start_0
    new-instance v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    invoke-direct {v1}, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;-><init>()V

    .line 101
    const-string v2, "name"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->name:Ljava/lang/String;

    .line 102
    const-string v2, "title"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->title:Ljava/lang/String;

    .line 103
    if-nez p1, :cond_1

    .line 104
    const-string v2, "id"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->id:Ljava/lang/String;

    .line 105
    const-string v2, "hash"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->hash:Ljava/lang/String;

    .line 106
    const-string v2, "url"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->url:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move-object v0, v1

    .line 108
    goto :goto_0

    .line 109
    :catch_0
    move-exception v1

    .line 110
    sget-object v2, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 116
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 117
    const-string v1, "type"

    const-string v2, "live_share"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 118
    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 119
    const-string v1, "title"

    invoke-virtual {v0, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 120
    return-object v0
.end method

.method public static a()V
    .locals 5

    .prologue
    .line 399
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "notification_api_timestamp"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 401
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "cleared_timestamp_livepartner_buddies"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 402
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "new_livepartner_count"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 406
    new-instance v2, Landroid/content/Intent;

    const-string v3, "more_tab_badge_update"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 407
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 409
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clear new live buddy badge count zero(0), update clear time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    return-void
.end method

.method public static b(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 222
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265
    :goto_0
    return-object v0

    .line 227
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 229
    const-string v2, "content"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 244
    const-string v2, "content"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 245
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/chaton/specialbuddy/g;->b(Lorg/json/JSONObject;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v0

    goto :goto_0

    .line 260
    :cond_1
    invoke-static {v1, p1}, Lcom/sec/chaton/specialbuddy/g;->b(Lorg/json/JSONObject;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v1

    .line 264
    sget-object v2, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Lorg/json/JSONObject;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 270
    if-nez p0, :cond_0

    .line 283
    :goto_0
    return-object v0

    .line 275
    :cond_0
    :try_start_0
    new-instance v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    invoke-direct {v1}, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;-><init>()V

    .line 276
    const-string v2, "name"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    .line 277
    if-nez p1, :cond_1

    .line 278
    const-string v2, "id"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->id:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move-object v0, v1

    .line 280
    goto :goto_0

    .line 281
    :catch_0
    move-exception v1

    .line 282
    sget-object v2, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 173
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    const-string v1, "ERROR In convertShareChatMessage(), emtpry input"

    sget-object v2, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :goto_0
    return-object v0

    .line 178
    :cond_0
    invoke-static {p0}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v1

    .line 179
    if-nez v1, :cond_1

    .line 180
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERROR In convertShareChatMessage(), cannot parse input : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :cond_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 187
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 188
    const-string v4, "name"

    iget-object v5, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 189
    const-string v4, "title"

    iget-object v5, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->title:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 190
    const-string v4, "id"

    iget-object v5, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->id:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 191
    const-string v4, "hash"

    iget-object v5, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->hash:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 192
    const-string v4, "url"

    iget-object v5, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->url:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 195
    iget-object v4, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->title:Ljava/lang/String;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->name:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/sec/chaton/specialbuddy/g;->b(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 198
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v4, :cond_2

    .line 199
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v4

    .line 200
    const-string v5, "convertShareChatMessage(), push_message bytes(%d), result : %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 201
    sget-object v5, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_2
    const-string v4, "type"

    const-string v5, "live_share"

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 206
    const-string v4, "format"

    const-string v5, "json"

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 207
    const-string v4, "push_message"

    invoke-virtual {v2, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 208
    const-string v1, "content"

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 210
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 211
    :catch_0
    move-exception v1

    .line 212
    sget-object v2, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 7

    .prologue
    const/16 v6, 0x62

    .line 125
    invoke-static {p0, p1}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 127
    invoke-static {v1}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v1

    .line 128
    if-lt v6, v1, :cond_1

    .line 168
    :cond_0
    return-object v0

    .line 133
    :cond_1
    const-string v0, ".."

    .line 134
    const-string v0, ".."

    invoke-static {v0}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v2

    .line 136
    invoke-static {p0}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v3

    .line 137
    invoke-static {p1}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v4

    .line 140
    add-int/lit8 v1, v1, -0x62

    .line 141
    sub-int v0, v4, v1

    .line 142
    if-le v0, v2, :cond_3

    .line 143
    const-string v5, ".."

    invoke-static {p1, v0, v5}, Lcom/sec/chaton/util/cl;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    :goto_0
    invoke-static {v0}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v5

    .line 149
    sub-int/2addr v4, v5

    .line 150
    sub-int/2addr v1, v4

    .line 151
    if-lez v1, :cond_2

    .line 152
    sub-int v1, v3, v1

    .line 153
    if-le v1, v2, :cond_4

    .line 154
    const-string v2, ".."

    invoke-static {p0, v1, v2}, Lcom/sec/chaton/util/cl;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 162
    :cond_2
    :goto_1
    invoke-static {p0, v0}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 164
    invoke-static {v1}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v1

    .line 165
    if-ge v6, v1, :cond_0

    .line 166
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "exceeds 100 bytes"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_3
    const-string v0, ".."

    invoke-static {p1, v2, v0}, Lcom/sec/chaton/util/cl;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 156
    :cond_4
    const-string v1, ".."

    invoke-static {p0, v2, v1}, Lcom/sec/chaton/util/cl;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method public static c(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/specialbuddy/g;->b(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 288
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 289
    const-string v1, "type"

    const-string v2, "live_recommend"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 290
    const-string v1, "name"

    invoke-virtual {v0, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 291
    return-object v0
.end method

.method public static e(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 4

    .prologue
    const/16 v3, 0x62

    .line 296
    invoke-static {p0}, Lcom/sec/chaton/specialbuddy/g;->d(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 297
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 298
    invoke-static {v1}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v1

    .line 299
    if-lt v3, v1, :cond_1

    .line 325
    :cond_0
    return-object v0

    .line 304
    :cond_1
    const-string v0, ".."

    .line 305
    const-string v0, ".."

    invoke-static {v0}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v0

    .line 306
    invoke-static {p0}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v2

    .line 309
    add-int/lit8 v1, v1, -0x62

    .line 310
    sub-int v1, v2, v1

    .line 311
    if-le v1, v0, :cond_2

    .line 312
    const-string v0, ".."

    invoke-static {p0, v1, v0}, Lcom/sec/chaton/util/cl;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 319
    :goto_0
    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/g;->d(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 321
    invoke-static {v1}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v1

    .line 322
    if-ge v3, v1, :cond_0

    .line 323
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "exceeds 100 bytes"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_2
    const-string v1, ".."

    invoke-static {p0, v0, v1}, Lcom/sec/chaton/util/cl;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 330
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    const-string v1, "ERROR In convertRecommendChatMessage(), emtpry input"

    sget-object v2, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :goto_0
    return-object v0

    .line 335
    :cond_0
    invoke-static {p0}, Lcom/sec/chaton/specialbuddy/g;->c(Ljava/lang/String;)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v1

    .line 336
    if-nez v1, :cond_1

    .line 337
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERROR In convertRecommendChatMessage(), cannot parse input : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 341
    :cond_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 344
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 345
    const-string v4, "id"

    iget-object v5, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->id:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 346
    const-string v4, "name"

    iget-object v5, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 349
    iget-object v1, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/chaton/specialbuddy/g;->e(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 352
    sget-boolean v4, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v4, :cond_2

    .line 353
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/chaton/util/cl;->a(Ljava/lang/String;)I

    move-result v4

    .line 354
    const-string v5, "convertRecommendChatMessage(), push_message byte(%d), result : %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 355
    sget-object v5, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :cond_2
    const-string v4, "type"

    const-string v5, "live_recommend"

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 360
    const-string v4, "format"

    const-string v5, "json"

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 361
    const-string v4, "push_message"

    invoke-virtual {v2, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 362
    const-string v1, "content"

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 364
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 365
    :catch_0
    move-exception v1

    .line 366
    sget-object v2, Lcom/sec/chaton/specialbuddy/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static g(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 373
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 393
    :cond_0
    :goto_0
    return-object v0

    .line 378
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 380
    const-string v2, "push_message"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 388
    const-string v0, "push_message"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 391
    :catch_0
    move-exception v0

    move-object v0, p0

    .line 393
    goto :goto_0
.end method
