.class public Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;
.super Lcom/sec/chaton/specialbuddy/AbstractChatONLiveActivity;
.source "ChatONLiveMainActivity.java"


# static fields
.field static final b:Ljava/lang/String;


# instance fields
.field c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveActivity;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->c:Z

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 18
    if-eqz v0, :cond_0

    const-string v1, "key_start_with_buddylist"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 19
    const-string v1, "key_start_with_buddylist"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->c:Z

    .line 22
    :cond_0
    new-instance v0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-direct {v0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->a:Lcom/sec/chaton/base/d;

    .line 28
    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 79
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->c:Z

    if-eqz v0, :cond_0

    .line 80
    invoke-static {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->a(Landroid/app/Activity;)V

    .line 82
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveActivity;->onResume()V

    .line 69
    const v0, 0x7f0b03c2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->setTitle(I)V

    .line 70
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->c:Z

    if-eqz v0, :cond_0

    .line 71
    invoke-static {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->a(Landroid/app/Activity;)V

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainActivity;->c()V

    .line 74
    return-void
.end method
