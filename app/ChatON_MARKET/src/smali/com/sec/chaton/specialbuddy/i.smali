.class Lcom/sec/chaton/specialbuddy/i;
.super Landroid/os/Handler;
.source "ChatONLiveMainFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->c(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {v0}, Lcom/sec/chaton/mobileweb/o;->a(I)Lcom/sec/chaton/mobileweb/o;

    move-result-object v1

    .line 70
    if-eqz v1, :cond_0

    .line 74
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 77
    invoke-virtual {v1}, Lcom/sec/chaton/mobileweb/o;->d()Ljava/lang/String;

    move-result-object v2

    .line 78
    const-string v3, "api(%s) with arguments : %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 80
    invoke-static {}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    sget-object v2, Lcom/sec/chaton/specialbuddy/k;->b:[I

    invoke-virtual {v1}, Lcom/sec/chaton/mobileweb/o;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 91
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->g()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/mobileweb/s;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 92
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-static {v2}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->d(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;)Landroid/os/Handler;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/chaton/mobileweb/a/a;->a(Landroid/os/Handler;Ljava/lang/String;)Lcom/sec/chaton/mobileweb/a/a;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/sec/chaton/mobileweb/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 98
    :pswitch_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/mobileweb/j;->a(Landroid/net/Uri;)Lcom/sec/chaton/mobileweb/j;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/j;->a()Lcom/sec/chaton/mobileweb/l;

    move-result-object v1

    .line 104
    sget-object v2, Lcom/sec/chaton/specialbuddy/k;->a:[I

    invoke-virtual {v1}, Lcom/sec/chaton/mobileweb/l;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 106
    :pswitch_2
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->f()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/mobileweb/j;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_0

    .line 109
    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/j;->b()Ljava/util/Map;

    move-result-object v0

    .line 110
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-static {v2, v7}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->a(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 111
    if-eqz v0, :cond_2

    .line 112
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    const-string v3, "buddyid"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->a(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 116
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 122
    :pswitch_3
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->f()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/mobileweb/j;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 123
    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/j;->b()Ljava/util/Map;

    move-result-object v0

    .line 126
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-static {v2, v7}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->a(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 127
    if-eqz v0, :cond_3

    .line 128
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    const-string v3, "buddyid"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->a(Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 132
    :cond_3
    const/high16 v0, 0x20000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/i;->a:Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/specialbuddy/ChatONLiveMainFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 139
    :pswitch_4
    const-string v0, "live_chat_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 160
    :pswitch_5
    const-string v1, "live"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-static {}, Lcom/sec/chaton/specialbuddy/g;->a()V

    goto/16 :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_5
    .end packed-switch

    .line 104
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
