.class public Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;
.super Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;
.source "SpecialBuddyChatFragment.java"

# interfaces
.implements Lcom/sec/chaton/chat/fi;


# static fields
.field private static final m:Ljava/lang/String;


# instance fields
.field private A:J

.field private B:Z

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Z

.field private final H:I

.field private final I:I

.field private final J:I

.field private final K:I

.field private final L:I

.field private final M:I

.field private final N:I

.field private final O:I

.field private final P:I

.field private final Q:I

.field private final R:I

.field private S:Z

.field private T:Z

.field private U:Landroid/os/Handler;

.field private V:Landroid/os/Handler;

.field private W:Landroid/os/Handler;

.field i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field k:Landroid/os/Handler;

.field l:Lcom/sec/chaton/e/a/v;

.field private n:Landroid/view/ViewGroup;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Lcom/sec/chaton/e/a/u;

.field private s:Lcom/sec/chaton/d/o;

.field private t:Z

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:I

.field private x:Ljava/lang/String;

.field private y:Lcom/sec/chaton/e/r;

.field private z:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const-class v0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 148
    sget-object v0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    sget-object v1, Lcom/sec/chaton/mobileweb/p;->a:Lcom/sec/chaton/mobileweb/p;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;-><init>(Ljava/lang/String;Lcom/sec/chaton/mobileweb/p;)V

    .line 104
    iput-boolean v3, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->t:Z

    .line 107
    iput-object v6, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u:Ljava/lang/String;

    .line 108
    iput-object v6, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    .line 109
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->w:I

    .line 110
    iput-object v6, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    .line 114
    iput-boolean v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->B:Z

    .line 119
    iput-boolean v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->G:Z

    .line 122
    iput v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->H:I

    .line 123
    iput v4, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->I:I

    .line 124
    iput v5, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->J:I

    .line 125
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->K:I

    .line 126
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->L:I

    .line 136
    iput v4, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->M:I

    .line 137
    iput v5, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->N:I

    .line 140
    iput v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->O:I

    .line 141
    iput v4, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->P:I

    .line 142
    iput v5, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->Q:I

    .line 143
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->R:I

    .line 145
    iput-boolean v3, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->S:Z

    .line 146
    iput-boolean v3, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->T:Z

    .line 156
    new-instance v0, Lcom/sec/chaton/specialbuddy/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/specialbuddy/m;-><init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->U:Landroid/os/Handler;

    .line 261
    new-instance v0, Lcom/sec/chaton/specialbuddy/o;

    invoke-direct {v0, p0}, Lcom/sec/chaton/specialbuddy/o;-><init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->V:Landroid/os/Handler;

    .line 682
    new-instance v0, Lcom/sec/chaton/specialbuddy/r;

    invoke-direct {v0, p0}, Lcom/sec/chaton/specialbuddy/r;-><init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k:Landroid/os/Handler;

    .line 801
    new-instance v0, Lcom/sec/chaton/specialbuddy/u;

    invoke-direct {v0, p0}, Lcom/sec/chaton/specialbuddy/u;-><init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->l:Lcom/sec/chaton/e/a/v;

    .line 990
    new-instance v0, Lcom/sec/chaton/specialbuddy/v;

    invoke-direct {v0, p0}, Lcom/sec/chaton/specialbuddy/v;-><init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->W:Landroid/os/Handler;

    .line 149
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;I)I
    .locals 0

    .prologue
    .line 87
    iput p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->w:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;J)J
    .locals 0

    .prologue
    .line 87
    iput-wide p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->A:J

    return-wide p1
.end method

.method private a(Landroid/content/Context;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1148
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/forward/ChatForwardActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1149
    const-string v1, "content_type"

    invoke-virtual {p2}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1150
    const-string v1, "inboxNO"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1151
    if-lez p5, :cond_0

    .line 1152
    const-string v1, "ACTIVITY_PURPOSE_ARG2"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1155
    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1156
    const-string v1, "download_uri"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1166
    :goto_0
    const-string v1, "is_forward_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1167
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1168
    return-object v0

    .line 1158
    :cond_1
    const-string v1, "download_uri"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1161
    const-string v1, "sub_content"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Landroid/content/Context;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 87
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Landroid/content/Context;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->z:Ljava/lang/Long;

    return-object p1
.end method

.method private a(Landroid/view/MenuItem;Z)V
    .locals 0

    .prologue
    .line 485
    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 486
    invoke-static {p1}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 487
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Lcom/sec/chaton/mobileweb/h;)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->b(Lcom/sec/chaton/mobileweb/h;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1181
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->o:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 1198
    :cond_0
    :goto_0
    return-void

    .line 1185
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    .line 1187
    if-eqz p1, :cond_3

    move v0, v1

    .line 1191
    :goto_1
    iget-object v4, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->o:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1193
    if-ne v3, v1, :cond_2

    if-ne v0, v1, :cond_2

    .line 1194
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 1196
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)Z
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->S:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->V:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Lcom/sec/chaton/mobileweb/h;)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->b(Lcom/sec/chaton/mobileweb/h;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)Z
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->G:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->C:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Lcom/sec/chaton/mobileweb/h;)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->b(Lcom/sec/chaton/mobileweb/h;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)Z
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->B:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/d/o;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->D:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)Z
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->T:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic e(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Z)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Z)V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/e/r;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->y:Lcom/sec/chaton/e/r;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic g(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->r()V

    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->t:Z

    return v0
.end method

.method static synthetic i(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k()Z

    move-result v0

    return v0
.end method

.method static synthetic j(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->W:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->r:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v()Z

    move-result v0

    return v0
.end method

.method static synthetic n(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->D:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u:Ljava/lang/String;

    return-object v0
.end method

.method private q()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 311
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 312
    const-string v0, "inbox_unread_count"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->r:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x4

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inbox_no=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 314
    return-void
.end method

.method static synthetic q(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->B:Z

    return v0
.end method

.method static synthetic r(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->z:Ljava/lang/Long;

    return-object v0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 431
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v0

    .line 432
    if-nez v0, :cond_1

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    instance-of v1, v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    if-eqz v1, :cond_0

    .line 437
    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0
.end method

.method static synthetic s(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->C:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)J
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->A:J

    return-wide v0
.end method

.method private t()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 602
    const-string v0, "spbd_refresh, askInboxNoAsync(), #1"

    sget-object v3, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->r:Lcom/sec/chaton/e/a/u;

    invoke-static {}, Lcom/sec/chaton/e/q;->g()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "buddy_no=?"

    const/4 v4, 0x3

    new-array v6, v4, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u:Ljava/lang/String;

    aput-object v4, v6, v7

    aput-object v2, v6, v1

    const/4 v4, 0x2

    sget-object v7, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v7}, Lcom/sec/chaton/e/r;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    :goto_0
    return-void

    .line 608
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->r:Lcom/sec/chaton/e/a/u;

    invoke-static {}, Lcom/sec/chaton/e/q;->a()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "inbox_no=?"

    new-array v6, v1, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private u()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 613
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k()Z

    move-result v2

    if-nez v2, :cond_0

    .line 637
    :goto_0
    return v0

    .line 617
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v2

    .line 618
    const/4 v3, -0x3

    if-eq v3, v2, :cond_1

    const/4 v3, -0x2

    if-ne v3, v2, :cond_2

    .line 619
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0b0205

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 620
    goto :goto_0

    .line 622
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 623
    const v2, 0x7f0b000e

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 624
    const v2, 0x7f0b03f2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 625
    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v2

    const v3, 0x7f0b0037

    new-instance v4, Lcom/sec/chaton/specialbuddy/q;

    invoke-direct {v4, p0}, Lcom/sec/chaton/specialbuddy/q;-><init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v2

    const v3, 0x7f0b0039

    new-instance v4, Lcom/sec/chaton/specialbuddy/p;

    invoke-direct {v4, p0}, Lcom/sec/chaton/specialbuddy/p;-><init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 636
    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0
.end method

.method static synthetic u(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->T:Z

    return v0
.end method

.method private v()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 661
    iget-boolean v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->t:Z

    if-eqz v1, :cond_1

    .line 678
    :cond_0
    :goto_0
    return v0

    .line 664
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/chat/notification/a;->b(Ljava/lang/String;)V

    .line 665
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->y:Lcom/sec/chaton/e/r;

    invoke-static {v1, v2}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    .line 666
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    if-eqz v1, :cond_0

    .line 669
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->a(Landroid/os/Handler;)Z

    .line 671
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->c(Ljava/lang/String;)V

    .line 674
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 678
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic v(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->G:Z

    return v0
.end method

.method static synthetic w(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->q()V

    return-void
.end method

.method static synthetic x(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k()Z

    move-result v0

    return v0
.end method

.method static synthetic y(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)Z
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/l;)Lcom/sec/common/a/a;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const v3, 0x7f0b0037

    .line 774
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k()Z

    move-result v1

    if-nez v1, :cond_1

    .line 798
    :cond_0
    :goto_0
    return-object v0

    .line 778
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 780
    sget-object v2, Lcom/sec/chaton/a/a/l;->i:Lcom/sec/chaton/a/a/l;

    if-ne p1, v2, :cond_2

    .line 781
    const v0, 0x7f0b0195

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/specialbuddy/s;

    invoke-direct {v2, p0}, Lcom/sec/chaton/specialbuddy/s;-><init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    invoke-virtual {v0, v3, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-object v0, v1

    .line 787
    goto :goto_0

    .line 788
    :cond_2
    sget-object v2, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    if-ne p1, v2, :cond_0

    .line 789
    const v0, 0x7f0b00c8

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/chaton/specialbuddy/t;

    invoke-direct {v2, p0}, Lcom/sec/chaton/specialbuddy/t;-><init>(Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;)V

    invoke-virtual {v0, v3, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-object v0, v1

    .line 795
    goto :goto_0
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1012
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1013
    :cond_0
    const/4 v0, 0x0

    .line 1054
    :goto_0
    return-object v0

    .line 1017
    :cond_1
    const-string v0, "{LIVE_MOBILEWEB}"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1019
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->h()Lcom/sec/chaton/mobileweb/p;

    move-result-object v0

    .line 1020
    invoke-virtual {v0}, Lcom/sec/chaton/mobileweb/p;->a()Ljava/lang/String;

    move-result-object v0

    .line 1022
    const-string v1, "{LIVE_MOBILEWEB}"

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 1034
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "specialuserid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1035
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "myid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1038
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->F:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1039
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1046
    :goto_1
    const-string v1, "?"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1047
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1053
    :goto_2
    const-string v1, "startWebContents(), requestUrl: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1041
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1049
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)V
    .locals 0

    .prologue
    .line 1107
    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1060
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_0

    .line 1082
    :goto_0
    return-void

    .line 1063
    :cond_0
    invoke-static {p5}, Lcom/sec/chaton/e/r;->b(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "null"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1065
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 1066
    invoke-static {v1, p5}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v0

    .line 1067
    if-eqz v0, :cond_2

    .line 1068
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/o;->a(Landroid/os/Handler;)Z

    .line 1069
    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 1070
    invoke-virtual {v0, v8}, Lcom/sec/chaton/d/o;->a(Z)V

    .line 1071
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->e(Ljava/lang/String;)V

    .line 1073
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/d/o;->c(J)V

    .line 1074
    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    iget-object v3, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->z:Ljava/lang/Long;

    iget-object v5, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    iget-wide v6, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->A:J

    move-object v2, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;JZ)Z

    .line 1081
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j()V

    goto :goto_0

    .line 1077
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 1078
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 1079
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    iget-object v3, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->z:Ljava/lang/Long;

    iget-object v5, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    iget-wide v6, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->A:J

    move-object v1, p5

    move-object v2, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v8}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;JZ)Z

    goto :goto_1
.end method

.method protected b()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->U:Landroid/os/Handler;

    return-object v0
.end method

.method protected d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->q:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 1178
    :goto_0
    return-void

    .line 1176
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->q:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public n()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 641
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    :goto_0
    return-void

    .line 645
    :cond_0
    iget v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->w:I

    if-ltz v0, :cond_1

    .line 651
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 652
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->r:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x2

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "inbox_no IN (\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 653
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->r:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x3

    sget-object v3, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "message_inbox_no=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 656
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 657
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method protected o()Ljava/lang/Boolean;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1110
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1137
    :cond_0
    :goto_0
    return-object v5

    .line 1114
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1118
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1119
    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "inbox_enable_noti"

    aput-object v3, v2, v6

    const-string v3, "inbox_no=?"

    new-array v4, v7, [Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    aput-object v8, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1120
    if-eqz v0, :cond_0

    .line 1123
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1124
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1128
    :cond_3
    const-string v1, "inbox_enable_noti"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1129
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1132
    const-string v0, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v6

    .line 1137
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    :cond_4
    move v0, v7

    .line 1135
    goto :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 318
    invoke-super {p0, p1}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 319
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 320
    new-instance v1, Lcom/sec/chaton/e/a/u;

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->l:Lcom/sec/chaton/e/a/v;

    invoke-direct {v1, v0, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->r:Lcom/sec/chaton/e/a/u;

    .line 322
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 323
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    const-string v2, "inboxNO"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 333
    const-string v2, "inboxNO"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    .line 336
    :cond_2
    const-string v2, "receivers"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 337
    const-string v2, "receivers"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 338
    array-length v3, v2

    if-lt v3, v6, :cond_3

    .line 339
    aget-object v2, v2, v5

    iput-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u:Ljava/lang/String;

    .line 343
    :cond_3
    const-string v2, "chatType"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 344
    const-string v2, "chatType"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->y:Lcom/sec/chaton/e/r;

    .line 347
    :cond_4
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_5

    .line 348
    const-string v2, "onActivityCreated(), mInboxNo(%s), mChatType(%s), mBuddyNo(%s)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->y:Lcom/sec/chaton/e/r;

    aput-object v4, v3, v6

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 350
    sget-object v3, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_5
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 355
    const-string v2, "key_buddy_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 356
    const-string v2, "key_buddy_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    .line 357
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hslee_title, BuddyName intent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 363
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/e/a/af;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    .line 364
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hslee_title, BuddyName from following DB : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 369
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/af;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hslee_title, BuddyName from recommendee DB : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->D:Ljava/lang/String;

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 378
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hslee_title, setTitle() #1 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->D:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->D:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 498
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0068

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v4, v3, v3, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02030d

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 499
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v4, v2, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020313

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 501
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v4, v5, v5, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f020304

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 505
    iget-boolean v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->S:Z

    if-ne v1, v3, :cond_0

    iget-boolean v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->T:Z

    if-ne v1, v3, :cond_0

    .line 506
    invoke-direct {p0, v0, v3}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Landroid/view/MenuItem;Z)V

    .line 512
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 513
    return-void

    .line 508
    :cond_0
    invoke-direct {p0, v0, v4}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Landroid/view/MenuItem;Z)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 269
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 272
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v1

    .line 273
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 274
    const v3, 0x7f030005

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    move-object v0, v1

    .line 275
    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/common/actionbar/a;->a(Landroid/view/View;)V

    .line 276
    const v0, 0x7f07001f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->q:Landroid/widget/TextView;

    .line 277
    const v0, 0x7f070020

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->n:Landroid/view/ViewGroup;

    .line 278
    const v0, 0x7f070021

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->p:Landroid/widget/TextView;

    .line 279
    const v0, 0x7f070022

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->o:Landroid/widget/ImageView;

    .line 285
    check-cast v1, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v1}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->e(I)V

    .line 288
    const v0, 0x7f0b0292

    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->i:Ljava/lang/String;

    .line 289
    const v0, 0x7f0b0293

    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j:Ljava/lang/String;

    .line 292
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 295
    const-string v1, "key_intent_ctid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->F:Ljava/lang/String;

    .line 296
    const-string v1, "key_web_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->E:Ljava/lang/String;

    .line 298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onCreateView(), mCtid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / mForwardedWebURL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    return-object v2
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 417
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 418
    const-string v0, "onDestroyView()"

    sget-object v1, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->U:Landroid/os/Handler;

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_1

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->b(Landroid/os/Handler;)Z

    .line 426
    :cond_1
    invoke-super {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onDestroyView()V

    .line 427
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 428
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 517
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->k()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    .line 576
    :goto_0
    return v0

    .line 526
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 576
    invoke-super {p0, p1}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 529
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v6

    .line 530
    goto :goto_0

    .line 532
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 533
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->i:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 534
    iput-boolean v6, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->G:Z

    .line 538
    :cond_3
    :goto_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->G:Z

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)I

    .line 539
    iget-boolean v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->G:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Z)V

    move v0, v6

    .line 541
    goto :goto_0

    .line 535
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 536
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->G:Z

    goto :goto_1

    .line 548
    :pswitch_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 550
    :try_start_0
    const-string v1, "id"

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 551
    const-string v1, "name"

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 552
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const v5, 0x7f0b03c3

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Landroid/content/Context;Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move v0, v6

    .line 556
    goto :goto_0

    .line 553
    :catch_0
    move-exception v0

    .line 554
    sget-object v1, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 559
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u()Z

    move v0, v6

    .line 560
    goto/16 :goto_0

    .line 563
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/SpecialBuddyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 564
    const-string v1, "specialuserid"

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 566
    const-string v1, "speicalusername"

    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 568
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->f()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    move v0, v6

    .line 572
    goto/16 :goto_0

    .line 569
    :catch_1
    move-exception v0

    .line 570
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_3

    .line 526
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onPause()V
    .locals 5

    .prologue
    .line 400
    invoke-super {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onPause()V

    .line 401
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->t:Z

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_0

    .line 404
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 405
    iget-object v2, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->s:Lcom/sec/chaton/d/o;

    const-wide/32 v3, 0x75300

    add-long/2addr v0, v3

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/d/o;->c(J)V

    .line 410
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/notification/a;->c(Ljava/lang/String;)V

    .line 411
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/receiver/PushReceiver;->a(Lcom/sec/chaton/chat/fi;)V

    .line 413
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    .line 461
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 462
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 463
    :cond_0
    const v0, 0x7f0b0292

    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->i:Ljava/lang/String;

    .line 464
    const v0, 0x7f0b0293

    invoke-virtual {p0, v0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j:Ljava/lang/String;

    .line 468
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->o()Ljava/lang/Boolean;

    move-result-object v1

    .line 469
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 470
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->j:Ljava/lang/String;

    invoke-interface {p1, v4, v3, v3, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v2, 0x7f0202fc

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 475
    :goto_0
    if-nez v1, :cond_4

    .line 476
    invoke-direct {p0, v0, v4}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Landroid/view/MenuItem;Z)V

    .line 481
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 482
    return-void

    .line 472
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->i:Ljava/lang/String;

    invoke-interface {p1, v4, v3, v3, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v2, 0x7f0202fd

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_0

    .line 478
    :cond_4
    iget-boolean v1, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->S:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->a(Landroid/view/MenuItem;Z)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 385
    invoke-super {p0}, Lcom/sec/chaton/specialbuddy/AbstractChatONLiveFragment;->onResume()V

    .line 386
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->t:Z

    .line 389
    invoke-static {}, Lcom/sec/common/util/i;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    invoke-virtual {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->l()V

    .line 396
    :goto_0
    return-void

    .line 394
    :cond_0
    invoke-static {p0}, Lcom/sec/chaton/receiver/PushReceiver;->a(Lcom/sec/chaton/chat/fi;)V

    .line 395
    invoke-direct {p0}, Lcom/sec/chaton/specialbuddy/SpecialBuddyChatFragment;->t()V

    goto :goto_0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 1086
    const/4 v0, 0x0

    return v0
.end method
