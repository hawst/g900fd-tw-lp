.class Lcom/sec/chaton/ad;
.super Ljava/lang/Object;
.source "PlusFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/PlusFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/PlusFragment;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 436
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 424
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    .line 365
    if-nez p1, :cond_8

    .line 367
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->c(Lcom/sec/chaton/PlusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 368
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->d(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 370
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->e(Lcom/sec/chaton/PlusFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->f(Lcom/sec/chaton/PlusFragment;)Lcom/sec/common/f/c;

    move-result-object v0

    if-nez v0, :cond_5

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->f(Lcom/sec/chaton/PlusFragment;)Lcom/sec/common/f/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->f(Lcom/sec/chaton/PlusFragment;)Lcom/sec/common/f/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    new-instance v1, Lcom/sec/common/f/c;

    invoke-direct {v1}, Lcom/sec/common/f/c;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/chaton/PlusFragment;->a(Lcom/sec/chaton/PlusFragment;Lcom/sec/common/f/c;)Lcom/sec/common/f/c;

    .line 377
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    new-instance v1, Lcom/sec/chaton/settings/moreapps/a;

    iget-object v2, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v2}, Lcom/sec/chaton/PlusFragment;->c(Lcom/sec/chaton/PlusFragment;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v3}, Lcom/sec/chaton/PlusFragment;->g(Lcom/sec/chaton/PlusFragment;)Lcom/sec/chaton/PlusFragment;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v4}, Lcom/sec/chaton/PlusFragment;->d(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v5}, Lcom/sec/chaton/PlusFragment;->f(Lcom/sec/chaton/PlusFragment;)Lcom/sec/common/f/c;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/chaton/settings/moreapps/a;-><init>(Landroid/content/Context;Lcom/sec/chaton/PlusFragment;Ljava/util/ArrayList;Lcom/sec/common/f/c;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/PlusFragment;->a(Lcom/sec/chaton/PlusFragment;Lcom/sec/chaton/settings/moreapps/a;)Lcom/sec/chaton/settings/moreapps/a;

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->i(Lcom/sec/chaton/PlusFragment;)Lcom/sec/widget/ExpandableHeightGridView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v1}, Lcom/sec/chaton/PlusFragment;->h(Lcom/sec/chaton/PlusFragment;)Lcom/sec/chaton/settings/moreapps/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/widget/ExpandableHeightGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 385
    :goto_0
    if-eqz p3, :cond_7

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/PlusFragment;->a(Lcom/sec/chaton/PlusFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 388
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    iget-object v1, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v1}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/PlusFragment;->a(Lcom/sec/chaton/PlusFragment;I)I

    .line 390
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 391
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v1}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v1

    const-string v2, "id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 392
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v2}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v2

    const-string v3, "type"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 393
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v3}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "title"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 394
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v4}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v4

    const-string v5, "contenturl"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 395
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v5, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v5}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v5

    const-string v6, "appid"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 396
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v6, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v6}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v6

    const-string v7, "linkurl"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 397
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v7, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v7}, Lcom/sec/chaton/PlusFragment;->j(Lcom/sec/chaton/PlusFragment;)Landroid/database/Cursor;

    move-result-object v7

    const-string v8, "samsungappsurl"

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 399
    const/4 v0, 0x1

    if-ne v2, v0, :cond_6

    .line 400
    new-instance v0, Lcom/sec/chaton/aj;

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/aj;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/sec/chaton/util/cb;->c()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/sec/chaton/util/cb;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 403
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v1}, Lcom/sec/chaton/PlusFragment;->d(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 381
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/PlusFragment;->a(Lcom/sec/chaton/PlusFragment;Z)Z

    goto/16 :goto_0

    .line 406
    :cond_6
    const/4 v0, 0x2

    if-ne v2, v0, :cond_2

    .line 407
    new-instance v0, Lcom/sec/chaton/aj;

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/aj;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    iget-object v1, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v1}, Lcom/sec/chaton/PlusFragment;->d(Lcom/sec/chaton/PlusFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 413
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->h(Lcom/sec/chaton/PlusFragment;)Lcom/sec/chaton/settings/moreapps/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/settings/moreapps/a;->notifyDataSetChanged()V

    .line 415
    iget-object v0, p0, Lcom/sec/chaton/ad;->a:Lcom/sec/chaton/PlusFragment;

    invoke-static {v0}, Lcom/sec/chaton/PlusFragment;->k(Lcom/sec/chaton/PlusFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 418
    :cond_8
    return-void
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 430
    return-void
.end method
