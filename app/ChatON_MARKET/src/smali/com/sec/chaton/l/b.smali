.class public final Lcom/sec/chaton/l/b;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final actionBarDivider:I = 0x7f010006

.field public static final actionBarSize:I = 0x7f010004

.field public static final actionBarStyle:I = 0x7f010000

.field public static final actionBarTabBarStyle:I = 0x7f010002

.field public static final actionBarTabStyle:I = 0x7f010001

.field public static final actionBarTabTextStyle:I = 0x7f010003

.field public static final actionButtonStyle:I = 0x7f010005

.field public static final actionDropDownStyle:I = 0x7f01000b

.field public static final actionMenuTextAppearance:I = 0x7f010009

.field public static final actionMenuTextColor:I = 0x7f01000a

.field public static final actionbarBackground:I = 0x7f01002e

.field public static final actionbarItemBackground:I = 0x7f010031

.field public static final actionbarItemStyle:I = 0x7f010030

.field public static final actionbarNavigationTabWidgetStyle:I = 0x7f010032

.field public static final actionbarTitleStyle:I = 0x7f01002f

.field public static final alertDialogStyle:I = 0x7f010010

.field public static final alertDialogTheme:I = 0x7f01000f

.field public static final backgroundStacked:I = 0x7f010022

.field public static final bottomBright:I = 0x7f01002a

.field public static final bottomDark:I = 0x7f010026

.field public static final bottomMedium:I = 0x7f01002b

.field public static final buttonBarButtonStyle:I = 0x7f010014

.field public static final buttonBarStyle:I = 0x7f010013

.field public static final buttonLayoutBackground:I = 0x7f010033

.field public static final centerBright:I = 0x7f010029

.field public static final centerDark:I = 0x7f010025

.field public static final centerMedium:I = 0x7f01002c

.field public static final checkable_widget:I = 0x7f0100b1

.field public static final clearableEditTextStyle:I = 0x7f010038

.field public static final disabledTextAppearance:I = 0x7f0100b0

.field public static final displayOptions:I = 0x7f01001f

.field public static final dropDownListViewStyle:I = 0x7f01000e

.field public static final firstIndicator:I = 0x7f010018

.field public static final focusedTextAppearance:I = 0x7f0100af

.field public static final ftBar01Color:I = 0x7f01003a

.field public static final ftBar02Color:I = 0x7f01003b

.field public static final ftBar03Color:I = 0x7f01003c

.field public static final ftBar04Color:I = 0x7f01003d

.field public static final ftBar04DisabledColor:I = 0x7f01003e

.field public static final ftBar05Color:I = 0x7f01003f

.field public static final ftBar05DisabledColor:I = 0x7f010040

.field public static final ftBar06Color:I = 0x7f010041

.field public static final ftBar07Color:I = 0x7f010042

.field public static final ftBtn01Color:I = 0x7f010074

.field public static final ftBtn02Color:I = 0x7f010075

.field public static final ftBtn03Color:I = 0x7f010076

.field public static final ftBtn04Color:I = 0x7f010077

.field public static final ftBtn05Color:I = 0x7f010078

.field public static final ftBtn09Color:I = 0x7f010079

.field public static final ftChat01Color:I = 0x7f010088

.field public static final ftChat02Color:I = 0x7f010089

.field public static final ftChat03Color:I = 0x7f01008a

.field public static final ftChat04Color:I = 0x7f01008b

.field public static final ftChat05Color:I = 0x7f01008c

.field public static final ftChat06Color:I = 0x7f01008d

.field public static final ftChat08Color:I = 0x7f01008e

.field public static final ftChat09Color:I = 0x7f01008f

.field public static final ftChat10Color:I = 0x7f010090

.field public static final ftChat11Color:I = 0x7f010091

.field public static final ftChat12Color:I = 0x7f010092

.field public static final ftChat13Color:I = 0x7f010093

.field public static final ftChat14Color:I = 0x7f010094

.field public static final ftChat15Color:I = 0x7f010095

.field public static final ftChat16Color:I = 0x7f010096

.field public static final ftChat17Color:I = 0x7f010097

.field public static final ftChat18Color:I = 0x7f010098

.field public static final ftChat21Color:I = 0x7f01009b

.field public static final ftChat29Color:I = 0x7f010099

.field public static final ftChat30Color:I = 0x7f01009a

.field public static final ftChat31Color:I = 0x7f01009c

.field public static final ftEtc01Color:I = 0x7f01007a

.field public static final ftEtc02Color:I = 0x7f01007b

.field public static final ftEtc03Color:I = 0x7f01007c

.field public static final ftEtc05Color:I = 0x7f01007d

.field public static final ftEtc06Color:I = 0x7f01007e

.field public static final ftEtc07Color:I = 0x7f01007f

.field public static final ftEtc08Color:I = 0x7f010080

.field public static final ftEtc09Color:I = 0x7f010081

.field public static final ftEtc10Color:I = 0x7f010082

.field public static final ftEtc12Color:I = 0x7f010083

.field public static final ftEtc15Color:I = 0x7f010084

.field public static final ftEtc16Color:I = 0x7f010085

.field public static final ftEtc17Color:I = 0x7f010086

.field public static final ftEtc20Color:I = 0x7f010087

.field public static final ftExt01Color:I = 0x7f0100ab

.field public static final ftList01Color:I = 0x7f010043

.field public static final ftList02Color:I = 0x7f010044

.field public static final ftList03Color:I = 0x7f010045

.field public static final ftList04Color:I = 0x7f010046

.field public static final ftList05Color:I = 0x7f010047

.field public static final ftList07Color:I = 0x7f010048

.field public static final ftList08Color:I = 0x7f010049

.field public static final ftList09Color:I = 0x7f01004a

.field public static final ftList10Color:I = 0x7f01004b

.field public static final ftList11Color:I = 0x7f01004c

.field public static final ftList12Color:I = 0x7f01004d

.field public static final ftList14Color:I = 0x7f01004e

.field public static final ftList15Color:I = 0x7f01004f

.field public static final ftList17Color:I = 0x7f010050

.field public static final ftList18Color:I = 0x7f010051

.field public static final ftList19Color:I = 0x7f010052

.field public static final ftList20Color:I = 0x7f010053

.field public static final ftList21Color:I = 0x7f010054

.field public static final ftList23Color:I = 0x7f010055

.field public static final ftList24Color:I = 0x7f010056

.field public static final ftList25Color:I = 0x7f010057

.field public static final ftList27Color:I = 0x7f010058

.field public static final ftList28Color:I = 0x7f010059

.field public static final ftList29Color:I = 0x7f01005a

.field public static final ftList30Color:I = 0x7f01005b

.field public static final ftList31Color:I = 0x7f01005c

.field public static final ftList32Color:I = 0x7f01005d

.field public static final ftList33Color:I = 0x7f01005e

.field public static final ftList34Color:I = 0x7f01005f

.field public static final ftList35Color:I = 0x7f010060

.field public static final ftList36Color:I = 0x7f010061

.field public static final ftList37Color:I = 0x7f010062

.field public static final ftList37DisabledColor:I = 0x7f010063

.field public static final ftList38Color:I = 0x7f010064

.field public static final ftList39Color:I = 0x7f010065

.field public static final ftList40Color:I = 0x7f010066

.field public static final ftList41Color:I = 0x7f010067

.field public static final ftList42Color:I = 0x7f010068

.field public static final ftList43Color:I = 0x7f010069

.field public static final ftList44Color:I = 0x7f01006a

.field public static final ftList49Color:I = 0x7f01006b

.field public static final ftList50Color:I = 0x7f01006c

.field public static final ftList51Color:I = 0x7f01006d

.field public static final ftList52Color:I = 0x7f01006e

.field public static final ftList53Color:I = 0x7f01006f

.field public static final ftList54Color:I = 0x7f010070

.field public static final ftList55Color:I = 0x7f010071

.field public static final ftList57Color:I = 0x7f010072

.field public static final ftList58Color:I = 0x7f010073

.field public static final ftPop01Color:I = 0x7f01009d

.field public static final ftPop02Color:I = 0x7f01009e

.field public static final ftPop03Color:I = 0x7f01009f

.field public static final ftPop04Color:I = 0x7f0100a0

.field public static final ftPop05Color:I = 0x7f0100a1

.field public static final ftPop06Color:I = 0x7f0100a2

.field public static final ftPop07Color:I = 0x7f0100a3

.field public static final ftPop08Color:I = 0x7f0100a4

.field public static final ftPop09Color:I = 0x7f0100a5

.field public static final ftPop10Color:I = 0x7f0100a6

.field public static final ftPop11Color:I = 0x7f0100a7

.field public static final ftPop12Color:I = 0x7f0100a8

.field public static final ftPop13Color:I = 0x7f0100a9

.field public static final ftPop14Color:I = 0x7f0100aa

.field public static final fullBright:I = 0x7f010027

.field public static final fullDark:I = 0x7f010023

.field public static final height:I = 0x7f01001d

.field public static final homeAsUpIndicator:I = 0x7f010007

.field public static final imageTextViewGroupStyle:I = 0x7f010037

.field public static final imageViewSelector:I = 0x7f0100b5

.field public static final indicator:I = 0x7f010015

.field public static final indicatorMargin:I = 0x7f01001a

.field public static final itemBackground:I = 0x7f010039

.field public static final lastIndicator:I = 0x7f010019

.field public static final layout:I = 0x7f0100b4

.field public static final leftIndicator:I = 0x7f010016

.field public static final listDividerAlertDialog:I = 0x7f010011

.field public static final listPopupWindowStyle:I = 0x7f01000d

.field public static final logo:I = 0x7f01001e

.field public static final maxIndicatorCount:I = 0x7f01001b

.field public static final normalTextAppearance:I = 0x7f0100ac

.field public static final pressedTextAppearance:I = 0x7f0100ae

.field public static final rightIndicator:I = 0x7f010017

.field public static final selectAllLayoutBackground:I = 0x7f010034

.field public static final selectableItemBackground:I = 0x7f010008

.field public static final selectedTextAppearance:I = 0x7f0100ad

.field public static final spinnerDropDownItemStyle:I = 0x7f01000c

.field public static final src:I = 0x7f0100b2

.field public static final srcPadding:I = 0x7f0100b3

.field public static final stateButtonStyle:I = 0x7f010036

.field public static final stateTextViewStyle:I = 0x7f010035

.field public static final subtitleTextStyle:I = 0x7f010021

.field public static final textColorAlertDialogListItem:I = 0x7f010012

.field public static final titleTextStyle:I = 0x7f010020

.field public static final topBright:I = 0x7f010028

.field public static final topDark:I = 0x7f010024

.field public static final windowBackgroundColor:I = 0x7f01002d

.field public static final windowNoTitle:I = 0x7f01001c
