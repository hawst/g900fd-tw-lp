.class public final Lcom/sec/chaton/l/g;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final ActionBarCompat:[I

.field public static final ActionBarCompat_backgroundStacked:I = 0x6

.field public static final ActionBarCompat_displayOptions:I = 0x3

.field public static final ActionBarCompat_height:I = 0x1

.field public static final ActionBarCompat_logo:I = 0x2

.field public static final ActionBarCompat_subtitleTextStyle:I = 0x5

.field public static final ActionBarCompat_titleTextStyle:I = 0x4

.field public static final ActionBarCompat_windowNoTitle:I = 0x0

.field public static final AlertDialogCompat:[I

.field public static final AlertDialogCompat_bottomBright:I = 0x7

.field public static final AlertDialogCompat_bottomDark:I = 0x3

.field public static final AlertDialogCompat_bottomMedium:I = 0x8

.field public static final AlertDialogCompat_centerBright:I = 0x6

.field public static final AlertDialogCompat_centerDark:I = 0x2

.field public static final AlertDialogCompat_centerMedium:I = 0x9

.field public static final AlertDialogCompat_fullBright:I = 0x4

.field public static final AlertDialogCompat_fullDark:I = 0x0

.field public static final AlertDialogCompat_topBright:I = 0x5

.field public static final AlertDialogCompat_topDark:I = 0x1

.field public static final AppTheme:[I

.field public static final AppTheme_actionbarBackground:I = 0x1

.field public static final AppTheme_actionbarItemBackground:I = 0x4

.field public static final AppTheme_actionbarItemStyle:I = 0x3

.field public static final AppTheme_actionbarNavigationTabWidgetStyle:I = 0x5

.field public static final AppTheme_actionbarTitleStyle:I = 0x2

.field public static final AppTheme_buttonLayoutBackground:I = 0x6

.field public static final AppTheme_clearableEditTextStyle:I = 0xb

.field public static final AppTheme_ftBar01Color:I = 0xd

.field public static final AppTheme_ftBar02Color:I = 0xe

.field public static final AppTheme_ftBar03Color:I = 0xf

.field public static final AppTheme_ftBar04Color:I = 0x10

.field public static final AppTheme_ftBar04DisabledColor:I = 0x11

.field public static final AppTheme_ftBar05Color:I = 0x12

.field public static final AppTheme_ftBar05DisabledColor:I = 0x13

.field public static final AppTheme_ftBar06Color:I = 0x14

.field public static final AppTheme_ftBar07Color:I = 0x15

.field public static final AppTheme_ftBtn01Color:I = 0x47

.field public static final AppTheme_ftBtn02Color:I = 0x48

.field public static final AppTheme_ftBtn03Color:I = 0x49

.field public static final AppTheme_ftBtn04Color:I = 0x4a

.field public static final AppTheme_ftBtn05Color:I = 0x4b

.field public static final AppTheme_ftBtn09Color:I = 0x4c

.field public static final AppTheme_ftChat01Color:I = 0x5b

.field public static final AppTheme_ftChat02Color:I = 0x5c

.field public static final AppTheme_ftChat03Color:I = 0x5d

.field public static final AppTheme_ftChat04Color:I = 0x5e

.field public static final AppTheme_ftChat05Color:I = 0x5f

.field public static final AppTheme_ftChat06Color:I = 0x60

.field public static final AppTheme_ftChat08Color:I = 0x61

.field public static final AppTheme_ftChat09Color:I = 0x62

.field public static final AppTheme_ftChat10Color:I = 0x63

.field public static final AppTheme_ftChat11Color:I = 0x64

.field public static final AppTheme_ftChat12Color:I = 0x65

.field public static final AppTheme_ftChat13Color:I = 0x66

.field public static final AppTheme_ftChat14Color:I = 0x67

.field public static final AppTheme_ftChat15Color:I = 0x68

.field public static final AppTheme_ftChat16Color:I = 0x69

.field public static final AppTheme_ftChat17Color:I = 0x6a

.field public static final AppTheme_ftChat18Color:I = 0x6b

.field public static final AppTheme_ftChat21Color:I = 0x6e

.field public static final AppTheme_ftChat29Color:I = 0x6c

.field public static final AppTheme_ftChat30Color:I = 0x6d

.field public static final AppTheme_ftChat31Color:I = 0x6f

.field public static final AppTheme_ftEtc01Color:I = 0x4d

.field public static final AppTheme_ftEtc02Color:I = 0x4e

.field public static final AppTheme_ftEtc03Color:I = 0x4f

.field public static final AppTheme_ftEtc05Color:I = 0x50

.field public static final AppTheme_ftEtc06Color:I = 0x51

.field public static final AppTheme_ftEtc07Color:I = 0x52

.field public static final AppTheme_ftEtc08Color:I = 0x53

.field public static final AppTheme_ftEtc09Color:I = 0x54

.field public static final AppTheme_ftEtc10Color:I = 0x55

.field public static final AppTheme_ftEtc12Color:I = 0x56

.field public static final AppTheme_ftEtc15Color:I = 0x57

.field public static final AppTheme_ftEtc16Color:I = 0x58

.field public static final AppTheme_ftEtc17Color:I = 0x59

.field public static final AppTheme_ftEtc20Color:I = 0x5a

.field public static final AppTheme_ftExt01Color:I = 0x7e

.field public static final AppTheme_ftList01Color:I = 0x16

.field public static final AppTheme_ftList02Color:I = 0x17

.field public static final AppTheme_ftList03Color:I = 0x18

.field public static final AppTheme_ftList04Color:I = 0x19

.field public static final AppTheme_ftList05Color:I = 0x1a

.field public static final AppTheme_ftList07Color:I = 0x1b

.field public static final AppTheme_ftList08Color:I = 0x1c

.field public static final AppTheme_ftList09Color:I = 0x1d

.field public static final AppTheme_ftList10Color:I = 0x1e

.field public static final AppTheme_ftList11Color:I = 0x1f

.field public static final AppTheme_ftList12Color:I = 0x20

.field public static final AppTheme_ftList14Color:I = 0x21

.field public static final AppTheme_ftList15Color:I = 0x22

.field public static final AppTheme_ftList17Color:I = 0x23

.field public static final AppTheme_ftList18Color:I = 0x24

.field public static final AppTheme_ftList19Color:I = 0x25

.field public static final AppTheme_ftList20Color:I = 0x26

.field public static final AppTheme_ftList21Color:I = 0x27

.field public static final AppTheme_ftList23Color:I = 0x28

.field public static final AppTheme_ftList24Color:I = 0x29

.field public static final AppTheme_ftList25Color:I = 0x2a

.field public static final AppTheme_ftList27Color:I = 0x2b

.field public static final AppTheme_ftList28Color:I = 0x2c

.field public static final AppTheme_ftList29Color:I = 0x2d

.field public static final AppTheme_ftList30Color:I = 0x2e

.field public static final AppTheme_ftList31Color:I = 0x2f

.field public static final AppTheme_ftList32Color:I = 0x30

.field public static final AppTheme_ftList33Color:I = 0x31

.field public static final AppTheme_ftList34Color:I = 0x32

.field public static final AppTheme_ftList35Color:I = 0x33

.field public static final AppTheme_ftList36Color:I = 0x34

.field public static final AppTheme_ftList37Color:I = 0x35

.field public static final AppTheme_ftList37DisabledColor:I = 0x36

.field public static final AppTheme_ftList38Color:I = 0x37

.field public static final AppTheme_ftList39Color:I = 0x38

.field public static final AppTheme_ftList40Color:I = 0x39

.field public static final AppTheme_ftList41Color:I = 0x3a

.field public static final AppTheme_ftList42Color:I = 0x3b

.field public static final AppTheme_ftList43Color:I = 0x3c

.field public static final AppTheme_ftList44Color:I = 0x3d

.field public static final AppTheme_ftList49Color:I = 0x3e

.field public static final AppTheme_ftList50Color:I = 0x3f

.field public static final AppTheme_ftList51Color:I = 0x40

.field public static final AppTheme_ftList52Color:I = 0x41

.field public static final AppTheme_ftList53Color:I = 0x42

.field public static final AppTheme_ftList54Color:I = 0x43

.field public static final AppTheme_ftList55Color:I = 0x44

.field public static final AppTheme_ftList57Color:I = 0x45

.field public static final AppTheme_ftList58Color:I = 0x46

.field public static final AppTheme_ftPop01Color:I = 0x70

.field public static final AppTheme_ftPop02Color:I = 0x71

.field public static final AppTheme_ftPop03Color:I = 0x72

.field public static final AppTheme_ftPop04Color:I = 0x73

.field public static final AppTheme_ftPop05Color:I = 0x74

.field public static final AppTheme_ftPop06Color:I = 0x75

.field public static final AppTheme_ftPop07Color:I = 0x76

.field public static final AppTheme_ftPop08Color:I = 0x77

.field public static final AppTheme_ftPop09Color:I = 0x78

.field public static final AppTheme_ftPop10Color:I = 0x79

.field public static final AppTheme_ftPop11Color:I = 0x7a

.field public static final AppTheme_ftPop12Color:I = 0x7b

.field public static final AppTheme_ftPop13Color:I = 0x7c

.field public static final AppTheme_ftPop14Color:I = 0x7d

.field public static final AppTheme_imageTextViewGroupStyle:I = 0xa

.field public static final AppTheme_itemBackground:I = 0xc

.field public static final AppTheme_selectAllLayoutBackground:I = 0x7

.field public static final AppTheme_stateButtonStyle:I = 0x9

.field public static final AppTheme_stateTextViewStyle:I = 0x8

.field public static final AppTheme_windowBackgroundColor:I = 0x0

.field public static final CheckableRelativeLayout:[I

.field public static final CheckableRelativeLayout_checkable_widget:I = 0x0

.field public static final ClearableEditText:[I

.field public static final ClearableEditText_android_hint:I = 0x1

.field public static final ClearableEditText_android_imeOptions:I = 0x4

.field public static final ClearableEditText_android_inputType:I = 0x3

.field public static final ClearableEditText_android_maxLength:I = 0x2

.field public static final ClearableEditText_android_minHeight:I = 0x0

.field public static final ClearableEditText_layout:I = 0x5

.field public static final CommonTheme:[I

.field public static final CommonTheme_actionBarDivider:I = 0x6

.field public static final CommonTheme_actionBarSize:I = 0x4

.field public static final CommonTheme_actionBarStyle:I = 0x0

.field public static final CommonTheme_actionBarTabBarStyle:I = 0x2

.field public static final CommonTheme_actionBarTabStyle:I = 0x1

.field public static final CommonTheme_actionBarTabTextStyle:I = 0x3

.field public static final CommonTheme_actionButtonStyle:I = 0x5

.field public static final CommonTheme_actionDropDownStyle:I = 0xb

.field public static final CommonTheme_actionMenuTextAppearance:I = 0x9

.field public static final CommonTheme_actionMenuTextColor:I = 0xa

.field public static final CommonTheme_alertDialogStyle:I = 0x10

.field public static final CommonTheme_alertDialogTheme:I = 0xf

.field public static final CommonTheme_buttonBarButtonStyle:I = 0x14

.field public static final CommonTheme_buttonBarStyle:I = 0x13

.field public static final CommonTheme_dropDownListViewStyle:I = 0xe

.field public static final CommonTheme_homeAsUpIndicator:I = 0x7

.field public static final CommonTheme_listDividerAlertDialog:I = 0x11

.field public static final CommonTheme_listPopupWindowStyle:I = 0xd

.field public static final CommonTheme_selectableItemBackground:I = 0x8

.field public static final CommonTheme_spinnerDropDownItemStyle:I = 0xc

.field public static final CommonTheme_textColorAlertDialogListItem:I = 0x12

.field public static final ImageTextViewGroup:[I

.field public static final ImageTextViewGroup_android_text:I = 0x1

.field public static final ImageTextViewGroup_android_textAppearance:I = 0x0

.field public static final ImageTextViewGroup_src:I = 0x2

.field public static final ImageTextViewGroup_srcPadding:I = 0x3

.field public static final PagerPositionStrip:[I

.field public static final PagerPositionStrip_firstIndicator:I = 0x3

.field public static final PagerPositionStrip_indicator:I = 0x0

.field public static final PagerPositionStrip_indicatorMargin:I = 0x5

.field public static final PagerPositionStrip_lastIndicator:I = 0x4

.field public static final PagerPositionStrip_leftIndicator:I = 0x1

.field public static final PagerPositionStrip_maxIndicatorCount:I = 0x6

.field public static final PagerPositionStrip_rightIndicator:I = 0x2

.field public static final SelectableImageView:[I

.field public static final SelectableImageView_imageViewSelector:I = 0x0

.field public static final SpinnerCompat:[I

.field public static final SpinnerCompat_android_dropDownHorizontalOffset:I = 0x5

.field public static final SpinnerCompat_android_dropDownSelector:I = 0x1

.field public static final SpinnerCompat_android_dropDownVerticalOffset:I = 0x6

.field public static final SpinnerCompat_android_dropDownWidth:I = 0x4

.field public static final SpinnerCompat_android_gravity:I = 0x0

.field public static final SpinnerCompat_android_popupBackground:I = 0x2

.field public static final SpinnerCompat_android_popupPromptView:I = 0x7

.field public static final SpinnerCompat_android_prompt:I = 0x3

.field public static final StateTextView:[I

.field public static final StateTextView_disabledTextAppearance:I = 0x4

.field public static final StateTextView_focusedTextAppearance:I = 0x3

.field public static final StateTextView_normalTextAppearance:I = 0x0

.field public static final StateTextView_pressedTextAppearance:I = 0x2

.field public static final StateTextView_selectedTextAppearance:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1144
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/chaton/l/g;->ActionBarCompat:[I

    .line 1152
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/chaton/l/g;->AlertDialogCompat:[I

    .line 1163
    const/16 v0, 0x7f

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/chaton/l/g;->AppTheme:[I

    .line 1291
    new-array v0, v3, [I

    const v1, 0x7f0100b1

    aput v1, v0, v2

    sput-object v0, Lcom/sec/chaton/l/g;->CheckableRelativeLayout:[I

    .line 1293
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/chaton/l/g;->ClearableEditText:[I

    .line 1300
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sec/chaton/l/g;->CommonTheme:[I

    .line 1322
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/sec/chaton/l/g;->ImageTextViewGroup:[I

    .line 1327
    new-array v0, v4, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/sec/chaton/l/g;->PagerPositionStrip:[I

    .line 1335
    new-array v0, v3, [I

    const v1, 0x7f0100b5

    aput v1, v0, v2

    sput-object v0, Lcom/sec/chaton/l/g;->SelectableImageView:[I

    .line 1337
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/sec/chaton/l/g;->SpinnerCompat:[I

    .line 1346
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/sec/chaton/l/g;->StateTextView:[I

    return-void

    .line 1144
    :array_0
    .array-data 4
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
    .end array-data

    .line 1152
    :array_1
    .array-data 4
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
    .end array-data

    .line 1163
    :array_2
    .array-data 4
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f01003e
        0x7f01003f
        0x7f010040
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
        0x7f010055
        0x7f010056
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f01005a
        0x7f01005b
        0x7f01005c
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
        0x7f010061
        0x7f010062
        0x7f010063
        0x7f010064
        0x7f010065
        0x7f010066
        0x7f010067
        0x7f010068
        0x7f010069
        0x7f01006a
        0x7f01006b
        0x7f01006c
        0x7f01006d
        0x7f01006e
        0x7f01006f
        0x7f010070
        0x7f010071
        0x7f010072
        0x7f010073
        0x7f010074
        0x7f010075
        0x7f010076
        0x7f010077
        0x7f010078
        0x7f010079
        0x7f01007a
        0x7f01007b
        0x7f01007c
        0x7f01007d
        0x7f01007e
        0x7f01007f
        0x7f010080
        0x7f010081
        0x7f010082
        0x7f010083
        0x7f010084
        0x7f010085
        0x7f010086
        0x7f010087
        0x7f010088
        0x7f010089
        0x7f01008a
        0x7f01008b
        0x7f01008c
        0x7f01008d
        0x7f01008e
        0x7f01008f
        0x7f010090
        0x7f010091
        0x7f010092
        0x7f010093
        0x7f010094
        0x7f010095
        0x7f010096
        0x7f010097
        0x7f010098
        0x7f010099
        0x7f01009a
        0x7f01009b
        0x7f01009c
        0x7f01009d
        0x7f01009e
        0x7f01009f
        0x7f0100a0
        0x7f0100a1
        0x7f0100a2
        0x7f0100a3
        0x7f0100a4
        0x7f0100a5
        0x7f0100a6
        0x7f0100a7
        0x7f0100a8
        0x7f0100a9
        0x7f0100aa
        0x7f0100ab
    .end array-data

    .line 1293
    :array_3
    .array-data 4
        0x1010140
        0x1010150
        0x1010160
        0x1010220
        0x1010264
        0x7f0100b4
    .end array-data

    .line 1300
    :array_4
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
    .end array-data

    .line 1322
    :array_5
    .array-data 4
        0x1010034
        0x101014f
        0x7f0100b2
        0x7f0100b3
    .end array-data

    .line 1327
    :array_6
    .array-data 4
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
    .end array-data

    .line 1337
    :array_7
    .array-data 4
        0x10100af
        0x1010175
        0x1010176
        0x101017b
        0x1010262
        0x10102ac
        0x10102ad
        0x1010411
    .end array-data

    .line 1346
    :array_8
    .array-data 4
        0x7f0100ac
        0x7f0100ad
        0x7f0100ae
        0x7f0100af
        0x7f0100b0
    .end array-data
.end method
