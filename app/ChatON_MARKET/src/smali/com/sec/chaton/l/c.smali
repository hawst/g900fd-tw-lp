.class public final Lcom/sec/chaton/l/c;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final actionbar_background:I = 0x7f020000

.field public static final actionbar_bg:I = 0x7f020001

.field public static final actionbar_color_btn_bg_pressed:I = 0x7f020002

.field public static final actionbar_dropdown_bg:I = 0x7f020003

.field public static final actionbar_home_background:I = 0x7f020004

.field public static final actionbar_home_item:I = 0x7f020005

.field public static final actionbar_home_item_focused:I = 0x7f020006

.field public static final actionbar_ic_arrow:I = 0x7f020008

.field public static final actionbar_ic_chaton:I = 0x7f02000a

.field public static final actionbar_ic_moreoverflow:I = 0x7f020018

.field public static final actionbar_ic_sms:I = 0x7f02001c

.field public static final actionbar_item_background:I = 0x7f020020

.field public static final actionbar_item_focused:I = 0x7f020021

.field public static final actionbar_item_pressed:I = 0x7f020022

.field public static final actionbar_stacked_background:I = 0x7f020024

.field public static final actionbar_tab:I = 0x7f020025

.field public static final actionbar_tab_selected:I = 0x7f020026

.field public static final actionbar_tab_selected_focused:I = 0x7f020027

.field public static final actionbar_tab_selected_pressed:I = 0x7f020028

.field public static final actionbar_tab_unselected:I = 0x7f020029

.field public static final actionbar_tab_unselected_focused:I = 0x7f02002a

.field public static final actionbar_tab_unselected_pressed:I = 0x7f02002b

.field public static final blank_divider_14:I = 0x7f0200b3

.field public static final blank_divider_8:I = 0x7f0200b4

.field public static final btn_radio_off_disabled_focused_holo_light:I = 0x7f0200be

.field public static final btn_radio_off_disabled_holo_light:I = 0x7f0200bf

.field public static final btn_radio_off_focused_holo_light:I = 0x7f0200c0

.field public static final btn_radio_off_holo_light:I = 0x7f0200c1

.field public static final btn_radio_off_pressed_holo_light:I = 0x7f0200c2

.field public static final buddieds_icon_phone:I = 0x7f0200e3

.field public static final buddieds_icon_search:I = 0x7f0200e4

.field public static final buddieds_icon_suggestions:I = 0x7f0200e5

.field public static final buddieds_icon_tellfriends:I = 0x7f0200e6

.field public static final button:I = 0x7f0200f8

.field public static final button2:I = 0x7f0200f9

.field public static final button2_disabled_focused:I = 0x7f0200fa

.field public static final button2_focused:I = 0x7f0200fb

.field public static final button3:I = 0x7f0200fc

.field public static final button3_disabled_focused:I = 0x7f0200fd

.field public static final button3_focused:I = 0x7f0200fe

.field public static final button4:I = 0x7f0200ff

.field public static final button5:I = 0x7f020100

.field public static final button5_disabled_focused:I = 0x7f020101

.field public static final button5_focused:I = 0x7f020102

.field public static final button6:I = 0x7f020103

.field public static final button6_disabled_focused:I = 0x7f020104

.field public static final button6_focused:I = 0x7f020105

.field public static final button_alertdialog:I = 0x7f020106

.field public static final button_alertdialog_focused:I = 0x7f020107

.field public static final button_disabled_focused:I = 0x7f020109

.field public static final button_focused:I = 0x7f02010a

.field public static final button_layout_background:I = 0x7f02010b

.field public static final button_layout_background_black:I = 0x7f02010c

.field public static final chat_btn_icon_cancel:I = 0x7f020134

.field public static final chat_btn_play:I = 0x7f020138

.field public static final chat_btn_send_normal:I = 0x7f020139

.field public static final chat_btn_send_normal_vitalize:I = 0x7f02013a

.field public static final chat_btn_send_press:I = 0x7f02013b

.field public static final chat_forward_btn_disable_center:I = 0x7f020141

.field public static final chat_forward_btn_disable_left:I = 0x7f020142

.field public static final chat_forward_btn_disable_right:I = 0x7f020143

.field public static final chat_forward_btn_normal_center:I = 0x7f020144

.field public static final chat_forward_btn_normal_left:I = 0x7f020145

.field public static final chat_forward_btn_normal_right:I = 0x7f020146

.field public static final chat_forward_btn_press_center:I = 0x7f020147

.field public static final chat_forward_btn_press_left:I = 0x7f020148

.field public static final chat_forward_btn_press_right:I = 0x7f020149

.field public static final chat_icon_arrow:I = 0x7f020152

.field public static final chaton_dropdown:I = 0x7f020162

.field public static final chaton_logo:I = 0x7f020165

.field public static final check_ic_close:I = 0x7f02016f

.field public static final check_on_disabled_holo_light:I = 0x7f020170

.field public static final check_on_holo_light:I = 0x7f020171

.field public static final check_on_holo_light_bg:I = 0x7f020172

.field public static final checkbox:I = 0x7f020173

.field public static final checkbox_checked:I = 0x7f020174

.field public static final checkbox_checked_disabled:I = 0x7f020175

.field public static final checkbox_checked_focused:I = 0x7f020176

.field public static final checkbox_checked_focused_disabled:I = 0x7f020177

.field public static final checkbox_checked_pressed:I = 0x7f020178

.field public static final checkbox_disabled:I = 0x7f020179

.field public static final checkbox_focused:I = 0x7f02017a

.field public static final checkbox_focused_disabled:I = 0x7f02017b

.field public static final checkbox_normal:I = 0x7f02017c

.field public static final checkbox_pressed:I = 0x7f02017d

.field public static final clearable_edit_text:I = 0x7f02017f

.field public static final co_btn_bg_color_disable:I = 0x7f02019b

.field public static final co_btn_bg_color_normal:I = 0x7f02019c

.field public static final co_btn_bg_color_press:I = 0x7f02019d

.field public static final co_btn_bg_disable:I = 0x7f02019e

.field public static final co_btn_bg_normal:I = 0x7f02019f

.field public static final co_btn_bg_press:I = 0x7f0201a0

.field public static final co_list_bg:I = 0x7f0201a5

.field public static final co_list_bg2:I = 0x7f0201a6

.field public static final co_winset_btn_bg_disable:I = 0x7f0201ac

.field public static final co_winset_btn_bg_normal:I = 0x7f0201ad

.field public static final co_winset_btn_bg_press:I = 0x7f0201ae

.field public static final common_new:I = 0x7f0201b0

.field public static final common_noti:I = 0x7f0201b1

.field public static final divider1:I = 0x7f0201c3

.field public static final divider2_vertical:I = 0x7f0201c4

.field public static final divider3_vertical:I = 0x7f0201c5

.field public static final divider4_vertical:I = 0x7f0201c6

.field public static final divider5_vertical:I = 0x7f0201c7

.field public static final divider6_vertical:I = 0x7f0201c8

.field public static final divider7_vertical:I = 0x7f0201c9

.field public static final dropdown_item_selector:I = 0x7f0201e3

.field public static final dropdown_listview_divider:I = 0x7f0201e4

.field public static final droppanel_menu_bg_color:I = 0x7f0201e5

.field public static final edit_text:I = 0x7f0201e6

.field public static final edit_text_background:I = 0x7f0201e7

.field public static final edit_text_font_color:I = 0x7f0201e8

.field public static final frame_background:I = 0x7f02028d

.field public static final ft_bar_02_text_color:I = 0x7f02028f

.field public static final ft_bar_02_text_color_black:I = 0x7f020290

.field public static final ft_btn_02_text_color:I = 0x7f020291

.field public static final ft_btn_02_text_color_black:I = 0x7f020292

.field public static final ft_btn_04_text_color:I = 0x7f020293

.field public static final ft_ext_01_text_color:I = 0x7f020294

.field public static final ft_list_01_text_color:I = 0x7f020295

.field public static final gridview_selector:I = 0x7f020297

.field public static final gridview_selector_focused:I = 0x7f020298

.field public static final gridview_selector_selected:I = 0x7f020299

.field public static final ic_arrow:I = 0x7f02029a

.field public static final imageview_selector:I = 0x7f0202af

.field public static final imageview_selector_pressed:I = 0x7f0202b0

.field public static final input_btn_ic_search:I = 0x7f0202b2

.field public static final input_btn_ic_search_disable:I = 0x7f0202b3

.field public static final input_ic_add:I = 0x7f0202b4

.field public static final input_ic_search:I = 0x7f0202b9

.field public static final input_line_disable:I = 0x7f0202ba

.field public static final input_line_focus:I = 0x7f0202bb

.field public static final input_line_normal:I = 0x7f0202bc

.field public static final input_line_select:I = 0x7f0202bd

.field public static final item_background:I = 0x7f0202c5

.field public static final item_background_focused:I = 0x7f0202c6

.field public static final item_background_pressed:I = 0x7f0202c7

.field public static final list_btn_bg_orange_disable:I = 0x7f0202cc

.field public static final list_btn_bg_orange_normal:I = 0x7f0202cd

.field public static final list_btn_bg_orange_press:I = 0x7f0202ce

.field public static final list_ic_add:I = 0x7f0202cf

.field public static final list_ic_add_disable:I = 0x7f0202d0

.field public static final list_ic_arrow_down:I = 0x7f0202d1

.field public static final list_ic_arrow_up:I = 0x7f0202d3

.field public static final listview_category_divider:I = 0x7f0202d9

.field public static final listview_divider:I = 0x7f0202da

.field public static final listview_divider_alertdialog:I = 0x7f0202db

.field public static final listview_selector:I = 0x7f0202dc

.field public static final listview_selector_focused:I = 0x7f0202dd

.field public static final listview_selector_highlight:I = 0x7f0202de

.field public static final listview_selector_highlight_normal:I = 0x7f0202df

.field public static final listview_selector_highlight_selected:I = 0x7f0202e0

.field public static final listview_selector_pressed:I = 0x7f0202e1

.field public static final more_option_bg:I = 0x7f0202ff

.field public static final namefield_bg:I = 0x7f020349

.field public static final option_menu_item_badge_wrapper:I = 0x7f020353

.field public static final popup_winset_bg:I = 0x7f020367

.field public static final popup_winset_bottom_bright_wrapper:I = 0x7f020368

.field public static final popup_winset_bottom_dark_wrapper:I = 0x7f020369

.field public static final popup_winset_bottom_medium_01:I = 0x7f02036a

.field public static final popup_winset_bottom_medium_02:I = 0x7f02036b

.field public static final popup_winset_bottom_medium_wrapper:I = 0x7f02036c

.field public static final popup_winset_center_bright:I = 0x7f02036d

.field public static final popup_winset_center_dark_wrapper:I = 0x7f02036e

.field public static final popup_winset_icon_loading:I = 0x7f02036f

.field public static final popup_winset_top_bright:I = 0x7f020370

.field public static final popup_winset_top_dark_wrapper:I = 0x7f020374

.field public static final progress:I = 0x7f02039c

.field public static final progress_bg:I = 0x7f02039d

.field public static final progress_horizontal:I = 0x7f02039e

.field public static final progress_indetermintate_horizontal:I = 0x7f02039f

.field public static final progress_line:I = 0x7f0203a0

.field public static final progress_wait_1:I = 0x7f0203a1

.field public static final progress_wait_2:I = 0x7f0203a2

.field public static final progress_wait_3:I = 0x7f0203a3

.field public static final provisioning_btn_bg_disable:I = 0x7f0203a4

.field public static final provisioning_btn_bg_normal:I = 0x7f0203a5

.field public static final provisioning_btn_bg_perss:I = 0x7f0203a6

.field public static final pulldown_ic_disable:I = 0x7f0203ad

.field public static final pulldown_ic_focus:I = 0x7f0203ae

.field public static final pulldown_ic_normal:I = 0x7f0203af

.field public static final pulldown_ic_select:I = 0x7f0203b0

.field public static final pulldown_icw_focus:I = 0x7f0203b1

.field public static final pulldown_icw_normal:I = 0x7f0203b2

.field public static final pulldown_icw_select:I = 0x7f0203b3

.field public static final radio_off_holo_light_bg:I = 0x7f0203be

.field public static final radio_on_disable_holo_light:I = 0x7f0203bf

.field public static final radio_on_holo_light:I = 0x7f0203c0

.field public static final radiobutton:I = 0x7f0203c1

.field public static final radiobutton_checked:I = 0x7f0203c2

.field public static final radiobutton_checked_disabled:I = 0x7f0203c3

.field public static final radiobutton_checked_focused:I = 0x7f0203c4

.field public static final radiobutton_checked_focused_disabled:I = 0x7f0203c5

.field public static final radiobutton_checked_pressed:I = 0x7f0203c6

.field public static final search_icon:I = 0x7f0203cc

.field public static final select_all_layout_background:I = 0x7f0203cd

.field public static final select_all_layout_background_black:I = 0x7f0203ce

.field public static final select_icon_check:I = 0x7f0203cf

.field public static final select_member_check_bg:I = 0x7f0203d0

.field public static final select_member_check_bg_focus:I = 0x7f0203d1

.field public static final select_member_check_bg_press:I = 0x7f0203d2

.field public static final spinner:I = 0x7f0203f4

.field public static final subtitle_selector:I = 0x7f0203f8

.field public static final subtitle_selector_focused:I = 0x7f0203f9

.field public static final tab_2_background:I = 0x7f0203fd

.field public static final tab_2_button:I = 0x7f0203fe

.field public static final tab_2_button_pressed:I = 0x7f0203ff

.field public static final tab_center_button_background:I = 0x7f020401

.field public static final tab_center_button_disabled_focused:I = 0x7f020402

.field public static final tab_center_button_focused:I = 0x7f020403

.field public static final tab_center_button_pressed_focused:I = 0x7f020404

.field public static final tab_left_button_background:I = 0x7f020405

.field public static final tab_left_button_disabled_focused:I = 0x7f020406

.field public static final tab_left_button_focused:I = 0x7f020407

.field public static final tab_left_button_pressed_focused:I = 0x7f020408

.field public static final tab_right_button_background:I = 0x7f020409

.field public static final tab_right_button_disabled_focused:I = 0x7f02040a

.field public static final tab_right_button_focused:I = 0x7f02040b

.field public static final tab_right_button_pressed_focused:I = 0x7f02040c

.field public static final text_cursor_holo_light:I = 0x7f020423

.field public static final thum_stroke:I = 0x7f020424

.field public static final thum_stroke01:I = 0x7f020425

.field public static final transparent:I = 0x7f020433

.field public static final trunk_ic_comment_read:I = 0x7f02043d

.field public static final trunk_ic_comment_unread_chats:I = 0x7f02043e

.field public static final trunk_ic_comment_unread_trunk:I = 0x7f02043f
