.class Lcom/sec/chaton/chat/az;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ay;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ay;)V
    .locals 0

    .prologue
    .line 7795
    iput-object p1, p0, Lcom/sec/chaton/chat/az;->a:Lcom/sec/chaton/chat/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 7798
    .line 7804
    iget-object v0, p0, Lcom/sec/chaton/chat/az;->a:Lcom/sec/chaton/chat/ay;

    iget-object v0, v0, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->al(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/chat/az;->a:Lcom/sec/chaton/chat/ay;

    iget-object v1, v1, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->W(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 7805
    iget-object v1, p0, Lcom/sec/chaton/chat/az;->a:Lcom/sec/chaton/chat/ay;

    iget-object v1, v1, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/az;->a:Lcom/sec/chaton/chat/ay;

    iget-object v2, v2, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->am(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/sec/widget/HeightChangedListView;->setSelectionFromTop(II)V

    .line 7807
    iget-object v1, p0, Lcom/sec/chaton/chat/az;->a:Lcom/sec/chaton/chat/ay;

    iget-object v1, v1, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    iput-boolean v3, v1, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    .line 7808
    iget-object v1, p0, Lcom/sec/chaton/chat/az;->a:Lcom/sec/chaton/chat/ay;

    iget-object v1, v1, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/widget/HeightChangedListView;->setTranscriptMode(I)V

    .line 7810
    iget-object v1, p0, Lcom/sec/chaton/chat/az;->a:Lcom/sec/chaton/chat/ay;

    iget-object v1, v1, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->i(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 7811
    iget-object v1, p0, Lcom/sec/chaton/chat/az;->a:Lcom/sec/chaton/chat/ay;

    iget-object v1, v1, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1, v3}, Lcom/sec/chaton/chat/ChatFragment;->h(Lcom/sec/chaton/chat/ChatFragment;Z)Z

    .line 7812
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onQueryCompleted - QUERY_MESSAGE_FOR_MARK (total:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/az;->a:Lcom/sec/chaton/chat/ay;

    iget-object v2, v2, Lcom/sec/chaton/chat/ay;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/widget/HeightChangedListView;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",position:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7813
    return-void
.end method
