.class public abstract Lcom/sec/chaton/chat/a/af;
.super Lcom/sec/chaton/chat/a/x;
.source "TransportBubbleDrawer.java"

# interfaces
.implements Lcom/sec/chaton/chat/a/m;
.implements Lcom/sec/chaton/chat/a/o;
.implements Lcom/sec/chaton/chat/a/p;


# instance fields
.field private F:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/x;-><init>()V

    .line 34
    new-instance v0, Lcom/sec/chaton/chat/a/ag;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/a/ag;-><init>(Lcom/sec/chaton/chat/a/af;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/a/af;->F:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 185
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/x;->a(Z)V

    .line 187
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/af;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->I:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iget v0, p0, Lcom/sec/chaton/chat/a/af;->A:I

    if-nez v0, :cond_0

    .line 191
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/af;->o:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/c/g;->a(J)Lcom/sec/chaton/j/c/i;

    move-result-object v0

    .line 193
    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {v0, v3}, Lcom/sec/chaton/j/c/i;->a(Landroid/os/Handler;)V

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->G:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ae:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected a_()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 115
    invoke-super {p0}, Lcom/sec/chaton/chat/a/x;->a_()V

    .line 117
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/af;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 118
    iget v0, p0, Lcom/sec/chaton/chat/a/af;->A:I

    if-nez v0, :cond_0

    .line 119
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/af;->o:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/c/g;->a(J)Lcom/sec/chaton/j/c/i;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_1

    .line 122
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->Z:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 124
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->Z:Landroid/widget/ProgressBar;

    .line 125
    iget-object v2, p0, Lcom/sec/chaton/chat/a/af;->F:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/j/c/i;->a(Landroid/os/Handler;)V

    .line 127
    invoke-virtual {v0}, Lcom/sec/chaton/j/c/i;->l()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aa:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->I:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 154
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->I:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    :cond_0
    :goto_1
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Z:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 136
    iget v0, p0, Lcom/sec/chaton/chat/a/af;->z:I

    if-ne v0, v5, :cond_3

    .line 137
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-ne v0, v5, :cond_2

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aa:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 151
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->I:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aa:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_2

    .line 146
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->Y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->aa:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 157
    :cond_4
    iget v0, p0, Lcom/sec/chaton/chat/a/af;->A:I

    if-ne v0, v6, :cond_5

    .line 158
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/af;->m:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/c/a;->b(J)Lcom/sec/chaton/j/c/c;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_6

    .line 161
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 162
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->F:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 163
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->G:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 164
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->G:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/c;->a(Landroid/view/View;)V

    .line 174
    :cond_5
    :goto_3
    iget v0, p0, Lcom/sec/chaton/chat/a/af;->A:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_7

    iget v0, p0, Lcom/sec/chaton/chat/a/af;->A:I

    if-eq v0, v6, :cond_7

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ae:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ae:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 168
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->F:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->G:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3

    .line 178
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ae:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method protected d()V
    .locals 7

    .prologue
    const v6, 0x7f090127

    .line 49
    .line 54
    const/4 v0, 0x0

    .line 56
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42de0000    # 111.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float v4, v1, v2

    .line 57
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/af;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v0, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    .line 60
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    .line 61
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v0, Lcom/sec/chaton/chat/el;->N:Landroid/widget/TextView;

    .line 62
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    .line 73
    :goto_0
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 76
    if-eqz v0, :cond_5

    .line 77
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    add-float/2addr v0, v1

    .line 80
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/af;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 83
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f09012d

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 84
    iget-object v5, p0, Lcom/sec/chaton/chat/a/af;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v1, v5

    .line 86
    cmpl-float v5, v0, v1

    if-lez v5, :cond_2

    .line 96
    :cond_0
    :goto_2
    sub-float v0, v4, v0

    .line 99
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/a/af;->a(F)F

    move-result v0

    .line 102
    float-to-int v1, v0

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 104
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->t:Landroid/widget/ImageView;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->t:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    .line 105
    const/high16 v1, 0x42240000    # 41.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    sub-float/2addr v0, v1

    .line 106
    float-to-int v0, v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 111
    :goto_3
    return-void

    .line 65
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v1, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    .line 66
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v1, Lcom/sec/chaton/chat/el;->s:Landroid/widget/TextView;

    .line 67
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->C:Landroid/widget/TextView;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 86
    goto :goto_2

    .line 90
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f09012e

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 91
    iget-object v5, p0, Lcom/sec/chaton/chat/a/af;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v1, v5

    .line 93
    cmpl-float v5, v0, v1

    if-gtz v5, :cond_0

    move v0, v1

    goto :goto_2

    .line 108
    :cond_4
    float-to-int v0, v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/af;->g()Z

    move-result v0

    return v0
.end method

.method public e_()Lcom/sec/chaton/chat/a/n;
    .locals 4

    .prologue
    .line 277
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/af;->w:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/af;->l()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()Lcom/sec/chaton/chat/a/n;
    .locals 4

    .prologue
    .line 264
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/af;->w:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/a/af;->x:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->w:Ljava/lang/String;

    const-string v1, "thumbnail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/chaton/chat/a/af;->A:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 241
    :cond_0
    const/4 v0, 0x0

    .line 244
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public h()Lcom/sec/chaton/chat/a/n;
    .locals 4

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/af;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->u:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->u:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_1

    .line 250
    :cond_0
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->u:Lcom/sec/chaton/e/w;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/chat/a/af;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/af;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/a/af;->w:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/af;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/af;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/a/af;->w:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 216
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/x;->onClick(Landroid/view/View;)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->I:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->k:Lcom/sec/chaton/chat/fk;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/af;->n:Ljava/lang/String;

    iget-wide v3, p0, Lcom/sec/chaton/chat/a/af;->o:J

    iget-object v5, p0, Lcom/sec/chaton/chat/a/af;->u:Lcom/sec/chaton/e/w;

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/chaton/chat/fk;->a(Landroid/view/View;Ljava/lang/String;JLcom/sec/chaton/e/w;)V

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->G:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->k:Lcom/sec/chaton/chat/fk;

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/af;->m:J

    invoke-interface {v0, p1, v1, v2}, Lcom/sec/chaton/chat/fk;->a(Landroid/view/View;J)V

    goto :goto_0

    .line 222
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ae:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->e:Landroid/database/Cursor;

    iget v1, p0, Lcom/sec/chaton/chat/a/af;->f:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/chat/a/af;->k:Lcom/sec/chaton/chat/fk;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/af;->n()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/af;->e:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/sec/chaton/chat/a/af;->d:Landroid/view/View;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/chat/fk;->a(ZLandroid/database/Cursor;Landroid/view/View;)V

    goto :goto_0
.end method
