.class Lcom/sec/chaton/chat/bg;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 10958
    iput-object p1, p0, Lcom/sec/chaton/chat/bg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 10961
    packed-switch p2, :pswitch_data_0

    .line 10979
    :goto_0
    return-void

    .line 10963
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/chat/bg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aI(Lcom/sec/chaton/chat/ChatFragment;)V

    goto :goto_0

    .line 10967
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 10968
    iget-object v0, p0, Lcom/sec/chaton/chat/bg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 10971
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 10973
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/chat/bg;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10974
    const-string v2, "preview_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 10975
    iget-object v0, p0, Lcom/sec/chaton/chat/bg;->a:Lcom/sec/chaton/chat/ChatFragment;

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 10961
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
