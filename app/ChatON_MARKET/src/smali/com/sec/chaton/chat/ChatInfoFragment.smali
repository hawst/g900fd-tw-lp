.class public Lcom/sec/chaton/chat/ChatInfoFragment;
.super Landroid/support/v4/app/Fragment;
.source "ChatInfoFragment.java"

# interfaces
.implements Lcom/sec/chaton/chat/fc;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:I

.field private C:Ljava/lang/String;

.field private D:Ljava/io/File;

.field private E:Ljava/io/File;

.field private F:Ljava/lang/String;

.field private G:Z

.field private H:Z

.field private I:Landroid/net/Uri;

.field private J:Landroid/widget/Toast;

.field private K:Z

.field private L:Landroid/app/ProgressDialog;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:Landroid/widget/ImageView;

.field private S:Lcom/sec/common/f/c;

.field private T:Lcom/sec/chaton/chat/eo;

.field private U:Landroid/os/Handler;

.field b:Ljava/lang/String;

.field c:Lcom/sec/chaton/e/a/v;

.field d:Ljava/io/File;

.field private e:Landroid/widget/TextView;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Landroid/content/Context;

.field private i:Lcom/sec/chaton/chat/fc;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:[Ljava/lang/String;

.field private m:Landroid/widget/TextView;

.field private n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/Button;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/ImageView;

.field private t:Landroid/widget/ImageButton;

.field private u:Lcom/sec/chaton/e/a/u;

.field private v:Landroid/widget/Toast;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-class v0, Lcom/sec/chaton/chat/ChatInfoActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/ChatInfoFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 89
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->z:Z

    .line 160
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->b:Ljava/lang/String;

    .line 161
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/profile/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->D:Ljava/io/File;

    .line 163
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->F:Ljava/lang/String;

    .line 164
    iput-boolean v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->G:Z

    .line 165
    iput-boolean v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->H:Z

    .line 168
    iput-boolean v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->K:Z

    .line 184
    new-instance v0, Lcom/sec/chaton/chat/dh;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/dh;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->U:Landroid/os/Handler;

    .line 233
    new-instance v0, Lcom/sec/chaton/chat/dn;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/dn;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->c:Lcom/sec/chaton/e/a/v;

    .line 730
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->d:Ljava/io/File;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatInfoFragment;I)I
    .locals 0

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->B:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 414
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->z:Z

    .line 418
    :cond_0
    const-string v0, "inbox_background_style"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->w:Ljava/lang/String;

    .line 419
    const-string v0, "inbox_send_bubble_style"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->x:Ljava/lang/String;

    .line 420
    const-string v0, "inbox_receive_bubble_style"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->y:Ljava/lang/String;

    .line 422
    const-string v0, "chatType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    .line 423
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    .line 424
    const-string v0, "inbox_title_fixed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->g:Ljava/lang/String;

    .line 426
    const-string v0, "inboxNO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    .line 427
    const-string v0, "buddyNO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->A:Ljava/lang/String;

    .line 428
    const-string v0, "groupId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 429
    const-string v0, "groupId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->C:Ljava/lang/String;

    .line 432
    :cond_1
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 433
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->l:[Ljava/lang/String;

    .line 437
    :cond_2
    const-string v0, "sessionID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 438
    const-string v0, "sessionID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->M:Ljava/lang/String;

    .line 441
    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/ChatInfoFragment;Z)Z
    .locals 0

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->K:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->U:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->M:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->N:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->L:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    return-object v0
.end method

.method private g()V
    .locals 8

    .prologue
    .line 1104
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->f()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1105
    :cond_0
    const-string v0, "[deleteTempFolder] External Storage Is Not Available or Writable!"

    sget-object v1, Lcom/sec/chaton/chat/ChatInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->J:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 1107
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->J:Landroid/widget/Toast;

    .line 1109
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->J:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1130
    :cond_2
    :goto_0
    return-void

    .line 1112
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/ck;->b()Ljava/lang/String;

    move-result-object v1

    .line 1113
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 1114
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->J:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1118
    :cond_4
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/profile/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1119
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 1120
    if-eqz v2, :cond_2

    .line 1121
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 1122
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/profile/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    .line 1123
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Delete File] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/profile/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/chat/ChatInfoFragment;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1121
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1126
    :catch_0
    move-exception v0

    .line 1127
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/chat/ChatInfoFragment;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->i()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->A:Ljava/lang/String;

    return-object v0
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1143
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 1144
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1145
    iput-boolean v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->G:Z

    .line 1146
    iput-boolean v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->H:Z

    .line 1154
    :goto_0
    return-void

    .line 1147
    :cond_0
    const-string v1, "mounted_ro"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1148
    iput-boolean v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->G:Z

    .line 1149
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->H:Z

    goto :goto_0

    .line 1151
    :cond_1
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->G:Z

    .line 1152
    iput-boolean v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->H:Z

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/chaton/chat/ChatInfoFragment;)I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->B:I

    return v0
.end method

.method private i()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1164
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_chat_profile.png_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->N:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1168
    :try_start_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1169
    const-string v2, ""

    .line 1170
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v5, "is_file_server_primary "

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 1171
    if-eqz v2, :cond_0

    .line 1172
    sget-object v2, Lcom/sec/chaton/util/cf;->a:Lcom/sec/chaton/util/cf;

    sget-object v5, Lcom/sec/chaton/util/cg;->c:Lcom/sec/chaton/util/cg;

    invoke-static {v2, v5}, Lcom/sec/chaton/j/c;->a(Lcom/sec/chaton/util/cf;Lcom/sec/chaton/util/cg;)Ljava/lang/String;

    move-result-object v2

    .line 1177
    :goto_0
    const-string v5, "%s%s?%s=%s&%s=%s&%s=%s&%s=%s"

    const/16 v6, 0xa

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v2, 0x1

    const-string v7, "/profileimage"

    aput-object v7, v6, v2

    const/4 v2, 0x2

    const-string v7, "uid"

    aput-object v7, v6, v2

    const/4 v2, 0x3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v7

    const-string v8, "uid"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x4

    const-string v7, "imei"

    aput-object v7, v6, v2

    const/4 v2, 0x5

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x6

    const-string v7, "size"

    aput-object v7, v6, v2

    const/4 v2, 0x7

    const/16 v7, 0xa0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    const/16 v2, 0x8

    const-string v7, "filename"

    aput-object v7, v6, v2

    const/16 v2, 0x9

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->N:Ljava/lang/String;

    aput-object v7, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1179
    const-string v5, "profile_url"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v5, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "inbox_no=\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v2, v5, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1182
    new-instance v2, Lcom/sec/chaton/chat/dm;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/dm;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    .line 1189
    iget-object v4, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->d:Ljava/io/File;

    invoke-virtual {v4, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 1190
    if-eqz v2, :cond_1

    .line 1191
    array-length v4, v2

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v2, v0

    .line 1192
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v6

    .line 1193
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Delete File] "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/chaton/chat/ChatInfoFragment;->a:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1174
    :cond_0
    sget-object v2, Lcom/sec/chaton/util/cf;->b:Lcom/sec/chaton/util/cf;

    sget-object v5, Lcom/sec/chaton/util/cg;->c:Lcom/sec/chaton/util/cg;

    invoke-static {v2, v5}, Lcom/sec/chaton/j/c;->a(Lcom/sec/chaton/util/cf;Lcom/sec/chaton/util/cg;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 1197
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    iget v4, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v4}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    .line 1198
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/16 v4, 0x258

    invoke-static {v0, v2, v4}, Lcom/sec/chaton/util/ad;->b(Landroid/content/Context;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    const/16 v2, 0x258

    const/16 v4, 0x258

    const/4 v5, 0x1

    invoke-static {v0, v2, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1199
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1202
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 1203
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5a

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1204
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    sput-object v2, Lcom/sec/chaton/global/GlobalApplication;->d:Ljava/lang/String;

    .line 1205
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1210
    if-eqz v1, :cond_2

    .line 1211
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1220
    :cond_2
    :goto_2
    return-void

    .line 1206
    :catch_0
    move-exception v0

    .line 1207
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1210
    if-eqz v1, :cond_2

    .line 1211
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 1213
    :catch_1
    move-exception v0

    .line 1214
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 1217
    :catch_2
    move-exception v0

    .line 1218
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 1209
    :catchall_0
    move-exception v0

    .line 1210
    if-eqz v1, :cond_3

    .line 1211
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 1209
    :cond_3
    :goto_3
    :try_start_7
    throw v0

    .line 1213
    :catch_3
    move-exception v1

    .line 1214
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_3

    .line 1213
    :catch_4
    move-exception v0

    .line 1214
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_2
.end method

.method static synthetic j(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1226
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    .line 1229
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1230
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1231
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1232
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->startActivity(Landroid/content/Intent;)V

    .line 1234
    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/chat/ChatInfoFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->l:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/chat/ChatInfoFragment;)I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    return v0
.end method

.method static synthetic m(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->r:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->v:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/chat/ChatInfoFragment;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->u:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/chaton/chat/ChatInfoFragment;)Lcom/sec/chaton/chat/fc;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->i:Lcom/sec/chaton/chat/fc;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/chat/ChatInfoFragment;)Lcom/sec/chaton/widget/CheckableRelativeLayout;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->I:Landroid/net/Uri;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 444
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f070126

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->t:Landroid/widget/ImageButton;

    .line 445
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->t:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/chat/dp;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/dp;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 462
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f070124

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->R:Landroid/widget/ImageView;

    .line 463
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f070076

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/chat/dq;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/dq;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 477
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->c()V

    .line 479
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f070125

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->e:Landroid/widget/TextView;

    .line 480
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 484
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f070127

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChoiceMode(I)V

    .line 486
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->o:Landroid/widget/TextView;

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->p:Landroid/widget/TextView;

    .line 488
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->o:Landroid/widget/TextView;

    const v1, 0x7f0b01df

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 489
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 501
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f070129

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->m:Landroid/widget/TextView;

    .line 502
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 503
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->l:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->m:Landroid/widget/TextView;

    const v1, 0x7f0b01a2

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->l:[Ljava/lang/String;

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/chat/ChatInfoFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 509
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setVisibility(I)V

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->z:Z

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setChecked(Z)V

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

    new-instance v1, Lcom/sec/chaton/chat/dr;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/dr;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 521
    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 522
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

    invoke-virtual {v0, v5}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setVisibility(I)V

    .line 527
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mChattitle:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 529
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f07012a

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->q:Landroid/widget/Button;

    .line 530
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->q:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/chat/ds;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/ds;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 581
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f07012b

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->r:Landroid/widget/Button;

    .line 582
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->r:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/chat/dt;

    invoke-direct {v1, p0}, Lcom/sec/chaton/chat/dt;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596
    return-void

    .line 524
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->n:Lcom/sec/chaton/widget/CheckableRelativeLayout;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/CheckableRelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 860
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 861
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 863
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 733
    .line 735
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 736
    const-string v2, "filename="

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 737
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->P:Ljava/lang/String;

    .line 742
    new-instance v2, Lcom/sec/chaton/chat/dk;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/dk;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    .line 749
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->d:Ljava/io/File;

    invoke-virtual {v3, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 750
    if-eqz v2, :cond_0

    array-length v3, v2

    if-nez v3, :cond_2

    :cond_0
    move v0, v1

    .line 766
    :cond_1
    :goto_0
    return v0

    .line 754
    :cond_2
    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->Q:Ljava/lang/String;

    .line 755
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->Q:Ljava/lang/String;

    const-string v3, "_chat_profile.png_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 756
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->O:Ljava/lang/String;

    .line 758
    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->P:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->O:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 766
    goto :goto_0

    .line 761
    :catch_0
    move-exception v1

    .line 763
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 708
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 709
    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 710
    const v1, 0x7f0b0033

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    .line 711
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/chat/dv;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/dv;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/chat/du;

    invoke-direct {v3, p0}, Lcom/sec/chaton/chat/du;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 727
    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 728
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1157
    iput-object p1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    .line 1158
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1159
    return-void
.end method

.method public c()V
    .locals 10

    .prologue
    const/16 v9, 0x3c

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 772
    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_1

    .line 773
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 774
    sget-object v0, Lcom/sec/chaton/util/bw;->a:Lcom/sec/chaton/util/bw;

    .line 775
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v3, v0}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    .line 777
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->u:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x2

    invoke-static {}, Lcom/sec/chaton/e/i;->c()Landroid/net/Uri;

    move-result-object v3

    const-string v5, "buddy_no = ? "

    new-array v6, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->A:Ljava/lang/String;

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    :cond_0
    :goto_0
    return-void

    .line 779
    :cond_1
    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_a

    .line 780
    :cond_2
    sget-object v0, Lcom/sec/chaton/util/bw;->c:Lcom/sec/chaton/util/bw;

    .line 781
    iget v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne v1, v3, :cond_c

    .line 782
    sget-object v0, Lcom/sec/chaton/util/bw;->b:Lcom/sec/chaton/util/bw;

    move-object v6, v0

    .line 784
    :goto_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const-string v3, "inbox_no=?"

    new-array v4, v8, [Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    aput-object v5, v4, v7

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 785
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gez v1, :cond_4

    .line 786
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v6}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    .line 787
    if-eqz v0, :cond_0

    .line 788
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 793
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 794
    const-string v1, "profile_url"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 795
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 799
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    if-nez v1, :cond_5

    .line 802
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->C:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/e/a/f;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 803
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->d:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "_group_profile.png_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 804
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 805
    invoke-virtual {p0, v2}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Ljava/io/File;)V

    goto/16 :goto_0

    .line 809
    :cond_5
    const-string v0, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 813
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->d:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_chat_profile.png_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 814
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 815
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 816
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 819
    :cond_6
    if-nez v1, :cond_7

    .line 822
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v6}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    goto/16 :goto_0

    .line 826
    :cond_7
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v8, :cond_8

    .line 827
    new-instance v0, Lcom/sec/chaton/chat/eo;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->R:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    iget v4, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v4}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v4

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/sec/chaton/chat/eo;-><init>(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->T:Lcom/sec/chaton/chat/eo;

    .line 828
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->T:Lcom/sec/chaton/chat/eo;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_chat_profile.png_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->P:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->O:Ljava/lang/String;

    move v1, v9

    move v2, v9

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/chat/eo;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->S:Lcom/sec/common/f/c;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->T:Lcom/sec/chaton/chat/eo;

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 833
    :cond_8
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->d:Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->Q:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 834
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 835
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 838
    :catch_0
    move-exception v0

    .line 839
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 844
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v6}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    goto/16 :goto_0

    .line 846
    :cond_a
    sget-object v0, Lcom/sec/chaton/util/bw;->b:Lcom/sec/chaton/util/bw;

    .line 849
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->d:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_chat_profile.png_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 851
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 852
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Ljava/io/File;)V

    goto/16 :goto_0

    .line 855
    :cond_b
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Lcom/sec/chaton/util/bw;)V

    goto/16 :goto_0

    :cond_c
    move-object v6, v0

    goto/16 :goto_1
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1252
    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/ChatInfoFragment;->b(Ljava/lang/String;)V

    .line 1253
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 867
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->D:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_0

    .line 868
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->D:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 870
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->g()V

    .line 872
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tmp_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpeg_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->F:Ljava/lang/String;

    .line 873
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->D:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->F:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    .line 874
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v0

    .line 875
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Create File] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->D:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->F:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->I:Landroid/net/Uri;

    .line 879
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->f()Z

    move-result v0

    if-nez v0, :cond_4

    .line 880
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->J:Landroid/widget/Toast;

    if-nez v0, :cond_2

    .line 881
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->J:Landroid/widget/Toast;

    .line 883
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->J:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 962
    :cond_3
    :goto_0
    return-void

    .line 888
    :cond_4
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-ne v0, v3, :cond_5

    .line 890
    const v0, 0x7f0d001d

    .line 895
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0126

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/chat/dl;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/dl;-><init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 954
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    .line 955
    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->K:Z

    if-eqz v1, :cond_3

    .line 956
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 959
    :catch_0
    move-exception v0

    .line 960
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 892
    :cond_5
    const v0, 0x7f0d001e

    goto :goto_1
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 1133
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->h()V

    .line 1134
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->G:Z

    return v0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 1138
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->h()V

    .line 1139
    iget-boolean v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->H:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 314
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 315
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cm;->a(Landroid/app/Activity;)V

    .line 316
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v5, -0x1

    const/16 v4, 0x258

    const/4 v3, 0x1

    .line 967
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 969
    packed-switch p1, :pswitch_data_0

    .line 1101
    :cond_0
    :goto_0
    return-void

    .line 971
    :pswitch_0
    if-ne p2, v5, :cond_0

    .line 972
    const-string v0, "receivers"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 973
    const-string v1, "MSCI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult.ACTIVITY_PURPOSE_INVITE_BUDDY.result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/chat/ChatActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 976
    const-string v2, "receivers"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 978
    const-string v0, "inboxNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 979
    const-string v0, "buddyNO"

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->A:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 980
    const-string v0, "chatType"

    iget v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 982
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v5, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 983
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/dw;

    invoke-interface {v0}, Lcom/sec/chaton/chat/dw;->c()V

    goto :goto_0

    .line 989
    :pswitch_1
    const-string v0, "backgroundStyle"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->w:Ljava/lang/String;

    .line 990
    const-string v0, "sendBubbleStyle"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->x:Ljava/lang/String;

    .line 991
    const-string v0, "receiveBubbleStyle"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->y:Ljava/lang/String;

    .line 993
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 994
    const-string v0, "inbox_is_change_skin"

    const-string v3, "Y"

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    const-string v0, "inbox_background_style"

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->w:Ljava/lang/String;

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    const-string v0, "inbox_send_bubble_style"

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->x:Ljava/lang/String;

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    const-string v0, "inbox_receive_bubble_style"

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->y:Ljava/lang/String;

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->u:Lcom/sec/chaton/e/a/u;

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inbox_no=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1003
    :pswitch_2
    if-ne p2, v5, :cond_0

    .line 1004
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->g:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    .line 1005
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1011
    :pswitch_3
    if-ne p2, v5, :cond_0

    .line 1012
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "temp_file_path"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1013
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1014
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-gtz v0, :cond_1

    .line 1015
    const-string v0, "Crop return null!"

    sget-object v1, Lcom/sec/chaton/chat/ChatInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1020
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    if-nez v0, :cond_2

    .line 1021
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->D:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->F:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    .line 1023
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    invoke-static {v1, v0}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 1024
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->N:Ljava/lang/String;

    .line 1026
    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne v0, v3, :cond_4

    .line 1027
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->U:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->N:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->L:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 1029
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->L:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto/16 :goto_0

    .line 1034
    :cond_4
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    iget v4, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v4}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    .line 1035
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "temp_file_path"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/16 v4, 0x258

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/util/ad;->b(Landroid/content/Context;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    const/16 v3, 0x258

    const/16 v4, 0x258

    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1036
    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->E:Ljava/io/File;

    invoke-static {v1, v3}, Lcom/sec/chaton/trunk/c/a;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    .line 1037
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1040
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_chat_profile.png_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 1043
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    .line 1044
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5a

    invoke-virtual {v0, v1, v3, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1045
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/global/GlobalApplication;->d:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1050
    if-eqz v2, :cond_0

    .line 1051
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 1053
    :catch_0
    move-exception v0

    .line 1054
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 1057
    :catch_1
    move-exception v0

    .line 1058
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1046
    :catch_2
    move-exception v0

    .line 1047
    :try_start_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1050
    if-eqz v2, :cond_0

    .line 1051
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 1053
    :catch_3
    move-exception v0

    .line 1054
    :try_start_6
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    .line 1049
    :catchall_0
    move-exception v0

    .line 1050
    if-eqz v2, :cond_5

    .line 1051
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    .line 1049
    :cond_5
    :goto_1
    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    .line 1067
    :pswitch_4
    if-nez p3, :cond_6

    .line 1068
    const-string v0, "Crop Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1072
    :cond_6
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->I:Landroid/net/Uri;

    .line 1073
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1074
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->I:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1075
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1076
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1077
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1078
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1079
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1080
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1084
    :pswitch_5
    if-ne p2, v5, :cond_7

    .line 1085
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1086
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->I:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1087
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1088
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1089
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1090
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1091
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1092
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1094
    :cond_7
    const-string v0, "Camera Return is NULL"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1053
    :catch_4
    move-exception v1

    .line 1054
    :try_start_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    goto/16 :goto_1

    .line 969
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 320
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 321
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 323
    if-eqz v0, :cond_0

    .line 324
    invoke-direct {p0, v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Landroid/os/Bundle;)V

    .line 327
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    .line 328
    iput-object p0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->i:Lcom/sec/chaton/chat/fc;

    .line 329
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->v:Landroid/widget/Toast;

    .line 331
    if-eqz p1, :cond_b

    .line 332
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 333
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->z:Z

    .line 335
    :cond_1
    const-string v0, "chatType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 336
    const-string v0, "chatType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    .line 338
    :cond_2
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 339
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    .line 341
    :cond_3
    const-string v0, "inbox_title_fixed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 342
    const-string v0, "inbox_title_fixed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->g:Ljava/lang/String;

    .line 344
    :cond_4
    const-string v0, "inboxNO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 345
    const-string v0, "inboxNO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    .line 347
    :cond_5
    const-string v0, "buddyNO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 348
    const-string v0, "buddyNO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->A:Ljava/lang/String;

    .line 350
    :cond_6
    const-string v0, "groupId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 351
    const-string v0, "groupId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->C:Ljava/lang/String;

    .line 353
    :cond_7
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 354
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->l:[Ljava/lang/String;

    .line 356
    :cond_8
    const-string v0, "inbox_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 357
    const-string v0, "inbox_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->M:Ljava/lang/String;

    .line 359
    :cond_9
    const-string v0, "captureUri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 360
    const-string v0, "captureUri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->I:Landroid/net/Uri;

    .line 362
    :cond_a
    const-string v0, "profileName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 363
    const-string v0, "profileName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->F:Ljava/lang/String;

    .line 367
    :cond_b
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f030028

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setContentView(I)V

    .line 369
    if-nez p1, :cond_c

    .line 370
    new-instance v0, Lcom/sec/chaton/buddy/BuddyFragment;

    invoke-direct {v0}, Lcom/sec/chaton/buddy/BuddyFragment;-><init>()V

    .line 372
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "PROFILE_BUDDY_FROM_CHATINFO"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 374
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/buddy/BuddyFragment;->setArguments(Landroid/os/Bundle;)V

    .line 375
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f070013

    const-string v3, "com.sec.chaton:single_pane_fragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 377
    :cond_c
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->c:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->u:Lcom/sec/chaton/e/a/u;

    .line 379
    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 381
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->g:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 382
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->g:Ljava/lang/String;

    const-string v2, "Y"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    if-lez v0, :cond_d

    .line 383
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    .line 395
    :cond_d
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->L:Landroid/app/ProgressDialog;

    if-nez v0, :cond_e

    .line 396
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->L:Landroid/app/ProgressDialog;

    .line 398
    :cond_e
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->S:Lcom/sec/common/f/c;

    .line 401
    invoke-static {p0, v4}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 402
    return-void

    .line 386
    :cond_f
    if-lez v0, :cond_d

    .line 387
    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 652
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653
    const v0, 0x7f0f0002

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 661
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 663
    return-void

    .line 657
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v3, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020304

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 1241
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 1242
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->L:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1243
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->L:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1245
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->S:Lcom/sec/common/f/c;

    if-eqz v0, :cond_1

    .line 1246
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->S:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 1248
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 667
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 684
    :goto_0
    return v3

    .line 672
    :sswitch_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 673
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->h:Landroid/content/Context;

    const v1, 0x7f0b0205

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 680
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->b()V

    goto :goto_0

    .line 667
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x7f070564 -> :sswitch_0
    .end sparse-switch
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 623
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 625
    iget v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_0

    .line 626
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->r:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 638
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->j()V

    .line 639
    return-void

    .line 628
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->u:Lcom/sec/chaton/e/a/u;

    const/4 v1, 0x4

    sget-object v3, Lcom/sec/chaton/e/o;->a:Landroid/net/Uri;

    const-string v5, "inbox_no=?"

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v7, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    aput-object v7, v6, v4

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 601
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 603
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->n:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 604
    const-string v0, "chatType"

    iget v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 605
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    const-string v0, "inbox_title_fixed"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    const-string v0, "inboxNO"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    const-string v0, "buddyNO"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    const-string v0, "groupId"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->l:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 611
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->l:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 613
    :cond_0
    const-string v0, "inbox_session_id"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->M:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    iget-object v0, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->I:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 615
    const-string v0, "captureUri"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->I:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    :cond_1
    const-string v0, "profileName"

    iget-object v1, p0, Lcom/sec/chaton/chat/ChatInfoFragment;->F:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 408
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 410
    invoke-virtual {p0}, Lcom/sec/chaton/chat/ChatInfoFragment;->a()V

    .line 411
    return-void
.end method
