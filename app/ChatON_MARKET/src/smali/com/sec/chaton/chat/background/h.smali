.class public Lcom/sec/chaton/chat/background/h;
.super Ljava/lang/Object;
.source "Request.java"


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:J

.field protected c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected d:Lcom/sec/chaton/chat/background/j;

.field protected e:I

.field protected f:I

.field protected g:Ljava/lang/String;

.field protected h:Ljava/lang/String;

.field protected i:J

.field protected j:Ljava/lang/String;

.field protected k:J

.field protected l:Ljava/lang/String;

.field protected m:Ljava/lang/String;

.field protected n:Ljava/lang/String;

.field protected o:Ljava/lang/String;

.field protected p:Lcom/sec/chaton/msgsend/k;

.field protected q:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/sec/chaton/chat/background/i;)V
    .locals 2

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    iget-object v0, p1, Lcom/sec/chaton/chat/background/i;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/h;->a:Ljava/lang/String;

    .line 179
    iget-wide v0, p1, Lcom/sec/chaton/chat/background/i;->b:J

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/h;->b:J

    .line 180
    iget-object v0, p1, Lcom/sec/chaton/chat/background/i;->c:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/h;->c:Ljava/util/ArrayList;

    .line 181
    iget v0, p1, Lcom/sec/chaton/chat/background/i;->g:I

    iput v0, p0, Lcom/sec/chaton/chat/background/h;->e:I

    .line 182
    iget v0, p1, Lcom/sec/chaton/chat/background/i;->h:I

    iput v0, p0, Lcom/sec/chaton/chat/background/h;->f:I

    .line 183
    iget-object v0, p1, Lcom/sec/chaton/chat/background/i;->d:Lcom/sec/chaton/chat/background/j;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/h;->d:Lcom/sec/chaton/chat/background/j;

    .line 184
    iget-object v0, p1, Lcom/sec/chaton/chat/background/i;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/h;->g:Ljava/lang/String;

    .line 185
    iget-object v0, p1, Lcom/sec/chaton/chat/background/i;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/h;->h:Ljava/lang/String;

    .line 186
    iget-wide v0, p1, Lcom/sec/chaton/chat/background/i;->i:J

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/h;->i:J

    .line 187
    iget-object v0, p1, Lcom/sec/chaton/chat/background/i;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/h;->j:Ljava/lang/String;

    .line 188
    iget-wide v0, p1, Lcom/sec/chaton/chat/background/i;->k:J

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/h;->k:J

    .line 189
    iget-object v0, p1, Lcom/sec/chaton/chat/background/i;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/h;->l:Ljava/lang/String;

    .line 190
    iget-object v0, p1, Lcom/sec/chaton/chat/background/i;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/h;->o:Ljava/lang/String;

    .line 191
    iget-object v0, p1, Lcom/sec/chaton/chat/background/i;->p:Lcom/sec/chaton/msgsend/k;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/h;->p:Lcom/sec/chaton/msgsend/k;

    .line 192
    iget-object v0, p1, Lcom/sec/chaton/chat/background/i;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/h;->q:Ljava/lang/String;

    .line 193
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/chaton/chat/background/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 200
    iget-wide v0, p0, Lcom/sec/chaton/chat/background/h;->b:J

    return-wide v0
.end method

.method public c()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/chaton/chat/background/h;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/h;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/sec/chaton/chat/background/h;->e:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/sec/chaton/chat/background/h;->f:I

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/chaton/chat/background/h;->g:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/chaton/chat/background/h;->h:Ljava/lang/String;

    return-object v0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 228
    iget-wide v0, p0, Lcom/sec/chaton/chat/background/h;->i:J

    return-wide v0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 232
    iget-wide v0, p0, Lcom/sec/chaton/chat/background/h;->k:J

    return-wide v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/chaton/chat/background/h;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/chaton/chat/background/h;->l:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/chaton/chat/background/h;->m:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/chaton/chat/background/h;->n:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/chaton/chat/background/h;->q:Ljava/lang/String;

    return-object v0
.end method
