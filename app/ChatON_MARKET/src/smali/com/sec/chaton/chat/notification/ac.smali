.class public Lcom/sec/chaton/chat/notification/ac;
.super Ljava/lang/Object;
.source "SensorChangeListenerOnCall.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean v0, p0, Lcom/sec/chaton/chat/notification/ac;->c:Z

    .line 36
    iput-boolean v0, p0, Lcom/sec/chaton/chat/notification/ac;->b:Z

    .line 37
    iput-object p1, p0, Lcom/sec/chaton/chat/notification/ac;->d:Ljava/lang/String;

    .line 38
    iput-boolean v1, p0, Lcom/sec/chaton/chat/notification/ac;->a:Z

    .line 39
    iput-boolean v1, p0, Lcom/sec/chaton/chat/notification/ac;->c:Z

    .line 40
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean v0, p0, Lcom/sec/chaton/chat/notification/ac;->c:Z

    .line 29
    iput-boolean p1, p0, Lcom/sec/chaton/chat/notification/ac;->b:Z

    .line 30
    iput-boolean v0, p0, Lcom/sec/chaton/chat/notification/ac;->a:Z

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/ac;->d:Ljava/lang/String;

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/chat/notification/ac;->c:Z

    .line 33
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/chat/notification/ac;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/sec/chaton/chat/notification/ac;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/ac;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Z)Lcom/sec/chaton/chat/notification/ac;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/sec/chaton/chat/notification/ac;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/ac;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/chaton/chat/notification/ac;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 52
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 54
    const-string v0, "do not show popup"

    const-string v1, "SensorListener"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unregister Listener. from noti:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/chat/notification/ac;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SensorListener"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 65
    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 66
    return-void

    .line 56
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/ac;->d:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/sec/chaton/chat/notification/ac;->a:Z

    iget-boolean v3, p0, Lcom/sec/chaton/chat/notification/ac;->b:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;ZZ)V

    goto :goto_0
.end method
