.class public Lcom/sec/chaton/chat/background/ChatBackgroundService;
.super Landroid/app/Service;
.source "ChatBackgroundService.java"


# instance fields
.field private a:Landroid/os/HandlerThread;

.field private b:Lcom/sec/chaton/chat/background/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 30
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ChatBackgroundService"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/background/ChatBackgroundService;->a:Landroid/os/HandlerThread;

    .line 31
    iget-object v0, p0, Lcom/sec/chaton/chat/background/ChatBackgroundService;->a:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 33
    new-instance v0, Lcom/sec/chaton/chat/background/a;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/ChatBackgroundService;->a:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/chaton/chat/background/a;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/background/ChatBackgroundService;->b:Lcom/sec/chaton/chat/background/a;

    .line 35
    const-string v0, "onCreate"

    const-string v1, "ChatBackgroundService"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 47
    iget-object v0, p0, Lcom/sec/chaton/chat/background/ChatBackgroundService;->b:Lcom/sec/chaton/chat/background/a;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/background/a;->a()Z

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/chat/background/ChatBackgroundService;->a:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 50
    const-string v0, "onDestory"

    const-string v1, "ChatBackgroundService"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v6, -0x1

    const-wide/16 v7, -0x1

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onStartCommand : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatBackgroundService"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    if-eqz p1, :cond_0

    .line 63
    const-string v0, "request"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    const-string v0, "request"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_0

    .line 67
    new-instance v1, Lcom/sec/chaton/chat/background/l;

    invoke-direct {v1}, Lcom/sec/chaton/chat/background/l;-><init>()V

    .line 69
    const-string v3, "request_id"

    invoke-virtual {v0, v3, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ChatBackgroundService"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "chat_type"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ChatBackgroundService"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v3, "media_type"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ChatBackgroundService"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v3, "content"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ChatBackgroundService"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v3, "packageName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ChatBackgroundService"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v3, "message_id"

    invoke-virtual {v0, v3, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ChatBackgroundService"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v3, "password"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ChatBackgroundService"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-virtual {v1, p3}, Lcom/sec/chaton/chat/background/l;->c(I)Lcom/sec/chaton/chat/background/l;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/chaton/chat/background/l;->a(Landroid/app/Service;)Lcom/sec/chaton/chat/background/l;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/chaton/chat/background/l;->d(I)Lcom/sec/chaton/chat/background/l;

    move-result-object v3

    const-string v4, "sender"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/chat/background/l;->a(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    const-string v4, "request_id"

    invoke-virtual {v0, v4, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/chat/background/i;->a(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    const-string v4, "message_id"

    invoke-virtual {v0, v4, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/chat/background/i;->b(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    const-string v4, "receivers"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/chat/background/i;->a([Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    const-string v4, "chat_type"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/chat/background/i;->a(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    const-string v4, "media_type"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/chat/background/i;->b(I)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    const-string v4, "content"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/chat/background/i;->b(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    const-string v4, "packageName"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/chat/background/i;->c(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v3

    const-string v4, "password"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/background/i;->h(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    .line 90
    invoke-virtual {v1}, Lcom/sec/chaton/chat/background/l;->b()Lcom/sec/chaton/chat/background/k;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/sec/chaton/chat/background/ChatBackgroundService;->b:Lcom/sec/chaton/chat/background/a;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/background/a;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 94
    iput v2, v1, Landroid/os/Message;->what:I

    .line 95
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/chat/background/ChatBackgroundService;->b:Lcom/sec/chaton/chat/background/a;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/background/a;->sendMessage(Landroid/os/Message;)Z

    .line 145
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0

    .line 100
    :cond_1
    const-string v0, "request3rdapp"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "request3rdapp"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 102
    if-eqz v3, :cond_0

    .line 104
    new-instance v4, Lcom/sec/chaton/chat/background/l;

    invoke-direct {v4}, Lcom/sec/chaton/chat/background/l;-><init>()V

    .line 106
    const-string v0, "request_id"

    invoke-virtual {v3, v0, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatBackgroundService"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v0, "inbox_no"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatBackgroundService"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v0, "message_id"

    invoke-virtual {v3, v0, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatBackgroundService"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v0, "packageName"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatBackgroundService"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v0, "inbox_no"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 114
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "inbox_no"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->h(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    move v1, v0

    .line 117
    :goto_1
    const/4 v0, 0x2

    .line 118
    if-lez v1, :cond_2

    .line 119
    const/4 v0, 0x1

    .line 122
    :cond_2
    invoke-virtual {v4, p3}, Lcom/sec/chaton/chat/background/l;->c(I)Lcom/sec/chaton/chat/background/l;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/chaton/chat/background/l;->a(Landroid/app/Service;)Lcom/sec/chaton/chat/background/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/chat/background/l;->d(I)Lcom/sec/chaton/chat/background/l;

    move-result-object v0

    const-string v1, "request_id"

    invoke-virtual {v3, v1, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/chaton/chat/background/l;->a(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v0

    const-string v1, "inbox_no"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/background/i;->i(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v0

    const-string v1, "message_id"

    invoke-virtual {v3, v1, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/chaton/chat/background/i;->b(J)Lcom/sec/chaton/chat/background/i;

    move-result-object v0

    const-string v1, "sender_id"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/background/i;->a(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v0

    const-string v1, "packageName"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/background/i;->c(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    move-result-object v0

    const-string v1, "password"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/background/i;->h(Ljava/lang/String;)Lcom/sec/chaton/chat/background/i;

    .line 132
    invoke-virtual {v4}, Lcom/sec/chaton/chat/background/l;->b()Lcom/sec/chaton/chat/background/k;

    move-result-object v0

    .line 135
    iget-object v1, p0, Lcom/sec/chaton/chat/background/ChatBackgroundService;->b:Lcom/sec/chaton/chat/background/a;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/background/a;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 136
    iput v2, v1, Landroid/os/Message;->what:I

    .line 137
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/chat/background/ChatBackgroundService;->b:Lcom/sec/chaton/chat/background/a;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/background/a;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method
