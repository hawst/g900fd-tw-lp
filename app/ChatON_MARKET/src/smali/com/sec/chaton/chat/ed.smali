.class Lcom/sec/chaton/chat/ed;
.super Ljava/lang/Object;
.source "ChatInfoMoreFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/b/d;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/eb;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/eb;)V
    .locals 0

    .prologue
    .line 481
    iput-object p1, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IZLjava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 485
    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 516
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    if-eqz p2, :cond_0

    .line 489
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 490
    instance-of v0, p3, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    .line 491
    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    invoke-static {v1}, Lcom/sec/chaton/chat/eb;->b(Lcom/sec/chaton/chat/eb;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/eb;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 493
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0196

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    invoke-static {v4}, Lcom/sec/chaton/chat/eb;->b(Lcom/sec/chaton/chat/eb;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 498
    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->l(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->f(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->b(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 500
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->c(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/n;->i(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 501
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "already exist relation inbox:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->c(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->g(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)V

    .line 509
    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/eh;

    invoke-interface {v0}, Lcom/sec/chaton/chat/eh;->c()V

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    invoke-static {v0}, Lcom/sec/chaton/chat/eb;->c(Lcom/sec/chaton/chat/eb;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0210

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 503
    :cond_5
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->c(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/lang/String;

    move-result-object v1

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ed;->a:Lcom/sec/chaton/chat/eb;

    iget-object v3, v3, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->l(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;ILcom/sec/chaton/e/r;)Landroid/net/Uri;

    goto :goto_1
.end method
