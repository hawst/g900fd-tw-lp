.class Lcom/sec/chaton/chat/an;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 4984
    iput-object p1, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 4987
    iget-object v0, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v0, v0, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/msgsend/p;->a(Ljava/lang/String;)V

    .line 4988
    iget-object v0, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "inbox_no IN (\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v5, v5, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 4989
    iget-object v0, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/4 v1, 0x2

    sget-object v3, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "message_inbox_no=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v5, v5, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 4990
    iget-object v0, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/4 v1, 0x3

    sget-object v3, Lcom/sec/chaton/e/z;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "participants_inbox_no=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v5, v5, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 4992
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/a;->a(Ljava/lang/String;)Z

    .line 4993
    invoke-static {}, Lcom/sec/chaton/j/c/g;->a()Lcom/sec/chaton/j/c/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/g;->a(Ljava/lang/String;)Z

    .line 4994
    iget-object v0, p0, Lcom/sec/chaton/chat/an;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->U(Lcom/sec/chaton/chat/ChatFragment;)V

    .line 4995
    return-void
.end method
