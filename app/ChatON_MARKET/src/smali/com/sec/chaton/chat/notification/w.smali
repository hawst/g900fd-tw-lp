.class public Lcom/sec/chaton/chat/notification/w;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "ScreenNotification2.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/notification/ScreenNotification2;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;Landroid/support/v4/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 983
    iput-object p1, p0, Lcom/sec/chaton/chat/notification/w;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    .line 984
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 985
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    const/16 v0, 0x63

    .line 1008
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/w;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->f(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v1

    iget v1, v1, Lcom/sec/chaton/chat/notification/a;->m:I

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/notification/w;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->f(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget v0, v0, Lcom/sec/chaton/chat/notification/a;->m:I

    :cond_0
    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 990
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/w;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->f(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/chaton/chat/notification/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/notification/g;

    .line 991
    new-instance v1, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-direct {v1}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;-><init>()V

    .line 992
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 993
    const-string v3, "message"

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/g;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    const-string v3, "msgType"

    iget v4, v0, Lcom/sec/chaton/chat/notification/g;->g:I

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 995
    const-string v3, "no"

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    const-string v3, "name"

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/g;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    const-string v3, "truncated"

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/g;->m:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    const-string v3, "msgID"

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/g;->n:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    const-string v3, "inboxNO"

    iget-object v4, v0, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    const-string v3, "chatType"

    iget v0, v0, Lcom/sec/chaton/chat/notification/g;->f:I

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1001
    invoke-virtual {v1, v2}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->setArguments(Landroid/os/Bundle;)V

    .line 1003
    return-object v1
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1014
    const/4 v0, -0x2

    return v0
.end method
