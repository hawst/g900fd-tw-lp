.class Lcom/sec/chaton/chat/dn;
.super Ljava/lang/Object;
.source "ChatInfoFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 300
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->n(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/Toast;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/chat/ChatInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0167

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->n(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->n(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 305
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 306
    const-string v0, "inbox_last_message"

    const-string v3, ""

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->p(Lcom/sec/chaton/chat/ChatInfoFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inbox_no=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v6}, Lcom/sec/chaton/chat/ChatInfoFragment;->o(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 309
    :cond_0
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 290
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 236
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    .line 237
    if-eqz p3, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Buddy information exist : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->h(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    const-string v1, "buddy_profile_status"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Lcom/sec/chaton/chat/ChatInfoFragment;I)I

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->i(Lcom/sec/chaton/chat/ChatInfoFragment;)I

    move-result v0

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_UPDATED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v1}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->i(Lcom/sec/chaton/chat/ChatInfoFragment;)I

    move-result v0

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NOT_CHANGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v1}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->i(Lcom/sec/chaton/chat/ChatInfoFragment;)I

    move-result v0

    sget-object v1, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_DELETED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v1}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 242
    :cond_0
    new-instance v0, Lcom/sec/chaton/d/h;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v0}, Lcom/sec/chaton/d/h;->a()V

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->j(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatInfoFragment;->h(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatInfoFragment;->i(Lcom/sec/chaton/chat/ChatInfoFragment;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;I)V

    .line 282
    :cond_1
    :goto_0
    if-eqz p3, :cond_2

    .line 283
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 285
    :cond_2
    return-void

    .line 246
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Buddy information not found : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->h(Lcom/sec/chaton/chat/ChatInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/ChatInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 248
    :cond_4
    const/16 v0, 0x46

    if-ne p1, v0, :cond_6

    .line 249
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 251
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 253
    const-string v1, "ACTIVITY_PURPOSE"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 254
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatInfoFragment;->k(Lcom/sec/chaton/chat/ChatInfoFragment;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    const-string v1, "chatType"

    iget-object v2, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatInfoFragment;->l(Lcom/sec/chaton/chat/ChatInfoFragment;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 257
    iget-object v1, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-virtual {v1, v0, v4}, Lcom/sec/chaton/chat/ChatInfoFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 259
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 260
    const v1, 0x7f0b02ba

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0078

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b035a

    new-instance v2, Lcom/sec/chaton/chat/do;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/do;-><init>(Lcom/sec/chaton/chat/dn;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 268
    :cond_6
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 270
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_7

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->m(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/Button;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 273
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->m(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->k(Lcom/sec/chaton/chat/ChatInfoFragment;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_8

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->m(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 277
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/dn;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoFragment;->m(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 295
    return-void
.end method
