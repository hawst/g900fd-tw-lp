.class Lcom/sec/chaton/chat/ek;
.super Ljava/lang/Object;
.source "ChatListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/el;

.field final synthetic b:Lcom/sec/chaton/chat/ei;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ei;Lcom/sec/chaton/chat/el;)V
    .locals 0

    .prologue
    .line 705
    iput-object p1, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    iput-object p2, p0, Lcom/sec/chaton/chat/ek;->a:Lcom/sec/chaton/chat/el;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 709
    check-cast p1, Landroid/widget/CheckBox;

    .line 710
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 712
    iget-object v1, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v1}, Lcom/sec/chaton/chat/ei;->a(Lcom/sec/chaton/chat/ei;)Lcom/sec/chaton/chat/em;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/em;->a:Lcom/sec/chaton/chat/em;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v1}, Lcom/sec/chaton/chat/ei;->a(Lcom/sec/chaton/chat/ei;)Lcom/sec/chaton/chat/em;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/em;->b:Lcom/sec/chaton/chat/em;

    if-ne v1, v2, :cond_3

    .line 713
    :cond_0
    if-eqz v0, :cond_2

    .line 714
    iget-object v0, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v0}, Lcom/sec/chaton/chat/ei;->f(Lcom/sec/chaton/chat/ei;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ek;->a:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 718
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v0}, Lcom/sec/chaton/chat/ei;->c(Lcom/sec/chaton/chat/ei;)V

    .line 728
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v0}, Lcom/sec/chaton/chat/ei;->e(Lcom/sec/chaton/chat/ei;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 729
    return-void

    .line 716
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v0}, Lcom/sec/chaton/chat/ei;->f(Lcom/sec/chaton/chat/ei;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ek;->a:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 719
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v1}, Lcom/sec/chaton/chat/ei;->a(Lcom/sec/chaton/chat/ei;)Lcom/sec/chaton/chat/em;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/em;->c:Lcom/sec/chaton/chat/em;

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v1}, Lcom/sec/chaton/chat/ei;->a(Lcom/sec/chaton/chat/ei;)Lcom/sec/chaton/chat/em;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/em;->d:Lcom/sec/chaton/chat/em;

    if-ne v1, v2, :cond_1

    .line 720
    :cond_4
    if-eqz v0, :cond_5

    .line 721
    iget-object v0, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v0}, Lcom/sec/chaton/chat/ei;->f(Lcom/sec/chaton/chat/ei;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ek;->a:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 725
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v0}, Lcom/sec/chaton/chat/ei;->d(Lcom/sec/chaton/chat/ei;)V

    goto :goto_1

    .line 723
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ek;->b:Lcom/sec/chaton/chat/ei;

    invoke-static {v0}, Lcom/sec/chaton/chat/ei;->f(Lcom/sec/chaton/chat/ei;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ek;->a:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method
