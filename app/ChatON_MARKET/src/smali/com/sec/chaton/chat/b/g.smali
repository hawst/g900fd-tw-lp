.class public Lcom/sec/chaton/chat/b/g;
.super Ljava/lang/Object;
.source "SentenceBreaker.java"


# instance fields
.field private a:Lcom/sec/chaton/chat/b/h;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/16 v0, 0x76c

    iput v0, p0, Lcom/sec/chaton/chat/b/g;->b:I

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    const/16 v0, 0x2710

    .line 30
    if-gez p1, :cond_1

    .line 31
    const/4 v1, 0x0

    .line 34
    :goto_0
    if-le v1, v0, :cond_0

    .line 38
    :goto_1
    iput v0, p0, Lcom/sec/chaton/chat/b/g;->b:I

    .line 39
    return-void

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v1, p1

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/chat/b/h;)V
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lcom/sec/chaton/chat/b/g;->a:Lcom/sec/chaton/chat/b/h;

    .line 14
    return-void
.end method

.method public a(Ljava/util/Locale;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 47
    if-nez p2, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Source is null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/b/g;->a:Lcom/sec/chaton/chat/b/h;

    if-nez v0, :cond_1

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SentenceListener is null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_1
    const-string v0, ""

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Source is empty!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_2
    invoke-static {p1}, Ljava/text/BreakIterator;->getSentenceInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v5

    .line 62
    invoke-virtual {v5, p2}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 64
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    invoke-virtual {v5}, Ljava/text/BreakIterator;->first()I

    move-result v2

    .line 69
    invoke-virtual {v5}, Ljava/text/BreakIterator;->next()I

    move-result v0

    move v4, v1

    move v9, v0

    move-object v0, v3

    move v3, v9

    :goto_0
    const/4 v6, -0x1

    if-eq v3, v6, :cond_3

    .line 71
    sub-int v6, v3, v2

    iget v7, p0, Lcom/sec/chaton/chat/b/g;->b:I

    if-le v6, v7, :cond_5

    .line 94
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 96
    :try_start_0
    iget-object v3, p0, Lcom/sec/chaton/chat/b/g;->a:Lcom/sec/chaton/chat/b/h;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/sec/chaton/chat/b/h;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 104
    :cond_4
    if-nez v4, :cond_9

    .line 106
    iget v0, p0, Lcom/sec/chaton/chat/b/g;->b:I

    .line 108
    iget v3, p0, Lcom/sec/chaton/chat/b/g;->b:I

    if-nez v3, :cond_7

    .line 109
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "threshold is zero!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_5
    const/4 v4, 0x1

    .line 77
    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 78
    const-string v6, ""

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 79
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    iget v6, p0, Lcom/sec/chaton/chat/b/g;->b:I

    if-lt v2, v6, :cond_6

    .line 82
    :try_start_1
    iget-object v2, p0, Lcom/sec/chaton/chat/b/g;->a:Lcom/sec/chaton/chat/b/h;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/sec/chaton/chat/b/h;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    :cond_6
    invoke-virtual {v5}, Ljava/text/BreakIterator;->next()I

    move-result v2

    move v9, v2

    move v2, v3

    move v3, v9

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SentenceListener error!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 87
    :catchall_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    throw v0

    .line 97
    :catch_1
    move-exception v0

    .line 98
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SentenceListener error!!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v9, v1

    move v1, v0

    move v0, v9

    .line 112
    :goto_1
    int-to-double v3, v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    int-to-double v5, v5

    iget v7, p0, Lcom/sec/chaton/chat/b/g;->b:I

    int-to-double v7, v7

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    cmpg-double v3, v3, v5

    if-gez v3, :cond_9

    .line 114
    iget-object v3, p0, Lcom/sec/chaton/chat/b/g;->a:Lcom/sec/chaton/chat/b/h;

    invoke-virtual {p2, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Lcom/sec/chaton/chat/b/h;->a(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v1

    iget v3, p0, Lcom/sec/chaton/chat/b/g;->b:I

    div-int/2addr v2, v3

    if-lez v2, :cond_8

    .line 118
    iget v2, p0, Lcom/sec/chaton/chat/b/g;->b:I

    add-int/2addr v2, v1

    .line 112
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move v9, v2

    move v2, v1

    move v1, v9

    goto :goto_1

    .line 122
    :cond_8
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v1

    iget v3, p0, Lcom/sec/chaton/chat/b/g;->b:I

    rem-int/2addr v2, v3

    add-int/2addr v2, v1

    goto :goto_2

    .line 126
    :cond_9
    return-void
.end method
