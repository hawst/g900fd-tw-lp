.class public Lcom/sec/chaton/chat/background/g;
.super Landroid/os/Handler;
.source "JobContainer.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/background/f;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/background/f;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 675
    iput-object p1, p0, Lcom/sec/chaton/chat/background/g;->a:Lcom/sec/chaton/chat/background/f;

    .line 676
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 677
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 681
    if-nez p1, :cond_1

    .line 731
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 685
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    .line 686
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 699
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    .line 700
    invoke-virtual {v0}, Lcom/sec/chaton/chat/fj;->b()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/chaton/chat/background/g;->a:Lcom/sec/chaton/chat/background/f;

    iget-wide v3, v3, Lcom/sec/chaton/chat/background/f;->o:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 703
    invoke-virtual {v0}, Lcom/sec/chaton/chat/fj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 705
    if-nez v0, :cond_2

    .line 706
    iget-object v0, p0, Lcom/sec/chaton/chat/background/g;->a:Lcom/sec/chaton/chat/background/f;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/g;->a:Lcom/sec/chaton/chat/background/f;

    iget-wide v1, v1, Lcom/sec/chaton/chat/background/f;->l:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/background/f;->a(J)Z

    goto :goto_0

    .line 710
    :cond_2
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    .line 711
    if-eqz v1, :cond_3

    .line 718
    iget-object v2, p0, Lcom/sec/chaton/chat/background/g;->a:Lcom/sec/chaton/chat/background/f;

    const/4 v3, 0x1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v4

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/sec/chaton/chat/background/f;->a(ZIJ)Z

    .line 719
    iget-object v0, p0, Lcom/sec/chaton/chat/background/g;->a:Lcom/sec/chaton/chat/background/f;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/g;->a:Lcom/sec/chaton/chat/background/f;

    iget-wide v1, v1, Lcom/sec/chaton/chat/background/f;->l:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/background/f;->a(J)Z

    goto :goto_0

    .line 724
    :cond_3
    iget-object v2, p0, Lcom/sec/chaton/chat/background/g;->a:Lcom/sec/chaton/chat/background/f;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v4

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/sec/chaton/chat/background/f;->a(ZIJ)Z

    .line 725
    iget-object v0, p0, Lcom/sec/chaton/chat/background/g;->a:Lcom/sec/chaton/chat/background/f;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/g;->a:Lcom/sec/chaton/chat/background/f;

    iget-wide v1, v1, Lcom/sec/chaton/chat/background/f;->l:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/background/f;->a(J)Z

    goto :goto_0

    .line 686
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x65 -> :sswitch_1
        0x66 -> :sswitch_1
        0x6a -> :sswitch_1
    .end sparse-switch
.end method
