.class public final enum Lcom/sec/chaton/chat/background/j;
.super Ljava/lang/Enum;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/chat/background/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/chat/background/j;

.field public static final enum b:Lcom/sec/chaton/chat/background/j;

.field public static final enum c:Lcom/sec/chaton/chat/background/j;

.field private static final synthetic e:[Lcom/sec/chaton/chat/background/j;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 8
    new-instance v0, Lcom/sec/chaton/chat/background/j;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/chaton/chat/background/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/chat/background/j;->a:Lcom/sec/chaton/chat/background/j;

    .line 9
    new-instance v0, Lcom/sec/chaton/chat/background/j;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/chaton/chat/background/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/chat/background/j;->b:Lcom/sec/chaton/chat/background/j;

    .line 10
    new-instance v0, Lcom/sec/chaton/chat/background/j;

    const-string v1, "MULITIFUL"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/chaton/chat/background/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/chat/background/j;->c:Lcom/sec/chaton/chat/background/j;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/chat/background/j;

    sget-object v1, Lcom/sec/chaton/chat/background/j;->a:Lcom/sec/chaton/chat/background/j;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/chat/background/j;->b:Lcom/sec/chaton/chat/background/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/chat/background/j;->c:Lcom/sec/chaton/chat/background/j;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/chaton/chat/background/j;->e:[Lcom/sec/chaton/chat/background/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput p3, p0, Lcom/sec/chaton/chat/background/j;->d:I

    .line 16
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/chat/background/j;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/sec/chaton/chat/background/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/background/j;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/chat/background/j;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/sec/chaton/chat/background/j;->e:[Lcom/sec/chaton/chat/background/j;

    invoke-virtual {v0}, [Lcom/sec/chaton/chat/background/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/chat/background/j;

    return-object v0
.end method
