.class Lcom/sec/chaton/chat/ey;
.super Landroid/os/Handler;
.source "EditChatTitleDialog.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ex;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ex;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/chaton/chat/ey;->a:Lcom/sec/chaton/chat/ex;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 77
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 98
    :goto_0
    return-void

    .line 81
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 83
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 84
    iget-object v1, p0, Lcom/sec/chaton/chat/ey;->a:Lcom/sec/chaton/chat/ex;

    iget-object v1, v1, Lcom/sec/chaton/chat/ex;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/sec/chaton/chat/ey;->a:Lcom/sec/chaton/chat/ex;

    iget-object v1, v1, Lcom/sec/chaton/chat/ex;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 87
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_2

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/chat/ey;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v0}, Lcom/sec/chaton/chat/ex;->a(Lcom/sec/chaton/chat/ex;)V

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/chat/ey;->a:Lcom/sec/chaton/chat/ex;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ex;->dismiss()V

    goto :goto_0

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ey;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v0}, Lcom/sec/chaton/chat/ex;->b(Lcom/sec/chaton/chat/ex;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 93
    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0097

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x326
        :pswitch_0
    .end packed-switch
.end method
