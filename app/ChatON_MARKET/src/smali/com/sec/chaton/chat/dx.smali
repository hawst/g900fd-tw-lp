.class Lcom/sec/chaton/chat/dx;
.super Landroid/os/Handler;
.source "ChatInfoMoreFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const v4, 0x7f0b00be

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 139
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 141
    iget-object v1, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 246
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_14

    .line 146
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x137

    if-ne v1, v2, :cond_a

    .line 148
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_d

    .line 149
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 150
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetBuddyList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetBuddyList;->buddy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Buddy;

    .line 151
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->result:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 152
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    .line 153
    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    const-string v5, "+"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 154
    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 156
    :cond_2
    iput-object v1, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    .line 158
    iget-object v2, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "member_name"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 159
    iget-object v2, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "member_name"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 160
    iput-object v1, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    move v2, v3

    .line 161
    :goto_2
    iget-object v6, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v6}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->b(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)[Ljava/lang/String;

    move-result-object v6

    array-length v6, v6

    if-ge v2, v6, :cond_3

    .line 162
    iget-object v6, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v6}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->b(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v2

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 163
    aget-object v1, v5, v2

    iput-object v1, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    .line 171
    :cond_3
    :goto_3
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 172
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Blocked Buddy NO : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 161
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 169
    :cond_6
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v5}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->c(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5, v1}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    goto :goto_3

    .line 178
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->e(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->e(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->k()Lcom/sec/widget/FastScrollableExpandableListView;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->e(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Lcom/sec/chaton/buddy/BuddyFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/BuddyFragment;->q()V

    .line 184
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Buddy;

    .line 187
    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 189
    :cond_9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    const v2, 0x7f0b00c9

    invoke-virtual {v0, v2}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 191
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 196
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->f(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_c

    .line 197
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_a

    .line 198
    const-string v0, "All buddies are blocked"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_a
    :goto_6
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->g(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)V

    goto/16 :goto_0

    .line 193
    :cond_b
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01fb

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_5

    .line 201
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->a()V

    goto :goto_6

    .line 204
    :cond_d
    iget-object v1, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 205
    const-string v1, ""

    .line 206
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 207
    const/4 v2, -0x3

    if-eq v2, v1, :cond_e

    const/4 v2, -0x2

    if-ne v2, v1, :cond_f

    .line 208
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 234
    :goto_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_6

    .line 211
    :cond_f
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e83

    if-ne v1, v2, :cond_10

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 215
    :cond_10
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e84

    if-ne v1, v2, :cond_11

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 218
    :cond_11
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e85

    if-ne v1, v2, :cond_12

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 224
    :cond_12
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const v1, 0xc355

    if-ne v0, v1, :cond_13

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0289

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 229
    :cond_13
    iget-object v0, p0, Lcom/sec/chaton/chat/dx;->a:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 241
    :cond_14
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_6
.end method
