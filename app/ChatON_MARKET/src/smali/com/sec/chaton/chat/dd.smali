.class Lcom/sec/chaton/chat/dd;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/sec/chaton/chat/cy;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/cy;Ljava/io/File;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 4165
    iput-object p1, p0, Lcom/sec/chaton/chat/dd;->c:Lcom/sec/chaton/chat/cy;

    iput-object p2, p0, Lcom/sec/chaton/chat/dd;->a:Ljava/io/File;

    iput-object p3, p0, Lcom/sec/chaton/chat/dd;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 4168
    const-wide/16 v2, 0x0

    move v0, v1

    .line 4169
    :goto_0
    iget-object v4, p0, Lcom/sec/chaton/chat/dd;->c:Lcom/sec/chaton/chat/cy;

    iget-object v4, v4, Lcom/sec/chaton/chat/cy;->h:[Ljava/io/File;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 4170
    iget-object v4, p0, Lcom/sec/chaton/chat/dd;->c:Lcom/sec/chaton/chat/cy;

    iget-object v4, v4, Lcom/sec/chaton/chat/cy;->h:[Ljava/io/File;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4171
    iget-object v4, p0, Lcom/sec/chaton/chat/dd;->c:Lcom/sec/chaton/chat/cy;

    iget-object v4, v4, Lcom/sec/chaton/chat/cy;->h:[Ljava/io/File;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 4172
    iget-object v4, p0, Lcom/sec/chaton/chat/dd;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v4, v2

    const-wide/32 v6, 0xa00000

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 4173
    iget-object v4, p0, Lcom/sec/chaton/chat/dd;->c:Lcom/sec/chaton/chat/cy;

    iget-object v4, v4, Lcom/sec/chaton/chat/cy;->i:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/chat/dd;->c:Lcom/sec/chaton/chat/cy;

    iget-object v5, v5, Lcom/sec/chaton/chat/cy;->h:[Ljava/io/File;

    aget-object v5, v5, v0

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4169
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4177
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4178
    const-string v2, "plain/text"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 4179
    const-string v2, "android.intent.extra.SUBJECT"

    iget-object v3, p0, Lcom/sec/chaton/chat/dd;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4180
    const-string v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Lcom/sec/chaton/chat/dd;->c:Lcom/sec/chaton/chat/cy;

    iget-object v3, v3, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    const v4, 0x7f0b01ed

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/chaton/chat/dd;->c:Lcom/sec/chaton/chat/cy;

    iget-object v6, v6, Lcom/sec/chaton/chat/cy;->b:Ljava/lang/String;

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/chat/ChatFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4181
    const-string v2, "android.intent.extra.STREAM"

    iget-object v3, p0, Lcom/sec/chaton/chat/dd;->c:Lcom/sec/chaton/chat/cy;

    iget-object v3, v3, Lcom/sec/chaton/chat/cy;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 4184
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/chat/dd;->c:Lcom/sec/chaton/chat/cy;

    iget-object v2, v2, Lcom/sec/chaton/chat/cy;->m:Lcom/sec/chaton/chat/ChatFragment;

    const/16 v3, 0x17

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4191
    :cond_2
    :goto_1
    return-void

    .line 4185
    :catch_0
    move-exception v0

    .line 4186
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0414

    invoke-static {v2, v3, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 4187
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 4188
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method
