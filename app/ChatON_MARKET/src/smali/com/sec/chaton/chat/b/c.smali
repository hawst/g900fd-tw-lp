.class public Lcom/sec/chaton/chat/b/c;
.super Ljava/lang/Object;
.source "LanguageTranslator.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static h:Lcom/sec/chaton/chat/b/c;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private final f:Ljava/lang/Object;

.field private g:Ljava/lang/String;

.field private i:Lcom/sec/chaton/chat/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/chaton/chat/b/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/chat/b/c;->b:I

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->c:Ljava/lang/String;

    .line 36
    iput-object v1, p0, Lcom/sec/chaton/chat/b/c;->d:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/sec/chaton/chat/b/c;->e:Ljava/lang/String;

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->f:Ljava/lang/Object;

    .line 40
    iput-object v1, p0, Lcom/sec/chaton/chat/b/c;->g:Ljava/lang/String;

    .line 88
    new-instance v0, Lcom/sec/chaton/chat/b/a;

    invoke-direct {v0}, Lcom/sec/chaton/chat/b/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->i:Lcom/sec/chaton/chat/b/a;

    .line 89
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/b/c;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/chaton/chat/b/c;->b:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/b/c;I)I
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/chaton/chat/b/c;->b:I

    return p1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 501
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 502
    const/4 v0, 0x0

    .line 504
    :try_start_0
    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 511
    :cond_0
    :goto_0
    return-object v0

    .line 505
    :catch_0
    move-exception v1

    .line 506
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 507
    sget-object v2, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 460
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/plugin/h;->c:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, p0, v1}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/d/a/ee;
    .locals 4

    .prologue
    .line 234
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/chat/b/c;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/d/a/ee;

    move-result-object v1

    .line 235
    sget-object v0, Lcom/sec/chaton/chat/b/m;->f:Lcom/sec/chaton/chat/b/m;

    .line 236
    if-eqz v1, :cond_0

    .line 237
    invoke-virtual {v1}, Lcom/sec/chaton/d/a/ee;->a()Lcom/sec/chaton/chat/b/m;

    move-result-object v0

    .line 240
    :cond_0
    sget-object v2, Lcom/sec/chaton/chat/b/e;->a:[I

    invoke-virtual {v0}, Lcom/sec/chaton/chat/b/m;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move-object v0, v1

    .line 268
    :goto_0
    return-object v0

    :pswitch_0
    move-object v0, v1

    .line 242
    goto :goto_0

    .line 245
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/b/c;->a(Z)Z

    .line 246
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/chat/b/c;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/d/a/ee;

    move-result-object v0

    goto :goto_0

    .line 250
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/chat/b/f;->b:Lcom/sec/chaton/chat/b/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/b/c;->a(Lcom/sec/chaton/chat/b/f;)V

    .line 251
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/chat/b/c;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/d/a/ee;

    move-result-object v0

    goto :goto_0

    .line 254
    :pswitch_3
    sget-object v1, Lcom/sec/chaton/chat/b/f;->b:Lcom/sec/chaton/chat/b/f;

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/b/c;->a(Lcom/sec/chaton/chat/b/f;)V

    .line 255
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/chat/b/c;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/d/a/ee;

    move-result-object v1

    .line 256
    sget-object v2, Lcom/sec/chaton/chat/b/m;->c:Lcom/sec/chaton/chat/b/m;

    .line 257
    if-eqz v1, :cond_1

    .line 258
    invoke-virtual {v1}, Lcom/sec/chaton/d/a/ee;->a()Lcom/sec/chaton/chat/b/m;

    .line 260
    :cond_1
    if-ne v0, v2, :cond_2

    .line 261
    invoke-virtual {p0}, Lcom/sec/chaton/chat/b/c;->e()V

    :cond_2
    move-object v0, v1

    .line 263
    goto :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static declared-synchronized c()Lcom/sec/chaton/chat/b/c;
    .locals 2

    .prologue
    .line 80
    const-class v1, Lcom/sec/chaton/chat/b/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/chat/b/c;->h:Lcom/sec/chaton/chat/b/c;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Lcom/sec/chaton/chat/b/c;

    invoke-direct {v0}, Lcom/sec/chaton/chat/b/c;-><init>()V

    sput-object v0, Lcom/sec/chaton/chat/b/c;->h:Lcom/sec/chaton/chat/b/c;

    .line 84
    :cond_0
    sget-object v0, Lcom/sec/chaton/chat/b/c;->h:Lcom/sec/chaton/chat/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 273
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(doTranslateSync)text is empty : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const/4 v0, 0x0

    .line 348
    :goto_0
    return-object v0

    .line 288
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/b/c;->a(Z)Z

    move-result v0

    .line 289
    if-nez v0, :cond_1

    .line 290
    const-string v0, "setAuthen error (1)"

    sget-object v1, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/b/c;->a(Z)Z

    move-result v0

    .line 292
    if-nez v0, :cond_1

    .line 293
    const-string v0, "setAuthen error (2), do nothing more"

    sget-object v1, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const/4 v0, 0x0

    goto :goto_0

    .line 299
    :cond_1
    const/4 v0, 0x0

    .line 300
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 301
    const/16 v1, 0x44c

    if-le v3, v1, :cond_2

    .line 302
    const-string v0, "sentenceBreaker start!!"

    sget-object v1, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const/4 v0, 0x1

    .line 306
    :cond_2
    const-string v1, "textLength(%d), isSentenceBreaking(%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    if-nez v0, :cond_3

    .line 309
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/chat/b/c;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/d/a/ee;

    move-result-object v0

    .line 310
    invoke-virtual {v0}, Lcom/sec/chaton/d/a/ee;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 313
    :cond_3
    const/16 v0, 0x3e8

    invoke-virtual {p0, p1, v0}, Lcom/sec/chaton/chat/b/c;->a(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    .line 314
    const-string v1, ""

    .line 315
    if-nez v4, :cond_4

    .line 316
    const-string v0, "sentenceArray is null"

    sget-object v1, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const/4 v0, 0x0

    goto :goto_0

    .line 320
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sentenceArray.length: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v2, v4

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const/4 v0, 0x0

    move v11, v0

    move-object v0, v1

    move v1, v11

    :goto_1
    array-length v2, v4

    if-ge v1, v2, :cond_8

    .line 322
    aget-object v5, v4, v1

    .line 324
    invoke-direct {p0, v5, p2, p3}, Lcom/sec/chaton/chat/b/c;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/d/a/ee;

    move-result-object v2

    .line 325
    invoke-virtual {v2}, Lcom/sec/chaton/d/a/ee;->a()Lcom/sec/chaton/chat/b/m;

    move-result-object v6

    sget-object v7, Lcom/sec/chaton/chat/b/m;->a:Lcom/sec/chaton/chat/b/m;

    if-eq v6, v7, :cond_5

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to translate HTTP : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 330
    :cond_5
    invoke-virtual {v2}, Lcom/sec/chaton/d/a/ee;->b()Ljava/lang/String;

    move-result-object v6

    .line 331
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 333
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_7

    .line 334
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    .line 335
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    .line 336
    const/4 v0, 0x0

    .line 337
    if-eqz v6, :cond_6

    .line 338
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    .line 341
    :cond_6
    const-string v6, "[%d/%d] origin text piece(%d) / trans text piece(%d), total trans current (%d)"

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    add-int/lit8 v10, v1, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    array-length v10, v4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v8, v9

    const/4 v7, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v8, v7

    const/4 v5, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v5

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 343
    sget-object v5, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto/16 :goto_1

    .line 347
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAST. origin text length : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", trans text length : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/d/a/ee;
    .locals 2

    .prologue
    .line 352
    invoke-static {p2, p3}, Lcom/sec/chaton/chat/b/c;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 355
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/b/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/sec/chaton/d/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/d/a/eb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/eb;->b()Lcom/sec/chaton/a/a/f;

    move-result-object v0

    .line 361
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/ee;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 367
    :goto_0
    return-object v0

    .line 363
    :catch_0
    move-exception v0

    .line 364
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 367
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    iget-object v1, p0, Lcom/sec/chaton/chat/b/c;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->c:Ljava/lang/String;

    monitor-exit v1

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/Long;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x6

    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 107
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "translateChatSync(), "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "isForced(%s), meLang(%s), buddyLang(%s), inboxNO(%s), msgId(%d)"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    aput-object p4, v4, v5

    const/4 v5, 0x2

    aput-object p5, v4, v5

    const/4 v5, 0x3

    aput-object p6, v4, v5

    const/4 v5, 0x4

    aput-object p1, v4, v5

    const/4 v5, 0x5

    invoke-static {p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    if-nez p7, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-object v1

    .line 117
    :cond_1
    if-nez p3, :cond_2

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, p6}, Lcom/sec/chaton/e/a/n;->m(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 126
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, p1, p7}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/Long;Z)Ljava/lang/String;

    move-result-object v2

    .line 127
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object v1, v2

    .line 128
    goto :goto_0

    .line 131
    :cond_3
    invoke-static {p2}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;)Lcom/sec/chaton/e/w;

    move-result-object v2

    .line 133
    sget-object v3, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v2, v3, :cond_4

    sget-object v3, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-eq v2, v3, :cond_4

    sget-object v3, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    if-eq v2, v3, :cond_4

    sget-object v3, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    if-eq v2, v3, :cond_4

    sget-object v3, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v2, v3, :cond_6

    .line 134
    :cond_4
    new-array v2, v7, [Ljava/lang/String;

    .line 135
    const-string v2, "\n"

    invoke-virtual {p2, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 136
    array-length v3, v2

    if-le v3, v0, :cond_0

    const-string v3, "mixed"

    aget-object v4, v2, v7

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 137
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 138
    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_5

    .line 139
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v2, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 141
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 148
    :cond_6
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 149
    :cond_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p6}, Lcom/sec/chaton/e/a/n;->n(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 150
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p6}, Lcom/sec/chaton/e/a/n;->o(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 154
    :goto_2
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    invoke-virtual {p0, p2, v3, v4}, Lcom/sec/chaton/chat/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 165
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v2, p1

    move v5, p7

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Z)I

    goto/16 :goto_0

    :cond_8
    move-object v3, p5

    move-object v4, p4

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->i:Lcom/sec/chaton/chat/b/a;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/chat/b/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 183
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-object v0

    .line 187
    :cond_1
    invoke-static {p2, p3}, Lcom/sec/chaton/chat/b/n;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    const-string v1, "\n"

    const-string v2, " --\n"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 195
    :try_start_0
    invoke-direct {p0, v1, p2, p3}, Lcom/sec/chaton/chat/b/c;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 201
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 202
    const-string v1, "  "

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 203
    const-string v1, "--\n"

    const-string v2, "\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 196
    :catch_0
    move-exception v1

    .line 197
    sget-object v2, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/chat/b/f;)V
    .locals 2

    .prologue
    .line 53
    iget-object v1, p0, Lcom/sec/chaton/chat/b/c;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 54
    :try_start_0
    sget-object v0, Lcom/sec/chaton/chat/b/f;->b:Lcom/sec/chaton/chat/b/f;

    if-ne p1, v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->c:Ljava/lang/String;

    .line 59
    :goto_0
    monitor-exit v1

    .line 60
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->c:Ljava/lang/String;

    goto :goto_0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Z)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAuthen(), force : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v3, p0, Lcom/sec/chaton/chat/b/c;->f:Ljava/lang/Object;

    monitor-enter v3

    .line 378
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v4, "primary_translation_address"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->d:Ljava/lang/String;

    .line 379
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v4, "secondary_translation_address"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->e:Ljava/lang/String;

    .line 381
    if-nez p1, :cond_1

    .line 382
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 383
    const-string v0, "setAuthen(), Exist URL for Translation already."

    sget-object v2, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    iget-object v2, p0, Lcom/sec/chaton/chat/b/c;->f:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 386
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    sget-object v0, Lcom/sec/chaton/chat/b/f;->a:Lcom/sec/chaton/chat/b/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/b/c;->a(Lcom/sec/chaton/chat/b/f;)V

    .line 389
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v0, v1

    .line 422
    :goto_0
    return v0

    .line 389
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 394
    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_1
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 397
    const/4 v3, 0x0

    .line 399
    :try_start_6
    invoke-virtual {p0}, Lcom/sec/chaton/chat/b/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/d/m;->a(Ljava/lang/String;)Lcom/sec/chaton/d/a/co;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/co;->b()Lcom/sec/chaton/a/a/f;
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0

    move-result-object v0

    .line 405
    :goto_1
    if-nez v0, :cond_2

    move v0, v2

    .line 406
    goto :goto_0

    .line 401
    :catch_0
    move-exception v0

    .line 402
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    move-object v0, v3

    goto :goto_1

    .line 409
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/d/a/cr;

    .line 411
    invoke-virtual {v0}, Lcom/sec/chaton/d/a/cr;->a()Lcom/sec/chaton/chat/b/m;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/chat/b/m;->a:Lcom/sec/chaton/chat/b/m;

    if-eq v0, v3, :cond_3

    .line 412
    const-string v0, "setAuthen(), failed to getUserInfo()"

    sget-object v1, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 413
    goto :goto_0

    .line 416
    :cond_3
    iget-object v2, p0, Lcom/sec/chaton/chat/b/c;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 417
    :try_start_7
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "primary_translation_address"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->d:Ljava/lang/String;

    .line 418
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "secondary_translation_address"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->e:Ljava/lang/String;

    .line 419
    sget-object v0, Lcom/sec/chaton/chat/b/f;->a:Lcom/sec/chaton/chat/b/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/b/c;->a(Lcom/sec/chaton/chat/b/f;)V

    .line 420
    monitor-exit v2

    move v0, v1

    .line 422
    goto :goto_0

    .line 420
    :catchall_2
    move-exception v0

    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0
.end method

.method public a(Ljava/lang/String;I)[Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 464
    const/4 v1, 0x0

    .line 465
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 467
    const/4 v2, 0x0

    :try_start_0
    iput v2, p0, Lcom/sec/chaton/chat/b/c;->b:I

    .line 468
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    const-string v6, "UTF8"

    invoke-virtual {p1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-string v6, "UTF8"

    invoke-direct {v4, v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 469
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 471
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 472
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 493
    :catch_0
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 494
    :goto_1
    sget-object v2, Lcom/sec/chaton/chat/b/c;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 497
    :goto_2
    return-object v0

    .line 474
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 476
    new-instance v2, Lcom/sec/chaton/chat/b/g;

    invoke-direct {v2}, Lcom/sec/chaton/chat/b/g;-><init>()V

    .line 477
    invoke-virtual {v2, p2}, Lcom/sec/chaton/chat/b/g;->a(I)V

    .line 478
    new-instance v5, Lcom/sec/chaton/chat/b/d;

    invoke-direct {v5, p0, v3}, Lcom/sec/chaton/chat/b/d;-><init>(Lcom/sec/chaton/chat/b/c;Ljava/util/List;)V

    invoke-virtual {v2, v5}, Lcom/sec/chaton/chat/b/g;->a(Lcom/sec/chaton/chat/b/h;)V

    .line 486
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Lcom/sec/chaton/chat/b/g;->a(Ljava/util/Locale;Ljava/lang/String;)V

    .line 488
    iget v2, p0, Lcom/sec/chaton/chat/b/c;->b:I

    new-array v1, v2, [Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v2, v0

    .line 489
    :goto_3
    :try_start_2
    iget v0, p0, Lcom/sec/chaton/chat/b/c;->b:I

    if-ge v2, v0, :cond_1

    .line 491
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v1, v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 489
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_1
    move-object v0, v1

    .line 495
    goto :goto_2

    .line 493
    :catch_1
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_1
.end method

.method public declared-synchronized b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->g:Ljava/lang/String;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->g:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->i:Lcom/sec/chaton/chat/b/a;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/chat/b/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Ljava/util/Locale;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->i:Lcom/sec/chaton/chat/b/a;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/chat/b/a;->c(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/chaton/chat/b/a;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/chaton/chat/b/c;->i:Lcom/sec/chaton/chat/b/a;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 525
    const-string v0, "en-UK"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 526
    const-string p1, "en-GB"

    .line 530
    :cond_0
    :goto_0
    return-object p1

    .line 527
    :cond_1
    const-string v0, "pt-PT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528
    const-string p1, "pt-BR"

    goto :goto_0
.end method

.method e()V
    .locals 3

    .prologue
    .line 535
    iget-object v1, p0, Lcom/sec/chaton/chat/b/c;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 536
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "primary_translation_address"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 537
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "secondary_translation_address"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 538
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->d:Ljava/lang/String;

    .line 539
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/b/c;->e:Ljava/lang/String;

    .line 540
    monitor-exit v1

    .line 541
    return-void

    .line 540
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
