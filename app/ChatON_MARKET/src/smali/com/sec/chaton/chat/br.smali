.class Lcom/sec/chaton/chat/br;
.super Landroid/os/Handler;
.source "ChatFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 13317
    iput-object p1, p0, Lcom/sec/chaton/chat/br;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 13320
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "download_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 13321
    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    .line 13323
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 13324
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "msgType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 13327
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/chat/br;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->aO(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 13350
    :cond_1
    :goto_0
    return-void

    .line 13331
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/chat/br;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->aP(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 13335
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 13337
    sget-object v2, Lcom/sec/chaton/chat/cp;->a:[I

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 13342
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/chat/br;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->p(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 13339
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/chat/br;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->o(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 13346
    :pswitch_2
    iget-object v1, p0, Lcom/sec/chaton/chat/br;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/ChatFragment;->q(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 13337
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
