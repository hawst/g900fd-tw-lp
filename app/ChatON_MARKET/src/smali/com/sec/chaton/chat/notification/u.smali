.class Lcom/sec/chaton/chat/notification/u;
.super Landroid/os/Handler;
.source "ScreenNotification2.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/notification/ScreenNotification2;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/notification/ScreenNotification2;)V
    .locals 0

    .prologue
    .line 860
    iput-object p1, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const-wide/32 v7, 0x75300

    const/4 v6, 0x0

    .line 863
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    .line 864
    invoke-virtual {v0}, Lcom/sec/chaton/chat/fj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/k;

    .line 866
    if-nez v0, :cond_1

    .line 867
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 868
    const-string v0, "resultEntry is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    :cond_0
    :goto_0
    return-void

    .line 874
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/a/a/l;->i:Lcom/sec/chaton/a/a/l;

    if-ne v1, v2, :cond_2

    .line 876
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 877
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->b()V

    goto :goto_0

    .line 887
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    if-ne v1, v2, :cond_3

    .line 888
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    const/16 v2, 0x64

    invoke-static {v1, v2}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b(Lcom/sec/chaton/chat/notification/ScreenNotification2;I)V

    .line 894
    :cond_3
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_5

    .line 895
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v1

    .line 896
    const/16 v2, 0x18

    if-eq v1, v2, :cond_4

    const/16 v2, 0x17

    if-eq v1, v2, :cond_4

    const/16 v2, 0x15

    if-ne v1, v2, :cond_5

    .line 897
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 898
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/o;->j()V

    .line 901
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->g(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/g;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->b()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 908
    :cond_5
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_0

    .line 912
    :sswitch_0
    iget-boolean v1, v0, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v1, :cond_8

    .line 914
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 916
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 917
    sget-object v3, Lcom/sec/chaton/chat/notification/v;->a:[I

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-static {v4, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/l;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 962
    :cond_6
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b008a

    invoke-static {v0, v1, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 975
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->b()V

    goto/16 :goto_0

    .line 921
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 922
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 926
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->g(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/g;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bk;->b()I

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 928
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v0

    add-long/2addr v1, v7

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->c(J)V

    goto :goto_1

    .line 935
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 936
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->g(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/g;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/chaton/chat/notification/g;->e:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bk;->b()I

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 940
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v0

    add-long/2addr v1, v7

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->c(J)V

    goto/16 :goto_1

    .line 950
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00c9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->g(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/g;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/chaton/chat/notification/g;->a:Ljava/lang/String;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 951
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 967
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 968
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->o(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->g(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/g;

    move-result-object v1

    iget v1, v1, Lcom/sec/chaton/chat/notification/g;->f:I

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->g(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Lcom/sec/chaton/chat/notification/g;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/chaton/chat/notification/g;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 969
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->p(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 970
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/u;->a:Lcom/sec/chaton/chat/notification/ScreenNotification2;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/ScreenNotification2;->n(Lcom/sec/chaton/chat/notification/ScreenNotification2;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00ff

    invoke-static {v0, v1, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 908
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_0
    .end sparse-switch

    .line 917
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
