.class public Lcom/sec/chaton/chat/a/t;
.super Lcom/sec/chaton/chat/a/af;
.source "ImageBubbleDrawer.java"


# static fields
.field private static final F:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/chaton/chat/a/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/a/t;->F:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/af;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->u:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_0

    .line 32
    const v0, 0x7f0b0049

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 35
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0b0051

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a()V
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 66
    invoke-super {p0}, Lcom/sec/chaton/chat/a/af;->a()V

    .line 68
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 69
    sget-object v0, Lcom/sec/chaton/chat/a/t;->F:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uri.parse(content): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/t;->m:J

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/chaton/j/c/a;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 74
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/t;->d:Landroid/view/View;

    const/4 v2, 0x0

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-super {p0}, Lcom/sec/chaton/chat/a/af;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/chaton/chat/a/t;->n:Ljava/lang/String;

    iget-wide v8, p0, Lcom/sec/chaton/chat/a/t;->m:J

    iget-object v10, p0, Lcom/sec/chaton/chat/a/t;->u:Lcom/sec/chaton/e/w;

    iget-object v11, p0, Lcom/sec/chaton/chat/a/t;->l:Lcom/sec/chaton/e/r;

    iget-object v12, p0, Lcom/sec/chaton/chat/a/t;->v:Ljava/lang/String;

    invoke-virtual/range {v0 .. v12}, Lcom/sec/chaton/j/c/a;->a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/t;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/t;->o:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/t;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 85
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    .line 87
    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->w:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 88
    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->X:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    move-object v4, v1

    .line 107
    :goto_0
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    new-instance v3, Lcom/sec/chaton/multimedia/image/c;

    iget-object v5, p0, Lcom/sec/chaton/chat/a/t;->w:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/chaton/chat/a/t;->u:Lcom/sec/chaton/e/w;

    move v7, v6

    invoke-direct/range {v3 .. v8}, Lcom/sec/chaton/multimedia/image/c;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/sec/chaton/e/w;)V

    .line 111
    iget-object v1, p0, Lcom/sec/chaton/chat/a/t;->j:Lcom/sec/common/f/c;

    invoke-virtual {v1, v0, v3}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 112
    return-void

    .line 91
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 92
    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->X:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v13}, Landroid/widget/ProgressBar;->setVisibility(I)V

    move-object v4, v1

    goto :goto_0

    .line 95
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p0, Lcom/sec/chaton/chat/a/t;->m:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/a/t;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    .line 98
    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->w:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 99
    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->ad:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    move-object v4, v1

    goto :goto_0

    .line 102
    :cond_4
    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 103
    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->ad:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v13}, Landroid/widget/ProgressBar;->setVisibility(I)V

    move-object v4, v1

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 116
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/af;->a(Z)V

    .line 120
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/t;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->X:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    .line 134
    :goto_0
    if-nez p1, :cond_0

    .line 135
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 136
    iget-object v1, p0, Lcom/sec/chaton/chat/a/t;->j:Lcom/sec/common/f/c;

    invoke-virtual {v1, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 140
    :cond_0
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    return-void

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->ad:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    goto :goto_0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lcom/sec/chaton/chat/a/af;->c()Ljava/lang/String;

    move-result-object v0

    .line 42
    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/sec/chaton/chat/a/n;
    .locals 5

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/t;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/t;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->w:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/t;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :goto_0
    return-object v0

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/t;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/t;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->w:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/t;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/t;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 56
    const-string v0, "onShare() - getTextContent() is null"

    const-string v1, "ImageBubbleDrawer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_2
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    iget-object v1, p0, Lcom/sec/chaton/chat/a/t;->u:Lcom/sec/chaton/e/w;

    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->w:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/t;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/af;->onClick(Landroid/view/View;)V

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->O:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->e:Landroid/database/Cursor;

    iget v1, p0, Lcom/sec/chaton/chat/a/t;->f:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/chat/a/t;->k:Lcom/sec/chaton/chat/fk;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/t;->n()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/chat/a/t;->e:Landroid/database/Cursor;

    invoke-interface {v0, v1, v2}, Lcom/sec/chaton/chat/fk;->a(ZLandroid/database/Cursor;)V

    goto :goto_0
.end method
