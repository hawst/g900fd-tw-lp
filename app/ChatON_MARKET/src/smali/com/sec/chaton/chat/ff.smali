.class Lcom/sec/chaton/chat/ff;
.super Ljava/lang/Object;
.source "ForwardMessageHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/e/w;

.field final synthetic b:J

.field final synthetic c:Landroid/os/Handler;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lcom/sec/chaton/e/r;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Ljava/lang/String;

.field final synthetic i:Ljava/lang/String;

.field final synthetic j:Lcom/sec/chaton/chat/fe;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/fe;Lcom/sec/chaton/e/w;JLandroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/chaton/chat/ff;->j:Lcom/sec/chaton/chat/fe;

    iput-object p2, p0, Lcom/sec/chaton/chat/ff;->a:Lcom/sec/chaton/e/w;

    iput-wide p3, p0, Lcom/sec/chaton/chat/ff;->b:J

    iput-object p5, p0, Lcom/sec/chaton/chat/ff;->c:Landroid/os/Handler;

    iput-object p6, p0, Lcom/sec/chaton/chat/ff;->d:Ljava/lang/String;

    iput-object p7, p0, Lcom/sec/chaton/chat/ff;->e:Ljava/lang/String;

    iput-object p8, p0, Lcom/sec/chaton/chat/ff;->f:Lcom/sec/chaton/e/r;

    iput-object p9, p0, Lcom/sec/chaton/chat/ff;->g:Ljava/lang/String;

    iput-object p10, p0, Lcom/sec/chaton/chat/ff;->h:Ljava/lang/String;

    iput-object p11, p0, Lcom/sec/chaton/chat/ff;->i:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/chat/ff;->a:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ff;->a:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, v2, :cond_2

    .line 211
    :cond_0
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/chaton/chat/ff;->b:J

    invoke-virtual {v0, v2, v3, v6}, Lcom/sec/chaton/j/c/a;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 212
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/ff;->c:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/chaton/chat/ff;->d:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/chat/ff;->e:Ljava/lang/String;

    iget-wide v8, p0, Lcom/sec/chaton/chat/ff;->b:J

    iget-object v10, p0, Lcom/sec/chaton/chat/ff;->a:Lcom/sec/chaton/e/w;

    iget-object v11, p0, Lcom/sec/chaton/chat/ff;->f:Lcom/sec/chaton/e/r;

    iget-object v12, p0, Lcom/sec/chaton/chat/ff;->g:Ljava/lang/String;

    move-object v2, v1

    invoke-virtual/range {v0 .. v12}, Lcom/sec/chaton/j/c/a;->a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 225
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Download (URL)] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ff;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Download (FileName)] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ff;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    return-void

    .line 218
    :cond_2
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/chat/ff;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/audio/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/chaton/chat/ff;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/j/c/a;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 220
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/ff;->c:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/chaton/chat/ff;->d:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/chaton/chat/ff;->e:Ljava/lang/String;

    iget-wide v8, p0, Lcom/sec/chaton/chat/ff;->b:J

    iget-object v10, p0, Lcom/sec/chaton/chat/ff;->a:Lcom/sec/chaton/e/w;

    iget-object v11, p0, Lcom/sec/chaton/chat/ff;->f:Lcom/sec/chaton/e/r;

    iget-object v12, p0, Lcom/sec/chaton/chat/ff;->g:Ljava/lang/String;

    move-object v2, v1

    move v6, v4

    invoke-virtual/range {v0 .. v12}, Lcom/sec/chaton/j/c/a;->a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    goto :goto_0
.end method
