.class Lcom/sec/chaton/chat/fa;
.super Ljava/lang/Object;
.source "EditChatTitleDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ex;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ex;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    iget-object v1, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v1}, Lcom/sec/chaton/chat/ex;->c(Lcom/sec/chaton/chat/ex;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ex;->a(Lcom/sec/chaton/chat/ex;Ljava/lang/String;)Ljava/lang/String;

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v0}, Lcom/sec/chaton/chat/ex;->d(Lcom/sec/chaton/chat/ex;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v0}, Lcom/sec/chaton/chat/ex;->d(Lcom/sec/chaton/chat/ex;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v0}, Lcom/sec/chaton/chat/ex;->a(Lcom/sec/chaton/chat/ex;)V

    .line 198
    :cond_1
    :goto_0
    return-void

    .line 192
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    iget-object v0, v0, Lcom/sec/chaton/chat/ex;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v1}, Lcom/sec/chaton/chat/ex;->d(Lcom/sec/chaton/chat/ex;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    invoke-static {v2}, Lcom/sec/chaton/chat/ex;->e(Lcom/sec/chaton/chat/ex;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/d/m;->c(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    iget-object v0, v0, Lcom/sec/chaton/chat/ex;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    iget-object v0, v0, Lcom/sec/chaton/chat/ex;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/chat/fa;->a:Lcom/sec/chaton/chat/ex;

    iget-object v0, v0, Lcom/sec/chaton/chat/ex;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method
