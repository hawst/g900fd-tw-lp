.class Lcom/sec/chaton/chat/ax;
.super Landroid/os/Handler;
.source "ChatFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 7026
    iput-object p1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/16 v12, 0x1b5d

    const/4 v2, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 7030
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    .line 7032
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 7613
    :cond_0
    :goto_0
    return-void

    .line 7036
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    check-cast v0, Lcom/sec/chaton/chat/fj;

    .line 7037
    invoke-virtual {v0}, Lcom/sec/chaton/chat/fj;->a()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/sec/chaton/a/a/k;

    .line 7039
    if-nez v8, :cond_2

    .line 7040
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 7041
    const-string v0, "resultEntry is null"

    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 7046
    :cond_2
    sget-object v0, Lcom/sec/chaton/chat/ChatFragment;->a:Ljava/lang/String;

    const-string v3, "ChatFragment, mUIHandler(), ChatType(%d), Result(%d), Error(%s)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v11

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v6

    invoke-static {v5, v6}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v5

    aput-object v5, v4, v13

    invoke-static {v0, v3, v4}, Lcom/sec/chaton/msgsend/s;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7053
    iget v0, p1, Landroid/os/Message;->what:I

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/a/a/l;->i:Lcom/sec/chaton/a/a/l;

    if-ne v0, v3, :cond_3

    .line 7054
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 7055
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->b()V

    goto :goto_0

    .line 7058
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    if-ne v0, v3, :cond_c

    .line 7059
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    sget-object v3, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/a/a/l;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 7060
    if-eqz v0, :cond_4

    .line 7061
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 7077
    :cond_4
    iget-boolean v0, v8, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v0, :cond_d

    .line 7078
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 7079
    const/16 v3, 0x18

    if-eq v0, v3, :cond_5

    const/16 v3, 0x17

    if-eq v0, v3, :cond_5

    const/16 v3, 0x15

    if-ne v0, v3, :cond_6

    .line 7080
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 7081
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 7084
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v3, v3, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bk;->b()I

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    :cond_6
    move v9, v10

    .line 7098
    :goto_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 7103
    :sswitch_0
    if-eqz v9, :cond_e

    .line 7104
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iput-boolean v11, v0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    .line 7112
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v0, v0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    if-nez v0, :cond_7

    .line 7114
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iput-boolean v1, v0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    .line 7120
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v0, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_8

    .line 7121
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aa(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 7122
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iput-boolean v11, v0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    .line 7133
    :cond_8
    :goto_3
    iget-boolean v0, v8, Lcom/sec/chaton/a/a/k;->a:Z

    if-eqz v0, :cond_1c

    .line 7135
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ab(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 7136
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->w()V

    .line 7139
    :cond_9
    instance-of v0, v8, Lcom/sec/chaton/d/a/cz;

    if-eqz v0, :cond_10

    .line 7140
    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object v0, v8

    check-cast v0, Lcom/sec/chaton/d/a/cz;

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/cz;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    .line 7145
    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/4 v1, 0x5

    iget-object v3, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v3, v3, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/e/z;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 7146
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->g()V

    .line 7148
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 7149
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-static {v1, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/a/a/l;->a:Lcom/sec/chaton/a/a/l;

    if-ne v0, v1, :cond_13

    .line 7151
    instance-of v0, v8, Lcom/sec/chaton/d/a/cz;

    if-eqz v0, :cond_11

    move-object v0, v8

    .line 7152
    check-cast v0, Lcom/sec/chaton/d/a/cz;

    iget-object v0, v0, Lcom/sec/chaton/d/a/cz;->d:Ljava/util/ArrayList;

    .line 7157
    :goto_5
    if-eqz v0, :cond_1a

    .line 7158
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 7159
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 7160
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 7161
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7162
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 7163
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 7066
    :cond_c
    iget v0, p1, Landroid/os/Message;->what:I

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/a/a/l;->l:Lcom/sec/chaton/a/a/l;

    if-ne v0, v3, :cond_4

    .line 7067
    invoke-static {}, Lcom/sec/chaton/ExitAppDialogActivity;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 7068
    invoke-static {v13}, Lcom/sec/chaton/ExitAppDialogActivity;->a(I)V

    goto/16 :goto_0

    :cond_d
    move v9, v11

    .line 7093
    goto/16 :goto_1

    .line 7107
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iput-boolean v10, v0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    goto/16 :goto_2

    .line 7124
    :cond_f
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iput-boolean v10, v0, Lcom/sec/chaton/chat/ChatFragment;->t:Z

    goto/16 :goto_3

    .line 7141
    :cond_10
    instance-of v0, v8, Lcom/sec/chaton/d/a/dh;

    if-eqz v0, :cond_a

    .line 7142
    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    move-object v0, v8

    check-cast v0, Lcom/sec/chaton/d/a/dh;

    iget-object v0, v0, Lcom/sec/chaton/d/a/dh;->d:Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    goto/16 :goto_4

    .line 7153
    :cond_11
    instance-of v0, v8, Lcom/sec/chaton/d/a/dh;

    if-eqz v0, :cond_3a

    move-object v0, v8

    .line 7154
    check-cast v0, Lcom/sec/chaton/d/a/dh;

    iget-object v0, v0, Lcom/sec/chaton/d/a/dh;->e:Ljava/util/ArrayList;

    goto :goto_5

    .line 7167
    :cond_12
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0b00ca

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v3, v11, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v10

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 7168
    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 7176
    :cond_13
    :goto_7
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ab(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 7177
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ac(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 7178
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1b

    .line 7179
    new-instance v0, Lcom/sec/chaton/chat/cx;

    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/chat/cx;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/c;)V

    new-array v1, v10, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/cx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 7186
    :cond_14
    :goto_8
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->e(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V

    .line 7292
    :cond_15
    :goto_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ae(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 7293
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 7300
    const-string v0, "inbox_title"

    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->ae(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7303
    const-string v0, "inbox_title_fixed"

    const-string v1, "Y"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7304
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->t(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/e/a/u;

    move-result-object v0

    const/4 v1, -0x1

    sget-object v3, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inbox_no=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v6, v6, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 7308
    :cond_16
    if-nez v9, :cond_37

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/j/q;->a(I)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 7311
    :goto_a
    if-eqz v10, :cond_17

    .line 7312
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0, v11, v9}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;ZZ)V

    .line 7318
    :cond_17
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    if-ne v0, v12, :cond_18

    .line 7319
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->c()V

    .line 7323
    :cond_18
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v0, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_19

    .line 7325
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->af(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v0, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    if-eqz v0, :cond_19

    .line 7326
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->af(Lcom/sec/chaton/chat/ChatFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v3, v3, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 7331
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v0, v0, Lcom/sec/chaton/chat/ChatFragment;->x:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne v0, v1, :cond_25

    .line 7332
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->r(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/widget/HeightChangedListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/widget/HeightChangedListView;->getCount()I

    move-result v0

    if-ne v0, v13, :cond_0

    .line 7333
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    goto/16 :goto_0

    .line 7170
    :cond_1a
    const-string v0, "1001 error - but invalid user list is empty"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 7181
    :cond_1b
    new-instance v0, Lcom/sec/chaton/chat/cx;

    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/chat/cx;-><init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/c;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v3, v10, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/chat/cx;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_8

    .line 7191
    :cond_1c
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->w()V

    .line 7193
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 7197
    sget-object v1, Lcom/sec/chaton/chat/cp;->b:[I

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-static {v3, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/l;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_9

    .line 7201
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 7202
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 7206
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/util/bk;->b()I

    move-result v4

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    goto/16 :goto_9

    .line 7215
    :pswitch_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00c9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v11, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v4}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v10

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 7216
    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_9

    .line 7223
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 7227
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_1e

    .line 7228
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1d
    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 7229
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 7230
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/a;

    .line 7231
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7232
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 7233
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_b

    .line 7238
    :cond_1e
    instance-of v0, v8, Lcom/sec/chaton/d/a/cz;

    if-eqz v0, :cond_20

    move-object v0, v8

    .line 7239
    check-cast v0, Lcom/sec/chaton/d/a/cz;

    iget-object v0, v0, Lcom/sec/chaton/d/a/cz;->d:Ljava/util/ArrayList;

    .line 7244
    :goto_c
    if-eqz v0, :cond_21

    .line 7246
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1f
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 7247
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 7248
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7249
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 7250
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_d

    .line 7240
    :cond_20
    instance-of v0, v8, Lcom/sec/chaton/d/a/dh;

    if-eqz v0, :cond_39

    move-object v0, v8

    .line 7241
    check-cast v0, Lcom/sec/chaton/d/a/dh;

    iget-object v0, v0, Lcom/sec/chaton/d/a/dh;->e:Ljava/util/ArrayList;

    goto :goto_c

    .line 7255
    :cond_21
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0b00ca

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v3, v11, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v10

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 7256
    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_9

    .line 7264
    :pswitch_3
    instance-of v0, v8, Lcom/sec/chaton/d/a/cz;

    if-eqz v0, :cond_23

    move-object v0, v8

    .line 7265
    check-cast v0, Lcom/sec/chaton/d/a/cz;

    iget-object v0, v0, Lcom/sec/chaton/d/a/cz;->e:Lcom/sec/chaton/a/ea;

    .line 7270
    :goto_e
    if-eqz v0, :cond_24

    .line 7272
    invoke-virtual {v0}, Lcom/sec/chaton/a/ea;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_22
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/dm;

    .line 7273
    invoke-virtual {v0}, Lcom/sec/chaton/a/dm;->d()Ljava/lang/String;

    move-result-object v3

    .line 7274
    invoke-virtual {v0}, Lcom/sec/chaton/a/dm;->f()Ljava/lang/String;

    move-result-object v4

    .line 7276
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 7278
    const/4 v5, -0x1

    if-le v0, v5, :cond_22

    .line 7279
    iget-object v5, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v5}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v0, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 7280
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/a;

    .line 7281
    invoke-virtual {v0, v4}, Lcom/sec/chaton/a/a/a;->a(Ljava/lang/String;)V

    .line 7282
    iget-object v3, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_f

    .line 7266
    :cond_23
    instance-of v0, v8, Lcom/sec/chaton/d/a/dh;

    if-eqz v0, :cond_38

    move-object v0, v8

    .line 7267
    check-cast v0, Lcom/sec/chaton/d/a/dh;

    iget-object v0, v0, Lcom/sec/chaton/d/a/dh;->f:Lcom/sec/chaton/a/ea;

    goto :goto_e

    .line 7286
    :cond_24
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->g()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/chat/ChatFragment;->a(J)V

    goto/16 :goto_9

    .line 7336
    :cond_25
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    check-cast v0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->supportInvalidateOptionsMenu()V

    goto/16 :goto_0

    .line 7344
    :sswitch_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->w()V

    .line 7346
    iget-boolean v0, v8, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v0, :cond_26

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/a/a/l;->e:Lcom/sec/chaton/a/a/l;

    if-ne v0, v1, :cond_28

    .line 7349
    :cond_26
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-nez v0, :cond_27

    .line 7351
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/ct;

    invoke-interface {v0}, Lcom/sec/chaton/chat/ct;->c()V

    .line 7361
    :goto_10
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0167

    invoke-static {v0, v1, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 7368
    :goto_11
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    if-ne v0, v12, :cond_0

    .line 7369
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->c()V

    goto/16 :goto_0

    .line 7355
    :cond_27
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->e(Lcom/sec/chaton/chat/ChatFragment;Ljava/lang/String;)V

    .line 7357
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/TabActivity;

    .line 7358
    const v1, 0x7f0704a1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/sec/chaton/chat/EmptyChatFragment;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/TabActivity;->a(ILandroid/content/Intent;Ljava/lang/Class;)V

    goto :goto_10

    .line 7363
    :cond_28
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b002c

    invoke-static {v0, v1, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_11

    .line 7376
    :sswitch_2
    iget-boolean v0, v8, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v0, :cond_29

    .line 7378
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 7380
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    .line 7381
    sget-object v1, Lcom/sec/chaton/chat/cp;->b:[I

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-static {v5, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/l;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 7448
    :cond_29
    :goto_12
    :pswitch_4
    if-nez v9, :cond_36

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/j/q;->a(I)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 7451
    :goto_13
    if-eqz v10, :cond_2a

    .line 7452
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0, v11, v9}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;ZZ)V

    .line 7458
    :cond_2a
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    if-ne v0, v12, :cond_0

    .line 7459
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->c()V

    goto/16 :goto_0

    .line 7385
    :pswitch_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 7386
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->j()V

    .line 7390
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bk;->b()I

    move-result v5

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 7392
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    const-wide/32 v1, 0x75300

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->c(J)V

    goto :goto_12

    .line 7402
    :pswitch_6
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 7403
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/chaton/util/bk;->b()I

    move-result v6

    invoke-virtual {v0, v1, v5, v6}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 7406
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->Z(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/d/o;

    move-result-object v0

    const-wide/32 v5, 0x75300

    add-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/d/o;->c(J)V

    .line 7408
    :cond_2b
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iput-object v2, v0, Lcom/sec/chaton/chat/ChatFragment;->y:Ljava/lang/String;

    goto/16 :goto_12

    .line 7417
    :pswitch_7
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00c9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v11, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->T(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 7418
    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_12

    .line 7422
    :pswitch_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 7423
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ad(Lcom/sec/chaton/chat/ChatFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2c
    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 7424
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 7425
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/a/a;

    .line 7426
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7427
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 7428
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_14

    .line 7434
    :pswitch_9
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->d()V

    goto/16 :goto_12

    .line 7438
    :pswitch_a
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ag(Lcom/sec/chaton/chat/ChatFragment;)V

    goto/16 :goto_12

    .line 7467
    :sswitch_3
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->w()V

    .line 7470
    iget-boolean v0, v8, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v0, :cond_0

    .line 7471
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 7473
    sget-object v1, Lcom/sec/chaton/chat/cp;->b:[I

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/l;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_0

    .line 7475
    :pswitch_b
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->d()V

    goto/16 :goto_0

    .line 7479
    :pswitch_c
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ag(Lcom/sec/chaton/chat/ChatFragment;)V

    goto/16 :goto_0

    .line 7489
    :sswitch_4
    iget-boolean v0, v8, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v0, :cond_0

    .line 7495
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->w()V

    .line 7498
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;I)I

    .line 7499
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ah(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v0

    const/16 v1, 0x6d64

    if-eq v0, v1, :cond_2d

    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ah(Lcom/sec/chaton/chat/ChatFragment;)I

    move-result v0

    const/16 v1, 0x6d63

    if-ne v0, v1, :cond_2f

    .line 7501
    :cond_2d
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b002f

    invoke-static {v0, v1, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 7514
    :cond_2e
    :goto_15
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0, v11, v9}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;ZZ)V

    goto/16 :goto_0

    .line 7505
    :cond_2f
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v1}, Lcom/sec/chaton/j/c/i;->a(II)Z

    move-result v0

    if-eqz v0, :cond_35

    move v0, v10

    .line 7509
    :goto_16
    if-eqz v0, :cond_2e

    .line 7510
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b013e

    invoke-static {v0, v1, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_15

    .line 7519
    :sswitch_5
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->w()V

    .line 7521
    iget-boolean v0, v8, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v0, :cond_30

    .line 7522
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 7523
    sget-object v1, Lcom/sec/chaton/chat/cp;->b:[I

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/l;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_3

    .line 7535
    :goto_17
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00e4

    invoke-static {v0, v1, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 7540
    :cond_30
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    if-ne v0, v12, :cond_0

    .line 7541
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->c()V

    goto/16 :goto_0

    .line 7525
    :pswitch_d
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->d()V

    goto :goto_17

    .line 7529
    :pswitch_e
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ag(Lcom/sec/chaton/chat/ChatFragment;)V

    goto :goto_17

    .line 7548
    :sswitch_6
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->w()V

    .line 7550
    iget-boolean v0, v8, Lcom/sec/chaton/a/a/k;->a:Z

    if-nez v0, :cond_31

    .line 7551
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    .line 7552
    sget-object v1, Lcom/sec/chaton/chat/cp;->b:[I

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2, v0}, Lcom/sec/chaton/chat/eq;->a(II)Lcom/sec/chaton/a/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/l;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_4

    .line 7560
    :goto_18
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00e4

    invoke-static {v0, v1, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 7565
    :cond_31
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    if-ne v0, v12, :cond_0

    .line 7566
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->c()V

    goto/16 :goto_0

    .line 7554
    :pswitch_f
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->d()V

    goto :goto_18

    .line 7571
    :sswitch_7
    check-cast v8, Lcom/sec/chaton/a/a/b;

    .line 7572
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/b;->b()I

    move-result v0

    .line 7578
    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    if-nez v1, :cond_32

    .line 7579
    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/b;->e()Lcom/sec/chaton/e/w;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/sec/chaton/chat/ChatFragment;->a(ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)V

    .line 7582
    :cond_32
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0, v10, v9}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;ZZ)V

    goto/16 :goto_0

    .line 7586
    :sswitch_8
    check-cast v8, Lcom/sec/chaton/a/a/d;

    .line 7587
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/d;->b()I

    move-result v0

    .line 7588
    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-boolean v1, v1, Lcom/sec/chaton/chat/ChatFragment;->E:Z

    if-nez v1, :cond_33

    .line 7589
    iget-object v1, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/d;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/sec/chaton/a/a/d;->e()Lcom/sec/chaton/e/w;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/sec/chaton/chat/ChatFragment;->a(ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)V

    .line 7592
    :cond_33
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0, v10, v9}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;ZZ)V

    goto/16 :goto_0

    .line 7599
    :sswitch_9
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    const/16 v1, 0x1bbc

    if-ne v0, v1, :cond_34

    .line 7600
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->ai(Lcom/sec/chaton/chat/ChatFragment;)V

    goto/16 :goto_0

    .line 7602
    :cond_34
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aj(Lcom/sec/chaton/chat/ChatFragment;)V

    goto/16 :goto_0

    .line 7606
    :sswitch_a
    invoke-virtual {v8}, Lcom/sec/chaton/a/a/k;->f()I

    move-result v0

    const/16 v1, 0xbbe

    if-ne v0, v1, :cond_0

    .line 7607
    iget-object v0, p0, Lcom/sec/chaton/chat/ax;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->d()V

    goto/16 :goto_0

    :cond_35
    move v0, v11

    goto/16 :goto_16

    :cond_36
    move v10, v11

    goto/16 :goto_13

    :cond_37
    move v10, v11

    goto/16 :goto_a

    :cond_38
    move-object v0, v2

    goto/16 :goto_e

    :cond_39
    move-object v0, v2

    goto/16 :goto_c

    :cond_3a
    move-object v0, v2

    goto/16 :goto_5

    .line 7098
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_3
        0x4 -> :sswitch_2
        0x6 -> :sswitch_7
        0x8 -> :sswitch_1
        0xc -> :sswitch_5
        0x10 -> :sswitch_a
        0x1f -> :sswitch_6
        0x22 -> :sswitch_8
        0x24 -> :sswitch_8
        0x65 -> :sswitch_4
        0x66 -> :sswitch_2
        0x6a -> :sswitch_0
        0x6b -> :sswitch_9
    .end sparse-switch

    .line 7197
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 7381
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_6
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 7473
    :pswitch_data_2
    .packed-switch 0x6
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 7523
    :pswitch_data_3
    .packed-switch 0x6
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 7552
    :pswitch_data_4
    .packed-switch 0x6
        :pswitch_f
    .end packed-switch
.end method
