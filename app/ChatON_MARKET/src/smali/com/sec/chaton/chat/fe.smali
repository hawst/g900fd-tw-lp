.class public Lcom/sec/chaton/chat/fe;
.super Ljava/lang/Object;
.source "ForwardMessageHandler.java"


# static fields
.field private static a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;>;"
        }
    .end annotation
.end field

.field private static b:Lcom/sec/chaton/chat/fe;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/chat/fe;->a:Ljava/util/HashMap;

    .line 75
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/chat/fe;
    .locals 2

    .prologue
    .line 63
    const-class v1, Lcom/sec/chaton/chat/fe;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/chat/fe;->b:Lcom/sec/chaton/chat/fe;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/sec/chaton/chat/fe;

    invoke-direct {v0}, Lcom/sec/chaton/chat/fe;-><init>()V

    sput-object v0, Lcom/sec/chaton/chat/fe;->b:Lcom/sec/chaton/chat/fe;

    .line 67
    :cond_0
    sget-object v0, Lcom/sec/chaton/chat/fe;->b:Lcom/sec/chaton/chat/fe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 477
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertParticipant() inboxNo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldBuddyNo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newBuddyNo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ForwardMessageHandler"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    if-eqz p5, :cond_0

    invoke-virtual {p5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 482
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p4, p2}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 483
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertParticipant() - old buddyNo is exist : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", skip insert"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ForwardMessageHandler"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    const-string v0, "message_sender=? AND message_inbox_no =?"

    .line 487
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    .line 489
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 490
    const-string v3, "message_sender"

    invoke-virtual {v2, v3, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 499
    :goto_0
    return-void

    .line 497
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertParticipant() - Insert Participants : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ForwardMessageHandler"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    invoke-static {p2, p5}, Lcom/sec/chaton/e/a/y;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(ZLjava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Lcom/sec/chaton/a/cr;)V
    .locals 21

    .prologue
    .line 354
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    .line 355
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-static {v3, v0}, Lcom/sec/chaton/e/a/n;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/sec/chaton/e/a/p;

    move-result-object v5

    .line 356
    if-nez v5, :cond_1

    .line 357
    const-string v2, "Error - InBoxData does not exist."

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/y;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 366
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v6, ""

    invoke-virtual {v3, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 368
    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v8

    .line 369
    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v4

    .line 380
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v2

    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v6

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/r;->a()I

    move-result v9

    iget-boolean v12, v5, Lcom/sec/chaton/e/a/p;->r:Z

    invoke-virtual/range {p5 .. p5}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v13

    iget-object v15, v5, Lcom/sec/chaton/e/a/p;->o:Ljava/lang/String;

    iget v0, v5, Lcom/sec/chaton/e/a/p;->p:I

    move/from16 v16, v0

    iget v0, v5, Lcom/sec/chaton/e/a/p;->h:I

    move/from16 v17, v0

    iget-wide v0, v5, Lcom/sec/chaton/e/a/p;->s:J

    move-wide/from16 v18, v0

    const-string v20, "N"

    move-object/from16 v5, p2

    move-object/from16 v10, p4

    invoke-virtual/range {v2 .. v20}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/e/w;ILjava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IIJLjava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public a(ZLcom/sec/chaton/a/cr;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/util/ArrayList;Ljava/util/List;Ljava/util/HashMap;)Lcom/sec/chaton/chat/fg;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/sec/chaton/a/cr;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/e/r;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cu;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/sec/chaton/chat/fg;"
        }
    .end annotation

    .prologue
    .line 79
    const/4 v14, 0x0

    .line 80
    const/4 v13, 0x0

    .line 82
    const/4 v5, 0x0

    .line 83
    const/16 v21, 0x0

    .line 84
    const/16 v22, 0x0

    .line 86
    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    move/from16 v7, v22

    move v4, v5

    move v6, v13

    move/from16 v5, v21

    .line 349
    :goto_1
    new-instance v2, Lcom/sec/chaton/chat/fg;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/sec/chaton/chat/fg;-><init>(Lcom/sec/chaton/chat/fe;ZZZI)V

    return-object v2

    .line 90
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v4

    .line 91
    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 93
    invoke-virtual {v4}, Lcom/sec/chaton/e/w;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    array-length v2, v15

    const/4 v3, 0x5

    if-ge v2, v3, :cond_1

    .line 94
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 95
    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v2

    .line 96
    const-string v4, "[throw away message] this one is not valid to process and show"

    const-string v6, "ForwardMessageHandler"

    invoke-static {v4, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " message id : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ForwardMessageHandler"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v7, v22

    move v4, v5

    move v6, v13

    move/from16 v5, v21

    .line 98
    goto :goto_1

    .line 102
    :cond_1
    sget-object v2, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    if-ne v4, v2, :cond_2

    .line 103
    const/4 v2, 0x2

    aget-object v2, v15, v2

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/u;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 107
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/chaton/settings/downloads/u;->i(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/z;

    move-result-object v3

    .line 109
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 110
    sget-object v5, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    if-eq v3, v5, :cond_c

    .line 111
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a()Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Ljava/lang/String;)V

    .line 125
    :cond_2
    :goto_2
    const/16 v23, 0x1

    .line 128
    sget-object v2, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    if-eq v4, v2, :cond_3

    sget-object v2, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    if-eq v4, v2, :cond_3

    sget-object v2, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    if-eq v4, v2, :cond_3

    sget-object v2, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    if-ne v4, v2, :cond_e

    :cond_3
    array-length v2, v15

    const/4 v3, 0x6

    if-lt v2, v3, :cond_e

    .line 133
    const/4 v2, 0x5

    aget-object v8, v15, v2

    .line 140
    :goto_3
    sget-object v2, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne v4, v2, :cond_4

    .line 142
    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/settings/downloads/u;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 144
    sget-object v4, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    .line 149
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 151
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/sec/chaton/e/a/n;->n(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 152
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/sec/chaton/e/a/n;->o(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 154
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/sec/chaton/e/a/n;->m(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 155
    const/4 v11, 0x0

    .line 156
    const/4 v10, 0x0

    .line 160
    :cond_5
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v12, 0x0

    move-object/from16 v3, p4

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v9, p5

    invoke-static/range {v2 .. v12}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/a/cr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/e/l;

    move-result-object v2

    .line 175
    sget-object v3, Lcom/sec/chaton/e/l;->b:Lcom/sec/chaton/e/l;

    if-ne v2, v3, :cond_f

    .line 176
    const/4 v2, 0x1

    move/from16 v20, v2

    move v2, v14

    .line 181
    :goto_4
    sget-object v3, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-eq v4, v3, :cond_8

    .line 182
    sget-object v3, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v4, v3, :cond_6

    sget-object v3, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-eq v4, v3, :cond_6

    sget-object v3, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    if-ne v4, v3, :cond_8

    .line 194
    :cond_6
    if-nez v20, :cond_7

    if-eqz v2, :cond_10

    :cond_7
    array-length v3, v15

    const/4 v5, 0x5

    if-lt v3, v5, :cond_10

    .line 197
    const/4 v3, 0x2

    aget-object v18, v15, v3

    .line 198
    const/4 v3, 0x3

    aget-object v19, v15, v3

    .line 200
    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v11

    .line 203
    new-instance v13, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v13, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 205
    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v14

    .line 206
    new-instance v8, Lcom/sec/chaton/chat/ff;

    move-object/from16 v9, p0

    move-object v10, v4

    move-object/from16 v15, p3

    move-object/from16 v16, p5

    move-object/from16 v17, v7

    invoke-direct/range {v8 .. v19}, Lcom/sec/chaton/chat/ff;-><init>(Lcom/sec/chaton/chat/fe;Lcom/sec/chaton/e/w;JLandroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 246
    :cond_8
    :goto_5
    invoke-static {}, Lcom/sec/chaton/a/cu;->newBuilder()Lcom/sec/chaton/a/cv;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/a/cv;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "chaton_id"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/a/cv;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/a/cv;->a(J)Lcom/sec/chaton/a/cv;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/a/cv;->b(J)Lcom/sec/chaton/a/cv;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/cv;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->p()Lcom/sec/chaton/a/bc;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cv;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->r()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/a/cv;->c(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/a/cv;->d()Lcom/sec/chaton/a/cu;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 259
    if-eqz v20, :cond_17

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 260
    const/16 v22, 0x1

    move/from16 v14, v22

    .line 264
    :goto_6
    if-nez v20, :cond_9

    if-eqz v2, :cond_a

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/Long;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 267
    :cond_9
    if-nez p1, :cond_a

    move-object/from16 v8, p0

    move/from16 v9, v20

    move-object/from16 v10, p3

    move-object/from16 v11, p5

    move-object/from16 v12, p4

    move-object/from16 v13, p2

    .line 269
    invoke-direct/range {v8 .. v13}, Lcom/sec/chaton/chat/fe;->a(ZLjava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;Lcom/sec/chaton/a/cr;)V

    .line 275
    :cond_a
    if-nez v20, :cond_b

    if-eqz v2, :cond_16

    .line 276
    :cond_b
    const/4 v2, 0x1

    move v10, v2

    .line 279
    :goto_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/sec/chaton/e/a/n;->m(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 280
    invoke-static {}, Lcom/sec/chaton/chat/b/c;->c()Lcom/sec/chaton/chat/b/c;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v9

    const-string v11, "chaton_id"

    const-string v12, ""

    invoke-virtual {v9, v11, v12}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_12

    const/4 v9, 0x1

    :goto_8
    move-object v7, v8

    move-object/from16 v8, p3

    invoke-virtual/range {v2 .. v9}, Lcom/sec/chaton/chat/b/c;->a(Ljava/lang/Long;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 283
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable translate, inboxNo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ForwardMessageHandler"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v14

    move v5, v10

    move/from16 v4, v23

    move/from16 v6, v20

    goto/16 :goto_1

    .line 114
    :cond_c
    sget-object v3, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 118
    :cond_d
    sget-object v3, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 119
    sget-object v3, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 135
    :cond_e
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 177
    :cond_f
    sget-object v3, Lcom/sec/chaton/e/l;->c:Lcom/sec/chaton/e/l;

    if-ne v2, v3, :cond_18

    .line 178
    const/4 v2, 0x1

    move/from16 v20, v13

    goto/16 :goto_4

    .line 234
    :cond_10
    array-length v3, v15

    const/4 v4, 0x4

    if-lt v3, v4, :cond_11

    .line 235
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No need to update and insert: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v4, v15, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 238
    :cond_11
    const-string v3, "Token Error"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 280
    :cond_12
    const/4 v9, 0x0

    goto/16 :goto_8

    .line 296
    :pswitch_1
    move-object/from16 v0, p6

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298
    invoke-static {}, Lcom/sec/chaton/a/cu;->newBuilder()Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v6, ""

    invoke-virtual {v3, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/a/cv;->a(J)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/a/cv;->b(J)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->p()Lcom/sec/chaton/a/bc;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->c(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/cv;->d()Lcom/sec/chaton/a/cu;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move/from16 v7, v22

    move v4, v5

    move v6, v13

    move/from16 v5, v21

    .line 308
    goto/16 :goto_1

    .line 312
    :pswitch_2
    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 313
    const/4 v3, 0x1

    .line 314
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/a/cr;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 316
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_13

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v6

    const-string v7, "chaton_id"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 317
    move-object/from16 v0, p8

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 318
    move-object/from16 v0, p8

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v3

    .line 320
    :goto_9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-virtual {v0, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    :cond_13
    invoke-static {}, Lcom/sec/chaton/a/cu;->newBuilder()Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/sec/chaton/a/cr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/a/cv;->a(J)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/a/cv;->b(J)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->p()Lcom/sec/chaton/a/bc;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->c(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/cv;->d()Lcom/sec/chaton/a/cu;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move/from16 v7, v22

    move v4, v5

    move v6, v13

    move/from16 v5, v21

    .line 333
    goto/16 :goto_1

    .line 336
    :pswitch_3
    invoke-static {}, Lcom/sec/chaton/a/cu;->newBuilder()Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chaton_id"

    const-string v6, ""

    invoke-virtual {v3, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->b(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/a/cv;->a(J)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/a/cv;->b(J)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/dp;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->p()Lcom/sec/chaton/a/bc;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->a(Lcom/sec/chaton/a/bc;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/sec/chaton/a/cr;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/a/cv;->c(Ljava/lang/String;)Lcom/sec/chaton/a/cv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/a/cv;->d()Lcom/sec/chaton/a/cu;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_14
    move v2, v3

    goto/16 :goto_9

    :cond_15
    move v7, v14

    move v5, v10

    move/from16 v4, v23

    move/from16 v6, v20

    goto/16 :goto_1

    :cond_16
    move/from16 v10, v21

    goto/16 :goto_7

    :cond_17
    move/from16 v14, v22

    goto/16 :goto_6

    :cond_18
    move/from16 v20, v13

    move v2, v14

    goto/16 :goto_4

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 656
    sget-object v0, Lcom/sec/chaton/chat/fe;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Ljava/lang/String;ILcom/sec/chaton/a/cr;Lcom/sec/chaton/e/a/p;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 502
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 504
    if-eqz p3, :cond_0

    .line 505
    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/e/a/y;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 507
    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v3

    iget-wide v5, p4, Lcom/sec/chaton/e/a/p;->g:J

    cmp-long v0, v3, v5

    if-ltz v0, :cond_0

    .line 510
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 511
    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "chaton_id"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 512
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    :goto_0
    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->n()Lcom/sec/chaton/a/dp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/a/dp;->getNumber()I

    move-result v4

    invoke-static {v0, v4}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v0

    .line 518
    invoke-virtual {v0}, Lcom/sec/chaton/e/w;->a()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 521
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPreExecute - (lastmsg) last msg sender : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v4

    const-string v5, "0999"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-ne v4, v7, :cond_3

    .line 524
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 525
    const-string v0, "push_message"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 526
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 534
    :goto_1
    invoke-static {v2}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 536
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p4, Lcom/sec/chaton/e/a/p;->e:Ljava/lang/String;

    .line 537
    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v2

    iput-wide v2, p4, Lcom/sec/chaton/e/a/p;->j:J

    .line 538
    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p4, Lcom/sec/chaton/e/a/p;->k:Ljava/lang/String;

    .line 539
    invoke-virtual {p3}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v2

    iput-wide v2, p4, Lcom/sec/chaton/e/a/p;->g:J

    .line 552
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553
    const/4 p2, 0x0

    .line 558
    :cond_1
    invoke-static {v1, p1, p4, p2}, Lcom/sec/chaton/e/a/n;->a(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/sec/chaton/e/a/p;I)I

    .line 559
    return-void

    .line 514
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 527
    :catch_0
    move-exception v0

    .line 528
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 531
    :cond_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v1, 0x0

    .line 636
    sget-object v0, Lcom/sec/chaton/chat/fe;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    sget-object v0, Lcom/sec/chaton/chat/fe;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 639
    :cond_0
    sget-object v0, Lcom/sec/chaton/chat/fe;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/a/cr;

    .line 642
    invoke-virtual {v0}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 643
    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 644
    const-string v6, ","

    const/4 v7, 0x3

    invoke-static {v5, v6, v7}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v5

    .line 645
    array-length v6, v5

    if-le v6, v10, :cond_2

    .line 646
    aget-object v6, v5, v1

    .line 647
    const/4 v7, 0x1

    aget-object v7, v5, v7

    .line 648
    aget-object v5, v5, v10

    .line 649
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Insert noti msg, status: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", number: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", name: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ForwardMessageHandler"

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 653
    :cond_3
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 399
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 400
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 401
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 402
    invoke-static {}, Lcom/sec/chaton/e/v;->a()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 403
    const-string v4, "message_sever_id"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "count"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 404
    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 408
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 409
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "com.sec.chaton.provider"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    :cond_1
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 416
    return-void

    .line 411
    :catch_0
    move-exception v0

    .line 412
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 414
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    throw v0
.end method

.method public a(ZLjava/util/ArrayList;Ljava/lang/String;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/a/cr;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 576
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 577
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v3

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-virtual {v3, v0, v1}, Lcom/sec/chaton/chat/fe;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 579
    if-nez p1, :cond_0

    .line 580
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processNotiTypeMessage() break : isFirstMsgOfFullMsg("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ForwardMessageHandler"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    :goto_0
    return-void

    .line 585
    :cond_0
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/fe;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/chaton/a/cr;

    .line 586
    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->j()Ljava/lang/String;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 587
    const-string v4, ""

    .line 588
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 589
    array-length v8, v6

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v8, :cond_6

    aget-object v4, v6, v5

    .line 590
    const-string v10, ","

    const/4 v11, 0x3

    invoke-static {v4, v10, v11}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v10

    .line 591
    array-length v4, v10

    const/4 v11, 0x2

    if-le v4, v11, :cond_3

    .line 592
    const/4 v4, 0x1

    aget-object v11, v10, v4

    .line 593
    const/4 v4, 0x2

    aget-object v4, v10, v4

    .line 594
    invoke-static {v2, v11, v4}, Lcom/sec/chaton/e/a/y;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 595
    move-object/from16 v0, p3

    invoke-static {v2, v11, v0}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    .line 596
    sget-object v4, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    .line 597
    const/4 v14, 0x0

    aget-object v14, v10, v14

    invoke-static {v14}, Lcom/sec/chaton/e/aj;->a(Ljava/lang/String;)Lcom/sec/chaton/e/aj;

    move-result-object v14

    sget-object v15, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    if-ne v14, v15, :cond_5

    .line 598
    if-nez v13, :cond_2

    .line 599
    move-object/from16 v0, p3

    invoke-static {v2, v0, v11, v12}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 600
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v10, "chaton_id"

    const-string v13, ""

    invoke-virtual {v4, v10, v13}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 601
    sget-object v4, Lcom/sec/chaton/e/aj;->b:Lcom/sec/chaton/e/aj;

    .line 613
    :cond_2
    :goto_3
    sget-object v10, Lcom/sec/chaton/e/aj;->a:Lcom/sec/chaton/e/aj;

    if-eq v4, v10, :cond_3

    .line 614
    const-string v10, "%d,%s,%s"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual {v4}, Lcom/sec/chaton/e/aj;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v13, v14

    const/4 v4, 0x1

    aput-object v11, v13, v4

    const/4 v4, 0x2

    invoke-static {v12}, Lcom/sec/chaton/chat/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v13, v4

    invoke-static {v10, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, ";"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 589
    :cond_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 604
    :cond_4
    sget-object v4, Lcom/sec/chaton/e/aj;->c:Lcom/sec/chaton/e/aj;

    goto :goto_3

    .line 607
    :cond_5
    const/4 v14, 0x0

    aget-object v10, v10, v14

    invoke-static {v10}, Lcom/sec/chaton/e/aj;->a(Ljava/lang/String;)Lcom/sec/chaton/e/aj;

    move-result-object v10

    sget-object v14, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    if-ne v10, v14, :cond_2

    .line 608
    if-eqz v13, :cond_2

    .line 609
    move-object/from16 v0, p3

    invoke-static {v2, v0, v11}, Lcom/sec/chaton/e/a/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    sget-object v4, Lcom/sec/chaton/e/aj;->d:Lcom/sec/chaton/e/aj;

    goto :goto_3

    .line 619
    :cond_6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 620
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 621
    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->l()J

    move-result-wide v5

    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->h()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v3, p3

    invoke-static/range {v2 .. v8}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    goto/16 :goto_1

    .line 625
    :cond_7
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/sec/chaton/chat/fe;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_8

    .line 626
    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/sec/chaton/e/a/n;->g(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 631
    :cond_8
    invoke-static {}, Lcom/sec/chaton/chat/fe;->a()Lcom/sec/chaton/chat/fe;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/sec/chaton/chat/fe;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public a(ZLjava/util/List;Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/a/cu;",
            ">;",
            "Lcom/sec/chaton/e/r;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 562
    if-nez p1, :cond_0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 563
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestReadMessage - isForce("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), isFrontInbox("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ForwardMessageHandler"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    :goto_0
    return-void

    .line 567
    :cond_0
    invoke-static {p5, p3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v0

    .line 568
    if-eqz v0, :cond_1

    .line 569
    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/chaton/d/o;->a(Ljava/util/List;Lcom/sec/chaton/e/r;Ljava/lang/String;)J

    goto :goto_0

    .line 571
    :cond_1
    const-string v0, "ForwardMessageHandler - updateInbox() : message control is null"

    const-string v1, "ForwardMessageHandler"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;Lcom/sec/chaton/a/cr;)Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 419
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 422
    invoke-static {v8, p1}, Lcom/sec/chaton/e/a/n;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    :goto_0
    return v7

    .line 426
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 428
    invoke-virtual {p4}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 432
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {v8, v5, p1}, Lcom/sec/chaton/e/a/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 433
    invoke-virtual {p4}, Lcom/sec/chaton/a/cr;->d()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/fe;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 440
    :goto_1
    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    if-ne p3, v2, :cond_3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    .line 460
    :goto_2
    :try_start_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 461
    const-string v0, "com.sec.chaton.provider"

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 467
    :cond_1
    :goto_3
    const/16 v0, 0xb

    invoke-static {v8, p1, v0}, Lcom/sec/chaton/e/a/n;->c(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    .line 469
    if-eqz v2, :cond_2

    .line 470
    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    invoke-static {v0, p1, p2, v1, v2}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;J)V

    :cond_2
    move v7, v6

    .line 473
    goto :goto_0

    .line 446
    :cond_3
    invoke-virtual {p4}, Lcom/sec/chaton/a/cr;->f()I

    move-result v9

    .line 447
    const-string v2, ""

    .line 448
    :goto_4
    if-ge v7, v9, :cond_6

    .line 449
    invoke-virtual {p4, v7}, Lcom/sec/chaton/a/cr;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 450
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "old_chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 448
    :cond_4
    :goto_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 454
    :cond_5
    invoke-virtual {p4, v7}, Lcom/sec/chaton/a/cr;->a(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/chat/fe;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 455
    goto :goto_5

    .line 463
    :catch_0
    move-exception v0

    .line 464
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    move v2, v0

    goto :goto_2

    :cond_7
    move v0, v7

    goto/16 :goto_1
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 660
    sget-object v0, Lcom/sec/chaton/chat/fe;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 661
    return-void
.end method
