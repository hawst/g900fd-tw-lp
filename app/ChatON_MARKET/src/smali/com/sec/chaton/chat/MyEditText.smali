.class public Lcom/sec/chaton/chat/MyEditText;
.super Lcom/sec/chaton/widget/AdaptableEditText;
.source "MyEditText.java"


# instance fields
.field a:I

.field private b:Lcom/sec/widget/w;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/chaton/widget/AdaptableEditText;-><init>(Landroid/content/Context;)V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/chat/MyEditText;->a:I

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/widget/AdaptableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/chat/MyEditText;->a:I

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/widget/AdaptableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/chat/MyEditText;->a:I

    .line 35
    return-void
.end method


# virtual methods
.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/chat/MyEditText;->b:Lcom/sec/widget/w;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/chat/MyEditText;->b:Lcom/sec/widget/w;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lcom/sec/widget/w;->a(Lcom/sec/chaton/chat/MyEditText;Ljava/lang/String;)V

    .line 59
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/widget/AdaptableEditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onSelectionChanged(II)V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/widget/AdaptableEditText;->onSelectionChanged(II)V

    .line 78
    return-void
.end method

.method public onTextContextMenuItem(I)Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 85
    const v0, 0x1020022

    if-ne p1, v0, :cond_5

    .line 86
    const-string v1, ""

    .line 87
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/sec/chaton/chat/MyEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 89
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    .line 90
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 91
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 111
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/MyEditText;->getSelectionStart()I

    move-result v1

    .line 112
    invoke-virtual {p0}, Lcom/sec/chaton/chat/MyEditText;->getSelectionEnd()I

    move-result v2

    .line 114
    invoke-virtual {p0}, Lcom/sec/chaton/chat/MyEditText;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/chaton/chat/MyEditText;->getLineHeight()I

    move-result v5

    int-to-float v5, v5

    const v6, 0x3f99999a    # 1.2f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v3, v0, v5}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 116
    invoke-virtual {p0}, Lcom/sec/chaton/chat/MyEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    add-int/2addr v0, v5

    sub-int v5, v2, v1

    sub-int/2addr v0, v5

    const/16 v5, 0x2710

    if-le v0, v5, :cond_3

    .line 117
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0031

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 125
    :goto_1
    const/4 v0, 0x1

    .line 128
    :goto_2
    return v0

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/MyEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 95
    invoke-virtual {v0}, Landroid/text/ClipboardManager;->hasText()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    invoke-virtual {v0}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 97
    invoke-virtual {v0}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    goto :goto_0

    .line 99
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_2

    .line 100
    const-string v0, "clipboard getText() is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 119
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/chat/MyEditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 120
    invoke-virtual {p0}, Lcom/sec/chaton/chat/MyEditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 122
    :cond_4
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/2addr v0, v1

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/MyEditText;->setSelection(II)V

    goto :goto_1

    .line 128
    :cond_5
    invoke-super {p0, p1}, Lcom/sec/chaton/widget/AdaptableEditText;->onTextContextMenuItem(I)Z

    move-result v0

    goto :goto_2
.end method

.method public setOnEditTextImeBackListener(Lcom/sec/widget/w;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/chaton/chat/MyEditText;->b:Lcom/sec/widget/w;

    .line 44
    return-void
.end method
