.class public Lcom/sec/chaton/chat/background/d;
.super Lcom/sec/chaton/chat/background/f;
.source "GetMessageJobContainer.java"


# instance fields
.field private final v:Ljava/lang/String;

.field private w:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/os/Looper;Landroid/app/Service;II)V
    .locals 2

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/sec/chaton/chat/background/f;-><init>(Landroid/os/Handler;Landroid/os/Looper;Landroid/app/Service;II)V

    .line 30
    const-string v0, "GetMessageJonContainer"

    iput-object v0, p0, Lcom/sec/chaton/chat/background/d;->v:Ljava/lang/String;

    .line 36
    iput-object p1, p0, Lcom/sec/chaton/chat/background/d;->m:Landroid/os/Handler;

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    .line 38
    invoke-static {}, Lcom/sec/chaton/util/bh;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/d;->l:J

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/chat/background/d;->n:Z

    .line 40
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/chaton/chat/background/d;->p:J

    .line 41
    iput-object p3, p0, Lcom/sec/chaton/chat/background/d;->s:Landroid/app/Service;

    .line 42
    iput p4, p0, Lcom/sec/chaton/chat/background/d;->t:I

    .line 43
    iput p5, p0, Lcom/sec/chaton/chat/background/d;->u:I

    .line 44
    new-instance v0, Lcom/sec/chaton/chat/background/e;

    invoke-direct {v0, p0, p2}, Lcom/sec/chaton/chat/background/e;-><init>(Lcom/sec/chaton/chat/background/d;Landroid/os/Looper;)V

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/background/d;->a(Landroid/os/Handler;)V

    .line 45
    return-void
.end method

.method private a(I)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 119
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "result_code : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GetMessageJonContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "request_id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lcom/sec/chaton/chat/background/d;->k:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GetMessageJonContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "request_msg_id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lcom/sec/chaton/chat/background/d;->p:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GetMessageJonContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_0
    new-instance v8, Landroid/content/Intent;

    const-string v0, "com.sec.chaton.chat.background.RES_FULL_MESSAGE"

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 127
    invoke-virtual {v8, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 128
    const-string v0, "request_id"

    iget-wide v3, p0, Lcom/sec/chaton/chat/background/d;->k:J

    invoke-virtual {v8, v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 131
    if-ne p1, v6, :cond_2

    .line 132
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/e/c;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    aput-object v3, v4, v7

    iget-wide v9, p0, Lcom/sec/chaton/chat/background/d;->p:J

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v6

    move-object v3, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 139
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 142
    :cond_1
    if-eqz v1, :cond_5

    .line 143
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move p1, v7

    .line 171
    :cond_2
    :goto_0
    const-string v0, "result_code"

    invoke-virtual {v8, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 172
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 173
    return-void

    .line 146
    :cond_3
    const-string v0, "inbox_no"

    iget-object v2, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    invoke-virtual {v8, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    const-string v0, "message_id"

    iget-wide v2, p0, Lcom/sec/chaton/chat/background/d;->p:J

    invoke-virtual {v8, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 148
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 151
    const-string v0, "sender_chatonv_available"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "true"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v6

    .line 155
    :goto_1
    const-string v2, "message_type"

    const-string v3, "message_type"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v8, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 156
    const-string v2, "message"

    const-string v3, "message"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    const-string v2, "sender_id"

    const-string v3, "sender_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    const-string v2, "sender_name"

    const-string v3, "sender_name"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string v2, "sender_phoneNumber"

    const-string v3, "sender_phoneNumber"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    const-string v2, "chat_type"

    const-string v3, "chat_type"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v8, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 161
    const-string v2, "received_time"

    const-string v3, "received_time"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v8, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 162
    const-string v2, "sender_chatonv_available"

    invoke-virtual {v8, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 163
    const-string v0, "chatroom_title"

    const-string v2, "chatroom_title"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/chat/background/d;->e()V

    goto/16 :goto_0

    :cond_4
    move v0, v7

    goto/16 :goto_1

    :cond_5
    move p1, v7

    goto/16 :goto_0
.end method

.method private e()V
    .locals 5

    .prologue
    .line 331
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 332
    const-string v1, "inbox_unread_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 333
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "inbox_no=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 335
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    sget v2, Lcom/sec/chaton/chat/notification/a;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;I)V

    .line 336
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/chat/background/h;)I
    .locals 5

    .prologue
    const/4 v0, 0x4

    .line 50
    if-nez p1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->b()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/chaton/chat/background/d;->k:J

    .line 55
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->h()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/chaton/chat/background/d;->p:J

    .line 57
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/d;->b:Ljava/lang/String;

    .line 58
    invoke-virtual {p1}, Lcom/sec/chaton/chat/background/h;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/d;->e:Ljava/lang/String;

    .line 60
    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/d;->p:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 70
    iget v1, p0, Lcom/sec/chaton/chat/background/d;->u:I

    if-ne v1, v0, :cond_6

    .line 71
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_0

    .line 72
    const-string v1, "Need to network"

    const-string v2, "GetMessageJonContainer"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 75
    const/4 v2, -0x3

    if-eq v2, v1, :cond_1

    const/4 v2, -0x2

    if-ne v2, v1, :cond_3

    .line 76
    :cond_1
    const/4 v0, 0x2

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/d;->p:J

    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    move-result v0

    .line 99
    :cond_2
    :goto_0
    return v0

    .line 79
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/background/d;->a(Lcom/sec/chaton/e/r;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 80
    const/16 v0, 0x9

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/d;->p:J

    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    move-result v0

    goto :goto_0

    .line 83
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->f:Lcom/sec/chaton/e/r;

    sget-object v2, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    if-ne v1, v2, :cond_5

    .line 84
    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/d;->p:J

    invoke-virtual {p0, v4, v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    move-result v0

    goto :goto_0

    .line 87
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->f:Lcom/sec/chaton/e/r;

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/background/d;->b(Lcom/sec/chaton/e/r;)I

    move-result v1

    .line 88
    if-eq v1, v0, :cond_2

    .line 89
    iget-wide v2, p0, Lcom/sec/chaton/chat/background/d;->p:J

    invoke-virtual {p0, v4, v1, v2, v3}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    move-result v0

    goto :goto_0

    .line 92
    :cond_6
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_7

    .line 93
    const-string v1, "No need to network"

    const-string v2, "GetMessageJonContainer"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_7
    iget-wide v1, p0, Lcom/sec/chaton/chat/background/d;->l:J

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(J)Z

    .line 96
    iget-wide v1, p0, Lcom/sec/chaton/chat/background/d;->p:J

    invoke-virtual {p0, v0, v0, v1, v2}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    move-result v0

    goto :goto_0
.end method

.method protected a(J)Z
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->m:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 320
    const/4 v0, 0x0

    .line 326
    :goto_0
    return v0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->m:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 324
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 325
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, p1, p2}, Ljava/lang/Long;-><init>(J)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 326
    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->m:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    goto :goto_0
.end method

.method protected a(Lcom/sec/chaton/e/r;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v6

    .line 223
    :goto_0
    return v0

    .line 182
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/q;->a:Landroid/net/Uri;

    const-string v3, "inbox_no=?"

    new-array v4, v7, [Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    aput-object v5, v4, v6

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 184
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 185
    const-string v1, "inbox_server_ip"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/d;->i:Ljava/lang/String;

    .line 186
    const-string v1, "inbox_server_port"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/chat/background/d;->j:I

    .line 187
    const-string v1, "inbox_chat_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/d;->f:Lcom/sec/chaton/e/r;

    .line 188
    const-string v1, "inbox_session_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/chat/background/d;->d:Ljava/lang/String;

    .line 189
    const-string v1, "lasst_session_merge_time"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/chaton/chat/background/d;->w:J

    .line 190
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->f:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 199
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/chat/background/d;->d:Ljava/lang/String;

    .line 200
    sget-object v0, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    iput-object v0, p0, Lcom/sec/chaton/chat/background/d;->f:Lcom/sec/chaton/e/r;

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->f:Lcom/sec/chaton/e/r;

    invoke-static {v0, v1}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    if-nez v0, :cond_5

    move v0, v6

    .line 206
    goto/16 :goto_0

    .line 192
    :cond_3
    if-eqz v0, :cond_4

    .line 193
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    move v0, v6

    .line 195
    goto/16 :goto_0

    .line 209
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->a(Landroid/os/Handler;)Z

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->c(J)V

    .line 211
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/chaton/util/bk;->b()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 217
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->f:Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_6

    const-string v0, "null"

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 218
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->e(Ljava/lang/String;)V

    :goto_2
    move v0, v7

    .line 223
    goto/16 :goto_0

    .line 214
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/d;->i:Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/chat/background/d;->j:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_1

    .line 220
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    iget-wide v1, p0, Lcom/sec/chaton/chat/background/d;->w:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->a(J)V

    goto :goto_2
.end method

.method public a(ZIJ)Z
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 107
    const-string v0, "packagename is empty"

    const-string v1, "GetMessageJonContainer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_0
    const/4 v0, 0x0

    .line 114
    :goto_0
    return v0

    .line 112
    :cond_1
    invoke-direct {p0, p2}, Lcom/sec/chaton/chat/background/d;->a(I)V

    .line 114
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b(Lcom/sec/chaton/e/r;)I
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->f:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/d;->e:Ljava/lang/String;

    iget-wide v3, p0, Lcom/sec/chaton/chat/background/d;->p:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/background/d;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/background/d;->c:Ljava/lang/String;

    iget-wide v6, p0, Lcom/sec/chaton/chat/background/d;->w:J

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/d/o;->a(Lcom/sec/chaton/e/r;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v0

    .line 233
    if-nez v0, :cond_0

    .line 234
    iget-wide v0, p0, Lcom/sec/chaton/chat/background/d;->p:J

    invoke-virtual {p0, v8, v8, v0, v1}, Lcom/sec/chaton/chat/background/d;->a(ZIJ)Z

    .line 235
    iget-wide v0, p0, Lcom/sec/chaton/chat/background/d;->l:J

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/chat/background/d;->a(J)Z

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/chat/background/d;->h:Lcom/sec/chaton/d/o;

    iget-object v1, p0, Lcom/sec/chaton/chat/background/d;->f:Lcom/sec/chaton/e/r;

    iget-object v2, p0, Lcom/sec/chaton/chat/background/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/o;->b(Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 239
    :cond_0
    return v8
.end method
