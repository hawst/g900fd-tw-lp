.class public Lcom/sec/chaton/chat/eb;
.super Landroid/app/AlertDialog;
.source "ChatInfoMoreFragment.java"


# instance fields
.field public final a:Ljava/lang/String;

.field b:Lcom/sec/chaton/e/b/d;

.field final synthetic c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

.field private d:Lcom/sec/chaton/widget/ClearableEditText;

.field private e:Landroid/content/Context;

.field private f:Landroid/text/TextWatcher;

.field private g:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/chat/ChatInfoMoreFragment;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 409
    iput-object p1, p0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    .line 410
    invoke-direct {p0, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 414
    const-class v0, Lcom/sec/chaton/chat/eb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/eb;->a:Ljava/lang/String;

    .line 481
    new-instance v0, Lcom/sec/chaton/chat/ed;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/ed;-><init>(Lcom/sec/chaton/chat/eb;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/eb;->b:Lcom/sec/chaton/e/b/d;

    .line 519
    new-instance v0, Lcom/sec/chaton/chat/ee;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/ee;-><init>(Lcom/sec/chaton/chat/eb;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/eb;->f:Landroid/text/TextWatcher;

    .line 611
    new-instance v0, Lcom/sec/chaton/chat/ef;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/ef;-><init>(Lcom/sec/chaton/chat/eb;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/eb;->g:Landroid/os/Handler;

    .line 411
    invoke-direct {p0}, Lcom/sec/chaton/chat/eb;->a()V

    .line 412
    return-void
.end method

.method private a()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x0

    const/4 v2, -0x1

    const/4 v3, -0x2

    const/4 v8, 0x1

    .line 442
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eb;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/eb;->e:Landroid/content/Context;

    .line 444
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->e:Landroid/content/Context;

    const v4, 0x7f0b00e2

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/eb;->setTitle(Ljava/lang/CharSequence;)V

    .line 445
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eb;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "layout_inflater"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 446
    const v4, 0x7f03001b

    invoke-virtual {v0, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 447
    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/eb;->setView(Landroid/view/View;)V

    .line 449
    const v4, 0x7f070075

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/chat/eb;->d:Lcom/sec/chaton/widget/ClearableEditText;

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->d:Lcom/sec/chaton/widget/ClearableEditText;

    new-array v4, v8, [Landroid/text/InputFilter;

    new-instance v5, Lcom/sec/chaton/util/w;

    iget-object v6, p0, Lcom/sec/chaton/chat/eb;->e:Landroid/content/Context;

    const/16 v7, 0x1e

    invoke-direct {v5, v6, v7}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v5, v4, v9

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 451
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->d:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v4, p0, Lcom/sec/chaton/chat/eb;->f:Landroid/text/TextWatcher;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->d:Lcom/sec/chaton/widget/ClearableEditText;

    new-array v4, v8, [C

    const/16 v5, 0x2c

    aput-char v5, v4, v9

    const v5, 0x7f0b0133

    invoke-static {v0, v4, v5}, Lcom/sec/widget/at;->a(Lcom/sec/chaton/widget/ClearableEditText;[CI)V

    .line 456
    invoke-virtual {p0}, Lcom/sec/chaton/chat/eb;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 462
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v0, v4, :cond_0

    .line 468
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->e:Landroid/content/Context;

    const v4, 0x7f0b0037

    invoke-virtual {v0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v4, Lcom/sec/chaton/chat/ec;

    invoke-direct {v4, p0}, Lcom/sec/chaton/chat/ec;-><init>(Lcom/sec/chaton/chat/eb;)V

    invoke-virtual {p0, v3, v0, v4}, Lcom/sec/chaton/chat/eb;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 476
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->e:Landroid/content/Context;

    const v3, 0x7f0b003a

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v0, v1

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v2, v3, v0}, Lcom/sec/chaton/chat/eb;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 478
    invoke-virtual {p0, v8}, Lcom/sec/chaton/chat/eb;->setInverseBackgroundForced(Z)V

    .line 479
    return-void

    :cond_0
    move v10, v3

    move v3, v2

    move v2, v10

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/chat/eb;)V
    .locals 0

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/sec/chaton/chat/eb;->b()V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/chat/eb;)Lcom/sec/chaton/widget/ClearableEditText;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->d:Lcom/sec/chaton/widget/ClearableEditText;

    return-object v0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 551
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->d:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 553
    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->d:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 555
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 557
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_1

    .line 558
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/eb;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 563
    :goto_0
    new-instance v1, Lcom/sec/chaton/e/b/j;

    iget-object v2, p0, Lcom/sec/chaton/chat/eb;->b:Lcom/sec/chaton/e/b/d;

    invoke-direct {v1, v2, v0, v4}, Lcom/sec/chaton/e/b/j;-><init>(Lcom/sec/chaton/e/b/d;Ljava/lang/String;I)V

    .line 564
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v0

    invoke-static {v0, v4, v1}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    .line 567
    :cond_0
    return-void

    .line 560
    :cond_1
    const/4 v1, -0x2

    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/eb;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/chat/eb;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/chat/eb;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->g:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 577
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NEW GROUP NAME : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MEMBER COUNT  : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->f(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BLOCK BUDDY COUNT  : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->f(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v0, v1

    if-lez v0, :cond_4

    .line 582
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 583
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->f(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    .line 586
    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->d(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/inner/Buddy;

    .line 587
    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 588
    const/4 v1, 0x1

    :goto_2
    move v3, v1

    goto :goto_1

    .line 592
    :cond_1
    if-nez v3, :cond_0

    .line 593
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 596
    :cond_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 597
    const/4 v1, -0x3

    if-eq v1, v0, :cond_3

    const/4 v1, -0x2

    if-ne v1, v0, :cond_5

    .line 598
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->e:Landroid/content/Context;

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 607
    :cond_4
    :goto_3
    return-void

    .line 600
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "REQUEST BUDDY COUNT  : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->g:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 603
    const-string v1, "group"

    const/4 v3, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    const/16 v6, 0x145

    move-object v4, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/h;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)I

    .line 604
    iget-object v0, p0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->h(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)V

    goto :goto_3

    :cond_6
    move v1, v3

    goto :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 426
    const-string v0, "onCreate()"

    iget-object v1, p0, Lcom/sec/chaton/chat/eb;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 430
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 431
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/eb;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 438
    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/chaton/chat/eb;->setIcon(I)V

    .line 439
    return-void

    .line 434
    :cond_0
    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/chat/eb;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public show()V
    .locals 0

    .prologue
    .line 572
    invoke-super {p0}, Landroid/app/AlertDialog;->show()V

    .line 573
    return-void
.end method
