.class Lcom/sec/chaton/chat/cw;
.super Landroid/os/AsyncTask;
.source "ChatFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/chaton/chat/el;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/sec/chaton/chat/el;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Lcom/sec/chaton/chat/b/c;

.field final synthetic e:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/chat/b/c;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15711
    iput-object p1, p0, Lcom/sec/chaton/chat/cw;->e:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 15712
    iput-object p3, p0, Lcom/sec/chaton/chat/cw;->b:Ljava/lang/String;

    .line 15713
    iput-object p4, p0, Lcom/sec/chaton/chat/cw;->c:Ljava/lang/String;

    .line 15714
    iput-object p2, p0, Lcom/sec/chaton/chat/cw;->d:Lcom/sec/chaton/chat/b/c;

    .line 15715
    return-void
.end method


# virtual methods
.method protected varargs a([Lcom/sec/chaton/chat/el;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 15719
    aget-object v0, p1, v7

    iput-object v0, p0, Lcom/sec/chaton/chat/cw;->a:Lcom/sec/chaton/chat/el;

    .line 15720
    iget-object v0, p0, Lcom/sec/chaton/chat/cw;->d:Lcom/sec/chaton/chat/b/c;

    iget-object v1, p0, Lcom/sec/chaton/chat/cw;->a:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/cw;->a:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->az:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/chat/cw;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/chat/cw;->c:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/chaton/chat/cw;->e:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v6, v6, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/chaton/chat/cw;->a:Lcom/sec/chaton/chat/el;

    iget-boolean v8, v8, Lcom/sec/chaton/chat/el;->aF:Z

    if-nez v8, :cond_0

    move v7, v3

    :cond_0
    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/chat/b/c;->a(Ljava/lang/Long;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 15721
    return-object v0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 15726
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 15727
    iget-object v0, p0, Lcom/sec/chaton/chat/cw;->e:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aV(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/chat/cw;->e:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aV(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15728
    iget-object v0, p0, Lcom/sec/chaton/chat/cw;->e:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->aV(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 15730
    :cond_0
    if-eqz p1, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 15731
    iget-object v0, p0, Lcom/sec/chaton/chat/cw;->a:Lcom/sec/chaton/chat/el;

    iput-object p1, v0, Lcom/sec/chaton/chat/el;->aG:Ljava/lang/String;

    .line 15735
    :goto_0
    return-void

    .line 15733
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b03b3

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15705
    check-cast p1, [Lcom/sec/chaton/chat/el;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/cw;->a([Lcom/sec/chaton/chat/el;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15705
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/chat/cw;->a(Ljava/lang/String;)V

    return-void
.end method
