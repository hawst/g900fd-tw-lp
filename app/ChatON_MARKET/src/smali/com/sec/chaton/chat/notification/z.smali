.class public Lcom/sec/chaton/chat/notification/z;
.super Landroid/widget/Toast;
.source "ScreenNotificationToast.java"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/sec/chaton/chat/notification/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 30
    invoke-direct {p0, p1}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 32
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 33
    const v1, 0x7f030109

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 34
    const v0, 0x7f0703c8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/z;->a:Landroid/widget/TextView;

    .line 35
    const v0, 0x7f0703d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/z;->b:Landroid/widget/TextView;

    .line 37
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/notification/z;->setView(Landroid/view/View;)V

    .line 38
    invoke-virtual {p0, v3}, Lcom/sec/chaton/chat/notification/z;->setDuration(I)V

    .line 39
    const/16 v0, 0x30

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v1

    invoke-virtual {p0, v0, v3, v1}, Lcom/sec/chaton/chat/notification/z;->setGravity(III)V

    .line 40
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/sec/chaton/chat/notification/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/z;-><init>(Landroid/content/Context;)V

    .line 45
    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 50
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "OFF"

    const-string v2, "OFF"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_1

    .line 53
    const-string v0, "OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v2, "keyguard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 55
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_0

    .line 56
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isLocked:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isSecure:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_0
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    const-string v0, "ON"

    .line 64
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/z;->c:Lcom/sec/chaton/chat/notification/a;

    if-nez v0, :cond_0

    .line 70
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/z;->c:Lcom/sec/chaton/chat/notification/a;

    .line 73
    :cond_0
    const-string v0, "ON"

    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/z;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting show receive message"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/z;->a:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0007

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/z;->c:Lcom/sec/chaton/chat/notification/a;

    iget v0, v0, Lcom/sec/chaton/chat/notification/a;->m:I

    if-le v0, v3, :cond_2

    .line 77
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0048

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/chaton/chat/notification/z;->c:Lcom/sec/chaton/chat/notification/a;

    iget v4, v4, Lcom/sec/chaton/chat/notification/a;->m:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/z;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/z;->show()V

    .line 96
    return-void

    .line 79
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0047

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_3
    invoke-static {p3}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_5

    .line 84
    invoke-static {p2}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 85
    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 87
    :cond_4
    invoke-static {p2}, Lcom/sec/chaton/chat/eq;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v1, v0, v2}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/z;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/z;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 91
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/z;->b:Landroid/widget/TextView;

    invoke-static {p3}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    invoke-static {v1, p2, p1, p4}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
