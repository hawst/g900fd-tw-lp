.class Lcom/sec/chaton/chat/e;
.super Ljava/lang/Object;
.source "ChatFragment.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatFragment;)V
    .locals 0

    .prologue
    .line 1501
    iput-object p1, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1507
    invoke-static {}, Lcom/sec/chaton/util/am;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/am;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1508
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00c5

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1567
    :cond_1
    :goto_0
    return v8

    .line 1514
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1515
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1521
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->z(Lcom/sec/chaton/chat/ChatFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1526
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->A(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1527
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->B(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1528
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->B(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->bringToFront()V

    .line 1529
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->C(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1531
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->d(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/chat/MyEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/MyEditText;->requestFocus()Z

    .line 1533
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->i(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 1534
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->i(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 1537
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    iput-boolean v8, v0, Lcom/sec/chaton/chat/ChatFragment;->I:Z

    .line 1538
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/b;->b()V

    .line 1539
    iget-object v6, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    new-instance v0, Lcom/sec/chaton/multimedia/audio/k;

    iget-object v1, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v1, v1, Lcom/sec/chaton/chat/ChatFragment;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatFragment;->A(Lcom/sec/chaton/chat/ChatFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v3}, Lcom/sec/chaton/chat/ChatFragment;->D(Lcom/sec/chaton/chat/ChatFragment;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v4}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/audio/k;-><init>(Ljava/lang/String;Landroid/widget/LinearLayout;Landroid/os/Handler;Landroid/app/Activity;Landroid/view/View;)V

    invoke-static {v6, v0}, Lcom/sec/chaton/chat/ChatFragment;->a(Lcom/sec/chaton/chat/ChatFragment;Lcom/sec/chaton/multimedia/audio/k;)Lcom/sec/chaton/multimedia/audio/k;

    .line 1541
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_5

    .line 1542
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->E(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/multimedia/audio/k;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/k;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1547
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1549
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    invoke-static {v0, v1, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v8, :cond_4

    .line 1550
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/i;->a(Landroid/app/Activity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1565
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->n(Lcom/sec/chaton/chat/ChatFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto/16 :goto_0

    .line 1544
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/chat/e;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatFragment;->E(Lcom/sec/chaton/chat/ChatFragment;)Lcom/sec/chaton/multimedia/audio/k;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v7, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/audio/k;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method
