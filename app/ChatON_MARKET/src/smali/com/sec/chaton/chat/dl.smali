.class Lcom/sec/chaton/chat/dl;
.super Ljava/lang/Object;
.source "ChatInfoFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/ChatInfoFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/ChatInfoFragment;)V
    .locals 0

    .prologue
    .line 895
    iput-object p1, p0, Lcom/sec/chaton/chat/dl;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const v5, 0x7f0b0414

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 898
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 899
    add-int/lit8 p2, p2, 0x1

    .line 901
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 943
    :goto_0
    return-void

    .line 903
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 904
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 905
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 906
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 907
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 908
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 911
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/chat/dl;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/chat/ChatInfoFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 918
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/dl;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Lcom/sec/chaton/chat/ChatInfoFragment;Z)Z

    goto :goto_0

    .line 912
    :catch_0
    move-exception v0

    .line 913
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 914
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 915
    sget-object v1, Lcom/sec/chaton/chat/ChatInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 921
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 922
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 923
    const-string v1, "output"

    iget-object v2, p0, Lcom/sec/chaton/chat/dl;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v2}, Lcom/sec/chaton/chat/ChatInfoFragment;->t(Lcom/sec/chaton/chat/ChatInfoFragment;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 924
    const-string v1, "outputFormat"

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 926
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/chat/dl;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/chat/ChatInfoFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 933
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/chat/dl;->a:Lcom/sec/chaton/chat/ChatInfoFragment;

    invoke-static {v0, v4}, Lcom/sec/chaton/chat/ChatInfoFragment;->a(Lcom/sec/chaton/chat/ChatInfoFragment;Z)Z

    goto :goto_0

    .line 927
    :catch_1
    move-exception v0

    .line 928
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 929
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 930
    sget-object v1, Lcom/sec/chaton/chat/ChatInfoFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 901
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
