.class Lcom/sec/chaton/chat/ee;
.super Ljava/lang/Object;
.source "ChatInfoMoreFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/eb;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/eb;)V
    .locals 0

    .prologue
    .line 519
    iput-object p1, p0, Lcom/sec/chaton/chat/ee;->a:Lcom/sec/chaton/chat/eb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 547
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 524
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    const/16 v5, 0xe

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, -0x2

    .line 528
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 530
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 531
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v5, :cond_0

    .line 532
    iget-object v0, p0, Lcom/sec/chaton/chat/ee;->a:Lcom/sec/chaton/chat/eb;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/chat/eb;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 543
    :goto_0
    return-void

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/ee;->a:Lcom/sec/chaton/chat/eb;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/eb;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 537
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v5, :cond_2

    .line 538
    iget-object v0, p0, Lcom/sec/chaton/chat/ee;->a:Lcom/sec/chaton/chat/eb;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/chat/eb;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 540
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/ee;->a:Lcom/sec/chaton/chat/eb;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/chat/eb;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
