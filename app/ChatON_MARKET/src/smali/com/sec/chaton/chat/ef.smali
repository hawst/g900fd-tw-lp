.class Lcom/sec/chaton/chat/ef;
.super Landroid/os/Handler;
.source "ChatInfoMoreFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/chat/eb;


# direct methods
.method constructor <init>(Lcom/sec/chaton/chat/eb;)V
    .locals 0

    .prologue
    .line 611
    iput-object p1, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 614
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 616
    iget-object v0, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    invoke-static {v0}, Lcom/sec/chaton/chat/eb;->c(Lcom/sec/chaton/chat/eb;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 648
    :goto_0
    return-void

    .line 620
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 622
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v3, :cond_2

    .line 623
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 624
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->f(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 625
    iget-object v0, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    iget-object v0, v0, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v0}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->f(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/buddy/a/c;

    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 624
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 627
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    invoke-static {v0}, Lcom/sec/chaton/chat/eb;->b(Lcom/sec/chaton/chat/eb;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 628
    new-instance v1, Lcom/sec/chaton/e/b/j;

    iget-object v0, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    iget-object v4, v0, Lcom/sec/chaton/chat/eb;->b:Lcom/sec/chaton/e/b/d;

    new-array v0, v2, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v1, v4, v0, v5}, Lcom/sec/chaton/e/b/j;-><init>(Lcom/sec/chaton/e/b/d;[Ljava/lang/String;I)V

    .line 629
    invoke-static {}, Lcom/sec/chaton/e/a/w;->a()Lcom/sec/chaton/e/a/w;

    move-result-object v0

    invoke-static {v0, v5, v1}, Lcom/sec/chaton/e/a/w;->a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V

    goto :goto_0

    .line 631
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-static {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->g(Lcom/sec/chaton/chat/ChatInfoMoreFragment;)V

    .line 632
    iget-object v1, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    invoke-static {v1}, Lcom/sec/chaton/chat/eb;->c(Lcom/sec/chaton/chat/eb;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/o;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 633
    iget-object v0, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    invoke-static {v0}, Lcom/sec/chaton/chat/eb;->c(Lcom/sec/chaton/chat/eb;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0122

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0139

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/chat/eg;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/eg;-><init>(Lcom/sec/chaton/chat/ef;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/chat/ef;->a:Lcom/sec/chaton/chat/eb;

    iget-object v1, v1, Lcom/sec/chaton/chat/eb;->c:Lcom/sec/chaton/chat/ChatInfoMoreFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/chat/ChatInfoMoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0
.end method
