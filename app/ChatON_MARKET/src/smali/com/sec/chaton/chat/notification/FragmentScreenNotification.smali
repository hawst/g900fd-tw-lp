.class public Lcom/sec/chaton/chat/notification/FragmentScreenNotification;
.super Landroid/support/v4/app/Fragment;
.source "FragmentScreenNotification.java"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Landroid/os/Handler;

.field private final c:Ljava/lang/String;

.field private d:Landroid/widget/EditText;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/ImageView;

.field private i:Lcom/sec/common/f/c;

.field private j:Lcom/sec/chaton/d/o;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 44
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->c:Ljava/lang/String;

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->j:Lcom/sec/chaton/d/o;

    .line 175
    new-instance v0, Lcom/sec/chaton/chat/notification/k;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/k;-><init>(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)V

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->a:Landroid/os/Handler;

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Lcom/sec/chaton/d/o;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->j:Lcom/sec/chaton/d/o;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->l:I

    return v0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v0, 0x4

    const/4 v3, 0x0

    .line 152
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->e:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 153
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->d:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 155
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_0

    .line 156
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->k:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    .line 157
    if-le v1, v0, :cond_3

    .line 160
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "extra text:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->k:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_0
    iget v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->l:I

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_2

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->k:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->k:Ljava/lang/String;

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 168
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v1, v0, v2}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->d:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 173
    :goto_1
    return-void

    .line 171
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->d:Landroid/widget/EditText;

    iget v1, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->l:I

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->n:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->c()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->k:Ljava/lang/String;

    return-object v0
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 379
    iget-object v1, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->k:Ljava/lang/String;

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 381
    const-string v2, "mixed"

    aget-object v1, v1, v0

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 382
    const/4 v0, 0x1

    .line 384
    :cond_0
    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Z
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->d:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->h:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/a/a/l;)Lcom/sec/common/a/a;
    .locals 4

    .prologue
    const v3, 0x7f0b0037

    .line 216
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 218
    sget-object v1, Lcom/sec/chaton/a/a/l;->i:Lcom/sec/chaton/a/a/l;

    if-ne p1, v1, :cond_0

    .line 219
    const v1, 0x7f0b0195

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/chat/notification/l;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/notification/l;-><init>(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)V

    invoke-virtual {v1, v3, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 235
    :goto_0
    return-object v0

    .line 226
    :cond_0
    sget-object v1, Lcom/sec/chaton/a/a/l;->j:Lcom/sec/chaton/a/a/l;

    if-ne p1, v1, :cond_1

    .line 227
    const v1, 0x7f0b00c8

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/chat/notification/m;

    invoke-direct {v2, p0}, Lcom/sec/chaton/chat/notification/m;-><init>(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)V

    invoke-virtual {v1, v3, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    goto :goto_0

    .line 235
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->k:Ljava/lang/String;

    .line 85
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "msgType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->l:I

    .line 86
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->n:Ljava/lang/String;

    .line 87
    invoke-virtual {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "msgID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->m:Ljava/lang/String;

    .line 89
    new-instance v0, Lcom/sec/chaton/chat/notification/j;

    invoke-direct {v0, p0}, Lcom/sec/chaton/chat/notification/j;-><init>(Lcom/sec/chaton/chat/notification/FragmentScreenNotification;)V

    .line 99
    const/4 v1, 0x5

    invoke-static {v1, v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/sec/common/f/c;

    invoke-direct {v1, v0}, Lcom/sec/common/f/c;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v1, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->i:Lcom/sec/common/f/c;

    .line 101
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 102
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->c:Ljava/lang/String;

    goto :goto_0

    .line 85
    :cond_1
    sget-object v0, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    invoke-virtual {v0}, Lcom/sec/chaton/e/w;->a()I

    move-result v0

    goto :goto_1

    .line 86
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->c:Ljava/lang/String;

    goto :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 107
    const v0, 0x7f0300d9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 109
    const v0, 0x7f0703d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->d:Landroid/widget/EditText;

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->d:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVerticalScrollBarEnabled(Z)V

    .line 111
    const v0, 0x7f0703d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->e:Landroid/widget/ImageView;

    .line 112
    const v0, 0x7f0703d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->f:Landroid/widget/LinearLayout;

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 114
    const v0, 0x7f0703d6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->g:Landroid/widget/TextView;

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    const v0, 0x7f0703d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->h:Landroid/widget/ImageView;

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 119
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 145
    const-string v0, "[onDestroy]"

    sget-object v1, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->i:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 147
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 148
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 125
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 126
    const-string v0, "[onResume]"

    sget-object v1, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/chat/notification/FragmentScreenNotification;->b()V

    .line 140
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 141
    return-void
.end method
