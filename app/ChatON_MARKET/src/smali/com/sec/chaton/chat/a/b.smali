.class public Lcom/sec/chaton/chat/a/b;
.super Lcom/sec/chaton/chat/a/x;
.source "AniconBubbleDrawer.java"

# interfaces
.implements Lcom/sec/chaton/chat/a/k;
.implements Lcom/sec/chaton/chat/a/m;
.implements Lcom/sec/chaton/chat/a/p;


# instance fields
.field private F:Landroid/graphics/drawable/Drawable;

.field private G:Z

.field private H:Landroid/widget/RelativeLayout$LayoutParams;

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private O:I

.field private P:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/chaton/chat/a/x;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    if-eqz v0, :cond_0

    .line 65
    const v0, 0x7f0b01a0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 68
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0b01cd

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a()V
    .locals 11

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, -0x2

    .line 104
    invoke-super {p0}, Lcom/sec/chaton/chat/a/x;->a()V

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    const/4 v0, 0x0

    .line 117
    iget-object v1, p0, Lcom/sec/chaton/chat/a/b;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090278

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 119
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 120
    iget-object v1, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v1, Lcom/sec/chaton/chat/el;->J:Landroid/widget/LinearLayout;

    .line 121
    iget-object v1, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v1, Lcom/sec/chaton/chat/el;->P:Landroid/widget/ImageView;

    .line 123
    iget-boolean v1, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    if-eqz v1, :cond_4

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->K:Landroid/widget/LinearLayout;

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v5, v0, Lcom/sec/chaton/chat/el;->U:Landroid/widget/LinearLayout;

    .line 127
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v0, p0, Lcom/sec/chaton/chat/a/b;->H:Landroid/widget/RelativeLayout$LayoutParams;

    .line 130
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/chaton/chat/a/b;->H:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 131
    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 132
    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 133
    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {v0, v9, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 134
    const/4 v6, 0x7

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 136
    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v10, v1

    move-object v1, v3

    move-object v3, v2

    move-object v2, v10

    .line 163
    :goto_1
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 166
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 167
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 168
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 169
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/chat/a/b;->F:Landroid/graphics/drawable/Drawable;

    .line 175
    invoke-virtual {v1, v8}, Landroid/view/View;->setBackgroundColor(I)V

    .line 178
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    if-eqz v0, :cond_2

    .line 179
    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/b;->K:I

    .line 180
    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/b;->L:I

    .line 181
    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/b;->I:I

    .line 182
    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/b;->J:I

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->F:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v0, v1

    .line 185
    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 188
    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/b;->M:I

    .line 189
    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/b;->N:I

    .line 190
    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/b;->O:I

    .line 191
    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/chat/a/b;->P:I

    .line 194
    iget v0, p0, Lcom/sec/chaton/chat/a/b;->I:I

    iget v2, p0, Lcom/sec/chaton/chat/a/b;->K:I

    iget v5, p0, Lcom/sec/chaton/chat/a/b;->J:I

    iget v6, p0, Lcom/sec/chaton/chat/a/b;->L:I

    invoke-virtual {v1, v0, v2, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 198
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->p:Ljava/lang/String;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 199
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lt v1, v9, :cond_0

    .line 203
    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 205
    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/anicon/m;

    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v2

    invoke-direct {v1, v0, v4, v2}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;-><init>(Ljava/lang/String;II)V

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->j:Lcom/sec/common/f/c;

    invoke-virtual {v0, v3, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto/16 :goto_0

    .line 141
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v1, Lcom/sec/chaton/chat/el;->o:Landroid/widget/LinearLayout;

    .line 142
    iget-object v1, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v1, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    .line 146
    iget-boolean v1, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    if-eqz v1, :cond_4

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->p:Landroid/widget/LinearLayout;

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v5, v0, Lcom/sec/chaton/chat/el;->B:Landroid/widget/RelativeLayout;

    .line 150
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v0, p0, Lcom/sec/chaton/chat/a/b;->H:Landroid/widget/RelativeLayout$LayoutParams;

    .line 153
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v6, p0, Lcom/sec/chaton/chat/a/b;->H:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 154
    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 155
    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 156
    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {v0, v9, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 157
    const/4 v6, 0x5

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 159
    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v10, v1

    move-object v1, v3

    move-object v3, v2

    move-object v2, v10

    goto/16 :goto_1

    :cond_4
    move-object v1, v3

    move-object v3, v2

    move-object v2, v0

    goto/16 :goto_1
.end method

.method public a(Z)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 212
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/x;->a(Z)V

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09026d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 221
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 222
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v0, Lcom/sec/chaton/chat/el;->J:Landroid/widget/LinearLayout;

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v0, Lcom/sec/chaton/chat/el;->P:Landroid/widget/ImageView;

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->K:Landroid/widget/LinearLayout;

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->U:Landroid/widget/LinearLayout;

    move-object v4, v2

    move-object v2, v0

    move-object v8, v1

    move-object v1, v3

    move-object v3, v8

    .line 234
    :goto_0
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 236
    if-nez p1, :cond_0

    .line 237
    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->j:Lcom/sec/common/f/c;

    invoke-virtual {v0, v4}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 242
    :cond_0
    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 246
    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 247
    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 248
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->F:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->F:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->H:Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v0, :cond_2

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->H:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 258
    :cond_2
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    if-eqz v0, :cond_3

    .line 259
    invoke-virtual {v3, v7}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 260
    iget v0, p0, Lcom/sec/chaton/chat/a/b;->I:I

    iget v2, p0, Lcom/sec/chaton/chat/a/b;->K:I

    iget v4, p0, Lcom/sec/chaton/chat/a/b;->J:I

    iget v5, p0, Lcom/sec/chaton/chat/a/b;->L:I

    invoke-virtual {v3, v0, v2, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    move-object v0, v1

    .line 261
    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 264
    iget v0, p0, Lcom/sec/chaton/chat/a/b;->M:I

    iget v2, p0, Lcom/sec/chaton/chat/a/b;->N:I

    iget v3, p0, Lcom/sec/chaton/chat/a/b;->O:I

    iget v4, p0, Lcom/sec/chaton/chat/a/b;->P:I

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 269
    :cond_3
    iput-object v7, p0, Lcom/sec/chaton/chat/a/b;->F:Landroid/graphics/drawable/Drawable;

    .line 270
    iput-boolean v6, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    .line 271
    iput-object v7, p0, Lcom/sec/chaton/chat/a/b;->H:Landroid/widget/RelativeLayout$LayoutParams;

    .line 273
    iput v6, p0, Lcom/sec/chaton/chat/a/b;->I:I

    .line 274
    iput v6, p0, Lcom/sec/chaton/chat/a/b;->J:I

    .line 275
    iput v6, p0, Lcom/sec/chaton/chat/a/b;->K:I

    .line 276
    iput v6, p0, Lcom/sec/chaton/chat/a/b;->L:I

    .line 279
    iput v6, p0, Lcom/sec/chaton/chat/a/b;->M:I

    .line 280
    iput v6, p0, Lcom/sec/chaton/chat/a/b;->N:I

    .line 281
    iput v6, p0, Lcom/sec/chaton/chat/a/b;->O:I

    .line 282
    iput v6, p0, Lcom/sec/chaton/chat/a/b;->P:I

    .line 284
    return-void

    .line 227
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v0, Lcom/sec/chaton/chat/el;->o:Landroid/widget/LinearLayout;

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v0, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v0, Lcom/sec/chaton/chat/el;->p:Landroid/widget/LinearLayout;

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->B:Landroid/widget/RelativeLayout;

    move-object v4, v2

    move-object v2, v0

    move-object v8, v1

    move-object v1, v3

    move-object v3, v8

    goto/16 :goto_0
.end method

.method protected c()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x6

    const/4 v5, 0x0

    .line 76
    invoke-super {p0}, Lcom/sec/chaton/chat/a/x;->c()Ljava/lang/String;

    move-result-object v2

    .line 77
    const-string v1, ""

    .line 78
    new-array v3, v5, [Ljava/lang/String;

    .line 80
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 81
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 82
    array-length v3, v2

    if-le v3, v0, :cond_2

    const-string v3, "mixed"

    aget-object v4, v2, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 83
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 84
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 85
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 91
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    iput-boolean v5, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    .line 97
    :goto_2
    return-object v0

    .line 94
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method protected d()V
    .locals 8

    .prologue
    const v7, 0x7f090127

    .line 289
    .line 290
    const/4 v4, 0x0

    .line 293
    const/4 v1, 0x0

    .line 296
    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x42de0000    # 111.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float v5, v0, v2

    .line 297
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v0, Lcom/sec/chaton/chat/el;->L:Landroid/widget/TextView;

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->N:Landroid/widget/TextView;

    .line 300
    iget-object v1, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v1, Lcom/sec/chaton/chat/el;->V:Landroid/widget/TextView;

    .line 301
    iget-object v1, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v1, v1, Lcom/sec/chaton/chat/el;->g:Landroid/widget/TextView;

    .line 310
    :goto_0
    iget-boolean v6, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    if-eqz v6, :cond_3

    .line 311
    iget-object v1, p0, Lcom/sec/chaton/chat/a/b;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09026d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, v4

    .line 312
    const/high16 v2, 0x42700000    # 60.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    .line 322
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->n()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 325
    iget-object v2, p0, Lcom/sec/chaton/chat/a/b;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f09012d

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 326
    iget-object v4, p0, Lcom/sec/chaton/chat/a/b;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v2, v4

    .line 328
    cmpl-float v4, v1, v2

    if-lez v4, :cond_4

    .line 329
    :goto_2
    const/high16 v2, 0x40a00000    # 5.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    .line 341
    :cond_0
    :goto_3
    iget v2, p0, Lcom/sec/chaton/chat/a/b;->z:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    .line 342
    const/high16 v2, 0x42200000    # 40.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    .line 345
    :cond_1
    sub-float v1, v5, v1

    .line 348
    invoke-virtual {p0, v1}, Lcom/sec/chaton/chat/a/b;->a(F)F

    move-result v1

    .line 351
    float-to-int v2, v1

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 353
    iget-object v2, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->t:Landroid/widget/ImageView;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->t:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_6

    .line 354
    const/high16 v2, 0x42240000    # 41.0f

    invoke-static {v2}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    .line 355
    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 359
    :goto_4
    return-void

    .line 303
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v3, v0, Lcom/sec/chaton/chat/el;->q:Landroid/widget/TextView;

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->s:Landroid/widget/TextView;

    .line 305
    iget-object v2, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v2, v2, Lcom/sec/chaton/chat/el;->C:Landroid/widget/TextView;

    goto/16 :goto_0

    .line 315
    :cond_3
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    .line 317
    if-eqz v1, :cond_7

    .line 318
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    add-float/2addr v1, v2

    goto/16 :goto_1

    :cond_4
    move v1, v2

    .line 328
    goto :goto_2

    .line 333
    :cond_5
    iget-object v2, p0, Lcom/sec/chaton/chat/a/b;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f09012e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 334
    iget-object v4, p0, Lcom/sec/chaton/chat/a/b;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v2, v4

    .line 336
    cmpl-float v4, v1, v2

    if-gtz v4, :cond_0

    move v1, v2

    goto/16 :goto_3

    .line 357
    :cond_6
    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_4

    :cond_7
    move v1, v2

    goto/16 :goto_1
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 417
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    return v0
.end method

.method public f()Lcom/sec/chaton/chat/a/n;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 422
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :goto_0
    return-object v0

    .line 426
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 427
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 430
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 431
    const-string v0, "onShare() - getTextContent() is null"

    const-string v1, "AniconBubbleDrawer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_2
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 438
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    return v0
.end method

.method public h()Lcom/sec/chaton/chat/a/n;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 443
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :goto_0
    return-object v0

    .line 446
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 447
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 450
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 451
    const-string v0, "onForward() - getTextContent() is null"

    const-string v1, "AniconBubbleDrawer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :cond_2
    new-instance v0, Lcom/sec/chaton/chat/a/n;

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/chaton/chat/a/n;-><init>(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 458
    iget-boolean v0, p0, Lcom/sec/chaton/chat/a/b;->G:Z

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 463
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->c()Ljava/lang/String;

    move-result-object v0

    .line 474
    :goto_0
    return-object v0

    .line 467
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 468
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 471
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_2

    .line 472
    const-string v0, "onCopy() - getTextContent() is null"

    const-string v1, "AniconBubbleDrawer"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/chat/a/b;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 367
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/chaton/chat/a/x;->onClick(Landroid/view/View;)V

    .line 374
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->u:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->h:Lcom/sec/chaton/chat/el;

    iget-object v0, v0, Lcom/sec/chaton/chat/el;->P:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    :cond_2
    check-cast p1, Landroid/widget/ImageView;

    .line 377
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 378
    const/4 v1, 0x0

    .line 380
    instance-of v2, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v2, :cond_4

    .line 381
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 384
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :goto_1
    if-eqz v0, :cond_3

    .line 393
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 395
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 398
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/chat/a/b;->p:Ljava/lang/String;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 399
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 403
    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/u;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 406
    iget-object v1, p0, Lcom/sec/chaton/chat/a/b;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/u;->i(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/z;

    move-result-object v1

    .line 407
    sget-object v2, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    if-eq v1, v2, :cond_0

    .line 408
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a()Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 385
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1

    .line 388
    :cond_4
    instance-of v2, v0, Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v2, :cond_5

    .line 389
    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method
